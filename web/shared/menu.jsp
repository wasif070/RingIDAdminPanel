<%-- 
    Document   : menu-new
    Created on : Jan 31, 2018, 12:28:03 PM
    Author     : rabby
--%>

<%@page import="com.ringid.admin.projectMenu.MenuDTO"%>
<%@page import="com.ringid.admin.projectMenu.MenuGenerator"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="java.util.HashMap"%>
<!-- BEGIN HEADER TOP -->
<%@include  file="/shared/header.jsp" %>
<!-- END HEADER TOP -->
<%@page import="com.ringid.admin.utils.Constants"%>
<%@page import="com.ringid.admin.actions.userFeatureManagement.UserFeatureTaskScheduler"%>
<%
    LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(com.ringid.admin.utils.Constants.LOGIN_DTO);
    List<MenuDTO> menuList = (List<MenuDTO>) request.getSession(true).getAttribute("menuList");
    if (menuList == null) {
        menuList = new ArrayList<>();
    }
    HashMap<Integer, List<MenuDTO>> subMenuMap = (HashMap<Integer, List<MenuDTO>>) request.getSession(true).getAttribute("subMenuMap");
    if (subMenuMap == null) {
        subMenuMap = new HashMap<>();
    }
%>

<div class="page-header-menu">
    <div class="container">
        <div class="hor-menu  ">
            <ul class="nav navbar-nav">
                <%for (MenuDTO menuDTO : menuList) {%>
                <%if (menuDTO.getMenuType() == MenuGenerator.CLASSIC_MENU_DROPDOWN) {%>
                <li aria-haspopup="true" class="menu-dropdown <%=menuDTO.getMenuClass()%>">
                    <%if (menuDTO.getParentId() <= 0) {%>
                    <a href="<%=menuDTO.getUrl()%>"><%=menuDTO.getFeatureName()%></a>
                    <%if (subMenuMap.containsKey(menuDTO.getId()) && subMenuMap.get(menuDTO.getId()).size() > 0) {%>
                    <ul class="dropdown-menu">
                        <%for (MenuDTO subMenuDTO : subMenuMap.get(menuDTO.getId())) {%>
                        <li aria-haspopup="true" class="<%=subMenuDTO.getMenuClass()%>">
                            <a href="<%=subMenuDTO.getUrl()%>"><%=subMenuDTO.getFeatureName()%></a>
                            <%if (subMenuMap.containsKey(subMenuDTO.getId()) && subMenuMap.get(subMenuDTO.getId()).size() > 0) {%>
                            <ul class="dropdown-menu">
                                <%for (MenuDTO subMenu2 : subMenuMap.get(subMenuDTO.getId())) {%>
                                <li aria-haspopup="true" class="<%=subMenu2.getMenuClass()%>">
                                    <a href="<%=subMenu2.getUrl()%>"> <%=subMenu2.getFeatureName()%> </a>
                                </li>
                                <%}%>
                            </ul>
                            <%}%>
                        </li>
                        <%}%>
                    </ul>
                    <%}%>
                    <%}%>
                </li>
                <%} else if (menuDTO.getMenuType() == MenuGenerator.MEGA_MENU_DROPDOWN) {%>
                <li aria-haspopup="true" class="menu-dropdown <%=menuDTO.getMenuClass()%>">
                    <%if (menuDTO.getParentId() <= 0) {%>
                    <a href="<%=menuDTO.getUrl()%>"><%=menuDTO.getFeatureName()%></a>
                    <%if (subMenuMap.containsKey(menuDTO.getId()) && subMenuMap.get(menuDTO.getId()).size() > 0) {%>
                    <ul class="dropdown-menu">
                        <li>
                            <div class="mega-menu-content">
                                <div class="row">
                                    <%
                                        int menuPerColum = 12;
                                        int menuSize = subMenuMap.get(menuDTO.getId()).size();
                                        int numberOfCloumn = menuSize / menuPerColum;
                                        if (menuSize % menuPerColum != 0) {
                                            numberOfCloumn++;
                                        }
                                        String columnDivClass = "col-md-" + (12 / numberOfCloumn);
                                    %>
                                    <%for (int i = 0, startIndex = 0; i < numberOfCloumn; i++, startIndex += menuPerColum) {%>
                                    <div class="<%=columnDivClass%>">
                                        <ul class="mega-menu-submenu">
                                            <%for (int j = startIndex; j < (startIndex + menuPerColum) && j < menuSize; j++) {%>
                                            <% MenuDTO subMenu = subMenuMap.get(menuDTO.getId()).get(j);%>
                                            <li class="<%=subMenu.getMenuClass()%>">
                                                <a href="<%=subMenu.getUrl()%>"><%=subMenu.getFeatureName()%></a>
                                            </li>
                                            <%}%>
                                        </ul>
                                    </div>
                                    <%}%>
                                </div>
                            </div>
                        </li>
                    </ul>
                    <%}%>
                    <%}%>
                </li>
                <%}%>
                <%}%>
            </ul>
        </div>
    </div>
</div>
