<%-- 
    Document   : header
    Created on : Jan 2, 2018, 6:54:10 PM
    Author     : mamun
    must include loginTimeExpiry.jsp file in main page otherwise get loginDTO2 from session to use this header
--%>
<%@page import="com.ringid.admin.dto.adminAuth.LoginDTO"%>
<%@page import="com.ringid.admin.projectMenu.MenuNames"%>
<%
    LoginDTO loginDTO2 = (LoginDTO) request.getSession(true).getAttribute(com.ringid.admin.utils.Constants.LOGIN_DTO);
%>
<%@page import="com.ringid.admin.utils.Constants"%>
<div class="page-header-top">
    <div class="container">
        <!-- BEGIN LOGO -->
        <div class="page-logo">
            <a href="welcome.do">
                <img src="assets/layouts/layout3/img/logo.jpg" alt="logo" class="logo-default">
            </a>
        </div>
        <!-- END LOGO -->
        <!-- BEGIN RESPONSIVE MENU TOGGLER -->
        <a href="javascript:;" class="menu-toggler"></a>
        <!-- END RESPONSIVE MENU TOGGLER -->
        <!-- BEGIN TOP NAVIGATION MENU -->
        <div class="top-menu">
            <ul class="nav navbar-nav pull-right">
                <!-- BEGIN USER LOGIN DROPDOWN -->
                <li class="dropdown dropdown-user dropdown-dark">
                    <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                        <%if (loginDTO2.getProfileImageUrl() != null && loginDTO2.getProfileImageUrl().length() > 0) {%>
                        <img alt="" class="img-circle" src="<%=Constants.IMAGE_BASE_URL + loginDTO2.getProfileImageUrl()%>">
                        <%} else {%>
                        <img alt="" class="img-circle" src="assets/layouts/layout3/img/avatar.png">
                        <% }%>
                        <span class="username username-hide-mobile">
                            <%=loginDTO2.getUserName()%>
                        </span>
                    </a>
                    <ul class="dropdown-menu dropdown-menu-default">
                        <%if ((loginDTO2.getPermissionLevel() == Constants.SUPER_ADMIN) || (loginDTO2.getFeauterMapList().containsKey(MenuNames.ADMIN_USER)
                                    && (loginDTO2.getFeauterMapList().get(MenuNames.ADMIN_USER).getPermissionLevel() > 0 || loginDTO2.getFeauterMapList().get(MenuNames.ADMIN_USER).getHasChildPermission()))) {%>
                        <li class="users"> <!-- active class go here -->
                            <a href="usersListInfo.do">
                                <i class="icon-users"></i> Admin Users </a>
                        </li>
                        <li class="divider"> </li>
                            <%}%>
                            <%if ((loginDTO2.getPermissionLevel() == Constants.SUPER_ADMIN) || (loginDTO2.getFeauterMapList().containsKey(MenuNames.ADMIN_USER_ROLE)
                                    && (loginDTO2.getFeauterMapList().get(MenuNames.ADMIN_USER_ROLE).getPermissionLevel() > 0 || loginDTO2.getFeauterMapList().get(MenuNames.ADMIN_USER_ROLE).getHasChildPermission()))) {%>
                        <li class="userRole"> <!-- active class go here -->
                            <a href="showUserRoleList.do">
                                <i class="icon-settings"></i> Admin User Role </a>
                        </li>
                        <li class="divider"> </li>
                            <%}%>
                            <%if ((loginDTO2.getPermissionLevel() == Constants.SUPER_ADMIN) || (loginDTO2.getFeauterMapList().containsKey(MenuNames.PROJECT_FEATURE)
                                    && (loginDTO2.getFeauterMapList().get(MenuNames.PROJECT_FEATURE).getPermissionLevel() > 0 || loginDTO2.getFeauterMapList().get(MenuNames.PROJECT_FEATURE).getHasChildPermission()))) {%>
                        <li class="features"> <!-- active class go here -->
                            <a href="showFeatureList.do">
                                <i class="icon-star"></i> Project Feature </a>
                        </li>
                        <li class="divider"> </li>
                            <%}%>
                            <%if ((loginDTO2.getPermissionLevel() == Constants.SUPER_ADMIN) || (loginDTO2.getFeauterMapList().containsKey(MenuNames.PROJECT_FEATURE)
                                    && (loginDTO2.getFeauterMapList().get(MenuNames.PROJECT_FEATURE).getPermissionLevel() > 0 || loginDTO2.getFeauterMapList().get(MenuNames.PROJECT_FEATURE).getHasChildPermission()))) {%>
                        <li class="featureRoleMapping"> <!-- active class go here -->
                            <a href="showFeatureRole.do">
                                <i class="icon-settings"></i> Feature Role Mapping </a>
                        </li>
                        <li class="divider"> </li>
                            <%}%>
                        <li>
                            <a href="logout.do">
                                <i class="icon-key"></i> Log Out </a>
                        </li>
                    </ul>
                </li>
                <!-- END USER LOGIN DROPDOWN -->
            </ul>
        </div>
        <!-- END TOP NAVIGATION MENU -->
    </div>
</div>
