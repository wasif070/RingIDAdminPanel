<%-- 
    Document   : footer.jsp
    Created on : Jan 22, 2018, 3:42:40 PM
    Author     : Rabby
--%>

<div class="page-wrapper-row">
    <div class="page-wrapper-bottom">
        <div class="page-footer">
            <div class="container"> Copyright &copy; 2015 All right reserved by
                <a target="_blank" href="http://www.ipvision.ca/">IPvision canada inc</a>
            </div>
        </div>
        <div class="scroll-to-top">
            <i class="icon-arrow-up"></i>
        </div>
    </div>
</div>
