<%
    com.ringid.admin.dto.adminAuth.LoginDTO loginDT = (com.ringid.admin.dto.adminAuth.LoginDTO) request.getSession(true).getAttribute(com.ringid.admin.utils.Constants.LOGIN_DTO);
    if (request.getParameter("logout") != null) {
        loginDT = null;
        request.getSession(true).removeAttribute(com.ringid.admin.utils.Constants.LOGIN_DTO);
        response.sendRedirect("./index.jsp");
        return;
    }
    if (loginDT == null) {
        response.sendRedirect("/configportal/index.do?reset=1");
        return;
    } else {
        if (loginDT.getLoginTime() + com.ringid.admin.utils.Constants.LOGIN_EXPIRE_TIME < System.currentTimeMillis()) {
            loginDT = null;
            request.getSession(true).removeAttribute(com.ringid.admin.utils.Constants.LOGIN_DTO);
            response.sendRedirect("./index.do?reset=1");
            return;
        } else {
            loginDT.setLoginTime(System.currentTimeMillis());
            request.getSession().setAttribute(com.ringid.admin.utils.Constants.LOGIN_DTO, loginDT);
        }
    }
%>