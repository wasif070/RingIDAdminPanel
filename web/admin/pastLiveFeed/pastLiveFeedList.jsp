<%-- 
    Document   : new-pastLiveFeedList
    Created on : May 8, 2018, 10:50:34 AM
    Author     : mamun
--%>

<%@include file="/login/loginTimeExpiry.jsp"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.ringid.admin.projectMenu.MenuNames"%>
<%@page import="com.ringid.admin.utils.Constants"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@include  file="/shared/head.jsp" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <style>
            .timeline::before{
                content: none !important;
            }
        </style>
        <title>Admin Panel | Past Live Feed List</title>
        <%@include file="/shared/css-list.jsp"%>
    </head>
    <body class="page-container-bg-solid">
        <div class="page-wrapper">
            <div class="page-wrapper-row">
                <div class="page-wrapper-top">
                    <div class="page-header">
                        <%@include  file="/shared/menu.jsp" %>
                        <%
                            String FEATURE_NAME = MenuNames.PAST_LIVE_FEED;
                            int TOTAL_RECORDS = 0, TOTAL_PAGES = 0, CURRENT_PAGE_NO = 0, STARTING_RECORD_NO = 0, ENDING_RECORD_NO = 0;
                            String disabledPrev = "", disabledNext = "";
                            if (request.getAttribute(Constants.TOTAL_RECORDS) != null) {
                                TOTAL_RECORDS = Integer.valueOf(request.getAttribute(Constants.TOTAL_RECORDS).toString());
                            }
                            if (request.getAttribute(Constants.TOTAL_PAGES) != null) {
                                TOTAL_PAGES = Integer.valueOf(request.getAttribute(Constants.TOTAL_PAGES).toString());
                            }
                            if (request.getAttribute(Constants.CURRENT_PAGE_NO) != null) {
                                CURRENT_PAGE_NO = Integer.valueOf(request.getAttribute(Constants.CURRENT_PAGE_NO).toString());
                            }
                            if (request.getAttribute(Constants.STARTING_RECORD_NO) != null) {
                                STARTING_RECORD_NO = Integer.valueOf(request.getAttribute(Constants.STARTING_RECORD_NO).toString());
                            }
                            if (request.getAttribute(Constants.ENDING_RECORD_NO) != null) {
                                ENDING_RECORD_NO = Integer.valueOf(request.getAttribute(Constants.ENDING_RECORD_NO).toString());
                            }
                            if (STARTING_RECORD_NO <= 0) {
                                disabledPrev = "disabled";
                                STARTING_RECORD_NO = 0;
                            }
                            if (ENDING_RECORD_NO >= TOTAL_RECORDS) {
                                disabledNext = "disabled";
                                ENDING_RECORD_NO = TOTAL_RECORDS;
                            }
                        %>
                    </div>
                </div>
            </div>
            <div class="page-wrapper-row full-height">
                <div class="page-wrapper-middle">
                    <div class="page-container">
                        <div class="page-content-wrapper">
                            <div class="page-content">
                                <div class="container">
                                    <ul class="page-breadcrumb breadcrumb">
                                        <li>
                                            <a href="welcome.do">Admin Panel</a>
                                            <span style="font-size: 14px; margin: 0 auto;" class="fa fa-angle-right"></span>
                                        </li>
                                        <li>
                                            <a href="javascript:;">Live/Channel</a>
                                            <i style="font-size: 14px; margin: 0 auto;" class="fa fa-angle-right"></i>
                                        </li>
                                        <li>
                                            <span id="features" data-class-active=".live-o-channel .pastLiveFeed"> Past Live Feed </span>
                                        </li>
                                    </ul>
                                    <div class="page-content-inner">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="tabbable-line boxless tabbable-reversed">
                                                    <div class="tab-content">
                                                        <div class="portlet box green">
                                                            <div class="portlet-title">
                                                                <div class="caption">
                                                                    <i class="fa fa-list-ul"></i>
                                                                    <span class="caption-subject bold uppercase"> Past Live Feed</span>
                                                                    <span class="caption-helper" style="color: white;">own live feed</span>
                                                                </div>
                                                                <div class="tools">
                                                                    <a href="javascript:;" class="collapse"> </a>
                                                                </div>
                                                            </div>
                                                            <div class="portlet-body form">
                                                                <html:form action="pastLiveFeedList" styleClass="form-horizontal" method="GET"  acceptCharset="UTF-8">
                                                                    <div class="form-body">
                                                                        <div class="form-group">
                                                                            <div class="col-md-6 col-sm-6">
                                                                                <html:select styleClass="form-control input-inline" property="viewType" onchange="javascript: submitform(1, 1, this);">
                                                                                    <html:option value="59">All Celebrity Past Live Feed</html:option>
                                                                                    <html:option value="70">Top Room Past Live Feed</html:option>
                                                                                    <html:option value="67">Celebrity Featured Past Live Feed</html:option>
                                                                                    <html:option value="72">Verified Top Room Past Live Feed</html:option>
                                                                                    <html:option value="73">Verified Celebrity Past Live Feed</html:option>
                                                                                </html:select>
                                                                            </div>
                                                                            <div class="col-md-6 col-sm-6">
                                                                                <div class="input-group" style="float: right;">
                                                                                    <input name="searchText" type="text" class="form-control input-sm input-small" placeholder="type userId or ringId" value="<bean:write name="SearchUserForm" property="searchText"/>">
                                                                                    <span>
                                                                                        <button class="btn green-soft uppercase bold input-sm" type="submit"><i class="icon-magnifier"></i></button>
                                                                                        <input name="action" type="hidden" value="<%=Constants.SEARCH%>" />    
                                                                                        <html:hidden property="column" />
                                                                                        <html:hidden property="sort" />
                                                                                    </span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <c:choose>
                                                                            <c:when test="${SearchUserForm.searchUserDTO.getCelebrityPastLiveFeed() ne null && SearchUserForm.searchUserDTO.getCelebrityPastLiveFeed().getFeedDTOList().size() > 0}">
                                                                                <input type="hidden" id="utId" value="<%=Constants.LOGGED_IN_USER_ID%>"/>
                                                                                <input type="hidden" id="pvtUUID" value="${SearchUserForm.searchUserDTO.getCelebrityPastLiveFeed().nextPivotId}"/>
                                                                                <div id="liveFeed" class="timeline">
                                                                                    <c:forEach items="${SearchUserForm.searchUserDTO.getCelebrityPastLiveFeed().getFeedDTOList()}" var="feedDTO">
                                                                                        <!-- TIMELINE ITEM -->
                                                                                        <div class="form-group">
                                                                                            <div class="col-md-12">
                                                                                                <div class='timeline-item'>
                                                                                                    <div class='timeline-body' style='margin-left: 0px;'>
                                                                                                        <div class='timeline-body-head'>
                                                                                                            <div class='timeline-badge' style='width: 50px;height: 65px;'>
                                                                                                                <c:choose>
                                                                                                                    <c:when test="${feedDTO.getWallOwnerInfo().getProfileImageURL() != null && feedDTO.getWallOwnerInfo().getProfileImageURL().length() > 0}">
                                                                                                                        <img src="<%=Constants.MEDIA_CLOUD_BASE_URL%>${feedDTO.getWallOwnerInfo().getProfileImageURL()}" class="timeline-badge-userpic" style='height: 44px;width: 44px;'>
                                                                                                                    </c:when>
                                                                                                                    <c:otherwise>
                                                                                                                        <img src="assets/global/img/default-pp.jpg" class="timeline-badge-userpic" style='height: 44px;width: 44px;'>
                                                                                                                    </c:otherwise>
                                                                                                                </c:choose>
                                                                                                            </div>
                                                                                                            <div class='timeline-body-head-caption' style='float: none;'>
                                                                                                                <a href='searchUser2.do?searchText=${feedDTO.getWallOwnerId()}' class='timeline-body-title font-blue-madison'>
                                                                                                                    ${feedDTO.getWallOwnerInfo().getFirstName()}
                                                                                                                </a>
                                                                                                                <span class='font-grey-cascade'> was </span><span class='font-red-intense'>LIVE </span><span class='font-grey-cascade'>on </span> 
                                                                                                                <jsp:useBean id="dateValuePastLiveTime" class="java.util.Date"/>
                                                                                                                <jsp:setProperty name="dateValuePastLiveTime" property="time" value="${feedDTO.getLiveTime()}"/>
                                                                                                                <span class='font-green-haze'><fmt:formatDate value="${dateValuePastLiveTime}" pattern="dd MMM yyyy hh:mma"/></span>
                                                                                                                <c:choose>
                                                                                                                    <c:when test="${feedDTO.getExpiredAfter() != null && feedDTO.getExpiredAfter() > 0}">
                                                                                                                        <jsp:useBean id="dateValueExpiredAfter" class="java.util.Date"/>
                                                                                                                        <jsp:setProperty name="dateValueExpiredAfter" property="time" value="${feedDTO.getExpiredAfter()}"/>
                                                                                                                        <br>
                                                                                                                        <span class='font-red-intense'>Validity <fmt:formatDate value="${dateValueExpiredAfter}" pattern="dd MMM yyyy hh:mma"/></span>
                                                                                                                    </c:when>
                                                                                                                    <c:otherwise>
                                                                                                                        <br>
                                                                                                                        <span class='font-red-intense'>Validity Unlimited</span>
                                                                                                                    </c:otherwise>
                                                                                                                </c:choose>
                                                                                                                <jsp:useBean id="dateValuePastLiveFeed" class="java.util.Date"/>
                                                                                                                <jsp:setProperty name="dateValuePastLiveFeed" property="time" value="${feedDTO.getAddTime()}"/>
                                                                                                                <br>
                                                                                                                <span class='timeline-body-time font-grey-cascade' style='margin-left: 0px;font-size: 12px;'><i class="fa fa-clock-o"></i> <fmt:formatDate value="${dateValuePastLiveFeed}" pattern="hh:mma MM/dd/yyyy"/>  </span>
                                                                                                                <span class='timeline-body-head-actions' style='float: none;'>
                                                                                                                    <div class='btn-group'>
                                                                                                                        <button class='btn btn-circle green btn-outline btn-sm dropdown-toggle' type='button' data-toggle='dropdown' data-hover='dropdown' data-close-others='true'> Actions
                                                                                                                            <i class='fa fa-angle-down'></i>
                                                                                                                        </button>
                                                                                                                        <ul class='dropdown-menu pull-right' role='menu'>
                                                                                                                            <c:choose>
                                                                                                                                <c:when test="${SearchUserForm.getViewType() == 70}">
                                                                                                                                    <li>
                                                                                                                                        <a href="javascript:processRequest(3, getParamData('${feedDTO.getFeedId()}', '${feedDTO.getLiveTimeUUID()}'), <bean:write name="SearchUserForm" property="viewType" />, 0)">Make Verified Top Past Live Feed</a>
                                                                                                                                    </li>
                                                                                                                                </c:when>
                                                                                                                                <c:when test="${SearchUserForm.getViewType() == 67}">
                                                                                                                                    <li>
                                                                                                                                        <a href="javascript:processRequest(4, getParamData('${feedDTO.getFeedId()}', '${feedDTO.getLiveTimeUUID()}'), <bean:write name="SearchUserForm" property="viewType" />, 1)">Remove Featured Past Live Feed</a>
                                                                                                                                    </li>
                                                                                                                                </c:when>
                                                                                                                                <c:when test="${SearchUserForm.getViewType() == 72}">
                                                                                                                                    <li>
                                                                                                                                        <a href="javascript:processRequest(4, getParamData('${feedDTO.getFeedId()}', '${feedDTO.getLiveTimeUUID()}'), <bean:write name="SearchUserForm" property="viewType" />, 0)">Remove Verified Top Past Live Feed</a>
                                                                                                                                    </li>
                                                                                                                                </c:when>
                                                                                                                                <c:when test="${SearchUserForm.getViewType() == 73}">
                                                                                                                                    <li>
                                                                                                                                        <a href="javascript:processRequest(4, getParamData('${feedDTO.getFeedId()}', '${feedDTO.getLiveTimeUUID()}'), <bean:write name="SearchUserForm" property="viewType" />, 0)">Remove Verified Celebrity Past Live Feed</a>
                                                                                                                                    </li>
                                                                                                                                </c:when>
                                                                                                                                <c:otherwise>
                                                                                                                                    <li>
                                                                                                                                        <a href="javascript:processRequest(3, getParamData('${feedDTO.getFeedId()}', '${feedDTO.getLiveTimeUUID()}'), <bean:write name="SearchUserForm" property="viewType" />, 1)">Make Featured Past Live Feed</a>
                                                                                                                                    </li>
                                                                                                                                    <li>
                                                                                                                                        <a href="javascript:processRequest(3, getParamData('${feedDTO.getFeedId()}', '${feedDTO.getLiveTimeUUID()}'), <bean:write name="SearchUserForm" property="viewType" />, 0)">Make Verified Celebrity Past Live Feed</a>
                                                                                                                                    </li>
                                                                                                                                </c:otherwise>
                                                                                                                            </c:choose>
                                                                                                                            <li>
                                                                                                                                <a data-toggle="modal" href="#basic" onclick="javascript:setParamForValidityEdit('${feedDTO.getFeedId()}', '${feedDTO.getExpiredAfter()}', '${feedDTO.getLive().getUrl()}', '${feedDTO.getWallOwnerId()}')"> Edit Validity </a>
                                                                                                                            </li>
                                                                                                                        </ul>
                                                                                                                    </div>
                                                                                                                </span>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div class='timeline-body-content' style='margin-top: 20px;'>
                                                                                                            <span class='font-grey-cascade'>
                                                                                                                <p style="word-wrap: break-word;">
                                                                                                                    ${feedDTO.getStatus()}
                                                                                                                </p>
                                                                                                            </span>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </c:forEach>
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-actions">
                                                                                <div class="row">
                                                                                    <div class="col-md-12">
                                                                                        <div class="row text-center">
                                                                                            <button id="featurepastLive_seeMoreFeed" type="button" onclick="javascript:seeMoreFeedList(<bean:write name="SearchUserForm" property="viewType" />);" class="btn btn-default mt-ladda-btn ladda-button btn-outline btn-circle featurepastLive_seeMoreFeed" data-style="expand-left" data-spinner-color="#333">
                                                                                                <span class="ladda-label">See More</span>
                                                                                            </button>
                                                                                        </div>
                                                                                    </c:when>
                                                                                    <c:otherwise>
                                                                                        <div class="row text-center">
                                                                                            <span class='font-grey-cascade'>No more feed</span>
                                                                                        </div>
                                                                                    </c:otherwise>
                                                                                </c:choose>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </html:form>
                                                                <div class="modal fade" id="basic" tabindex="-1" role="basic" aria-hidden="true">
                                                                    <div class="modal-dialog">
                                                                        <div class="modal-content">
                                                                            <div class="modal-header">
                                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                                                <h4 class="modal-title">Edit Feed Validity</h4>
                                                                            </div>
                                                                            <div class="modal-body">
                                                                                <div id="divforvalidity" class="row" style="display: none;">
                                                                                    <label id="msgforvalidity" class="control-label"></label>
                                                                                </div>
                                                                                <div class="row">
                                                                                    <div class="col-md-12">
                                                                                        <label class="col-md-3 control-label">Is Unlimited</label>
                                                                                        <div class="col-md-9">
                                                                                            <select id="select_unlimited" class="form-control input-inline" name="unlimited">
                                                                                                <option value="false">NO</option>
                                                                                                <option value="true">Yes</option>
                                                                                            </select>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="row">
                                                                                    <div class="col-md-12">
                                                                                        <label class="col-md-3 control-label">Validity</label>
                                                                                        <div class="col-md-9">
                                                                                            <input type="text" id="expiredAfter" placeholder="yyyy-mm-dd HH:mm:ss" class="form-control date form_datetime" title="time" />
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="modal-footer">
                                                                                <button type="button" data-dismiss="modal" class="btn dark btn-outline">Close</button>
                                                                                <button type="button" class="btn green" onclick="javascript:editStatus()">Save changes</button>
                                                                                <input type="hidden" id="wallOwnerId" />
                                                                                <input type="hidden" id="mediaurl" />
                                                                                <input type="hidden" id="feedId" />
                                                                            </div>
                                                                        </div>
                                                                        <!-- /.modal-content -->
                                                                    </div>
                                                                    <!-- /.modal-dialog -->
                                                                </div>
                                                                <!-- /.modal -->
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <%@include file="/shared/footer.jsp"%>
        </div>
        <%@include file="/shared/script-list.jsp"%>
        <script type="text/javascript">
            var base_url = '<%=resourceURL%>';
            var media_base_url = '<%=Constants.MEDIA_CLOUD_BASE_URL%>';
            $(".form_datetime").datetimepicker({
                autoclose: true,
                format: "yyyy-mm-dd hh:ii:ss"
            });
            function setParamForValidityEdit(feedId, expiredAfter, url, wallOwnerId) {
                $('#feedId').val(feedId);
                if (expiredAfter != null && !isNaN(expiredAfter) && Number(expiredAfter) > 0) {
                    var date = new Date(Number(expiredAfter));
                    $('#expiredAfter').val(date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate() + " " + date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds());
                } else {
                    $('#select_unlimited').val("true");
                }
                $('#mediaurl').val(url);
                $('#wallOwnerId').val(wallOwnerId);
            }
        </script>
    </body>
</html>
