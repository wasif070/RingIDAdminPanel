<%-- 
    Document   : donationPageTable
    Created on : Aug 20, 2017, 11:27:02 PM
    Author     : mamun
--%>

<%@page import="com.ringid.admin.utils.Constants"%>
<%@page import="java.util.List"%>
<%
    int action = (Integer) request.getSession(true).getAttribute("formType");
%>
<div class="form-body">
    <div class="form-group">
        <label class="col-md-3 control-label">Page Name <span style="color: red">*</span></label>
        <div class="col-md-4">
            <html:text property="pageName" title="Page Name" styleClass="form-control" />
            <html:messages id="pageName" property="pageName">
                <span style="color:red"> Page Name Required </span>
            </html:messages>
        </div>
    </div>
    <%if (!(action == Constants.UPDATE)) {%>
    <div class="form-group">
        <label class="col-md-3 control-label">Owner RingId <span style="color: red">*</span></label>
        <div class="col-md-4">
            <html:text property="pageOwnerId" title="Owner RingId" styleClass="form-control" />
            <html:messages id="pageOwnerId" property="pageOwnerId">
                <span style="color:red"> Owner RingId Required </span>
            </html:messages>
        </div>
    </div>
    <%}%>
    <div class="form-group">
        <%
            List<com.ringid.country.CountryDTO> countryDTOs = com.ringid.country.CountryLoader.getInstance().getCountryList();
        %>
        <label class="col-md-3 control-label">Country <span style="color: red">*</span></label>
        <div class="col-md-4">
            <html:select property="country" styleClass="form-control"> 
                <html:option  value=""> Select Country </html:option>     
                    <%
                        for (com.ringid.country.CountryDTO dto : countryDTOs) {
                    %>
                <html:option value="<%=dto.getName()%>"><%=dto.getName()%></html:option>
                    <% }%>  
            </html:select>
            <html:messages id="country" property="country">
                <span style="color:red"> Please Select Country </span>
            </html:messages>
        </div>
    </div>
    <div class="form-group ">
        <label class="control-label col-md-3"> Banner Image </label>
        <div class="col-md-9">
            <div class="fileinput fileinput-new" data-provides="fileinput">
                <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px; line-height: 150px;">
                    <img src="<%=Constants.IMAGE_BASE_URL%>${DonationPageForm.bannerImageUrl}" />
                </div>
                <div>
                    <span class="btn green btn-outline btn-file">
                        <span class="fileinput-new"> Select image </span>
                        <span class="fileinput-exists"> Change image </span>
                        <html:file property="theFileBannerImage" onchange="removeSpanText()" />
                    </span>
                    <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                    <span id="msgSpan" style="display: inline-block;">
                        <html:messages id="theFileBannerImage" property="theFileBannerImage">
                            <bean:write name="theFileBannerImage"  filter="false"/>
                        </html:messages>
                    </span>
                </div>
            </div>
        </div>
    </div>
    <div class="form-group ">
        <label class="control-label col-md-3"> Profile Image </label>
        <div class="col-md-9">
            <div class="fileinput fileinput-new" data-provides="fileinput">
                <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px; line-height: 150px;">
                    <img src="<%=Constants.IMAGE_BASE_URL%>${DonationPageForm.profileImageUrl}" />
                </div>
                <div>
                    <span class="btn green btn-outline btn-file">
                        <span class="fileinput-new"> Select image </span>
                        <span class="fileinput-exists"> Change image </span>
                        <html:file property="theFileProfileImage" onchange="removeSpanText()" />
                    </span>
                    <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                    <span id="msgSpan" style="display: inline-block;">
                        <html:messages id="theFileProfileImage" property="theFileProfileImage">
                            <bean:write name="theFileProfileImage"  filter="false"/>
                        </html:messages>
                    </span>
                </div>
            </div>
        </div>
    </div>
    <div class="form-group ">
        <label class="control-label col-md-3"> Cover Image </label>
        <div class="col-md-9">
            <div class="fileinput fileinput-new" data-provides="fileinput">
                <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px; line-height: 150px;">
                    <img src="<%=Constants.IMAGE_BASE_URL%>${DonationPageForm.coverImageUrl}" />
                </div>
                <div>
                    <span class="btn green btn-outline btn-file">
                        <span class="fileinput-new"> Select image </span>
                        <span class="fileinput-exists"> Change image </span>
                        <html:file property="theFileCoverImage" onchange="removeSpanText()" />
                    </span>
                    <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                    <span id="msgSpan" style="display: inline-block;">
                        <html:messages id="theFileCoverImage" property="theFileCoverImage">
                            <bean:write name="theFileCoverImage"  filter="false"/>
                        </html:messages>
                    </span>
                </div>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label"> Visible </label>
        <div class="col-md-4" style="margin-top: 6px;">
            <html:radio property="visible" value="1" styleClass="mt-radio mt-radio-outline" style="margin-right: 3px;">True</html:radio>
            <html:radio property="visible" value="0" styleClass="mt-radio mt-radio-outline" style="margin-right: 3px; margin-left: 20px;">False</html:radio>
        </div>
    </div>
</div>
<div class="form-actions">
    <div class="row">
        <div class="col-md-offset-3 col-md-4">
            <button type="submit" class="btn green" name="saveButton">Submit</button>
            <a href="showDonationPageList.do" class="btn default">Cancel</a>
            <%if (action == Constants.UPDATE) {%>
            <html:hidden property="pageOwnerId"/>
            <html:hidden property="pageType"/>
            <html:hidden property="pageId"/>
            <html:hidden property="pageRingId"/>
            <html:hidden property="bannerImageUrl"/>
            <%}%>
            <input type="hidden" name="action" value="<%=request.getSession(true).getAttribute("formType")%>" />
        </div>
    </div>
</div>
<script type="text/javascript">
    function removeSpanText() {
        document.getElementById("msgSpan").innerHTML = "";
    }
</script>