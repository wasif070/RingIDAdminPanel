<%-- 
    Document   : m_spamReasonsTable
    Created on : Feb 14, 2017, 7:25:26 PM
    Author     : ipvision
--%>


<div class="form-body">
    <div class="form-group">
        <label class="col-md-3 control-label">Span Type</label>
        <div class="col-md-4">
            <html:select property="spamType" styleClass="form-control"> 
                <html:option  value="1">SPAM USER</html:option>     
                <html:option  value="2">SPAM FEED</html:option>  
                <html:option  value="3">SPAM IMAGE</html:option>
                <html:option  value="4">SPAM MEDIA CONTENT</html:option>
                <html:option  value="5">SPAM CHANNEL</html:option>
                <html:option  value="6">SPAM LIVE STREAM</html:option>
                <html:option  value="7">SPAM PAGE</html:option>
                <html:option  value="8">SPAM COMMENT</html:option>
            </html:select>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label">Reasons <span style="color: red">*</span></label>
        <div class="col-md-4">
            <html:textarea property="reason" title="Reasons" styleClass="form-control" />
            <html:messages id="reason" property="reason">
                <bean:write name="reason"  filter="false"/>
            </html:messages>
        </div>
    </div>
    <div class="form-group last">
        <label class="col-md-3 control-label"></label>
        <div class="col-md-4">
            <%
                if (request.getSession(true).getAttribute("error") == "2") {
            %>
            <td> <html:messages id="message" property="message"><bean:write name="msg"  filter="false"/></html:messages>
            <bean:write name="SpamReasonsForm" property="message" filter="false"/></td>
            <%}%>
        </div>
    </div>
</div>
<div class="form-actions">
    <div class="row">
        <div class="col-md-offset-3 col-md-4">
            <button type="submit" class="btn green" name="saveButton">Submit</button>
            <a href="spamReasonsListInfo.do" class="btn default">Cancel</a>
            <html:hidden property="id"/>
            <input type="hidden" name="action" value="<%=request.getSession(true).getAttribute("formType")%>" />
        </div>
    </div>
</div>
