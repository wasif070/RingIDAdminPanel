<%-- 
    Document   : updateChannel
    Created on : May 6, 2018, 12:44:09 PM
    Author     : mamun
--%>

<%@page import="com.ringid.admin.actions.servers.streamingservers.StreamingServerTaskScheduler"%>
<%@page import="com.ringid.admin.actions.servers.streamingservers.StreamingServerDTO"%>
<%@page import="com.ringid.admin.actions.servers.channelservers.ChannelServerTaskScheduler"%>
<%@page import="com.ringid.admin.actions.servers.channelservers.ChannelServerDTO"%>
<%@page import="java.util.List"%>
<%@include file="/login/loginTimeExpiry.jsp"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@include  file="/shared/head.jsp" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Admin Panel | Edit Channel </title>
        <%@include file="/shared/css-list.jsp"%>
    </head>
    <body class="page-container-bg-solid">
        <div class="page-wrapper">
            <div class="page-wrapper-row">
                <div class="page-wrapper-top">
                    <div class="page-header">
                        <%@include  file="/shared/menu.jsp" %>
                        <%
                            String msgStr = "", classForTab0 = "", classForTab1 = "", classForTab2 = "";
                            int activeTab = 0;
                            if (request.getAttribute("errormsg") != null && request.getAttribute("errormsg").toString().length() > 0) {
                                msgStr = request.getAttribute("errormsg").toString();
                            }
                            if (request.getAttribute("activeTab") != null && request.getAttribute("activeTab").toString().length() > 0) {
                                activeTab = Integer.parseInt(request.getAttribute("activeTab").toString());
                            }
                            switch (activeTab) {
                                case 0:
                                    classForTab0 = "active";
                                    break;
                                case 1:
                                    classForTab1 = "active";
                                    break;
                                case 2:
                                    classForTab2 = "active";
                                    break;
                            }
                        %>
                    </div>
                </div>
            </div>
            <div class="page-wrapper-row full-height">
                <div class="page-wrapper-middle">
                    <div class="page-container">
                        <div class="page-content-wrapper">
                            <div class="page-content">
                                <div class="container">
                                    <ul class="page-breadcrumb breadcrumb">
                                        <li>
                                            <a href="welcome.do">Admin Panel</a>
                                            <span style="font-size: 14px; margin: 0 auto;" class="fa fa-angle-right"></span>
                                        </li>
                                        <li>
                                            <a href="javascript:;"> Live / Channel </a>
                                            <span style="font-size: 14px; margin: 0 auto;" class="fa fa-angle-right"></span>
                                        </li>
                                        <li>
                                            <a href="channelList.do"> Channel </a>
                                            <span style="font-size: 14px; margin: 0 auto;" class="fa fa-angle-right"></span>
                                        </li>
                                        <li>
                                            <span id="features" data-class-active=".live-o-channel .channel">Edit Channel </span>
                                        </li>
                                    </ul>
                                    <%if (msgStr.length() > 0) {%>
                                    <div id="prefix_661213402591" class="custom-alerts alert alert-success fade in">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                        <strong> <%=msgStr%> </strong>
                                    </div>
                                    <% }%>
                                    <div class="page-content-inner">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="tabbable-line boxless tabbable-reversed">
                                                    <ul class="nav nav-tabs">
                                                        <li class="<%=classForTab0%>">
                                                            <a href="#tab_0" data-toggle="tab"> Booster </a>
                                                        </li>
                                                        <li class="<%=classForTab1%>">
                                                            <a href="#tab_1" data-toggle="tab"> Change Owner </a>
                                                        </li>
                                                        <li class="<%=classForTab2%>">
                                                            <a href="#tab_2" data-toggle="tab"> IP and Port </a>
                                                        </li>
                                                    </ul>
                                                    <div class="tab-content">
                                                        <div class="tab-pane <%=classForTab0%>" id="tab_0">
                                                            <div class="portlet box green">
                                                                <div class="portlet-title">
                                                                    <div class="caption">
                                                                        <i class="fa fa-edit"></i>Edit Channel Booster </div>
                                                                    <div class="tools">
                                                                        <a href="javascript:;" class="collapse"> </a>
                                                                    </div>
                                                                </div>
                                                                <div class="portlet-body form">
                                                                    <!-- BEGIN FORM-->
                                                                    <html:form action="/updateChannelBoster" method="POST" acceptCharset="UTF-8" styleClass="form-horizontal">
                                                                        <div class="form-body">
                                                                            <div class="form-group">
                                                                                <label class="col-md-3 control-label">Channel</label>
                                                                                <div class="col-md-4">
                                                                                    <html:text property="name" title="channel" styleClass="form-control" disabled="true" readonly="true" />
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label class="col-md-3 control-label">Weight</label>
                                                                                <div class="col-md-4">
                                                                                    <html:hidden property="oldValueOfChannelBoster"/>
                                                                                    <html:text property="channelBoster" title="channel boster" styleClass="form-control" />
                                                                                    <html:messages id="channelBoster" property="channelBoster">
                                                                                        <bean:write name="channelBoster"  filter="false"/>
                                                                                    </html:messages>
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label class="col-md-3 control-label">Is Sharable</label>
                                                                                <div class="col-md-4">
                                                                                    <html:hidden property="oldValueOfChannelSharable"/>
                                                                                    <div class="input-group">
                                                                                        <div class="icheck-inline">
                                                                                            <label><html:checkbox property="channelSharable" styleClass="icheck" /></label>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-actions">
                                                                            <div class="row">
                                                                                <div class="col-md-offset-3 col-md-9">
                                                                                    <button type="submit" class="btn green">Submit</button>
                                                                                    <a href="channelList.do?channelType=<bean:write name='ChannelForm' property='channelType' />" class="btn default">Cancel</a>
                                                                                    <html:hidden property="channelId"/>
                                                                                    <html:hidden property="channelType"/>
                                                                                    <html:hidden property="name"/>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </html:form>
                                                                    <!-- END FORM-->
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="tab-pane <%=classForTab1%>" id="tab_1">
                                                            <div class="portlet box green">
                                                                <div class="portlet-title">
                                                                    <div class="caption">
                                                                        <i class="fa fa-edit"></i>Change Channel Owner </div>
                                                                    <div class="tools">
                                                                        <a href="javascript:;" class="collapse"> </a>
                                                                    </div>
                                                                </div>
                                                                <div class="portlet-body form">
                                                                    <!-- BEGIN FORM-->
                                                                    <html:form action="/changeChannelOwner" method="POST" acceptCharset="UTF-8" styleClass="form-horizontal">
                                                                        <div class="form-body">
                                                                            <div class="form-group">
                                                                                <label class="col-md-3 control-label">Channel</label>
                                                                                <div class="col-md-4">
                                                                                    <html:text property="name" title="channel" styleClass="form-control" disabled="true" readonly="true" />
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label class="col-md-3 control-label">Old RingId</label>
                                                                                <div class="col-md-4">
                                                                                    <html:hidden property="oldUserId"/>
                                                                                    <html:text property="userId" title="old ringId" styleClass="form-control" disabled="true" readonly="true" />
                                                                                    <html:messages id="userId" property="userId">
                                                                                        <bean:write name="userId"  filter="false"/>
                                                                                    </html:messages>
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label class="col-md-3 control-label">New RingId</label>
                                                                                <div class="col-md-4">
                                                                                    <html:text property="newUserId" title="new ringId" styleClass="form-control" />
                                                                                    <html:messages id="newUserId" property="newUserId">
                                                                                        <bean:write name="newUserId"  filter="false"/>
                                                                                    </html:messages>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-actions">
                                                                            <div class="row">
                                                                                <div class="col-md-offset-3 col-md-9">
                                                                                    <button type="submit" class="btn green">Submit</button>
                                                                                    <a href="channelList.do?channelType=<bean:write name='ChannelForm' property='channelType' />" class="btn default">Cancel</a>
                                                                                    <html:hidden property="channelId"/>
                                                                                    <html:hidden property="channelType"/>
                                                                                    <html:hidden property="name"/>
                                                                                    <html:hidden property="oldValueOfChannelSharable"/>
                                                                                    <html:hidden property="oldValueOfChannelBoster"/>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </html:form>
                                                                    <!-- END FORM-->
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="tab-pane <%=classForTab2%>" id="tab_2">
                                                            <div class="portlet box green">
                                                                <div class="portlet-title">
                                                                    <div class="caption">
                                                                        <i class="fa fa-edit"></i>Change IP and Port </div>
                                                                    <div class="tools">
                                                                        <a href="javascript:;" class="collapse"> </a>
                                                                    </div>
                                                                </div>
                                                                <div class="portlet-body form">
                                                                    <!-- BEGIN FORM-->
                                                                    <html:form action="/updateIpAndPort" method="POST" acceptCharset="UTF-8" styleClass="form-horizontal">
                                                                        <div class="form-body">
                                                                            <div class="form-group">
                                                                                <label class="col-md-3 control-label">Channel</label>
                                                                                <div class="col-md-4">
                                                                                    <html:text property="name" title="channel" styleClass="form-control" disabled="true" readonly="true" />
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label class="col-md-3 control-label">Old ChannelServer IP/Port</label>
                                                                                <div class="col-md-4">
                                                                                    <html:hidden property="channelIP"/>
                                                                                    <html:hidden property="channelPort"/>
                                                                                    <label class="control-label"><bean:write name="ChannelForm" property="channelIP" /></label>
                                                                                    &nbsp;
                                                                                    <label class="control-label">( <bean:write name="ChannelForm" property="channelPort" /> )</label>
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label class="col-md-3 control-label">New ChannelServer IP/Port <span style="color: red">*</span></label>
                                                                                <div class="col-md-4">
                                                                                    <html:select styleId="channelipport" property="channelIPandPort" styleClass="form-control"> 
                                                                                        <html:option  value=""> Select IP/Port </html:option>     
                                                                                        <%
                                                                                            List<ChannelServerDTO> channelServerDTOs = ChannelServerTaskScheduler.getInstance().getActiveChannelServerList();
                                                                                            for (ChannelServerDTO dto : channelServerDTOs) {
                                                                                        %>
                                                                                        <html:option value="<%=String.valueOf(dto.getServerIP() + ',' + dto.getRegisterPort())%>"><%=dto.getServerIP() + " ( " + dto.getRegisterPort() + " )"%></html:option>
                                                                                        <% }%>  
                                                                                    </html:select>
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label class="col-md-3 control-label">Old StreamingServer IP/Port</label>
                                                                                <div class="col-md-4">
                                                                                    <html:hidden property="streamingServerIP"/>
                                                                                    <html:hidden property="streamingServerPort"/>
                                                                                    <label class="control-label"><bean:write name="ChannelForm" property="streamingServerIP" /></label>
                                                                                    &nbsp;
                                                                                    <label class="control-label">( <bean:write name="ChannelForm" property="streamingServerPort" /> )</label>
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label class="col-md-3 control-label">New StreamingServer IP/Port<span style="color: red">*</span></label>
                                                                                <div class="col-md-4">
                                                                                    <html:select property="streamingServerIPandPort" styleClass="form-control"> 
                                                                                        <html:option  value=""> Select IP/Port </html:option>     
                                                                                        <%
                                                                                            List<StreamingServerDTO> streamingServerDTOs = StreamingServerTaskScheduler.getInstance().getActiveChannelStreamingServerServerList();
                                                                                            for (StreamingServerDTO dto : streamingServerDTOs) {
                                                                                        %>
                                                                                        <html:option value="<%=String.valueOf(dto.getServerIP() + ',' + dto.getRegisterPort())%>"><%=dto.getServerIP() + " ( " + dto.getRegisterPort() + " )"%></html:option>
                                                                                        <% }%>  
                                                                                    </html:select>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-actions">
                                                                            <div class="row">
                                                                                <div class="col-md-offset-3 col-md-9">
                                                                                    <button name="submitType" type="submit" class="btn green" value="submit">Submit</button>
                                                                                    <c:if test="${ChannelForm.getChannelTypeStream() == 1 || ChannelForm.getChannelTypeStream() == 2 || ChannelForm.getChannelTypeStream() == 3}">
                                                                                        &nbsp;<button name="submitType" type="submit" class="btn green" value="unregister">Unregister</button>
                                                                                        <logic:equal name="ChannelForm" property="activeRegisterBtn" value="true">
                                                                                            &nbsp;<button name="submitType" type="submit" class="btn green" value="register">Register</button>
                                                                                        </logic:equal>
                                                                                    </c:if>
                                                                                    &nbsp;<a href="channelList.do?channelType=<bean:write name='ChannelForm' property='channelType' />" class="btn default">Cancel</a>
                                                                                    <html:hidden property="name"/>
                                                                                    <html:hidden property="channelId"/>
                                                                                    <html:hidden property="channelType"/>
                                                                                    <html:hidden property="oldValueOfChannelSharable"/>
                                                                                    <html:hidden property="oldValueOfChannelBoster"/>
                                                                                    <html:hidden property="oldUserId"/>
                                                                                    <html:hidden property="userId"/>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </html:form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <%@include file="/shared/footer.jsp"%>
        </div>
        <%@include file="/shared/script-list.jsp"%>
    </body>
</html>