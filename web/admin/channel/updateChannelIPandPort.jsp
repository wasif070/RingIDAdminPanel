<%-- 
    Document   : updateChannelIPandPort
    Created on : Sep 17, 2017, 8:06:30 PM
    Author     : mamun
--%>

<%@page import="com.ringid.admin.actions.servers.streamingservers.StreamingServerTaskScheduler"%>
<%@page import="com.ringid.admin.actions.servers.streamingservers.StreamingServerDTO"%>
<%@page import="com.ringid.admin.actions.servers.streamingservers.StreamingServerDTO"%>
<%@page import="com.ringid.admin.actions.servers.channelservers.ChannelServerTaskScheduler"%>
<%@page import="com.ringid.admin.actions.servers.channelservers.ChannelServerDTO"%>
<%@page import="java.util.List"%>
<%@include file="/login/loginTimeExpiry.jsp"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@include  file="/shared/head.jsp" %>
<!DOCTYPE html>
<html>
    <head>
        <title>Admin Panel | Update Channel IP and Port </title>
        <%@include file="/shared/css-list.jsp"%>
    </head>
    <body class="page-container-bg-solid">
        <div class="page-wrapper">
            <div class="page-wrapper-row">
                <div class="page-wrapper-top">
                    <div class="page-header">
                        <%@include  file="/shared/menu.jsp" %>
                    </div>
                </div>
            </div>
            <div class="page-wrapper-row full-height">
                <div class="page-wrapper-middle">
                    <div class="page-container">
                        <div class="page-content-wrapper">
                            <div class="page-content">
                                <div class="container">
                                    <ul class="page-breadcrumb breadcrumb">
                                        <li>
                                            <a href="welcome.do">Admin Panel</a>
                                            <span style="font-size: 14px; margin: 0 auto;" class="fa fa-angle-right"></span>
                                        </li>
                                        <li>
                                            <a href="javascript:;"> Live / Channel </a>
                                            <span style="font-size: 14px; margin: 0 auto;" class="fa fa-angle-right"></span>
                                        </li>
                                        <li>
                                            <a href="channelList.do"> Channel </a>
                                            <span style="font-size: 14px; margin: 0 auto;" class="fa fa-angle-right"></span>
                                        </li>
                                        <li>
                                            <span id="features" data-class-active=".live-o-channel .channel">Edit Channel IP and Port </span>
                                        </li>
                                    </ul>
                                    <logic:messagesPresent message="false">
                                        <html:messages id="errormsg" message="false">
                                            <logic:present name="errormsg">
                                                <div id="prefix_661213402591"
                                                     class="custom-alerts alert alert-success fade in">
                                                    <button type="button" class="close" data-dismiss="alert"
                                                            aria-hidden="true"></button>
                                                    <strong> <bean:write name="errormsg" filter="false" /> </strong>
                                                </div>
                                            </logic:present>
                                        </html:messages>
                                    </logic:messagesPresent>
                                    <div class="page-content-inner">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="tabbable-line boxless tabbable-reversed">
                                                    <div class="portlet box green">
                                                        <div class="portlet-title">
                                                            <div class="caption">
                                                                Edit Channel IP and Port
                                                            </div>
                                                        </div>
                                                        <div class="portlet-body form">
                                                            <html:form action="/updateChannelIPandPort" method="POST" acceptCharset="UTF-8" styleClass="form-horizontal" >
                                                                <div class="form-body">
                                                                    <div class="form-group">
                                                                        <label class="col-md-3 control-label"> Old ChannelServerIPandPort </label>
                                                                        <div class="col-md-4">
                                                                            <html:hidden property="channelIP"/>
                                                                            <html:hidden property="channelPort"/>
                                                                            <label class="control-label"> <bean:write name="ChannelForm" property="channelIP" /> </label>
                                                                            &nbsp;<label class="control-label">( <bean:write name="ChannelForm" property="channelPort" /> )</label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <%
                                                                            List<ChannelServerDTO> channelServerDTOs = ChannelServerTaskScheduler.getInstance().getActiveChannelServerList();
                                                                        %>
                                                                        <label class="col-md-3 control-label">New ChannelServerIPandPort <span style="color: red">*</span></label>
                                                                        <div class="col-md-4">
                                                                            <html:select property="channelIPandPort" styleClass="form-control"> 
                                                                                <html:option  value=""> Select ChannelIPandPort </html:option>     
                                                                                <%
                                                                                    for (ChannelServerDTO dto : channelServerDTOs) {
                                                                                %>
                                                                                <html:option value="<%=String.valueOf(dto.getServerIP() + ',' + dto.getRegisterPort())%>"><%=dto.getServerIP() + " ( " + dto.getRegisterPort() + " )"%></html:option>
                                                                                <% }%>  
                                                                            </html:select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label class="col-md-3 control-label"> Old StreamingServerIPandPort </label>
                                                                        <div class="col-md-4">
                                                                            <html:hidden property="streamingServerIP"/>
                                                                            <html:hidden property="streamingServerPort"/>
                                                                            <label class="control-label"> <bean:write name="ChannelForm" property="streamingServerIP" /> </label>
                                                                            &nbsp;<label class="control-label">( <bean:write name="ChannelForm" property="streamingServerPort" /> )</label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <%
                                                                            List<StreamingServerDTO> streamingServerDTOs = StreamingServerTaskScheduler.getInstance().getActiveChannelStreamingServerServerList();
                                                                        %>
                                                                        <label class="col-md-3 control-label">New StreamingServerIPandPort <span style="color: red">*</span></label>
                                                                        <div class="col-md-4">
                                                                            <html:select property="streamingServerIPandPort" styleClass="form-control"> 
                                                                                <html:option  value=""> Select ChannelIPandPort </html:option>     
                                                                                <%
                                                                                    for (StreamingServerDTO dto : streamingServerDTOs) {
                                                                                %>
                                                                                <html:option value="<%=String.valueOf(dto.getServerIP() + ',' + dto.getRegisterPort())%>"><%=dto.getServerIP() + " ( " + dto.getRegisterPort() + " )"%></html:option>
                                                                                <% }%>  
                                                                            </html:select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="form-actions">
                                                                    <div class="row">
                                                                        <div class="col-md-offset-3 col-md-9">
                                                                            <button type="submit" class="btn green" name="saveButton">Submit</button>
                                                                            <a href="channelList.do" class="btn default">Cancel</a>
                                                                            <html:hidden property="id"/>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </html:form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="page-wrapper-row">
                <div class="page-wrapper-bottom">
                    <div class="page-footer">
                        <div class="container"> Copyright &copy; 2015 All right reserved by
                            <a target="_blank" href="http://www.ipvision.ca/">IPvision canada inc</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <%@include file="/shared/script-list.jsp"%>
        <script type="text/javascript">
            $(document).ready(function () {
                $('#pageableDataTables').dataTable({
                    "lengthMenu": [
                        [25, 50, 100, -1],
                        [25, 50, 100, "All"]
                    ],
                    "pageLength": 25,
                    "pagingType": "bootstrap_full_number",
                    "columnDefs": [
                        {
                            'orderable': false,
                            'targets': [0]
                        },
                        {
                            "searchable": false,
                            "targets": [0]
                        },
                        {
                            "className": "dt-right"
                        }
                    ],
                    "scrollY": 550,
                    "scrollCollapse": true,
                    "sScrollX": "100%",
                    "sScrollXInner": "100%",
                    "bScrollCollapse": true,
                    "order": [
                        [1, "asc"]
                    ]
                });

                $(".table-scrollable").css('border-style', 'none');
            });

            function checkAll(objCheckAll) {
                var checkboxes = document.getElementsByName('ids'), Checked = objCheckAll.checked, Length, i;
                for (i = 0, Length = checkboxes.length; i < Length; i++) {
                    checkboxes[i].checked = Checked;
                }
            }

            function submitform(page, action, iobj)
            {
                document.forms[0].submit();
            }

            function submitList() {
                var count = 0;
                for (var i = 0, max = document.getElementsByClassName('checkboxes').length; i < max; i++) {
                    if (document.getElementsByClassName('checkboxes')[i].checked === true) {
                        count++;
                    }
                }
                console.log(count);
                if (count === 0) {
                    alert("No item selected");
                    return false;
                }
            }
        </script>
    </body>
</html>
