<%-- 
    Document   : channelCategoryTable
    Created on : Aug 30, 2017, 11:26:47 AM
    Author     : mamun
--%>

<%@page import="com.ringid.admin.utils.Constants"%>
<div class="form-body">
    <div class="form-group">
        <label class="col-md-3 control-label"> 
            Category
            <span class="required" aria-required="true"> * </span>
        </label>
        <div class="col-md-4">
            <html:text property="category" title="category" styleClass="form-control" />
            <html:messages id="category" property="category">
                <span style="color: red;">Category Required</span>
            </html:messages>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label"> Description </label>
        <div class="col-md-4">
            <html:text property="description" title="description" styleClass="form-control" />
            <html:messages id="description" property="description">
                <span style="color: red;">Description Required</span>
            </html:messages>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label"> Vertical Weight </label>
        <div class="col-md-4">
            <html:text property="verticalWeight" title="Vertical Weight" styleClass="form-control" />
            <html:messages id="verticalWeight" property="verticalWeight">
                <bean:write name="verticalWeight"  filter="false"/>
            </html:messages>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label"> Horizontal Weight </label>
        <div class="col-md-4">
            <html:text property="horizontalWeight" title="Horizontal Weight" styleClass="form-control" />
            <html:messages id="horizontalWeight" property="horizontalWeight">
                <bean:write name="horizontalWeight"  filter="false"/>
            </html:messages>
        </div>
    </div>
    <div class="form-group ">
        <label class="control-label col-md-3">
            Banner Image
            <span class="required" aria-required="true"> * </span>
        </label>
        <div class="col-md-9">
            <div class="fileinput fileinput-new" data-provides="fileinput">
                <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px; line-height: 150px;">
                    <img src="<%=Constants.IMAGE_BASE_URL%>${ChannelCategoryForm.banner}" />
                </div>
                <div>
                    <span class="btn green btn-outline btn-file">
                        <span class="fileinput-new"> Select image </span>
                        <span class="fileinput-exists"> Change image </span>
                        <html:file property="theFile" onchange="removeSpanText()" />
                    </span>
                    <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                    <span id="msgSpan" style="display: inline-block;">
                        <html:messages id="theFile" property="theFile">
                            <span style="color: red;">Banner Image Required</span>
                        </html:messages>
                    </span>
                </div>
            </div>
        </div>
    </div>
    <div class="form-group last">
        <label class="col-md-3 control-label"></label>
        <div class="col-md-4">
            <%
                if (request.getSession(true).getAttribute("error") == "2") {
            %>
            <html:messages id="message" property="message" ><bean:write name="msg"  filter="false"/></html:messages>
            <bean:write name="ChannelCategoryForm" property="message" filter="false"/>
            <%}%>
        </div>
    </div>
</div>
<div class="form-actions">
    <div class="row">
        <div class="col-md-offset-3 col-md-4">
            <button type="submit" class="btn green" name="saveButton">Submit</button>
            <a href="showChannelCategory.do" class="btn default">Cancel</a>
            <html:hidden property="categoryId"/>
            <html:hidden property="banner"/>
            <input type="hidden" name="action" value="<%=request.getSession(true).getAttribute("formType")%>" />
        </div>
    </div>
</div>
<script type="text/javascript">
    function removeSpanText() {
        document.getElementById("msgSpan").innerHTML = "";
    }
</script>
