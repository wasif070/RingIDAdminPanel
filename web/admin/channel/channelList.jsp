<%-- 
    Document   : m_breakingNewsList
    Created on : Feb 15, 2017, 3:11:57 PM
    Author     : MAMUN
--%>

<%@page import="com.ringid.admin.dto.ChannelVerificationDTO"%>
<%@page import="com.ringid.admin.projectMenu.MenuNames"%>
<%@include file="/login/loginTimeExpiry.jsp"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="java.util.*,com.ringid.admin.utils.Constants"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@include  file="/shared/head.jsp" %>

<!DOCTYPE html>
<html>
    <head>
        <title>Admin Panel | Channel</title>
        <%@include file="/shared/css-list.jsp"%>
    </head>
    <body class="page-container-bg-solid">
        <div class="page-wrapper">
            <div class="page-wrapper-row">
                <div class="page-wrapper-top">
                    <div class="page-header">
                        <%@include  file="/shared/menu.jsp" %>
                        <%
                            String FEATURE_NAME = MenuNames.CHANNEL;
                            String msgStr = "", channelType = "";
                            if (request.getAttribute("errormsg") != null && request.getAttribute("errormsg").toString().length() > 0) {
                                msgStr = request.getAttribute("errormsg").toString();
                            }
                            if (request.getAttribute("channelType") != null && request.getAttribute("channelType").toString().length() > 0) {
                                channelType = request.getAttribute("channelType").toString();
                            }
                            int TOTAL_RECORDS = 0, TOTAL_PAGES = 0, CURRENT_PAGE_NO = 0, STARTING_RECORD_NO = 0, ENDING_RECORD_NO = 0;
                            String disabledPrev = "", disabledNext = "";
                            if (request.getAttribute(Constants.TOTAL_RECORDS) != null) {
                                TOTAL_RECORDS = Integer.valueOf(request.getAttribute(Constants.TOTAL_RECORDS).toString());
                            }
                            if (request.getAttribute(Constants.TOTAL_PAGES) != null) {
                                TOTAL_PAGES = Integer.valueOf(request.getAttribute(Constants.TOTAL_PAGES).toString());
                            }
                            if (request.getAttribute(Constants.CURRENT_PAGE_NO) != null) {
                                CURRENT_PAGE_NO = Integer.valueOf(request.getAttribute(Constants.CURRENT_PAGE_NO).toString());
                            }
                            if (request.getAttribute(Constants.STARTING_RECORD_NO) != null) {
                                STARTING_RECORD_NO = Integer.valueOf(request.getAttribute(Constants.STARTING_RECORD_NO).toString());
                            }
                            if (request.getAttribute(Constants.ENDING_RECORD_NO) != null) {
                                ENDING_RECORD_NO = Integer.valueOf(request.getAttribute(Constants.ENDING_RECORD_NO).toString());
                            }
                            if (STARTING_RECORD_NO <= 0) {
                                disabledPrev = "disabled";
                                STARTING_RECORD_NO = 0;
                            }
                            if (ENDING_RECORD_NO >= TOTAL_RECORDS) {
                                disabledNext = "disabled";
                                ENDING_RECORD_NO = TOTAL_RECORDS;
                            }
                        %>
                    </div>
                </div>
            </div>
            <div class="page-wrapper-row full-height">
                <div class="page-wrapper-middle">
                    <div class="page-container">
                        <div class="page-content-wrapper">
                            <div class="page-content">
                                <div class="container">
                                    <ul class="page-breadcrumb breadcrumb">
                                        <li>
                                            <a href="welcome.do">Admin Panel</a>
                                            <span style="font-size: 14px; margin: 0 auto;" class="fa fa-angle-right"></span>
                                        </li>
                                        <li>
                                            <a href="javascript:;"> Live / Channel </a>
                                            <span style="font-size: 14px; margin: 0 auto;" class="fa fa-angle-right"></span>
                                        </li>
                                        <li>
                                            <span id="features" data-class-active=".live-o-channel .channel">Channel</span>
                                        </li>
                                    </ul>
                                    <%if (msgStr.length() > 0) {%>
                                    <div id="prefix_661213402591" class="custom-alerts alert alert-success fade in">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                        <strong> <%=msgStr%> </strong>
                                    </div>
                                    <% }%>
                                    <div class="page-content-inner">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="tabbable-line boxless tabbable-reversed">
                                                    <div class="tab-content" style="padding-top: 15px;">
                                                        <div class="portlet box green">
                                                            <div class="portlet-title">
                                                                <div class="caption">
                                                                    <i class="fa fa-list-ul"></i> Channel List Info
                                                                </div>
                                                                <div class="tools">
                                                                    <a href="javascript:;" class="collapse"> </a>

                                                                    <a data-original-title="Reload" href="channelList.do?ForceReload=1" style="width: 13px; background-image: url(assets/global/img/portlet-reload-icon-white.png);" class="tooltips"> </a>
                                                                </div>
                                                            </div>
                                                            <div class="portlet-body form">
                                                                <html:form action="/channelList" styleClass="form-horizontal" method="POST"  acceptCharset="UTF-8">
                                                                    <div class="form-body" >
                                                                        <div class="form-group">
                                                                            <div class="col-md-12 col-sm-12">
                                                                                <label>
                                                                                    <html:select styleClass="form-control input-inline" property="channelType" onchange="submitform(1,1,this)">
                                                                                        <html:option value="1">Primary Unverified Channel</html:option>
                                                                                        <html:option value="2">Primary Verified Channel</html:option>
                                                                                        <html:option value="3">Featured Channel</html:option>
                                                                                        <html:option value="4">Officially Verified Channel</html:option>
                                                                                    </html:select>
                                                                                </label>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <div class="col-md-6 col-sm-6">
                                                                                <label>
                                                                                    <html:select styleClass="bs-select form-control input-sm input-xsmall input-inline" property="recordPerPage" onchange="javascript: submitform(1, 1, this);">
                                                                                        <html:option value="10">10</html:option>
                                                                                        <html:option value="25">25</html:option>
                                                                                        <html:option value="50">50</html:option>
                                                                                        <html:option value="100">100</html:option>
                                                                                        <html:option value="200">200</html:option>
                                                                                    </html:select>
                                                                                    records</label>
                                                                            </div>
                                                                            <div class="col-md-6 col-sm-6">
                                                                                <div class="input-group" style="float: right;">
                                                                                    <input name="searchText" type="text" value="<bean:write name="ChannelForm" property="searchText"/>" class="form-control input-sm input-small" placeholder="Search">
                                                                                    <span>
                                                                                        <button class="btn green-soft uppercase bold input-sm" type="submit"><i class="icon-magnifier"></i></button>
                                                                                        <input name="action" type="hidden" value="<%=Constants.SEARCH%>" />
                                                                                        <html:hidden property="column" />
                                                                                        <html:hidden property="sort" />
                                                                                    </span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="table-scrollable">
                                                                            <table class="table table-striped table-bordered table-hover">
                                                                                <thead>
                                                                                    <tr>
                                                                                        <th class="text-center align-middle">
                                                                                            <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                                                                                <input type="checkbox" onchange="checkAll(this)" class="group-checkable" />
                                                                                                <span></span>
                                                                                            </label>
                                                                                        </th>
                                                                                        <th class="text-center align-middle">ChannelID</th>
                                                                                        <th class="text-center align-middle">Profile Image</th>
                                                                                        <th id="col_1" class="text-center align-middle sortable">Title</th>
                                                                                        <th id="col_2" class="text-center align-middle sortable">User Identity</th>
                                                                                        <th id="col_3" class="text-center align-middle sortable">Weight</th>
                                                                                        <th class="text-center align-middle">IsSharable</th>
                                                                                        <th class="text-center align-middle">Type</th>
                                                                                        <th class="text-center align-middle">Status</th>
                                                                                            <%if ((loginDTO.getPermissionLevel() == Constants.SUPER_ADMIN) || (loginDTO.getFeauterMapList().get(FEATURE_NAME).getHasModifyPermission())) {%>
                                                                                        <th class="text-center align-middle">Action</th>
                                                                                            <%}%>
                                                                                    </tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                    <%
                                                                                        ArrayList<ChannelVerificationDTO> data1 = (ArrayList<ChannelVerificationDTO>) session.getAttribute(Constants.DATA_ROWS);
                                                                                        session.removeAttribute(Constants.DATA_ROWS);
                                                                                        if (data1 != null && data1.size() > 0) {
                                                                                            for (int i = STARTING_RECORD_NO; i < ENDING_RECORD_NO; i++) {
                                                                                                ChannelVerificationDTO dto = (ChannelVerificationDTO) data1.get(i);
                                                                                    %>
                                                                                    <tr>
                                                                                        <td class="text-center align-middle">
                                                                                            <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                                                                                <input name="selectedIDs" type="checkbox" class="checkboxes" value="<%=String.valueOf(dto.getChannelId())%>" />
                                                                                                <span></span>
                                                                                            </label>
                                                                                        </td>
                                                                                        <td class="text-center align-middle"> <%=dto.getChannelId()%></td>
                                                                                        <td class="text-center align-middle"><img style="height: 100px; width: 100px" src="<%=Constants.MEDIA_CLOUD_BASE_URL%><%=dto.getProfileImageUrl()%>"></img></td>
                                                                                        <td class="text-center align-middle"><a href="channelMediaList.do?channelId=<%=dto.getChannelId()%>&channelType=<%=channelType%>"> <%=dto.getTitle()%></a></td>
                                                                                        <td class="text-center align-middle"> <%=dto.getUserIdentity()%></td>
                                                                                        <td class="text-center align-middle"> <%=dto.getChannelBooster()%></td>
                                                                                        <td class="text-center align-middle"> <%=dto.isChannelSharable()%></td>
                                                                                        <td class="text-center align-middle"> <%=dto.getChannelTypeStr()%></td>
                                                                                        <td class="text-center align-middle"> <%=dto.getChannelStatusStr()%></td>
                                                                                        <%if ((loginDTO.getPermissionLevel() == Constants.SUPER_ADMIN) || (loginDTO.getFeauterMapList().get(FEATURE_NAME).getHasModifyPermission())) {%>
                                                                                        <td class="text-center align-middle">
                                                                                            <a href="editChannel.do?channelId=<%=dto.getChannelId()%>&channelTypeStream=<%=dto.getChannelType()%>&action=<%=Constants.EDIT%>&channelType=<bean:write name='ChannelForm' property='channelType' />" class="" title="Change">
                                                                                                <span class="fa fa-edit"></span>
                                                                                            </a>
                                                                                        </td>
                                                                                        <%}%>
                                                                                    </tr>
                                                                                    <%
                                                                                            }
                                                                                        }
                                                                                    %>
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                        <div class="row">
                                                                            <div class="col-md-5 col-sm-12">
                                                                                <div>
                                                                                    Showing <%=(STARTING_RECORD_NO + 1)%> to <%=ENDING_RECORD_NO%> of <%=TOTAL_RECORDS%> entries
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-7 col-sm-12">
                                                                                <div style="float: right;">
                                                                                    <div> 
                                                                                        Page <a href="javascript: submitform(2, <%=(CURRENT_PAGE_NO - 1)%>, this);" class="btn btn-sm default prev <%=disabledPrev%>"><i class="fa fa-angle-left"></i></a>
                                                                                        <input name="pageNo" class="form-control input-sm input-inline input-mini" maxlenght="5" style="text-align:center; margin: 0 5px;" type="text" value="<%=CURRENT_PAGE_NO%>" onchange="javascript: submitform(2, 0, this);">
                                                                                        <a href="javascript: submitform(2, <%=(CURRENT_PAGE_NO + 1)%>, this);" class="btn btn-sm default next <%=disabledNext%>"><i class="fa fa-angle-right"></i></a> of <span><%=TOTAL_PAGES%></span>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-actions">
                                                                        <div class="row">
                                                                            <div class="col-md-offset-3 col-md-9">
                                                                                <%if (channelType.equals("1")) {%>
                                                                                <html:submit styleClass="btn green" property="submitType" value="Primary Verify" />
                                                                                <%if ((loginDTO.getPermissionLevel() == Constants.SUPER_ADMIN) || (loginDTO.getFeauterMapList().get(FEATURE_NAME).getHasDeletePermission())) {%>
                                                                                <html:submit styleClass="btn green" property="submitType" value="Delete Channel" />
                                                                                <%}%>
                                                                                <% } else if (channelType.equals("2")) { %>
                                                                                <html:submit styleClass="btn green" property="submitType" value="Primary Unverify" />
                                                                                <html:submit styleClass="btn green" property="submitType" value="Officially Verify" />
                                                                                <html:submit styleClass="btn green" property="submitType" value="Make Featured" />
                                                                                <% } else if (channelType.equals("3")) { %>
                                                                                <html:submit styleClass="btn green" property="submitType" value="Primary Unverify" />
                                                                                <html:submit styleClass="btn green" property="submitType" value="Officially Verify" />
                                                                                <% } else if (channelType.equals("4")) { %>
                                                                                <html:submit styleClass="btn green" property="submitType" value="Officially Unverify" />
                                                                                <% }%>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </html:form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div><%@include file="/shared/footer.jsp"%>
        </div>
        <%@include file="/shared/script-list.jsp"%>
    </body>
</html>
