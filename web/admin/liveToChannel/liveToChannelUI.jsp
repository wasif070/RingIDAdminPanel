<%-- 
    Document   : liveToChannelUI
    Created on : Mar 3, 2018, 4:30:49 PM
    Author     : mamun
--%>

<%@include file="/login/loginTimeExpiry.jsp"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.ringid.admin.utils.Constants"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@include  file="/shared/head.jsp" %>

<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Admin Panel | Add App Constants</title>
        <%@include file="/shared/css-list.jsp"%>
    </head>
    <body class="page-container-bg-solid">
        <div class="page-wrapper">
            <div class="page-wrapper-row">
                <div class="page-wrapper-top">
                    <div class="page-header">
                        <%@include  file="/shared/menu.jsp"%>
                    </div>
                </div>
            </div>
            <div class="page-wrapper-row full-height">
                <div class="page-wrapper-middle">
                    <div class="page-container">
                        <div class="page-content-wrapper">
                            <div class="page-content">
                                <div class="container">
                                    <ul class="page-breadcrumb breadcrumb">
                                        <li>
                                            <a href="welcome.do">Admin Panel</a>
                                            <span style="font-size: 14px; margin: 0 auto;" class="fa fa-angle-right"></span>
                                        </li>
                                        <li>
                                            <a href="javascript:;">Common</a>
                                            <i style="font-size: 14px; margin: 0 auto;" class="fa fa-angle-right"></i>
                                        </li>
                                        <li>
                                            <a href="liveToChannelListInfo.do">Live To Channel</a>
                                            <i style="font-size: 14px; margin: 0 auto;" class="fa fa-angle-right"></i>
                                        </li>
                                        <li>
                                            <span id="features" data-class-active=".common .live-to-channel">Live to Channel Info</span>
                                        </li>
                                    </ul>
                                    <div class="page-content-inner">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="tabbable-line boxless tabbable-reversed">
                                                    <div class="tab-content" style="padding-top: 15px;">
                                                        <div class="portlet box green">
                                                            <div class="portlet-title">
                                                                <div class="caption">
                                                                    <i class="fa fa-plus"></i> Live to channel Info </div>
                                                                <div class="tools">
                                                                    <a href="javascript:;" class="collapse"> </a>
                                                                </div>
                                                            </div>
                                                            <div class="portlet-body form">
                                                                <html:form action="addLiveToChannelInfo" styleClass="form-horizontal" method="POST"  acceptCharset="UTF-8">
                                                                    <div class="form-body">
                                                                        <div class="form-group">
                                                                            <label class="col-md-3 control-label">PageId</label>
                                                                            <div class="col-md-4">
                                                                                <html:text property="pageId" title="Page ID" styleClass="form-control" />
                                                                                <html:messages id="pageId" property="pageId">
                                                                                    <bean:write name="pageId"  filter="false"/>
                                                                                </html:messages>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label class="col-md-3 control-label">ChannelId</label>
                                                                            <div class="col-md-4">
                                                                                <html:text property="channelId" title="channelId" styleClass="form-control" />
                                                                                <html:messages id="channelId" property="channelId">
                                                                                    <bean:write name="channelId"  filter="false"/>
                                                                                </html:messages>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label class="col-md-3 control-label">Title</label>
                                                                            <div class="col-md-4">
                                                                                <html:text property="title" title="title" styleClass="form-control" />
                                                                                <html:messages id="title" property="title">
                                                                                    <bean:write name="title"  filter="false"/>
                                                                                </html:messages>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label class="col-md-3 control-label">Description</label>
                                                                            <div class="col-md-4">
                                                                                <html:text property="description" title="description" styleClass="form-control" />
                                                                                <html:messages id="description" property="description">
                                                                                    <bean:write name="description"  filter="false"/>
                                                                                </html:messages>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label class="col-md-3 control-label">Live Time</label>
                                                                            <div class="col-md-4">
                                                                                <input type="text" name="liveTime" placeholder="yyyy-mm-dd HH:mm" class="form-control date form_datetime" title="live time" value="<bean:write name="LiveToChannelForm" property="liveTime" />" />
                                                                                <html:messages id="liveTime" property="liveTime">
                                                                                    <bean:write name="liveTime"  filter="false"/>
                                                                                </html:messages>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label class="col-md-3 control-label">Show</label>
                                                                            <div class="col-md-4">
                                                                                <div class="input-group">
                                                                                    <div class="icheck-inline">
                                                                                        <label>
                                                                                            <html:radio property="liveNow" value="1" styleClass="icheck"/> ON </label>
                                                                                        <label>
                                                                                            <html:radio property="liveNow" value="0" styleClass="icheck"/> OFF </label>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-actions">
                                                                        <div class="row">
                                                                            <div class="col-md-offset-3 col-md-4">
                                                                                <button type="submit" class="btn green" name="saveButton">Submit</button>
                                                                                <a href="liveToChannelListInfo.do" class="btn default">Cancel</a>
                                                                                <html:hidden property="pageId"/>
                                                                                <input type="hidden" name="action" value="3" />
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </html:form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <%@include file="/shared/footer.jsp"%>
            </div>
            <%@include file="/shared/script-list.jsp"%>
            <script type="text/javascript">
                $(".form_datetime").datetimepicker({
                    autoclose: true,
                    format: "yyyy-mm-dd hh:ii"
                });
            </script>
        </div>
    </body>
</html>
