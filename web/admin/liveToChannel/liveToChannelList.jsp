<%-- 
    Document   : liveToChannelList
    Created on : Mar 3, 2018, 4:30:24 PM
    Author     : mamun
--%>

<%@page import="com.ringid.admin.actions.ringId.celebritySchedule.LiveToChannelDTO"%>
<%@page import="com.ringid.admin.projectMenu.MenuNames"%>
<%@include file="/login/loginTimeExpiry.jsp"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.ringid.admin.utils.Constants"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@include  file="/shared/head.jsp" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Admin Panel | App Constants List</title>
        <%@include file="/shared/css-list.jsp"%>
    </head>
    <body class="page-container-bg-solid">
        <div class="page-wrapper">
            <div class="page-wrapper-row">
                <div class="page-wrapper-top">
                    <div class="page-header">
                        <%@include  file="/shared/menu.jsp" %>
                        <%
                            String FEATURE_NAME = MenuNames.LIVE_TO_CHANNEL;
                            int TOTAL_RECORDS = 0, TOTAL_PAGES = 0, CURRENT_PAGE_NO = 0, STARTING_RECORD_NO = 0, ENDING_RECORD_NO = 0;
                            String disabledPrev = "", disabledNext = "";
                            if (request.getAttribute(Constants.TOTAL_RECORDS) != null) {
                                TOTAL_RECORDS = Integer.valueOf(request.getAttribute(Constants.TOTAL_RECORDS).toString());
                            }
                            if (request.getAttribute(Constants.TOTAL_PAGES) != null) {
                                TOTAL_PAGES = Integer.valueOf(request.getAttribute(Constants.TOTAL_PAGES).toString());
                            }
                            if (request.getAttribute(Constants.CURRENT_PAGE_NO) != null) {
                                CURRENT_PAGE_NO = Integer.valueOf(request.getAttribute(Constants.CURRENT_PAGE_NO).toString());
                            }
                            if (request.getAttribute(Constants.STARTING_RECORD_NO) != null) {
                                STARTING_RECORD_NO = Integer.valueOf(request.getAttribute(Constants.STARTING_RECORD_NO).toString());
                            }
                            if (request.getAttribute(Constants.ENDING_RECORD_NO) != null) {
                                ENDING_RECORD_NO = Integer.valueOf(request.getAttribute(Constants.ENDING_RECORD_NO).toString());
                            }
                            if (STARTING_RECORD_NO <= 0) {
                                disabledPrev = "disabled";
                                STARTING_RECORD_NO = 0;
                            }
                            if (ENDING_RECORD_NO >= TOTAL_RECORDS) {
                                disabledNext = "disabled";
                                ENDING_RECORD_NO = TOTAL_RECORDS;
                            }
                        %>
                    </div>
                </div>
            </div>
            <div class="page-wrapper-row full-height">
                <div class="page-wrapper-middle">
                    <div class="page-container">
                        <div class="page-content-wrapper">
                            <div class="page-content">
                                <div class="container">
                                    <ul class="page-breadcrumb breadcrumb">
                                        <li>
                                            <a href="welcome.do">Admin Panel</a>
                                            <span style="font-size: 14px; margin: 0 auto;" class="fa fa-angle-right"></span>
                                        </li>
                                        <li>
                                            <a href="javascript:;">Common</a>
                                            <i style="font-size: 14px; margin: 0 auto;" class="fa fa-angle-right"></i>
                                        </li>
                                        <li>
                                            <a href="liveToChannelListInfo.do">Live To Channel</a>
                                            <i style="font-size: 14px; margin: 0 auto;" class="fa fa-angle-right"></i>
                                        </li>
                                        <li>
                                            <span id="features" data-class-active=".common .live-to-channel">Manage Live To Channel</span>
                                        </li>
                                    </ul>
                                    <div class="page-content-inner">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="tabbable-line boxless tabbable-reversed">
                                                    <div class="tab-content" style="padding-top: 15px;">
                                                        <div class="portlet box green">
                                                            <div class="portlet-title">
                                                                <div class="caption">
                                                                    <i class="fa fa-list-ul"></i> Live to channel List Info </div>
                                                                <div class="tools">
                                                                    <a href="javascript:;" class="collapse"> </a>
                                                                    <a data-original-title="Reload" href="liveToChannelListInfo.do" style="width: 13px; background-image: url(assets/global/img/portlet-reload-icon-white.png);" class="tooltips"> </a>
                                                                    <%if ((loginDTO.getPermissionLevel() == Constants.SUPER_ADMIN) || (loginDTO.getFeauterMapList().get(FEATURE_NAME).getHasAddPermission())) {%>
                                                                    <a data-original-title="AddNew" style="color: white; width: 16px; background-image: url(assets/global/img/add-1.png);" href="admin/liveToChannel/liveToChannelUI.jsp" class="tooltips"> </a>
                                                                    <% }%>
                                                                </div>
                                                            </div>
                                                            <div class="portlet-body form">
                                                                <html:form action="liveToChannelListInfo" styleClass="form-horizontal" method="POST"  acceptCharset="UTF-8">
                                                                    <div class="form-body">
                                                                        <div class="form-group">
                                                                            <div class="col-md-6 col-sm-6">
                                                                                <label>
                                                                                    <html:select styleClass="bs-select form-control input-sm input-xsmall input-inline" property="recordPerPage" onchange="javascript: submitform(1, 1, this);">
                                                                                        <html:option value="10">10</html:option>
                                                                                        <html:option value="25">25</html:option>
                                                                                        <html:option value="50">50</html:option>
                                                                                        <html:option value="100">100</html:option>
                                                                                        <html:option value="200">200</html:option>
                                                                                    </html:select>
                                                                                    records</label>
                                                                            </div>
                                                                        </div>
                                                                        <div class="table-scrollable">
                                                                            <table class="table table-striped table-bordered table-hover">
                                                                                <thead>
                                                                                    <tr>
                                                                                        <th class="text-center">PageId</th>
                                                                                        <th class="text-center">Page Name</th>
                                                                                        <th class="text-center">ChannelId</th>
                                                                                        <th class="text-center">Channel Name</th>
                                                                                        <th class="text-center">Title</th>
                                                                                        <th class="text-center">Description</th>
                                                                                        <th class="text-center">Show</th>
                                                                                        <th class="text-center">Live Time</th>
                                                                                            <%if ((loginDTO.getPermissionLevel() == Constants.SUPER_ADMIN) || (loginDTO.getFeauterMapList().get(FEATURE_NAME).getHasModifyPermission()
                                                                                                        || loginDTO.getFeauterMapList().get(FEATURE_NAME).getHasDeletePermission())) {%>      
                                                                                        <th class="text-center">Action</th>
                                                                                            <% }%> 
                                                                                    </tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                    <%
                                                                                        List<LiveToChannelDTO> data1 = (ArrayList<LiveToChannelDTO>) session.getAttribute(Constants.DATA_ROWS);
                                                                                        session.removeAttribute(Constants.DATA_ROWS);
                                                                                        if (data1 != null && data1.size() > 0) {
                                                                                            //out.print(data.size());
                                                                                            for (int i = STARTING_RECORD_NO; i < ENDING_RECORD_NO; i++) {
                                                                                                LiveToChannelDTO dto = (LiveToChannelDTO) data1.get(i);
                                                                                    %>
                                                                                    <tr>
                                                                                        <td class="text-center"><%=dto.getPageId()%></td>
                                                                                        <td class="text-center"><%=dto.getPageName()%></td>
                                                                                        <td class="text-center"><%=dto.getChannelId()%></td>
                                                                                        <td class="text-center"><%=dto.getChannelName()%></td>
                                                                                        <td class="text-center"><%=dto.getTitle()%></td>
                                                                                        <td class="text-center"><%=dto.getDescription()%></td>
                                                                                        <td class="text-center"><%=dto.getLiveNow()%></td>
                                                                                        <td class="text-center"><%=dto.getLiveTimeStr()%></td>
                                                                                        <%if ((loginDTO.getPermissionLevel() == Constants.SUPER_ADMIN) || (loginDTO.getFeauterMapList().get(FEATURE_NAME).getHasModifyPermission() || loginDTO.getFeauterMapList().get(FEATURE_NAME).getHasDeletePermission())) {%>
                                                                                        <td class="text-center">
                                                                                            <%if ((loginDTO.getPermissionLevel() == Constants.SUPER_ADMIN) || (loginDTO.getFeauterMapList().get(FEATURE_NAME).getHasModifyPermission())) {%>
                                                                                            <a href="resetLiveTime.do?pageId=<%=dto.getPageId() + "&action=" + Constants.RESET%>" class="" title="reset live time"><span class="btn green">Reset Live Time</span></a>
                                                                                            &nbsp;<a href="editLiveToChannelInfo.do?pageId=<%=dto.getPageId() + "&action=" + Constants.EDIT%>" class="" title="Change"><span class="fa fa-edit"></span></a>
                                                                                                <% }%>
                                                                                                <%if ((loginDTO.getPermissionLevel() == Constants.SUPER_ADMIN) || (loginDTO.getFeauterMapList().get(FEATURE_NAME).getHasDeletePermission())) {%>
                                                                                            &nbsp;<a href="deleteLiveToChannelInfo.do?pageId=<%=dto.getPageId() + "&action=" + Constants.DELETE%>" class="" title="Delete" onclick="javascript:return confirm('Are you sure you want to delete this content')"><span class="fa fa-remove"></span></a>
                                                                                                <% } %>
                                                                                        </td>
                                                                                        <% } %>      
                                                                                    </tr>
                                                                                    <%
                                                                                            }
                                                                                        }
                                                                                    %>
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                        <div class="row">
                                                                            <div class="col-md-5 col-sm-12">
                                                                                <div>
                                                                                    Showing <%=(STARTING_RECORD_NO + 1)%> to <%=ENDING_RECORD_NO%> of <%=TOTAL_RECORDS%> entries
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-7 col-sm-12">
                                                                                <div style="float: right;">
                                                                                    <div> 
                                                                                        Page <a href="javascript: submitform(2, <%=(CURRENT_PAGE_NO - 1)%>, this);" class="btn btn-sm default prev <%=disabledPrev%>"><i class="fa fa-angle-left"></i></a>
                                                                                        <input name="pageNo" class="form-control input-sm input-inline input-mini" maxlenght="5" style="text-align:center; margin: 0 5px;" type="text" value="<%=CURRENT_PAGE_NO%>" onchange="javascript: submitform(2, 0, this);">
                                                                                        <a href="javascript: submitform(2, <%=(CURRENT_PAGE_NO + 1)%>, this);" class="btn btn-sm default next <%=disabledNext%>"><i class="fa fa-angle-right"></i></a> of <span><%=TOTAL_PAGES%></span>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </html:form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <%@include file="/shared/footer.jsp"%>
        </div>
        <%@include file="/shared/script-list.jsp"%>
    </body>
</html>

