<%-- 
    Document   : ringIdDistributorTable
    Created on : Dec 9, 2017, 3:02:14 PM
    Author     : Rabby
--%>

<%@page import="com.ringid.admin.actions.servers.authServers.AuthServerLoader"%>
<%@page import="com.ringid.admin.actions.servers.authServers.AuthServerDTO"%>
<%@page import="java.util.ArrayList"%>
<div class="form-body">
    <div class="form-group">
        <label class="col-md-3 control-label">Server<span style="color: red">*</span></label>
        <div class="col-md-4">
            <%
                ArrayList<AuthServerDTO> authServerList = AuthServerLoader.getInstance().getAuthServerList(new AuthServerDTO());
            %>
            <html:select property="serverId" styleClass="form-control">
                <%for (AuthServerDTO dto : authServerList) {%>
                <html:option value="<%=String.valueOf(dto.getServerID())%>"><%=dto.getId()%>-<%=dto.getServerIP()%></html:option>
                    <%}%>
            </html:select>
            <html:messages id="serverIP" property="serverId">
                <bean:write name="serverId"  filter="false"/>
            </html:messages>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label">Start RingID <span style="color: red">*</span></label>
        <div class="col-md-4">
            <html:text property="startRingId" title="Start RingId"  styleClass="form-control" />
            <html:messages id="registerPort" property="startRingId">
                <bean:write name="startRingId"  filter="false"/>
            </html:messages>

        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label">End RingId <span style="color: red">*</span></label>
        <div class="col-md-4">
            <html:text property="endRingId" title="End RingId"  styleClass="form-control" />
            <html:messages id="endRingId" property="endRingId">
                <bean:write name="endRingId"  filter="false"/>
            </html:messages>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label">Status <span style="color: red">*</span></label>
        <div class="col-md-4">
            <html:select property="status" styleClass="form-control">
                <html:option value="0">Inactive</html:option>
                <html:option value="1">Active</html:option>
            </html:select>
            <html:messages id="status" property="status">
                <bean:write name="status"  filter="false"/>
            </html:messages>
        </div>
    </div>
</div>
<div class="form-actions">
    <div class="row">
        <div class="col-md-offset-3 col-md-4">
            <button type="submit" class="btn green" name="saveButton">Submit</button>
            <a href="ringIdDistributorList.do" class="btn default">Cancel</a>
            <html:hidden property="oldServerId"/>
            <html:hidden property="oldStartRingId"/>
            <html:hidden property="oldEndRingId"/>
            <input type="hidden" name="action" value="<%=request.getSession(true).getAttribute("formType")%>" />
        </div>
    </div>
</div>

