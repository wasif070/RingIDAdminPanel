<%-- 
    Document   : m_appConstantsTable
    Created on : Feb 14, 2017, 12:09:51 PM
    Author     : ipvision
--%>

<div class="form-body">
    <div class="form-group">
        <label class="col-md-3 control-label">Platform</label>
        <div class="col-md-4">
            <html:select property="platform" styleClass="form-control"> 
                <html:option  value="1">PC</html:option>     
                <html:option  value="2">Android</html:option>  
                <html:option  value="3">iOS</html:option>
                <html:option  value="4">Windows Phone</html:option>
                <html:option  value="5">Web</html:option>
            </html:select>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label">App Type</label>
        <div class="col-md-4">
            <html:text property="appType" title="App Type" styleClass="form-control" />
            <html:messages id="appType" property="appType">
                <bean:write name="appType"  filter="false"/>
            </html:messages>

        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label">Update Url</label>
        <div class="col-md-4">
            <html:text property="updateUrl" title="Version" styleClass="form-control" />
            <html:messages id="updateUrl" property="updateUrl">
                <bean:write name="updateUrl"  filter="false"/>
            </html:messages>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label">App Name</label>
        <div class="col-md-4">
            <html:text property="appName" title="App Name" styleClass="form-control" />
            <html:messages id="appName" property="appName">
                <bean:write name="appName"  filter="false"/>
            </html:messages>
        </div>
    </div>
</div>
