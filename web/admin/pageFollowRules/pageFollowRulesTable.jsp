<%-- 
    Document   : pageFollowRulesTable
    Created on : Aug 30, 2017, 12:02:20 PM
    Author     : Rabby
--%>


<%@page import="java.util.List"%>
<%@page import="com.ringid.admin.utils.Constants"%>
<div class="form-body">
    <div class="form-group">
        <div class="col-md-6 col-sm-6">
        </div>
        <div class="col-md-6 col-sm-6">
            <div class="input-group" style="float: right;">
                <input type="text" name="pageId" class="form-control input-sm input-small" placeholder="Search" title="Search" value="<bean:write name='PageFollowRulesForm' property='pageId' />"/>
                <span>
                    <button formaction="editPageFollowRules.do" class="btn green-soft uppercase bold input-sm" type="submit"><i class="icon-magnifier"></i></button>
                    <input name="action" type="hidden" value="<%=Constants.SEARCH%>" />
                </span>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label"> 
            Page Name
        </label>
        <div class="col-md-4">
            <label class="control-label"><bean:write name="PageFollowRulesForm" property="pageName"/></label>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label"> 
            Cities ISO
        </label>
        <div class="col-md-4">
            <html:text property="citiesIso" title="comma seperated city iso" styleClass="form-control" />
            <html:messages id="citiesIso" property="citiesIso">
                <span style="color: red;">CitiesIso Required</span>
            </html:messages>
        </div>
    </div>
    <div class="form-group">
        <%
            List<com.ringid.country.CountryDTO> countryDTOs = com.ringid.country.CountryLoader.getInstance().getCountryList();
        %>
        <label class="control-label col-md-3"> Countries ISO <span style="color: red">*</span></label>
        <div class="col-md-5">
            <html:select styleId="my_multi_select1" property="countriesISO" styleClass="multi-select form-control" multiple="multiple"> 
                <html:option  value="world"> world </html:option>     
                    <%
                        for (com.ringid.country.CountryDTO dto : countryDTOs) {
                    %>
                <html:option value="<%=dto.getCode()%>"><%=dto.getName()%></html:option>
                    <% }%>  
            </html:select>
            <html:messages id="countriesISO" property="countriesISO">
                <span style="color: red;">CountriesIso Required</span>
            </html:messages>
        </div>
        <div class="col-md-4">
            <label style="font-weight: bold"> Countries ISO : 
                <c:forEach items="${PageFollowRulesForm.countriesISO}" var="cntryISO" varStatus="loop">
                    <c:choose>
                        <c:when test="${loop.first}">
                            ${cntryISO}
                        </c:when>
                        <c:otherwise>
                            , ${cntryISO}
                        </c:otherwise>
                    </c:choose>
                </c:forEach>
            </label> 
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label"> 
            Languages
        </label>
        <div class="col-md-4">
            <html:text property="languages" title="comma seperated languages" styleClass="form-control" />
            <html:messages id="languages" property="languages">
                <span style="color: red;">Languages Required</span>
            </html:messages>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label"> 
            LivesInCityISO
        </label>
        <div class="col-md-4">
            <html:text property="livesInCityISO" title="Lives In City ISO" styleClass="form-control" />
            <html:messages id="livesInCityISO" property="livesInCityISO">
                <span style="color: red;">LivesInCityISO Required</span>
            </html:messages>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label"> 
            LivesInCountryISO
        </label>
        <div class="col-md-4">
            <html:text property="livesInCountryISO" title="Lives In Country ISO" styleClass="form-control" />
            <html:messages id="livesInCountryISO" property="livesInCountryISO">
                <span style="color: red;">LivesInCountryISO Required</span>
            </html:messages>
        </div>
    </div>
    <div class="form-group last">
        <label class="col-md-3 control-label"></label>
        <div class="col-md-4">
            <%
                if (request.getSession(true).getAttribute("error") == "2") {
            %>
            <html:messages id="message" property="message" ><bean:write name="msg"  filter="false"/></html:messages>
            <bean:write name="PageFollowRulesForm" property="message" filter="false"/>
            <%}%>
        </div>
    </div>
</div>
<div class="form-actions">
    <div class="row">
        <div class="col-md-offset-3 col-md-4">
            <button type="submit" class="btn green" name="saveButton">Submit</button>
            <html:hidden property="pageType"/>
            <html:hidden property="pageFollow"/>
            <html:hidden property="featuredTime"/>
            <html:hidden property="searchByRingId"/>
            <input type="hidden" name="action" value="<%=request.getSession(true).getAttribute("formType")%>" />
        </div>
    </div>
</div>
<script type="text/javascript">
    function removeSpanText() {
        document.getElementById("msgSpan").innerHTML = "";
    }
</script>
