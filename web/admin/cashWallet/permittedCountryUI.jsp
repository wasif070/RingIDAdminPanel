<%-- 
    Document   : permittedCountryUI
    Created on : Oct 17, 2017, 11:26:20 AM
    Author     : mamun
--%>

<%@include file="/login/loginTimeExpiry.jsp"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.ringid.country.CountryLoader"%>
<%@page import="com.ringid.country.CountryDTO"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.ringid.admin.utils.Constants"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@include  file="/shared/head.jsp" %>

<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Admin Panel | Add Reward Permitted Country</title>
        <%@include file="/shared/css-list.jsp"%>
    </head>
    <body class="page-container-bg-solid">
        <div class="page-wrapper">
            <div class="page-wrapper-row">
                <div class="page-wrapper-top">
                    <div class="page-header">
                        <%@include  file="/shared/menu.jsp" %>
                    </div>
                </div>
            </div>
            <div class="page-wrapper-row full-height">
                <div class="page-wrapper-middle">
                    <div class="page-container">
                        <div class="page-content-wrapper">
                            <div class="page-content">
                                <div class="container">
                                    <ul class="page-breadcrumb breadcrumb">
                                        <li>
                                            <a href="welcome.do">Admin Panel</a>
                                            <span style="font-size: 14px; margin: 0 auto;" class="fa fa-angle-right"></span>
                                        </li>
                                        <li>
                                            <a href="javascript:;">Common</a>
                                            <span style="font-size: 14px; margin: 0 auto;" class="fa fa-angle-right"></span>
                                        </li>
                                        <li>
                                            <a href="showCashWalletPermittedCountryList.do">Reward Permitted Country</a>
                                            <span style="font-size: 14px; margin: 0 auto;" class="fa fa-angle-right"></span>
                                        </li>
                                        <li>
                                            <span id="features" data-class-active=".common .cashWalletPermittedCountry .addCashWalletPermittedCountry"> Add Reward Permitted Country </span>
                                        </li>
                                    </ul>
                                    <div class="page-content-inner">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="tabbable-line boxless tabbable-reversed">
                                                    <div class="tab-content" style="padding-top: 15px;">
                                                        <div class="portlet box green">
                                                            <div class="portlet-title">
                                                                <div class="caption">
                                                                    <i class="fa fa-plus"></i> Add Reward Permitted Country Info
                                                                </div>
                                                                <div class="tools">
                                                                    <a href="javascript:;" class="collapse"> </a>
                                                                </div>
                                                            </div>
                                                            <div class="portlet-body form">
                                                                <html:form action="addOrUpdateCashWalletPermittedCountry" method="POST" acceptCharset="UTF-8" styleClass="form-horizontal" >
                                                                    <div class="form-body">
                                                                        <div class="form-group">
                                                                            <label class="col-md-3 control-label">Supported Platform <span style="color: red">*</span> </label>
                                                                            <div class="col-md-4">
                                                                                <html:select property="supportedPlatform" styleClass="form-control" multiple="multiple" size="5">
                                                                                    <html:option  value="1">PC</html:option>     
                                                                                    <html:option  value="2">Android</html:option>  
                                                                                    <html:option  value="3">iOS</html:option>
                                                                                    <html:option  value="4">Windows Phone</html:option>
                                                                                    <html:option  value="5">Web</html:option>
                                                                                </html:select>
                                                                                <html:messages id="supportedPlatform" property="supportedPlatform">
                                                                                    <bean:write name="supportedPlatform"  filter="false"/>
                                                                                </html:messages>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label class="col-md-3 control-label">Country <span style="color: red">*</span> </label>
                                                                            <div class="col-md-4">
                                                                                <%
                                                                                    ArrayList<CountryDTO> countryDTOs = CountryLoader.getInstance().getCountryList();
                                                                                %>
                                                                                <html:select property="countryId" styleClass="form-control" style="width:205px" >
                                                                                    <html:option value="">-- select country --</html:option>
                                                                                    <%
                                                                                        for (CountryDTO dto : countryDTOs) {
                                                                                    %>
                                                                                    <html:option value="<%=String.valueOf(dto.getId())%>"><%=dto.getName()%></html:option>
                                                                                    <%
                                                                                        }
                                                                                    %>
                                                                                </html:select>  
                                                                                <html:messages id="countryId" property="countryId">
                                                                                    <bean:write name="countryId"  filter="false"/>
                                                                                </html:messages>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label class="col-md-3 control-label">DialingCode <span style="color: red">*</span> </label>
                                                                            <div class="col-md-4">
                                                                                <html:text property="mobilePhoneDialingCode" title="Mobile Phone Dialing Code" styleClass="form-control" />
                                                                                <html:messages id="mobilePhoneDialingCode" property="mobilePhoneDialingCode">
                                                                                    <bean:write name="mobilePhoneDialingCode"  filter="false"/>
                                                                                </html:messages>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label class="col-md-3 control-label">Currency <span style="color: red">*</span> </label>
                                                                            <div class="col-md-4">
                                                                                <html:text property="currency" title="currency" styleClass="form-control" />
                                                                                <html:messages id="currency" property="currency">
                                                                                    <bean:write name="currency"  filter="false"/>
                                                                                </html:messages>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label class="col-md-3 control-label">Signup Cash</label>
                                                                            <div class="col-md-4">
                                                                                <html:text property="signupCash" title="signup Cash" styleClass="form-control" />
                                                                                <html:messages id="signupCash" property="signupCash">
                                                                                    <bean:write name="signupCash"  filter="false"/>
                                                                                </html:messages>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label class="col-md-3 control-label">Signup Coin</label>
                                                                            <div class="col-md-4">
                                                                                <html:text property="signupCoin" title="signup Coin" styleClass="form-control" />
                                                                                <html:messages id="signupCoin" property="signupCoin">
                                                                                    <bean:write name="signupCoin"  filter="false"/>
                                                                                </html:messages>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label class="col-md-3 control-label">Signup Ringbit</label>
                                                                            <div class="col-md-4">
                                                                                <html:text property="signupRingbit" title="signup Ringbit" styleClass="form-control" />
                                                                                <html:messages id="signupRingbit" property="signupRingbit">
                                                                                    <bean:write name="signupRingbit"  filter="false"/>
                                                                                </html:messages>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label class="col-md-3 control-label">Referral Cash</label>
                                                                            <div class="col-md-4">
                                                                                <html:text property="referralCash" title="referral Cash" styleClass="form-control" />
                                                                                <html:messages id="referralCash" property="referralCash">
                                                                                    <bean:write name="referralCash"  filter="false"/>
                                                                                </html:messages>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label class="col-md-3 control-label">Referral Coin</label>
                                                                            <div class="col-md-4">
                                                                                <html:text property="referralCoin" title="referral Coin" styleClass="form-control" />
                                                                                <html:messages id="referralCoin" property="referral Coin">
                                                                                    <bean:write name="referralCoin"  filter="false"/>
                                                                                </html:messages>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label class="col-md-3 control-label">Referral Ringbit</label>
                                                                            <div class="col-md-4">
                                                                                <html:text property="referralRingbit" title="referral Ringbit" styleClass="form-control" />
                                                                                <html:messages id="referralRingbit" property="referralRingbit">
                                                                                    <bean:write name="referralRingbit"  filter="false"/>
                                                                                </html:messages>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label class="col-md-3 control-label">Cashout  Limit</label>
                                                                            <div class="col-md-4">
                                                                                <html:text property="cashoutlimit" title="cashout limit" styleClass="form-control" />
                                                                                <html:messages id="cashoutlimit" property="cashoutlimit">
                                                                                    <bean:write name="cashoutlimit"  filter="false"/>
                                                                                </html:messages>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label class="col-md-3 control-label">Status</label>
                                                                            <div class="col-md-4">
                                                                                <html:select property="status" styleClass="form-control" >
                                                                                    <html:option  value="1">Active</html:option>     
                                                                                    <html:option  value="0">Inactive</html:option>
                                                                                </html:select>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-actions">
                                                                        <div class="row">
                                                                            <div class="col-md-offset-3 col-md-8">
                                                                                <logic:equal name="PermittedCountryForm" property="action" value="5">
                                                                                    <input type="hidden" name="action" value="5" />
                                                                                </logic:equal>
                                                                                <logic:notEqual name="PermittedCountryForm" property="action" value="5">
                                                                                    <input type="hidden" name="action" value="3" />
                                                                                </logic:notEqual>
                                                                                <button type="submit" class="btn green" name="saveButton">Submit</button>
                                                                                <a href="showCashWalletPermittedCountryList.do" class="btn default">Cancel</a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </html:form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <%@include file="/shared/footer.jsp"%>
        </div>
        <%@include file="/shared/script-list.jsp"%>
    </body>
</html>
