<%-- 
    Document   : permittedCountryList
    Created on : Oct 17, 2017, 11:25:55 AM
    Author     : mamun
--%>

<%@page import="com.ringid.admin.dto.PermittedCountryDTO"%>
<%@page import="com.ringid.admin.projectMenu.MenuNames"%>
<%@include file="/login/loginTimeExpiry.jsp"%>
<%@page import="com.ringid.country.CountryLoader"%>
<%@page import="com.ringid.country.CountryDTO"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@include  file="/shared/head.jsp" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Admin Panel | Cash Wallet Reward Permitted Country Info</title>
        <%@include file="/shared/css-list.jsp"%>
    </head>
    <body class="page-container-bg-solid">
        <div class="page-wrapper">
            <div class="page-wrapper-row">
                <div class="page-wrapper-top">
                    <div class="page-header">
                        <%@include  file="/shared/menu.jsp" %>
                        <%
                            String FEATURE_NAME = MenuNames.CASH_WALLET_PERMITTED_COUNTRY;
                            int TOTAL_RECORDS = 0, TOTAL_PAGES = 0, CURRENT_PAGE_NO = 0, STARTING_RECORD_NO = 0, ENDING_RECORD_NO = 0;
                            String disabledPrev = "", disabledNext = "", msg = "";
                            if (session.getAttribute("msg") != null) {
                                msg = session.getAttribute("msg").toString();
                                session.removeAttribute("msg");
                            }
                            if (request.getAttribute(Constants.TOTAL_RECORDS) != null) {
                                TOTAL_RECORDS = Integer.valueOf(request.getAttribute(Constants.TOTAL_RECORDS).toString());
                            }
                            if (request.getAttribute(Constants.TOTAL_PAGES) != null) {
                                TOTAL_PAGES = Integer.valueOf(request.getAttribute(Constants.TOTAL_PAGES).toString());
                            }
                            if (request.getAttribute(Constants.CURRENT_PAGE_NO) != null) {
                                CURRENT_PAGE_NO = Integer.valueOf(request.getAttribute(Constants.CURRENT_PAGE_NO).toString());
                            }
                            if (request.getAttribute(Constants.STARTING_RECORD_NO) != null) {
                                STARTING_RECORD_NO = Integer.valueOf(request.getAttribute(Constants.STARTING_RECORD_NO).toString());
                            }
                            if (request.getAttribute(Constants.ENDING_RECORD_NO) != null) {
                                ENDING_RECORD_NO = Integer.valueOf(request.getAttribute(Constants.ENDING_RECORD_NO).toString());
                            }
                            if (STARTING_RECORD_NO <= 0) {
                                disabledPrev = "disabled";
                                STARTING_RECORD_NO = 0;
                            }
                            if (ENDING_RECORD_NO >= TOTAL_RECORDS) {
                                disabledNext = "disabled";
                                ENDING_RECORD_NO = TOTAL_RECORDS;
                            }
                        %>
                    </div>
                </div>
            </div>
            <div class="page-wrapper-row full-height">
                <div class="page-wrapper-middle">
                    <div class="page-container">
                        <div class="page-content-wrapper">
                            <div class="page-content">
                                <div class="container">
                                    <ul class="page-breadcrumb breadcrumb">
                                        <li>
                                            <a href="welcome.do">Admin Panel</a>
                                            <span style="font-size: 14px; margin: 0 auto;" class="fa fa-angle-right"></span>
                                        </li>
                                        <li>
                                            <a href="javascript:;">Common</a>
                                            <span style="font-size: 14px; margin: 0 auto;" class="fa fa-angle-right"></span>
                                        </li>
                                        <li>
                                            <a href="showCashWalletPermittedCountryList.do">Reward Permitted Country</a>
                                            <span style="font-size: 14px; margin: 0 auto;" class="fa fa-angle-right"></span>
                                        </li>
                                        <li>
                                            <span id="features" data-class-active=".common .cashWalletPermittedCountry .manageCashWalletPermittedCountry"> Manage Reward Permitted Country </span>
                                        </li>
                                    </ul>
                                    <% if (msg.length() > 0) {%>
                                    <div id="prefix_508472435942" class="custom-alerts alert alert-success fade in">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                        <strong> <%=msg%> </strong>
                                    </div>
                                    <% }%>
                                    <div class="page-content-inner">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="tabbable-line boxless tabbable-reversed">
                                                    <div class="tab-content" style="padding-top: 15px;">
                                                        <div class="portlet box green">
                                                            <div class="portlet-title">
                                                                <div class="caption">
                                                                    <i class="fa fa-list-ul"></i> Cash Wallet Permitted Country List Info </div>
                                                                <div class="tools">
                                                                    <a href="javascript:;" class="collapse"> </a>
                                                                    <a data-original-title="Reload" href="showCashWalletPermittedCountryList.do?ForceReload=1" style="width: 13px; background-image: url(assets/global/img/portlet-reload-icon-white.png);" class="tooltips"> </a>
                                                                    <%if ((loginDTO.getPermissionLevel() == Constants.SUPER_ADMIN) || (loginDTO.getFeauterMapList().get(FEATURE_NAME).getHasAddPermission())) {%>
                                                                    <a data-original-title="AddNew" style="color: white; width: 16px; background-image: url(assets/global/img/add-1.png);" href="admin/cashWallet/permittedCountryUI.jsp" class="tooltips"> </a>
                                                                    <% }%>
                                                                </div>
                                                            </div>
                                                            <div class="portlet-body form">
                                                                <html:form action="showCashWalletPermittedCountryList" styleClass="form-horizontal" method="POST"  acceptCharset="UTF-8">
                                                                    <div class="form-body" >
                                                                        <div class="form-group">
                                                                            <div class="col-md-12 col-sm-12">
                                                                                <label>
                                                                                    <%
                                                                                        ArrayList<CountryDTO> lst = CountryLoader.getInstance().getCountryList();
                                                                                    %>
                                                                                    <html:select property="countryId" styleClass="bs-select form-control">
                                                                                        <html:option value="">--Select Country--</html:option>    
                                                                                        <%
                                                                                            for (CountryDTO dto : lst) {
                                                                                        %>
                                                                                        <html:option value="<%=String.valueOf(dto.getId())%>"><%=dto.getName()%></html:option>
                                                                                        <%
                                                                                            }
                                                                                        %>
                                                                                    </html:select>
                                                                                </label>
                                                                                <label>
                                                                                    <html:select property="status" styleClass="bs-select form-control input-inline">
                                                                                        <html:option value="-1">--All--</html:option>
                                                                                        <html:option value="1">Active</html:option>
                                                                                        <html:option value="0">Inactive</html:option>
                                                                                    </html:select>
                                                                                </label>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <div class="col-md-6 col-sm-6">
                                                                                <label>
                                                                                    <html:select styleClass="bs-select form-control input-sm input-xsmall input-inline" property="recordPerPage" onchange="javascript: submitform(1, 1, this);">
                                                                                        <html:option value="10">10</html:option>
                                                                                        <html:option value="25">25</html:option>
                                                                                        <html:option value="50">50</html:option>
                                                                                        <html:option value="100">100</html:option>
                                                                                        <html:option value="200">200</html:option>
                                                                                    </html:select>
                                                                                    records</label>
                                                                            </div>
                                                                            <div class="col-md-6 col-sm-6">
                                                                                <div class="input-group" style="float: right;">
                                                                                    <input name="searchText" type="text" class="form-control input-sm input-small" placeholder="Search" value="<bean:write name="PermittedCountryForm" property="searchText"/>">
                                                                                    <span>
                                                                                        <button class="btn green-soft uppercase bold input-sm" type="submit"><i class="icon-magnifier"></i></button>
                                                                                        <input name="action" type="hidden" value="<%=Constants.SEARCH%>" />
                                                                                        <html:hidden property="column" />
                                                                                        <html:hidden property="sort" />
                                                                                    </span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="table-scrollable">
                                                                            <table class="table table-striped table-bordered table-hover">
                                                                                <thead>
                                                                                    <tr>
                                                                                        <th class="text-center">Country</th>
                                                                                        <th class="text-center">Currency</th>
                                                                                        <th class="text-center">DialingCode</th>
                                                                                        <th class="text-center">SignupCash</th>
                                                                                        <th class="text-center">SignupCoin</th>
                                                                                        <th class="text-center">SignupRingbit</th>
                                                                                        <th class="text-center">ReferralCash</th>
                                                                                        <th class="text-center">ReferralCoin</th>
                                                                                        <th class="text-center">ReferralRingbit</th>
                                                                                        <th class="text-center">CashoutLimit</th>
                                                                                        <th class="text-center">Status</th>
                                                                                        <th class="text-center">CashSupportedPlatform</th>
                                                                                            <%if ((loginDTO.getPermissionLevel() == Constants.SUPER_ADMIN) || (loginDTO.getFeauterMapList().get(FEATURE_NAME).getHasModifyPermission()
                                                                                                        || loginDTO.getFeauterMapList().get(FEATURE_NAME).getHasDeletePermission())) {%>      
                                                                                        <th class="text-center">Action</th>
                                                                                            <% }%> 
                                                                                    </tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                    <%
                                                                                        String platform, status;
                                                                                        ArrayList<PermittedCountryDTO> data1 = (ArrayList<PermittedCountryDTO>) session.getAttribute(Constants.DATA_ROWS);
                                                                                        session.removeAttribute(Constants.DATA_ROWS);
                                                                                        if (data1 != null && data1.size() > 0) {
                                                                                            for (int i = STARTING_RECORD_NO; i < ENDING_RECORD_NO; i++) {
                                                                                                PermittedCountryDTO dto = (PermittedCountryDTO) data1.get(i);
                                                                                                platform = "";
                                                                                                if (dto.getStatus() == 1) {
                                                                                                    status = "Active";
                                                                                                } else {
                                                                                                    status = "Inactive";
                                                                                                }
                                                                                                for (String str : dto.getSupportedPlatform().split(",")) {
                                                                                                    if (str != null && str.length() > 0) {
                                                                                                        if (platform.length() > 0) {
                                                                                                            platform += ", ";
                                                                                                        }
                                                                                                        switch (Integer.parseInt(str)) {
                                                                                                            case 1:
                                                                                                                platform += "PC";
                                                                                                                break;
                                                                                                            case 2:
                                                                                                                platform += "Android";
                                                                                                                break;
                                                                                                            case 3:
                                                                                                                platform += "iOS";
                                                                                                                break;
                                                                                                            case 4:
                                                                                                                platform += "Windows Phone";
                                                                                                                break;
                                                                                                            case 5:
                                                                                                                platform += "Web";
                                                                                                                break;
                                                                                                            default:
                                                                                                                platform += str;
                                                                                                                break;
                                                                                                        }
                                                                                                    }
                                                                                                }
                                                                                    %>
                                                                                    <tr>
                                                                                        <td class="text-center"><%=dto.getCountryName()%></td>
                                                                                        <td class="text-center"><%=dto.getCurrency()%></td>
                                                                                        <td class="text-center"><%=dto.getMobilePhoneDialingCode()%></td>
                                                                                        <td class="text-center"><%=dto.getSignupCash()%></td>
                                                                                        <td class="text-center"><%=dto.getSignupCoin()%></td>
                                                                                        <td class="text-center"><%=dto.getSignupRingbit()%></td>
                                                                                        <td class="text-center"><%=dto.getReferralCash()%></td>
                                                                                        <td class="text-center"><%=dto.getReferralCoin()%></td>
                                                                                        <td class="text-center"><%=dto.getReferralRingbit()%></td>
                                                                                        <td class="text-center"><%=dto.getCashoutlimit()%></td>
                                                                                        <td class="text-center"><%=status%></td>
                                                                                        <td class="text-center"><%=platform%></td>
                                                                                        <%if ((loginDTO.getPermissionLevel() == Constants.SUPER_ADMIN) || (loginDTO.getFeauterMapList().get(FEATURE_NAME).getHasModifyPermission() || loginDTO.getFeauterMapList().get(FEATURE_NAME).getHasDeletePermission())) {%>
                                                                                        <td class="text-center">
                                                                                            <%if ((loginDTO.getPermissionLevel() == Constants.SUPER_ADMIN) || (loginDTO.getFeauterMapList().get(FEATURE_NAME).getHasModifyPermission())) {%>
                                                                                            <a href="editCashWalletPermittedCountry.do?countryShortCode=<%=dto.getCountryShortCode() + "&action=" + Constants.EDIT%>" class="" title="Change"><span class="fa fa-edit"></span></a>
                                                                                                <% }%>
                                                                                                <%if ((loginDTO.getPermissionLevel() == Constants.SUPER_ADMIN) || (loginDTO.getFeauterMapList().get(FEATURE_NAME).getHasDeletePermission())) {%>
                                                                                            &nbsp;<a href="deleteCashWalletPermittedCountry.do?countryShortCode=<%=dto.getCountryShortCode() + "&action=" + Constants.DELETE%>" class="" title="Delete" onclick="javascript:return confirm('Are you sure you want to delete this content')"><span class="fa fa-remove"></span></a>
                                                                                                <% } %>  
                                                                                        </td>
                                                                                        <% } %>        
                                                                                    </tr>
                                                                                    <%
                                                                                            }
                                                                                        }
                                                                                    %>
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                        <div class="row">
                                                                            <div class="col-md-5 col-sm-12">
                                                                                <div>
                                                                                    Showing <%=(STARTING_RECORD_NO + 1)%> to <%=ENDING_RECORD_NO%> of <%=TOTAL_RECORDS%> entries
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-7 col-sm-12">
                                                                                <div style="float: right;">
                                                                                    <div> 
                                                                                        Page <a href="javascript: submitform(2, <%=(CURRENT_PAGE_NO - 1)%>, this);" class="btn btn-sm default prev <%=disabledPrev%>"><i class="fa fa-angle-left"></i></a>
                                                                                        <input name="pageNo" class="form-control input-sm input-inline input-mini" maxlenght="5" style="text-align:center; margin: 0 5px;" type="text" value="<%=CURRENT_PAGE_NO%>" onchange="javascript: submitform(2, 0, this);">
                                                                                        <a href="javascript: submitform(2, <%=(CURRENT_PAGE_NO + 1)%>, this);" class="btn btn-sm default next <%=disabledNext%>"><i class="fa fa-angle-right"></i></a> of <span><%=TOTAL_PAGES%></span>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </html:form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <%@include file="/shared/footer.jsp"%>
        </div>
        <%@include file="/shared/script-list.jsp"%>
    </body>
</html>
