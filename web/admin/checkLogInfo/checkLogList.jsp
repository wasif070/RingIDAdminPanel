<%-- 
    Document   : m_checkLogList
    Created on : Feb 15, 2017, 12:44:38 PM
    Author     : MAMUN
--%>

<%@page import="com.ringid.admin.dto.CheckLogDTO"%>
<%@include file="/login/loginTimeExpiry.jsp"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="java.util.*,com.ringid.admin.utils.Constants"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@include  file="/shared/head.jsp" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Admin Panel | Check Log List Info</title>
        <%@include file="/shared/css-list.jsp"%>
    </head>
    <body class="page-container-bg-solid">
        <div class="page-wrapper">
            <div class="page-wrapper-row">
                <div class="page-wrapper-top">
                    <div class="page-header">
                        <%@include  file="/shared/menu.jsp" %>
                        <%
                            int TOTAL_RECORDS = 0, TOTAL_PAGES = 0, CURRENT_PAGE_NO = 0, STARTING_RECORD_NO = 0, ENDING_RECORD_NO = 0;
                            String disabledPrev = "", disabledNext = "";
                            if (request.getAttribute(Constants.TOTAL_RECORDS) != null) {
                                TOTAL_RECORDS = Integer.valueOf(request.getAttribute(Constants.TOTAL_RECORDS).toString());
                            }
                            if (request.getAttribute(Constants.TOTAL_PAGES) != null) {
                                TOTAL_PAGES = Integer.valueOf(request.getAttribute(Constants.TOTAL_PAGES).toString());
                            }
                            if (request.getAttribute(Constants.CURRENT_PAGE_NO) != null) {
                                CURRENT_PAGE_NO = Integer.valueOf(request.getAttribute(Constants.CURRENT_PAGE_NO).toString());
                            }
                            if (request.getAttribute(Constants.STARTING_RECORD_NO) != null) {
                                STARTING_RECORD_NO = Integer.valueOf(request.getAttribute(Constants.STARTING_RECORD_NO).toString());
                            }
                            if (request.getAttribute(Constants.ENDING_RECORD_NO) != null) {
                                ENDING_RECORD_NO = Integer.valueOf(request.getAttribute(Constants.ENDING_RECORD_NO).toString());
                            }
                            if (STARTING_RECORD_NO <= 0) {
                                disabledPrev = "disabled";
                                STARTING_RECORD_NO = 0;
                            }
                            if (ENDING_RECORD_NO >= TOTAL_RECORDS) {
                                disabledNext = "disabled";
                                ENDING_RECORD_NO = TOTAL_RECORDS;
                            }
                        %>
                    </div>
                </div>
            </div>
            <div class="page-wrapper-row full-height">
                <div class="page-wrapper-middle">
                    <div class="page-container">
                        <div class="page-content-wrapper">
                            <div class="page-content">
                                <div class="container">
                                    <ul class="page-breadcrumb breadcrumb">
                                        <li>
                                            <a href="welcome.do">Admin Panel</a>
                                            <span style="font-size: 14px; margin: 0 auto;" class="fa fa-angle-right"></span>
                                        </li>
                                        <li>
                                            <a href="javascript:;">Notify</a>
                                            <span style="font-size: 14px; margin: 0 auto;" class="fa fa-angle-right"></span>
                                        </li>
                                        <li>
                                            <span id="features" data-class-active=".notify .checkLog">Check Log</span>
                                        </li>
                                    </ul>
                                    <div class="page-content-inner">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="tabbable-line boxless tabbable-reversed">
                                                    <div class="tab-content" style="padding-top: 15px;">
                                                        <div class="portlet box green">
                                                            <div class="portlet-title">
                                                                <div class="caption">
                                                                    <i class="fa fa-list-ul"></i> Check Log Info </div>
                                                                <div class="tools">
                                                                    <a href="javascript:;" class="collapse"> </a>
                                                                    <a data-original-title="Reload" href="reloadCheckLog.do?ForceReload=1" style="width: 13px; background-image: url(assets/global/img/portlet-reload-icon-white.png);" class="tooltips"> </a>
                                                                </div>
                                                            </div>
                                                            <div class="portlet-body form">
                                                                <html:form action="checkLogResendOrDelete" styleClass="form-horizontal" method="POST"  acceptCharset="UTF-8">
                                                                    <div class="form-body">
                                                                        <div class="form-group">
                                                                            <div class="col-md-6 col-sm-6">
                                                                                <label>
                                                                                    <html:select styleClass="bs-select form-control input-sm input-xsmall input-inline" property="recordPerPage" onchange="javascript: submitform(1, 1, this);">
                                                                                        <html:option value="10">10</html:option>
                                                                                        <html:option value="25">25</html:option>
                                                                                        <html:option value="50">50</html:option>
                                                                                        <html:option value="100">100</html:option>
                                                                                        <html:option value="200">200</html:option>
                                                                                    </html:select>
                                                                                    records</label>
                                                                            </div>
                                                                            <div class="col-md-6 col-sm-6">
                                                                                <div class="input-group" style="float: right;">
                                                                                    <input name="searchText" type="text" class="form-control input-sm input-small" placeholder="Search" value="<bean:write name="CheckLogForm" property="searchText"/>">
                                                                                    <span>
                                                                                        <button class="btn green-soft uppercase bold input-sm" type="submit"><i class="icon-magnifier"></i></button>
                                                                                        <input name="action" type="hidden" value="<%=Constants.SEARCH%>" />
                                                                                        <html:hidden name="CheckLogForm" property="column" />
                                                                                        <html:hidden name="CheckLogForm" property="sort" />
                                                                                    </span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="table-scrollable">
                                                                            <table class="table table-striped table-bordered table-hover">
                                                                                <thead>
                                                                                    <tr>
                                                                                        <th class="text-center">
                                                                                            <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                                                                                <input type="checkbox" onchange="checkAll(this)" class="group-checkable" />
                                                                                                <span></span>
                                                                                            </label>
                                                                                        </th>
                                                                                        <th class="text-center">Server Type</th>
                                                                                        <th class="text-center">Server IP</th>
                                                                                        <th class="text-center">Response MSG</th>
                                                                                        <th class="text-center">Request Time</th>
                                                                                        <th class="text-center">Response Time</th>
                                                                                        <th class="text-center">Status</th>
                                                                                    </tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                    <%
                                                                                        ArrayList<CheckLogDTO> data1 = (ArrayList<CheckLogDTO>) session.getAttribute(Constants.DATA_ROWS);
                                                                                        session.removeAttribute(Constants.DATA_ROWS);
                                                                                        if (data1 != null && data1.size() > 0) {
                                                                                            //out.print(data.size());
                                                                                            String serverType = "";
                                                                                            for (int i = STARTING_RECORD_NO; i < ENDING_RECORD_NO; i++) {
                                                                                                CheckLogDTO dto = (CheckLogDTO) data1.get(i);
                                                                                                switch (dto.getServerType()) {
                                                                                                    case Constants.RELOAD_VOICE_SERVERS:
                                                                                                        serverType = "RELOAD VOICE SERVERS";
                                                                                                        break;
                                                                                                    case Constants.RELOAD_CHAT_SERVERS:
                                                                                                        serverType = "RELOAD CHAT SERVERS";
                                                                                                        break;
                                                                                                    case Constants.RELOAD_LOG_SETTINGS:
                                                                                                        serverType = "RELOAD LOG SETTINGS";
                                                                                                        break;
                                                                                                    case Constants.RELOAD_SETTINGS:
                                                                                                        serverType = "RELOAD SETTINGS";
                                                                                                        break;
                                                                                                    case Constants.RELOAD_AUTH_SERVERS:
                                                                                                        serverType = "RELOAD AUTH SERVERS";
                                                                                                        break;
                                                                                                    case Constants.RELOAD_SELF_SETTINGS:
                                                                                                        serverType = "RELOAD SELF SETTINGS";
                                                                                                        break;
                                                                                                    case Constants.RELOAD_FLOATING_MENU:
                                                                                                        serverType = "RELOAD FLOATING MENU";
                                                                                                        break;
                                                                                                    case Constants.RELOAD_BREAKING_FEEDS:
                                                                                                        serverType = "RELOAD BREAKING FEEDS";
                                                                                                        break;
                                                                                                    case Constants.RELOAD_PAGE_TYPES:
                                                                                                        serverType = "RELOAD PAGE TYPES";
                                                                                                        break;
                                                                                                    case Constants.RELOAD_PAGE_CATEGORIES:
                                                                                                        serverType = "RELOAD PAGE CATEGORIES";
                                                                                                        break;
                                                                                                    case Constants.RELOAD_OFFERS:
                                                                                                        serverType = "RELOAD OFFERS";
                                                                                                        break;
                                                                                                    case Constants.RELOAD_WALLET_PERMITTED_COUNTRY_LIST:
                                                                                                        serverType = "RELOAD WALLET PERMITTED COUNTRY LIST";
                                                                                                        break;
                                                                                                    case Constants.RELOAD_RINGSTUDIO_SETTINGS:
                                                                                                        serverType = "RELOAD RINGSTUDIO SETTINGS";
                                                                                                        break;
                                                                                                    case Constants.RELOAD_CELEBRITY_ROOM_DETAILS:
                                                                                                        serverType = "RELOAD CELEBRITY ROOM DETAILS";
                                                                                                        break;
                                                                                                    case Constants.RELOAD_WALLET_FLEXI_BLACKLISTED_USERS:
                                                                                                        serverType = "RELOAD WALLET FLEXI BLACKLISTED USERS";
                                                                                                        break;
                                                                                                    case Constants.RELOAD_RINGSTORE_SETTINGS:
                                                                                                        serverType = "RELOAD RINGSTORE SETTINGS";
                                                                                                        break;
                                                                                                    case Constants.RELOAD_HOME_DONATION_PAGE:
                                                                                                        serverType = "RELOAD HOME DONATION PAGE";
                                                                                                        break;
                                                                                                    case Constants.RELOAD_STREAMING_CODEC_SETTINGS:
                                                                                                        serverType = "RELOAD STREAMING CODEC SETTINGS";
                                                                                                        break;
                                                                                                    case Constants.RESET_STATIC_FEEDBACK_TIME:
                                                                                                        serverType = "RESET STATIC FEEDBACK TIME";
                                                                                                        break;
                                                                                                    case Constants.RELOAD_SPORTS_SERVER_DATA:
                                                                                                        serverType = "RELOAD SPORTS SERVER DATA";
                                                                                                        break;
                                                                                                    case Constants.RESET_SQL_EXCEPTION_COUNTER:
                                                                                                        serverType = "RESET SQL EXCEPTION COUNTER";
                                                                                                        break;
                                                                                                    case Constants.RELOAD_COIN_SERVER_DATA:
                                                                                                        serverType = "RELOAD COIN SERVER DATA";
                                                                                                        break;
                                                                                                    case Constants.RELOAD_CHANNEL_CATEGORY:
                                                                                                        serverType = "RELOAD CHANNEL CATEGORY";
                                                                                                        break;
                                                                                                    case Constants.RELOAD_LIVE_TO_CHANNEL:
                                                                                                        serverType = "RELOAD LIVE TO CHANNEL";
                                                                                                        break;
                                                                                                    case Constants.RELOAD_RINGBIT_SETTINGS:
                                                                                                        serverType = "RELOAD RINGBIT SETTINGS";
                                                                                                        break;
                                                                                                    case Constants.RELOAD_DYNAMIC_MENU:
                                                                                                        serverType = "RELOAD DYNAMIC MENU";
                                                                                                        break;
                                                                                                    case Constants.RELOAD_RINGID_DISTRIBUTOR:
                                                                                                        serverType = "RELOAD RINGID DISTRIBUTOR";
                                                                                                        break;
                                                                                                    case Constants.RELOAD_SIGNUP_REWARD_SETTINGS:
                                                                                                        serverType = "RELOAD SIGNUP REWARD SETTINGS";
                                                                                                        break;
                                                                                                    case Constants.RELOAD_SPAM_REASONS:
                                                                                                        serverType = "RELOAD SPAM REASONS";
                                                                                                        break;
                                                                                                    case Constants.RELOAD_FEATURE_MANAGER:
                                                                                                        serverType = "RELOAD FEATURE MANAGER";
                                                                                                        break;
                                                                                                    case Constants.RELOAD_FIREBASE_IP_PORT:
                                                                                                        serverType = "RELOAD FIREBASE IP PORT";
                                                                                                        break;
                                                                                                    case Constants.RELOAD_FIREBASE_COUNTRY_LIST:
                                                                                                        serverType = "RELOAD FIREBASE COUNTRY LIST";
                                                                                                        break;
                                                                                                    case Constants.RELOAD_SERVER_MESSAGES:
                                                                                                        serverType = "RELOAD SERVER MESSAGES";
                                                                                                        break;
                                                                                                    case Constants.RELOAD_AD_SERVER_DATA:
                                                                                                        serverType = "RELOAD AD SERVER DATA";
                                                                                                        break;
                                                                                                    case Constants.RELOAD_STREAMING_SERVERS:
                                                                                                        serverType = "RELOAD STREAMING SERVERS";
                                                                                                        break;
                                                                                                    case Constants.RELOAD_MANUAL_RECORDED_LIVES:
                                                                                                        serverType = "RELOAD MANUAL RECORDED LIVES";
                                                                                                        break;
                                                                                                    case Constants.RELOAD_USER_WALLETS:
                                                                                                        serverType = "RELOAD USER WALLETS";
                                                                                                        break;
                                                                                                    case Constants.RELOAD_STICKER_REPO:
                                                                                                        serverType = "RELOAD STICKER REPO";
                                                                                                        break;
                                                                                                    case Constants.RELOAD_STREAM_ADMINS_REPO:
                                                                                                        serverType = "RELOAD STREAM ADMINS REPO";
                                                                                                        break;
                                                                                                    case Constants.RELOAD_APP_UPDATE_SETTINGS:
                                                                                                        serverType = "RELOAD APP UPDATE SETTINGS";
                                                                                                        break;
                                                                                                    default:
                                                                                                        serverType = String.valueOf(dto.getServerType());
                                                                                                        break;
                                                                                                }
                                                                                    %>
                                                                                    <tr>
                                                                                        <td class="text-center">
                                                                                            <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                                                                                <input type="checkbox" class="checkboxes" onchange="checkItem(this)" name="selectedIDs" value="<%=dto.getPckId()%>" />
                                                                                                <span></span>
                                                                                            </label>
                                                                                        </td>
                                                                                        <td class="text-center"><%=serverType%></td>
                                                                                        <td class="text-center"><%=dto.getServerIP()%></td>
                                                                                        <td class="text-center"><%=dto.getResponseMSG()%></td>
                                                                                        <td class="text-center"><%=dto.getRequestDate()%></td>
                                                                                        <td class="text-center"><%=dto.getResponseDate()%></td>
                                                                                        <td class="text-center"><%=dto.getStatus()%></td>       
                                                                                    </tr>
                                                                                    <%
                                                                                            }
                                                                                        }
                                                                                    %>
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                        <div class="row">
                                                                            <div class="col-md-5 col-sm-12">
                                                                                <div>
                                                                                    Showing <%=(STARTING_RECORD_NO + 1)%> to <%=ENDING_RECORD_NO%> of <%=TOTAL_RECORDS%> entries
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-7 col-sm-12">
                                                                                <div style="float: right;">
                                                                                    <div> 
                                                                                        Page <a href="javascript: submitform(2, <%=(CURRENT_PAGE_NO - 1)%>, this);" class="btn btn-sm default prev <%=disabledPrev%>"><i class="fa fa-angle-left"></i></a>
                                                                                        <input name="pageNo" class="form-control input-sm input-inline input-mini" maxlenght="5" style="text-align:center; margin: 0 5px;" type="text" value="<%=CURRENT_PAGE_NO%>" onchange="javascript: submitform(2, 0, this);">
                                                                                        <a href="javascript: submitform(2, <%=(CURRENT_PAGE_NO + 1)%>, this);" class="btn btn-sm default next <%=disabledNext%>"><i class="fa fa-angle-right"></i></a> of <span><%=TOTAL_PAGES%></span>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-actions">
                                                                        <div class="row">
                                                                            <div class="col-md-offset-3 col-md-9">
                                                                                <input type="hidden" id="action" name="action" />
                                                                                <button type="submit" class="btn red" formaction="checkLogResendOrDelete.do?action=<%=Constants.LOG_DELETE%>">Remove</button>
                                                                                <button type="submit" class="btn green" formaction="checkLogResendOrDelete.do?action=<%=Constants.LOG_RESEND%>">Resend</button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </html:form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <%@include file="/shared/footer.jsp"%>
        </div>
        <%@include file="/shared/script-list.jsp"%>
    </body>
</html>

