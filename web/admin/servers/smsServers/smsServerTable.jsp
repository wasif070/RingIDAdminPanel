<%-- 
    Document   : m_appConstantsTable
    Created on : Feb 14, 2017, 12:09:51 PM
    Author     : rabby
--%>

<div class="form-body">
    <div class="form-group">
        <label class="col-md-3 control-label">Id</label>
        <div class="col-md-4">
            <html:text property="newId" title="newId" styleClass="form-control" />
            <html:messages id="newId" property="newId">
                <bean:write name="newId"  filter="false"/>
            </html:messages>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label">Server URL <span style="color: red">*</span></label>
        <div class="col-md-4">
            <html:text property="url" title="Server URL" styleClass="form-control" />
            <html:messages id="url" property="url">
                <bean:write name="url"  filter="false"/>
            </html:messages>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label">SMS Verification URL <span style="color: red">*</span></label>
        <div class="col-md-4">
            <html:text property="smsVerificationUrl" title="SMS Verification URL" styleClass="form-control" />
            <html:messages id="smsVerificationUrl" property="smsVerificationUrl">
                <bean:write name="smsVerificationUrl"  filter="false"/>
            </html:messages>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label">Server Status</label>
        <div class="col-md-4">
            <html:text property="status" title="Server Status" styleClass="form-control" />
            <html:messages id="status" property="status">
                <bean:write name="status"  filter="false"/>
            </html:messages>
        </div>
    </div>
    <div class="form-group last">
        <label class="col-md-3 control-label"></label>
        <div class="col-md-4">
            <%
                if (request.getSession(true).getAttribute("error") == "2") {
            %>  
            <html:messages id="message" property="message"><bean:write name="msg"  filter="false"/></html:messages>
            <bean:write name="SmsServerForm" property="message" filter="false"/>
            <%}%>
        </div>
    </div>
</div>
<div class="form-actions">
    <div class="row">
        <div class="col-md-offset-3 col-md-4">
            <button type="submit" class="btn green" name="saveButton">Submit</button>
            <a href="smsServerListInfo.do" class="btn default">Cancel</a>
            <html:hidden property="id"/>
            <input type="hidden" name="action" value="<%=request.getSession(true).getAttribute("formType")%>" />
        </div>
    </div>
</div>
