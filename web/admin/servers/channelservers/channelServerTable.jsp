<%-- 
    Document   : channelServerTable
    Created on : Aug 2, 2017, 1:09:14 PM
    Author     : ipvision
--%>

<%@page import="java.util.List"%>
<%@page import="com.ringid.admin.utils.Constants"%>
<div class="form-body">
    <div class="form-group">
        <label class="col-md-3 control-label">Id</label>
        <div class="col-md-4">
            <html:text property="newId" title="newId" styleClass="form-control" />
            <html:messages id="newId" property="newId">
                <bean:write name="newId"  filter="false"/>
            </html:messages>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label">Server IP</label>
        <div class="col-md-4">
            <html:text property="serverIP" title="server ip" styleClass="form-control" />
            <html:messages id="serverIP" property="serverIP">
                <bean:write name="serverIP"  filter="false"/>
            </html:messages>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label">Register Port</label>
        <div class="col-md-4">
            <html:text property="registerPort" title="register port" styleClass="form-control" />
            <html:messages id="registerPort" property="registerPort">
                <bean:write name="registerPort"  filter="false"/>
            </html:messages>

        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label">Heartbit Port</label>
        <div class="col-md-4">
            <html:text property="heartbitPort" title="heartbit port" styleClass="form-control" />
            <html:messages id="heartbitPort" property="heartbitPort">
                <bean:write name="heartbitPort"  filter="false"/>
            </html:messages>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label"> Current Registration </label>
        <div class="col-md-4">
            <html:text property="currentRegistration" title="current registration" styleClass="form-control" />
            <html:messages id="currentRegistration" property="currentRegistration">
                <bean:write name="currentRegistration"  filter="false"/>
            </html:messages>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label"> Max Registration </label>
        <div class="col-md-4">
            <html:text property="maxRegistration" title="max registration" styleClass="form-control" />
            <html:messages id="maxRegistration" property="maxRegistration">
                <bean:write name="maxRegistration"  filter="false"/>
            </html:messages>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label">Server Status</label>
        <div class="col-md-4">
            <div class="input-group">
                <div class="icheck-inline">
                    <label>
                        <html:radio property="serverStatus" value="1" styleClass="icheck"/> Active </label>
                    <label>
                        <html:radio property="serverStatus" value="0" styleClass="icheck"/> Down </label>
                </div>
            </div>
        </div>
    </div>
    <div class="form-group last">
        <label class="col-md-3 control-label"></label>
        <div class="col-md-4">
            <%                if (request.getSession(true).getAttribute("error") == "2") {
            %>
            <html:messages id="message" property="message" ><bean:write name="msg"  filter="false"/></html:messages>
            <bean:write name="ChannelServerForm" property="message" filter="false"/>
            <%}%>
        </div>
    </div>
</div>
<div class="form-actions">
    <div class="row">
        <div class="col-md-offset-3 col-md-4">
            <button type="submit" class="btn green" name="saveButton">Submit</button>
            <a href="channelServerListInfo.do" class="btn default">Cancel</a>
            <html:hidden property="id"/>
            <html:hidden property="action" styleId="actionid" />
        </div>
    </div>
</div>
<script type="text/javascript">
    var action = document.getElementById("actionid");
    if (action.value == "0") {
        action.value = "<%=Constants.ADD%>";
    }
</script>