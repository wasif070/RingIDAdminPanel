<%-- 
    Document   : m_appConstantsTable
    Created on : Feb 14, 2017, 12:09:51 PM
    Author     : rabby
--%>

<div class="form-body">
    <div class="form-group">
        <label class="col-md-3 control-label">Id</label>
        <div class="col-md-4">
            <html:text property="newId" title="newId" styleClass="form-control" />
            <html:messages id="newId" property="newId">
                <bean:write name="newId"  filter="false"/>
            </html:messages>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label">Relay Server IP <span style="color: red">*</span></label>
        <div class="col-md-4">
            <html:text property="serverIP" title="Relay Server IP" styleClass="form-control" />
            <html:messages id="serverIP" property="serverIP">
                <bean:write name="serverIP"  filter="false"/>
            </html:messages>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label">Relay Port</label>
        <div class="col-md-4">
            <html:text property="relayPort" title="Relay Port" styleClass="form-control" />
            <html:messages id="relayPort" property="relayPort">
                <bean:write name="relayPort"  filter="false"/>
            </html:messages>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label">Log Level</label>
        <div class="col-md-4">
            <html:text property="logLevel" title="Log Level" styleClass="form-control" />
            <html:messages id="logLevel" property="logLevel">
                <bean:write name="logLevel"  filter="false"/>
            </html:messages>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label">Active </label>
        <div class="col-md-4">
            <html:text property="active" title="Active" styleClass="form-control" />
            <html:messages id="active" property="active">
                <bean:write name="active"  filter="false"/>
            </html:messages>
        </div>
    </div>
    <div class="form-group last">
        <label class="col-md-3 control-label"></label>
        <div class="col-md-4">
            <%
                if (request.getSession(true).getAttribute("error") == "2") {
            %>  
            <html:messages id="message" property="message"><bean:write name="msg"  filter="false"/></html:messages>
            <bean:write name="RelayServerForm" property="message" filter="false"/>
            <%}%>
        </div>
    </div>
</div>
<div class="form-actions">
    <div class="row">
        <div class="col-md-offset-3 col-md-4">
            <button type="submit" class="btn green" name="saveButton">Submit</button>
            <a href="relayServerListInfo.do" class="btn default">Cancel</a>
            <html:hidden property="id"/>
            <input type="hidden" name="action" value="<%=request.getSession(true).getAttribute("formType")%>" />
        </div>
    </div>
</div>
