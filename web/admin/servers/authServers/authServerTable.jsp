<%-- 
    Document   : m_appConstantsTable
    Created on : Feb 14, 2017, 12:09:51 PM
    Author     : rabby
--%>

<div class="form-body">
    <div class="form-group">
        <label class="col-md-3 control-label">Id</label>
        <div class="col-md-4">
            <html:text property="newId" title="newId" styleClass="form-control" />
            <html:messages id="newId" property="newId">
                <bean:write name="newId"  filter="false"/>
            </html:messages>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label">Start Ring ID</label>
        <div class="col-md-4">
            <html:text property="startRingID" title="Start Ring ID" styleClass="form-control" />
            <html:messages id="startRingID" property="startRingID">
                <bean:write name="startRingID"  filter="false"/>
            </html:messages>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label">End Ring ID</label>
        <div class="col-md-4">
            <html:text property="endRingID" title="End Ring ID" styleClass="form-control" />
            <html:messages id="endRingID" property="endRingID">
                <bean:write name="endRingID"  filter="false"/>
            </html:messages>

        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label">Auth Server ID</label>
        <div class="col-md-4">
            <html:text property="serverID" title="Auth Server ID" styleClass="form-control" />
            <html:messages id="serverID" property="serverID">
                <bean:write name="serverID"  filter="false"/>
            </html:messages>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label">Auth Server IP <span style="color: red">*</span></label>
        <div class="col-md-4">
            <html:text property="serverIP" title="Auth Server IP" styleClass="form-control" />
            <html:messages id="serverIP" property="serverIP">
                <bean:write name="serverIP"  filter="false"/>
            </html:messages>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label">Start Port</label>
        <div class="col-md-4">
            <html:text property="startPort" title="Start Port" styleClass="form-control" />
            <html:messages id="startPort" property="startPort">
                <bean:write name="startPort"  filter="false"/>
            </html:messages>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label">End Port</label>
        <div class="col-md-4">
            <html:text property="endPort" title="End Port" styleClass="form-control" />
            <html:messages id="endPort" property="endPort">
                <bean:write name="endPort"  filter="false"/>
            </html:messages>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label">Relay Port</label>
        <div class="col-md-4">
            <html:text property="relayPort" title="Relay Port" styleClass="form-control" />
            <html:messages id="relayPort" property="relayPort">
                <bean:write name="relayPort"  filter="false"/>
            </html:messages>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label">Thread Pool Size</label>
        <div class="col-md-4">
            <html:text property="threadPoolSize" title="Thread Pool Size" styleClass="form-control" />
            <html:messages id="threadPoolSize" property="threadPoolSize">
                <bean:write name="threadPoolSize"  filter="false"/>
            </html:messages>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label">Log Level</label>
        <div class="col-md-4">
            <html:text property="logLevel" title="Log Level" styleClass="form-control" />
            <html:messages id="logLevel" property="logLevel">
                <bean:write name="logLevel"  filter="false"/>
            </html:messages>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label">Current Registration</label>
        <div class="col-md-4">
            <html:text property="currentRegistration" title="Current Registration" styleClass="form-control" />
            <html:messages id="currentRegistration" property="currentRegistration">
                <bean:write name="currentRegistration"  filter="false"/>
            </html:messages>
        </div>
    </div><div class="form-group">
        <label class="col-md-3 control-label">Live Sessions</label>
        <div class="col-md-4">
            <html:text property="liveSessions" title="Live Sessions" styleClass="form-control" />
            <html:messages id="liveSessions" property="liveSessions">
                <bean:write name="liveSessions"  filter="false"/>
            </html:messages>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label">Maximum Registration</label>
        <div class="col-md-4">
            <html:text property="maxRegistration" title="Maximum Registration" styleClass="form-control" />
            <html:messages id="maxRegistration" property="maxRegistration">
                <bean:write name="maxRegistration"  filter="false"/>
            </html:messages>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label">Last Packet ID</label>
        <div class="col-md-4">
            <html:text property="lastPacketID" title="Last Packet ID" styleClass="form-control" />
            <html:messages id="lastPacketID" property="lastPacketID">
                <bean:write name="lastPacketID"  filter="false"/>
            </html:messages>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label">Playing Role</label>
        <div class="col-md-4">
            <html:text property="playingRole" title="Playing Role" styleClass="form-control" />
            <html:messages id="playingRole" property="playingRole">
                <bean:write name="playingRole"  filter="false"/>
            </html:messages>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label">Server Status</label>
        <div class="col-md-4">
            <html:text property="serverStatus" title="Server Status" styleClass="form-control" />
            <html:messages id="serverStatus" property="serverStatus">
                <bean:write name="serverStatus"  filter="false"/>
            </html:messages>
        </div>
    </div>
    <div class="form-group last">
        <label class="col-md-3 control-label"></label>
        <div class="col-md-4">
            <%
                if (request.getSession(true).getAttribute("error") == "2") {
            %>  
            <html:messages id="message" property="message"><bean:write name="msg"  filter="false"/></html:messages>
            <bean:write name="AuthServerForm" property="message" filter="false"/>
            <%}%>
        </div>
    </div>
</div>
<div class="form-actions">
    <div class="row">
        <div class="col-md-offset-3 col-md-4">
            <button type="submit" class="btn green" name="saveButton">Submit</button>
            <a href="authServerListInfo.do" class="btn default">Cancel</a>
            <html:hidden property="id"/>
            <input type="hidden" name="action" value="<%=request.getSession(true).getAttribute("formType")%>" />
        </div>
    </div>
</div>
