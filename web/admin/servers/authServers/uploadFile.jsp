<%-- 
    Document   : uploadFile-new
    Created on : Jan 21, 2018, 12:51:59 PM
    Author     : mamun
--%>

<%@include file="/login/loginTimeExpiry.jsp"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@include  file="/shared/head.jsp" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Admin Panel | Upload Auth Server Info</title>
        <%@include file="/shared/css-list.jsp"%>
    </head>
    <body class="page-container-bg-solid">
        <div class="page-wrapper">
            <div class="page-wrapper-row">
                <div class="page-wrapper-top">
                    <div class="page-header">
                        <%@include  file="/shared/menu.jsp" %>
                    </div>
                </div>
            </div>
            <div class="page-wrapper-row full-height">
                <div class="page-wrapper-middle">
                    <div class="page-container">
                        <div class="page-content-wrapper">
                            <div class="page-content">
                                <div class="container">
                                    <ul class="page-breadcrumb breadcrumb">
                                        <li>
                                            <a href="welcome.do">Admin Panel</a>
                                            <span style="font-size: 14px; margin: 0 auto;" class="fa fa-angle-right"></span>
                                        </li>
                                        <li>
                                            <a href="javascript:;">Servers</a>
                                            <i style="font-size: 14px; margin: 0 auto;" class="fa fa-angle-right"></i>
                                        </li>
                                        <li>
                                            <a href="authServerListInfo.do">Auth Server</a>
                                            <i style="font-size: 14px; margin: 0 auto;" class="fa fa-angle-right"></i>
                                        </li>
                                        <li>
                                            <span id="features" data-class-active=".servers .authServer">Upload Auth Server</span>
                                        </li>
                                    </ul>
                                    <div class="page-content-inner">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="tabbable-line boxless tabbable-reversed">
                                                    <div class="tab-content" style="padding-top: 15px;">
                                                        <div class="portlet box green">
                                                            <div class="portlet-title">
                                                                <div class="caption">
                                                                    <i class="icon-cloud-upload"></i>
                                                                    Upload Auth Server CSV file format example </div>
                                                                <div class="tools">
                                                                    <a href="javascript:;" class="collapse"> </a>
                                                                </div>
                                                            </div>
                                                            <div class="portlet-body form">
                                                                <html:form action="uploadAuthServerFile" styleClass="form-horizontal" method="POST"  acceptCharset="UTF-8" enctype="multipart/form-data">
                                                                    <div class="form-body">
                                                                        <div class="table-scrollable">
                                                                            <table class="table table-striped table-bordered table-hover">
                                                                                <thead>
                                                                                    <tr>
                                                                                        <th class="text-center">ID</th>
                                                                                        <th class="text-center">Start Ring ID</th>
                                                                                        <th class="text-center">End Ring ID</th>
                                                                                        <th class="text-center">Server ID</th>
                                                                                        <th class="text-center">Server IP</th>
                                                                                        <th class="text-center">Start Port</th>
                                                                                        <th class="text-center">End Port</th>
                                                                                        <th class="text-center">Relay Port</th>
                                                                                        <th class="text-center">Thread Pool Size</th>
                                                                                        <th class="text-center">Log Level</th>
                                                                                        <th class="text-center">Current Registration</th> 
                                                                                        <th class="text-center">Live Sessions</th>
                                                                                        <th class="text-center">Maximum Registration</th>
                                                                                        <th class="text-center">Last Packet ID</th>
                                                                                        <th class="text-center">Playing Role</th>
                                                                                        <th class="text-center">Server Status</th>
                                                                                    </tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td class="text-center">1</td>
                                                                                        <td class="text-center">2110012500</td>
                                                                                        <td class="text-center">2110100000</td>
                                                                                        <td class="text-center">2</td>
                                                                                        <td class="text-center">38.127.68.52</td>
                                                                                        <td class="text-center">30400</td>
                                                                                        <td class="text-center">30500</td> 
                                                                                        <td class="text-center">500</td> 
                                                                                        <td class="text-center">100</td> 
                                                                                        <td class="text-center">ALL</td> 
                                                                                        <td class="text-center">16</td>
                                                                                        <td class="text-center">10</td>
                                                                                        <td class="text-center">10000</td>
                                                                                        <td class="text-center">16530357</td>
                                                                                        <td class="text-center">1</td>
                                                                                        <td class="text-center">1</td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label class="control-label col-md-3">File Name</label>
                                                                            <div class="col-md-9">
                                                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                                    <span class="btn green btn-file">
                                                                                        <span class="fileinput-new">
                                                                                            Select file
                                                                                        </span>
                                                                                        <span class="fileinput-exists">
                                                                                            Change 
                                                                                        </span>
                                                                                        <html:file property="theFile" accept=".csv" />
                                                                                    </span>
                                                                                    <span class="fileinput-filename"> </span> &nbsp;
                                                                                    <a href="javascript:;" class="close fileinput-exists" data-dismiss="fileinput"> </a>
                                                                                    <span id="msgSpan" style="display: inline-block;"><html:errors/> </span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-actions">
                                                                        <div class="row">
                                                                            <div class="col-md-offset-3 col-md-9">
                                                                                <button type="submit" class="btn green">
                                                                                    <i class="fa fa-upload"></i>
                                                                                    Upload File
                                                                                </button>
                                                                                <a href="authServerListInfo.do" class="btn default">Cancel</a>
                                                                            </div>
                                                                        </div>

                                                                    </div>
                                                                </html:form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <%@include file="/shared/footer.jsp"%>
        </div>
        <%@include file="/shared/script-list.jsp"%>
    </body>

</html>
