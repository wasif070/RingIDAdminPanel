<%-- 
    Document   : coinServerTable
    Created on : Jun 21, 2017, 11:30:56 AM
    Author     : Rabby
--%>

<div class="form-body">
    <div class="form-group">
        <label class="col-md-3 control-label">Id</label>
        <div class="col-md-4">
            <html:text property="newId" title="newId" styleClass="form-control" />
            <html:messages id="newId" property="newId">
                <bean:write name="newId"  filter="false"/>
            </html:messages>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label">Server Url <span style="color: red">*</span></label>
        <div class="col-md-4">
            <html:text property="serverUrl" title="Server Url" styleClass="form-control" />
            <html:messages id="serverUrl" property="serverUrl">
                <bean:write name="serverUrl"  filter="false"/>
            </html:messages>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label">ServerIP <span style="color: red">*</span></label>
        <div class="col-md-4">
            <html:text property="serverIP" title="Server ip" styleClass="form-control" />
            <html:messages id="serverIP" property="serverIP">
                <bean:write name="serverIP"  filter="false"/>
            </html:messages>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label">Server Port <span style="color: red">*</span></label>
        <div class="col-md-4">
            <html:text property="serverPort" title="Server port" styleClass="form-control" />
            <html:messages id="serverPort" property="serverPort">
                <bean:write name="serverPort"  filter="false"/>
            </html:messages>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label">Status</label>
        <div class="col-md-4">
            <html:select property="serverStatus" styleClass="form-control"> 
                <html:option  value="1">Active</html:option>
                <html:option  value="0">Inactive</html:option>
            </html:select>
            <html:messages id="serverStatus" property="serverStatus">
                <bean:write name="serverStatus"  filter="false"/>
            </html:messages>
        </div>
    </div>
    <div class="form-group last">
        <label class="col-md-3 control-label"></label>
        <div class="col-md-4">
            <%
                if (request.getSession(true).getAttribute("error") == "2") {
            %>
            <html:messages id="message" property="message"><bean:write name="msg"  filter="false"/></html:messages>
            <bean:write name="CoinServerForm" property="message" filter="false"/>
            <%}%>
        </div>
    </div>
</div>
<div class="form-actions">
    <div class="row">
        <div class="col-md-offset-3 col-md-4">
            <button type="submit" class="btn green" name="saveButton">Submit</button>
            <a href="coinServerListInfo.do" class="btn default">Cancel</a>
            <html:hidden property="id"/>
            <input type="hidden" name="action" value="<%=request.getSession(true).getAttribute("formType")%>" />
        </div>
    </div>
</div>
