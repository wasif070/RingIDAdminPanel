<%-- 
    Document   : m_appConstantsTable
    Created on : Feb 14, 2017, 12:09:51 PM
    Author     : rabby
--%>

<div class="form-body">
    <div class="form-group">
        <label class="col-md-3 control-label">Id</label>
        <div class="col-md-4">
            <html:text property="newId" title="newId" styleClass="form-control" />
            <html:messages id="newId" property="newId">
                <bean:write name="newId"  filter="false"/>
            </html:messages>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label">Chat Server IP <span style="color: red">*</span></label>
        <div class="col-md-4">
            <html:text property="serverIP" title="Chat Server IP" styleClass="form-control" />
            <html:messages id="serverIP" property="serverIP">
                <bean:write name="serverIP"  filter="false"/>
            </html:messages>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label">Register Port <span style="color: red">*</span></label>
        <div class="col-md-4">
            <html:text property="registerPort" title="Register Port"  styleClass="form-control" />
            <html:messages id="registerPort" property="registerPort">
                <bean:write name="registerPort"  filter="false"/>
            </html:messages>

        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label">Current Registration</label>
        <div class="col-md-4">
            <html:text property="currentRegistration" title="Current Registration"  styleClass="form-control" />
            <html:messages id="currentRegistration" property="currentRegistration">
                <bean:write name="currentRegistration"  filter="false"/>
            </html:messages>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label">Maximum Registration </label>
        <div class="col-md-4">
            <html:text property="maxRegistration" title="Maximum Registration"  styleClass="form-control" />
            <html:messages id="maxRegistration" property="maxRegistration">
                <bean:write name="maxRegistration"  filter="false"/>
            </html:messages>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label">Server Status </label>
        <div class="col-md-4">
            <html:text property="serverStatus" title="Server Status" styleClass="form-control" />
            <html:messages id="serverStatus" property="serverStatus">
                <bean:write name="serverStatus"  filter="false"/>
            </html:messages>
        </div>
    </div>
    <div class="form-group last">
        <label class="col-md-3 control-label"></label>
        <div class="col-md-4">
            <%
                if (request.getSession(true).getAttribute("error") == "2") {
            %>  
            <html:messages id="message" property="message"><bean:write name="msg"  filter="false"/></html:messages>
            <bean:write name="ChatServerForm" property="message" filter="false"/>
            <%}%>
        </div>
    </div>
</div>
<div class="form-actions">
    <div class="row">
        <div class="col-md-offset-3 col-md-4">
            <button type="submit" class="btn green" name="saveButton">Submit</button>
            <a href="chatServerListInfo.do" class="btn default">Cancel</a>
            <html:hidden property="id"/>
            <input type="hidden" name="action" value="<%=request.getSession(true).getAttribute("formType")%>" />
        </div>
    </div>
</div>
