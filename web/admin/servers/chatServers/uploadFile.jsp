<%@include file="/login/loginTimeExpiry.jsp"%>
<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ page import="java.util.*,com.ringid.admin.utils.Constants"%>
<%@include  file="/shared/head.jsp" %>

<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Admin Panel | Chat Server File Upload</title>
        <%@include file="/shared/css-list.jsp"%>
    </head>
    <body class="page-container-bg-solid">
        <div class="page-wrapper">
            <div class="page-wrapper-row">
                <div class="page-wrapper-top">
                    <div class="page-header">
                        <%@include  file="/shared/menu.jsp" %>
                    </div>
                </div>
            </div>
            <div class="page-wrapper-row full-height">
                <div class="page-wrapper-middle">
                    <div class="page-container">
                        <div class="page-content-wrapper">
                            <div class="page-content">
                                <div class="container">
                                    <ul class="page-breadcrumb breadcrumb">
                                        <li>
                                            <a href="home/index.jsp">Admin Panel</a>
                                            <i class="fa fa-circle"></i>
                                        </li>
                                        <li>
                                            <a href="javascript:;">Servers</a>
                                            <i class="fa fa-circle"></i>
                                        </li>
                                        <li>
                                            <a href="chatServerListInfo.do">Chat Server</a>
                                            <i class="fa fa-circle"></i>
                                        </li>
                                        <li>
                                            <span id="features" data-class-active=".servers .chatServer">Upload File</span>
                                        </li>
                                    </ul>
                                    <div class="page-content-inner">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="tabbable-line boxless tabbable-reversed">
                                                    <div class="tab-content" style="padding-top: 15px;">
                                                        <div class="portlet box green">
                                                            <div class="portlet-title">
                                                                <div class="caption">
                                                                    <i class="icon-cloud-upload"></i>
                                                                    Upload Chat Server CSV file format example
                                                                </div>
                                                                <div class="tools">
                                                                    <a href="javascript:;" class="collapse"> </a>
                                                                </div>
                                                            </div>
                                                            <div class="portlet-body form">
                                                                <html:form action="/uploadChatServerFile" styleClass="form-horizontal" method="POST"  acceptCharset="UTF-8" enctype="multipart/form-data">
                                                                    <div class="form-body" >
                                                                        <div class="table-scrollable">
                                                                            <table class="table table-striped table-bordered table-hover">
                                                                                <thead>
                                                                                    <tr>
                                                                                        <th align="center">Server IP</th>
                                                                                        <th align="center">Register Port</th>
                                                                                        <th align="center">Current Registration</th> 
                                                                                        <th align="center">Maximum Registration</th>
                                                                                        <th align="center">Server Status</th>
                                                                                    </tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td align="center">38.127.68.54</td>
                                                                                        <td align="center">1500</td>
                                                                                        <td align="center">2</td> 
                                                                                        <td align="center">10000</td> 
                                                                                        <td align="center">1</td> 
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label class="control-label col-md-3">File Name</label>
                                                                            <div class="col-md-9">
                                                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                                    <span class="btn green btn-file">
                                                                                        <span class="fileinput-new"> Select file </span>
                                                                                        <span class="fileinput-exists"> Change </span>
                                                                                        <html:file property="theFile" accept=".csv"/>
                                                                                    </span>
                                                                                    <span class="fileinput-filename"> </span> &nbsp;
                                                                                    <a href="javascript:;" class="close fileinput-exists" data-dismiss="fileinput"> </a>
                                                                                    <span id="msgSpan" style="display: inline-block;"><html:errors/> </span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-actions">
                                                                        <div class="row">
                                                                            <div class="col-md-offset-3 col-md-9">
                                                                                <button type="submit" class="btn green">
                                                                                    <i class="fa fa-upload"></i>Upload File
                                                                                </button>
                                                                                <a href="chatServerListInfo.do" class="btn default">Cancel</a>
                                                                            </div>
                                                                        </div>

                                                                    </div>
                                                                </html:form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <%@include file="/shared/footer.jsp"%>
        </div>
        <%@include file="/shared/script-list.jsp"%>
    </body>
</html>
