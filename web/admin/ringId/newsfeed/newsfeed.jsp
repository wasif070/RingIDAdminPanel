<%-- 
    Document   : newsfeed
    Created on : Sep 24, 2017, 11:38:32 AM
    Author     : mamun
--%>

<%@include file="/login/loginTimeExpiry.jsp"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.google.gson.Gson"%>
<%@page import="com.google.gson.JsonParser"%>
<%@page import="com.google.gson.JsonElement"%>
<%@page import="com.google.gson.JsonArray"%>
<%@page import="java.util.List"%>
<%@page import="org.ringid.newsfeeds.FeedDTO"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@include  file="/shared/head.jsp" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Admin Panel | News Feed </title>
        <%@include file="/shared/css-list.jsp"%>
    </head>
    <body class="page-container-bg-solid">
        <div class="page-wrapper">
            <div class="page-wrapper-row">
                <div class="page-wrapper-top">
                    <div class="page-header">
                        <%@include  file="/shared/menu.jsp" %>
                        <%
                            int TOTAL_RECORDS = 0, TOTAL_PAGES = 0, CURRENT_PAGE_NO = 0, STARTING_RECORD_NO = 0, ENDING_RECORD_NO = 0;
                            String disabledPrev = "", disabledNext = "", message = "", userBasicInfo = "", userBasicInfoHashMap = "";
                            if (request.getAttribute("message") != null) {
                                message = request.getAttribute("message").toString();
                            }
                            if (request.getAttribute(Constants.TOTAL_RECORDS) != null) {
                                TOTAL_RECORDS = Integer.valueOf(request.getAttribute(Constants.TOTAL_RECORDS).toString());
                            }
                            if (request.getAttribute(Constants.TOTAL_PAGES) != null) {
                                TOTAL_PAGES = Integer.valueOf(request.getAttribute(Constants.TOTAL_PAGES).toString());
                            }
                            if (request.getAttribute(Constants.CURRENT_PAGE_NO) != null) {
                                CURRENT_PAGE_NO = Integer.valueOf(request.getAttribute(Constants.CURRENT_PAGE_NO).toString());
                            }
                            if (request.getAttribute(Constants.STARTING_RECORD_NO) != null) {
                                STARTING_RECORD_NO = Integer.valueOf(request.getAttribute(Constants.STARTING_RECORD_NO).toString());
                            }
                            if (request.getAttribute(Constants.ENDING_RECORD_NO) != null) {
                                ENDING_RECORD_NO = Integer.valueOf(request.getAttribute(Constants.ENDING_RECORD_NO).toString());
                            }
                            if (STARTING_RECORD_NO <= 0) {
                                disabledPrev = "disabled";
                                STARTING_RECORD_NO = 0;
                            }
                            if (ENDING_RECORD_NO >= TOTAL_RECORDS) {
                                disabledNext = "disabled";
                                ENDING_RECORD_NO = TOTAL_RECORDS;
                            }
                            JsonArray jsonArray = new JsonArray();
                        %>
                    </div>
                </div>
            </div>
            <div class="page-wrapper-row full-height">
                <div class="page-wrapper-middle">
                    <div class="page-container">
                        <div class="page-content-wrapper">
                            <div class="page-content">
                                <div class="container">
                                    <ul class="page-breadcrumb breadcrumb">
                                        <li>
                                            <a href="welcome.do">Admin Panel</a>
                                            <span style="font-size: 14px; margin: 0 auto;" class="fa fa-angle-right"></span>
                                        </li>
                                        <li>
                                            <a href="javascript:;">Manage Operations</a>
                                            <span style="font-size: 14px; margin: 0 auto;" class="fa fa-angle-right"></span>
                                        </li>
                                        <li>
                                            <span id="features" data-class-active=".manageOperations .newsFeed">News Feed </span>
                                        </li>
                                    </ul>
                                    <%if (message.length() > 0) {%>
                                    <div id="prefix_661213402591" class="custom-alerts alert alert-success fade in">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                        <strong> <%=message%> </strong>
                                    </div>
                                    <%}%>
                                    <div class="page-content-inner">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="tabbable-line boxless tabbable-reversed">
                                                    <div class="tab-content" style="padding-top: 15px;">
                                                        <div class="portlet box green">
                                                            <div class="portlet-title">
                                                                <div class="caption">
                                                                    <i class="fa fa-list-ul"></i> News Feed List Info </div>
                                                                <div class="tools">
                                                                    <a href="javascript:;" class="collapse"> </a>
                                                                </div>
                                                            </div>
                                                            <div class="portlet-body form">
                                                                <html:form action="newsfeed" styleClass="form-horizontal" method="POST"  acceptCharset="UTF-8">
                                                                    <div class="form-body">
                                                                        <div class="form-group">
                                                                            <div class="col-md-6 col-sm-6">
                                                                                <label>
                                                                                    <html:select styleClass="bs-select form-control input-sm input-xsmall input-inline" property="recordPerPage" onchange="javascript: submitform(1, 1, this);">
                                                                                        <html:option value="10">10</html:option>
                                                                                        <html:option value="25">25</html:option>
                                                                                        <html:option value="50">50</html:option>
                                                                                        <html:option value="100">100</html:option>
                                                                                        <html:option value="200">200</html:option>
                                                                                    </html:select>
                                                                                    records</label>
                                                                            </div>
                                                                            <div class="col-md-6 col-sm-6">
                                                                                <div class="input-group" style="float: right;">
                                                                                    <input name="id" type="text" value="<bean:write name="NewsFeedForm" property="id"/>" class="form-control input-sm input-small" placeholder="ringId or userId">
                                                                                    <span>
                                                                                        <button class="btn green-soft uppercase bold input-sm" type="submit"><i class="icon-magnifier"></i></button>
                                                                                        <input name="action" type="hidden" value="<%=Constants.SEARCH%>" />
                                                                                        <html:hidden property="totalDataSize" />
                                                                                    </span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="table-scrollable">
                                                                            <table class="table table-striped table-bordered table-hover">
                                                                                <thead>
                                                                                    <tr>
                                                                                        <th class="text-center">
                                                                                            <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                                                                                <input type="checkbox" onchange="checkAllAndSetWeight(this)" class="group-checkable" />
                                                                                                <span></span>
                                                                                            </label>
                                                                                        </th>
                                                                                        <th class="text-center">User Id</th>
                                                                                        <th class="text-center">Content</th>
                                                                                        <th class="text-center">Type</th>
                                                                                        <th class="text-center">HighlightsFeedWeight</th>
                                                                                    </tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                    <%
                                                                                        List<FeedDTO> data1 = (List<FeedDTO>) session.getAttribute(Constants.DATA_ROWS);
                                                                                        session.removeAttribute(Constants.DATA_ROWS);
                                                                                        Gson gson = new Gson();
                                                                                        if (session.getAttribute("userBasicInfoDTO") != null) {
                                                                                            userBasicInfo = gson.toJson(session.getAttribute("userBasicInfoDTO"));
                                                                                            session.removeAttribute("userBasicInfoDTO");
                                                                                        }
                                                                                        if (session.getAttribute("userBasicInfoDTOHashMap") != null) {
                                                                                            userBasicInfoHashMap = gson.toJson(session.getAttribute("userBasicInfoDTOHashMap"));
                                                                                            session.removeAttribute("userBasicInfoDTOHashMap");
                                                                                        }
                                                                                        if (data1 != null && data1.size() > 0) {
                                                                                            for (int i = STARTING_RECORD_NO, k = 0; i < ENDING_RECORD_NO; i++, k++) {
                                                                                                FeedDTO dto = (FeedDTO) data1.get(i);
                                                                                                JsonElement jsonElement = new JsonParser().parse(gson.toJson(dto));
                                                                                                jsonArray.add(jsonElement);
                                                                                    %>
                                                                                    <tr>
                                                                                        <td class="text-center">
                                                                                            <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                                                                                <input name="selectedIDs" type="checkbox" class="checkboxes" value="<%=String.valueOf(dto.getFeedId())%>" onclick="setfeedIdAndWght(<%=k%>)" />
                                                                                                <span></span>
                                                                                            </label>
                                                                                        </td>
                                                                                        <td class="text-center">
                                                                                            <%=dto.getWallOwnerId()%>
                                                                                        </td>
                                                                                        <td class="text-center" style='word-wrap: break-word;max-width: 320px'>
                                                                                            <a href="#large" data-toggle="modal" onclick="previewfeed(<%=k%>)"> 
                                                                                                <%= dto.getStatus() == null || dto.getStatus().trim().length() == 0 ? "not available" : dto.getStatus()%>
                                                                                            </a>
                                                                                        </td>
                                                                                        <td class="text-center">
                                                                                            <%
                                                                                                String contentType = "";
                                                                                                switch (dto.getContentType()) {
                                                                                                    case 1:
                                                                                                        contentType = "TEXT";
                                                                                                        break;
                                                                                                    case 2:
                                                                                                        contentType = "SINGLE IMAGE";
                                                                                                        break;
                                                                                                    case 3:
                                                                                                        contentType = "SINGLE IMAGE WITH ALBUM";
                                                                                                        break;
                                                                                                    case 4:
                                                                                                        contentType = "MULTIPLE IMAGE WITH ALBUM";
                                                                                                        break;
                                                                                                    case 5:
                                                                                                        contentType = "SINGLE AUDIO";
                                                                                                        break;
                                                                                                    case 6:
                                                                                                        contentType = "SINGLE AUDIO WITH ALBUM";
                                                                                                        break;
                                                                                                    case 7:
                                                                                                        contentType = "MULTIPLE AUDIO WITH ALBUM";
                                                                                                        break;
                                                                                                    case 8:
                                                                                                        contentType = "SINGLE VIDEO";
                                                                                                        break;
                                                                                                    case 9:
                                                                                                        contentType = "SINGLE VIDEO WITH ALBUM";
                                                                                                        break;
                                                                                                    case 10:
                                                                                                        contentType = "MULTIPLE VIDEO WITH ALBUM";
                                                                                                        break;
                                                                                                    default:
                                                                                                        contentType = "UNKNOWN";
                                                                                                }
                                                                                            %>
                                                                                            <%=contentType%>
                                                                                        </td>
                                                                                        <td class="text-center">
                                                                                            <a name="anchrEdit" href="javascript:toggleWeightText(1, 0, <%=k%>)" class="" title="Change"><span class="fa fa-edit"></span></a>
                                                                                            <input class="form-control input-inline input-small input-xsmall" name="wght" style="display: none;" onchange="setfeedIdAndWght(<%=k%>)"/>
                                                                                            &nbsp;<a name="anchrRemove" style="display: none;" href="javascript:toggleWeightText(0, 1, <%=k%>)" class="" title="Delete" ><span class="fa fa-remove"></span></a>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <% }
                                                                                        }
                                                                                    %>
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                        <div class="row">
                                                                            <div class="col-md-5 col-sm-12">
                                                                                <div>
                                                                                    Showing <%=(STARTING_RECORD_NO + 1)%> to <%=ENDING_RECORD_NO%> of <%=TOTAL_RECORDS%> entries
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-7 col-sm-12">
                                                                                <div style="float: right;">
                                                                                    <div> 
                                                                                        Page <a href="javascript: submitform(2, <%=(CURRENT_PAGE_NO - 1)%>, this);" class="btn btn-sm default prev <%=disabledPrev%>"><i class="fa fa-angle-left"></i></a>
                                                                                        <input name="pageNo" class="form-control input-sm input-inline input-mini" maxlenght="5" style="text-align:center; margin: 0 5px;" type="text" value="<%=CURRENT_PAGE_NO%>" onchange="javascript: submitform(2, 0, this);">
                                                                                        <a href="javascript: submitform(2, <%=(CURRENT_PAGE_NO + 1)%>, this);" class="btn btn-sm default next <%=disabledNext%>"><i class="fa fa-angle-right"></i></a> of <span><%=TOTAL_PAGES%></span>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <!-- /.modal -->
                                                                        <div class="modal fade bs-modal-lg" id="large" tabindex="-1" role="dialog" aria-hidden="true">
                                                                            <div class="modal-dialog modal-lg">
                                                                                <div class="modal-content">
                                                                                    <div class="modal-header">
                                                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                                                        <h4 class="modal-title">User NewsFeed Details</h4>
                                                                                    </div>
                                                                                    <div class="modal-body">
                                                                                        <table class="table table-striped table-bordered table-hover order-column">
                                                                                            <tbody>
                                                                                                <tr>
                                                                                                    <th style="text-align: right; ">
                                                                                                        Owner Name
                                                                                                    </th>
                                                                                                    <td style="text-align: left; ">
                                                                                                        <div id="fn">

                                                                                                        </div>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <th style="text-align: right; ">
                                                                                                        NewsFeed ID
                                                                                                    </th>
                                                                                                    <td style="text-align: left; ">
                                                                                                        <div id="feedid">

                                                                                                        </div>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <th style="text-align: right; ">
                                                                                                        Status
                                                                                                    </th>
                                                                                                    <td style="text-align: left; ">
                                                                                                        <div id="feedstatus" style="word-wrap: break-word;max-width: 300px;">

                                                                                                        </div>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <th style="text-align: right; ">
                                                                                                        Total Like
                                                                                                    </th>
                                                                                                    <td style="text-align: left; ">
                                                                                                        <div id="feedtotallike">

                                                                                                        </div>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <th style="text-align: right; ">
                                                                                                        Content Type
                                                                                                    </th>
                                                                                                    <td style="text-align: left; ">
                                                                                                        <div id="contenttype">

                                                                                                        </div>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr id="lnkContent" style="display: none;">
                                                                                                    <th style="text-align: right; vertical-align: middle;">
                                                                                                        Link Content
                                                                                                    </th>
                                                                                                    <td>
                                                                                                        <table class="table table-striped table-bordered table-hover order-column">
                                                                                                            <tr>
                                                                                                                <th style="text-align: right; ">
                                                                                                                    Link Title
                                                                                                                </th>
                                                                                                                <td style="text-align: left; ">
                                                                                                                    <div id="lnkTitle">

                                                                                                                    </div>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <th style="text-align: right; ">
                                                                                                                    Link URL
                                                                                                                </th>
                                                                                                                <td style="text-align: left; ">
                                                                                                                    <div id="lnkURL">

                                                                                                                    </div>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <th style="text-align: right; ">
                                                                                                                    Description
                                                                                                                </th>
                                                                                                                <td style="text-align: left; ">
                                                                                                                    <div id="lnkDesc">

                                                                                                                    </div>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <th style="text-align: right; ">
                                                                                                                    Image URL
                                                                                                                </th>
                                                                                                                <td style="text-align: left; ">
                                                                                                                    <div id="lnkImageURL">

                                                                                                                    </div>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr id="palbumid" >
                                                                                                                <th style="text-align: right; ">
                                                                                                                    Link Domain
                                                                                                                </th>
                                                                                                                <td style="text-align: left; ">
                                                                                                                    <div id="lnkDomain">

                                                                                                                    </div>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </table>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr id="albumid">
                                                                                                    <th style="text-align: right; ">
                                                                                                        Album ID
                                                                                                    </th>
                                                                                                    <td style="text-align: left; ">
                                                                                                        <div id="albid">

                                                                                                        </div>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr id="albumname">
                                                                                                    <th style="text-align: right; ">
                                                                                                        Album Name
                                                                                                    </th>
                                                                                                    <td style="text-align: left; ">
                                                                                                        <div id="albname">

                                                                                                        </div>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr id="imgdto" style="display: none;">
                                                                                                    <th style="text-align: right; vertical-align: middle;">
                                                                                                        Image
                                                                                                    </th>
                                                                                                    <td style="text-align: center; vertical-align: middle;">
                                                                                                        <div id="imgcontainer" class="cbp-popup-lightbox cbp-popup-transitionend cbp-popup-ready" style="width: 500px; text-align: center;">
                                                                                                            <div class="cbp-popup-content">
                                                                                                                <div class="cbp-popup-lightbox-figure" data-action="close">
                                                                                                                    <img style="height: 250px; width: 350px" src="" id="imgurl" class="cbp-popup-lightbox-img" />
                                                                                                                    <div class="cbp-popup-lightbox-bottom">
                                                                                                                        <div class="cbp-popup-lightbox-title">
                                                                                                                            <div style="display: inline-block;" title="view">
                                                                                                                                <i class="fa fa-eye"></i>
                                                                                                                                <span id="imgview">0</span>
                                                                                                                            </div>
                                                                                                                            <div style="display: inline-block; margin-left: 5px;" title="like">
                                                                                                                                <i class="fa fa-thumbs-up"></i>
                                                                                                                                <span id="imglike">0</span>
                                                                                                                            </div>
                                                                                                                            <div style="display: inline-block; margin-left: 5px;" title="comment">
                                                                                                                                <i class="fa fa-comment"></i>
                                                                                                                                <span id="imgcomment">0</span>
                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                        <div class="cbp-popup-lightbox-counter">
                                                                                                                            <span id="imgcurrent">1</span> of <span id="imgtotal">1</span>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="cbp-popup-loadingBox" style="margin-top: 70px;">
                                                                                                            </div>
                                                                                                            <div class="cbp-popup-navigation-wrap">
                                                                                                                <div class="cbp-popup-navigation">
                                                                                                                    <div id="imgnext" class="cbp-popup-next" title="next image" data-action="next" style="display: inline-block; margin-right: 225px; margin-top: 400px;">
                                                                                                                    </div>
                                                                                                                    <div id="imgprevious" class="cbp-popup-prev" title="previous image" data-action="prev" style="display: inline-block; margin-left: 170px; margin-top: 400px;">
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr id="media_video_dto" style="display: none;">
                                                                                                    <th style="text-align: right; vertical-align: middle;">
                                                                                                        Video
                                                                                                    </th>
                                                                                                    <td style="text-align: center; vertical-align: middle;">
                                                                                                        <div id="jp_container_1" class="jp-video jp-video-270p" role="application" aria-label="media player">
                                                                                                            <div class="jp-type-playlist">
                                                                                                                <div id="jquery_jplayer_1" class="jp-jplayer"></div>
                                                                                                                <div class="jp-gui">
                                                                                                                    <div class="jp-video-play">
                                                                                                                        <button class="jp-video-play-icon" role="button" tabindex="0">play</button>
                                                                                                                    </div>
                                                                                                                    <div class="jp-interface">
                                                                                                                        <div class="jp-progress">
                                                                                                                            <div class="jp-seek-bar">
                                                                                                                                <div class="jp-play-bar"></div>
                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                        <div class="jp-current-time" role="timer" aria-label="time">&nbsp;</div>
                                                                                                                        <div class="jp-duration" role="timer" aria-label="duration">&nbsp;</div>
                                                                                                                        <div class="jp-controls-holder">
                                                                                                                            <div class="jp-controls">
                                                                                                                                <button class="jp-previous" role="button" tabindex="0">previous</button>
                                                                                                                                <button class="jp-play" role="button" tabindex="0">play</button>
                                                                                                                                <button class="jp-next" role="button" tabindex="0">next</button>
                                                                                                                                <button class="jp-stop" role="button" tabindex="0">stop</button>
                                                                                                                            </div>
                                                                                                                            <div class="jp-volume-controls">
                                                                                                                                <button class="jp-mute" role="button" tabindex="0">mute</button>
                                                                                                                                <button class="jp-volume-max" role="button" tabindex="0">max volume</button>
                                                                                                                                <div class="jp-volume-bar">
                                                                                                                                    <div class="jp-volume-bar-value"></div>
                                                                                                                                </div>
                                                                                                                            </div>
                                                                                                                            <div class="jp-toggles">
                                                                                                                                <button class="jp-repeat" role="button" tabindex="0">repeat</button>
                                                                                                                                <button class="jp-shuffle" role="button" tabindex="0">shuffle</button>
                                                                                                                                <button class="jp-full-screen" role="button" tabindex="0">full screen</button>
                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                        <div class="jp-details">
                                                                                                                            <div class="jp-title" aria-label="title">&nbsp;</div>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                                <div class="jp-playlist">
                                                                                                                    <ul>
                                                                                                                        <!-- The method Playlist.displayPlaylist() uses this unordered list -->
                                                                                                                        <li>&nbsp;</li>
                                                                                                                    </ul>
                                                                                                                </div>
                                                                                                                <div class="jp-no-solution">
                                                                                                                    <span>Update Required</span>
                                                                                                                    To play the media you will need to either update your browser to a recent version or update your <a href="http://get.adobe.com/flashplayer/" target="_blank">Flash plugin</a>.
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr id="media_audio_dto" style="display: none;">
                                                                                                    <th style="text-align: right; vertical-align: middle;">
                                                                                                        Audio
                                                                                                    </th>
                                                                                                    <td style="text-align: center; vertical-align: middle;">
                                                                                                        <div id="jquery_jplayer_2" class="jp-jplayer"></div>
                                                                                                        <div id="jp_container_2" class="jp-audio" role="application" aria-label="media player">
                                                                                                            <div class="jp-type-playlist">
                                                                                                                <div class="jp-gui jp-interface">
                                                                                                                    <div class="jp-controls">
                                                                                                                        <button class="jp-previous" role="button" tabindex="0">previous</button>
                                                                                                                        <button class="jp-play" role="button" tabindex="0">play</button>
                                                                                                                        <button class="jp-next" role="button" tabindex="0">next</button>
                                                                                                                        <button class="jp-stop" role="button" tabindex="0">stop</button>
                                                                                                                    </div>
                                                                                                                    <div class="jp-progress">
                                                                                                                        <div class="jp-seek-bar">
                                                                                                                            <div class="jp-play-bar"></div>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <div class="jp-volume-controls">
                                                                                                                        <button class="jp-mute" role="button" tabindex="0">mute</button>
                                                                                                                        <button class="jp-volume-max" role="button" tabindex="0">max volume</button>
                                                                                                                        <div class="jp-volume-bar">
                                                                                                                            <div class="jp-volume-bar-value"></div>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <div class="jp-time-holder">
                                                                                                                        <div class="jp-current-time" role="timer" aria-label="time">&nbsp;</div>
                                                                                                                        <div class="jp-duration" role="timer" aria-label="duration">&nbsp;</div>
                                                                                                                    </div>
                                                                                                                    <div class="jp-toggles">
                                                                                                                        <button class="jp-repeat" role="button" tabindex="0">repeat</button>
                                                                                                                        <button class="jp-shuffle" role="button" tabindex="0">shuffle</button>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                                <div class="jp-playlist">
                                                                                                                    <ul>
                                                                                                                        <li>&nbsp;</li>
                                                                                                                    </ul>
                                                                                                                </div>
                                                                                                                <div class="jp-no-solution">
                                                                                                                    <span>Update Required</span>
                                                                                                                    To play the media you will need to either update your browser to a recent version or update your <a href="http://get.adobe.com/flashplayer/" target="_blank">Flash plugin</a>.
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr id="sharecontent" style="display: none;">
                                                                                                    <th style="text-align: right; vertical-align: middle;">
                                                                                                        Share Content
                                                                                                    </th>
                                                                                                    <td>
                                                                                                        <table class="table table-striped table-bordered table-hover order-column">
                                                                                                            <tr>
                                                                                                                <th style="text-align: right; ">
                                                                                                                    Owner Name
                                                                                                                </th>
                                                                                                                <td style="text-align: left; ">
                                                                                                                    <div id="pfn">

                                                                                                                    </div>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <th style="text-align: right; ">
                                                                                                                    Feed ID
                                                                                                                </th>
                                                                                                                <td style="text-align: left; ">
                                                                                                                    <div id="pfeedid">

                                                                                                                    </div>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <th style="text-align: right; ">
                                                                                                                    Status
                                                                                                                </th>
                                                                                                                <td style="text-align: left; ">
                                                                                                                    <div id="pfeedstatus">

                                                                                                                    </div>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                                <th style="text-align: right; ">
                                                                                                                    Content Type
                                                                                                                </th>
                                                                                                                <td style="text-align: left; ">
                                                                                                                    <div id="pcontenttype">

                                                                                                                    </div>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr id="palbumid" >
                                                                                                                <th style="text-align: right; ">
                                                                                                                    Album ID
                                                                                                                </th>
                                                                                                                <td style="text-align: left; ">
                                                                                                                    <div id="palbid">

                                                                                                                    </div>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr id="palbumname">
                                                                                                                <th style="text-align: right; ">
                                                                                                                    Album Name
                                                                                                                </th>
                                                                                                                <td style="text-align: left; ">
                                                                                                                    <div id="palbname">

                                                                                                                    </div>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr id="pimgdto" style="display: none;">
                                                                                                                <th style="text-align: right; vertical-align: middle;">
                                                                                                                    Image
                                                                                                                </th>
                                                                                                                <td style="text-align: center; vertical-align: middle;">
                                                                                                                    <div id="pimgcontainer" class="cbp-popup-lightbox cbp-popup-transitionend cbp-popup-ready" style="width: 500px; text-align: center;">
                                                                                                                        <div class="cbp-popup-content">
                                                                                                                            <div class="cbp-popup-lightbox-figure" data-action="close">
                                                                                                                                <img style="height: 250px; width: 350px" src="" id="pimgurl" class="cbp-popup-lightbox-img" />
                                                                                                                                <div class="cbp-popup-lightbox-bottom">
                                                                                                                                    <div class="cbp-popup-lightbox-title">
                                                                                                                                        <div style="display: inline-block;" title="view">
                                                                                                                                            <i class="fa fa-eye"></i>
                                                                                                                                            <span id="pimgview">0</span>
                                                                                                                                        </div>
                                                                                                                                        <div style="display: inline-block; margin-left: 5px;" title="like">
                                                                                                                                            <i class="fa fa-thumbs-up"></i>
                                                                                                                                            <span id="pimglike">0</span>
                                                                                                                                        </div>
                                                                                                                                        <div style="display: inline-block; margin-left: 5px;" title="comment">
                                                                                                                                            <i class="fa fa-comment"></i>
                                                                                                                                            <span id="pimgcomment">0</span>
                                                                                                                                        </div>
                                                                                                                                    </div>
                                                                                                                                    <div class="cbp-popup-lightbox-counter">
                                                                                                                                        <span id="pimgcurrent">1</span> of <span id="pimgtotal">1</span>
                                                                                                                                    </div>
                                                                                                                                </div>
                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                        <div class="cbp-popup-loadingBox" style="margin-top: 125px; margin-left: 60px;">
                                                                                                                        </div>
                                                                                                                        <div class="cbp-popup-navigation-wrap">
                                                                                                                            <div class="cbp-popup-navigation">
                                                                                                                                <div id="pimgnext" class="cbp-popup-next" title="next image" data-action="next" style="display: inline-block; margin-right: 115px; margin-top: 485px;">
                                                                                                                                </div>
                                                                                                                                <div id="pimgprevious" class="cbp-popup-prev" title="previous image" data-action="prev" style="display: inline-block; margin-left: 280px; margin-top: 485px;">
                                                                                                                                </div>
                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr id="pmedia_video_dto" style="display: none;">
                                                                                                                <th style="text-align: right; vertical-align: middle;">
                                                                                                                    Video
                                                                                                                </th>
                                                                                                                <td style="text-align: center; vertical-align: middle;">
                                                                                                                    <div id="pjp_container_1" class="jp-video jp-video-270p" role="application" aria-label="media player">
                                                                                                                        <div class="jp-type-playlist">
                                                                                                                            <div id="pjquery_jplayer_1" class="jp-jplayer"></div>
                                                                                                                            <div class="jp-gui">
                                                                                                                                <div class="jp-video-play">
                                                                                                                                    <button class="jp-video-play-icon" role="button" tabindex="0">play</button>
                                                                                                                                </div>
                                                                                                                                <div class="jp-interface">
                                                                                                                                    <div class="jp-progress">
                                                                                                                                        <div class="jp-seek-bar">
                                                                                                                                            <div class="jp-play-bar"></div>
                                                                                                                                        </div>
                                                                                                                                    </div>
                                                                                                                                    <div class="jp-current-time" role="timer" aria-label="time">&nbsp;</div>
                                                                                                                                    <div class="jp-duration" role="timer" aria-label="duration">&nbsp;</div>
                                                                                                                                    <div class="jp-controls-holder">
                                                                                                                                        <div class="jp-controls">
                                                                                                                                            <button class="jp-previous" role="button" tabindex="0">previous</button>
                                                                                                                                            <button class="jp-play" role="button" tabindex="0">play</button>
                                                                                                                                            <button class="jp-next" role="button" tabindex="0">next</button>
                                                                                                                                            <button class="jp-stop" role="button" tabindex="0">stop</button>
                                                                                                                                        </div>
                                                                                                                                        <div class="jp-volume-controls">
                                                                                                                                            <button class="jp-mute" role="button" tabindex="0">mute</button>
                                                                                                                                            <button class="jp-volume-max" role="button" tabindex="0">max volume</button>
                                                                                                                                            <div class="jp-volume-bar">
                                                                                                                                                <div class="jp-volume-bar-value"></div>
                                                                                                                                            </div>
                                                                                                                                        </div>
                                                                                                                                        <div class="jp-toggles">
                                                                                                                                            <button class="jp-repeat" role="button" tabindex="0">repeat</button>
                                                                                                                                            <button class="jp-shuffle" role="button" tabindex="0">shuffle</button>
                                                                                                                                            <button class="jp-full-screen" role="button" tabindex="0">full screen</button>
                                                                                                                                        </div>
                                                                                                                                    </div>
                                                                                                                                    <div class="jp-details">
                                                                                                                                        <div class="jp-title" aria-label="title">&nbsp;</div>
                                                                                                                                    </div>
                                                                                                                                </div>
                                                                                                                            </div>
                                                                                                                            <div class="jp-playlist">
                                                                                                                                <ul>
                                                                                                                                    <!-- The method Playlist.displayPlaylist() uses this unordered list -->
                                                                                                                                    <li>&nbsp;</li>
                                                                                                                                </ul>
                                                                                                                            </div>
                                                                                                                            <div class="jp-no-solution">
                                                                                                                                <span>Update Required</span>
                                                                                                                                To play the media you will need to either update your browser to a recent version or update your <a href="http://get.adobe.com/flashplayer/" target="_blank">Flash plugin</a>.
                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                            <tr id="pmedia_audio_dto" style="display: none;">
                                                                                                                <th style="text-align: right; vertical-align: middle;">
                                                                                                                    Audio
                                                                                                                </th>
                                                                                                                <td style="text-align: center; vertical-align: middle;">
                                                                                                                    <div id="pjquery_jplayer_2" class="jp-jplayer"></div>
                                                                                                                    <div id="pjp_container_2" class="jp-audio" role="application" aria-label="media player">
                                                                                                                        <div class="jp-type-playlist">
                                                                                                                            <div class="jp-gui jp-interface">
                                                                                                                                <div class="jp-controls">
                                                                                                                                    <button class="jp-previous" role="button" tabindex="0">previous</button>
                                                                                                                                    <button class="jp-play" role="button" tabindex="0">play</button>
                                                                                                                                    <button class="jp-next" role="button" tabindex="0">next</button>
                                                                                                                                    <button class="jp-stop" role="button" tabindex="0">stop</button>
                                                                                                                                </div>
                                                                                                                                <div class="jp-progress">
                                                                                                                                    <div class="jp-seek-bar">
                                                                                                                                        <div class="jp-play-bar"></div>
                                                                                                                                    </div>
                                                                                                                                </div>
                                                                                                                                <div class="jp-volume-controls">
                                                                                                                                    <button class="jp-mute" role="button" tabindex="0">mute</button>
                                                                                                                                    <button class="jp-volume-max" role="button" tabindex="0">max volume</button>
                                                                                                                                    <div class="jp-volume-bar">
                                                                                                                                        <div class="jp-volume-bar-value"></div>
                                                                                                                                    </div>
                                                                                                                                </div>
                                                                                                                                <div class="jp-time-holder">
                                                                                                                                    <div class="jp-current-time" role="timer" aria-label="time">&nbsp;</div>
                                                                                                                                    <div class="jp-duration" role="timer" aria-label="duration">&nbsp;</div>
                                                                                                                                </div>
                                                                                                                                <div class="jp-toggles">
                                                                                                                                    <button class="jp-repeat" role="button" tabindex="0">repeat</button>
                                                                                                                                    <button class="jp-shuffle" role="button" tabindex="0">shuffle</button>
                                                                                                                                </div>
                                                                                                                            </div>
                                                                                                                            <div class="jp-playlist">
                                                                                                                                <ul>
                                                                                                                                    <li>&nbsp;</li>
                                                                                                                                </ul>
                                                                                                                            </div>
                                                                                                                            <div class="jp-no-solution">
                                                                                                                                <span>Update Required</span>
                                                                                                                                To play the media you will need to either update your browser to a recent version or update your <a href="http://get.adobe.com/flashplayer/" target="_blank">Flash plugin</a>.
                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </table>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </div>
                                                                                    <div class="modal-footer">
                                                                                        <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
                                                                                    </div>
                                                                                </div>
                                                                                <!-- /.modal-content -->
                                                                            </div>
                                                                            <!-- /.modal-dialog -->
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-actions">
                                                                        <div class="row">
                                                                            <div class="col-md-offset-3 col-md-9">
                                                                                <input type="submit" class="btn green" value="Make Highlight" formaction="addFeedHighlights.do" />
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </html:form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <%@include file="/shared/footer.jsp"%>
        </div>
        <%@include file="/shared/script-list.jsp"%>
        <script type="text/javascript">
            // use for details information
            var jsonArray = <%=jsonArray%>,
                    userBasicInfo = <%=userBasicInfo.length() > 0 ? userBasicInfo : " '' "%>,
                    userBasicInfoHashMap = <%=userBasicInfoHashMap.length() > 0 ? userBasicInfoHashMap : " '' "%>,
                    BASE_MEDIA_URL = '<%=Constants.MEDIA_CLOUD_BASE_URL%>',
                    jPlayerVideo, jPlayerAudio, jPlayerShareVideo, jPlayerShareAudio,
                    checkboxes = document.getElementsByName('selectedIDs'),
                    Checked, Length, i,
                    feedWghtJson,
                    wght = document.getElementsByName('wght'),
                    anchrEdit = document.getElementsByName("anchrEdit"),
                    anchrRemove = document.getElementsByName("anchrRemove");

            function toggleWeightText(flag, anchr, i) {
                if (flag == 1) {
                    wght[i].style.display = 'inline-block';
                } else {
                    wght[i].style.display = 'none';
                }
                if (anchr == 0) {
                    anchrEdit[i].style.display = 'none';
                    anchrRemove[i].style.display = 'inline-block';
                } else {
                    anchrEdit[i].style.display = 'inline-block';
                    anchrRemove[i].style.display = 'none';
                    wght[i].style.display = 'none';
                    checkboxes[i].checked = false;
                }
            }

            function IsJsonString(str) {
                try {
                    JSON.parse(str);
                } catch (e) {
                    return false;
                }
                return true;
            }

            function  setfeedIdAndWght(i) {
                wght[i].style.display = 'inline-block';
                anchrEdit[i].style.display = 'none';
                anchrRemove[i].style.display = 'inline-block';
                feedWghtJson = {};
                if (IsJsonString(checkboxes[i].value)) {
                    feedWghtJson = JSON.parse(checkboxes[i].value);
                    feedWghtJson["weight"] = wght[i].value;
                } else {
                    feedWghtJson["feedId"] = checkboxes[i].value;
                    feedWghtJson["weight"] = wght[i].value;
                }
                checkboxes[i].value = JSON.stringify(feedWghtJson);
            }

            function checkAllAndSetWeight(objCheckAll) {
                Checked = objCheckAll.checked;
                for (i = 0, Length = checkboxes.length; i < Length; i++) {
                    feedWghtJson = {};
                    if (IsJsonString(checkboxes[i].value)) {
                        feedWghtJson = JSON.parse(checkboxes[i].value);
                        feedWghtJson["weight"] = wght[i].value;
                    } else {
                        feedWghtJson["feedId"] = checkboxes[i].value;
                        feedWghtJson["weight"] = wght[i].value;
                    }
                    checkboxes[i].checked = Checked;
                    checkboxes[i].value = JSON.stringify(feedWghtJson);
                }
            }
        </script>
        <script src="<%=resourceURL%>/assets/layouts/layout3/scripts/previewfeed.js" type="text/javascript"></script>
    </body>
</html>

