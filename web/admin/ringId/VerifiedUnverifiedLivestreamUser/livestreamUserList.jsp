<%-- 
    Document   : livestreamUserList
    Created on : May 6, 2017, 8:18:49 PM
    Author     : user
--%>


<%@page import="org.ringid.livestream.StreamingDTO"%>
<%@page import="org.ringid.users.UserDTO"%>
<%@page import="org.ringid.users.UserBasicInfoDTO"%>
<%@ page import="java.util.List,java.util.ArrayList" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@include file="/login/loginTimeExpiry.jsp"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ page import="java.util.*,com.ringid.admin.utils.Constants"%>
<%@include  file="/shared/head.jsp" %>


<!DOCTYPE html>
<html>
    <head>
        <title>Admin Panel | Active Users List </title>
        <%@include file="/shared/css-list.jsp"%>
    </head>
    <body class="page-container-bg-solid">
        <div class="page-wrapper">
            <div class="page-wrapper-row">
                <div class="page-wrapper-top">
                    <div class="page-header">
                        <%@include  file="/shared/menu.jsp" %>
                        <%
                            String message = "";
                            if (request.getAttribute("message") != null) {
                                message = request.getAttribute("message").toString();
                                request.removeAttribute("message");
                            }
                            int TOTAL_RECORDS = 0, TOTAL_PAGES = 0, CURRENT_PAGE_NO = 0, STARTING_RECORD_NO = 0, ENDING_RECORD_NO = 0;
                            String disabledPrev = "", disabledNext = "";
                            if (request.getAttribute(Constants.TOTAL_RECORDS) != null) {
                                TOTAL_RECORDS = Integer.valueOf(request.getAttribute(Constants.TOTAL_RECORDS).toString());
                            }
                            if (request.getAttribute(Constants.TOTAL_PAGES) != null) {
                                TOTAL_PAGES = Integer.valueOf(request.getAttribute(Constants.TOTAL_PAGES).toString());
                            }
                            if (request.getAttribute(Constants.CURRENT_PAGE_NO) != null) {
                                CURRENT_PAGE_NO = Integer.valueOf(request.getAttribute(Constants.CURRENT_PAGE_NO).toString());
                            }
                            if (request.getAttribute(Constants.STARTING_RECORD_NO) != null) {
                                STARTING_RECORD_NO = Integer.valueOf(request.getAttribute(Constants.STARTING_RECORD_NO).toString());
                            }
                            if (request.getAttribute(Constants.ENDING_RECORD_NO) != null) {
                                ENDING_RECORD_NO = Integer.valueOf(request.getAttribute(Constants.ENDING_RECORD_NO).toString());
                            }
                            if (STARTING_RECORD_NO <= 0) {
                                disabledPrev = "disabled";
                                STARTING_RECORD_NO = 0;
                            }
                            if (ENDING_RECORD_NO >= TOTAL_RECORDS) {
                                disabledNext = "disabled";
                                ENDING_RECORD_NO = TOTAL_RECORDS;
                            }
                        %>

                    </div>
                </div>
            </div>
            <div class="page-wrapper-row full-height">
                <div class="page-wrapper-middle">
                    <div class="page-container">
                        <div class="page-content-wrapper">
                            <div class="page-content">
                                <div class="container">
                                    <ul class="page-breadcrumb breadcrumb">
                                        <li>
                                            <a href="welcome.do">Admin Panel</a>
                                            <span style="font-size: 14px; margin: 0 auto;" class="fa fa-angle-right"></span>
                                        </li>
                                        <li>
                                            <a href="javascript:;">Live / Channel</a>
                                            <span style="font-size: 14px; margin: 0 auto;" class="fa fa-angle-right"></span>
                                        </li>
                                        <li>
                                            <a href="javascript:;">Live Info</a>
                                            <span style="font-size: 14px; margin: 0 auto;" class="fa fa-angle-right"></span>
                                        </li>
                                        <li>
                                            <span id="features" data-class-active=".live-o-channel .liveInfo .activeUsers">Active Users</span>
                                        </li>
                                    </ul>
                                    <div class="page-content-inner">
                                        <%if (message != null && message.length() > 0) {%>
                                        <div id="bootstrap_alerts_demo" >
                                            <div id="prefix_661213402591" class="custom-alerts alert alert-success fade in">
                                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                <strong > <%=message%> </strong>
                                            </div>
                                        </div>
                                        <% }%>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="tabbable-line boxless tabbable-reversed">
                                                    <div class="tab-content" style="padding-top: 15px;">
                                                        <div class="portlet box green">
                                                            <div class="portlet-title">
                                                                <div class="caption">
                                                                    <i class="fa fa-list-ul"></i> Active Users List
                                                                </div>
                                                                <div class="tools">
                                                                    <a href="javascript:;" class="collapse"> </a>

                                                                    <a data-original-title="Reload" href="showLivestreamUser.do" style="width: 13px; background-image: url(assets/global/img/portlet-reload-icon-white.png);" class="tooltips"> </a>
                                                                </div>
                                                            </div>
                                                            <div class="portlet-body form">

                                                                <html:form action="/showLivestreamUser" styleClass="form-horizontal" method="GET"  acceptCharset="UTF-8">
                                                                    <div class="form-body">
                                                                        <div class="form-group">
                                                                            <div class="col-md-12 col-sm-12">
                                                                                <label>
                                                                                    <html:select styleClass="form-control input-inline" property="type" onchange="submitform(1,1,this)">
                                                                                        <html:option value="1">Unverified stream</html:option>
                                                                                        <html:option value="2">Verified stream</html:option>
                                                                                    </html:select>
                                                                                </label>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <div class="col-md-6 col-sm-6">
                                                                                <label>
                                                                                    <html:select styleClass="bs-select form-control input-sm input-xsmall input-inline" property="recordPerPage" onchange="javascript: submitform(1, 1, this);">
                                                                                        <html:option value="10">10</html:option>
                                                                                        <html:option value="25">25</html:option>
                                                                                        <html:option value="50">50</html:option>
                                                                                        <html:option value="100">100</html:option>
                                                                                        <html:option value="200">200</html:option>
                                                                                    </html:select>
                                                                                    records</label>
                                                                            </div>
                                                                            <div class="col-md-6 col-sm-6">
                                                                                <div class="input-group" style="float: right;">
                                                                                    <input name="searchText" type="text" value="<bean:write name="LivestreamUserForm" property="searchText"/>" class="form-control input-sm input-small" placeholder="ringID or name">
                                                                                    <span>
                                                                                        <button class="btn green-soft uppercase bold input-sm" type="submit"><i class="icon-magnifier"></i></button>
                                                                                        <input name="action" type="hidden" value="<%=Constants.SEARCH%>" />
                                                                                    </span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="table-scrollable">
                                                                            <table class="table table-striped table-bordered table-hover">
                                                                                <thead>
                                                                                    <tr>
                                                                                        <th style="text-align: center" class="no-sort">
                                                                                            <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                                                                                <input type="checkbox" onchange="checkAll(this)" class="group-checkable" />
                                                                                                <span></span>
                                                                                            </label>
                                                                                        </th>
                                                                                        <th style="text-align: center;" >Ring ID</th>
                                                                                        <th style="text-align: center;">Name</th>
                                                                                        <th style="text-align: center;" class="no-sort">Profile Image</th>
                                                                                    </tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                    <%
                                                                                        ArrayList<StreamingDTO> data1 = (ArrayList<StreamingDTO>) session.getAttribute(Constants.DATA_ROWS);
                                                                                        session.removeAttribute(Constants.DATA_ROWS);
                                                                                        if (data1 != null && data1.size() > 0) {
                                                                                            for (int i = STARTING_RECORD_NO; i < ENDING_RECORD_NO; i++) {
                                                                                                StreamingDTO dto = (StreamingDTO) data1.get(i);
                                                                                    %>
                                                                                    <tr>
                                                                                        <td style="text-align: center;">
                                                                                            <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                                                                                <input name="selectedIDs" type="checkbox" class="checkboxes" value="<%=String.valueOf(dto.getUserTableId())%>" />
                                                                                                <span></span>
                                                                                            </label>
                                                                                        </td>
                                                                                        <td style="text-align: center;"><%=dto.getuId()%></td>
                                                                                        <td style="text-align: center;"><%=dto.getName()%></td>
                                                                                        <td style="text-align: center;">
                                                                                            <%
                                                                                                if (dto.getProfileImage() != null && dto.getProfileImage().length() > 0) {
                                                                                            %>
                                                                                            <img style="height: 100px; width: 100px" src="<%=Constants.IMAGE_BASE_URL%><%=dto.getProfileImage()%>" />
                                                                                            <%
                                                                                            } else {
                                                                                            %>
                                                                                            <img style="height: 100px; width: 100px" src="resources/images/default-pp.jpg" />                                                                                    
                                                                                            <%
                                                                                                }
                                                                                            %>                                                                                    
                                                                                        </td>
                                                                                        <% }
                                                                                            }%>        
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                        <div class="row">
                                                                            <div class="col-md-5 col-sm-12">
                                                                                <div>
                                                                                    Showing <%=(STARTING_RECORD_NO + 1)%> to <%=ENDING_RECORD_NO%> of <%=TOTAL_RECORDS%> entries
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-7 col-sm-12">
                                                                                <div style="float: right;">
                                                                                    <div> 
                                                                                        Page <a href="javascript: submitform(2, <%=(CURRENT_PAGE_NO - 1)%>, this);" class="btn btn-sm default prev <%=disabledPrev%>"><i class="fa fa-angle-left"></i></a>
                                                                                        <input name="pageNo" class="form-control input-sm input-inline input-mini" maxlenght="5" style="text-align:center; margin: 0 5px;" type="text" value="<%=CURRENT_PAGE_NO%>" onchange="javascript: submitform(2, 0, this);">
                                                                                        <a href="javascript: submitform(2, <%=(CURRENT_PAGE_NO + 1)%>, this);" class="btn btn-sm default next <%=disabledNext%>"><i class="fa fa-angle-right"></i></a> of <span><%=TOTAL_PAGES%></span>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-actions">
                                                                        <div class="row">
                                                                            <div class="col-md-offset-4 col-md-8">
                                                                                <logic:equal name="LivestreamUserForm" property="type" value="1">
                                                                                    <html:submit styleClass="btn green"  property="submitType" value="Verify" />
                                                                                </logic:equal>
                                                                                <logic:equal name="LivestreamUserForm" property="type" value="2">
                                                                                    <html:submit styleClass="btn green"  property="submitType" value="Unverify" />
                                                                                    <html:submit styleClass="btn green"  property="submitType" value="Make Featured" />
                                                                                </logic:equal>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </html:form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <%@include file="/shared/footer.jsp"%>
        </div>
        <%@include file="/shared/script-list.jsp"%>
    </body>

</html>
