<%-- 
    Document   : adminAction
    Created on : May 15, 2018, 11:41:35 AM
    Author     : user
--%>

<%@include file="/login/loginTimeExpiry.jsp"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@include  file="/shared/head.jsp" %>
<!DOCTYPE html>
<html>
    <head>
        <title>Admin Panel | Admin Action </title>
        <%@include file="/shared/css-list.jsp"%>
    </head>
    <body class="page-container-bg-solid">
        <div class="page-wrapper">
            <div class="page-wrapper-row">
                <div class="page-wrapper-top">
                    <div class="page-header">
                        <%@include  file="/shared/menu.jsp" %>
                        <%
                            String message = "";
                            if (request.getAttribute("message") != null) {
                                message = request.getAttribute("message").toString();
                                request.removeAttribute("message");
                            }
                        %>
                    </div>
                </div>
            </div>
            <div class="page-wrapper-row full-height">
                <div class="page-wrapper-middle">
                    <div class="page-container">
                        <div class="page-content-wrapper">
                            <div class="page-content">
                                <div class="container">
                                    <ul class="page-breadcrumb breadcrumb">
                                        <li>
                                            <a href="welcome.do">Admin Panel</a>
                                            <span style="font-size: 14px; margin: 0 auto;" class="fa fa-angle-right"></span>
                                        </li>
                                        <li>
                                            <a href="javascript:;">Utility</a>
                                            <span style="font-size: 14px; margin: 0 auto;" class="fa fa-angle-right"></span>
                                        </li>
                                        <li>
                                            <span id="features" data-class-active=".manageOperations .adminAction">Admin Action</span>
                                        </li>
                                    </ul>
                                    <%if (message != null && message.length() > 0) {%>
                                    <div id="prefix_661213402591" class="custom-alerts alert alert-success fade in" style="word-wrap: break-word;">
                                        <button type="button" class="close" data-dismiss="alert"
                                                aria-hidden="true"></button>
                                        <strong> <%=message%> </strong>
                                    </div>
                                    <%}%>
                                    <div class="page-content-inner">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="tabbable-line boxless tabbable-reversed">
                                                    <div class="tab-content" style="padding-top: 15px;">
                                                        <div class="portlet box green">
                                                            <div class="portlet-title">
                                                                <div class="caption">
                                                                    <i class="fa fa-wrench"></i> Admin Action </div>
                                                                <div class="tools">
                                                                    <a href="javascript:;" class="collapse"> </a>
                                                                </div>
                                                            </div>
                                                            <div class="portlet-body form">
                                                                <html:form action="adminAction" method="POST" acceptCharset="UTF-8" styleClass="form-horizontal" >
                                                                    <div class="form-body">
                                                                        <div class="form-group">
                                                                            <label class="col-md-3 control-label"> Action <span class="required">*</span> </label>
                                                                            <div class="col-md-4">
                                                                                <html:select property="actionType" styleClass="form-control" onchange="resetAdminAction(this)">
                                                                                    <html:option value=""> -- Select Action -- </html:option>
                                                                                    <html:option value="10">Destroy User Sessions</html:option>
                                                                                    <html:option value="13"> Destroy Device Sessions </html:option>
                                                                                    <html:option value="17"> Destroy All Sessions </html:option>
                                                                                    <html:option value="18"> Session Details</html:option>
                                                                                    <html:option value="23"> Clear Cache </html:option>
                                                                                    <html:option value="24"> Sign Out </html:option>
                                                                                    <html:option value="25"> Send General Message </html:option>
                                                                                </html:select>
                                                                                <html:messages id="actionType" property="actionType">
                                                                                    <span style="color: red;"> Please Select Action Type </span>
                                                                                </html:messages>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label class="col-md-3 control-label"> RingID <span class="required">*</span> </label>
                                                                            <div class="col-md-4">
                                                                                <html:text property="userId" styleClass="form-control" title="ringId" />
                                                                                <html:messages id="userId" property="userId">
                                                                                    <span style="color: red;"> RingID Required </span>
                                                                                </html:messages>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group" id="divDevice">
                                                                            <label class="col-md-3 control-label"> Device </label>
                                                                            <div class="col-md-4">
                                                                                <html:select property="device" styleClass="form-control">
                                                                                    <html:option value="1"> Desktop </html:option>
                                                                                    <html:option value="2"> Android </html:option>
                                                                                    <html:option value="3"> iPhone </html:option>
                                                                                    <html:option value="4"> Windows </html:option>
                                                                                    <html:option value="5"> Web </html:option>
                                                                                </html:select>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group" id="divMsg">
                                                                            <label class="col-md-3 control-label"> Message </label>
                                                                            <div class="col-md-4">
                                                                                <html:text property="message" styleClass="form-control" title="message" />
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-actions">
                                                                        <div class="row">
                                                                            <div class="col-md-offset-3 col-md-9">
                                                                                <button type="submit" class="btn green" name="submitType">Submit</button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </html:form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <%@include file="/shared/footer.jsp"%>
        </div>
        <%@include file="/shared/script-list.jsp"%>
        <script type="text/javascript">
            function resetAdminAction(obj) {
                var action;
                if (typeof obj.value != 'undefined') {
                    action = parseInt(obj.value);
                } else if (parseInt(obj) > 0) {
                    action = parseInt(obj);
                } else {
                    action = 0;
                }
                switch (action) {
                    case 13:
                        $('#divMsg').hide();
                        $('#divDevice').show();
                        break;
                    case 24:
                    case 25:
                        $('#divDevice').hide();
                        $('#divMsg').show();
                        break;
                    default:
                        $('#divMsg').hide();
                        $('#divDevice').hide();
                        break;
                }
            }
            resetAdminAction($('select[name=actionType]').val());
        </script>
    </body>
</html>
