<%-- 
    Document   : appConstantsUI-new
    Created on : Jan 22, 2018, 3:29:47 PM
    Author     : mamun
--%>

<%@include file="/login/loginTimeExpiry.jsp"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ page import="java.util.*,com.ringid.admin.utils.Constants"%>
<%@include  file="/shared/head.jsp" %>
<!DOCTYPE html>
<html>
    <head>
        <title>Admin Panel | Reset Retry Limit </title>
        <%@include file="/shared/css-list.jsp"%>
    </head>
    <body class="page-container-bg-solid">
        <div class="page-wrapper">
            <div class="page-wrapper-row">
                <div class="page-wrapper-top">
                    <div class="page-header">
                        <%@include  file="/shared/menu.jsp" %>
                    </div>
                </div>
            </div>
            <div class="page-wrapper-row full-height">
                <div class="page-wrapper-middle">
                    <div class="page-container">
                        <div class="page-content-wrapper">
                            <div class="page-content">
                                <div class="container">
                                    <ul class="page-breadcrumb breadcrumb">
                                        <li>
                                            <a href="welcome.do">Admin Panel</a>
                                            <span style="font-size: 14px; margin: 0 auto;" class="fa fa-angle-right"></span>
                                        </li>
                                        <li>
                                            <a href="javascript:;">Manage Operations</a>
                                            <span style="font-size: 14px; margin: 0 auto;" class="fa fa-angle-right"></span>
                                        </li>
                                        <li>
                                            <span id="features" data-class-active=".manageOperations .resetRetryLimit">Reset Retry Limit</span>
                                        </li>
                                    </ul>
                                    <logic:messagesPresent property="errormsg" message="false">
                                        <html:messages id="errormsg" message="false">
                                            <logic:present name="errormsg">
                                                <div  class="custom-alerts alert alert-success fade in">
                                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                    <strong> <bean:write name="errormsg" filter="false" /> </strong>
                                                </div>
                                            </logic:present>
                                        </html:messages>
                                    </logic:messagesPresent>
                                    <div class="page-content-inner">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="tabbable-line boxless tabbable-reversed">
                                                    <div class="tab-content" style="padding-top: 15px;">
                                                        <div class="portlet box green">
                                                            <div class="portlet-title">
                                                                <div class="caption">
                                                                    <i class="fa fa-wrench"></i> Reset Retry Limit </div>
                                                                <div class="tools">
                                                                    <a href="javascript:;" class="collapse"> </a>
                                                                </div>
                                                            </div>
                                                            <div class="portlet-body form">
                                                                <html:form action="resetRetryLimit" method="POST" acceptCharset="UTF-8" styleClass="form-horizontal" >
                                                                    <div class="form-body">
                                                                        <div class="form-group">
                                                                            <label class="col-md-3 control-label">Reset</label>
                                                                            <div class="col-md-4">
                                                                                <html:select property="resetType" styleClass="form-control" onchange="resetRetry(this)" styleId="rsttype">
                                                                                    <html:option value="0"> All</html:option>
                                                                                    <html:option value="1"> MNVC Retry Limit </html:option>
                                                                                    <html:option value="2"> RVC Retry Limit </html:option>
                                                                                    <html:option value="3"> Signup Retry Limit</html:option>
                                                                                    <html:option value="4"> OTP History</html:option>
                                                                                    <html:option value="5"> OTP History By DeviceId</html:option>
                                                                                    <html:option value="6"> OTP History By Number</html:option>
                                                                                    <html:option value="7"> Secondary Contact Verification History</html:option>
                                                                                    <html:option value="8"> DeviceId Verification History</html:option>
                                                                                </html:select>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group" id="dvusr">
                                                                            <label class="col-md-3 control-label"> UserId </label>
                                                                            <div class="col-md-4">
                                                                                <input type="text" name="userId" title="user id" class="form-control" placeholder="type ringId or userId" />
                                                                                <html:messages id="userId" property="userId">
                                                                                    <span style="color: red;"> RingID Required </span>
                                                                                </html:messages>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group" id="dvdevice">
                                                                            <label class="col-md-3 control-label"> DeviceId </label>
                                                                            <div class="col-md-4">
                                                                                <html:text property="deviceId" title="device ID" styleClass="form-control" />
                                                                                <html:messages id="deviceId" property="deviceId">
                                                                                    <span style="color: red;"> DeviceId Required </span>
                                                                                </html:messages>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group" id="dvdc">
                                                                            <label class="col-md-3 control-label"> Dialing Code </label>
                                                                            <div class="col-md-4">
                                                                                <html:select property="dialingCode" styleClass="form-control"> 
                                                                                    <html:option  value=""> Select Country </html:option>     
                                                                                    <%
                                                                                        List<com.ringid.country.CountryDTO> countryDTOs = com.ringid.country.CountryLoader.getInstance().getCountryList();
                                                                                        for (com.ringid.country.CountryDTO dto : countryDTOs) {
                                                                                    %>
                                                                                    <html:option value="<%=dto.getDialingCode()%>"><%=dto.getName()%>(<%=dto.getDialingCode()%>)</html:option>
                                                                                    <% }%>
                                                                                </html:select>
                                                                                <html:messages id="dialingCode" property="dialingCode">
                                                                                    <span style="color: red;"> Please Select Dialing Code </span>
                                                                                </html:messages>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group" id="dvmbl">
                                                                            <label class="col-md-3 control-label"> Mobile </label>
                                                                            <div class="col-md-4">
                                                                                <html:text property="mobileNo" title="User ID" styleClass="form-control" />
                                                                                <html:messages id="mobileNo" property="mobileNo">
                                                                                    <span style="color: red;"> MobileNo Required </span>
                                                                                </html:messages>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group last">
                                                                            <label class="col-md-3 control-label"></label>
                                                                            <div class="col-md-4">
                                                                                <%
                                                                                    if (request.getSession(true).getAttribute("error") == "2") {
                                                                                %>
                                                                                <html:messages id="message" property="message" ><bean:write name="msg"  filter="false"/></html:messages>
                                                                                <bean:write name="AuthServerForm" property="message" filter="false"/>
                                                                                <%}%>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-actions">
                                                                        <div class="row">
                                                                            <div class="col-md-offset-3 col-md-9">
                                                                                <button type="submit" class="btn green" name="saveButton">Submit</button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </html:form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <%@include file="/shared/footer.jsp"%>
        </div>
        <%@include file="/shared/script-list.jsp"%>
        <script type="text/javascript">
            function showHideDiv(type) {
                switch (type) {
                    case '1':
                    case '2':
                    case '4':
                    case '7':
                        $("#dvdevice").hide();
                        $("#dvusr").show();
                        $("#dvdc").hide();
                        $("#dvmbl").hide();
                        break;
                    case '5':
                    case '8':
                        $("#dvdevice").show();
                        $("#dvusr").hide();
                        $("#dvdc").hide();
                        $("#dvmbl").hide();
                        break;
                    case '3':
                    case '6':
                        $("#dvdevice").hide();
                        $("#dvusr").hide();
                        $("#dvdc").show();
                        $("#dvmbl").show();
                        break;
                    default :
                        $("#dvusr").show();
                        $("#dvdc").show();
                        $("#dvmbl").show();
                        $("#dvdevice").hide();
                        break;
                }
            }

            var rstType = document.getElementById("rsttype");
            showHideDiv(rstType.options[rstType.selectedIndex].value);

            function resetRetry(objResetType) {
                showHideDiv(objResetType.value);
            }
        </script>
    </body>
</html>
