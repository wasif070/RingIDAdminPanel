<%@include file="/login/loginTimeExpiry.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@include  file="/shared/head.jsp" %>

<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Admin Panel | Edit Feed Highlights</title>
        <%@include file="/shared/css-list.jsp"%>
    </head>
    <body class="page-container-bg-solid">
        <div class="page-wrapper">
            <div class="page-wrapper-row">
                <div class="page-wrapper-top">
                    <div class="page-header">
                        <%@include  file="/shared/menu.jsp" %>
                    </div>
                </div>
            </div>
            <div class="page-wrapper-row full-height">
                <div class="page-wrapper-middle">
                    <div class="page-container">
                        <div class="page-content-wrapper">
                            <div class="page-content">
                                <div class="container">
                                    <ul class="page-breadcrumb breadcrumb">
                                        <li>
                                            <a href="welcome.do">Admin Panel</a>
                                            <span style="font-size: 14px; margin: 0 auto;" class="fa fa-angle-right"></span>
                                        </li>
                                        <li>
                                            <a href="javascript:;">Manage Operations</a>
                                            <span style="font-size: 14px; margin: 0 auto;" class="fa fa-angle-right"></span>
                                        </li>
                                        <li>
                                            <span id="features" data-class-active=".manageOperations .feedHighlights">Edit Feed Highlights </span>
                                        </li>
                                    </ul>
                                    <div class="page-content-inner">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="tabbable-line boxless tabbable-reversed">
                                                    <div class="portlet box green">
                                                        <div class="portlet-title">
                                                            <div class="caption">
                                                                Edit Feed Highlights
                                                            </div>
                                                        </div>
                                                        <div class="portlet-body form">
                                                            <html:form action="/updateFeedHighlights" method="POST" acceptCharset="UTF-8" styleClass="form-horizontal" >
                                                                <% request.getSession(true).setAttribute("formType", Constants.UPDATE);%>
                                                                <%@include file="feedHighlightsTable.jsp" %>
                                                            </html:form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="page-wrapper-row">
                <div class="page-wrapper-bottom">
                    <div class="page-footer">
                        <div class="container"> Copyright &copy; 2015 All right reserved by
                            <a target="_blank" href="http://www.ipvision.ca/">IPvision canada inc</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <%@include file="/shared/script-list.jsp"%>
    </body>
</html>

