<%-- 
    Document   : m_appConstantsTable
    Created on : Feb 14, 2017, 12:09:51 PM
    Author     : ipvision
--%>

<div class="form-body">
    <div class="form-group">
        <label class="col-md-3 control-label">FeedId</label>
        <div class="col-md-4">
            <label> <bean:write name="FeedHighlightForm" property="feedId" /> </label>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label">Weight</label>
        <div class="col-md-4">
            <html:text property="weight" title="weight" styleClass="form-control" />
            <html:messages id="weight" property="weight">
                <bean:write name="weight"  filter="false"/>
            </html:messages>
        </div>
    </div>
</div>
<div class="form-actions">
    <div class="row">
        <div class="col-md-offset-3 col-md-4">
            <button type="submit" class="btn green" name="saveButton">Submit</button>
            <a href="showFeedHighlightsList.do" class="btn default">Cancel</a>
            <html:hidden property="feedId"/>
            <input type="hidden" name="action" value="<%=request.getSession(true).getAttribute("formType")%>" />
        </div>
    </div>
</div>
