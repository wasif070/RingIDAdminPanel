<%-- 
    Document   : verifyNumber
    Created on : Jun 3, 2017, 1:25:59 PM
    Author     : Rabby
--%>

<%@include file="/login/loginTimeExpiry.jsp"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ page import="java.util.*,com.ringid.admin.utils.Constants"%>
<%@include  file="/shared/head.jsp" %>
<!DOCTYPE html>
<html>
    <head>
        <title>Admin Panel | Verify Number </title>
        <%@include file="/shared/css-list.jsp"%>
    </head>
    <body class="page-container-bg-solid">
        <div class="page-wrapper">
            <div class="page-wrapper-row">
                <div class="page-wrapper-top">
                    <div class="page-header">
                        <%@include  file="/shared/menu.jsp" %>
                    </div>
                </div>
            </div>
            <div class="page-wrapper-row full-height">
                <div class="page-wrapper-middle">
                    <div class="page-container">
                        <div class="page-content-wrapper">
                            <div class="page-content">
                                <div class="container">
                                    <ul class="page-breadcrumb breadcrumb">
                                        <li>
                                            <a href="welcome.do">Admin Panel</a>
                                            <span style="font-size: 14px; margin: 0 auto;" class="fa fa-angle-right"></span>
                                        </li>
                                        <li>
                                            <a href="javascript:;">Manage Operations</a>
                                            <span style="font-size: 14px; margin: 0 auto;" class="fa fa-angle-right"></span>
                                        </li>
                                        <li>
                                            <span id="features" data-class-active=".manageOperations .verifyNumber">Verify Number</span>
                                        </li>
                                    </ul>
                                    <logic:messagesPresent property="errormsg" message="false">
                                        <html:messages id="errormsg" message="false">
                                            <logic:present name="errormsg">
                                                <div  class="custom-alerts alert alert-success fade in">
                                                    <button type="button" class="close" data-dismiss="alert"
                                                            aria-hidden="true"></button>
                                                    <strong> <bean:write name="errormsg" filter="false" /> </strong>
                                                </div>
                                            </logic:present>
                                        </html:messages>
                                    </logic:messagesPresent>
                                    <div class="page-content-inner">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="tabbable-line boxless tabbable-reversed">
                                                    <div class="tab-content" style="padding-top: 15px;">
                                                        <div class="portlet box green">
                                                            <div class="portlet-title">
                                                                <div class="caption">
                                                                    <i class="fa fa-wrench"></i> Verify Number </div>
                                                                <div class="tools">
                                                                    <a href="javascript:;" class="collapse"> </a>
                                                                </div>
                                                            </div>
                                                            <div class="portlet-body form">
                                                                <html:form action="verifyNumber" method="POST" acceptCharset="UTF-8" styleClass="form-horizontal" >
                                                                    <div class="form-body">
                                                                        <div class="form-group">
                                                                            <label class="col-md-3 control-label"> RingID <span class="required"> * </span> </label>
                                                                            <div class="col-md-4">
                                                                                <html:text property="userId" title="ringid" styleClass="form-control" />
                                                                                <html:messages id="userId" property="userId">
                                                                                    <span style="color: red;"> UserId Required </span>
                                                                                </html:messages>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <%
                                                                                List<com.ringid.country.CountryDTO> countryDTOs = com.ringid.country.CountryLoader.getInstance().getCountryList();
                                                                            %>
                                                                            <label class="col-md-3 control-label"> Dialing Code <span class="required"> * </span> </label>
                                                                            <div class="col-md-4">
                                                                                <html:select property="dialingCode" styleClass="form-control"> 
                                                                                    <html:option  value=""> Select Country </html:option>     
                                                                                    <%
                                                                                        for (com.ringid.country.CountryDTO dto : countryDTOs) {
                                                                                    %>
                                                                                    <html:option value="<%=dto.getDialingCode()%>"><%=dto.getName()%>(<%=dto.getDialingCode()%>)</html:option>
                                                                                    <% }%>
                                                                                </html:select>
                                                                                <html:messages id="dialingCode" property="dialingCode">
                                                                                    <span style="color: red;"> Please Select Dialing Code </span>
                                                                                </html:messages>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label class="col-md-3 control-label"> Mobile Number <span class="required"> * </span> </label>
                                                                            <div class="col-md-1">
                                                                                <html:hidden property="prefix" value="<%=Constants.MOBILE_NUMBER_VERIFY_PREFIX%>"/>

                                                                                <a href="#large" data-toggle="modal" class="btn grey"><%=Constants.MOBILE_NUMBER_VERIFY_PREFIX%></a>
                                                                                <html:messages id="prefix" property="prefix">
                                                                                    <span style="color: red;"> Mobile Number Required </span>
                                                                                </html:messages>
                                                                            </div>
                                                                            <div class="col-md-3">
                                                                                <html:text property="mobileNumber" title="Mobile Number" styleClass="form-control" />
                                                                                <html:messages id="mobileNumber" property="mobileNumber">
                                                                                    <span style="color: red;"> Mobile Number Required </span>
                                                                                </html:messages>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-actions">
                                                                        <div class="row">
                                                                            <div class="col-md-offset-3 col-md-9">
                                                                                <button type="submit" class="btn green" name="saveButton">Submit</button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </html:form>
                                                                <div class="modal fade bs-modal-lg" id="large" tabindex="-1" role="dialog" aria-hidden="true">
                                                                    <html:form action="/verifyNumber" method="POST" acceptCharset="UTF-8" styleClass="form-horizontal" >
                                                                        <html:hidden property="userId"/>
                                                                        <html:hidden property="dialingCode"/>
                                                                        <html:hidden property="mobileNumber"/>
                                                                        <div class="modal-dialog modal-lg">
                                                                            <div class="modal-content">
                                                                                <div class="modal-header">
                                                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                                                    <h4 class="modal-title">Edit Mobile Prefix</h4>
                                                                                </div>
                                                                                <div class="modal-body">
                                                                                    <div class="form-group">
                                                                                        <label class="col-md-3 control-label"> Mobile Number Prefix</label>
                                                                                        <div class="col-md-3">
                                                                                            <html:text property="prefix" title="Mobile Number" styleClass="form-control" value="<%=Constants.MOBILE_NUMBER_VERIFY_PREFIX%>"/>
                                                                                            <html:messages id="prefix" property="prefix">
                                                                                                <span style="color: red;"> Mobile Number Required </span>
                                                                                            </html:messages>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="modal-footer">
                                                                                    <button type="submit" class="btn green" name="submitType" value="prefix">Submit</button>
                                                                                </div>
                                                                            </div>
                                                                            <!-- /.modal-content -->
                                                                        </div>
                                                                    </html:form>
                                                                    <!-- /.modal-dialog -->
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <%@include file="/shared/footer.jsp"%>
        </div>
        <%@include file="/shared/script-list.jsp"%>
    </body>
</html>

