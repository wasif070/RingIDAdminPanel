<%-- 
    Document   : searchUser
    Created on : February 7, 2018, 2:51:35 PM
    Author     : Rabby
--%>

<%@page import="com.ringid.admin.actions.ringId.searchUser.SearchUserDTO"%>
<%@page import="org.ringid.contacts.SearchedContactDTO"%>
<%@page import="com.ringid.admin.projectMenu.MenuNames"%>
<%@include file="/login/loginTimeExpiry.jsp"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="org.ringid.newsfeeds.CassMultiMediaDTO"%>
<%@ page import="java.util.*,com.ringid.admin.utils.Constants"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@include  file="/shared/head.jsp" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Admin Panel | Search User</title>
        <%@include file="/shared/css-list.jsp"%>
    </head>
    <body class="page-container-bg-solid">
        <div class="page-wrapper">
            <div class="page-wrapper-row">
                <div class="page-wrapper-top">
                    <div class="page-header">
                        <%@include  file="/shared/menu.jsp" %>
                        <%
                            String FEATURE_NAME = MenuNames.SEARCH_USER;
                            String message = "";
                            if (request.getAttribute("message") != null) {
                                message = request.getAttribute("message").toString();
                                request.removeAttribute("message");
                            }
                        %>
                    </div>
                </div>
            </div>
            <div class="page-wrapper-row full-height">
                <div class="page-wrapper-middle">
                    <div class="page-container">
                        <div class="page-content-wrapper">
                            <div class="page-content">
                                <div class="container">
                                    <ul class="page-breadcrumb breadcrumb">
                                        <li>
                                            <a href="welcome.do">Admin Panel</a>
                                            <span style="font-size: 14px; margin: 0 auto;" class="fa fa-angle-right"></span>
                                        </li>
                                        <li>
                                            <a href="javascript:;">Manage Operations</a>
                                            <span style="font-size: 14px; margin: 0 auto;" class="fa fa-angle-right"></span>
                                        </li>
                                        <li>
                                            <span id="features" data-class-active=".manageOperations .searchUser">Search User Contacts</span>
                                        </li>
                                    </ul>
                                    <div class="page-content-inner">
                                        <html:form action="searchUserContacts" styleClass="form-horizontal" method="GET"  acceptCharset="UTF-8">
                                            <div class="search-page search-content-4">
                                                <div class="search-bar-custom">
                                                    <div class="row">
                                                        <div class="col-lg-12">
                                                            <div class="input-group">
                                                                <input type="text" name="searchText" value="<bean:write name="SearchUserForm" property="searchText"/>" class="form-control" placeholder="ringId, utId, phone OR name">
                                                                <span class="input-group-btn">
                                                                    <button class="btn green-soft uppercase bold" type="submit">Search</button>
                                                                    <input type="hidden" id="currentPeopleData" value="10">
                                                                    <input type="hidden" id="currentPageData" value="10">
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <%
                                                    SearchUserDTO searchUserDTO = (SearchUserDTO) session.getAttribute(Constants.DATA_ROWS);
                                                    session.removeAttribute(Constants.DATA_ROWS);
                                                    List<SearchedContactDTO> data1 = null;
                                                    List<SearchedContactDTO> data2 = null;
                                                    if (searchUserDTO != null) {
                                                        data1 = searchUserDTO.getSearchResultPeople();
                                                        data2 = searchUserDTO.getSearchResultPage();
                                                    }
                                                %>
                                                <%if (data1 != null && data1.size() > 0) {%>
                                                <!--people-->
                                                <div class="row">
                                                    <div class="col-sm-12 col-xs-12">
                                                        <h4>People</h4>
                                                    </div>
                                                </div>
                                                <div class="search-table table-responsive">
                                                    <table id="tablePeople" class="table table-bordered table-striped table-condensed">
                                                        <thead class="bg-blue">
                                                            <tr>
                                                                <th>
                                                                    Profile Image
                                                                </th>
                                                                <th>
                                                                    Name/Title
                                                                </th>
                                                                <th>
                                                                    RingID
                                                                </th>
                                                                <th>
                                                                    User Table ID
                                                                </th>
                                                                <th>
                                                                    Country
                                                                </th>
                                                                <th>
                                                                    VIEW
                                                                </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <%
                                                                for (int i = 0; i < data1.size(); i++) {
                                                                    SearchedContactDTO dto = data1.get(i);
                                                            %>
                                                            <tr>
                                                                <td class="table-status">
                                                                    <%if (dto.getProfileImage() != null && dto.getProfileImage().length() > 0) {%>
                                                                    <img src="<%=Constants.MEDIA_CLOUD_BASE_URL%><%=dto.getProfileImage()%>" style="height: 100px;width: 100px"/>
                                                                    <%} else {%>
                                                                    <img src="assets/global/img/default-pp.jpg" style="height: 100px;width: 100px"/>
                                                                    <%}%>
                                                                </td>
                                                                <td class="table-title">
                                                                    <h3>
                                                                        <a href="searchUser2.do?searchText=<%=dto.getUserTableID()%>"><%=dto.getFullName()%></a>
                                                                    </h3>
                                                                    <p class="font-blue">
                                                                        <!--<a href="javascript:;">-->
                                                                        <%if (dto.getProfileType() == 1) {%>
                                                                        DEFAULT USER
                                                                        <%} else if (dto.getProfileType() == 2) {%>
                                                                        SPECIAL CONTACT
                                                                        <%} else if (dto.getProfileType() == 3) {%>
                                                                        USER PAGE
                                                                        <%} else if (dto.getProfileType() == 4) {%>
                                                                        ROOM PAGE
                                                                        <%} else if (dto.getProfileType() == 5) {%>
                                                                        DONATION PAGE
                                                                        <%} else if (dto.getProfileType() == 6) {%>
                                                                        BEAUTY CONTESTANT
                                                                        <%} else if (dto.getProfileType() == 7) {%>
                                                                        SPORTS PAGE
                                                                        <%} else if (dto.getProfileType() == 10) {%>
                                                                        CELEBRITY
                                                                        <%} else if (dto.getProfileType() == 15) {%>
                                                                        NEWSPORTAL
                                                                        <%} else if (dto.getProfileType() == 35) {%>
                                                                        MEDIA PAGE
                                                                        <%} else if (dto.getProfileType() == 25) {%>
                                                                        BUSINESS PAGE
                                                                        <%} else if (dto.getProfileType() == 99) {%>
                                                                        GROUP
                                                                        <%}%>
                                                                        <!--</a>--> 
                                                                        <%if (dto.getProfileType() == 1) {%>
                                                                        -
                                                                        <span class="font-grey-cascade"><%=dto.getDialingCode()%><%=dto.getMobilePhone()%></span>
                                                                        <%}%>
                                                                    </p>
                                                                </td>
                                                                <td class="table-date font-blue">
                                                                    <%=dto.getUserIdentity()%>
                                                                </td>
                                                                <td class="table-date font-blue">
                                                                    <%=dto.getUserTableID()%>
                                                                </td>
                                                                <td class="table-desc"> <%=dto.getCountry()%></td>
                                                                <td class="table-download">
                                                                    <a href="searchUser2.do?searchText=<%=dto.getUserTableID()%>" class="btn green-soft uppercase bold">
                                                                        VIEW
                                                                    </a>
                                                                </td>
                                                            </tr>
                                                            <%}%>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="row text-center">
                                                    <button id="seeMorePeople" type="button" class="btn btn-default mt-ladda-btn ladda-button btn-outline btn-circle seeMorePeople" data-style="expand-left" data-spinner-color="#333">
                                                        <span class="ladda-label">See More</span>
                                                    </button>
                                                </div>
                                                <%}%>

                                                <%if (data2 != null && data2.size() > 0) {%>
                                                <!--page-->
                                                <div class="row">
                                                    <div class="col-sm-12 col-xs-12">
                                                        <h4>Page</h4>
                                                    </div>
                                                </div>
                                                <div class="search-table table-responsive">
                                                    <table id="tablePage" class="table table-bordered table-striped table-condensed">
                                                        <thead class="bg-blue">
                                                            <tr>
                                                                <th>
                                                                    Profile Image
                                                                </th>
                                                                <th>
                                                                    Name/Title
                                                                </th>
                                                                <th>
                                                                    RingID
                                                                </th>
                                                                <th>
                                                                    User Table ID
                                                                </th>
                                                                <th>
                                                                    Country
                                                                </th>
                                                                <th>
                                                                    VIEW
                                                                </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <%
                                                                for (int i = 0; i < data2.size(); i++) {
                                                                    SearchedContactDTO dto = data2.get(i);
                                                            %>
                                                            <tr>
                                                                <td class="table-status">
                                                                    <%if (dto.getProfileImage() != null && dto.getProfileImage().length() > 0) {%>
                                                                    <img src="<%=Constants.MEDIA_CLOUD_BASE_URL%><%=dto.getProfileImage()%>" style="height: 100px;width: 100px"/>
                                                                    <%} else {%>
                                                                    <img src="assets/global/img/default-pp.jpg" style="height: 100px;width: 100px"/>
                                                                    <%}%>
                                                                </td>
                                                                <td class="table-title">
                                                                    <h3>
                                                                        <a href="searchUser2.do?searchText=<%=dto.getUserTableID()%>"><%=dto.getFullName()%></a>
                                                                    </h3>
                                                                    <p class="font-blue">
                                                                        <!--<a href="javascript:;">-->
                                                                        <%if (dto.getProfileType() == 1) {%>
                                                                        DEFAULT USER
                                                                        <%} else if (dto.getProfileType() == 2) {%>
                                                                        SPECIAL CONTACT
                                                                        <%} else if (dto.getProfileType() == 3) {%>
                                                                        USER PAGE
                                                                        <%} else if (dto.getProfileType() == 4) {%>
                                                                        ROOM PAGE
                                                                        <%} else if (dto.getProfileType() == 5) {%>
                                                                        DONATION PAGE
                                                                        <%} else if (dto.getProfileType() == 6) {%>
                                                                        BEAUTY CONTESTANT
                                                                        <%} else if (dto.getProfileType() == 7) {%>
                                                                        SPORTS PAGE
                                                                        <%} else if (dto.getProfileType() == 10) {%>
                                                                        CELEBRITY
                                                                        <%} else if (dto.getProfileType() == 15) {%>
                                                                        NEWSPORTAL
                                                                        <%} else if (dto.getProfileType() == 35) {%>
                                                                        MEDIA PAGE
                                                                        <%} else if (dto.getProfileType() == 25) {%>
                                                                        BUSINESS PAGE
                                                                        <%} else if (dto.getProfileType() == 99) {%>
                                                                        GROUP
                                                                        <%}%>
                                                                        <!--</a>--> 
                                                                        <%if (dto.getProfileType() == 1) {%>
                                                                        -
                                                                        <span class="font-grey-cascade"><%=dto.getDialingCode()%><%=dto.getMobilePhone()%></span>
                                                                        <%}%>
                                                                    </p>
                                                                </td>
                                                                <td class="table-date font-blue">
                                                                    <%=dto.getUserIdentity()%>
                                                                </td>
                                                                <td class="table-date font-blue">
                                                                    <%=dto.getUserTableID()%>
                                                                </td>
                                                                <td class="table-desc"> <%=dto.getCountry()%></td>
                                                                <td class="table-download">
                                                                    <a href="searchUser2.do?searchText=<%=dto.getUserTableID()%>" class="btn green-soft uppercase bold">
                                                                        VIEW
                                                                    </a>
                                                                </td>
                                                            </tr>
                                                            <%}%>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="row text-center">
                                                    <button id="seeMorePage" type="button" class="btn btn-default mt-ladda-btn ladda-button btn-outline btn-circle seeMorePage" data-style="expand-left" data-spinner-color="#333">
                                                        <span class="ladda-label">See More</span>
                                                    </button>
                                                </div>
                                                <%}%>
                                                <%if ((data1 == null || data1.size() == 0) && (data2 == null || data2.size() == 0)) {%>
                                                <div class="text-center">
                                                    <span class="font-grey-cascade">No result found</span>
                                                </div>
                                                <%}%>
                                            </div>
                                        </html:form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <%@include file="/shared/footer.jsp"%>
        </div>
        <%@include file="/shared/script-list.jsp"%>
        <script type="text/javascript">
            var base_url = '<%=resourceURL%>';
            var media_base_url = '<%=Constants.MEDIA_CLOUD_BASE_URL%>';
        </script>
    </body>
</html>
