<%-- 
    Document   : searchUser
    Created on : Jul 4, 2017, 2:51:35 PM
    Author     : Rabby
--%>

<%@page import="com.ringid.admin.projectMenu.MenuNames"%>
<%@include file="/login/loginTimeExpiry.jsp"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="org.ringid.newsfeeds.CassMultiMediaDTO"%>
<%@ page import="java.util.*,com.ringid.admin.utils.Constants"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@include  file="/shared/head.jsp" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Admin Panel | Search User</title>
        <%@include file="/shared/css-list.jsp"%>
    </head>
    <body class="page-container-bg-solid">
        <div class="page-wrapper">
            <div class="page-wrapper-row">
                <div class="page-wrapper-top">
                    <div class="page-header">
                        <%@include  file="/shared/menu.jsp" %>
                        <%
                            String FEATURE_NAME = MenuNames.SEARCH_USER;
                            String message = "";
                            if (request.getAttribute("message") != null) {
                                message = request.getAttribute("message").toString();
                                request.removeAttribute("message");
                            }
                        %>
                    </div>
                </div>
            </div>
            <div class="page-wrapper-row full-height">
                <div class="page-wrapper-middle">
                    <div class="page-container">
                        <div class="page-content-wrapper">
                            <div class="page-content">
                                <div class="container">
                                    <ul class="page-breadcrumb breadcrumb">
                                        <li>
                                            <a href="welcome.do">Admin Panel</a>
                                            <span style="font-size: 14px; margin: 0 auto;" class="fa fa-angle-right"></span>
                                        </li>
                                        <li>
                                            <a href="javascript:;">Manage Operations</a>
                                            <span style="font-size: 14px; margin: 0 auto;" class="fa fa-angle-right"></span>
                                        </li>
                                        <li>
                                            <span id="features" data-class-active=".manageOperations .searchUser">Search User</span>
                                        </li>
                                    </ul>
                                    <%if (message != null && message.length() > 0) {%>
                                    <div id="prefix_661213402591" class="custom-alerts alert alert-success fade in">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                        <strong> <%=message%> </strong>
                                    </div>
                                    <%}%>
                                    <div class="page-content-inner">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="tabbable-line boxless tabbable-reversed">
                                                    <div class="tab-content" style="padding-top: 15px;">
                                                        <div class="portlet box green">
                                                            <div class="portlet-title">
                                                                <div class="caption">
                                                                    <i class="fa fa-search"></i> Search User </div>
                                                                <div class="tools">
                                                                    <a href="javascript:;" class="collapse"> </a>
                                                                </div>
                                                            </div>
                                                            <div class="portlet-body form">
                                                                <html:form action="searchUser" styleClass="form-horizontal" method="GET"  acceptCharset="UTF-8">
                                                                    <div class="form-body">
                                                                        <div class="form-group">
                                                                            <div class="col-md-12 col-sm-12">
                                                                                <div class="input-group" style="float: right;">
                                                                                    <input name="searchText" type="text" class="form-control input-sm input-small" placeholder="ringId or userId" title="ringId or userId" value="<bean:write name="SearchUserForm" property="searchText" />"/>
                                                                                    <span>
                                                                                        <button class="btn green-soft uppercase bold input-sm" type="submit"><i class="icon-magnifier"></i></button>
                                                                                        <input name="action" type="hidden" value="<%=Constants.SEARCH%>" />
                                                                                    </span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <c:choose>
                                                                            <c:when test="${SearchUserForm.searchUserDTO ne null}">
                                                                                <div class="table-scrollable">
                                                                                    <table class="table table-striped table-bordered table-hover">
                                                                                        <tr>
                                                                                            <th style="vertical-align: middle;"> 
                                                                                                User Identity
                                                                                            </th>
                                                                                            <td>
                                                                                                ${SearchUserForm.searchUserDTO.getUserDetailsDTO().getRingId()}
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <th style="vertical-align: middle;"> 
                                                                                                User TableId
                                                                                            </th>
                                                                                            <td>
                                                                                                ${SearchUserForm.searchUserDTO.getUserDetailsDTO().getUtid()}
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <th style="vertical-align: middle;"> 
                                                                                                Full Name
                                                                                            </th>
                                                                                            <td>
                                                                                                ${SearchUserForm.searchUserDTO.getUserDetailsDTO().getFullName()}
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <th style="vertical-align: middle;"> 
                                                                                                Profile Image
                                                                                            </th>
                                                                                            <td>
                                                                                                <c:choose>
                                                                                                    <c:when test="${SearchUserForm.searchUserDTO.getUserDetailsDTO().getProfileImageUrl() != null && SearchUserForm.searchUserDTO.getUserDetailsDTO().getProfileImageUrl().length() > 0}">
                                                                                                        <img src="<%=Constants.MEDIA_CLOUD_BASE_URL%>${SearchUserForm.searchUserDTO.getUserDetailsDTO().getProfileImageUrl()}" style="max-height: 250px;max-width: 250px"/>
                                                                                                    </c:when>
                                                                                                    <c:otherwise>
                                                                                                        <img src="resources/images/default-pp.jpg" style="max-height: 250px;max-width: 250px"/>
                                                                                                    </c:otherwise>
                                                                                                </c:choose>                                                                                                                                   
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <th style="vertical-align: middle;"> 
                                                                                                Cover Image
                                                                                            </th>
                                                                                            <td>
                                                                                                <c:choose>
                                                                                                    <c:when test="${SearchUserForm.searchUserDTO.getUserDetailsDTO().getCoverImageUrl() != null && SearchUserForm.searchUserDTO.getUserDetailsDTO().getCoverImageUrl().length() > 0}">
                                                                                                        <img src="<%=Constants.MEDIA_CLOUD_BASE_URL%>${SearchUserForm.searchUserDTO.getUserDetailsDTO().getCoverImageUrl()}" style="max-height: 250px;max-width: 250px"/>
                                                                                                    </c:when>
                                                                                                    <c:otherwise>
                                                                                                        <img src="resources/images/no-cover.png" style="max-height: 250px;max-width: 250px"/>
                                                                                                    </c:otherwise>
                                                                                                </c:choose>                                                                                        
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <th style="vertical-align: middle;"> 
                                                                                                User Type
                                                                                            </th>
                                                                                            <td>
                                                                                                ${SearchUserForm.searchUserDTO.getUserDetailsDTO().getUserTypeStr()}
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <th style="vertical-align: middle;"> 
                                                                                                Gender
                                                                                            </th>
                                                                                            <td>
                                                                                                ${SearchUserForm.searchUserDTO.getUserDetailsDTO().getGender()}
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <th style="vertical-align: middle;"> 
                                                                                                Phone
                                                                                            </th>
                                                                                            <td>
                                                                                                ${SearchUserForm.searchUserDTO.getUserDetailsDTO().getDialingCode()}-${SearchUserForm.searchUserDTO.getUserDetailsDTO().getMobilePhone()}
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <th style="vertical-align: middle;"> 
                                                                                                Secondary Phone
                                                                                            </th>
                                                                                            <td>
                                                                                                ${SearchUserForm.searchUserDTO.getUserDetailsDTO().getSecondaryDialingCode()}-${SearchUserForm.searchUserDTO.getUserDetailsDTO().getSecondaryMobileNumber()}
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <th style="vertical-align: middle;"> 
                                                                                                Email
                                                                                            </th>
                                                                                            <td>
                                                                                                ${SearchUserForm.searchUserDTO.getUserDetailsDTO().getEmail()}
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <th style="vertical-align: middle;"> 
                                                                                                Secondary Email
                                                                                            </th>
                                                                                            <td>
                                                                                                ${SearchUserForm.searchUserDTO.getUserDetailsDTO().getSecondaryEmail()}
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <th style="vertical-align: middle;"> 
                                                                                                Birthday
                                                                                            </th>
                                                                                            <td>
                                                                                                ${SearchUserForm.searchUserDTO.getUserDetailsDTO().getBirthday()}
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <th style="vertical-align: middle;"> 
                                                                                                Home City
                                                                                            </th>
                                                                                            <td>
                                                                                                ${SearchUserForm.searchUserDTO.getUserDetailsDTO().getHomeCity()}
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <th style="vertical-align: middle;"> 
                                                                                                Current City
                                                                                            </th>
                                                                                            <td>
                                                                                                ${SearchUserForm.searchUserDTO.getUserDetailsDTO().getCurrentCity()}
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <th style="vertical-align: middle;"> 
                                                                                                Country
                                                                                            </th>
                                                                                            <td>
                                                                                                ${SearchUserForm.searchUserDTO.getUserDetailsDTO().getCountry()}
                                                                                            </td>
                                                                                        </tr>
                                                                                        <c:if test="${SearchUserForm.searchUserDTO.userPageMap ne null}">
                                                                                            <tr>
                                                                                                <th style="vertical-align: middle;"> 
                                                                                                    Page Info
                                                                                                </th>
                                                                                                <td>
                                                                                                    <div style="overflow-y: scroll; width: 925px">
                                                                                                        <table class="table table-striped table-bordered table-hover">
                                                                                                            <thead>
                                                                                                                <tr>
                                                                                                                    <th style="text-align: center">PageId</th>
                                                                                                                    <th style="text-align: center">Page RingId</th>
                                                                                                                    <th style="text-align: center">Page Name</th>
                                                                                                                    <th style="text-align: center">Page Type</th>
                                                                                                                    <th style="text-align: center">Page Status</th>
                                                                                                                </tr>
                                                                                                            </thead>
                                                                                                            <c:forEach items="${SearchUserForm.searchUserDTO.getUserPageMap()}" var="pagelist">
                                                                                                                <c:forEach items="${pagelist.value}" var="page">
                                                                                                                    <tr>
                                                                                                                        <td style="text-align: center"><c:out value="${page.getPageId()}"/></td>
                                                                                                                        <td style="text-align: center"><c:out value="${page.getRingId()}"/></td>
                                                                                                                        <td style="text-align: center"><c:out value="${page.getServiceName()}"/></td>
                                                                                                                        <td style="text-align: center"><c:out value="${pagelist.key}"/></td>
                                                                                                                        <td style="text-align: center"><c:out value="${page.getPageActiveStatus()}"/></td>
                                                                                                                    </tr>
                                                                                                                </c:forEach>
                                                                                                            </c:forEach>
                                                                                                        </table>
                                                                                                    </div>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </c:if>
                                                                                        <c:if test="${SearchUserForm.searchUserDTO.getUserDetailsDTO().getUserTypeStr() ne 'DEFAULT USER'}">
                                                                                            <tr>
                                                                                                <th style="vertical-align: middle;"> 
                                                                                                    Page Owner Info
                                                                                                </th>
                                                                                                <td>
                                                                                                    <table class="table table-striped table-bordered table-hover order-column">
                                                                                                        <tr>
                                                                                                            <th style="vertical-align: middle;"> 
                                                                                                                Owner Id
                                                                                                            </th>
                                                                                                            <td>
                                                                                                                ${SearchUserForm.searchUserDTO.getPageOwnerInfo().getUserId()}
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <th style="vertical-align: middle;"> 
                                                                                                                Ring Id
                                                                                                            </th>
                                                                                                            <td>
                                                                                                                ${SearchUserForm.searchUserDTO.getPageOwnerInfo().getRingId()}
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <th style="vertical-align: middle;"> 
                                                                                                                Name
                                                                                                            </th>
                                                                                                            <td>
                                                                                                                ${SearchUserForm.searchUserDTO.getPageOwnerInfo().getFirstName()}
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <th style="vertical-align: middle;"> 
                                                                                                                Profile Image
                                                                                                            </th>
                                                                                                            <td>
                                                                                                                <c:choose>
                                                                                                                    <c:when test="${SearchUserForm.searchUserDTO.getPageOwnerInfo().getProfileImageURL() != null && SearchUserForm.searchUserDTO.getPageOwnerInfo().getProfileImageURL().length() > 0}">
                                                                                                                        <img src="<%=Constants.MEDIA_CLOUD_BASE_URL%>${SearchUserForm.searchUserDTO.getPageOwnerInfo().getProfileImageURL()}" style="max-height: 250px;max-width: 250px"/>
                                                                                                                    </c:when>
                                                                                                                    <c:otherwise>
                                                                                                                        <img src="resources/images/no-cover.png" style="max-height: 250px;max-width: 250px"/>
                                                                                                                    </c:otherwise>
                                                                                                                </c:choose>       
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </c:if>
                                                                                        <tr>
                                                                                            <th style="vertical-align: middle;"> 
                                                                                                Device Information
                                                                                            </th>
                                                                                            <td>
                                                                                                <div style="overflow-y: scroll; width: 925px">
                                                                                                    <table class="table table-striped table-bordered table-hover">
                                                                                                        <thead>
                                                                                                            <tr>
                                                                                                                <th style="text-align: center">Device Type</th>
                                                                                                                <th style="text-align: center">Device Category</th>
                                                                                                                <th style="text-align: center">App Type</th>
                                                                                                                <th style="text-align: center">App Version</th>
                                                                                                                <th style="text-align: center">Device Id</th>
                                                                                                                <th style="text-align: center">Device Name</th>
                                                                                                                <th style="text-align: center">Device Token</th>
                                                                                                            </tr>
                                                                                                        </thead>
                                                                                                        <c:forEach items="${SearchUserForm.searchUserDTO.getUserDeviceList()}" var="device">
                                                                                                            <tr>
                                                                                                                <c:if test="${device.getDeviceType() eq 1}">
                                                                                                                    <td style="text-align: center">Desktop</td>
                                                                                                                </c:if>
                                                                                                                <c:if test="${device.getDeviceType() eq 2}">
                                                                                                                    <td style="text-align: center">Android</td>
                                                                                                                </c:if>
                                                                                                                <c:if test="${device.getDeviceType() eq 3}">
                                                                                                                    <td style="text-align: center">iOS</td>
                                                                                                                </c:if>
                                                                                                                <c:if test="${device.getDeviceType() eq 4}">
                                                                                                                    <td style="text-align: center">Windows Phone</td>
                                                                                                                </c:if>
                                                                                                                <c:if test="${device.getDeviceType() eq 5}">
                                                                                                                    <td style="text-align: center">Web</td>
                                                                                                                </c:if>
                                                                                                                <td style="text-align: center"><c:out value="${device.getDeviceCategory()}"/></td>
                                                                                                                <td style="text-align: center"><c:out value="${device.getAppType()}"/></td>
                                                                                                                <td style="text-align: center"><c:out value="${device.getAppVersion()}"/></td>
                                                                                                                <td style="text-align: center"><c:out value="${device.getDeviceID()}"/></td>
                                                                                                                <td style="text-align: center"><c:out value="${device.getDeviceName()}"/></td>
                                                                                                                <td style="text-align: center"><c:out value="${device.getDeviceToken()}"/></td>
                                                                                                            </tr>
                                                                                                        </c:forEach>
                                                                                                    </table>
                                                                                                </div>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <th style="vertical-align: middle;"> 
                                                                                                About
                                                                                            </th>
                                                                                            <td>
                                                                                                ${SearchUserForm.searchUserDTO.getUserDetailsDTO().getAboutMe()}
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </div>
                                                                            </c:when>
                                                                        </c:choose>
                                                                    </div>
                                                                    <logic:equal name="SearchUserForm" property="userType" value="1">
                                                                        <%if ((loginDTO.getPermissionLevel() == Constants.SUPER_ADMIN) || (loginDTO.getFeauterMapList().containsKey(FEATURE_NAME) && loginDTO.getFeauterMapList().get(FEATURE_NAME).getHasModifyPermission())) {%>
                                                                        <div class="form-actions">
                                                                            <div class="row">
                                                                                <div class="col-md-offset-3 col-md-9">
                                                                                    <button type="submit" class="btn green" name="submitType" value="Deactivate" onclick="javascript:return confirm('Are you sure you want to deactivate this account?')">Deactivate</button>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <% }%>
                                                                    </logic:equal>
                                                                </html:form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <%@include file="/shared/footer.jsp"%>
        </div>
        <%@include file="/shared/script-list.jsp"%>
    </body>
</html>
