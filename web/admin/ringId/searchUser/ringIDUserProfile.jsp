<%-- 
    Document   : ringIDUserProfile
    Created on : Feb 10, 2018, 2:41:38 PM
    Author     : Rabby
--%>

<%@page import="com.ringid.admin.projectMenu.MenuNames"%>
<%@include file="/login/loginTimeExpiry.jsp"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="org.ringid.newsfeeds.CassMultiMediaDTO"%>
<%@ page import="java.util.*,com.ringid.admin.utils.Constants"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@include  file="/shared/head.jsp" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <style>
            .timeline::before{
                content: none !important;
            }
        </style>
        <title>Admin Panel | User Profile</title>
        <%@include file="/shared/css-list.jsp"%>
    </head>
    <body class="page-container-bg-solid">
        <div class="page-wrapper">
            <div class="page-wrapper-row">
                <div class="page-wrapper-top">
                    <div class="page-header">
                        <%@include  file="/shared/menu.jsp" %>
                        <%
                            String FEATURE_NAME = MenuNames.SEARCH_USER;
                            String message = "";
                            if (request.getAttribute("message") != null) {
                                message = request.getAttribute("message").toString();
                                request.removeAttribute("message");
                            }
                        %>
                    </div>
                </div>
            </div>
            <div class="page-wrapper-row full-height">
                <div class="page-wrapper-middle">
                    <div class="page-container">
                        <div class="page-content-wrapper">
                            <div class="page-content">
                                <div class="container">
                                    <ul class="page-breadcrumb breadcrumb">
                                        <li>
                                            <a href="welcome.do">Admin Panel</a>
                                            <span style="font-size: 14px; margin: 0 auto;" class="fa fa-angle-right"></span>
                                        </li>
                                        <li>
                                            <a href="javascript:;">Manage Operations</a>
                                            <span style="font-size: 14px; margin: 0 auto;" class="fa fa-angle-right"></span>
                                        </li>
                                        <li>
                                            <span id="features" data-class-active=".manageOperations .searchUser">User Profile</span>
                                        </li>
                                    </ul>
                                    <%if (message != null && message.length() > 0) {%>
                                    <div id="prefix_661213402591" class="custom-alerts alert alert-success fade in">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                        <strong> <%=message%> </strong>
                                    </div>
                                    <%}%>
                                    <div class="page-content-inner">
                                        <html:form action="searchUserContacts" styleClass="form-horizontal" method="GET"  acceptCharset="UTF-8">
                                            <div class="search-page search-content-4">
                                                <div class="search-bar-custom">
                                                    <div class="row">
                                                        <div class="col-lg-12">
                                                            <div class="input-group">
                                                                <input type="text" name="searchText" value="<bean:write name="SearchUserForm" property="searchText"/>" class="form-control" placeholder="ringId, utId, phone OR name">
                                                                <span class="input-group-btn">
                                                                    <button class="btn green-soft uppercase bold" type="submit">Search</button>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </html:form>
                                        <html:form action="searchUser2" styleClass="" method="GET"  acceptCharset="UTF-8">
                                            <c:choose>
                                                <c:when test="${SearchUserForm.searchUserDTO ne null}">
                                                    <input type="hidden" name="searchText" id="utId" value="${SearchUserForm.searchUserDTO.getUserDetailsDTO().getUtid()}"/>
                                                    <input type="hidden" id="ringId" value="${SearchUserForm.searchUserDTO.getUserDetailsDTO().getRingId()}"/>
                                                    <input type="hidden" id="userType" value="${SearchUserForm.searchUserDTO.getUserDetailsDTO().getUserType()}"/>
                                                    <html:hidden styleId="viewType" property="viewType"/>
                                                    <%
                                                        String activeOverview = "", activeSetting = "";
                                                    %>
                                                    <c:choose>
                                                        <c:when test="${SearchUserForm.getViewType() eq 2}">
                                                            <%activeSetting = "active";%>
                                                        </c:when>
                                                        <c:otherwise>
                                                            <%activeOverview = "active";%>
                                                        </c:otherwise>
                                                    </c:choose>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <!-- BEGIN PROFILE SIDEBAR -->
                                                            <div class="profile-sidebar" style="width: 350px;margin-right: 10px;">
                                                                <!-- PORTLET MAIN -->
                                                                <div class="portlet light profile-sidebar-portlet " style="padding: 0px !important;">
                                                                    <!-- Cover image -->
                                                                    <a href="searchUser2.do?searchText=${SearchUserForm.searchUserDTO.getUserDetailsDTO().getRingId()}" style="background-color: #1DA1F2 !important;height: 120px;;display: block !important;">
                                                                        <c:choose>
                                                                            <c:when test="${SearchUserForm.searchUserDTO.getUserDetailsDTO().getCoverImageUrl() != null && SearchUserForm.searchUserDTO.getUserDetailsDTO().getCoverImageUrl().length() > 0}">
                                                                                <img src="<%=Constants.MEDIA_CLOUD_BASE_URL%>${SearchUserForm.searchUserDTO.getUserDetailsDTO().getCoverImageUrl()}" class="img-responsive" alt="" style="height: 100%;width: 100%;">
                                                                            </c:when>
                                                                            <c:otherwise>
                                                                                <img src="assets/global/img/default_cover.jpg" class="img-responsive" alt="" style="height: 100%;width: 100%;">
                                                                            </c:otherwise>
                                                                        </c:choose>
                                                                    </a>
                                                                    <div style="height: 80px;">
                                                                        <!-- SIDEBAR USERPIC -->
                                                                        <a class="profile-info-avatarLink" href="searchUser2.do?searchText=${SearchUserForm.searchUserDTO.getUserDetailsDTO().getRingId()}">
                                                                            <c:choose>
                                                                                <c:when test="${SearchUserForm.searchUserDTO.getUserDetailsDTO().getProfileImageUrl() != null && SearchUserForm.searchUserDTO.getUserDetailsDTO().getProfileImageUrl().length() > 0}">
                                                                                    <img id='proImage' class="profile-info-avatarImage" src="<%=Constants.MEDIA_CLOUD_BASE_URL%>${SearchUserForm.searchUserDTO.getUserDetailsDTO().getProfileImageUrl()}" class="img-responsive" alt="">
                                                                                </c:when>
                                                                                <c:otherwise>
                                                                                    <img id='proImage' class="profile-info-avatarImage" src="assets/global/img/default-pp.jpg" class="img-responsive" alt="">
                                                                                </c:otherwise>
                                                                            </c:choose>
                                                                        </a>
                                                                        <!-- END SIDEBAR USERPIC -->
                                                                        <div class="profile-info-userFields">
                                                                            <div class="profile-info-name">
                                                                                <a href="searchUser2.do?searchText=${SearchUserForm.searchUserDTO.getUserDetailsDTO().getRingId()}" style="color: inherit !important;">
                                                                                    ${SearchUserForm.searchUserDTO.getUserDetailsDTO().getFullName()}
                                                                                </a>
                                                                                <span style="font-size: 13px;">
                                                                                    <c:if test="${SearchUserForm.searchUserDTO.getUserDetailsDTO().getUserType() ne 1 && SearchUserForm.searchUserDTO.getUserDetailsDTO().getIsPageVerified() eq true}">
                                                                                        <i class="fa fa-check-circle" style="color: #1DA1F2;"></i>
                                                                                    </c:if>
                                                                                </span>
                                                                            </div>
                                                                            <span class="profile-info-userInfo">
                                                                                <span class="profile-info-userId">
                                                                                    <b style="font-weight: normal">
                                                                                        ${SearchUserForm.searchUserDTO.getUserDetailsDTO().getRingId()} <i class="fa fa-circle" style="font-size: 6px;top: -2px;position: relative;margin: 0 2px 0 2px;"></i>
                                                                                        ${SearchUserForm.searchUserDTO.getUserDetailsDTO().getUtid()} <i class="fa fa-circle" style="font-size: 6px;top: -2px;position: relative;margin: 0 2px 0 2px;"></i>
                                                                                        ${SearchUserForm.searchUserDTO.getUserDetailsDTO().getUserTypeStr()} <i class="fa fa-circle" style="font-size: 6px;top: -2px;position: relative;margin: 0 2px 0 2px;"></i>
                                                                                        ${SearchUserForm.searchUserDTO.getUserDetailsDTO().getActiveStatusStr()}
                                                                                        <c:if test="${SearchUserForm.searchUserDTO.getUserDetailsDTO().isSpamUser() eq true}">
                                                                                            <i class="fa fa-circle" style="font-size: 6px;top: -2px;position: relative;margin: 0 2px 0 2px;"></i> <span style="color: red;">Spammed</span>
                                                                                        </c:if>
                                                                                    </b>
                                                                                </span>
                                                                                <!--</a>-->
                                                                            </span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="profile-userbuttons" style="max-width: 350px;margin-top: 15px;padding-bottom: 15px;">
                                                                        <%if ((loginDTO.getPermissionLevel() == Constants.SUPER_ADMIN) || (loginDTO.getFeauterMapList().containsKey(FEATURE_NAME) && loginDTO.getFeauterMapList().get(FEATURE_NAME).getHasModifyPermission())) {%>
                                                                        <logic:equal name="SearchUserForm" property="userType" value="1">
                                                                            <button type="submit" name="submitType" value="Deactivate" title="Deactivate" onclick="javascript:return confirm('Are you sure you want to deactivate this account?')" class="btn btn-circle red btn-sm">Deactivate</button>
                                                                        </logic:equal>
                                                                        <logic:equal name="SearchUserForm" property="userType" value="3">
                                                                            <a href="updateUserPageStatus.do?pageId=${SearchUserForm.searchUserDTO.getUserDetailsDTO().getUtid()}&pageStatusUpdateType=makeDefault&userId=${SearchUserForm.searchUserDTO.getPageOwnerInfo().getUserId()}&searchText=<bean:write name='SearchUserForm' property='searchText'/>" class="btn btn-circle green btn-sm" title="Make Page Default" onclick="javascript:return confirm('Are you sure you want to make this user page default?')">Make Default</a>
                                                                            <a href="editPageFollowRules.do?pageId=${SearchUserForm.searchUserDTO.getUserDetailsDTO().getUtid()}" class="btn btn-circle green btn-sm" title="Make Featured" target="_blank">Make Featured</a>
                                                                            <a href="updateUserPageStatus.do?userId=${SearchUserForm.searchUserDTO.getUserDetailsDTO().getUtid()}&pageStatusUpdateType=makeCelebrity&searchText=<bean:write name='SearchUserForm' property='searchText'/>" class="btn btn-circle green btn-sm" title="Make Celebrity" onclick="javascript:return confirm('Are you sure you want to make this account celebrity?')">Make Celebrity</a>
                                                                        </logic:equal>
                                                                        <logic:equal name="SearchUserForm" property="userType" value="10">
                                                                            <a href="updateUserPageStatus.do?userId=${SearchUserForm.searchUserDTO.getUserDetailsDTO().getUtid()}&pageStatusUpdateType=removeCelebrity&searchText=<bean:write name='SearchUserForm' property='searchText'/>" class="btn btn-circle red btn-sm" title="Remove From Celebrity" onclick="javascript:return confirm('Are you sure you want to remove this account from celebrity?')">Remove Celebrity</a>
                                                                        </logic:equal>
                                                                        <logic:notEqual name="SearchUserForm" property="userType" value="1">
                                                                            <c:choose>
                                                                                <c:when test="${SearchUserForm.searchUserDTO.getUserDetailsDTO().getIsPageVerified() eq true}">
                                                                                    <a style="margin-top: 10px;" href="updateUserPageStatus.do?userId=${SearchUserForm.searchUserDTO.getUserDetailsDTO().getUtid()}&userType=${SearchUserForm.searchUserDTO.getUserDetailsDTO().getUserType()}&pageStatusUpdateType=unverifyPage&searchText=<bean:write name='SearchUserForm' property='searchText'/>" class="btn btn-circle red btn-sm" title="Unverfiy Page" onclick="javascript:return confirm('Are you sure you want to unverfiy this page?')">Unverify</a>
                                                                                </c:when>
                                                                                <c:otherwise>
                                                                                    <a style="margin-top: 10px;" href="updateUserPageStatus.do?userId=${SearchUserForm.searchUserDTO.getUserDetailsDTO().getUtid()}&userType=${SearchUserForm.searchUserDTO.getUserDetailsDTO().getUserType()}&pageStatusUpdateType=verifyPage&searchText=<bean:write name='SearchUserForm' property='searchText'/>" class="btn btn-circle green btn-sm" title="Verify Page" onclick="javascript:return confirm('Are you sure you want to verify this page?')">Verify</a>
                                                                                </c:otherwise>
                                                                            </c:choose>
                                                                        </logic:notEqual>
                                                                        <% }%>
                                                                    </div>

                                                                    <!-- END SIDEBAR BUTTONS -->
                                                                    <!-- SIDEBAR MENU -->
                                                                    <div class="profile-usermenu">
                                                                        <ul class="nav">
                                                                            <li class="<%=activeOverview%>">
                                                                                <a href="searchUser2.do?searchText=${SearchUserForm.searchUserDTO.getUserDetailsDTO().getRingId()}">
                                                                                    <i class="icon-home"></i> Overview 
                                                                                </a>
                                                                            </li>
                                                                            <%if (loginDTO.getPermissionLevel() == Constants.SUPER_ADMIN) {%>
                                                                            <c:if test="${SearchUserForm.searchUserDTO.getUserDetailsDTO().getUserType() eq 1}">
                                                                                <li class="<%=activeSetting%>">
                                                                                    <a href="searchUser2.do?searchText=${SearchUserForm.searchUserDTO.getUserDetailsDTO().getRingId()}&viewType=2">
                                                                                        <i class="icon-settings"></i> Account Settings
                                                                                    </a>
                                                                                </li>
                                                                            </c:if>
                                                                            <%}%>
                                                                        </ul>
                                                                    </div>
                                                                    <!-- END MENU -->
                                                                </div>
                                                                <!-- END PORTLET MAIN -->
                                                                <!-- PORTLET MAIN -->
                                                                <div class="portlet light ">
                                                                    <!-- STAT -->
                                                                    <div class="row list-separated profile-stat">
                                                                        <c:choose>
                                                                            <c:when test="${SearchUserForm.getUserType() == 1}">
                                                                                <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                    <div class="uppercase profile-stat-title"> ${SearchUserForm.searchUserDTO.getUserDetailsDTO().getFollowerCount()} </div>
                                                                                    <div class="uppercase profile-stat-text"> Follower </div>
                                                                                </div>
                                                                                <div class="col-md-6 col-sm-6 col-xs-6">
                                                                                    <div class="uppercase profile-stat-title"> ${SearchUserForm.searchUserDTO.getUserDetailsDTO().getFollowingCount()} </div>
                                                                                    <div class="uppercase profile-stat-text"> Following </div>
                                                                                </div>
                                                                            </c:when>
                                                                            <c:otherwise>
                                                                                <div class="col-md-12 col-sm-12 col-xs-12">
                                                                                    <div class="uppercase profile-stat-title"> ${SearchUserForm.searchUserDTO.getUserDetailsDTO().getFollowerCount()} </div>
                                                                                    <div class="uppercase profile-stat-text"> Follower </div>
                                                                                </div>
                                                                            </c:otherwise>
                                                                        </c:choose>
                                                                    </div>
                                                                    <!-- END STAT -->
                                                                    <div>
                                                                        <h4 class="profile-desc-title">About</h4>
                                                                        <span class="profile-desc-text">
                                                                            ${SearchUserForm.searchUserDTO.getUserDetailsDTO().getAboutMe()}
                                                                        </span>
                                                                        <c:if test="${(SearchUserForm.searchUserDTO.getUserDetailsDTO().getMobilePhone() ne null) and (SearchUserForm.searchUserDTO.getUserDetailsDTO().getMobilePhone() ne '')}">
                                                                            <div class="margin-top-20 profile-desc-link font-blue">
                                                                                <i class="fa fa-phone"></i>
                                                                                ${SearchUserForm.searchUserDTO.getUserDetailsDTO().getDialingCode()}${SearchUserForm.searchUserDTO.getUserDetailsDTO().getMobilePhone()}
                                                                            </div>
                                                                        </c:if>
                                                                        <c:if test="${(SearchUserForm.searchUserDTO.getUserDetailsDTO().getSecondaryMobileNumber() ne null) and (SearchUserForm.searchUserDTO.getUserDetailsDTO().getSecondaryMobileNumber() ne '')}">
                                                                            <div class="margin-top-20 profile-desc-link font-grey-cascade">
                                                                                <i class="fa fa-phone"></i>
                                                                                ${SearchUserForm.searchUserDTO.getUserDetailsDTO().getSecondaryDialingCode()}-${SearchUserForm.searchUserDTO.getUserDetailsDTO().getSecondaryMobileNumber()}
                                                                            </div>
                                                                        </c:if>
                                                                        <c:if test="${(SearchUserForm.searchUserDTO.getUserDetailsDTO().getEmail() ne null) and (SearchUserForm.searchUserDTO.getUserDetailsDTO().getEmail() ne '')}">
                                                                            <div class="margin-top-20 profile-desc-link font-blue">
                                                                                <i class="fa fa-envelope"></i>
                                                                                ${SearchUserForm.searchUserDTO.getUserDetailsDTO().getEmail()}
                                                                            </div>
                                                                        </c:if>
                                                                        <c:if test="${(SearchUserForm.searchUserDTO.getUserDetailsDTO().getSecondaryEmail() ne null) and (SearchUserForm.searchUserDTO.getUserDetailsDTO().getSecondaryEmail() ne '')}">
                                                                            <div class="margin-top-20 profile-desc-link font-grey-cascade">
                                                                                <i class="fa fa-envelope"></i>
                                                                                ${SearchUserForm.searchUserDTO.getUserDetailsDTO().getSecondaryEmail()}
                                                                            </div>
                                                                        </c:if>
                                                                        <c:if test="${(SearchUserForm.searchUserDTO.getUserDetailsDTO().getGender() ne null) and (SearchUserForm.searchUserDTO.getUserDetailsDTO().getGender() ne '')}">
                                                                            <div class="margin-top-20 profile-desc-link font-blue">
                                                                                <i class="fa fa-intersex"></i>
                                                                                ${SearchUserForm.searchUserDTO.getUserDetailsDTO().getGender()}
                                                                            </div>
                                                                        </c:if>
                                                                        <logic:equal name="SearchUserForm" property="userType" value="1">
                                                                            <c:if test="${(SearchUserForm.searchUserDTO.getUserDetailsDTO().getBirthday() ne null) and (SearchUserForm.searchUserDTO.getUserDetailsDTO().getBirthday() ne '')}">
                                                                                <div class="margin-top-20 profile-desc-link font-blue">
                                                                                    <i class="fa fa-calendar"></i>
                                                                                    ${SearchUserForm.searchUserDTO.getUserDetailsDTO().getBirthday()}
                                                                                </div>
                                                                            </c:if>
                                                                        </logic:equal>
                                                                        <c:if test="${(SearchUserForm.searchUserDTO.getUserDetailsDTO().getHomeCity() ne null) and (SearchUserForm.searchUserDTO.getUserDetailsDTO().getHomeCity() ne '')}">
                                                                            <div class="margin-top-20 profile-desc-link font-blue">
                                                                                <i class="fa fa-home"></i>
                                                                                ${SearchUserForm.searchUserDTO.getUserDetailsDTO().getHomeCity()}
                                                                            </div>
                                                                        </c:if>
                                                                        <c:if test="${(SearchUserForm.searchUserDTO.getUserDetailsDTO().getCurrentCity() ne null) and (SearchUserForm.searchUserDTO.getUserDetailsDTO().getCurrentCity() ne '')}">
                                                                            <div class="margin-top-20 profile-desc-link font-blue">
                                                                                <i class="fa fa-map-marker"></i>
                                                                                ${SearchUserForm.searchUserDTO.getUserDetailsDTO().getCurrentCity()}
                                                                            </div>
                                                                        </c:if>
                                                                        <c:if test="${(SearchUserForm.searchUserDTO.getUserDetailsDTO().getCountry() ne null) and (SearchUserForm.searchUserDTO.getUserDetailsDTO().getCountry() ne '')}">
                                                                            <div class="margin-top-20 profile-desc-link font-blue">
                                                                                <i class="fa fa-globe"></i>
                                                                                ${SearchUserForm.searchUserDTO.getUserDetailsDTO().getCountry()}
                                                                            </div>
                                                                        </c:if>
                                                                        <c:if test="${(SearchUserForm.searchUserDTO.getUserDetailsDTO().getRegistrationDateStr() ne null) and (SearchUserForm.searchUserDTO.getUserDetailsDTO().getRegistrationDateStr() ne '')}">
                                                                            <div class="margin-top-20 profile-desc-link font-blue">
                                                                                <i class="fa fa-calendar"></i>
                                                                                Registration : ${SearchUserForm.searchUserDTO.getUserDetailsDTO().getRegistrationDateStr()}
                                                                            </div>
                                                                        </c:if>
                                                                        <c:if test="${(SearchUserForm.searchUserDTO.getUserDetailsDTO().getLastLoginTime() ne null) and (SearchUserForm.searchUserDTO.getUserDetailsDTO().getLastLoginTime() ne '')}">
                                                                            <div class="margin-top-20 profile-desc-link font-blue">
                                                                                <i class="fa fa-sign-in"></i>
                                                                                Last Seen : ${SearchUserForm.searchUserDTO.getUserDetailsDTO().getLastLoginTime()} (${SearchUserForm.searchUserDTO.getUserDetailsDTO().getLastLoginPlatform()})
                                                                            </div>
                                                                        </c:if>
                                                                    </div>
                                                                </div>
                                                                <!--Wallet Info -->
                                                                <c:if test="${SearchUserForm.searchUserDTO.getUserDetailsDTO().getUserType() eq 1}">
                                                                    <div class="portlet light ">
                                                                        <div class="portlet-title">
                                                                            <div class="caption caption-md">
                                                                                <i class="icon-bar-chart theme-font hide"></i>
                                                                                <span class="caption-subject font-blue-madison bold uppercase">Wallet Info</span>
                                                                            </div>
                                                                        </div>
                                                                        <div class="portlet-body">
                                                                            <div class="margin-top-20 profile-desc-link font-blue">
                                                                                <span>Total Coin: </span>
                                                                                <span id="totalCoin">0</span>
                                                                            </div>
                                                                            <div class="margin-top-20 profile-desc-link font-blue">
                                                                                <span>Gift Coin: </span>
                                                                                <span id="giftCoin">0</span>
                                                                            </div>
                                                                            <div class="margin-top-20 profile-desc-link font-blue">
                                                                                <span>Blocked Earned Coin: </span>
                                                                                <span id="blockedEarnedCoin">0</span>
                                                                            </div>
                                                                            <div class="margin-top-20 profile-desc-link font-blue">
                                                                                <span>Blocked Gift Coin: </span>
                                                                                <span id="blockedGiftCoin">0</span>
                                                                            </div>
                                                                            <div class="margin-top-20 profile-desc-link font-blue">
                                                                                <span>My Coin: </span>
                                                                                <span id="myCoin">0</span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </c:if>
                                                                <!-- Page Owner info-->
                                                                <c:if test="${SearchUserForm.searchUserDTO.getUserDetailsDTO().getUserType() ne 1}">
                                                                    <div class="portlet light ">
                                                                        <div class="portlet-title">
                                                                            <div class="caption caption-md">
                                                                                <i class="icon-bar-chart theme-font hide"></i>
                                                                                <span class="caption-subject font-blue-madison bold uppercase">Page Owner</span>
                                                                            </div>
                                                                        </div>
                                                                        <div class="portlet-body">
                                                                            <div style="height: 80px;">
                                                                                <!-- SIDEBAR USERPIC -->
                                                                                <a class="profile-info-avatarLink" href="searchUser2.do?searchText=${SearchUserForm.searchUserDTO.getPageOwnerInfo().getRingId()}" style="margin: 0px;">
                                                                                    <c:choose>
                                                                                        <c:when test="${SearchUserForm.searchUserDTO.getPageOwnerInfo().getProfileImageURL() != null && SearchUserForm.searchUserDTO.getPageOwnerInfo().getProfileImageURL().length() > 0}">
                                                                                            <img id='proImage' class="profile-info-avatarImage" src="<%=Constants.MEDIA_CLOUD_BASE_URL%>${SearchUserForm.searchUserDTO.getPageOwnerInfo().getProfileImageURL()}" class="img-responsive" alt="">
                                                                                        </c:when>
                                                                                        <c:otherwise>
                                                                                            <img id='proImage' class="profile-info-avatarImage" src="assets/global/img/default-pp.jpg" class="img-responsive" alt="">
                                                                                        </c:otherwise>
                                                                                    </c:choose>
                                                                                </a>
                                                                                <!-- END SIDEBAR USERPIC -->
                                                                                <div class="profile-info-userFields" style="position: relative;top: 5px;left: 85px;">
                                                                                    <div class="profile-info-name">
                                                                                        <a href="searchUser2.do?searchText=${SearchUserForm.searchUserDTO.getPageOwnerInfo().getRingId()}" >
                                                                                            ${SearchUserForm.searchUserDTO.getPageOwnerInfo().getFirstName()}
                                                                                        </a>
                                                                                    </div>
                                                                                    <span class="profile-info-userInfo">
                                                                                        <span class="profile-info-userId">
                                                                                            <b style="font-weight: normal">
                                                                                                ${SearchUserForm.searchUserDTO.getPageOwnerInfo().getRingId()} <i class="fa fa-circle" style="font-size: 6px;top: -2px;position: relative;margin: 0 2px 0 2px;"></i>
                                                                                                ${SearchUserForm.searchUserDTO.getPageOwnerInfo().getUserId()} <i class="fa fa-circle" style="font-size: 6px;top: -2px;position: relative;margin: 0 2px 0 2px;"></i>
                                                                                                ${SearchUserForm.searchUserDTO.getPageOwnerInfo().getDialingCode()}${SearchUserForm.searchUserDTO.getPageOwnerInfo().getMobilePhone()}
                                                                                            </b>
                                                                                        </span>
                                                                                    </span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </c:if>
                                                                <!--Coin Transaction Info-->
                                                                <c:if test="${SearchUserForm.searchUserDTO.getUserDetailsDTO().getUserType() eq 1}">
                                                                    <div class="tabbable-line boxless tabbable-reversed">
                                                                        <ul class="nav nav-tabs">
                                                                            <li class="active">
                                                                                <a href="#tab_0" data-toggle="tab">Available Coin</a>
                                                                            </li>
                                                                            <li class="">
                                                                                <a href="#tab_1" data-toggle="tab">Gift Coin</a>
                                                                            </li>
                                                                        </ul>
                                                                        <div class="tab-content" style="padding-top: 15px;">
                                                                            <div class="tab-pane active" id="tab_0">
                                                                                <div class="portlet light portlet-fit ">
                                                                                    <div class="portlet-title">
                                                                                        <div class="caption">
                                                                                            <span class="caption-subject bold font-green uppercase"> Available Coin </span>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="portlet-body">
                                                                                        <input type="hidden" id="available_coin_pvtId" value=""/>
                                                                                        <div class="timeline white-bg table-scrollable availableCoinList" style="overflow-y: scroll;max-height: 350px;display: none;">
                                                                                            <table id="availableCoinList" class="table table-striped table-bordered table-hover">
                                                                                                <thead>
                                                                                                    <tr>
                                                                                                        <th class="text-center">Amount</th>
                                                                                                        <th class="text-center">To/From</th>
                                                                                                        <th class="text-center">Status</th>
                                                                                                        <th class="text-center">Date</th>
                                                                                                    </tr>
                                                                                                </thead>
                                                                                                <tbody>
                                                                                                </tbody>
                                                                                            </table>
                                                                                        </div>
                                                                                        <div class="row text-center">
                                                                                            <button id='seeMoreAvailableCoin' type="button" class="btn btn-default mt-ladda-btn ladda-button btn-outline btn-circle seeMoreAvailableCoin" data-style="expand-left" data-spinner-color="#333" disabled="true">
                                                                                                <span class="ladda-label">Loading...</span>
                                                                                            </button>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="tab-pane " id="tab_1">
                                                                                <div class="portlet light portlet-fit ">
                                                                                    <div class="portlet-title">
                                                                                        <div class="caption">
                                                                                            <span class="caption-subject bold font-green uppercase"> Gift Coin </span>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="portlet-body">
                                                                                        <input type="hidden" id="gift_coin_pvtId" value=""/>
                                                                                        <div class="timeline white-bg table-scrollable giftCoinList" style="overflow-y: scroll;max-height: 350px;display: none;">
                                                                                            <table id="giftCoinList" class="table table-striped table-bordered table-hover">
                                                                                                <thead>
                                                                                                    <tr>
                                                                                                        <th class="text-center">Amount</th>
                                                                                                        <th class="text-center">To/From</th>
                                                                                                        <th class="text-center">Status</th>
                                                                                                        <th class="text-center">Date</th>
                                                                                                    </tr>
                                                                                                </thead>
                                                                                                <tbody>
                                                                                                </tbody>
                                                                                            </table>
                                                                                        </div>
                                                                                        <div class="row text-center">
                                                                                            <button id='seeMoreGiftCoin' type="button" class="btn btn-default mt-ladda-btn ladda-button btn-outline btn-circle seeMoreGiftCoin" data-style="expand-left" data-spinner-color="#333" disabled="true">
                                                                                                <span class="ladda-label">Loading...</span>
                                                                                            </button>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </c:if>
                                                                <!--Friend List-->
                                                                <c:if test="${SearchUserForm.searchUserDTO.getUserDetailsDTO().getUserType() eq 1}">
                                                                    <div class="portlet light portlet-fit ">
                                                                        <div class="portlet-title">
                                                                            <div class="caption">
                                                                                <span class="caption-subject bold font-green uppercase"> Friends</span>
                                                                                <span class="caption-helper">${SearchUserForm.searchUserDTO.getUserDetailsDTO().getFriendCount()}</span>
                                                                            </div>
                                                                        </div>
                                                                        <div class="portlet-body">
                                                                            <input type="hidden" id="friend_pvtId" value=""/>
                                                                            <div id="friendList" class="timeline white-bg" style="overflow-y: scroll;max-height: 350px;display: none;">
                                                                            </div>
                                                                            <div class="row text-center">
                                                                                <button id="seeMoreFriend" type="button" class="btn btn-default mt-ladda-btn ladda-button btn-outline btn-circle seeMoreFriend" data-style="expand-left" data-spinner-color="#333" disabled="true">
                                                                                    <span class="ladda-label">Loading...</span>
                                                                                </button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </c:if>
                                                                <!--Only for celebrity past live feed-->
                                                                <c:if test="${SearchUserForm.searchUserDTO.getUserDetailsDTO().getUserType() eq 10}">
                                                                    <div class="portlet light portlet-fit ">
                                                                        <div class="portlet-title">
                                                                            <div class="caption">
                                                                                <span class="caption-subject bold font-green uppercase"> Past Live Feed</span>
                                                                                <span class="caption-helper">own live feed</span>
                                                                            </div>
                                                                        </div>
                                                                        <div class="portlet-body">
                                                                            <input type="hidden" id="pastLive_pvtUUID" value=""/>
                                                                            <div id="pastLiveFeed" class="timeline white-bg" style="overflow-y: scroll;max-height: 350px;display: none;">
                                                                            </div>
                                                                            <div class="row text-center">
                                                                                <button id="pastLive_seeMoreFeed" type="button" class="btn btn-default mt-ladda-btn ladda-button btn-outline btn-circle pastLive_seeMoreFeed" data-style="expand-left" data-spinner-color="#333" disabled="true">
                                                                                    <span class="ladda-label">Loading...</span>
                                                                                </button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </c:if>
                                                                <!-- END PORTLET MAIN -->
                                                            </div>
                                                            <!-- END BEGIN PROFILE SIDEBAR -->
                                                            <!-- BEGIN PROFILE CONTENT -->
                                                            <div class="profile-content">
                                                                <c:choose>
                                                                    <c:when test="${SearchUserForm.getViewType() eq 2}">
                                                                        <!-- Account Settings -->
                                                                        <div class="row">
                                                                            <div class="col-md-12">
                                                                                <div class="portlet light ">
                                                                                    <div class="portlet-title tabbable-line">
                                                                                        <div class="caption caption-md">
                                                                                            <i class="icon-globe theme-font hide"></i>
                                                                                            <span class="caption-subject font-blue-madison bold uppercase">Account Settings</span>
                                                                                        </div>
                                                                                        <ul class="nav nav-tabs">
                                                                                            <li class="active">
                                                                                                <a href="#tab_1_3" data-toggle="tab">Change Password</a>
                                                                                            </li>
                                                                                        </ul>
                                                                                    </div>
                                                                                    <div class="portlet-body">
                                                                                        <div class="tab-content">
                                                                                            <!-- CHANGE PASSWORD TAB -->
                                                                                            <div class="tab-pane active" id="tab_1_3">
                                                                                                <div class="form-group">
                                                                                                    <label class="control-label">New Password</label>
                                                                                                    <input id="newPassword" type="text" class="form-control" /> 
                                                                                                </div>
                                                                                                <div class="margin-top-10">
                                                                                                    <a  href="javascript:changePassword(document.getElementById('ringId').value, document.getElementById('newPassword').value)" class="btn green" onclick="return confirm('Do you want to change password?');"> Change Password </a>
                                                                                                </div>
                                                                                            </div>
                                                                                            <!-- END CHANGE PASSWORD TAB -->
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </c:when>
                                                                    <c:otherwise>
                                                                        <!-- Page Info -->
                                                                        <c:if test="${SearchUserForm.searchUserDTO.getUserPageMap() ne null && SearchUserForm.searchUserDTO.getUserPageMap().size() > 0}">
                                                                            <div class="row">
                                                                                <div class="col-md-12">
                                                                                    <!-- BEGIN PORTLET -->
                                                                                    <div class="portlet light ">
                                                                                        <div class="portlet-title">
                                                                                            <div class="caption caption-md">
                                                                                                <i class="icon-bar-chart theme-font hide"></i>
                                                                                                <span class="caption-subject font-blue-madison bold uppercase">Page Information</span>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="portlet-body">
                                                                                            <div class="table-scrollable table-scrollable-borderless">
                                                                                                <table class="table table-hover table-light">
                                                                                                    <thead>
                                                                                                        <tr class="uppercase">
                                                                                                            <th colspan="2"> Name </th>
                                                                                                            <th> RingID </th>
                                                                                                            <th> PageID </th>
                                                                                                            <th> Status </th>
                                                                                                            <th> Type </th>
                                                                                                        </tr>
                                                                                                    </thead>
                                                                                                    <c:forEach items="${SearchUserForm.searchUserDTO.getUserPageMap()}" var="pagelist">
                                                                                                        <c:forEach items="${pagelist.value}" var="page">
                                                                                                            <tr>
                                                                                                                <td class="fit">
                                                                                                                    <c:choose>
                                                                                                                        <c:when test="${page.getProfileImage() != null && page.getProfileImage().length() > 0}">
                                                                                                                            <img class="user-pic" src="<%=Constants.MEDIA_CLOUD_BASE_URL%>${page.getProfileImage()}" style="height: 30px;width: 30px"/>
                                                                                                                        </c:when>
                                                                                                                        <c:otherwise>
                                                                                                                            <img class="user-pic" src="assets/global/img/default-pp.jpg" style="height: 30px;width: 30px"/> 
                                                                                                                        </c:otherwise>
                                                                                                                    </c:choose>
                                                                                                                </td>
                                                                                                                <td>
                                                                                                                    <a href="searchUser2.do?searchText=${page.getRingId()}">${page.getServiceName()}</a>
                                                                                                                </td>
                                                                                                                <td>
                                                                                                                    <span class="bold theme-font">${page.getRingId()}</span>
                                                                                                                </td>
                                                                                                                <td> ${page.getPageId()} </td>
                                                                                                                <td>
                                                                                                                    <c:choose>
                                                                                                                        <c:when test="${page.getPageActiveStatus() eq 0}">
                                                                                                                            Active
                                                                                                                        </c:when>
                                                                                                                        <c:otherwise>
                                                                                                                            Deactive
                                                                                                                        </c:otherwise>
                                                                                                                    </c:choose>
                                                                                                                </td>
                                                                                                                <td> ${pagelist.key} </td>
                                                                                                            </tr>
                                                                                                        </c:forEach>
                                                                                                    </c:forEach>
                                                                                                </table>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <!-- END PORTLET -->
                                                                                </div>
                                                                            </div>
                                                                        </c:if>
                                                                        <!-- Device Info -->
                                                                        <c:if test="${SearchUserForm.searchUserDTO.getUserDeviceList() ne null && SearchUserForm.searchUserDTO.getUserDeviceList().size() > 0}">
                                                                            <div class="row">
                                                                                <div class="col-md-12">
                                                                                    <!-- BEGIN PORTLET -->
                                                                                    <div class="portlet light ">
                                                                                        <div class="portlet-title">
                                                                                            <div class="caption caption-md">
                                                                                                <i class="icon-bar-chart theme-font hide"></i>
                                                                                                <span class="caption-subject font-blue-madison bold uppercase">Device Information</span>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="portlet-body">
                                                                                            <div class="table-scrollable table-scrollable-borderless">
                                                                                                <table class="table table-hover table-light">
                                                                                                    <thead>
                                                                                                        <tr class="uppercase">
                                                                                                            <th> Type </th>
                                                                                                            <th> Category </th>
                                                                                                            <th> AppType </th>
                                                                                                            <th> Version </th>
                                                                                                            <th> Id </th>
                                                                                                            <th> Name </th>
                                                                                                            <th> Token </th>
                                                                                                        </tr>
                                                                                                    </thead>
                                                                                                    <c:forEach items="${SearchUserForm.searchUserDTO.getUserDeviceList()}" var="device">
                                                                                                        <tr>
                                                                                                            <c:choose>
                                                                                                                <c:when test="${device.getDeviceType() eq 1}">
                                                                                                                    <td class="font-blue">Desktop</td>
                                                                                                                </c:when>
                                                                                                                <c:when test="${device.getDeviceType() eq 2}">
                                                                                                                    <td class="font-blue">Android</td>
                                                                                                                </c:when>
                                                                                                                <c:when test="${device.getDeviceType() eq 3}">
                                                                                                                    <td class="font-blue">iOS</td>
                                                                                                                </c:when>
                                                                                                                <c:when test="${device.getDeviceType() eq 4}">
                                                                                                                    <td class="font-blue">Windows Phone</td>
                                                                                                                </c:when>
                                                                                                                <c:when test="${device.getDeviceType() eq 5}">
                                                                                                                    <td class="font-blue">Web</td>
                                                                                                                </c:when>
                                                                                                                <c:otherwise>
                                                                                                                    <td class="font-blue">${device.getDeviceType()}</td>
                                                                                                                </c:otherwise>
                                                                                                            </c:choose>
                                                                                                            <td>${device.getDeviceCategory()}</td>
                                                                                                            <td>${device.getAppType()}</td>
                                                                                                            <td>${device.getAppVersion()}</td>
                                                                                                            <td>
                                                                                                                <span class="bold theme-font">${device.getDeviceID()}</span>
                                                                                                            </td>
                                                                                                            <td>${device.getDeviceName()}</td>
                                                                                                            <td>${device.getDeviceToken()}</td>
                                                                                                        </tr>
                                                                                                    </c:forEach>
                                                                                                </table>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <!-- END PORTLET -->
                                                                                </div>
                                                                            </div>
                                                                        </c:if>
                                                                        <!-- Call & Chat Info -->
                                                                        <c:if test="${SearchUserForm.searchUserDTO.getCallAndChatStatusDTOs() ne null && SearchUserForm.searchUserDTO.getCallAndChatStatusDTOs().size() > 0}">
                                                                            <div class="row">
                                                                                <div class="col-md-12">
                                                                                    <!-- BEGIN PORTLET -->
                                                                                    <div class="portlet light ">
                                                                                        <div class="portlet-title">
                                                                                            <div class="caption caption-md">
                                                                                                <i class="icon-bar-chart theme-font hide"></i>
                                                                                                <span class="caption-subject font-blue-madison bold uppercase">Call & Chat Status Info</span>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="portlet-body">
                                                                                            <div class="table-scrollable table-scrollable-borderless">
                                                                                                <table class="table table-hover table-light">
                                                                                                    <thead>
                                                                                                        <tr class="uppercase">
                                                                                                            <th> App Type </th>
                                                                                                            <th> Anonymous Call </th>
                                                                                                            <th> Global Call </th>
                                                                                                            <th> Anonymous Chat </th>
                                                                                                        </tr>
                                                                                                    </thead>
                                                                                                    <c:forEach items="${SearchUserForm.searchUserDTO.getCallAndChatStatusDTOs()}" var="callAndChat">
                                                                                                        <tr>
                                                                                                            <td>${callAndChat.getAppType()}</td>
                                                                                                            <td>${callAndChat.getAnonymousCallStatus()}</td>
                                                                                                            <td>${callAndChat.getGlobalCallStatus()}</td>
                                                                                                            <td>${callAndChat.getAnonymousChatStatus()}</td>
                                                                                                        </tr>
                                                                                                    </c:forEach>
                                                                                                </table>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <!-- END PORTLET -->
                                                                                </div>
                                                                            </div>
                                                                        </c:if>
                                                                        <!-- Feed List -->
                                                                        <div class="row">
                                                                            <div class="col-md-9">
                                                                                <div class="portlet light portlet-fit bg-inverse">
                                                                                    <div class="portlet-title" style="border-bottom: 0px solid #eee;padding-bottom: 0px;">
                                                                                        <div class="caption">
                                                                                            <span class="caption-subject bold font-green uppercase"> Feeds</span>
                                                                                            <span class="caption-helper">own wall feed</span>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="portlet-body" style="padding: 0px 0px 20px 0px;">
                                                                                        <input type="hidden" id="pvtUUID" value=""/>
                                                                                        <div id="homeFeed" class="timeline white-bg" style="display: none;">
                                                                                        </div>
                                                                                        <div class="row text-center">
                                                                                            <button id="seeMoreFeed" type="button" class="btn btn-default mt-ladda-btn ladda-button btn-outline btn-circle seeMoreFeed" data-style="expand-left" data-spinner-color="#333" disabled="true">
                                                                                                <span class="ladda-label">Loading...</span>
                                                                                            </button>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </c:otherwise>
                                                                </c:choose>
                                                            </div>
                                                            <!-- END PROFILE CONTENT -->
                                                        </div>
                                                    </div>
                                                </c:when>
                                                <c:otherwise>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="text-center">
                                                                <span class="font-grey-cascade">No result found</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </c:otherwise>
                                            </c:choose>
                                        </html:form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <%@include file="/shared/footer.jsp"%>
        </div>
        <%@include file="/shared/script-list.jsp"%>
        <script type="text/javascript">
            var base_url = '<%=resourceURL%>';
            var media_base_url = '<%=Constants.MEDIA_CLOUD_BASE_URL%>';
            var image_base_url = '<%=Constants.IMAGE_UPLOAD_BASE_URL%>'
            function getDateFromLong(value) {
                var date = new Date(value);
                return date;
            }
            $('#viewType').val() == 0 || $('#viewType').val() == 1 ? seeMoreFeedList(5) : "";
            $('#userType').val() == 10 ? seeMoreFeedList(66) : '';
            $('#userType').val() == 1 ? seeMoreCoin(0) : '';
            $('#userType').val() == 1 ? seeMoreCoin(1) : '';
            $('#userType').val() == 1 ? seeMoreFriend() : '';
            $('#userType').val() == 1 ? getWalletInfo($("#utId").val()) : '';
            window.onbeforeunload = function () {
                $('#pvtUUID').val("");
                if ($('#userType').val() == 10) {
                    $('#pastLive_pvtUUID').val("");
                } else if ($('#userType').val() == 1) {
                    $('#friend_pvtId').val("");
                }
            };
        </script>
    </body>
</html>

