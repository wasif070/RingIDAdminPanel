<%-- 
    Document   : updateRestrictionTime
    Created on : Jul 2, 2017, 3:11:28 PM
    Author     : Rabby
--%>

<%@include file="/login/loginTimeExpiry.jsp"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ page import="java.util.*,com.ringid.admin.utils.Constants"%>
<%@include  file="/shared/head.jsp" %>
<!DOCTYPE html>
<html>
    <head>
        <title>Admin Panel | Restriction Time </title>
        <%@include file="/shared/css-list.jsp"%>
    </head>
    <body class="page-container-bg-solid">
        <div class="page-wrapper">
            <div class="page-wrapper-row">
                <div class="page-wrapper-top">
                    <div class="page-header">
                        <%                            String message = "";
                            if (request.getAttribute("message") != null) {
                                message = request.getAttribute("message").toString();
                                request.removeAttribute("message");
                            }
                            String classForTab_0 = "", classForTab_1 = "", classForTab_2 = "", classForTab_3 = "", classForTab_4 = "", classForTab_5 = "";
                            if (request.getAttribute("type") != null && request.getAttribute("type").toString().length() > 0) {
                                if (Integer.valueOf(request.getAttribute("type").toString()) == 1) {
                                    classForTab_0 = "active";
                                } else if (Integer.valueOf(request.getAttribute("type").toString()) == 2) {
                                    classForTab_1 = "active";
                                } else if (Integer.valueOf(request.getAttribute("type").toString()) == 3) {
                                    classForTab_2 = "active";
                                } else if (Integer.valueOf(request.getAttribute("type").toString()) == 4) {
                                    classForTab_3 = "active";
                                } else if (Integer.valueOf(request.getAttribute("type").toString()) == 5) {
                                    classForTab_4 = "active";
                                } else if (Integer.valueOf(request.getAttribute("type").toString()) == 6) {
                                    classForTab_5 = "active";
                                } else {
                                    classForTab_0 = "active";
                                }
                            } else {
                                classForTab_0 = "active";
                            }
                        %>
                        <%@include  file="/shared/menu.jsp" %>
                    </div>
                </div>
            </div>
            <div class="page-wrapper-row full-height">
                <div class="page-wrapper-middle">
                    <div class="page-container">
                        <div class="page-content-wrapper">
                            <div class="page-content">
                                <div class="container">
                                    <ul class="page-breadcrumb breadcrumb">
                                        <li>
                                            <a href="welcome.do">Admin Panel</a>
                                            <span style="font-size: 14px; margin: 0 auto;" class="fa fa-angle-right"></span>
                                        </li>
                                        <li>
                                            <a href="javascript:;">Manage Operations</a>
                                            <span style="font-size: 14px; margin: 0 auto;" class="fa fa-angle-right"></span>
                                        </li>
                                        <li>
                                            <span id="features" data-class-active=".manageOperations .updateRestrictionTime">Limt / Time Update</span>
                                        </li>
                                    </ul>
                                    <%if (message != null && message.length() > 0) {%>
                                    <div id="prefix_661213402591" class="custom-alerts alert alert-success fade in">
                                        <button type="button" class="close" data-dismiss="alert"
                                                aria-hidden="true"></button>
                                        <strong> <%=message%> </strong>
                                    </div>
                                    <%}%>
                                    <div class="page-content-inner">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="tabbable-line boxless tabbable-reversed">
                                                    <ul class="nav nav-tabs">
                                                        <li class="<%=classForTab_0%>">
                                                            <a href="#tab_0" data-toggle="tab">Restriction Time</a>
                                                        </li>
                                                        <li class="<%=classForTab_1%>">
                                                            <a href="#tab_1" data-toggle="tab">NameUpdate Limit</a>
                                                        </li>
                                                        <li class="<%=classForTab_2%>">
                                                            <a href="#tab_2" data-toggle="tab">Total Friend Limit</a>
                                                        </li>
                                                        <li class="<%=classForTab_3%>">
                                                            <a href="#tab_3" data-toggle="tab">Pending Friend Limit</a>
                                                        </li>
                                                        <li class="<%=classForTab_4%>">
                                                            <a href="#tab_4" data-toggle="tab">OTP Retry Limit </a>
                                                        </li>
                                                        <li class="<%=classForTab_5%>">
                                                            <a href="#tab_5" data-toggle="tab">OTP Retry Limit Reset Time </a>
                                                        </li>
                                                    </ul>
                                                    <div class="tab-content" style="padding-top: 15px;">
                                                        <div class="tab-pane <%=classForTab_0%>" id="tab_0">
                                                            <div class="portlet box green">
                                                                <div class="portlet-title">
                                                                    <div class="caption">
                                                                        <i class="fa fa-wrench"></i>Restriction Time</div>
                                                                    <div class="tools">
                                                                        <a href="javascript:;" class="collapse"> </a>
                                                                    </div>
                                                                </div>
                                                                <div class="portlet-body form">
                                                                    <html:form action="updateRestrictionTime" method="POST" acceptCharset="UTF-8" styleClass="form-horizontal" >
                                                                        <html:hidden property='type' value='1'/>
                                                                        <html:hidden property='nameUpdateLimit'/>
                                                                        <html:hidden property='totalFriendLimit'/>
                                                                        <html:hidden property='pendingFriendLimit'/>
                                                                        <html:hidden property='otpRetryLimit'/>
                                                                        <html:hidden property='otpResetTimeDay'/>
                                                                        <html:hidden property='otpResetTimeHour'/>
                                                                        <html:hidden property='otpResetTimeMin'/>
                                                                        <html:hidden property='otpResetTimeSec'/>
                                                                        <div class="form-body">
                                                                            <div class="form-group">
                                                                                <label class="col-md-3 control-label">New Time</label>
                                                                                <div class="col-md-2">
                                                                                    <html:text property="day" title="day" styleClass="form-control "/>
                                                                                    <span class="help-inline">[Day]</span>
                                                                                    <html:messages id="day" property="day">
                                                                                        <bean:write name="day"  filter="false"/>
                                                                                    </html:messages>
                                                                                </div>
                                                                                <div class="col-md-2">
                                                                                    <html:text property="hour" title="hour" styleClass="form-control " />
                                                                                    <span class="help-inline">[HOUR]</span>
                                                                                    <html:messages id="hour" property="hour">
                                                                                        <bean:write name="hour"  filter="false"/>
                                                                                    </html:messages>
                                                                                </div>
                                                                                <div class="col-md-2">
                                                                                    <html:text property="minute" title="minute" styleClass="form-control " />
                                                                                    <span class="help-inline">[Min]</span>
                                                                                    <html:messages id="minute" property="minute">
                                                                                        <bean:write name="minute"  filter="false"/>
                                                                                    </html:messages>
                                                                                </div>
                                                                                <div class="col-md-2">
                                                                                    <html:text property="secound" title="secound" styleClass="form-control " />
                                                                                    <span class="help-inline">[Sec]</span>
                                                                                    <html:messages id="secound" property="secound">
                                                                                        <bean:write name="secound"  filter="false"/>
                                                                                    </html:messages>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-actions">
                                                                            <div class="row">
                                                                                <div class="col-md-offset-3 col-md-9">
                                                                                    <button type="submit" class="btn green" name="saveButton">Submit</button>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </html:form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="tab-pane <%=classForTab_1%>" id="tab_1">
                                                            <div class="portlet box green">
                                                                <div class="portlet-title">
                                                                    <div class="caption">
                                                                        <i class="fa fa-wrench"></i>NameUpdate Limit</div>
                                                                    <div class="tools">
                                                                        <a href="javascript:;" class="collapse"> </a>
                                                                    </div>
                                                                </div>
                                                                <div class="portlet-body form">
                                                                    <html:form action="updateRestrictionTime" method="POST" acceptCharset="UTF-8" styleClass="form-horizontal" >
                                                                        <html:hidden property='type' value='2'/>
                                                                        <html:hidden property='day'/>
                                                                        <html:hidden property='hour'/>
                                                                        <html:hidden property='minute'/>
                                                                        <html:hidden property='secound'/>
                                                                        <html:hidden property='totalFriendLimit'/>
                                                                        <html:hidden property='pendingFriendLimit'/>
                                                                        <html:hidden property='otpRetryLimit'/>
                                                                        <html:hidden property='otpResetTimeDay'/>
                                                                        <html:hidden property='otpResetTimeHour'/>
                                                                        <html:hidden property='otpResetTimeMin'/>
                                                                        <html:hidden property='otpResetTimeSec'/>
                                                                        <div class="form-body">
                                                                            <div class="form-group">
                                                                                <label class="col-md-4 control-label">Name Update Limit :</label>
                                                                                <div class="col-md-6">
                                                                                    <html:text property="nameUpdateLimit" title="nameUpdate Limit" styleClass="form-control" />
                                                                                    <html:messages id="nameUpdateLimit" property="nameUpdateLimit">
                                                                                        <span style="color: red;"> Value Required</span>
                                                                                    </html:messages>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-actions">
                                                                            <div class="row">
                                                                                <div class="col-md-offset-3 col-md-9">
                                                                                    <button type="submit" class="btn green" name="saveButton">Submit</button>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </html:form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="tab-pane <%=classForTab_2%>" id="tab_2">
                                                            <div class="portlet box green">
                                                                <div class="portlet-title">
                                                                    <div class="caption">
                                                                        <i class="fa fa-wrench"></i>Total Friend Limit</div>
                                                                    <div class="tools">
                                                                        <a href="javascript:;" class="collapse"> </a>
                                                                    </div>
                                                                </div>
                                                                <div class="portlet-body form">
                                                                    <html:form action="updateRestrictionTime" method="POST" acceptCharset="UTF-8" styleClass="form-horizontal" >
                                                                        <html:hidden property='type' value='3'/>
                                                                        <html:hidden property='day'/>
                                                                        <html:hidden property='hour'/>
                                                                        <html:hidden property='minute'/>
                                                                        <html:hidden property='secound'/>
                                                                        <html:hidden property='pendingFriendLimit'/>
                                                                        <html:hidden property='nameUpdateLimit'/>
                                                                        <html:hidden property='otpRetryLimit'/>
                                                                        <html:hidden property='otpResetTimeDay'/>
                                                                        <html:hidden property='otpResetTimeHour'/>
                                                                        <html:hidden property='otpResetTimeMin'/>
                                                                        <html:hidden property='otpResetTimeSec'/>
                                                                        <div class="form-body">
                                                                            <div class="form-group">
                                                                                <label class="col-md-4 control-label">Total Friend Limit :</label>
                                                                                <div class="col-md-6">
                                                                                    <html:text property="totalFriendLimit" title="Total Friend Limit" styleClass="form-control" />
                                                                                    <html:messages id="totalFriendLimit" property="totalFriendLimit">
                                                                                        <span style="color: red;"> Value Required</span>
                                                                                    </html:messages>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-actions">
                                                                            <div class="row">
                                                                                <div class="col-md-offset-3 col-md-9">
                                                                                    <button type="submit" class="btn green" name="saveButton">Submit</button>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </html:form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="tab-pane <%=classForTab_3%>" id="tab_3">
                                                            <div class="portlet box green">
                                                                <div class="portlet-title">
                                                                    <div class="caption">
                                                                        <i class="fa fa-wrench"></i>Pending Friend Limit</div>
                                                                    <div class="tools">
                                                                        <a href="javascript:;" class="collapse"> </a>
                                                                    </div>
                                                                </div>
                                                                <div class="portlet-body form">
                                                                    <html:form action="updateRestrictionTime" method="POST" acceptCharset="UTF-8" styleClass="form-horizontal" >
                                                                        <html:hidden property='type' value='4'/>
                                                                        <html:hidden property='day'/>
                                                                        <html:hidden property='hour'/>
                                                                        <html:hidden property='minute'/>
                                                                        <html:hidden property='secound'/>
                                                                        <html:hidden property='totalFriendLimit'/>
                                                                        <html:hidden property='nameUpdateLimit'/>
                                                                        <html:hidden property='otpRetryLimit'/>
                                                                        <html:hidden property='otpResetTimeDay'/>
                                                                        <html:hidden property='otpResetTimeHour'/>
                                                                        <html:hidden property='otpResetTimeMin'/>
                                                                        <html:hidden property='otpResetTimeSec'/>
                                                                        <div class="form-body">
                                                                            <div class="form-group">
                                                                                <label class="col-md-4 control-label">Pending Friend Limit :</label>
                                                                                <div class="col-md-6">
                                                                                    <html:text property="pendingFriendLimit" title="Pending Friend Limit" styleClass="form-control" />
                                                                                    <html:messages id="pendingFriendLimit" property="pendingFriendLimit">
                                                                                        <span style="color: red;"> Value Required</span>
                                                                                    </html:messages>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-actions">
                                                                            <div class="row">
                                                                                <div class="col-md-offset-3 col-md-9">
                                                                                    <button type="submit" class="btn green" name="saveButton">Submit</button>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </html:form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="tab-pane <%=classForTab_4%>" id="tab_4">
                                                            <div class="portlet box green">
                                                                <div class="portlet-title">
                                                                    <div class="caption">
                                                                        <i class="fa fa-wrench"></i>OTP Retry Limit</div>
                                                                    <div class="tools">
                                                                        <a href="javascript:;" class="collapse"> </a>
                                                                    </div>
                                                                </div>
                                                                <div class="portlet-body form">
                                                                    <html:form action="updateRestrictionTime" method="POST" acceptCharset="UTF-8" styleClass="form-horizontal" >
                                                                        <html:hidden property='type' value='5'/>
                                                                        <html:hidden property='day'/>
                                                                        <html:hidden property='hour'/>
                                                                        <html:hidden property='minute'/>
                                                                        <html:hidden property='secound'/>
                                                                        <html:hidden property='nameUpdateLimit'/>
                                                                        <html:hidden property='totalFriendLimit'/>
                                                                        <html:hidden property='pendingFriendLimit'/>
                                                                        <html:hidden property='otpResetTimeDay'/>
                                                                        <html:hidden property='otpResetTimeHour'/>
                                                                        <html:hidden property='otpResetTimeMin'/>
                                                                        <html:hidden property='otpResetTimeSec'/>
                                                                        <div class="form-body">
                                                                            <div class="form-group">
                                                                                <label class="col-md-4 control-label">OTP Retry Limit :</label>
                                                                                <div class="col-md-6">
                                                                                    <html:text property="otpRetryLimit" title="OTP Retry Limit" styleClass="form-control" />
                                                                                    <html:messages id="otpRetryLimit" property="otpRetryLimit">
                                                                                        <span style="color: red;"> Value Required</span>
                                                                                    </html:messages>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-actions">
                                                                            <div class="row">
                                                                                <div class="col-md-offset-3 col-md-9">
                                                                                    <button type="submit" class="btn green" name="saveButton">Submit</button>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </html:form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="tab-pane <%=classForTab_5%>" id="tab_5">
                                                            <div class="portlet box green">
                                                                <div class="portlet-title">
                                                                    <div class="caption">
                                                                        <i class="fa fa-wrench"></i>OTP Retry Limit Reset Time</div>
                                                                    <div class="tools">
                                                                        <a href="javascript:;" class="collapse"> </a>
                                                                    </div>
                                                                </div>
                                                                <div class="portlet-body form">
                                                                    <html:form action="updateRestrictionTime" method="POST" acceptCharset="UTF-8" styleClass="form-horizontal" >
                                                                        <html:hidden property='type' value='6'/>
                                                                        <html:hidden property='day'/>
                                                                        <html:hidden property='hour'/>
                                                                        <html:hidden property='minute'/>
                                                                        <html:hidden property='secound'/>
                                                                        <html:hidden property='nameUpdateLimit'/>
                                                                        <html:hidden property='totalFriendLimit'/>
                                                                        <html:hidden property='pendingFriendLimit'/>
                                                                        <html:hidden property='otpRetryLimit'/>
                                                                        <div class="form-body">
                                                                            <div class="form-group">
                                                                                <label class="col-md-3 control-label">New Time</label>
                                                                                <div class="col-md-2">
                                                                                    <html:text property="otpResetTimeDay" title="day" styleClass="form-control "/>
                                                                                    <span class="help-inline">[Day]</span>
                                                                                    <html:messages id="otpResetTimeDay" property="otpResetTimeDay">
                                                                                        <bean:write name="otpResetTimeDay"  filter="false"/>
                                                                                    </html:messages>
                                                                                </div>
                                                                                <div class="col-md-2">
                                                                                    <html:text property="otpResetTimeHour" title="hour" styleClass="form-control " />
                                                                                    <span class="help-inline">[HOUR]</span>
                                                                                    <html:messages id="otpResetTimeHour" property="otpResetTimeHour">
                                                                                        <bean:write name="otpResetTimeHour"  filter="false"/>
                                                                                    </html:messages>
                                                                                </div>
                                                                                <div class="col-md-2">
                                                                                    <html:text property="otpResetTimeMin" title="minute" styleClass="form-control " />
                                                                                    <span class="help-inline">[Min]</span>
                                                                                    <html:messages id="otpResetTimeMin" property="otpResetTimeMin">
                                                                                        <bean:write name="otpResetTimeMin"  filter="false"/>
                                                                                    </html:messages>
                                                                                </div>
                                                                                <div class="col-md-2">
                                                                                    <html:text property="otpResetTimeSec" title="secound" styleClass="form-control " />
                                                                                    <span class="help-inline">[Sec]</span>
                                                                                    <html:messages id="otpResetTimeSec" property="otpResetTimeSec">
                                                                                        <bean:write name="otpResetTimeSec"  filter="false"/>
                                                                                    </html:messages>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-actions">
                                                                            <div class="row">
                                                                                <div class="col-md-offset-3 col-md-9">
                                                                                    <button type="submit" class="btn green" name="saveButton">Submit</button>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </html:form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <%@include file="/shared/footer.jsp"%>
        </div>
        <%@include file="/shared/script-list.jsp"%>
    </body>
</html>

