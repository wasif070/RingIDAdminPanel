<%-- 
    Document   : postRecordedLiveFeed
    Created on : Apr 24, 2018, 12:59:25 PM
    Author     : mamun
--%>

<%@page import="com.ringid.admin.service.CelebrityService"%>
<%@page import="com.google.gson.JsonParser"%>
<%@page import="com.google.gson.JsonElement"%>
<%@page import="com.google.gson.JsonObject"%>
<%@page import="com.google.gson.JsonArray"%>
<%@page import="com.google.gson.Gson"%>
<%@page import="org.ringid.pages.PageDTO"%>
<%@page import="org.ringid.users.UserDTO"%>
<%@include file="/login/loginTimeExpiry.jsp"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.ringid.admin.utils.Constants"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@include  file="/shared/head.jsp" %>

<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Admin Panel | Post Recorded Live Feed</title>
        <%@include file="/shared/css-list.jsp"%>
    </head>
    <body class="page-container-bg-solid">
        <div class="page-wrapper">
            <div class="page-wrapper-row">
                <div class="page-wrapper-top">
                    <div class="page-header">
                        <%@include  file="/shared/menu.jsp"%>
                        <%
                            Gson gson = new Gson();
                            JsonArray jsonArray = new JsonArray();
                            String msgStr = "";
                            if (request.getAttribute("message") != null && request.getAttribute("message").toString().length() > 0) {
                                msgStr = request.getAttribute("message").toString();
                            }
                        %>
                    </div>
                </div>
            </div>
            <div class="page-wrapper-row full-height">
                <div class="page-wrapper-middle">
                    <div class="page-container">
                        <div class="page-content-wrapper">
                            <div class="page-content">
                                <div class="container">
                                    <ul class="page-breadcrumb breadcrumb">
                                        <li>
                                            <a href="welcome.do">Admin Panel</a>
                                            <span style="font-size: 14px; margin: 0 auto;" class="fa fa-angle-right"></span>
                                        </li>
                                        <li>
                                            <a href="javascript:;">Live / Channel </a>
                                            <span style="font-size: 14px; margin: 0 auto;" class="fa fa-angle-right"></span>
                                        </li>
                                        <li>
                                            <span id="features" data-class-active=".live-o-channel .postRecordedLiveFeed"> Post Recorded Live Feed </span>
                                        </li>
                                    </ul>
                                    <%if (msgStr.length() > 0) {%>
                                    <div id="prefix_661213402591" class="custom-alerts alert alert-success fade in">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                        <strong> <%=msgStr%> </strong>
                                    </div>
                                    <% }%>
                                    <div class="page-content-inner">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="tabbable-line boxless tabbable-reversed">
                                                    <div class="tab-content" style="padding-top: 15px;">
                                                        <div class="portlet box green">
                                                            <div class="portlet-title">
                                                                <div class="caption">
                                                                    <i class="fa fa-plus"></i> Post Recorded Live Feed </div>
                                                                <div class="tools">
                                                                    <a href="javascript:;" class="collapse"> </a>
                                                                </div>
                                                            </div>
                                                            <div class="portlet-body form">
                                                                <html:form action="postRecordedLiveFeed" styleClass="form-horizontal" method="POST"  acceptCharset="UTF-8" enctype="multipart/form-data">
                                                                    <div class="form-body">
                                                                        <div class="form-group">
                                                                            <label class="col-md-3 control-label">Celebrity Page <span style="color: red">*</span></label>
                                                                            <div class="col-md-4">
                                                                                <html:hidden property="pageType" value="10" />
                                                                                <html:hidden property="duration" styleId="mediaduration" />
                                                                                <html:select styleClass="form-control" property="pageId" onchange="changeProfileImage(this)">
                                                                                    <html:option value="">-- Select Celebrity --</html:option>
                                                                                    <%
                                                                                        List<UserDTO> userDTOs = CelebrityService.getInstance().getCelebrityList(null, 0, 0);
                                                                                        JsonParser parser = new JsonParser();
                                                                                        for (UserDTO userDTO : userDTOs) {
                                                                                            PageDTO celebrity = userDTO.getCelebrityDTO();
                                                                                            jsonArray.add(parser.parse(gson.toJson(celebrity)));
                                                                                    %>
                                                                                    <html:option value="<%=String.valueOf(celebrity.getPageId())%>"><%=celebrity.getPageName()%></html:option>
                                                                                    <%}%>
                                                                                </html:select>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label class="col-md-3 control-label">Profile Image</label>
                                                                            <div class="col-md-4">
                                                                                <img id="profileImage" src="" style="width: 100px; height: 100px;" />
                                                                                <html:hidden property="profileImage" styleId="profileImageUrl" />
                                                                            </div>
                                                                        </div>
                                                                        <div id="mediaDiv" class="form-group" style="display: none;">
                                                                            <label class="col-md-3 control-label"></label>
                                                                            <div class="col-md-4">
                                                                                <video controls preload="metadata" style="width: 250px; height: 200px;">

                                                                                </video>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label class="control-label col-md-3">Media <span style="color: red">*</span></label>
                                                                            <div class="col-md-4">
                                                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                                    <div class="input-group input-large">
                                                                                        <div class="form-control uneditable-input input-fixed input-medium" data-trigger="fileinput">
                                                                                            <i class="fa fa-file fileinput-exists"></i>&nbsp;
                                                                                            <span class="fileinput-filename"> </span>
                                                                                        </div>
                                                                                        <span class="input-group-addon btn default btn-file">
                                                                                            <span class="fileinput-new"> Select file </span>
                                                                                            <span class="fileinput-exists"> Change </span>
                                                                                            <input id="fileInput" name="statusvideo" type="file" accept="video/mp4"> </span>
                                                                                        <a href="javascript:;" class="input-group-addon btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label class="col-md-3 control-label">Viewer Count</label>
                                                                            <div class="col-md-4">
                                                                                <html:text property="viewerCount" title="viewer count" styleClass="form-control" />
                                                                                <html:messages id="viewerCount" property="viewerCount">
                                                                                    <bean:write name="viewerCount"  filter="false"/>
                                                                                </html:messages>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label class="col-md-3 control-label">Location</label>
                                                                            <div class="col-md-4">
                                                                                <html:text property="location" title="location" styleClass="form-control" />
                                                                                <html:messages id="location" property="location">
                                                                                    <bean:write name="location"  filter="false"/>
                                                                                </html:messages>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label class="col-md-3 control-label">Title</label>
                                                                            <div class="col-md-4">
                                                                                <html:text property="title" title="title" styleClass="form-control" />
                                                                                <html:messages id="title" property="title">
                                                                                    <bean:write name="title"  filter="false"/>
                                                                                </html:messages>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label class="col-md-3 control-label">Start Time</label>
                                                                            <div class="col-md-4">
                                                                                <input type="text" name="startTime" placeholder="yyyy-mm-dd HH:mm" class="form-control date form_datetime" title="start time" />
                                                                                <html:messages id="startTime" property="startTime">
                                                                                    <bean:write name="startTime"  filter="false"/>
                                                                                </html:messages>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label class="col-md-3 control-label">Streaming Type </label>
                                                                            <div class="col-md-4">
                                                                                <html:select styleClass="form-control" property="streamingType">
                                                                                    <html:option value="1">Audio Stream</html:option>
                                                                                    <html:option value="2">Video Stream</html:option>
                                                                                    <html:option value="3">Channel Stream</html:option>
                                                                                    <html:option value="4">TV Channel Stream</html:option>
                                                                                </html:select>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-actions">
                                                                        <div class="row">
                                                                            <div class="col-md-offset-3 col-md-4">
                                                                                <button type="submit" class="btn green">Submit</button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </html:form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <%@include file="/shared/footer.jsp"%>
        </div>
        <%@include file="/shared/script-list.jsp"%>
        <script type="text/javascript">
            var jsonArray = <%=jsonArray%>, BASE_MEDIA_URL = '<%=Constants.MEDIA_CLOUD_BASE_URL%>', URL = window.URL || window.webkitURL, videoNode = document.querySelector("video");
            $(".form_datetime").datetimepicker({
                autoclose: true,
                format: "yyyy-mm-dd hh:ii"
            });
//            $(".form_time").timepicker({
//                autoclose: true,
//                showSeconds: true,
//                showMeridian: false,
//                minuteStep: 1
//            });
            $("#fileInput").change(function () {
                if (this.files.length === 0) {
                    $("#mediaDiv").hide();
                    videoNode.src = "";
                    return;
                }
                var file = this.files[0];
                if (videoNode.canPlayType(file.type) === '' || videoNode.canPlayType(file.type) === 'no') {
                    $("#mediaDiv").hide();
                    videoNode.src = "";
                    return;
                }
                $("#mediaDiv").show();
                videoNode.src = URL.createObjectURL(file);
                videoNode.ondurationchange = function (e) {
                    $("#mediaduration").val(videoNode.duration);
                };
            });

            function changeProfileImage(obj) {
                var val = obj.value;
                for (var i = 0; i < jsonArray.length; i++) {
                    if (jsonArray[i].pgId.toString() === val) {
                        $("#profileImage").attr("src", "");
                        $("#profileImage").attr("src", BASE_MEDIA_URL + jsonArray[i].prflImgUrl);
                        $("#profileImageUrl").val(jsonArray[i].prflImgUrl);
                        break;
                    }
                }
            }
        </script>
    </body>
</html>
