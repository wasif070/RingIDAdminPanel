<%-- 
    Document   : appConstantsList-new
    Created on : Jan 22, 2018, 3:17:49 PM
    Author     : mamun
--%>

<%@page import="com.ringid.admin.dto.DonationPageDTO"%>
<%@page import="com.ringid.admin.service.PageService"%>
<%@page import="com.ringid.admin.service.CelebrityService"%>
<%@include file="/login/loginTimeExpiry.jsp"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.util.ArrayList"%>
<%@page import="org.ringid.pages.PageDTO"%>
<%@page import="java.util.List"%>
<%@page import="org.ringid.users.UserDTO"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@include  file="/shared/head.jsp" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Admin Panel | Post Celebrity Page Status </title>
        <%@include file="/shared/css-list.jsp"%>
    </head>
    <body class="page-container-bg-solid">
        <div class="page-wrapper">
            <div class="page-wrapper-row">
                <div class="page-wrapper-top">
                    <div class="page-header">
                        <%@include  file="/shared/menu.jsp" %>
                        <%
                            String message = "";
                            if (session.getAttribute("message") != null) {
                                message = session.getAttribute("message").toString();
                                session.removeAttribute("message");
                            }
                        %>
                    </div>
                </div>
            </div>
            <div class="page-wrapper-row full-height">
                <div class="page-wrapper-middle">
                    <div class="page-container">
                        <div class="page-content-wrapper">
                            <div class="page-content">
                                <div class="container">
                                    <ul class="page-breadcrumb breadcrumb">
                                        <li>
                                            <a href="welcome.do">Admin Panel</a>
                                            <span style="font-size: 14px; margin: 0 auto;" class="fa fa-angle-right"></span>
                                        </li>
                                        <li>
                                            <a href="javascript:;">Special Users</a>
                                            <span style="font-size: 14px; margin: 0 auto;" class="fa fa-angle-right"></span>
                                        </li>
                                        <li>
                                            <span id="features" data-class-active=".specialUsers .feedPost"> Feed Post </span>
                                        </li>
                                    </ul>
                                    <div id="bootstrap_alerts_cus" style="display: none;">
                                        <div class="custom-alerts alert alert-success fade in">
                                            <button type="button" class="close" onclick="hideDiv()"></button>
                                            <sapn id="bootstrap_alerts_msg"> </sapn>
                                        </div>
                                    </div>
                                    <%if (message != null && message.length() > 0) {%>
                                    <div id="bootstrap_alerts_demo1" >
                                        <div id="prefix_661213402591" class="custom-alerts alert alert-success fade in">
                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                            <strong > <%=message%> </strong>
                                        </div>
                                    </div>
                                    <% }%>
                                    <div class="page-content-inner">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="tabbable-line boxless tabbable-reversed">
                                                    <div class="tab-content" style="padding-top: 15px;">
                                                        <div class="portlet box green">
                                                            <div class="portlet-title">
                                                                <div class="caption">
                                                                    <i class="fa fa-list-ul"></i> Post Celebrity Page Status </div>
                                                                <div class="tools">
                                                                    <a href="javascript:;" class="collapse"> </a>
                                                                </div>
                                                            </div>
                                                            <div class="portlet-body form">
                                                                <html:form action="postStatus" styleClass="form-horizontal" method="POST" acceptCharset="UTF-8" enctype="multipart/form-data">
                                                                    <html:hidden property="pageType" value="10" />
                                                                    <div class="form-body">
                                                                        <div class="form-group">
                                                                            <label class="col-md-3 control-label">Celebrity Page <span style="color: red">*</span></label>
                                                                            <div class="col-md-4">
                                                                                <html:select styleClass="bs-select form-control input-inline" property="pageId" styleId="celebrity">
                                                                                    <%
                                                                                        List<UserDTO> userDTOs = CelebrityService.getInstance().getCelebrityList(null, 0, 0);
                                                                                        for (UserDTO userDTO : userDTOs) {
                                                                                            PageDTO celebrity = userDTO.getCelebrityDTO();

                                                                                    %>
                                                                                    <html:option value="<%=String.valueOf(celebrity.getPageId())%>"><%=celebrity.getPageName()%></html:option>
                                                                                    <%}%>
                                                                                </html:select>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label class="col-md-3 control-label">Donation Page</label>
                                                                            <div class="col-md-4">
                                                                                <html:select styleClass="bs-select form-control input-inline" property="donationPageId" styleId="donation">
                                                                                    <html:option value="0">Select Donation Page </html:option>
                                                                                    <%
                                                                                        List<PageDTO> pageDTOs = PageService.getInstance().getDonationPages(new DonationPageDTO());
                                                                                        for (PageDTO pageDTO : pageDTOs) {
                                                                                            if (pageDTO.getPageActiveStatus() == 0) {
                                                                                    %>
                                                                                    <html:option value="<%=String.valueOf(pageDTO.getPageId())%>"><%=pageDTO.getPageName()%></html:option>
                                                                                    <% }
                                                                                        }%>
                                                                                </html:select>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label class="col-md-3 control-label">Datetime</label>
                                                                            <div class="col-md-4">
                                                                                <input id="datetime" type="text" name="datetime" placeholder="yyyy-mm-dd HH:mm" class="form-control date form_datetime" title="date time" />
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label class="col-md-3 control-label">Status</label>
                                                                            <div class="col-md-4">
                                                                                <textarea name="statustext" cols="50" rows="4" title="write somethings" class="form-control" ></textarea>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group" id="tr_liveserial">
                                                                            <label class="col-md-3 control-label">Live Schedule Serial</label>
                                                                            <div class="col-md-4">
                                                                                <html:text styleId="liveserial" property="liveScheduleSerial" styleClass="form-control" />
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group" id="tr_album_name">
                                                                            <label class="col-md-3 control-label">Album Name</label>
                                                                            <div class="col-md-4">
                                                                                <input id="albumName" type="text" name="albumName" class="form-control" title="album name"/>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group" id="tr_media_duration">
                                                                            <label class="col-md-3 control-label">Media Duration</label>
                                                                            <div class="col-md-4">
                                                                                <div class="input-icon">
                                                                                    <i class="fa fa-clock-o"></i>
                                                                                    <input id="mediaDuration" name="mediaDuration" type="text" class="form-control timepicker"/>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label class="col-md-3 control-label">Media</label>
                                                                            <div class="col-md-9">
                                                                                <div class="row" id="tr_media_container">
                                                                                    <div class="col-md-12">
                                                                                        <div class="table-scrollable">
                                                                                            <table id="media_container" class="table table-striped table-bordered table-hover">

                                                                                            </table>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="row">
                                                                                    <div class="col-md-12">
                                                                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                                            <div>
                                                                                                <span class="btn green btn-file" id="span_image">
                                                                                                    <i class="fa fa-image"></i>
                                                                                                    <span class="fileinput-new"> Add Images... </span>
                                                                                                    <input id="statusimage" name="statusimage" type="file" multiple="multiple" accept="image/*">
                                                                                                </span>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="fileinput fileinput-new" data-provides="fileinput" style="margin-left: 10px; margin-top: 5px;">
                                                                                            <div>
                                                                                                <span class="btn green btn-file" id="span_video">
                                                                                                    <i class="fa fa-video-camera"></i>
                                                                                                    <span class="fileinput-new"> Add Video... </span>
                                                                                                    <input id="statusvideo" name="statusvideo" type="file" class="pst-input" multiple="multiple" accept="video/mp4">
                                                                                                </span>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="fileinput fileinput-new" data-provides="fileinput" style="margin-left: 10px; margin-top: 5px;">
                                                                                            <div>
                                                                                                <span class="btn green btn-file" id="span_audio">
                                                                                                    <i class="fa fa-file-audio-o"></i>
                                                                                                    <span class="fileinput-new"> Add Audio... </span>
                                                                                                    <input id="statusaudio" name="statusaudio" type="file" class="pst-input" multiple="multiple" accept="audio/mp3">
                                                                                                </span>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="modal hide" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false">
                                                                            <div class="modal-header" style="margin-top: 50px;">
                                                                                <h1>Please Wait</h1>
                                                                            </div>
                                                                            <div class="modal-body">
                                                                                <div id="ajax_loader">
                                                                                    <img src="assets/global/img/loading-full.gif" style="display: block; margin-left: auto; margin-right: auto;">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-actions">
                                                                        <div class="row">
                                                                            <div class="col-md-offset-3 col-md-9">
                                                                                <button id="uploadFile" type="button" class="btn green input-small" >Post Status</button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </html:form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <%@include file="/shared/footer.jsp"%>
        </div>
        <%@include file="/shared/script-list.jsp"%>
        <script type="text/javascript">
            var form = document.forms[0];
            var base_url = "<%=resourceURL%>";
            $(".form_datetime").datetimepicker({
                autoclose: true,
                format: "yyyy-mm-dd hh:ii"
            });
            function hideDiv() {
                $("#bootstrap_alerts_cus").hide();
            }
        </script>
    </body>
</html>