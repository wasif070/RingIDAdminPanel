<%-- 
    Document   : UpdateAppConstants-new
    Created on : Jan 22, 2018, 3:09:36 PM
    Author     : mamun
--%>

<%@include file="/login/loginTimeExpiry.jsp"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ page import="java.util.*,com.ringid.admin.utils.Constants"%>
<%@include  file="/shared/head.jsp" %>
<!DOCTYPE html>
<html>
    <head>
        <title>Admin Panel | Change RingId </title>
        <%@include file="/shared/css-list.jsp"%>
    </head>
    <body class="page-container-bg-solid">
        <div class="page-wrapper">
            <div class="page-wrapper-row">
                <div class="page-wrapper-top">
                    <div class="page-header">
                        <%@include  file="/shared/menu.jsp" %>
                        <%
                            String msgstr = "";
                            if (request.getAttribute("msgstr") != null) {
                                msgstr = request.getAttribute("msgstr").toString();
                                request.removeAttribute("msgstr");
                            }
                        %>
                    </div>
                </div>
            </div>
            <div class="page-wrapper-row full-height">
                <div class="page-wrapper-middle">
                    <div class="page-container">
                        <div class="page-content-wrapper">
                            <div class="page-content">
                                <div class="container">
                                    <ul class="page-breadcrumb breadcrumb">
                                        <li>
                                            <a href="welcome.do">Admin Panel</a>
                                            <span style="font-size: 14px; margin: 0 auto;" class="fa fa-angle-right"></span>
                                        </li>
                                        <li>
                                            <a href="javascript:;">Manage Operations</a>
                                            <span style="font-size: 14px; margin: 0 auto;" class="fa fa-angle-right"></span>
                                        </li>
                                        <li>
                                            <span id="features" data-class-active=".manageOperations .changeRingID">Change RingID</span>
                                        </li>
                                    </ul>
                                    <% if (msgstr.length() > 0) {%>
                                    <div  class="custom-alerts alert alert-success fade in">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                        <strong> <%=msgstr%> </strong>
                                    </div>
                                    <% } %>
                                    <div class="page-content-inner">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="tabbable-line boxless tabbable-reversed">
                                                    <div class="tab-content" style="padding-top: 15px;">
                                                        <div class="portlet box green">
                                                            <div class="portlet-title">
                                                                <div class="caption">
                                                                    <i class="fa fa-wrench"></i> Change RingID </div>
                                                                <div class="tools">
                                                                    <a href="javascript:;" class="collapse"> </a>
                                                                </div>
                                                            </div>
                                                            <div class="portlet-body form">
                                                                <html:form action="changeRingId" method="POST" acceptCharset="UTF-8" styleClass="form-horizontal" >
                                                                    <div class="form-body">
                                                                        <div class="form-group">
                                                                            <label class="col-md-3 control-label">Old RingId <span class="required"> * </span> </label>
                                                                            <div class="col-md-4">
                                                                                <html:text property="oldRingId" title="old ringID" styleClass="form-control" />
                                                                                <html:messages id="oldRingId" property="oldRingId">
                                                                                    <span style="color: red;"> Old RingID Required </span>
                                                                                </html:messages>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label class="col-md-3 control-label">New RingId <span class="required"> * </span> </label>
                                                                            <div class="col-md-4">
                                                                                <html:text property="newRingId" title="old ringID" styleClass="form-control" />
                                                                                <html:messages id="newRingId" property="newRingId">
                                                                                    <span style="color: red;">New RingID Required </span>
                                                                                </html:messages>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group last">
                                                                            <label class="col-md-3 control-label"></label>
                                                                            <div class="col-md-4">
                                                                                <%
                                                                                    if (request.getSession(true).getAttribute("error") == "2") {
                                                                                %>
                                                                                <html:messages id="message" property="message" ><bean:write name="msg"  filter="false"/></html:messages>
                                                                                <bean:write name="AuthServerForm" property="message" filter="false"/>
                                                                                <%}%>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-actions">
                                                                        <div class="row">
                                                                            <div class="col-md-offset-3 col-md-9">
                                                                                <button type="submit" class="btn green" name="saveButton">Submit</button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </html:form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <%@include file="/shared/footer.jsp"%>
        </div>
        <%@include file="/shared/script-list.jsp"%>
    </body>
</html>
