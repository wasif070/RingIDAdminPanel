<%-- 
    Document   : editCelebritySchedule
    Created on : Jun 12, 2017, 2:55:19 PM
    Author     : ipvision
--%>

<%@page import="com.ringid.admin.dto.DonationPageDTO"%>
<%@page import="com.ringid.admin.service.PageService"%>
<%@page import="com.ringid.admin.projectMenu.MenuNames"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.ArrayList"%>
<%@page import="org.ringid.pages.PageDTO"%>
<%@page import="org.ringid.users.UserDTO"%>
<%@page import="java.util.List"%>
<%@include file="/login/loginTimeExpiry.jsp"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@include  file="/shared/head.jsp" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Admin Panel | Edit Celebrity Schedule </title>
        <%@include file="/shared/css-list.jsp"%>
    </head>
    <body class="page-container-bg-solid">
        <div class="page-wrapper">
            <div class="page-wrapper-row">
                <div class="page-wrapper-top">
                    <div class="page-header">
                        <%@include  file="/shared/menu.jsp" %>
                        <%
                            String FEATURE_NAME = MenuNames.CELEBRITY_SCHEDULE;
                            String message = "";
                            if (request.getAttribute("successMessage") != null) {
                                message = request.getAttribute("successMessage").toString();
                            }
                        %>
                    </div>
                </div>
            </div>
            <div class="page-wrapper-row full-height">
                <div class="page-wrapper-middle">
                    <div class="page-container">
                        <div class="page-content-wrapper">
                            <div class="page-content">
                                <div class="container">
                                    <ul class="page-breadcrumb breadcrumb">
                                        <li>
                                            <a href="welcome.do">Admin Panel</a>
                                            <span style="font-size: 14px; margin: 0 auto;" class="fa fa-angle-right"></span>
                                        </li>
                                        <li>
                                            <a href="javascript:;"> Manage Operations </a>
                                            <span style="font-size: 14px; margin: 0 auto;" class="fa fa-angle-right"></span>
                                        </li>
                                        <li>
                                            <a href="javascript:;"> Celebrity Schedule </a>
                                            <span style="font-size: 14px; margin: 0 auto;" class="fa fa-angle-right"></span>
                                        </li>
                                        <li>
                                            <span id="features" data-class-active=".specialUsers .celebritySchedule"> Edit Schedule </span>
                                        </li>
                                    </ul>
                                    <div class="page-content-inner">
                                        <%if (message != null && message.length() > 0) {%>
                                        <div id="bootstrap_alerts_demo1" >
                                            <div id="prefix_661213402591" class="custom-alerts alert alert-success fade in">
                                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                <strong > <%=message%> </strong>
                                            </div>
                                        </div>
                                        <% }%>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="tabbable-line boxless tabbable-reversed">
                                                    <div class="tab-content" style="padding-top: 15px;">
                                                        <div class="portlet box green">
                                                            <div class="portlet-title">
                                                                <div class="caption"> 
                                                                    <i class="fa fa-pencil"></i>Edit Celebrity Schedule
                                                                </div>
                                                                <div class="tools">
                                                                    <a href="javascript:;" class="collapse"> </a>
                                                                </div>
                                                            </div>
                                                            <div class="portlet-body form">
                                                                <html:form action="/updateCelebritySchedule" styleClass="form-horizontal" method="POST" acceptCharset="UTF-8">
                                                                    <html:hidden property="liveTime" />
                                                                    <html:hidden property="userType" />
                                                                    <div class="form-body" >
                                                                        <div class="table-scrollable">
                                                                            <table class="table table-striped table-bordered table-hover">
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <th style="text-align: right; vertical-align: middle;">
                                                                                            <label class="control-label">Celebrity Page <span style="color: red">*</span></label>
                                                                                        </th>
                                                                                        <td style="text-align: left;">
                                                                                            <label class="control-label"><bean:write name="CelebrityScheduleForm" property="pageName" /></label>
                                                                                            <html:hidden property="pageId" />
                                                                                            <input type="hidden" name="oldDonationPageId" value="<bean:write name="CelebrityScheduleForm" property="donationPageId" />" />
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <th style="text-align: right; vertical-align: middle;">
                                                                                            <label class="control-label">Donation Page </label>
                                                                                        </th>
                                                                                        <td style="text-align: left;">
                                                                                            <html:select styleClass="form-control" property="donationPageId" style="width: 35%;" >
                                                                                                <html:option value="0">Select Donation Page </html:option>
                                                                                                <%
                                                                                                    List<PageDTO> pageDTOs = PageService.getInstance().getDonationPages(new DonationPageDTO());
                                                                                                    for (PageDTO pageDTO : pageDTOs) {
                                                                                                        if (pageDTO.getPageActiveStatus() == 0) {
                                                                                                %>
                                                                                                <html:option value="<%=String.valueOf(pageDTO.getPageId())%>"><%=pageDTO.getPageName()%></html:option>
                                                                                                <% }
                                                                                                    }%>
                                                                                            </html:select>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <th style="text-align: right; vertical-align: middle;">
                                                                                            <label class="control-label ">Current Datetime</label>
                                                                                        </th>
                                                                                        <td style="text-align: left;">
                                                                                            <html:text property="currentLiveTime" styleClass="form-control" readonly="true" size="16" style="width : 35%;" />
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <th style="text-align: right; vertical-align: middle;">
                                                                                            <label class="control-label ">New Datetime</label>
                                                                                        </th>
                                                                                        <td style="text-align: left;">
                                                                                            <input type="text" size="16" name="newLiveTime" placeholder="yyyy-mm-dd HH:mm" class="form-control date form_datetime" title="date time" style="width: 35%;" />
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <th style="text-align: right; vertical-align: middle;">
                                                                                            <label class="control-label "> Live Schedule Serial </label>
                                                                                        </th>
                                                                                        <td style="text-align: left;">
                                                                                            <html:text property="liveScheduleSerial" styleClass="form-control" size="16" style="width : 35%;" />
                                                                                        </td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-actions">
                                                                        <div class="row">
                                                                            <div class="col-md-offset-3">
                                                                                <button type="submit" class="btn green" >Submit</button>
                                                                                &nbsp;<a class="btn green" href="showCelebrityScheduleList.do">Cancel</a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </html:form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <%@include file="/shared/footer.jsp"%>
        </div>
        <%@include file="/shared/script-list.jsp"%>
        <script type="text/javascript">
            $(".form_datetime").datetimepicker({
                autoclose: true,
                format: "yyyy-mm-dd hh:ii"
            });
        </script>
    </body>
</html>
