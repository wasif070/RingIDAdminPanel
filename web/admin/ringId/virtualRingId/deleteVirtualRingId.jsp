<%-- 
    Document   : deleteVirtualRingId
    Created on : Dec 5, 2017, 3:09:52 PM
    Author     : mamun
--%>

<%@include file="/login/loginTimeExpiry.jsp"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.ringid.admin.utils.Constants"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@include  file="/shared/head.jsp" %>
<!DOCTYPE html>
<html>
    <head>
        <title>Admin Panel | Delete Virtual RingIds </title>
        <%@include file="/shared/css-list.jsp"%>
    </head>
    <body class="page-container-bg-solid">
        <div class="page-wrapper">
            <div class="page-wrapper-row">
                <div class="page-wrapper-top">
                    <div class="page-header">
                        <%@include  file="/shared/menu.jsp" %>
                        <%
                            String msgstr = "";
                            if (request.getAttribute("message") != null && request.getAttribute("message").toString().length() > 0) {
                                msgstr = request.getAttribute("message").toString();
                            }
                        %>
                    </div>
                </div>
            </div>
            <div class="page-wrapper-row full-height">
                <div class="page-wrapper-middle">
                    <div class="page-container">
                        <div class="page-content-wrapper">
                            <div class="page-content">
                                <div class="container">
                                    <ul class="page-breadcrumb breadcrumb">
                                        <li>
                                            <a href="welcome.do">Admin Panel</a>
                                            <span style="font-size: 14px; margin: 0 auto;" class="fa fa-angle-right"></span>
                                        </li>
                                        <li>
                                            <a href="javascript:;">Manage Operations</a>
                                            <span style="font-size: 14px; margin: 0 auto;" class="fa fa-angle-right"></span>
                                        </li>
                                        <li>
                                            <span id="features" data-class-active=".manageOperations .deleteVirtualRingIDs">Delete Virtual RingIDs </span>
                                        </li>
                                    </ul>
                                    <% if (msgstr.length() > 0) {%>
                                    <div  class="custom-alerts alert alert-success fade in">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                        <strong> <%=msgstr%> </strong>
                                    </div>
                                    <% }%>
                                    <div class="page-content-inner">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="tabbable-line boxless tabbable-reversed">
                                                    <div class="tab-content" style="padding-top: 15px;">
                                                        <div class="portlet box green">
                                                            <div class="portlet-title">
                                                                <div class="caption">
                                                                    <i class="fa fa-wrench"></i> Delete Virtual RingIDs </div>
                                                                <div class="tools">
                                                                    <a href="javascript:;" class="collapse"> </a>
                                                                </div>
                                                            </div>
                                                            <div class="portlet-body form">
                                                                <html:form action="virtualRingIDs" method="POST" acceptCharset="UTF-8" styleClass="form-horizontal" >
                                                                    <div class="form-body">
                                                                        <div class="form-group">
                                                                            <label class="col-md-3 control-label">Date <span class="required"> * </span> </label>
                                                                            <div class="col-md-4">
                                                                                <div class="input-group">
                                                                                    <input type="text" name="sendDate" placeholder="yyyy-mm-dd" class="form-control date-picker input-sm input-small" data-date-format="yyyy-mm-dd" title="date" value="<bean:write name="VirtualRingIdForm" property="sendDate"/>"/>
                                                                                    <html:messages id="sendDate" property="sendDate">
                                                                                        <span style="color: red;"> Sorry. Can not delete. The date must be 7 days ago from current date. </span>
                                                                                    </html:messages>
                                                                                    <span>
                                                                                        <button class="btn green-soft uppercase bold input-sm" type="submit"><i class="icon-magnifier"></i></button>
                                                                                        <input name="action" type="hidden" value="<%=Constants.SEARCH%>" />
                                                                                    </span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group last">
                                                                            <label class="col-md-3 control-label">Virtual RingId Count Before This Date</label>
                                                                            <div class="col-md-4">
                                                                                <label class="control-label">
                                                                                    <bean:write name="VirtualRingIdForm" property="count" />
                                                                                </label>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-actions">
                                                                        <div class="row">
                                                                            <div class="col-md-offset-3 col-md-9">
                                                                                <button type="submit" class="btn red" name="submitType" value="delete" onclick="return confirm('All Virtual RingIds will be delete before this date?');">Delete</button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </html:form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <%@include file="/shared/footer.jsp"%>
        </div>
        <%@include file="/shared/script-list.jsp"%>
    </body>
</html>
