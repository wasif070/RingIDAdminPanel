<%--
    Document   : celebrityList
    Created on : Apr 19, 2017, 3:03:51 PM
    Author     : ipvision
--%>

<%@page import="com.ringid.admin.projectMenu.MenuNames"%>
<%@include file="/login/loginTimeExpiry.jsp"%>
<%@page import="org.ringid.users.UserDTO"%>
<%@page import="com.ringid.admin.utils.Constants"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@include  file="/shared/head.jsp" %>

<!DOCTYPE html>
<html>
    <head>
        <title>Admin Panel | Celebrity List </title>
        <%@include file="/shared/css-list.jsp"%>
    </head>
    <body class="page-container-bg-solid">
        <div class="page-wrapper">
            <div class="page-wrapper-row">
                <div class="page-wrapper-top">
                    <div class="page-header">
                        <%@include  file="/shared/menu.jsp" %>
                        <%
                            String FEATURE_NAME = MenuNames.SPECIAL_USER_ACCOUNT;
                            String message = "";
                            if (session.getAttribute("message") != null) {
                                message = session.getAttribute("message").toString();
                                session.removeAttribute("message");
                            }
                            int TOTAL_RECORDS = 0, TOTAL_PAGES = 0, CURRENT_PAGE_NO = 0, STARTING_RECORD_NO = 0, ENDING_RECORD_NO = 0;
                            String disabledPrev = "", disabledNext = "";
                            if (request.getAttribute(Constants.TOTAL_RECORDS) != null) {
                                TOTAL_RECORDS = Integer.valueOf(request.getAttribute(Constants.TOTAL_RECORDS).toString());
                            }
                            if (request.getAttribute(Constants.TOTAL_PAGES) != null) {
                                TOTAL_PAGES = Integer.valueOf(request.getAttribute(Constants.TOTAL_PAGES).toString());
                            }
                            if (request.getAttribute(Constants.CURRENT_PAGE_NO) != null) {
                                CURRENT_PAGE_NO = Integer.valueOf(request.getAttribute(Constants.CURRENT_PAGE_NO).toString());
                            }
                            if (request.getAttribute(Constants.STARTING_RECORD_NO) != null) {
                                STARTING_RECORD_NO = Integer.valueOf(request.getAttribute(Constants.STARTING_RECORD_NO).toString());
                            }
                            if (request.getAttribute(Constants.ENDING_RECORD_NO) != null) {
                                ENDING_RECORD_NO = Integer.valueOf(request.getAttribute(Constants.ENDING_RECORD_NO).toString());
                            }
                            if (STARTING_RECORD_NO <= 0) {
                                disabledPrev = "disabled";
                                STARTING_RECORD_NO = 0;
                            }
                            if (ENDING_RECORD_NO >= TOTAL_RECORDS) {
                                disabledNext = "disabled";
                                ENDING_RECORD_NO = TOTAL_RECORDS;
                            }
                        %>
                    </div>
                </div>
            </div>
            <div class="page-wrapper-row full-height">
                <div class="page-wrapper-middle">
                    <div class="page-container">
                        <div class="page-content-wrapper">
                            <div class="page-content">
                                <div class="container">
                                    <ul class="page-breadcrumb breadcrumb">
                                        <li>
                                            <a href="welcome.do">Admin Panel</a>
                                            <span style="font-size: 14px; margin: 0 auto;" class="fa fa-angle-right"></span>
                                        </li>
                                        <li>
                                            <a href="javascript:;">Special Users</a>
                                            <span style="font-size: 14px; margin: 0 auto;" class="fa fa-angle-right"></span>
                                        </li>
                                        <li>
                                            <a href="showCelebrityList.do">Special Users Accounts</a>
                                            <span style="font-size: 14px; margin: 0 auto;" class="fa fa-angle-right"></span>
                                        </li>
                                        <li>
                                            <span id="features" data-class-active=".specialUsers .specialUsersAccounts .manageSpecialUsersAccounts">Manage Special User Accounts</span>
                                        </li>
                                    </ul>
                                    <%if (message != null && message.length() > 0) {%>
                                    <div id="bootstrap_alerts_demo" >
                                        <div id="prefix_661213402591" class="custom-alerts alert alert-success fade in">
                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                            <strong > <%=message%> </strong>
                                        </div>
                                    </div>
                                    <% }%>
                                    <div class="page-content-inner">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="tabbable-line boxless tabbable-reversed">
                                                    <div class="tab-content" style="padding-top: 15px;">
                                                        <div class="portlet box green">
                                                            <div class="portlet-title">
                                                                <div class="caption">
                                                                    <i class="fa fa-list-ul"></i> Special User Accounts Info
                                                                </div>
                                                                <div class="tools">
                                                                    <a href="javascript:;" class="collapse"> </a>

                                                                    <a data-original-title="Reload" href="showCelebrityList.do" style="width: 13px; background-image: url(assets/global/img/portlet-reload-icon-white.png);" class="tooltips"> </a>
                                                                    <%if ((loginDTO.getPermissionLevel() == Constants.SUPER_ADMIN) || (loginDTO.getFeauterMapList().get(FEATURE_NAME).getHasAddPermission())) {%>
                                                                    <a data-original-title="AddNew" style="color: white; width: 16px; background-image: url(assets/global/img/add-1.png);" href="admin/ringId/celebrity/makeCelebrity.jsp" class="tooltips"> </a>
                                                                    <% }%>
                                                                </div>
                                                            </div>
                                                            <div class="portlet-body form">
                                                                <html:form action="/showCelebrityList" styleClass="form-horizontal" method="POST"  acceptCharset="UTF-8">
                                                                    <div class="form-body" >
                                                                        <div class="form-group">
                                                                            <div class="col-md-12 col-sm-12">
                                                                                <label>
                                                                                    <html:select styleId="dropdowntype" styleClass="form-control input-sm input-inline" property="type" onchange="submitform(1,1,this)">
                                                                                        <html:option value="1">Featured Streamers</html:option>
                                                                                        <html:option value="2">Celebrity</html:option>
                                                                                    </html:select>
                                                                                </label>
                                                                                <logic:equal name="CelebrityForm" property="type" value="1">
                                                                                    <label>
                                                                                        <html:select styleClass="form-control input-sm input-inline" property="pageType" size="1">
                                                                                            <html:option value="0">--Type--</html:option>
                                                                                            <html:option value="3">User Page</html:option>
                                                                                            <html:option value="10">Celebrity</html:option>
                                                                                        </html:select> 
                                                                                    </label>
                                                                                </logic:equal>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <div class="col-md-6 col-sm-6">
                                                                                <label>
                                                                                    <html:select styleClass="bs-select form-control input-sm input-xsmall input-inline" property="recordPerPage" onchange="javascript: submitform(1, 1, this);">
                                                                                        <html:option value="2">2</html:option>
                                                                                        <html:option value="10">10</html:option>
                                                                                        <html:option value="25">25</html:option>
                                                                                        <html:option value="50">50</html:option>
                                                                                        <html:option value="100">100</html:option>
                                                                                        <html:option value="200">200</html:option>
                                                                                    </html:select>
                                                                                    records</label>
                                                                            </div>
                                                                            <div class="col-md-6 col-sm-6">
                                                                                <div class="input-group" style="float: right;">
                                                                                    <input name="searchText" title="Name" value="<bean:write name="CelebrityForm" property="searchText"/>" class="form-control input-sm input-small" placeholder="Search"/>
                                                                                    <span>
                                                                                        <button class="btn green-soft uppercase bold input-sm" type="submit"><i class="icon-magnifier"></i></button>
                                                                                        <input name="action" type="hidden" value="<%=Constants.SEARCH%>" />
                                                                                        <html:hidden property="column" />
                                                                                        <html:hidden property="sort" />
                                                                                    </span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="table-scrollable">
                                                                            <table class="table table-striped table-bordered table-hover">
                                                                                <thead>
                                                                                    <tr>
                                                                                        <th style="text-align: center" class="no-sort">
                                                                                            <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                                                                                <input type="checkbox" onchange="checkAll(this)" class="group-checkable" />
                                                                                                <span></span>
                                                                                            </label>
                                                                                        </th>
                                                                                        <th id="col_1" class="text-center align-middle sortable">ID</th>
                                                                                        <th id="col_2" class="text-center align-middle sortable">Name</th>
                                                                                        <th class="text-center align-middle">Profile Image</th>
                                                                                            <logic:equal name="CelebrityForm" property="type" value="1">
                                                                                            <th id="col_3" class="text-center align-middle sortable">Weight</th>
                                                                                            <th class="text-center align-middle">Type</th>
                                                                                            </logic:equal>
                                                                                            <logic:equal name="CelebrityForm" property="type" value="2">
                                                                                            <th class="text-center align-middle">OwnerId</th>
                                                                                            <th class="text-center align-middle ">PageStatus</th>
                                                                                            <th  class="text-center align-middle">AutoFollowStatus</th>
                                                                                            </logic:equal>
                                                                                        <th class="text-center align-middle">Country</th>
                                                                                            <%if ((loginDTO.getPermissionLevel() == Constants.SUPER_ADMIN) || (loginDTO.getFeauterMapList().get(FEATURE_NAME).getHasModifyPermission() || loginDTO.getFeauterMapList().get(FEATURE_NAME).getHasDeletePermission())) { %>
                                                                                        <th class="text-center align-middle">Action</th>
                                                                                            <%}%>
                                                                                    </tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                    <%
                                                                                        ArrayList<UserDTO> data1 = (ArrayList<UserDTO>) session.getAttribute(Constants.DATA_ROWS);
                                                                                        session.removeAttribute(Constants.DATA_ROWS);
                                                                                        if (data1 != null && data1.size() > 0) {
                                                                                            for (int i = STARTING_RECORD_NO; i < ENDING_RECORD_NO; i++) {
                                                                                                UserDTO dto = (UserDTO) data1.get(i);
                                                                                    %>
                                                                                    <tr>
                                                                                        <logic:equal name="CelebrityForm" property="type" value="1">
                                                                                            <td class="text-center align-middle">
                                                                                                <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                                                                                    <input type="checkbox" class="checkboxes" onchange="checkItem(this)" name="selectedIDs" value="<%=dto.getRingID() + "," + dto.getWeight() + "," + dto.getFullName()%>" />
                                                                                                    <span></span>
                                                                                                </label>
                                                                                            </td>
                                                                                            <td class="text-center align-middle"><%=dto.getRingID()%></td>
                                                                                            <td class="text-center align-middle"><%=dto.getFullName()%></td>
                                                                                            <td class="text-center align-middle">
                                                                                                <%
                                                                                                    if (dto.getProfileImage() != null && dto.getProfileImage().length() > 0) {
                                                                                                %>
                                                                                                <img style="height: 100px; width: 100px" src="<%=Constants.IMAGE_BASE_URL%><%=dto.getProfileImage()%>" />
                                                                                                <%
                                                                                                } else {
                                                                                                %>
                                                                                                <img style="height: 100px; width: 100px" src="resources/images/default-pp.jpg" />                                                                                    
                                                                                                <%
                                                                                                    }
                                                                                                %>                                                                                    
                                                                                            </td>
                                                                                            <td class="text-center align-middle"> <%=dto.getWeight()%> </td>
                                                                                            <td class="text-center align-middle">
                                                                                                <% if (dto.getUserType() == 3) { %>
                                                                                                User Page
                                                                                                <% } else if (dto.getUserType() == 10) { %>
                                                                                                Celebrity
                                                                                                <% } else {%>
                                                                                                <%=dto.getUserType()%>
                                                                                                <% }%>
                                                                                            </td>
                                                                                            <td class="text-center align-middle"> <%=dto.getCountry()%> </td>
                                                                                            <%if ((loginDTO.getPermissionLevel() == Constants.SUPER_ADMIN) || (loginDTO.getFeauterMapList().get(FEATURE_NAME).getHasModifyPermission() || loginDTO.getFeauterMapList().get(FEATURE_NAME).getHasDeletePermission())) {%>
                                                                                            <td class="text-center align-middle">
                                                                                                <%if ((loginDTO.getPermissionLevel() == Constants.SUPER_ADMIN) || (loginDTO.getFeauterMapList().get(FEATURE_NAME).getHasModifyPermission())) {%>
                                                                                                &nbsp;<a href="editFeaturedUser.do?id=<%=dto.getRingID()%>&weight=<%=dto.getWeight()%>&type=1" class="btn btn-sm green" title="Edit"><i class="fa fa-edit"></i> Edit</a>
                                                                                                <% }%>
                                                                                                <%if ((loginDTO.getPermissionLevel() == Constants.SUPER_ADMIN) || (loginDTO.getFeauterMapList().get(FEATURE_NAME).getHasDeletePermission())) {%>
                                                                                                &nbsp;<a href="deleteFeaturedUser.do?id=<%=dto.getRingID()%>&weight=<%=dto.getWeight()%>&verificationState=1&type=1" class="btn btn-sm green" title="Delete and keep streamer verified" onclick="javascript:return confirm('Are you sure you want to delete this content with VERIFIED?')"><i class="fa fa-times"></i> With Verify</a>
                                                                                                &nbsp;<a href="deleteFeaturedUser.do?id=<%=dto.getRingID()%>&weight=<%=dto.getWeight()%>&verificationState=0&type=1" class="btn btn-sm green" title="Delete and unverify streamer" onclick="javascript:return confirm('Are you sure you want to delete this content with UNVERIFIED?')"><i class="fa fa-times"></i> With Unverify</a>
                                                                                                <% }%>
                                                                                            </td>
                                                                                            <% }%>
                                                                                        </logic:equal>

                                                                                        <logic:equal name="CelebrityForm" property="type" value="2">
                                                                                            <td  class="text-center align-middle">
                                                                                                <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                                                                                    <input type="checkbox" class="checkboxes" onchange="checkItem(this)" name="selectedIDs" value="<%=dto.getCelebrityDTO().getPageId()%>" />
                                                                                                    <span></span>
                                                                                                </label>
                                                                                            </td>
                                                                                            <td class="text-center align-middle"><%=dto.getCelebrityDTO().getPageRingId()%></td>
                                                                                            <td class="text-center align-middle"><%=dto.getCelebrityDTO().getPageName()%></td>
                                                                                            <td class="text-center align-middle">
                                                                                                <%
                                                                                                    if (dto.getCelebrityDTO().getProfileImageUrl() != null && dto.getCelebrityDTO().getProfileImageUrl().length() > 0) {
                                                                                                %>
                                                                                                <img style="height: 100px; width: 100px" src="<%=Constants.IMAGE_BASE_URL%><%=dto.getCelebrityDTO().getProfileImageUrl()%>" />
                                                                                                <%
                                                                                                } else {
                                                                                                %>
                                                                                                <img style="height: 100px; width: 100px" src="resources/images/default-pp.jpg" />                                                                                    
                                                                                                <%
                                                                                                    }
                                                                                                %>                                                                                    
                                                                                            </td>
                                                                                            <td class="text-center align-middle"><%=dto.getCelebrityDTO().getPageOwner()%></td>
                                                                                            <%if (dto.getCelebrityDTO().getPageActiveStatus() == 0) { %>
                                                                                            <td class="text-center align-middle"> Active </td>
                                                                                            <% } else if (dto.getCelebrityDTO().getPageActiveStatus() == 1) { %>
                                                                                            <td class="text-center align-middle"> Deactive </td>
                                                                                            <% } else if (dto.getCelebrityDTO().getPageActiveStatus() == 2) { %>
                                                                                            <td  class="text-center align-middle"> Closed </td>
                                                                                            <% }%>
                                                                                            <%if (dto.getCelebrityDTO().isIsAutoFollowDisabled() == null || dto.getCelebrityDTO().isIsAutoFollowDisabled()) {%>
                                                                                            <td class="text-center align-middle"> <a href="autoFollowSetting.do?pageId=<%=dto.getCelebrityDTO().getPageId()%>&autoFollowStatus=1&goback=celebrity&type=2" class="btn btn-sm green" title="click to enable auto follow"><span class="fa fa-play"></span> Disabled </a> </td>
                                                                                            <% } else {%>
                                                                                            <td  class="text-center align-middle"> <a href="autoFollowSetting.do?pageId=<%=dto.getCelebrityDTO().getPageId()%>&autoFollowStatus=0&goback=celebrity&type=2" class="btn btn-sm green" title="click to disable auto follow"><span class="fa fa-stop"></span> Enabled </a> </td> </td>
                                                                                            <% }%>
                                                                                            <td class="text-center align-middle"> <%=dto.getCelebrityDTO().getCountry()%> </td>
                                                                                            <%if ((loginDTO.getPermissionLevel() == Constants.SUPER_ADMIN) || (loginDTO.getFeauterMapList().get(FEATURE_NAME).getHasModifyPermission() || loginDTO.getFeauterMapList().get(FEATURE_NAME).getHasDeletePermission())) {%>
                                                                                            <td class="text-center align-middle">
                                                                                                <%if ((loginDTO.getPermissionLevel() == Constants.SUPER_ADMIN) || (loginDTO.getFeauterMapList().get(FEATURE_NAME).getHasModifyPermission())) {%>
                                                                                                &nbsp;<a href="editCelebrityInfo.do?id=<%=dto.getCelebrityDTO().getPageId()%>&action=<%=Constants.EDIT%>" class="" title="change"><span class="btn btn-sm green fa fa-edit">  </span></a>
                                                                                                &nbsp;<a href="editPageFollowRules.do?pageId=<%=dto.getCelebrityDTO().getPageId()%>&pageType=10" class="btn btn-sm green" title="change page rules"><span class="fa fa-edit"></span> Page Rules</a>
                                                                                                <%}%>
                                                                                                <%if ((loginDTO.getPermissionLevel() == Constants.SUPER_ADMIN) || (loginDTO.getFeauterMapList().get(FEATURE_NAME).getHasDeletePermission())) {%>
                                                                                                <a style="display: block; margin-top: 3px;" href="deleteFeaturedUser.do?id=<%=dto.getCelebrityDTO().getPageId()%>&type=2" class="" title="Delete" onclick="javascript:return confirm('Are you sure you want to delete this content')"><span class="btn btn-sm green fa fa-remove"></span></a>
                                                                                                    <% }%>
                                                                                            </td>
                                                                                            <%}%>
                                                                                        </logic:equal>

                                                                                        <% } %>        
                                                                                    </tr>
                                                                                    <%
                                                                                        }
                                                                                    %>
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                        <div class="row">
                                                                            <div class="col-md-5 col-sm-12">
                                                                                <div>
                                                                                    Showing <%=(STARTING_RECORD_NO + 1)%> to <%=ENDING_RECORD_NO%> of <%=TOTAL_RECORDS%> entries
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-7 col-sm-12">
                                                                                <div style="float: right;">
                                                                                    <div> 
                                                                                        Page <a href="javascript: submitform(2, <%=(CURRENT_PAGE_NO - 1)%>, this);" class="btn btn-sm default prev <%=disabledPrev%>"><i class="fa fa-angle-left"></i></a>
                                                                                        <input name="pageNo" class="form-control input-sm input-inline input-mini" maxlenght="5" style="text-align:center; margin: 0 5px;" type="text" value="<%=CURRENT_PAGE_NO%>" onchange="javascript: submitform(2, 0, this);">
                                                                                        <a href="javascript: submitform(2, <%=(CURRENT_PAGE_NO + 1)%>, this);" class="btn btn-sm default next <%=disabledNext%>"><i class="fa fa-angle-right"></i></a> of <span><%=TOTAL_PAGES%></span>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-actions">
                                                                        <div class="row">
                                                                            <div class="col-md-offset-3 col-md-9">
                                                                                <logic:equal name="CelebrityForm" property="type" value="1">
                                                                                    <input type="hidden" name="type" value="1" />
                                                                                    <button type="submit" class="btn green" formaction="editFeaturedUser.do?editType=multiple">Edit</button>
                                                                                    <button type="submit" class="btn red" formaction="deleteFeaturedUser.do?verificationState=1" onclick="javascript:return confirm('Are you sure you want to delete this content with VERIFIED?')">Delete With Verify</button>
                                                                                    <button type="submit" class="btn red" formaction="deleteFeaturedUser.do?verificationState=0" onclick="javascript:return confirm('Are you sure you want to delete this content with UNVERIFIED?')">Delete With Unverify</button>
                                                                                </logic:equal>
                                                                                <logic:equal name="CelebrityForm" property="type" value="2">
                                                                                    <input type="hidden" name="type" value="2" />
                                                                                    <button type="submit" class="btn red" formaction="deleteFeaturedUser.do" onclick="javascript:return confirm('Are you sure you want to delete this content')">Delete</button>
                                                                                </logic:equal>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </html:form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <%@include file="/shared/footer.jsp"%>
        </div>
        <%@include file="/shared/script-list.jsp"%>
    </body>
</html>
