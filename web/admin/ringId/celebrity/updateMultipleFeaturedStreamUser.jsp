<%-- 
    Document   : updateMultipleFeaturedStreamUser
    Created on : May 12, 2018, 12:04:34 PM
    Author     : mamun
--%>

<%@page import="com.ringid.admin.actions.ringId.celebrity.SpecialUsersDTO"%>
<%@include file="../../../login/loginTimeExpiry.jsp"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ page import="java.util.*,com.ringid.admin.utils.Constants"%>
<%@include  file="/shared/head.jsp" %>

<!DOCTYPE html>
<html>
    <head>
        <title>Admin Panel | Assign User Speciality  </title>
        <%@include file="/shared/css-list.jsp"%>
    </head>
    <body class="page-container-bg-solid">
        <div class="page-wrapper">
            <div class="page-wrapper-row">
                <div class="page-wrapper-top">
                    <div class="page-header">
                        <%@include  file="/shared/menu.jsp" %>
                    </div>
                </div>
            </div>
            <div class="page-wrapper-row full-height">
                <div class="page-wrapper-middle">
                    <div class="page-container">
                        <div class="page-content-wrapper">
                            <div class="page-content">
                                <div class="container">
                                    <ul class="page-breadcrumb breadcrumb">
                                        <li>
                                            <a href="welcome.do">Admin Panel</a>
                                            <span style="font-size: 14px; margin: 0 auto;" class="fa fa-angle-right"></span>
                                        </li>
                                        <li>
                                            <a href="javascript:;">RingID</a>
                                            <span style="font-size: 14px; margin: 0 auto;" class="fa fa-angle-right"></span>
                                        </li>
                                        <li>
                                            <a href="javascript:;">Special User</a>
                                            <span style="font-size: 14px; margin: 0 auto;" class="fa fa-angle-right"></span>
                                        </li>
                                        <li>
                                            <span id="features" data-class-active=".specialUsers .specialUsersAccounts">Make Special</span>
                                        </li>
                                    </ul>
                                    <logic:messagesPresent property="errormsg" message="false">
                                        <html:messages id="errormsg" message="false">
                                            <logic:present name="errormsg">
                                                <div  class="custom-alerts alert alert-danger fade in">
                                                    <button type="button" class="close" data-dismiss="alert"
                                                            aria-hidden="true"></button>
                                                    <strong> <bean:write name="errormsg" filter="false" /> </strong>
                                                </div>
                                            </logic:present>
                                        </html:messages>
                                    </logic:messagesPresent>
                                    <div class="page-content-inner">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="tabbable-line boxless tabbable-reversed">
                                                    <div class="tab-content" style="padding-top: 15px;">
                                                        <div class="portlet box green">
                                                            <div class="portlet-title">
                                                                <div class="caption">
                                                                    <i class="fa fa-pencil"></i> Assign Speciality
                                                                </div>
                                                                <div class="tools">
                                                                    <a href="javascript:;" class="collapse"> </a>
                                                                </div>
                                                            </div>
                                                            <div class="portlet-body form">
                                                                <html:form action="/updateMultipleFeaturedUser" method="POST" acceptCharset="UTF-8" styleClass="form-horizontal" >
                                                                    <div class="form-body">
                                                                        <div class="form-group">
                                                                            <div class="col-md-12">
                                                                                <label class="control-label">Apply Common Weight
                                                                                    <label style="margin-left: 10px;">
                                                                                        <input name="commonWeight" type="checkbox" onchange="toggleCommonWeight(this)">
                                                                                    </label>
                                                                                </label>
                                                                                <html:text property="weight" title="weight" styleClass="form-control input-small input-inline" />
                                                                            </div>
                                                                        </div>
                                                                        <div class="table-scrollable">
                                                                            <table class="table table-striped table-bordered table-hover">
                                                                                <thead>
                                                                                    <tr>
                                                                                        <th class="text-center align-middle">
                                                                                            <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                                                                                <input type="checkbox" onchange="checkAll(this)" class="group-checkable" />
                                                                                                <span></span>
                                                                                            </label>
                                                                                        </th>
                                                                                        <th class="text-center">Name</th>
                                                                                        <th class="text-center">RingId</th>
                                                                                        <th class="text-center">Weight</th>
                                                                                    </tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                    <%
                                                                                        List<SpecialUsersDTO> data1 = (List<SpecialUsersDTO>) session.getAttribute(Constants.DATA_ROWS);
                                                                                        session.removeAttribute(Constants.DATA_ROWS);
                                                                                        if (data1 != null && data1.size() > 0) {
                                                                                            //out.print(data.size());
                                                                                            for (int i = 0; i < data1.size(); i++) {
                                                                                                SpecialUsersDTO dto = (SpecialUsersDTO) data1.get(i);
                                                                                    %>
                                                                                    <tr>
                                                                                        <td class="text-center align-middle">
                                                                                            <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                                                                                <input name="selectedIDs" type="checkbox" class="checkboxes" value="<%=dto.getUserId()%>" />
                                                                                                <span></span>
                                                                                            </label>
                                                                                        </td>
                                                                                        <td class="text-center"><%=dto.getName()%></td>
                                                                                        <td class="text-center"><%=dto.getUserId()%></td>
                                                                                        <td class="text-center"><input class="form-control input-small input-inline" name="weights" type="text" value="<%=dto.getWeight()%>"/></td>    
                                                                                    </tr>
                                                                                    <%
                                                                                            }
                                                                                        }
                                                                                    %>
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                    </div> 
                                                                    <div class="form-actions">
                                                                        <div class="row">
                                                                            <div class="col-md-offset-3 col-md-9">
                                                                                <button type="submit" class="btn green" name="submitType" onclick="javascript:setWieghtWithUserIds()">Submit</button>
                                                                                <a href="showCelebrityList.do" class="btn default" >Cancel</a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </html:form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <%@include file="/shared/footer.jsp"%>
        </div>
        <%@include file="/shared/script-list.jsp"%>
        <script type="text/javascript">
            var selectedIds = document.getElementsByName("selectedIDs"), weights = document.getElementsByName("weights");
            function toggleCommonWeight(obj) {
                for (var i = 0; i < weights.length; i++) {
                    weights[i].disabled = obj.checked;
                }
            }

            function setWieghtWithUserIds() {
                for (var i = 0; i < selectedIDs.length; i++) {
                    if (selectedIDs[i].checked) {
                        selectedIDs[i].value = selectedIDs[i].value + "," + weights[i].value;
                    }
                }
                weights.prop("disabled", true);
            }
        </script>
    </body>
</html>
