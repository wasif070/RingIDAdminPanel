<%-- 
    Document   : updateCelebrityInfo
    Created on : Jul 15, 2017, 6:06:37 PM
    Author     : ipvision
--%>

<%@page import="com.ringid.admin.projectMenu.MenuNames"%>
<%@page import="org.ringid.celebrities.CelebrityCategoryDTO"%>
<%@page import="java.util.List"%>
<%@include file="/login/loginTimeExpiry.jsp"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@include  file="/shared/head.jsp" %>

<!DOCTYPE html>
<html>
    <head>
        <title>Admin Panel | Edit Celebrity Info </title>
        <%@include file="/shared/css-list.jsp"%>
    </head>
    <body class="page-container-bg-solid">
        <div class="page-wrapper">
            <div class="page-wrapper-row">
                <div class="page-wrapper-top">
                    <div class="page-header">
                        <%@include  file="/shared/menu.jsp" %>
                        <%
                            String FEATURE_NAME = MenuNames.SPECIAL_USER_ACCOUNT;
                            String message = "";
                            if (request.getAttribute("message") != null) {
                                message = request.getAttribute("message").toString();
                            }
                        %>
                    </div>
                </div>
            </div>
            <div class="page-wrapper-row full-height">
                <div class="page-wrapper-middle">
                    <div class="page-container">
                        <div class="page-content-wrapper">
                            <div class="page-content">
                                <div class="container">
                                    <ul class="page-breadcrumb breadcrumb">
                                        <li>
                                            <a href="welcome.do">Admin Panel</a>
                                            <span style="font-size: 14px; margin: 0 auto;" class="fa fa-angle-right"></span>
                                        </li>
                                        <li>
                                            <a href="javascript:;">Special Users</a>
                                            <span style="font-size: 14px; margin: 0 auto;" class="fa fa-angle-right"></span>
                                        </li>
                                        <li>
                                            <a href="showCelebrityList.do">Special User Accounts</a>
                                            <span style="font-size: 14px; margin: 0 auto;" class="fa fa-angle-right"></span>
                                        </li>
                                        <li>
                                            <span id="features" data-class-active=".specialUsers .specialUsersAccounts">Edit Celebrity Info</span>
                                        </li>
                                    </ul>
                                    <%if (message.length() > 0) {%>
                                    <div id="bootstrap_alerts_demo" >
                                        <div id="prefix_661213402591" class="custom-alerts alert alert-success fade in">
                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                            <strong > <%=message%> </strong>
                                        </div>
                                    </div>
                                    <% }%>
                                    <div class="page-content-inner">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="tabbable-line boxless tabbable-reversed">
                                                    <div class="tab-content" style="padding-top: 15px;">
                                                        <div class="portlet box green">
                                                            <div class="portlet-title">
                                                                <div class="caption">
                                                                    <i class="fa fa-pencil"></i> Edit Celebrity Info
                                                                </div>
                                                                <div class="tools">
                                                                    <a href="javascript:;" class="collapse"> </a>
                                                                </div>
                                                            </div>
                                                            <div class="portlet-body form">
                                                                <html:form action="/updateCelebrityInfo" method="POST" acceptCharset="UTF-8" styleClass="form-horizontal" >
                                                                    <input type="hidden" name="type" value="1" id="type" />
                                                                    <div class="form-body">
                                                                        <ul class="nav nav-tabs">
                                                                            <li class="active">
                                                                                <a href="#tab_1_1" data-toggle="tab" onclick="setType(1)" > Category </a>
                                                                            </li>
                                                                            <li>
                                                                                <a href="#tab_1_2" data-toggle="tab" onclick="setType(2)" > Min Gift </a>
                                                                            </li>
                                                                            <li>
                                                                                <a href="#tab_1_3" data-toggle="tab" onclick="setType(3)" > Call & Chat </a>
                                                                            </li>
                                                                        </ul>
                                                                        <div class="tab-content">
                                                                            <div class="tab-pane fade active in" id="tab_1_1">
                                                                                <div class="form-group">
                                                                                    <label class="col-md-5 control-label"> UserId : </label>
                                                                                    <div class="col-md-2">
                                                                                        <html:text property="userId" title="userId" styleClass="form-control" readonly="true"/>
                                                                                    </div>
                                                                                </div>
                                                                                <table class="table table-striped table-bordered table-hover table-checkable order-column">
                                                                                    <thead>
                                                                                        <tr>
                                                                                            <th style="text-align: center" class="no-sort">
                                                                                                <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                                                                                    <input type="checkbox" onchange="checkAll(this)" class="group-checkable"/>
                                                                                                    <span></span>
                                                                                                </label>
                                                                                            </th>
                                                                                            <th style="text-align: center;" class="no-sort">Category Name</th>
                                                                                        </tr>
                                                                                    </thead>
                                                                                    <tbody>
                                                                                        <%
                                                                                            String className = "odd gradeX";
                                                                                            List<CelebrityCategoryDTO> data1 = (List<CelebrityCategoryDTO>) session.getAttribute(Constants.DATA_ROWS);
                                                                                            List<String> categoryListOfCelebrity = (List<String>) request.getAttribute("categoryListOfCelebrity");
                                                                                            session.removeAttribute(Constants.DATA_ROWS);
                                                                                            if (data1 != null && data1.size() > 0) {
                                                                                                for (int i = 0; i < data1.size(); i++) {
                                                                                                    CelebrityCategoryDTO dto = (CelebrityCategoryDTO) data1.get(i);
                                                                                                    if (i % 2 == 0) {
                                                                                                        className = "even gradeX";
                                                                                                    } else {
                                                                                                        className = "odd gradeX";
                                                                                                    }
                                                                                        %>
                                                                                        <tr class="<%=className%>" >
                                                                                            <th style="text-align: center"> <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                                                                                    <input name="selectedIDs" type="checkbox" class="checkboxes" value="<%=dto.getCategoryID()%>" <%if (categoryListOfCelebrity.contains(dto.getCategoryName())) {%> checked <%}%>/>
                                                                                                    <span></span>
                                                                                                </label>
                                                                                            </th>
                                                                                            <td style="text-align: center;"><%=dto.getCategoryName()%></td>
                                                                                            <% } %>        
                                                                                        </tr>
                                                                                        <%
                                                                                            }
                                                                                        %>
                                                                                    </tbody>
                                                                                </table>
                                                                            </div>
                                                                            <div class="tab-pane fade" id="tab_1_2">
                                                                                <table class="table table-striped table-bordered table-hover order-column">
                                                                                    <tbody>
                                                                                        <tr>
                                                                                            <th style="text-align: right;">
                                                                                                <label class="control-label"> Min Gift <span class="required"> * </span> </label>
                                                                                            </th>
                                                                                            <td style="text-align: left;">
                                                                                                <div class="col-md-4">
                                                                                                    <html:text property="gift" title="celebrity minimum gift" styleClass="form-control" />
                                                                                                    <html:messages id="gift" property="gift">
                                                                                                        <span style="color: red;"> Min Gift Required </span>
                                                                                                    </html:messages>
                                                                                                </div>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                            </div>
                                                                            <div class="tab-pane fade" id="tab_1_3">
                                                                                <table class="table table-striped table-bordered table-hover order-column">
                                                                                    <tbody>
                                                                                        <tr>
                                                                                            <th style="text-align: right;">
                                                                                                <label class="control-label">Anonymous Call </label>
                                                                                            </th>
                                                                                            <td style="text-align: left;">
                                                                                                <div class="col-md-4">
                                                                                                    <html:radio property="anonymousCall" style="margin-top: 11px; margin-right: 3px;" value="1" >On</html:radio>                     
                                                                                                    <html:radio property="anonymousCall" style="margin-left: 20px; margin-right: 3px;" value="0">Off</html:radio>
                                                                                                    <html:messages id="anonymousCall" property="anonymousCall">
                                                                                                        <bean:write name="anonymousCall"  filter="false"/>
                                                                                                    </html:messages>
                                                                                                </div>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <th style="text-align: right;">
                                                                                                <label class="control-label">Anonymous Chat </label>
                                                                                            </th>
                                                                                            <td style="text-align: left;">
                                                                                                <div class="col-md-4">
                                                                                                    <html:radio property="anonymousChat" style="margin-top: 11px; margin-right: 3px;" value="1" >On</html:radio>                     
                                                                                                    <html:radio property="anonymousChat" style="margin-left: 20px; margin-right: 3px;" value="0">Off</html:radio>
                                                                                                    <html:messages id="anonymousChat" property="anonymousChat">
                                                                                                        <bean:write name="anonymousChat"  filter="false"/>
                                                                                                    </html:messages>
                                                                                                </div>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </tbody>
                                                                                </table>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-actions">
                                                                        <div class="row">
                                                                            <div class="col-md-offset-3 col-md-9">
                                                                                <button type="submit" class="btn green" name="saveButton">Submit</button>
                                                                                <a href="showCelebrityList.do?type=2" class="btn default" >Cancel</a>
                                                                                <input type="hidden" name="action" value="<%=Constants.UPDATE%>"/>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </html:form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <%@include file="/shared/footer.jsp"%>
        </div>
        <%@include file="/shared/script-list.jsp"%>
        <script type="text/javascript">
            var type = document.getElementById("type");
            function setType(type) {
                type.value = type;
            }
        </script>
    </body>

</html>