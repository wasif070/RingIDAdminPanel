<%-- 
    Document   : reset-retry-limit
    Created on : Apr 17, 2017, 1:10:52 PM
    Author     : ipvision
--%>
<%@include file="../../../login/loginTimeExpiry.jsp"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ page import="java.util.*,com.ringid.admin.utils.Constants"%>
<%@include  file="/shared/head.jsp" %>

<!DOCTYPE html>
<html>
    <head>
        <title>Admin Panel | Edit Featured User Weight  </title>
        <%@include file="/shared/css-list.jsp"%>
    </head>
    <body class="page-container-bg-solid">
        <div class="page-wrapper">
            <div class="page-wrapper-row">
                <div class="page-wrapper-top">
                    <div class="page-header">
                        <%@include  file="/shared/menu.jsp" %>
                    </div>
                </div>
            </div>
            <div class="page-wrapper-row full-height">
                <div class="page-wrapper-middle">
                    <div class="page-container">
                        <div class="page-content-wrapper">
                            <div class="page-content">
                                <div class="container">
                                    <ul class="page-breadcrumb breadcrumb">
                                        <li>
                                            <a href="welcome.do">Admin Panel</a>
                                            <span style="font-size: 14px; margin: 0 auto;" class="fa fa-angle-right"></span>
                                        </li>
                                        <li>
                                            <a href="javascript:;">RingID</a>
                                            <span style="font-size: 14px; margin: 0 auto;" class="fa fa-angle-right"></span>
                                        </li>
                                        <li>
                                            <a href="javascript:;">Special User</a>
                                            <span style="font-size: 14px; margin: 0 auto;" class="fa fa-angle-right"></span>
                                        </li>
                                        <li>
                                            <span id="features" data-class-active=".specialUsers .specialUsersAccounts">Edit Weight</span>
                                        </li>
                                    </ul>
                                    <logic:messagesPresent property="errormsg" message="false">
                                        <html:messages id="errormsg" message="false">
                                            <logic:present name="errormsg">
                                                <div  class="custom-alerts alert alert-danger fade in">
                                                    <button type="button" class="close" data-dismiss="alert"
                                                            aria-hidden="true"></button>
                                                    <strong> <bean:write name="errormsg" filter="false" /> </strong>
                                                </div>
                                            </logic:present>
                                        </html:messages>
                                    </logic:messagesPresent>
                                    <div class="page-content-inner">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="tabbable-line boxless tabbable-reversed">
                                                    <div class="tab-content" style="padding-top: 15px;">
                                                        <div class="portlet box green">
                                                            <div class="portlet-title">
                                                                <div class="caption">
                                                                    <i class="fa fa-pencil"></i> Edit Featured Page Weight
                                                                </div>
                                                                <div class="tools">
                                                                    <a href="javascript:;" class="collapse"> </a>
                                                                </div>
                                                            </div>
                                                            <div class="portlet-body form">
                                                                <html:form action="/updateFeaturedUser" method="POST" acceptCharset="UTF-8" styleClass="form-horizontal" >
                                                                    <input type="hidden" name="action" value="5" />
                                                                    <html:hidden property="type" />
                                                                    <div class="form-body">
                                                                        <div class="form-group">
                                                                            <label class="col-md-3 control-label">RingId <span class="required"> * </span> </label>
                                                                            <div class="col-md-4">
                                                                                <html:text property="userId" title="ringId" styleClass="form-control" readonly="true"/>
                                                                                <html:messages id="userId" property="userId">
                                                                                    <span style="color: red;"> RingID Required </span>
                                                                                </html:messages>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label class="col-md-3 control-label">Weight <span class="required"> * </span> </label>
                                                                            <div class="col-md-4">
                                                                                <html:text property="weight" title="weight" styleClass="form-control" />
                                                                                <html:messages id="weight" property="weight">
                                                                                    <span style="color: red;"> Weight Required </span>
                                                                                </html:messages>
                                                                            </div>
                                                                        </div>
                                                                    </div> 
                                                                    <div class="form-actions">
                                                                        <div class="row">
                                                                            <div class="col-md-offset-3 col-md-9">
                                                                                <button type="submit" class="btn green" name="saveButton">Submit</button>
                                                                                <a href="showCelebrityList.do" class="btn default" >Cancel</a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </html:form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <%@include file="/shared/footer.jsp"%>
        </div>
        <%@include file="/shared/script-list.jsp"%>
    </body>
</html>
