<%-- 
    Document   : appConstantsList-new
    Created on : Jan 22, 2018, 3:17:49 PM
    Author     : mamun
--%>

<%@page import="com.ringid.admin.projectMenu.MenuNames"%>
<%@include file="/login/loginTimeExpiry.jsp"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="org.ringid.pages.ServiceDTO"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ page import="java.util.*,com.ringid.admin.utils.Constants"%>
<%@include  file="/shared/head.jsp" %>

<!DOCTYPE html>
<html>
    <head>
        <title>Admin Panel | Assign User Speciality  </title>
        <%@include file="/shared/css-list.jsp"%>
    </head>
    <body class="page-container-bg-solid">
        <div class="page-wrapper">
            <div class="page-wrapper-row">
                <div class="page-wrapper-top">
                    <div class="page-header">
                        <%@include  file="/shared/menu.jsp" %>
                        <%
                            String FEATURE_NAME = MenuNames.SPECIAL_USER_ACCOUNT;
                            int TOTAL_RECORDS = 0, TOTAL_PAGES = 0, CURRENT_PAGE_NO = 0, STARTING_RECORD_NO = 0, ENDING_RECORD_NO = 0;
                            String disabledPrev = "", disabledNext = "", ringIdField = "", classForTab_0 = "", classForTab_1 = "";
                            if (request.getAttribute("ringId") != null) {
                                ringIdField = request.getAttribute("ringId").toString();
                            }
                            if (request.getAttribute(Constants.TOTAL_RECORDS) != null) {
                                TOTAL_RECORDS = Integer.valueOf(request.getAttribute(Constants.TOTAL_RECORDS).toString());
                            }
                            if (request.getAttribute(Constants.TOTAL_PAGES) != null) {
                                TOTAL_PAGES = Integer.valueOf(request.getAttribute(Constants.TOTAL_PAGES).toString());
                            }
                            if (request.getAttribute(Constants.CURRENT_PAGE_NO) != null) {
                                CURRENT_PAGE_NO = Integer.valueOf(request.getAttribute(Constants.CURRENT_PAGE_NO).toString());
                            }
                            if (request.getAttribute(Constants.STARTING_RECORD_NO) != null) {
                                STARTING_RECORD_NO = Integer.valueOf(request.getAttribute(Constants.STARTING_RECORD_NO).toString());
                            }
                            if (request.getAttribute(Constants.ENDING_RECORD_NO) != null) {
                                ENDING_RECORD_NO = Integer.valueOf(request.getAttribute(Constants.ENDING_RECORD_NO).toString());
                            }
                            if (STARTING_RECORD_NO <= 0) {
                                disabledPrev = "disabled";
                                STARTING_RECORD_NO = 0;
                            }
                            if (ENDING_RECORD_NO >= TOTAL_RECORDS) {
                                disabledNext = "disabled";
                                ENDING_RECORD_NO = TOTAL_RECORDS;
                            }
                            if (request.getAttribute("type") != null && request.getAttribute("type").toString().length() > 0) {
                                if (Integer.valueOf(request.getAttribute("type").toString()) == 1) {
                                    classForTab_1 = "active";
                                } else {
                                    classForTab_0 = "active";
                                }
                            } else {
                                classForTab_0 = "active";
                            }
                        %>
                    </div>
                </div>
            </div>
            <div class="page-wrapper-row full-height">
                <div class="page-wrapper-middle">
                    <div class="page-container">
                        <div class="page-content-wrapper">
                            <div class="page-content">
                                <div class="container">
                                    <ul class="page-breadcrumb breadcrumb">
                                        <li>
                                            <a href="welcome.do">Admin Panel</a>
                                            <span style="font-size: 14px; margin: 0 auto;" class="fa fa-angle-right"></span>
                                        </li>
                                        <li>
                                            <a href="javascript:;">Special Users</a>
                                            <span style="font-size: 14px; margin: 0 auto;" class="fa fa-angle-right"></span>
                                        </li>
                                        <li>
                                            <a href="javascript:;">Special User Accounts</a>
                                            <span style="font-size: 14px; margin: 0 auto;" class="fa fa-angle-right"></span>
                                        </li>
                                        <li>
                                            <span id="features" data-class-active=".specialUsers .specialUsersAccounts .addSpecialUsersAccount">Make Special</span>
                                        </li>
                                    </ul>
                                    <logic:messagesPresent property="errormsg" message="false">
                                        <html:messages id="errormsg" message="false">
                                            <logic:present name="errormsg">
                                                <div  class="custom-alerts alert alert-danger fade in">
                                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                    <strong> <bean:write name="errormsg" filter="false" /> </strong>
                                                </div>
                                            </logic:present>
                                        </html:messages>
                                    </logic:messagesPresent>
                                    <div class="page-content-inner">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="tabbable-line boxless tabbable-reversed">
                                                    <ul class="nav nav-tabs">
                                                        <li class="<%=classForTab_0%>">
                                                            <a href="#tab_0" data-toggle="tab"> Featured user </a>
                                                        </li>
                                                        <li class="<%=classForTab_1%>">
                                                            <a href="#tab_1" data-toggle="tab"> Celebrity </a>
                                                        </li>
                                                    </ul>
                                                    <div class="tab-content" style="padding-top: 15px;">
                                                        <div class="tab-pane <%=classForTab_0%>" id="tab_0">
                                                            <div class="portlet box green">
                                                                <div class="portlet-title">
                                                                    <div class="caption">
                                                                        <i class="fa fa-wrench"></i> Featured user </div>
                                                                    <div class="tools">
                                                                        <a href="javascript:;" class="collapse"> </a>
                                                                    </div>
                                                                </div>
                                                                <div class="portlet-body form">
                                                                    <html:form action="/makeCelebrity" method="POST" acceptCharset="UTF-8" styleClass="form-horizontal" >
                                                                        <input type="hidden" value="2" name="type" />
                                                                        <div class="form-body">
                                                                            <div class="form-group">
                                                                                <label class="col-md-3 control-label">RingId <span class="required"> * </span> </label>
                                                                                <div class="col-md-4">
                                                                                    <html:text property="userId" title="ringId" styleClass="form-control" />
                                                                                    <html:messages id="userId" property="userId">
                                                                                        <span style="color: red;"> RingID Required </span>
                                                                                    </html:messages>
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label class="col-md-3 control-label">Weight <span class="required"> * </span> </label>
                                                                                <div class="col-md-4">
                                                                                    <html:text property="weight" title="weight" styleClass="form-control" />
                                                                                    <html:messages id="weight" property="weight">
                                                                                        <span style="color: red;"> Weight Required </span>
                                                                                    </html:messages>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-actions">
                                                                            <div class="row">
                                                                                <div class="col-md-offset-3 col-md-9">
                                                                                    <button type="submit" class="btn green" name="saveButton">Submit</button>
                                                                                    <a href="showCelebrityList.do" class="btn default" >Cancel</a>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </html:form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="tab-pane <%=classForTab_1%>" id="tab_1">
                                                            <div class="portlet box green">
                                                                <div class="portlet-title">
                                                                    <div class="caption">
                                                                        <i class="fa fa-wrench"></i> Celebrity </div>
                                                                    <div class="tools">
                                                                        <a href="javascript:;" class="collapse"> </a>
                                                                    </div>
                                                                </div>
                                                                <div class="portlet-body form">
                                                                    <html:form action="/makeCelebrity" method="POST" acceptCharset="UTF-8" styleClass="form-horizontal" >
                                                                        <input type="hidden" value="1" name="type" />
                                                                        <div class="form-body">
                                                                            <div class="form-group">
                                                                                <div class="col-md-6 col-sm-6">
                                                                                    <label>
                                                                                        <html:select styleClass="bs-select form-control input-sm input-xsmall input-inline" property="recordPerPage" onchange="javascript: submitform(1, 1, this);">
                                                                                            <html:option value="10">10</html:option>
                                                                                            <html:option value="25">25</html:option>
                                                                                            <html:option value="50">50</html:option>
                                                                                            <html:option value="100">100</html:option>
                                                                                            <html:option value="200">200</html:option>
                                                                                        </html:select>
                                                                                        records</label>
                                                                                </div>
                                                                                <div class="col-md-6 col-sm-6">
                                                                                    <div class="input-group" style="float: right;">
                                                                                        <input name="ringId" type="search" class="form-control input-sm input-small" placeholder="type ringId" value="<%=ringIdField%>"/>
                                                                                        <span>
                                                                                            <button class="btn green-soft uppercase bold input-sm" type="submit"><i class="icon-magnifier"></i></button>
                                                                                            <input name="action" type="hidden" value="<%=Constants.SEARCH%>" />
                                                                                        </span>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="table-scrollable">
                                                                                <table class="table table-striped table-bordered table-hover">
                                                                                    <thead>
                                                                                        <tr>
                                                                                            <th class="text-center">
                                                                                                <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                                                                                    <input type="checkbox" onchange="checkAll(this)" class="group-checkable" />
                                                                                                    <span></span>
                                                                                                </label>
                                                                                            </th>
                                                                                            <th class="text-center">Page Name</th>
                                                                                            <th class="text-center">Profile Image</th>
                                                                                            <th class="text-center">Edit</th>
                                                                                        </tr>
                                                                                    </thead>
                                                                                    <tbody>
                                                                                        <%
                                                                                            ArrayList<ServiceDTO> data1 = (ArrayList<ServiceDTO>) session.getAttribute(Constants.DATA_ROWS);
                                                                                            session.removeAttribute(Constants.DATA_ROWS);
                                                                                            if (data1 != null && data1.size() > 0) {
                                                                                                int start = 0;
                                                                                                int end = data1.size();
                                                                                                for (int i = start; i < end; i++) {
                                                                                                    ServiceDTO dto = (ServiceDTO) data1.get(i);
                                                                                        %>
                                                                                        <tr>
                                                                                            <td class="text-center">
                                                                                                <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                                                                                    <input name="selectedIDs" type="checkbox" class="checkboxes" value="<%=String.valueOf(dto.getPageId())%>" />
                                                                                                    <span></span>
                                                                                                </label>
                                                                                            </td>
                                                                                            <td class="text-center"><%=dto.getServiceName()%></td>
                                                                                            <td class="text-center">
                                                                                                <img style="max-height: 200px;max-width: 200px" src="<%=Constants.IMAGE_BASE_URL%><%=dto.getProfileImage()%>"/>
                                                                                            </td>
                                                                                            <td class="text-center">
                                                                                                <a href="editNewsPortal.do?id=<%=dto.getPageId() + "&action=" + Constants.EDIT%>" class="" title="Change"><span class="fa fa-edit"></span></a>
                                                                                            </td>
                                                                                            <% } %>        
                                                                                        </tr>
                                                                                        <%
                                                                                            }
                                                                                        %>
                                                                                    </tbody>
                                                                                </table>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-actions">
                                                                            <div class="row">
                                                                                <div class="col-md-offset-3 col-md-9">
                                                                                    <button type="submit" class="btn green" name="submitType" value="Make Celebrity">Make Celebrity</button>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </html:form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <%@include file="/shared/footer.jsp"%>
        </div>
        <%@include file="/shared/script-list.jsp"%>
    </body>
</html>
