<%-- 
    Document   : ringStudioTable
    Created on : Apr 3, 2017, 11:27:58 AM
    Author     : ipvision
--%>

<%@page import="com.ringid.admin.service.RingStudioService"%>
<%@page import="com.ringid.admin.dto.RingStudioDTO"%>
<%@page import="java.util.List"%>
<%@page import="com.ringid.admin.utils.Constants"%>
<div class="form-body">
    <div class="form-group" id="device">
        <label class="col-md-3 control-label"> Device </label>
        <div class="col-md-4">
            <html:select property="device" styleClass="form-control"> 
                <html:option  value="1">Desktop</html:option>     
                <html:option  value="2">Android</html:option>  
                <html:option  value="3">iOS</html:option>
                <html:option  value="4">Windows Phone</html:option>
                <html:option  value="5">Web</html:option>
            </html:select>
        </div>
    </div>
    <div class="form-group" id="apptype">
        <label class="col-md-3 control-label"> App Type </label>
        <div class="col-md-4">
            <html:select property="appType" styleClass="form-control">
                <%
                    for (int i = 1; i < Constants.APP_TYPE.length; i++) {
                %>
                <html:option  value='<%=String.valueOf(i)%>'><%=Constants.APP_TYPE[i]%></html:option>
                    <%}%>
            </html:select>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label"> Country </label>
        <div class="col-md-4">
            <%
                List<com.ringid.country.CountryDTO> countryDTOs = com.ringid.country.CountryLoader.getInstance().getCountryList();
            %>
            <html:select property="countryCode" styleClass="form-control" onchange="setImage(this)" styleId="country"> 
                <html:option  value="*"> Global </html:option>     
                    <%
                        for (com.ringid.country.CountryDTO dto : countryDTOs) {
                    %>
                <html:option value="<%=dto.getDialingCode()%>"><%=dto.getName()%> (<%=dto.getDialingCode()%>) </html:option>
                    <% }%>  
            </html:select>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label"> Image </label>
        <div class="col-md-4">
            <%
                List<RingStudioDTO> ringStudioDTOs = RingStudioService.getInstance().getImageAndCountryCodeList();
            %>
            <html:select property="imageName" styleClass="form-control" styleId="images"> 
                <html:option  value=""> Select Image </html:option>
            </html:select>
            <html:messages id="imageName" property="imageName">
                <bean:write name="imageName"  filter="false"/>
            </html:messages>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label"> Title </label>
        <div class="col-md-4">
            <html:text property="title" title="Title" styleClass="form-control" />
            <html:messages id="title" property="title">
                <bean:write name="title"  filter="false"/>
            </html:messages>

        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label"> URL </label>
        <div class="col-md-4">
            <html:text property="url" title="url" styleClass="form-control" />
            <html:messages id="url" property="url">
                <bean:write name="url"  filter="false"/>
            </html:messages>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label"> Supported Countries </label>
        <div class="col-md-4">
            <html:select styleId="my_multi_select1" property="supportedCountries" styleClass="form-control multi-select" onchange="setImage(this)" multiple="multiple"> 
                <html:option  value="*"> Global </html:option>     
                    <%
                        for (com.ringid.country.CountryDTO dto : countryDTOs) {
                    %>
                <html:option value="<%=dto.getDialingCode()%>"><%=dto.getName()%> (<%=dto.getDialingCode()%>) </html:option>
                    <% }%>  
            </html:select>
        </div>
    </div>
    <!--    <div class="form-group" id="imgname">
            <label class="col-md-3 control-label"> Image Name </label>
            <div class="col-md-4">
                <html:text property="imageName" title="Image Name" styleClass="form-control" />
                <html:messages id="imageName" property="imageName">
                    <bean:write name="imageName"  filter="false"/>
                </html:messages>
            </div>
        </div>-->
    <div class="form-group">
        <label class="col-md-3 control-label"> Display Title </label>
        <div class="col-md-4">
            <html:select property="displayTitle" styleClass="form-control">
                <html:option  value="0">NO</html:option>
                <html:option  value="1">YES</html:option>
            </html:select>
            <html:messages id="displayTitle" property="displayTitle">
                <bean:write name="displayTitle"  filter="false"/>
            </html:messages>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label"> Display Floating Menu </label>
        <div class="col-md-4">
            <html:select property="displayFloatingMenu" styleClass="form-control">
                <html:option  value="0">NO</html:option>
                <html:option  value="1">YES</html:option>
            </html:select>
            <html:messages id="displayFloatingMenu" property="displayFloatingMenu">
                <bean:write name="displayFloatingMenu"  filter="false"/>
            </html:messages>
        </div>
    </div>
    <div class="form-group last">
        <label class="col-md-3 control-label"></label>
        <div class="col-md-4">
            <%                if (request.getSession(true).getAttribute("error") == "2") {
            %>
            <html:messages id="message" property="message" ><bean:write name="msg"  filter="false"/></html:messages>
            <bean:write name="RingStudioForm" property="message" filter="false"/>
            <%}%>
        </div>
    </div>
</div>
<div class="form-actions">
    <div class="row">
        <div class="col-md-offset-3 col-md-4">
            <button type="submit" class="btn green" name="saveButton">Submit</button>
            <a href="ringStudioListInfo.do" class="btn default">Cancel</a>
            <html:hidden property="id"/>
            <html:hidden property="action" styleId="actionid" />
        </div>
    </div>
</div>
<script type="text/javascript">
    var ringStudioDTOs = <%=ringStudioDTOs%>;
    function setImage(country) {
        var dialingCode = country.options[country.selectedIndex].value;
        $("#images").empty();
        $('#images').append($('<option/>', {
            value: "",
            text: 'Select Image'
        }));
        for (var i = 0; i < ringStudioDTOs.length; i++) {
            if (ringStudioDTOs[i].countryCode == dialingCode || ringStudioDTOs[i].countryCode == "*") {
                $('#images').append($('<option/>', {
                    value: ringStudioDTOs[i].imageName,
                    text: ringStudioDTOs[i].imageName
                }));
            }
        }
    }

    var action = document.getElementById("actionid"), dialingCode = document.getElementById("country"), dcValue, select = document.getElementById("images"), imageName = '<bean:write name="RingStudioForm" property="imageName"/>';
    dcValue = dialingCode.options[dialingCode.selectedIndex].value;
    if (action.value == "0") {
        action.value = "<%=Constants.ADD%>";
    } else if (action.value == "<%=Constants.UPDATE%>") {
        document.getElementById("device").style.display = "none";
        document.getElementById("apptype").style.display = "none";
    }
    for (var i = 0; i < ringStudioDTOs.length; i++) {
        if (ringStudioDTOs[i].countryCode == dcValue || ringStudioDTOs[i].countryCode == "*") {
            var opt = document.createElement('option');
            opt.value = ringStudioDTOs[i].imageName;
            opt.innerHTML = ringStudioDTOs[i].imageName;
            if (imageName == ringStudioDTOs[i].imageName) {
                opt.selected = true;
            }
            select.appendChild(opt);
        }
    }

</script>
