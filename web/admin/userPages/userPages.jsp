<%-- 
    Document   : userPages
    Created on : Aug 28, 2017, 12:58:41 PM
    Author     : mamun
--%>

<%@page import="org.ringid.users.DeactiveReasonDTO"%>
<%@page import="com.ringid.admin.dto.UserPageDTO"%>
<%@page import="com.ringid.admin.projectMenu.MenuNames"%>
<%@include file="/login/loginTimeExpiry.jsp"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ page import="java.util.*,com.ringid.admin.utils.Constants"%>
<%@include  file="/shared/head.jsp" %>

<!DOCTYPE html>
<html>
    <head>
        <title>Admin Panel | User Pages  </title>
        <%@include file="/shared/css-list.jsp"%>
    </head>
    <body class="page-container-bg-solid">
        <div class="page-wrapper">
            <div class="page-wrapper-row">
                <div class="page-wrapper-top">
                    <div class="page-header">
                        <%@include  file="/shared/menu.jsp" %>
                        <%
                            String FEATURE_NAME = MenuNames.USER_PAGE;
                            String msgStr = "";
                            if (request.getAttribute("message") != null) {
                                msgStr = request.getAttribute("message").toString();
                            }
                            int TOTAL_RECORDS = 0, TOTAL_PAGES = 0, CURRENT_PAGE_NO = 0, STARTING_RECORD_NO = 0, ENDING_RECORD_NO = 0;
                            String disabledPrev = "", disabledNext = "";
                            if (request.getAttribute(Constants.TOTAL_RECORDS) != null) {
                                TOTAL_RECORDS = Integer.valueOf(request.getAttribute(Constants.TOTAL_RECORDS).toString());
                            }
                            if (request.getAttribute(Constants.TOTAL_PAGES) != null) {
                                TOTAL_PAGES = Integer.valueOf(request.getAttribute(Constants.TOTAL_PAGES).toString());
                            }
                            if (request.getAttribute(Constants.CURRENT_PAGE_NO) != null) {
                                CURRENT_PAGE_NO = Integer.valueOf(request.getAttribute(Constants.CURRENT_PAGE_NO).toString());
                            }
                            if (request.getAttribute(Constants.STARTING_RECORD_NO) != null) {
                                STARTING_RECORD_NO = Integer.valueOf(request.getAttribute(Constants.STARTING_RECORD_NO).toString());
                            }
                            if (request.getAttribute(Constants.ENDING_RECORD_NO) != null) {
                                ENDING_RECORD_NO = Integer.valueOf(request.getAttribute(Constants.ENDING_RECORD_NO).toString());
                            }
                            if (STARTING_RECORD_NO <= 0) {
                                disabledPrev = "disabled";
                                STARTING_RECORD_NO = 0;
                            }
                            if (ENDING_RECORD_NO >= TOTAL_RECORDS) {
                                disabledNext = "disabled";
                                ENDING_RECORD_NO = TOTAL_RECORDS;
                            }
                        %>
                    </div>
                </div>
            </div>
            <div class="page-wrapper-row full-height">
                <div class="page-wrapper-middle">
                    <div class="page-container">
                        <div class="page-content-wrapper">
                            <div class="page-content">
                                <div class="container">
                                    <ul class="page-breadcrumb breadcrumb">
                                        <li>
                                            <a href="welcome.do">Admin Panel</a>
                                            <span style="font-size: 14px; margin: 0 auto;" class="fa fa-angle-right"></span>
                                        </li>
                                        <li>
                                            <a href="javascript:;"> Page & Media </a>
                                            <span style="font-size: 14px; margin: 0 auto;" class="fa fa-angle-right"></span>
                                        </li>
                                        <li>
                                            <a href="/admin/userPages/userPages.jsp"> User Pages </a>
                                            <span style="font-size: 14px; margin: 0 auto;" class="fa fa-angle-right"></span>
                                        </li>
                                        <li>
                                            <span id="features" data-class-active=".page-o-media .userPages .manageUserPages"> Manage User Pages </span>
                                        </li>
                                    </ul>
                                    <% if (msgStr.length() > 0) {%>
                                    <div  class="custom-alerts alert alert-danger fade in">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                        <strong><%=msgStr%></strong>
                                    </div>
                                    <% }%>
                                    <div class="page-content-inner">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="tab-content" style="padding-top: 15px;">
                                                    <div class="tabbable-line boxless tabbable-reversed">
                                                        <div class="portlet box green">
                                                            <div class="portlet-title">
                                                                <div class="caption">
                                                                    <i class="fa fa-list-ul"></i>User Pages Info
                                                                </div>
                                                                <div class="tools">
                                                                    <a href="javascript:;" class="collapse"> </a>

                                                                    <a data-original-title="Reload" href="showUserPages.do" style="width: 13px; background-image: url(assets/global/img/portlet-reload-icon-white.png);" class="tooltips"> </a>
                                                                    <%if ((loginDTO.getPermissionLevel() == Constants.SUPER_ADMIN) || (loginDTO.getFeauterMapList().get(FEATURE_NAME).getHasAddPermission())) {%>
                                                                    <a data-original-title="AddNew" style="color: white; width: 16px; background-image: url(assets/global/img/add-1.png);" href="admin/userPages/userPageUI.jsp" class="tooltips"> </a>
                                                                    <% }%>
                                                                </div>
                                                            </div>
                                                            <div class="portlet-body form">
                                                                <html:form action="/showUserPages" method="POST" acceptCharset="UTF-8" styleClass="form-horizontal" >
                                                                    <div class="form-body">
                                                                        <div class="form-group">
                                                                            <div class="col-md-12 col-sm-12">
                                                                                <label>
                                                                                    <html:select property="pageType" styleClass="form-control input-inline"> 
                                                                                        <html:option  value="0">--Page Type--</html:option>     
                                                                                        <html:option  value="3">User Page</html:option>  
                                                                                        <html:option  value="5">Donation Page</html:option>
                                                                                        <html:option  value="10">Celebrity Page</html:option>
                                                                                    </html:select>
                                                                                </label>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <div class="col-md-6 col-sm-6">
                                                                                <label>
                                                                                    <html:select styleClass="bs-select form-control input-sm input-xsmall input-inline" property="recordPerPage" onchange="javascript: submitform(1, 1, this);">
                                                                                        <html:option value="10">10</html:option>
                                                                                        <html:option value="25">25</html:option>
                                                                                        <html:option value="50">50</html:option>
                                                                                        <html:option value="100">100</html:option>
                                                                                        <html:option value="200">200</html:option>
                                                                                    </html:select>
                                                                                    records</label>
                                                                            </div>
                                                                            <div class="col-md-6 col-sm-6">
                                                                                <div class="input-group" style="float: right;">
                                                                                    <input name="id" type="text" value="<bean:write name="UserPageForm" property="id"/>" class="form-control input-sm input-small" placeholder="ringID or UserId">
                                                                                    <span>
                                                                                        <button class="btn green-soft uppercase bold input-sm" type="submit"><i class="icon-magnifier"></i></button>
                                                                                        <input name="action" type="hidden" value="<%=Constants.SEARCH%>" />
                                                                                        <html:hidden property="column" />
                                                                                        <html:hidden property="sort" />
                                                                                    </span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="table-scrollable">        
                                                                            <table class="table table-striped table-bordered table-hover">
                                                                                <thead>
                                                                                    <tr>
                                                                                        <th class="text-center align-middle">
                                                                                            <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                                                                                <input type="checkbox" onchange="checkAll(this)" class="group-checkable" />
                                                                                                <span></span>
                                                                                            </label>
                                                                                        </th>
                                                                                        <th class="text-center align-middle">Reason</th>
                                                                                        <th id="col_1" class="text-center align-middle sortable">PageId</th>
                                                                                        <th class="text-center align-middle">Page Name</th>
                                                                                        <th class="text-center align-middle">Page Type</th>
                                                                                        <th class="text-center align-middle">Status</th>
                                                                                        <th class="text-center align-middle">Profile Image</th>
                                                                                        <th class="text-center align-middle">Action</th>
                                                                                    </tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                    <%
                                                                                        ArrayList<UserPageDTO> data1 = (ArrayList<UserPageDTO>) session.getAttribute(Constants.DATA_ROWS);
                                                                                        session.removeAttribute(Constants.DATA_ROWS);
                                                                                        ArrayList<DeactiveReasonDTO> deactiveReasonList = (ArrayList<DeactiveReasonDTO>) session.getAttribute("deactiveReasonList");
                                                                                        session.removeAttribute("deactiveReasonList");
                                                                                        if (data1 != null && data1.size() > 0) {
                                                                                            for (int i = STARTING_RECORD_NO; i < ENDING_RECORD_NO; i++) {
                                                                                                UserPageDTO dto = (UserPageDTO) data1.get(i);
                                                                                    %>
                                                                                    <tr >
                                                                                        <td class="text-center align-middle">
                                                                                            <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                                                                                <input name="deactivateReasons" type="checkbox" class="checkboxes" value="" onclick="checkSelect(this, <%=i%>)" />
                                                                                                <span></span>
                                                                                            </label>
                                                                                        </td>
                                                                                        <td class="text-center align-middle" name="td_deactivateReasons">
                                                                                            <select class="form-control" name="select_deactivateReasons" onchange="setReason(this, <%=i%>)">
                                                                                                <option  value=""> Select Reason </option>  
                                                                                                <%
                                                                                                    for (int k = 0; k < deactiveReasonList.size(); k++) {
                                                                                                        DeactiveReasonDTO deactiveReasonDTO = deactiveReasonList.get(k);
                                                                                                %>
                                                                                                <option  value="<%=String.valueOf(deactiveReasonDTO.getId())%>"><%=deactiveReasonDTO.getDescription()%></option>
                                                                                                <% }%>
                                                                                            </select>
                                                                                        </td>
                                                                                        <td class="text-center align-middle"><span name="pageIds"><%=dto.getPageId()%></span></td>
                                                                                        <td class="text-center align-middle"><%=dto.getPageName()%></td>
                                                                                        <%if (dto.getPageType() == 3) { %>
                                                                                        <td class="text-center align-middle"> USER PAGE </td>
                                                                                        <% } else if (dto.getPageType() == 5) {%>
                                                                                        <td class="text-center align-middle"> DONATION PAGE </td>
                                                                                        <% } else if (dto.getPageType() == 10) {%>
                                                                                        <td class="text-center align-middle"> CELEBRITY </td>
                                                                                        <% } else {%>
                                                                                        <td class="text-center align-middle">  </td>
                                                                                        <% }%>
                                                                                        <td class="text-center align-middle"> <%=dto.getPageActiveStatus()%> </td>
                                                                                        <td class="text-center align-middle">
                                                                                            <img style="height: 100px; width: 100px" src="<%=Constants.IMAGE_BASE_URL%><%=dto.getProfileImageUrl()%>"/>
                                                                                        </td>
                                                                                        <td class="text-center align-middle">
                                                                                            <a href="editUserPage.do?pageId=<%=dto.getPageId() + "&action=" + Constants.EDIT%>&id=<bean:write name='UserPageForm' property='id'/>&searchByRingId=<bean:write name='UserPageForm' property='searchByRingId'/>" cclass="btn btn-sm green" title="Change"><span class="fa fa-edit"></span></a>

                                                                                            &nbsp;<a href="editPageFollowRules.do?pageId=<%=dto.getPageId()%>&pageType=<%=dto.getPageType()%>" class="btn btn-sm green" title="Change"><span class="fa fa-edit"></span> Page Rules</a>
                                                                                        </td>
                                                                                        <% } %>        
                                                                                    </tr>
                                                                                    <%
                                                                                        }
                                                                                    %>
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                        <div class="row">
                                                                            <div class="col-md-5 col-sm-12">
                                                                                <div>
                                                                                    Showing <%=(STARTING_RECORD_NO + 1)%> to <%=ENDING_RECORD_NO%> of <%=TOTAL_RECORDS%> entries
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-7 col-sm-12">
                                                                                <div style="float: right;">
                                                                                    <div> 
                                                                                        Page <a href="javascript: submitform(2, <%=(CURRENT_PAGE_NO - 1)%>, this);" class="btn btn-sm default prev <%=disabledPrev%>"><i class="fa fa-angle-left"></i></a>
                                                                                        <input name="pageNo" class="form-control input-sm input-inline input-mini" maxlenght="5" style="text-align:center; margin: 0 5px;" type="text" value="<%=CURRENT_PAGE_NO%>" onchange="javascript: submitform(2, 0, this);">
                                                                                        <a href="javascript: submitform(2, <%=(CURRENT_PAGE_NO + 1)%>, this);" class="btn btn-sm default next <%=disabledNext%>"><i class="fa fa-angle-right"></i></a> of <span><%=TOTAL_PAGES%></span>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-actions">
                                                                        <div class="row">
                                                                            <div class="col-md-offset-3 col-md-9">
                                                                                <button type="submit" class="btn green" name="saveButton" onclick="return checkSelectedValue();" formaction="deactiveUserPages.do">Deactive</button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </html:form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <%@include file="/shared/footer.jsp"%>
        </div>
        <%@include file="/shared/script-list.jsp"%>
        <script type="text/javascript">

            var checkboxes = document.getElementsByName('deactivateReasons'), len, i, flag, pageIds = document.getElementsByName('pageIds'), pageIdAndReason,
                    select_deactivateReasons = document.getElementsByName('select_deactivateReasons'), td_deactivateReasons = document.getElementsByName('td_deactivateReasons');
            function checkAll(obj_check_all) {
                flag = obj_check_all.checked;
                for (i = 0, len = checkboxes.length; i < len; i++) {
                    checkboxes[i].checked = flag;
                    if (typeof select_deactivateReasons[i].value === "undefined" || select_deactivateReasons[i].value === "") {
                        td_deactivateReasons[i].classList.add("has-error");
                    } else {
                        td_deactivateReasons[i].classList.remove("has-error");
                    }
                }
            }

            function setReason(obj_select, pos) {
                if (typeof obj_select.value === "undefined" || obj_select.value === "") {
                    td_deactivateReasons[pos].classList.add("has-error");
                } else {
                    pageIdAndReason = {};
                    pageIdAndReason["pageId"] = pageIds[pos].innerText;
                    pageIdAndReason["reason"] = obj_select.value;
                    checkboxes[pos].value = JSON.stringify(pageIdAndReason);
                    td_deactivateReasons[pos].classList.remove("has-error");
                }
            }

            function checkSelect(obj_checkbox, pos) {
                if (obj_checkbox.checked) {
                    if (typeof select_deactivateReasons[pos].value === "undefined" || select_deactivateReasons[pos].value === "") {
                        td_deactivateReasons[pos].classList.add("has-error");
                    } else {
                        td_deactivateReasons[pos].classList.remove("has-error");
                    }
                }
            }

            function checkSelectedValue() {
                flag = true;
                for (i = 0, len = checkboxes.length; i < len; i++) {
                    if (checkboxes[i].checked) {
                        flag = false;
                        if (typeof select_deactivateReasons[i].value === "undefined" || select_deactivateReasons[i].value === "") {
                            alert("please select reason");
                            return false;
                        }
                    }
                }
                if (flag) {
                    alert("Please select  at least one item");
                    return false;
                }
                return true;
            }
        </script>
    </body>
</html>
