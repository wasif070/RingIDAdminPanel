<%-- 
    Document   : userPageTable
    Created on : Aug 28, 2017, 12:59:14 PM
    Author     : mamun
--%>

<%@page import="com.ringid.admin.repository.NewsPortalCategoryLoader"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.ringid.admin.dto.newsportal.NewsPortalCategoryDTO"%>
<%@page import="com.ringid.admin.utils.Constants"%>
<%@page import="java.util.List"%>
<%
    int action = (Integer) request.getSession(true).getAttribute("formType");
%>
<div class="form-body">
    <div class="form-group">
        <label class="col-md-3 control-label">Page Name <span style="color: red">*</span></label>
        <div class="col-md-4">
            <html:text property="pageName" title="Page Name" styleClass="form-control" />
            <html:messages id="name" property="pageName">
                <bean:write name="pageName"  filter="false"/>
            </html:messages>
        </div>
    </div>
    <%if (!(action == Constants.UPDATE)) {%>
    <div class="form-group">
        <label class="col-md-3 control-label">Page Type <span style="color: red">*</span></label>
        <div class="col-md-4">
            <html:select property="pageType" styleClass="form-control" onchange="toggleSportsCat(this)"> 
                <html:option  value="3">User Page</html:option>
                <html:option  value="7">Sports Page</html:option> 
                <html:option  value="30">Product Page</html:option> 
                <html:option  value="20">Music Page</html:option>  
            </html:select>
        </div>
    </div>
    <div class="form-group" style="display: none;" id="sportsCategoryId">
        <%
            NewsPortalCategoryDTO newsPortalDTO = new NewsPortalCategoryDTO();
            newsPortalDTO.setType(7);
            newsPortalDTO.setIsSearchByType(true);
            ArrayList<NewsPortalCategoryDTO> sportsCatList = NewsPortalCategoryLoader.getInstance().getNewsPortalCategoryList(newsPortalDTO);
        %>
        <label class="col-md-3 control-label">Sports Category <span style="color: red">*</span></label>
        <div class="col-md-4">
            <html:select property="pageCategoryId" styleClass="form-control"> 
                <%
                    for (NewsPortalCategoryDTO newsPortalCategoryDTO : sportsCatList) {
                %>
                <html:option value="<%=String.valueOf(newsPortalCategoryDTO.getPageId())%>"><%=newsPortalCategoryDTO.getName()%></html:option>
                    <% }%>  
            </html:select>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label">Owner RingId <span style="color: red">*</span></label>
        <div class="col-md-4">
            <html:text property="ownerId" title="Owner RingId" styleClass="form-control" />
            <html:messages id="name" property="ownerId">
                <bean:write name="ownerId"  filter="false"/>
            </html:messages>
        </div>
    </div>
    <%}%>
    <div class="form-group">
        <%
            List<com.ringid.country.CountryDTO> countryDTOs = com.ringid.country.CountryLoader.getInstance().getCountryList();
        %>
        <label class="col-md-3 control-label">Country <span style="color: red">*</span></label>
        <div class="col-md-4">
            <html:select property="country" styleClass="form-control"> 
                <html:option  value=""> Select Country </html:option>     
                    <%
                        for (com.ringid.country.CountryDTO dto : countryDTOs) {
                    %>
                <html:option value="<%=dto.getName()%>"><%=dto.getName()%></html:option>
                    <% }%>  
            </html:select>
        </div>
    </div>
    <div class="form-group ">
        <label class="control-label col-md-3">
            Profile Image
            <span class="required" aria-required="true"> * </span>
        </label>
        <div class="col-md-9">
            <div class="fileinput fileinput-new" data-provides="fileinput">
                <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px; line-height: 150px;">
                    <img src="<%=Constants.IMAGE_BASE_URL%>${UserPageForm.profileImageUrl}" />
                </div>
                <div>
                    <span class="btn green btn-outline btn-file">
                        <span class="fileinput-new"> Select image </span>
                        <span class="fileinput-exists"> Change image </span>
                        <html:file property="theFileProfileImage" onchange="removeSpanText()" />
                    </span>
                    <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                    <span id="msgSpan" style="display: inline-block;">
                        <html:messages id="theFileProfileImage" property="theFileProfileImage">
                            <bean:write name="theFileProfileImage"  filter="false"/>
                        </html:messages>
                    </span>
                </div>
            </div>
        </div>
    </div>
    <div class="form-group ">
        <label class="control-label col-md-3">Cover Image<span style="color: red">*</span></label>
        <div class="col-md-9">
            <div class="fileinput fileinput-new" data-provides="fileinput">
                <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px; line-height: 150px;">
                    <img src="<%=Constants.IMAGE_BASE_URL%>${UserPageForm.coverImageUrl}" />
                </div>
                <div>
                    <span class="btn green btn-outline btn-file">
                        <span class="fileinput-new"> Select image </span>
                        <span class="fileinput-exists"> Change image </span>
                        <html:file property="theFileCoverImage" onchange="removeSpanText()" />
                    </span>
                    <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                    <span id="msgSpan" style="display: inline-block;">
                        <html:messages id="theFileCoverImage" property="theFileCoverImage">
                            <bean:write name="theFileCoverImage"  filter="false"/>
                        </html:messages>
                    </span>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="form-actions">
    <div class="row">
        <div class="col-md-offset-3 col-md-4">
            <button type="submit" class="btn green" name="saveButton">Submit</button>
            <logic:equal name="UserPageForm" property="pageType" value="7">
                <a href="showSportsPageList.do" class="btn default">Cancel</a>
            </logic:equal>
            <logic:notEqual name="UserPageForm" property="pageType" value="7">
                <a href="showUserPages.do?searchByRingId=<bean:write name='UserPageForm' property='searchByRingId'/>&id=<bean:write name='UserPageForm' property='id'/>&pageType=<bean:write name='UserPageForm' property='pageType'/>" class="btn default">Cancel</a>
            </logic:notEqual>
            <html:hidden property="id"/>
            <html:hidden property="searchByRingId"/>
            <html:hidden property="pageId"/>
            <html:hidden property="pageRingId"/>
            <%if (action == Constants.UPDATE) {%>
            <html:hidden property="ownerId"/>
            <html:hidden property="pageType"/>
            <%}%>
            <input type="hidden" name="action" value="<%=request.getSession(true).getAttribute("formType")%>" />
        </div>
    </div>
</div>
<script type="text/javascript">
    var sportsCategory = document.getElementById("sportsCategoryId");
    function removeSpanText() {
        document.getElementById("msgSpan").innerHTML = "";
    }

    function toggleSportsCat(obj) {
        if (obj.value == '7') {
            sportsCategory.style.display = "block";
        } else {
            sportsCategory.style.display = "none";
        }
    }
</script>
