<%-- 
    Document   : liveInfoSummary
    Created on : Aug 19, 2017, 11:44:26 AM
    Author     : mamun
--%>

<%@page import="java.util.List"%>
<%@include file="/login/loginTimeExpiry.jsp"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@include  file="/shared/head.jsp" %>

<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Admin Panel | Live Info Summary </title>
        <%@include file="/shared/css-list.jsp"%>
    </head>
    <body class="page-container-bg-solid">
        <div class="page-wrapper">
            <div class="page-wrapper-row">
                <div class="page-wrapper-top">
                    <div class="page-header">
                        <%@include  file="/shared/menu.jsp" %>
                        <%
                            String message = "";
                            if (request.getAttribute("message") != null) {
                                message = request.getAttribute("message").toString();
                            }
                        %>
                    </div>
                </div>
            </div>
            <div class="page-wrapper-row full-height">
                <div class="page-wrapper-middle">
                    <div class="page-container">
                        <div class="page-content-wrapper">
                            <div class="page-content">
                                <div class="container">
                                    <ul class="page-breadcrumb breadcrumb">
                                        <li>
                                            <a href="welcome.do">Admin Panel</a>
                                            <span style="font-size: 14px; margin: 0 auto;" class="fa fa-angle-right"></span>
                                        </li>
                                        <li>
                                            <a href="javascript:;"> Live / Channel </a>
                                            <span style="font-size: 14px; margin: 0 auto;" class="fa fa-angle-right"></span>
                                        </li>
                                        <li>
                                            <a href="javascript:;"> Live Info </a>
                                            <span style="font-size: 14px; margin: 0 auto;" class="fa fa-angle-right"></span>
                                        </li>
                                        <li>
                                            <span id="features" data-class-active=".live-o-channel .liveInfo .liveSummary"> Live Summary </span>
                                        </li>
                                    </ul>
                                    <div class="page-content-inner">
                                        <% if (message.length() > 0) {%>
                                        <div  class="custom-alerts alert alert-success fade in">
                                            <button type="button" class="close" data-dismiss="alert"
                                                    aria-hidden="true"></button>
                                            <strong> <%=message%> </strong>
                                        </div>
                                        <% }%>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="tabbable-line boxless tabbable-reversed">
                                                    <div class="tab-content" style="padding-top: 15px;">
                                                        <div class="portlet box green">
                                                            <div class="portlet-title">
                                                                <div class="caption">
                                                                    <i class="fa fa-plus"></i>Add Live Summary
                                                                </div>
                                                                <div class="tools">
                                                                    <a href="javascript:;" class="collapse"> </a>
                                                                </div>
                                                            </div>
                                                            <div class="portlet-body form">
                                                                <html:form action="/liveSummary" styleClass="form-horizontal" method="POST"  acceptCharset="UTF-8">
                                                                    <div class="form-body" >
                                                                        <div class="form-group">
                                                                            <label class="col-md-3 control-label"> Start Date <span class="required">*</span> </label>
                                                                            <div class="col-md-4">
                                                                                <input type="text" value="<bean:write name="LiveSummaryForm" property='startDate' />" name="startDate" class="form-control date-picker" data-date-format="yyyy-mm-dd" size="16" placeholder="yyyy-mm-dd" />
                                                                                <html:messages id="startDate" property="startDate">
                                                                                    <span style="color: red;"> Start Date Required </span>
                                                                                </html:messages>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label class="col-md-3 control-label"> End Date <span class="required">*</span> </label>
                                                                            <div class="col-md-4">
                                                                                <input type="text" value="<bean:write name="LiveSummaryForm" property='endDate' />" name="endDate"  class="form-control date-picker" data-date-format="yyyy-mm-dd" size="16" placeholder="yyyy-mm-dd" />
                                                                                <html:messages id="endDate" property="endDate">
                                                                                    <span style="color: red;"> End Date Required </span>
                                                                                </html:messages>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-actions">
                                                                        <div class="row">
                                                                            <div class="col-md-offset-3 col-md-9">
                                                                                <button type="submit" class="btn green" name="saveButton"> Generate Summary </button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </html:form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <%@include file="/shared/footer.jsp"%>
        </div>
        <%@include file="/shared/script-list.jsp"%>
    </body>
</html>

