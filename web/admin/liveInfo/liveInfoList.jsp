<%-- 
    Document   : liveInfoUI
    Created on : May 6, 2017, 3:26:31 PM
    Author     : ipvision
--%>

<%@page import="com.ringid.admin.dto.live.LiveSummaryDTO"%>
<%@page import="com.ringid.admin.utils.Constants"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@include file="/login/loginTimeExpiry.jsp"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@include  file="/shared/head.jsp" %>

<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Admin Panel | Live Info List Summary By Date </title>
        <%@include file="/shared/css-list.jsp"%>
    </head>
    <body class="page-container-bg-solid">
        <div class="page-wrapper">
            <div class="page-wrapper-row">
                <div class="page-wrapper-top">
                    <div class="page-header">
                        <%@include  file="/shared/menu.jsp" %>
                        <%
                            int TOTAL_RECORDS = 0, TOTAL_PAGES = 0, CURRENT_PAGE_NO = 0, STARTING_RECORD_NO = 0, ENDING_RECORD_NO = 0;
                            String disabledPrev = "", disabledNext = "";
                            if (request.getAttribute(Constants.TOTAL_RECORDS) != null) {
                                TOTAL_RECORDS = Integer.valueOf(request.getAttribute(Constants.TOTAL_RECORDS).toString());
                            }
                            if (request.getAttribute(Constants.TOTAL_PAGES) != null) {
                                TOTAL_PAGES = Integer.valueOf(request.getAttribute(Constants.TOTAL_PAGES).toString());
                            }
                            if (request.getAttribute(Constants.CURRENT_PAGE_NO) != null) {
                                CURRENT_PAGE_NO = Integer.valueOf(request.getAttribute(Constants.CURRENT_PAGE_NO).toString());
                            }
                            if (request.getAttribute(Constants.STARTING_RECORD_NO) != null) {
                                STARTING_RECORD_NO = Integer.valueOf(request.getAttribute(Constants.STARTING_RECORD_NO).toString());
                            }
                            if (request.getAttribute(Constants.ENDING_RECORD_NO) != null) {
                                ENDING_RECORD_NO = Integer.valueOf(request.getAttribute(Constants.ENDING_RECORD_NO).toString());
                            }
                            if (STARTING_RECORD_NO <= 0) {
                                disabledPrev = "disabled";
                                STARTING_RECORD_NO = 0;
                            }
                            if (ENDING_RECORD_NO >= TOTAL_RECORDS) {
                                disabledNext = "disabled";
                                ENDING_RECORD_NO = TOTAL_RECORDS;
                            }
                        %>
                    </div>
                </div>
            </div>
            <div class="page-wrapper-row full-height">
                <div class="page-wrapper-middle">
                    <div class="page-container">
                        <div class="page-content-wrapper">
                            <div class="page-content">
                                <div class="container">
                                    <ul class="page-breadcrumb breadcrumb">
                                        <li>
                                            <a href="welcome.do">Admin Panel</a>
                                            <span style="font-size: 14px; margin: 0 auto;" class="fa fa-angle-right"></span>
                                        </li>
                                        <li>
                                            <a href="javascript:;"> Live / Channel </a>
                                            <span style="font-size: 14px; margin: 0 auto;" class="fa fa-angle-right"></span>
                                        </li>
                                        <li>
                                            <a href="javascript:;"> Live Info </a>
                                            <span style="font-size: 14px; margin: 0 auto;" class="fa fa-angle-right"></span>
                                        </li>
                                        <li>
                                            <span id="features" data-class-active=".live-o-channel .liveInfo"> Summary By Date </span>
                                        </li>
                                    </ul>
                                    <div class="page-content-inner">

                                        <%if (request.getSession(true).getAttribute("dateerror") != null && !request.getSession(true).getAttribute("dateerror").toString().isEmpty()) {%>
                                        <div id="prefix_661213402591" class="custom-alerts alert alert-success fade in">
                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                            <strong> 
                                                <font color="red"><%=request.getSession(true).getAttribute("dateerror")%></font> 
                                                <%request.getSession(true).setAttribute("dateerror", "");%>
                                            </strong>
                                        </div>
                                        <% }%>

                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="tab-content" style="padding-top: 15px;">
                                                    <div class="tabbable-line boxless tabbable-reversed">
                                                        <div class="portlet box green">
                                                            <div class="portlet-title">
                                                                <div class="caption">
                                                                    <i class="fa fa-list-ul"></i>Live Info Summary By Date
                                                                </div>
                                                                <div class="tools">
                                                                    <a href="javascript:;" class="collapse"> </a>
                                                                </div>
                                                            </div>
                                                            <div class="portlet-body form">
                                                                <html:form action="/showLiveInfoList" styleClass="form-horizontal" method="POST"  acceptCharset="UTF-8">
                                                                    <input type="hidden" name="action" value="7" />
                                                                    <div class="form-body" >
                                                                        <div class="form-group">
                                                                            <div class="col-md-12 col-sm-12">
                                                                                <label>
                                                                                    From: <input type="text" name="startDate" value="<bean:write name="LiveInfoForm" property='startDate' />" class="form-control input-sm input-small input-inline date-picker" data-date-format="yyyy-mm-dd" />
                                                                                </label>
                                                                                <label>
                                                                                    To: <input type="text" name="endDate" value="<bean:write name="LiveInfoForm" property='endDate' />"  class="form-control input-sm input-small input-inline date-picker" data-date-format="yyyy-mm-dd" size="16"  />
                                                                                </label>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <div class="col-md-6 col-sm-6">
                                                                                <label>
                                                                                    <html:select styleClass="bs-select form-control input-sm input-xsmall input-inline" property="recordPerPage" onchange="javascript: submitform(1, 1, this);">
                                                                                        <html:option value="10">10</html:option>
                                                                                        <html:option value="25">25</html:option>
                                                                                        <html:option value="50">50</html:option>
                                                                                        <html:option value="100">100</html:option>
                                                                                        <html:option value="200">200</html:option>
                                                                                    </html:select>
                                                                                    records</label>
                                                                            </div>
                                                                            <div class="col-md-6 col-sm-6">
                                                                                <div class="input-group" style="float: right;">
                                                                                    <input name="ringId" type="text" value="<bean:write name="LiveInfoForm" property="ringId"/>" class="form-control input-sm input-small" placeholder="ringId">
                                                                                    <span>
                                                                                        <button class="btn green-soft uppercase bold input-sm" type="submit"><i class="icon-magnifier"></i></button>
                                                                                        <input name="action" type="hidden" value="<%=Constants.SEARCH%>" />
                                                                                    </span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="table-scrollable">
                                                                            <table class="table table-striped table-bordered table-hover">
                                                                                <thead>
                                                                                    <tr>
                                                                                        <th id="col_1" style="text-align: center;">Date</th>
                                                                                        <th id="col_2" style="text-align: center;">RingId</th>
                                                                                        <th id="col_3" style="text-align: center;">Name</th>
                                                                                        <th id="col_4" style="text-align: center;">Total Live Duration</th>
                                                                                        <th id="col_5" style="text-align: center;">No Of Appearance</th>
                                                                                    </tr>
                                                                                </thead>
                                                                                <tbody>

                                                                                    <%
                                                                                        List<LiveSummaryDTO> data1 = (List<LiveSummaryDTO>) session.getAttribute(Constants.DATA_ROWS);
                                                                                        session.removeAttribute(Constants.DATA_ROWS);
                                                                                        if (data1 != null && data1.size() > 0) {
                                                                                            for (int i = 0; i < data1.size(); i++) {
                                                                                                LiveSummaryDTO dto = (LiveSummaryDTO) data1.get(i);
                                                                                    %>
                                                                                    <tr>
                                                                                        <td style="text-align: center;">
                                                                                            <a href="showLiveInfoDetailsList.do?streamDate=<%=dto.getStreamDateStr()%>&ringId=<%=dto.getRingId()%>" >
                                                                                                <%=dto.getStreamDateStr()%>
                                                                                            </a>
                                                                                        </td>
                                                                                        <td style="text-align: center;"><%=String.format("%.0f", Long.valueOf(dto.getRingId()) % Math.pow(10, 8))%></td>
                                                                                        <td style="text-align: center;"><%=dto.getName()%></td>
                                                                                        <td style="text-align: center;"><%=dto.getTotalLiveDurationStr()%></td>
                                                                                        <td style="text-align: center;"><%=dto.getNoOfAppearance()%></td>     
                                                                                    </tr>
                                                                                    <%
                                                                                            }
                                                                                        }
                                                                                    %>
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                        <div class="row">
                                                                            <div class="col-md-5 col-sm-12">
                                                                                <div>
                                                                                    Showing <%=(STARTING_RECORD_NO + 1)%> to <%=ENDING_RECORD_NO%> of <%=TOTAL_RECORDS%> entries
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-7 col-sm-12">
                                                                                <div style="float: right;">
                                                                                    <div> 
                                                                                        Page <a href="javascript: submitform(2, <%=(CURRENT_PAGE_NO - 1)%>, this);" class="btn btn-sm default prev <%=disabledPrev%>"><i class="fa fa-angle-left"></i></a>
                                                                                        <input name="pageNo" class="form-control input-sm input-inline input-mini" maxlenght="5" style="text-align:center; margin: 0 5px;" type="text" value="<%=CURRENT_PAGE_NO%>" onchange="javascript: submitform(2, 0, this);">
                                                                                        <a href="javascript: submitform(2, <%=(CURRENT_PAGE_NO + 1)%>, this);" class="btn btn-sm default next <%=disabledNext%>"><i class="fa fa-angle-right"></i></a> of <span><%=TOTAL_PAGES%></span>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </html:form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <%@include file="/shared/footer.jsp"%>
        </div>
        <%@include file="/shared/script-list.jsp"%>
    </body>
</html>
