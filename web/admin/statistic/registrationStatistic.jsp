<%-- 
    Document   : registrationStatistics
    Created on : Mar 13, 2018, 3:15:31 PM
    Author     : Rabby
--%>

<%@include file="/login/loginTimeExpiry.jsp"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@include  file="/shared/head.jsp" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Admin Panel | Statistic</title>
        <%@include file="/shared/css-list.jsp"%>
    </head>
    <body class="page-container-bg-solid">
        <div class="page-wrapper">
            <div class="page-wrapper-row">
                <div class="page-wrapper-top">
                    <div class="page-header">
                        <%@include  file="/shared/menu.jsp" %>
                    </div>
                </div>
            </div>
            <div class="page-wrapper-row full-height">
                <div class="page-wrapper-middle">
                    <div class="page-container">
                        <div class="page-content-wrapper">
                            <div class="page-content">
                                <div class="container">
                                    <ul class="page-breadcrumb breadcrumb">
                                        <li>
                                            <a href="welcome.do">Admin Panel</a>
                                            <span style="font-size: 14px; margin: 0 auto;" class="fa fa-angle-right"></span>
                                        </li>
                                        <li>
                                            <a href="javascript:;">Common</a>
                                            <i style="font-size: 14px; margin: 0 auto;" class="fa fa-angle-right"></i>
                                        </li>
                                        <li>
                                            <span id="features" data-class-active=".common .statistic">Statistic</span>
                                        </li>
                                    </ul>
                                    <div class="page-content-inner">
                                        <div class="row">
                                            <div class="col-lg-12 col-xs-12 col-sm-12">
                                                <div class="tabbable-line boxless tabbable-reversed">
                                                    <div class="tab-content" style="padding-top: 15px;">
                                                        <div class="portlet light ">
                                                            <div class="portlet-title">
                                                                <div class="caption">
                                                                    <span class="caption-subject bold font-dark">Registration Statistic</span>
                                                                    <span class="caption-helper"></span>
                                                                </div>
                                                                <div class="actions">
                                                                    <a class="btn btn-circle btn-icon-only btn-default" href="javascript:Dashboard.resetRegistrationStatData();" title="Reload">
                                                                        <i class="icon-refresh"></i>
                                                                    </a>
                                                                    <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="#"> </a>
                                                                </div>
                                                            </div>
                                                            <div class="portlet-body">
                                                                <div class="row">
                                                                    <div class="col-md-12 col-sm-12 text-center">
                                                                        <label>
                                                                            From: <input id='startDate' type="text" name="startDate" class="form-control input-sm input-small input-inline date-picker" data-date-format="mm-dd-yyyy" placeholder="mm-dd-yyyy" value=""/>
                                                                        </label>
                                                                        <label>
                                                                            To: <input id='endDate' type="text" name="endDate" class="form-control input-sm input-small input-inline date-picker" data-date-format="mm-dd-yyyy" placeholder="mm-dd-yyyy"  />
                                                                        </label>
                                                                        <button class="btn green-soft uppercase bold input-sm" type="button" onclick="javascript:Dashboard.ajaxCallForRegistrationStat();"><i class="icon-magnifier"></i></button>
                                                                    </div>
                                                                </div>
                                                                <div id="dashboard_registration_stat" class="CSSAnimationChart"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <%@include file="/shared/footer.jsp"%>
        </div>
        <%@include file="/shared/script-list.jsp"%>
        <script type="text/javascript">
            var baseUrl = '<%=resourceURL%>';
        </script>
    </body>
</html>

