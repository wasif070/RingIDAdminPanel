<%-- 
    Document   : deactivateReasonsTable
    Created on : Jul 10, 2017, 6:09:34 PM
    Author     : ipvision
--%>

<div class="form-body">
    <!--    <div class="form-group">
            <label class="col-md-3 control-label">
                ID <span class="required" aria-required="true"> * </span>
            </label>
            <div class="col-md-4">
                <html:text property="reasonId" title="Deactivate Reasons Id" styleClass="form-control" />
                <html:messages id="reasonId" property="reasonId">
                    <span style="color: red;"> ID Required </span>
                </html:messages>
            </div>
        </div>-->
    <div class="form-group">
        <label class="col-md-3 control-label">
            Description <span class="required" aria-required="true"> * </span>
        </label>
        <div class="col-md-4">
            <html:text property="description" title="description" styleClass="form-control" />
            <html:messages id="description" property="description">
                <span style="color: red;"> Description Required </span>
            </html:messages>
        </div>
    </div>
    <div class="form-group last">
        <label class="col-md-3 control-label"></label>
        <div class="col-md-4">
            <%
                if (request.getSession(true).getAttribute("error") == "2") {
            %>
            <html:messages id="message" property="message" ><bean:write name="msg"  filter="false"/></html:messages>
            <bean:write name="AuthServerForm" property="message" filter="false"/>
            <%}%>
        </div>
    </div>
</div>
<div class="form-actions">
    <div class="row">
        <div class="col-md-offset-3 col-md-4">
            <button type="submit" class="btn green" name="saveButton">Submit</button>
            <a href="deactivateReasonsListInfo.do" class="btn default">Cancel</a>
            <input type="hidden" name="action" value="<%=request.getSession(true).getAttribute("formType")%>" />
        </div>
    </div>
</div>
