<%-- 
    Document   : liveRoomTable
    Created on : Apr 6, 2017, 3:59:02 PM
    Author     : ipvision
--%>

<%@page import="com.ringid.admin.utils.Constants"%>

<div class="form-body">
    <div class="form-group">
        <label class="col-md-3 control-label"> 
            Room Name
            <span class="required" aria-required="true"> * </span>
        </label>
        <div class="col-md-4">
            <html:text property="name" title="Room Name" styleClass="form-control" />
            <html:messages id="name" property="name">
                <bean:write name="name"  filter="false"/>
            </html:messages>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label"> 
            Description
        </label>
        <div class="col-md-4">
            <html:text property="description" title="Description" styleClass="form-control" />
            <html:messages id="description" property="description">
                <bean:write name="description"  filter="false"/>
            </html:messages>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label">PageId </label>
        <div class="col-md-4">
            <html:text property="pageId" title="pageId" styleClass="form-control" />
            <html:messages id="pageId" property="pageId">
                <bean:write name="pageId"  filter="false"/>
            </html:messages>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label">Page Profile Type </label>
        <div class="col-md-4">
            <html:text property="pageProfileType" title="pageProfileType" styleClass="form-control" />
            <html:messages id="pageProfileType" property="pageProfileType">
                <bean:write name="pageProfileType"  filter="false"/>
            </html:messages>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label">  Voting Start Time <span class="required" aria-required="true"> * </span> </label>
        <div class="col-md-4">
            <input type="text" size="16" name="votingStartTime" placeholder="yyyy-mm-dd HH:mm" class="form-control date form_datetime" title="voting time" value="<bean:write name='SpecialRoomForm' property='votingStartTime' />" />
            <html:messages id="votingStartTime" property="votingStartTime">
                <bean:write name="votingStartTime"  filter="false"/>
            </html:messages>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label">  Voting End Time <span class="required" aria-required="true"> * </span> </label>
        <div class="col-md-4">
            <input type="text" size="16" name="votingEndTime" placeholder="yyyy-mm-dd HH:mm" class="form-control date form_datetime" title="voting time" value="<bean:write name='SpecialRoomForm' property='votingEndTime' />" />
            <html:messages id="votingEndTime" property="votingEndTime">
                <bean:write name="votingEndTime"  filter="false"/>
            </html:messages>
        </div>
    </div>
    <div class="form-group ">
        <label class="control-label col-md-3">
            Banner Image
            <span class="required" aria-required="true"> * </span>
        </label>
        <div class="col-md-9">
            <div class="fileinput fileinput-new" data-provides="fileinput">
                <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px; line-height: 150px;">
                    <img src="<%=Constants.IMAGE_BASE_URL%>${SpecialRoomForm.banner}" />
                </div>
                <div>
                    <span class="btn green btn-outline btn-file">
                        <span class="fileinput-new"> Select image </span>
                        <span class="fileinput-exists"> Change image </span>
                        <html:file property="theFile" onchange="removeSpanText()" />
                    </span>
                    <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                    <span id="msgSpan" style="display: inline-block;">
                        <html:messages id="theFile" property="theFile">
                            <bean:write name="theFile"  filter="false"/>
                        </html:messages>
                    </span>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="form-group">
    <label class="col-md-3 control-label"> Visible </label>
    <div class="col-md-4" style="margin-top: 6px;">
        <html:radio property="visible" value="1" styleClass="mt-radio mt-radio-outline" style="margin-right: 3px;">True</html:radio>
        <html:radio property="visible" value="0" styleClass="mt-radio mt-radio-outline" style="margin-right: 3px; margin-left: 20px;">False</html:radio>
    </div>
</div>
<div class="form-actions">
    <div class="row">
        <div class="col-md-offset-3 col-md-4">
            <button type="submit" class="btn green" name="saveButton">Submit</button>
            <a href="showSpecialRoomList.do" class="btn default">Cancel</a>
            <html:hidden property="roomId"/>
            <html:hidden property="banner"/>
            <input type="hidden" name="action" value="<%=request.getSession(true).getAttribute("formType")%>" />
        </div>
    </div>
</div>
<script type="text/javascript">
    function removeSpanText() {
        document.getElementById("msgSpan").innerHTML = "";
    }
</script>
