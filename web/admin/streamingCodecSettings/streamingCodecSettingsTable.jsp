<%-- 
    Document   : streamingCodecSettingsTable
    Created on : Sep 19, 2017, 3:19:01 PM
    Author     : mamun
--%>

<div class="form-body">
    <div class="form-group">
        <label class="col-md-3 control-label">Streaming Type <span style="color: red">*</span></label>
        <div class="col-md-4">
            <html:select property="streamType" styleClass="form-control"> 
                <html:option  value="1">AUDIO STREAM</html:option>     
                <html:option  value="2">VIDEO STREAM </html:option>  
                <html:option  value="3">CHANNEL STREAM</html:option>
                <html:option  value="4">TV CHANNEL STREAM</html:option>
            </html:select>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label">Audio Codec <span style="color: red">*</span></label>
        <div class="col-md-4">
            <html:select property="audioCodec" styleClass="form-control"> 
                <html:option  value="1">PCM</html:option>     
                <html:option  value="2">OPUS</html:option>  
                <html:option  value="3">AAC</html:option>
            </html:select>
        </div>
    </div>
</div>
<div class="form-actions">
    <div class="row">
        <div class="col-md-offset-3 col-md-4">
            <button type="submit" class="btn green" name="saveButton">Submit</button>
            <a href="streamingCodecSettingsList.do" class="btn default">Cancel</a>
            <input type="hidden" name="action" value="<%=request.getSession(true).getAttribute("formType")%>" />
        </div>
    </div>
</div>
