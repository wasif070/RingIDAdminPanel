<%-- 
    Document   : liveRoomEditList
    Created on : Apr 25, 2017, 7:57:09 PM
    Author     : user
--%>

<%-- 
    Document   : liveRoomList
    Created on : Apr 6, 2017, 3:58:29 PM
    Author     : ipvision
--%>
<%@page import="org.ringid.livestream.StreamRoomDTO"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@include file="/login/loginTimeExpiry.jsp"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@include  file="/shared/head.jsp" %>
<%    final int featureIndex = 16;
%>

<!DOCTYPE html>
<html>
    <head>
        <title>Admin Panel | Edit Live Rooms List</title>
        <%@include file="/shared/css-list.jsp"%>
    </head>
    <body class="page-container-bg-solid">
        <div class="page-wrapper">
            <div class="page-wrapper-row">
                <div class="page-wrapper-top">
                    <div class="page-header">
                        <%@include  file="/shared/menu.jsp" %>
                    </div>
                </div>
            </div>
            <div class="page-wrapper-row full-height">
                <div class="page-wrapper-middle">
                    <div class="page-container">
                        <div class="page-content-wrapper">
                            <div class="page-content">
                                <div class="container">
                                    <ul class="page-breadcrumb breadcrumb">
                                        <li>
                                            <a href="welcome.do">Admin Panel</a>
                                            <span style="font-size: 14px; margin: 0 auto;" class="fa fa-angle-right"></span>
                                        </li>
                                        <li>
                                            <a href="javascript:;"> Live / Channel </a>
                                            <span style="font-size: 14px; margin: 0 auto;" class="fa fa-angle-right"></span>
                                        </li>
                                        <li>
                                            <a href="showLiveRoomList.do"> Live Room </a>
                                            <span style="font-size: 14px; margin: 0 auto;" class="fa fa-angle-right"></span>
                                        </li>
                                        <li>
                                            <span id="features" data-class-active=".live-o-channel .liveRoom"> Edit Live Rooms List </span>
                                        </li>
                                    </ul>
                                    <div class="page-content-inner">
                                        <logic:messagesPresent message="false">
                                            <html:messages id="roomNameError" message="false">
                                                <logic:present name="roomNameError">
                                                    <div id="prefix_661213402591" class="custom-alerts alert alert-success fade in">
                                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                        <strong><bean:write name="roomNameError" filter="false" /></strong>
                                                    </div>
                                                </logic:present>
                                            </html:messages>
                                        </logic:messagesPresent>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="tabbable-line boxless tabbable-reversed">
                                                    <div class="tab-content" style="padding-top: 15px;">
                                                        <div class="portlet box green">
                                                            <div class="portlet-title">
                                                                <div class="caption">
                                                                    <i class="fa fa-pencil"></i>Edit Live Rooms List 
                                                                </div>
                                                                <div class="tools">
                                                                    <a href="javascript:;" class="collapse"> </a>
                                                                </div>
                                                            </div>
                                                            <div class="portlet-body form">
                                                                <html:form action="/saveEditLiveRoomList" styleClass="form-horizontal" method="POST"  acceptCharset="UTF-8">
                                                                    <div class="form-body" >
                                                                        <div class="table-scrollable">
                                                                            <table class="table table-striped table-bordered table-hover">
                                                                                <thead>
                                                                                    <tr>
                                                                                        <th style="text-align: center;"> Room ID </th>
                                                                                        <th style="text-align: center;"> 
                                                                                            Room Name 
                                                                                            <label class="control-label"> 
                                                                                                <span class="required" aria-required="true"> * </span>
                                                                                            </label> 
                                                                                        </th>
                                                                                        <th style="text-align: center;"> Weight </th>
                                                                                    </tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                    <logic:notEmpty name ="LiveRoomForm" property="editedRoomsList">
                                                                                        <logic:iterate property="editedRoomsList" id="editedRoomsList" name="LiveRoomForm">
                                                                                            <tr>
                                                                                                <html:hidden name ="editedRoomsList" property="roomId" indexed="true"/>
                                                                                                <html:hidden name ="editedRoomsList" property="description" indexed="true"/>
                                                                                                <html:hidden name ="editedRoomsList" property="banner" indexed="true"/>
                                                                                                <html:hidden name ="editedRoomsList" property="liveUserCount" indexed="true"/>
                                                                                                <td style="text-align: center;"> <bean:write name="editedRoomsList" property="roomId" /> </td>
                                                                                                <td style="text-align: center;"> <html:text name="editedRoomsList" property="roomName" styleClass="form-control" indexed="true" /> </td>
                                                                                                <td style="text-align: center;"> <html:text name="editedRoomsList" property="weight" styleClass="form-control" indexed="true" /> </td>
                                                                                            </tr>
                                                                                        </logic:iterate>
                                                                                    </logic:notEmpty>
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-actions">
                                                                        <div class="row">
                                                                            <div class="col-md-offset-5 col-md-7">
                                                                                <button id="btnSaveList" type="submit" class="btn green" style="width: 200px;">Save</button>
                                                                            </div>
                                                                        </div>
                                                                    </div>        
                                                                </html:form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <%@include file="/shared/footer.jsp"%>
        </div>
        <%@include file="/shared/script-list.jsp"%>
    </body>
</html>

