<%-- 
    Document   : liveRoomTable
    Created on : Apr 6, 2017, 3:59:02 PM
    Author     : ipvision
--%>


<%@page import="com.ringid.admin.utils.Constants"%>
<div class="form-body">
    <div class="form-group">
        <label class="col-md-3 control-label"> 
            Room Name
            <span class="required" aria-required="true"> * </span>
        </label>
        <div class="col-md-4">
            <html:text property="roomName" title="Room Name" styleClass="form-control" />
            <html:messages id="roomName" property="roomName">
                <bean:write name="roomName"  filter="false"/>
            </html:messages>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label"> 
            Description
            <!--<span class="required" aria-required="true"> * </span>-->
        </label>
        <div class="col-md-4">
            <html:text property="description" title="Description" styleClass="form-control" />
            <html:messages id="description" property="description">
                <bean:write name="description"  filter="false"/>
            </html:messages>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label"> 
            Weight
            <!--<span class="required" aria-required="true"> * </span>-->
        </label>
        <div class="col-md-4">
            <html:text property="weight" title="Weight" styleClass="form-control" />
            <html:messages id="weight" property="weight">
                <bean:write name="weight"  filter="false"/>
            </html:messages>
        </div>
    </div>
    <% if (Integer.parseInt(request.getSession(true).getAttribute("formType").toString()) == Constants.ADD) { %>
    <div class="form-group">
        <label class="col-md-3 control-label"> 
            Is Celebrity Page
            <span class="required" aria-required="true"> * </span>
        </label>
        <div class="col-md-4">
            <html:select property="celebrityRoomPage" styleClass="form-control"> 
                <html:option  value="">Select Is Celebrity Page</html:option>     
                <html:option  value="true">Yse</html:option>
                <html:option  value="false">No</html:option>
            </html:select>
            <html:messages id="celebrityRoomPage" property="celebrityRoomPage">
                <bean:write name="celebrityRoomPage"  filter="false"/>
            </html:messages>
        </div>
    </div>
    <% }%>
    <% if (Integer.parseInt(request.getSession(true).getAttribute("formType").toString()) == Constants.UPDATE) {%>
    <div class="form-group">
        <label class="col-md-3 control-label"> 
            Room Type
            <span class="required" aria-required="true"> * </span>
        </label>
        <div class="col-md-4">
            <html:select property="roomType" styleClass="form-control"> 
                <html:option  value="">Normal Room</html:option>     
                <html:option  value="<%=String.valueOf(Constants.LIVE_ROOM_TOP)%>">Top Room</html:option>
                <html:option  value="<%=String.valueOf(Constants.LIVE_ROOM_DONATION)%>">Donation Room</html:option>
            </html:select>
        </div>
    </div>
    <% }%>
    <div class="form-group ">
        <label class="control-label col-md-3">
            Banner Image
            <span class="required" aria-required="true"> * </span>
        </label>
        <div class="col-md-9">
            <div class="fileinput fileinput-new" data-provides="fileinput">
                <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px; line-height: 150px;">
                    <img src="<%=Constants.IMAGE_BASE_URL%>${LiveRoomForm.banner}" />
                </div>
                <div>
                    <span class="btn green btn-outline btn-file">
                        <span class="fileinput-new"> Select image </span>
                        <span class="fileinput-exists"> Change image </span>
                        <html:file property="theFile" onchange="removeSpanText()" />
                    </span>
                    <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                    <span id="msgSpan" style="display: inline-block;">
                        <html:messages id="theFile" property="theFile">
                            <bean:write name="theFile"  filter="false"/>
                        </html:messages>
                    </span>
                </div>
            </div>
        </div>
    </div>
    <div class="form-group last">
        <label class="col-md-3 control-label"></label>
        <div class="col-md-4">
            <%
                if (request.getSession(true).getAttribute("error") == "2") {
            %>
            <html:messages id="message" property="message" ><bean:write name="msg"  filter="false"/></html:messages>
            <bean:write name="LiveRoomForm" property="message" filter="false"/>
            <%}%>
        </div>
    </div>
</div>
<div class="form-actions">
    <div class="row">
        <div class="col-md-offset-3 col-md-4">
            <button type="submit" class="btn green" name="saveButton">Submit</button>
            <a href="showLiveRoomList.do" class="btn default">Cancel</a>
            <html:hidden property="roomId"/>
            <html:hidden property="banner"/>
            <input type="hidden" name="action" value="<%=request.getSession(true).getAttribute("formType")%>" />
        </div>
    </div>
</div>
<script type="text/javascript">
    function removeSpanText() {
        document.getElementById("msgSpan").innerHTML = "";
    }
</script>
