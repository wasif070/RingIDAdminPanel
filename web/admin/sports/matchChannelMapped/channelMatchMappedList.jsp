<%-- 
    Document   : channelMatchMappingList
    Created on : Nov 26, 2017, 12:10:45 PM
    Author     : mamun
--%>

<%@page import="com.ringid.admin.dto.MatchChannelMappedDTO"%>
<%@page import="com.ringid.admin.dto.MatchChannelMappedDTO"%>
<%@page import="org.ringid.sports.cricket.MatchInfoDTO"%>
<%@page import="com.ringid.admin.service.SportsService"%>
<%@page import="com.ringid.admin.projectMenu.MenuNames"%>
<%@include file="/login/loginTimeExpiry.jsp"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="org.ringid.channel.ChannelDTO"%>
<%@page import="java.util.List"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@include  file="/shared/head.jsp" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Admin Panel | Channel Match Mapped </title>
        <%@include file="/shared/css-list.jsp"%>
    </head>
    <body class="page-container-bg-solid">
        <div class="page-wrapper">
            <div class="page-wrapper-row">
                <div class="page-wrapper-top">
                    <div class="page-header">
                        <%@include  file="/shared/menu.jsp" %>
                        <%
                            String FEATURE_NAME = MenuNames.MATCH_CHANNEL_MAPPED;
                            int TOTAL_RECORDS = 0, TOTAL_PAGES = 0, CURRENT_PAGE_NO = 0, STARTING_RECORD_NO = 0, ENDING_RECORD_NO = 0;
                            String disabledPrev = "", disabledNext = "";
                            if (request.getAttribute(Constants.TOTAL_RECORDS) != null) {
                                TOTAL_RECORDS = Integer.valueOf(request.getAttribute(Constants.TOTAL_RECORDS).toString());
                            }
                            if (request.getAttribute(Constants.TOTAL_PAGES) != null) {
                                TOTAL_PAGES = Integer.valueOf(request.getAttribute(Constants.TOTAL_PAGES).toString());
                            }
                            if (request.getAttribute(Constants.CURRENT_PAGE_NO) != null) {
                                CURRENT_PAGE_NO = Integer.valueOf(request.getAttribute(Constants.CURRENT_PAGE_NO).toString());
                            }
                            if (request.getAttribute(Constants.STARTING_RECORD_NO) != null) {
                                STARTING_RECORD_NO = Integer.valueOf(request.getAttribute(Constants.STARTING_RECORD_NO).toString());
                            }
                            if (request.getAttribute(Constants.ENDING_RECORD_NO) != null) {
                                ENDING_RECORD_NO = Integer.valueOf(request.getAttribute(Constants.ENDING_RECORD_NO).toString());
                            }
                            if (STARTING_RECORD_NO <= 0) {
                                disabledPrev = "disabled";
                                STARTING_RECORD_NO = 0;
                            }
                            if (ENDING_RECORD_NO >= TOTAL_RECORDS) {
                                disabledNext = "disabled";
                                ENDING_RECORD_NO = TOTAL_RECORDS;
                            }
                        %>
                    </div>
                </div>
            </div>
            <div class="page-wrapper-row full-height">
                <div class="page-wrapper-middle">
                    <div class="page-container">
                        <div class="page-content-wrapper">
                            <div class="page-content">
                                <div class="container">
                                    <ul class="page-breadcrumb breadcrumb">
                                        <li>
                                            <a href="welcome.do">Admin Panel</a>
                                            <span style="font-size: 14px; margin: 0 auto;" class="fa fa-angle-right"></span>
                                        </li>
                                        <li>
                                            <a href="javascript:;">Common</a>
                                            <i style="font-size: 14px; margin: 0 auto;" class="fa fa-angle-right"></i>
                                        </li>
                                        <li>
                                            <a href="channleMatchMappedInfo.do"> Channel Match Mapped </a>
                                            <span style="font-size: 14px; margin: 0 auto;" class="fa fa-angle-right"></span>
                                        </li>
                                        <li>
                                            <span id="features" data-class-active=".common .sports .matchChannelMapped">Manage Channel Match Mapped </span>
                                        </li>
                                    </ul>
                                    <div class="page-content-inner">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="tabbable-line boxless tabbable-reversed">
                                                    <div class="tab-content" style="padding-top: 15px;">
                                                        <div class="portlet box green">
                                                            <div class="portlet-title">
                                                                <div class="caption">
                                                                    <i class="fa fa-list-ul"></i> Channel Match Mapped Info </div>
                                                                <div class="tools">
                                                                    <a href="javascript:;" class="collapse"> </a>
                                                                    <a data-original-title="Reload" href="channleMatchMappedInfo.do?ForceReload=1" style="width: 13px; background-image: url(assets/global/img/portlet-reload-icon-white.png);" class="tooltips"> </a>
                                                                    <%if ((loginDTO.getPermissionLevel() == Constants.SUPER_ADMIN) || (loginDTO.getFeauterMapList().get(FEATURE_NAME).getHasAddPermission())) {%>
                                                                    <a data-original-title="AddNew" style="color: white; width: 16px; background-image: url(assets/global/img/add-1.png);" href="matchChannelMapping.do" class="tooltips"> </a>
                                                                    <% }%>
                                                                </div>
                                                            </div>
                                                            <div class="portlet-body form">
                                                                <html:form action="/channleMatchMappedInfo" styleClass="form-horizontal" method="POST"  acceptCharset="UTF-8">
                                                                    <div class="form-body" >
                                                                        <div class="form-group">
                                                                            <div class="col-md-2 col-sm-2">
                                                                                <label>
                                                                                    <html:select styleClass="bs-select form-control input-sm input-xsmall input-inline" property="recordPerPage" onchange="javascript: submitform(1, 1, this);">
                                                                                        <html:option value="10">10</html:option>
                                                                                        <html:option value="25">25</html:option>
                                                                                        <html:option value="50">50</html:option>
                                                                                        <html:option value="100">100</html:option>
                                                                                        <html:option value="200">200</html:option>
                                                                                    </html:select>
                                                                                    records</label>
                                                                            </div>
                                                                            <div class="col-md-4">
                                                                                <label>
                                                                                    <html:select property="matchId" styleClass="form-control input-sm">
                                                                                        <html:option value=""> --select match-- </html:option>
                                                                                        <%
                                                                                            List<MatchInfoDTO> matchInfoDTOs = SportsService.getInstance().getMatchList();
                                                                                            for (MatchInfoDTO dto : matchInfoDTOs) {
                                                                                        %>
                                                                                        <html:option value="<%=String.valueOf(dto.getMatchID())%>"> <%=dto.getTitle()%> </html:option>
                                                                                        <% }%>
                                                                                    </html:select>
                                                                                </label>
                                                                            </div>
                                                                            <div class="col-md-2 col-sm-2">
                                                                                <label>
                                                                                    <html:select property="matchStatus" styleClass="form-control input-sm">
                                                                                        <html:option value=""> --select status-- </html:option>
                                                                                        <html:option value="1"> UPCOMING </html:option>
                                                                                        <html:option value="2"> RUNNING </html:option>
                                                                                        <html:option value="3"> ENDED </html:option>
                                                                                    </html:select>
                                                                                </label>
                                                                            </div>
                                                                            <div class="col-md-4">
                                                                                <button name="submitType" class="btn green-soft uppercase bold input-sm" type="submit"><i class="icon-magnifier"></i></button>
                                                                                <input name="action" type="hidden" value="<%=Constants.SEARCH%>" />
                                                                                <html:hidden name="ChannelMatchForm" property="column" />
                                                                                <html:hidden name="ChannelMatchForm" property="sort" />
                                                                            </div>
                                                                        </div>
                                                                        <div class="table-scrollable">
                                                                            <table class="table table-striped table-bordered table-hover">
                                                                                <thead>
                                                                                    <tr>
                                                                                        <th class="text-center">
                                                                                            <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                                                                                <input type="checkbox" onchange="checkAll(this)" class="group-checkable" />
                                                                                                <span></span>
                                                                                            </label>
                                                                                        </th>
                                                                                        <th class="text-center">Match</th>
                                                                                        <th class="text-center">Match Status</th>
                                                                                        <th class="text-center">Channel</th>
                                                                                    </tr>
                                                                                </thead>
                                                                                <tbody>

                                                                                    <%
                                                                                        List<MatchChannelMappedDTO> data1 = (List<MatchChannelMappedDTO>) session.getAttribute(Constants.DATA_ROWS);
                                                                                        session.removeAttribute(Constants.DATA_ROWS);
                                                                                        if (data1 != null && data1.size() > 0) {
                                                                                            for (int i = STARTING_RECORD_NO; i < ENDING_RECORD_NO; i++) {
                                                                                                MatchChannelMappedDTO dto = (MatchChannelMappedDTO) data1.get(i);
                                                                                                String matchStatus = "";
                                                                                                switch (dto.getMatchInfoDTO().getStatus()) {
                                                                                                    case 1:
                                                                                                        matchStatus = "UPCOMING";
                                                                                                        break;
                                                                                                    case 2:
                                                                                                        matchStatus = "RUNNING";
                                                                                                        break;
                                                                                                    case 3:
                                                                                                        matchStatus = "ENDED";
                                                                                                        break;
                                                                                                    default:
                                                                                                        matchStatus = "" + dto.getMatchInfoDTO().getStatus();
                                                                                                        break;
                                                                                                }
                                                                                    %>
                                                                                    <tr>
                                                                                        <td class="text-center">
                                                                                            <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                                                                                <input name="selectedIDs" type="checkbox" class="checkboxes" value="<%=dto.getMatchInfoDTO().getMatchID()%>" />
                                                                                                <span></span>
                                                                                            </label>
                                                                                        </td>
                                                                                        <td class="text-center"><%=dto.getMatchInfoDTO().getTitle()%></td>
                                                                                        <td class="text-center"><%=matchStatus%></td>
                                                                                        <td class="text-center">
                                                                                            <%
                                                                                                List<ChannelDTO> channelDTOs = dto.getChannelDTOList();
                                                                                                for (ChannelDTO channelDTO : channelDTOs) {
                                                                                            %>
                                                                                            <%if ((loginDTO.getPermissionLevel() == Constants.SUPER_ADMIN) || (loginDTO.getFeauterMapList().get(FEATURE_NAME).getHasDeletePermission())) {%>
                                                                                            <a href="deleteChannleMatchMapped.do?selectedIDs=<%=dto.getMatchInfoDTO().getMatchID()%>&channelId=<%=channelDTO.getChannelId()%>&action=<%=Constants.DELETE%>" class="btn btn-sm green" title="delete" onclick="javascript:return confirm('Are you sure you want to delete this content')"> <%=channelDTO.getTitle()%>
                                                                                                &nbsp;<i class="fa fa-remove"></i>
                                                                                            </a>
                                                                                            <% } else {%>
                                                                                            <label class="btn btn-sm green">
                                                                                                <%=channelDTO.getTitle()%>
                                                                                            </label>
                                                                                            <%}%>
                                                                                            <%} %>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <%
                                                                                            }
                                                                                        }
                                                                                    %>
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                        <div class="row">
                                                                            <div class="col-md-5 col-sm-12">
                                                                                <div>
                                                                                    Showing <%=(STARTING_RECORD_NO + 1)%> to <%=ENDING_RECORD_NO%> of <%=TOTAL_RECORDS%> entries
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-7 col-sm-12">
                                                                                <div style="float: right;">
                                                                                    <div> 
                                                                                        Page <a href="javascript: submitform(2, <%=(CURRENT_PAGE_NO - 1)%>, this);" class="btn btn-sm default prev <%=disabledPrev%>"><i class="fa fa-angle-left"></i></a>
                                                                                        <input name="pageNo" class="form-control input-sm input-inline input-mini" maxlenght="5" style="text-align:center; margin: 0 5px;" type="text" value="<%=CURRENT_PAGE_NO%>" onchange="javascript: submitform(2, 0, this);">
                                                                                        <a href="javascript: submitform(2, <%=(CURRENT_PAGE_NO + 1)%>, this);" class="btn btn-sm default next <%=disabledNext%>"><i class="fa fa-angle-right"></i></a> of <span><%=TOTAL_PAGES%></span>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-actions">
                                                                        <div class="col-md-offset-3 col-md-9">
                                                                            <%if ((loginDTO.getPermissionLevel() == Constants.SUPER_ADMIN) || (loginDTO.getFeauterMapList().get(FEATURE_NAME).getHasDeletePermission())) {%>
                                                                            <button type="submit" class="btn red" formaction="deleteChannleMatchMapped.do?action=<%=Constants.DELETE%>"> Delete </button>
                                                                            <% }%>
                                                                        </div>
                                                                    </div>
                                                                </html:form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <%@include file="/shared/footer.jsp"%>
        </div>
        <%@include file="/shared/script-list.jsp"%>
    </body>
</html>