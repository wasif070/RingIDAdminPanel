<%-- 
    Document   : channelMatchMappingDetails
    Created on : Nov 26, 2017, 12:11:20 PM
    Author     : mamun
--%>

<%@page import="org.ringid.sports.cricket.MatchInfoDTO"%>
<%@page import="com.ringid.admin.actions.sports.matchChannelMapped.MatchDTO"%>
<%@page import="com.ringid.admin.service.SportsService"%>
<%@include file="/login/loginTimeExpiry.jsp"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.util.List"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@include  file="/shared/head.jsp" %>

<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Admin Panel | Channel Match Mapping </title>
        <%@include file="/shared/css-list.jsp"%>
    </head>
    <body class="page-container-bg-solid">
        <div class="page-wrapper">
            <div class="page-wrapper-row">
                <div class="page-wrapper-top">
                    <div class="page-header">
                        <%@include  file="/shared/menu.jsp" %>
                    </div>
                </div>
            </div>
            <div class="page-wrapper-row full-height">
                <div class="page-wrapper-middle">
                    <div class="page-container">
                        <div class="page-content-wrapper">
                            <div class="page-content">
                                <div class="container">
                                    <ul class="page-breadcrumb breadcrumb">
                                        <li>
                                            <a href="welcome.do">Admin Panel</a>
                                            <span style="font-size: 14px; margin: 0 auto;" class="fa fa-angle-right"></span>
                                        </li>
                                        <li>
                                            <a href="javascript:;">Common</a>
                                            <i style="font-size: 14px; margin: 0 auto;" class="fa fa-angle-right"></i>
                                        </li>
                                        <li>
                                            <a href="channleMatchMappedInfo.do"> Channel Match Mapped </a>
                                            <span style="font-size: 14px; margin: 0 auto;" class="fa fa-angle-right"></span>
                                        </li>
                                        <li>
                                            <span id="features" data-class-active=".common .sports .matchChannelMapped"> Channel Match Mapping </span>
                                        </li>
                                    </ul>
                                    <div class="page-content-inner">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="tabbable-line boxless tabbable-reversed">
                                                    <div class="tab-content" style="padding-top: 15px;">
                                                        <div class="portlet box green">
                                                            <div class="portlet-title">
                                                                <div class="caption">
                                                                    <i class="fa fa-plus"></i> Channel Match Mapping Info </div>
                                                                <div class="tools">
                                                                    <a href="javascript:;" class="collapse"> </a>
                                                                </div>
                                                            </div>
                                                            <div class="portlet-body form">
                                                                <html:form action="channleMatchMapping" styleClass="form-horizontal" method="POST"  acceptCharset="UTF-8">
                                                                    <div class="form-body" >
                                                                        <div class="form-group">
                                                                            <label class="col-md-3 control-label">Select Match : </label>
                                                                            <div class="col-md-4 col-sm-4">
                                                                                <html:select property="matchId" styleClass="form-control">
                                                                                    <html:option value=""> -- Select Match -- </html:option>
                                                                                    <%
                                                                                        List<MatchInfoDTO> matchInfoDTOs = SportsService.getInstance().getMatchList();
                                                                                        for (MatchInfoDTO dto : matchInfoDTOs) {
                                                                                    %>
                                                                                    <html:option value="<%=String.valueOf(dto.getMatchID())%>"> <%=dto.getTitle()%> </html:option>
                                                                                    <% } %>
                                                                                </html:select>
                                                                                <html:messages id="matchId" property="matchId">
                                                                                    <span> Please Select Match </span>
                                                                                </html:messages>
                                                                            </div>
                                                                            <div class="col-md-4 col-sm-4">
                                                                                <button type="submit" class="btn green input-small" formaction="matchChannelMapping.do">Search</button>
                                                                            </div>
                                                                        </div>
                                                                        <div class="table-scrollable">
                                                                            <table class="table table-striped table-bordered table-hover">
                                                                                <thead>
                                                                                    <tr>
                                                                                        <th style="text-align: center" class="no-sort">
                                                                                            <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                                                                                <input type="checkbox" onchange="checkAll(this)" class="group-checkable" />
                                                                                                <span></span>
                                                                                            </label>
                                                                                        </th>
                                                                                        <th style="text-align: center;" class="no-sort">Channel</th>
                                                                                    </tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                    <%
                                                                                        String className = "odd gradeX";
                                                                                        List<MatchDTO> data1 = (List<MatchDTO>) session.getAttribute(Constants.DATA_ROWS);
                                                                                        session.removeAttribute(Constants.DATA_ROWS);
                                                                                        if (data1 != null && data1.size() > 0) {
                                                                                            for (int i = 0; i < data1.size(); i++) {
                                                                                                MatchDTO dto = (MatchDTO) data1.get(i);
                                                                                                if (i % 2 == 0) {
                                                                                                    className = "even gradeX";
                                                                                                } else {
                                                                                                    className = "odd gradeX";
                                                                                                }
                                                                                    %>
                                                                                    <tr class="<%=className%>" >
                                                                                        <td style="text-align: center">
                                                                                            <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                                                                                <input name="selectedIDs" type="checkbox" class="checkboxes" value="<%=dto.getChannelId()%>" <%if (dto.isActive()) {%> checked="checked" disabled="disabled" <% }%> />
                                                                                                <span></span>
                                                                                            </label>
                                                                                        </td>
                                                                                        <td style="text-align: center;"><%=dto.getChannelTitle()%></td>  
                                                                                    </tr>
                                                                                    <%
                                                                                            }
                                                                                        }
                                                                                    %>
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-actions">
                                                                        <div class="col-md-offset-3 col-md-9">
                                                                            <input type="hidden" name="action" value="3" />
                                                                            <input type="hidden" name="pageNo" />
                                                                            <input type="hidden" name="column" />
                                                                            <input type="hidden" name="sort" />
                                                                            <button type="submit" class="btn green"> Submit </button>
                                                                            &nbsp;<a href="channleMatchMappedInfo.do?matchId=<bean:write name="ChannelMatchForm" property="matchId" />&matchStatus=<bean:write name="ChannelMatchForm" property="matchStatus" />" class="btn green">Cancel</a>
                                                                        </div>
                                                                    </div>
                                                                </html:form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <%@include file="/shared/footer.jsp"%>
        </div>
        <%@include file="/shared/script-list.jsp"%>
    </body>
</html>