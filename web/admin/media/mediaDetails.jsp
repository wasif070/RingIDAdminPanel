<%-- 
    Document   : m_mediaList
    Created on : Feb 15, 2017, 7:25:51 PM
    Author     : MAMUN
--%>
<%@page import="com.ringid.admin.projectMenu.MenuNames"%>
<%@page import="org.ringid.newsfeeds.CassMultiMediaDTO"%>
<%@include file="../../login/loginTimeExpiry.jsp"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="java.util.*,com.ringid.admin.utils.Constants"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@include  file="/shared/head.jsp" %>
<%    String mediaType = request.getParameter("mediaType").toString();%>

<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Admin Panel | Media Details</title>
        <%@include file="/shared/css-list.jsp"%>
    </head>
    <body class="page-container-bg-solid">
        <div class="page-wrapper">
            <div class="page-wrapper-row">
                <div class="page-wrapper-top">
                    <div class="page-header">
                        <%@include  file="/shared/menu.jsp" %>
                        <%
                            String FEATURE_NAME = MenuNames.MEDIA;
                        %>
                    </div>
                </div>
            </div>
            <div class="page-wrapper-row full-height">
                <div class="page-wrapper-middle">
                    <div class="page-container">
                        <div class="page-content-wrapper">
                            <div class="page-content">
                                <div class="container">
                                    <ul class="page-breadcrumb breadcrumb">
                                        <li>
                                            <a href="welcome.do">Admin Panel</a>
                                            <span style="font-size: 14px; margin: 0 auto;" class="fa fa-angle-right"></span>
                                        </li>
                                        <li>
                                            <a href="mediaListInfo.do">Media</a>
                                            <span style="font-size: 14px; margin: 0 auto;" class="fa fa-angle-right"></span>
                                        </li>
                                        <li>
                                            <span id="features" data-class-active=".page-o-media .Media">Media Details</span>
                                        </li>
                                    </ul>
                                    <div class="page-content-inner">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="tabbable-line boxless tabbable-reversed">
                                                    <div class="tab-content" style="padding-top: 15px;">
                                                        <div class="portlet box green">
                                                            <div class="portlet-title">
                                                                <div class="caption">
                                                                    <i class="fa fa-pencil"></i>Media Details</div>
                                                                <div class="tools">
                                                                    <a href="javascript:;" class="collapse"> </a>
                                                                </div>
                                                            </div>
                                                            <div class="portlet-body form">
                                                                <html:form action="/makeTrending?id=${MediaDetailsForm.cassMultiMediaDTO.getMultiMediaId()}" styleClass="form-horizontal" method="POST">
                                                                    <div class="form-body">
                                                                        <div class="form-group last">
                                                                            <label class="col-md-3 control-label" style="font-weight: bold;">Title:</label>
                                                                            <div class="col-md-4">
                                                                                <p style="font-weight: bold; color: #008000;" class="form-control-static">${MediaDetailsForm.cassMultiMediaDTO.getTitle()}</p>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group last">
                                                                            <label class="col-md-3 control-label" style="font-weight: bold;">Artist:</label>
                                                                            <div class="col-md-4">
                                                                                <p style="font-weight: bold; color: #008000;" class="form-control-static">${MediaDetailsForm.cassMultiMediaDTO.getArtist()}</p>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group last">
                                                                            <label class="col-md-3 control-label" style="font-weight: bold;">User Name:</label>
                                                                            <div class="col-md-4">
                                                                                <p style="font-weight: bold; color: #008000;" class="form-control-static">${MediaDetailsForm.cassMultiMediaDTO.getFullName()}</p>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group last">
                                                                            <label class="col-md-3 control-label" style="font-weight: bold;">Media Type:</label>
                                                                            <div class="col-md-4">
                                                                                <p style="font-weight: bold; color: #008000;" class="form-control-static">${MediaDetailsForm.cassMultiMediaDTO.getMultiMediaType()}</p>
                                                                            </div>
                                                                        </div>
                                                                        <% if (request.getAttribute("type").toString().equals("1")) {%>
                                                                        <div class="form-group last">
                                                                            <label class="col-md-3 control-label" style="font-weight: bold;">Play Audio :</label>
                                                                            <div class="col-md-4">
                                                                                <video controls preload="metadata" class="form-control-static">
                                                                                    <source src="<%=Constants.MEDIA_CLOUD_BASE_URL%>${MediaDetailsForm.cassMultiMediaDTO.getMultiMediaUrl()}" type="video/mp4">
                                                                                    Your browser does not support HTML5 video.
                                                                                </video>
                                                                            </div>
                                                                        </div>
                                                                        <% }%>
                                                                        <% if (request.getAttribute("type").toString().equals("2")) {%>
                                                                        <div class="form-group last">
                                                                            <label class="col-md-3 control-label" style="font-weight: bold;">Play Video:</label>
                                                                            <div class="col-md-4">
                                                                                <video controls preload="metadata"  class="form-control-static">
                                                                                    <source src="<%=Constants.MEDIA_CLOUD_BASE_URL%>${MediaDetailsForm.cassMultiMediaDTO.getMultiMediaUrl()}" type="video/mp4">
                                                                                    Your browser does not support HTML5 video.
                                                                                </video>
                                                                            </div>
                                                                        </div>
                                                                        <% }%>
                                                                        <div class="form-group last">
                                                                            <label class="col-md-3 control-label" style="font-weight: bold;">Album Name:</label>
                                                                            <div class="col-md-4">
                                                                                <p style="font-weight: bold; color: #008000;" class="form-control-static">${MediaDetailsForm.cassMultiMediaDTO.getAlbumName()}</p>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group last">
                                                                            <label class="col-md-3 control-label" style="font-weight: bold;">Like Count:</label>
                                                                            <div class="col-md-4">
                                                                                <p style="font-weight: bold; color: #008000;" class="form-control-static">${MediaDetailsForm.cassMultiMediaDTO.getLikeCount()}</p>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group last">
                                                                            <label class="col-md-3 control-label" style="font-weight: bold;">View Count:</label>
                                                                            <div class="col-md-4">
                                                                                <p style="font-weight: bold; color: #008000;" class="form-control-static">${MediaDetailsForm.cassMultiMediaDTO.getViewCount()}</p>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group last">
                                                                            <label class="col-md-3 control-label" style="font-weight: bold;">Duration:</label>
                                                                            <div class="col-md-4">
                                                                                <p style="font-weight: bold; color: #008000;" class="form-control-static">${MediaDetailsForm.cassMultiMediaDTO.getDuration()}</p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-actions">
                                                                        <div class="row">
                                                                            <div class="col-md-offset-3 col-md-9">
                                                                                <%if (mediaType.equals("recent")) {%>
                                                                                <html:submit  styleClass="btn green" property="submitType" value="Make Trending" />
                                                                                <%} else if (mediaType.equals("trending")) {%>
                                                                                <html:submit  styleClass="btn green" property="submitType" value="Remove From Trending" />
                                                                                <%}%>
                                                                                &nbsp;<a href="mediaListInfo.do?mediaType=<%=mediaType%>" ></a>
                                                                                <html:hidden property="mediaType" value='<%=mediaType%>' />
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </html:form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <%@include file="/shared/footer.jsp"%>
        </div>
        <%@include file="/shared/script-list.jsp"%>
    </body>
</html>