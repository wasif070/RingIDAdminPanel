<%-- 
    Document   : permittedIpsTable
    Created on : Jun 18, 2017, 3:19:16 PM
    Author     : Rabby
--%>

<div class="form-body">
    <div class="form-group">
        <label class="col-md-3 control-label">IP <span style="color: red">*</span></label>
        <div class="col-md-4">
            <html:text property="ip" title="IP" styleClass="form-control" />
            <html:messages id="ip" property="ip">
                <bean:write name="ip"  filter="false"/>
            </html:messages>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label">Status </label>
        <div class="col-md-4">
            <html:select property="status" styleClass="form-control"> 
                <html:option  value="1">Active</html:option>
                <html:option  value="0">Inactive</html:option>
            </html:select>
            <html:messages id="status" property="status">
                <bean:write name="status"  filter="false"/>
            </html:messages>
        </div>
    </div>
    <div class="form-group last">
        <label class="col-md-3 control-label"></label>
        <div class="col-md-4">
            <%
                if (request.getSession(true).getAttribute("error") == "2") {
            %>  
            <html:messages id="message" property="message"><bean:write name="msg"  filter="false"/></html:messages>
            <bean:write name="PermittedIpsForm" property="message" filter="false"/>
            <%}%>
        </div>
    </div>
</div>
<div class="form-actions">
    <div class="row">
        <div class="col-md-offset-3 col-md-4">
            <button type="submit" class="btn green" name="saveButton">Submit</button>
            <a href="permittedIpsListInfo.do" class="btn default">Cancel</a>
            <html:hidden property="id"/>
            <input type="hidden" name="action" value="<%=request.getSession(true).getAttribute("formType")%>" />
        </div>
    </div>
</div>
