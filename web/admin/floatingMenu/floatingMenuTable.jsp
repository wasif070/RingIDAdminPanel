<%-- 
    Document   : floatingMenuTable
    Created on : Sep 26, 2017, 2:12:06 PM
    Author     : mamun
--%>

<div class="form-group">
    <label class="col-md-3 control-label">Name</label>
    <div class="col-md-4">
        <html:text property="name" title="name" styleClass="form-control" />
        <html:messages id="name" property="name">
            <bean:write name="name"  filter="false"/>
        </html:messages>
    </div>
</div>
<div class="form-group">
    <label class="col-md-3 control-label">Weight</label>
    <div class="col-md-4">
        <html:text property="weight" title="weight" styleClass="form-control" />
        <html:messages id="weight" property="weight">
            <bean:write name="weight"  filter="false"/>
        </html:messages>
    </div>
</div>
<div class="form-group">
    <label class="col-md-3 control-label">Slide Time</label>
    <div class="col-md-4">
        <html:text property="slideTime" title="slideTime" styleClass="form-control" />
        <html:messages id="slideTime" property="slideTime">
            <bean:write name="slideTime"  filter="false"/>
        </html:messages>
    </div>
</div>
<div class="form-group">
    <label class="col-md-3 control-label">Redirect Id</label>
    <div class="col-md-4">
        <html:text property="redirectId" title="redirect id" styleClass="form-control" />
        <html:messages id="redirectId" property="redirectId">
            <bean:write name="redirectId"  filter="false"/>
        </html:messages>
    </div>
</div>
<div class="form-group">
    <label class="col-md-3 control-label">Button Type</label>
    <div class="col-md-4">
        <html:text property="btnType" title="button type" styleClass="form-control" />
    </div>
</div>
<div class="form-group">
    <label class="col-md-3 control-label">Button Background Color</label>
    <div class="col-md-4">
        <input name="btnBgColor" type="text" id="hue-demo" class="form-control demo" data-control="hue" value='<bean:write name="FloatingMenuForm" property="btnBgColor"/>'>
    </div>
</div>
<div class="form-group">
    <label class="col-md-3 control-label">Button Text Color</label>
    <div class="col-md-4">
        <input name="btnTxtColor" type="text" id="hue-demo" class="form-control demo" data-control="hue" value='<bean:write name="FloatingMenuForm" property="btnTxtColor"/>'>
    </div>
</div>