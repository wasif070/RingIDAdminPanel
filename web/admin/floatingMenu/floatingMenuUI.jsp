<%-- 
    Document   : floatingMenuUI
    Created on : Sep 26, 2017, 2:11:52 PM
    Author     : mamun
--%>

<%@include file="/login/loginTimeExpiry.jsp"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.ringid.admin.utils.Constants"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@include  file="/shared/head.jsp" %>

<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Admin Panel | Add Floating Menu</title>
        <%@include file="/shared/css-list.jsp"%>
    </head>
    <body class="page-container-bg-solid">
        <div class="page-wrapper">
            <div class="page-wrapper-row">
                <div class="page-wrapper-top">
                    <div class="page-header">
                        <%@include  file="/shared/menu.jsp" %>
                    </div>
                </div>
            </div>
            <div class="page-wrapper-row full-height">
                <div class="page-wrapper-middle">
                    <div class="page-container">
                        <div class="page-content-wrapper">
                            <div class="page-content">
                                <div class="container">
                                    <ul class="page-breadcrumb breadcrumb">
                                        <li>
                                            <a href="welcome.do">Admin Panel</a>
                                            <span style="font-size: 14px; margin: 0 auto;" class="fa fa-angle-right"></span>
                                        </li>
                                        <li>
                                            <a href="javascript:;">Common</a>
                                            <span style="font-size: 14px; margin: 0 auto;" class="fa fa-angle-right"></span>
                                        </li>
                                        <li>
                                            <a href="showFloatingMenuList.do">Floating Menus</a>
                                            <span style="font-size: 14px; margin: 0 auto;" class="fa fa-angle-right"></span>
                                        </li>
                                        <li>
                                            <span id="features" data-class-active=".common .floatingMenus .addFloatingMenu">Add Floating Menu</span>
                                        </li>
                                    </ul>
                                    <div class="page-content-inner">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="tabbable-line boxless tabbable-reversed">
                                                    <div class="portlet box green">
                                                        <div class="portlet-title">
                                                            <div class="caption">
                                                                <i class="fa fa-plus"></i> Add Floating Menu Info </div>
                                                            <div class="tools">
                                                                <a href="javascript:;" class="collapse"> </a>
                                                            </div>
                                                        </div>
                                                        <div class="portlet-body form">
                                                            <html:form action="addFloatingMenu" method="POST" acceptCharset="UTF-8" styleClass="form-horizontal" >
                                                                <div class="form-body">
                                                                    <div class="form-group">
                                                                        <label class="col-md-3 control-label">Tab</label>
                                                                        <div class="col-md-4">
                                                                            <html:select property="tab" styleClass="form-control"> 
                                                                                <html:option  value="1">HOME</html:option>     
                                                                                <html:option  value="2">LIVE</html:option>  
                                                                                <html:option  value="3">CHANNEL</html:option>
                                                                                <html:option  value="4">VIRAL</html:option>
                                                                            </html:select>
                                                                            <html:messages id="tab" property="tab">
                                                                                <bean:write name="tab"  filter="false"/>
                                                                            </html:messages>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label class="col-md-3 control-label">Type</label>
                                                                        <div class="col-md-4">
                                                                            <html:text property="type" title="type" styleClass="form-control" />
                                                                            <html:messages id="type" property="type">
                                                                                <bean:write name="type"  filter="false"/>
                                                                            </html:messages>
                                                                        </div>
                                                                    </div>
                                                                    <%@include file="floatingMenuTable.jsp" %>
                                                                </div>
                                                                <div class="form-actions">
                                                                    <div class="row">
                                                                        <div class="col-md-offset-3 col-md-4">
                                                                            <button type="submit" class="btn green" name="saveButton">Submit</button>
                                                                            <a href="showFloatingMenuList.do" class="btn default">Cancel</a>
                                                                            <input type="hidden" name="action" value="<%=Constants.ADD%>" />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </html:form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <%@include file="/shared/footer.jsp"%>
        </div>
        <%@include file="/shared/script-list.jsp"%>
    </body>
</html>
