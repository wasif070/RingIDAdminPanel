<%-- 
    Document   : ringBitSettingList
    Created on : Jan 31, 2018, 3:23:51 PM
    Author     : mamun
--%>

<%@page import="com.ringid.admin.dto.RingBitDTO"%>
<%@page import="com.ringid.admin.projectMenu.MenuNames"%>
<%@page import="java.util.ArrayList"%>
<%@include file="/login/loginTimeExpiry.jsp"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.ringid.admin.utils.Constants"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@include  file="/shared/head.jsp" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Admin Panel | RingBit Settings List</title>
        <%@include file="/shared/css-list.jsp"%>
    </head>
    <body class="page-container-bg-solid">
        <div class="page-wrapper">
            <div class="page-wrapper-row">
                <div class="page-wrapper-top">
                    <div class="page-header">
                        <%@include  file="/shared/menu.jsp" %>
                        <%  
                            String FEATURE_NAME = MenuNames.RINGBIT_IMAGE_O_TEXT;
                            int TOTAL_RECORDS = 0, TOTAL_PAGES = 0, CURRENT_PAGE_NO = 0, STARTING_RECORD_NO = 0, ENDING_RECORD_NO = 0;
                            String disabledPrev = "", disabledNext = "";
                            if (request.getAttribute(Constants.TOTAL_RECORDS) != null) {
                                TOTAL_RECORDS = Integer.valueOf(request.getAttribute(Constants.TOTAL_RECORDS).toString());
                            }
                            if (request.getAttribute(Constants.TOTAL_PAGES) != null) {
                                TOTAL_PAGES = Integer.valueOf(request.getAttribute(Constants.TOTAL_PAGES).toString());
                            }
                            if (request.getAttribute(Constants.CURRENT_PAGE_NO) != null) {
                                CURRENT_PAGE_NO = Integer.valueOf(request.getAttribute(Constants.CURRENT_PAGE_NO).toString());
                            }
                            if (request.getAttribute(Constants.STARTING_RECORD_NO) != null) {
                                STARTING_RECORD_NO = Integer.valueOf(request.getAttribute(Constants.STARTING_RECORD_NO).toString());
                            }
                            if (request.getAttribute(Constants.ENDING_RECORD_NO) != null) {
                                ENDING_RECORD_NO = Integer.valueOf(request.getAttribute(Constants.ENDING_RECORD_NO).toString());
                            }
                            if (STARTING_RECORD_NO <= 0) {
                                disabledPrev = "disabled";
                                STARTING_RECORD_NO = 0;
                            }
                            if (ENDING_RECORD_NO >= TOTAL_RECORDS) {
                                disabledNext = "disabled";
                                ENDING_RECORD_NO = TOTAL_RECORDS;
                            }
                        %>
                    </div>
                </div>
            </div>
            <div class="page-wrapper-row full-height">
                <div class="page-wrapper-middle">
                    <div class="page-container">
                        <div class="page-content-wrapper">
                            <div class="page-content">
                                <div class="container">
                                    <ul class="page-breadcrumb breadcrumb">
                                        <li>
                                            <a href="welcome.do">Admin Panel</a>
                                            <span style="font-size: 14px; margin: 0 auto;" class="fa fa-angle-right"></span>
                                        </li>
                                        <li>
                                            <a href="javascript:;">Common</a>
                                            <i style="font-size: 14px; margin: 0 auto;" class="fa fa-angle-right"></i>
                                        </li>
                                        <li>
                                            <a href="showRingBitSettingsList.do">RingBit</a>
                                            <i style="font-size: 14px; margin: 0 auto;" class="fa fa-angle-right"></i>
                                        </li>
                                        <li>
                                            <span id="features" data-class-active=".common .ringBit .manageRingBitSettings">RingBit Image & Text</span>
                                        </li>
                                    </ul>
                                    <div class="page-content-inner">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="tabbable-line boxless tabbable-reversed">
                                                    <div class="tab-content" style="padding-top: 15px;">
                                                        <%
                                                            ArrayList<RingBitDTO> data1 = (ArrayList<RingBitDTO>) session.getAttribute(Constants.DATA_ROWS);
                                                            session.removeAttribute(Constants.DATA_ROWS);
                                                            RingBitDTO ringBitDTO = null;
                                                            if (data1 != null && data1.size() > 0) {
                                                                ringBitDTO = (RingBitDTO) data1.get(0);
                                                            }
                                                        %>
                                                        <div class="portlet box green">
                                                            <div class="portlet-title">
                                                                <div class="caption">
                                                                    <i class="fa fa-list-ul"></i> RingBit Image & Text Info </div>
                                                                <div class="tools">
                                                                    <a href="javascript:;" class="collapse"> </a>
                                                                    <a data-original-title="Reload" href="showRingBitSettingsList.do?ForceReload=1" style="width: 13px; background-image: url(assets/global/img/portlet-reload-icon-white.png);" class="tooltips"> </a>
                                                                    <%if (ringBitDTO.getRingBitImageId() <= 0 && ((loginDTO.getPermissionLevel() == Constants.SUPER_ADMIN) || (loginDTO.getFeauterMapList().get(FEATURE_NAME).getHasAddPermission()))) {%>
                                                                    <a data-original-title="AddNew" style="color: white; width: 16px; background-image: url(assets/global/img/add-1.png);" href="admin/ringBit/ringBitSettingUI.jsp" class="tooltips"> </a>
                                                                    <% }%>
                                                                </div>
                                                            </div>
                                                            <div class="portlet-body form">
                                                                <html:form action="showPaymentMethodList" styleClass="form-horizontal" method="POST"  acceptCharset="UTF-8">
                                                                    <div class="form-body">
                                                                        <div class="table-scrollable">
                                                                            <%
                                                                                    if (ringBitDTO.getSettingValueImage() != null || ringBitDTO.getSettingValueText() != null) {
                                                                            %>
                                                                            <table class="table table-striped table-bordered table-hover">
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <th class="text-center align-middle">Ringbit Mining ImageId</th>
                                                                                        <td class="text-center align-middle"><%=ringBitDTO.getRingBitImageId()%></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <th class="text-center align-middle">Ringbit Mining Image Setting Value</th>
                                                                                        <td class="text-center align-middle"><%=ringBitDTO.getSettingValueImage()%></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <th class="text-center align-middle">Ringbit Mining TextId</th>
                                                                                        <td class="text-center align-middle"><%=ringBitDTO.getRingBitTextId()%></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <th class="text-center align-middle">Ringbit Mining Text Setting Value</th>
                                                                                        <td class="text-center align-middle"><%=ringBitDTO.getSettingValueText()%></td>
                                                                                    </tr>
                                                                                    <%if ((loginDTO.getPermissionLevel() == Constants.SUPER_ADMIN) || (loginDTO.getFeauterMapList().get(FEATURE_NAME).getHasModifyPermission())) {%>
                                                                                    <tr>
                                                                                        <th class="text-center align-middle">Action</th>
                                                                                        <td class="text-center align-middle"><a href="editRingBitSetting.do?action=<%=Constants.EDIT%>" class="" title="Change"><span class="fa fa-edit"></span>Edit</a></td>
                                                                                    </tr>
                                                                                    <% }%>
                                                                                </tbody>
                                                                            </table>
                                                                            <%}%>
                                                                        </div>
                                                                    </div>
                                                                </html:form>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <%@include file="/shared/footer.jsp"%>
        </div>
        <%@include file="/shared/script-list.jsp"%>
    </body>
</html>
