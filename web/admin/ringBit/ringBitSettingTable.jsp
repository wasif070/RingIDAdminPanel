<%-- 
    Document   : RingBitSettingTable
    Created on : Jan 31, 2018, 4:13:21 PM
    Author     : mamun
--%>

<%@page import="com.ringid.admin.utils.Constants"%>
<div class="form-body">
    <div class="form-group">
        <label class="col-md-3 control-label"> 
            RingBit Setting Value Text
            <span class="required" aria-required="true"> * </span>
        </label>
        <div class="col-md-4">
            <html:text property="settingsValueText" title="ringbit setting value text" styleClass="form-control" />
            <html:messages id="settingsValueText" property="settingsValueText">
                <bean:write name="settingsValueText"  filter="false"/>
            </html:messages>
        </div>
    </div>
    <div class="form-group ">
        <label class="control-label col-md-3">
            RingBit Setting value Image
            <span class="required" aria-required="true"> * </span>
        </label>
        <div class="col-md-9">
            <div class="fileinput fileinput-new" data-provides="fileinput">
                <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px; line-height: 150px;">
                    <img src="<%=Constants.IMAGE_BASE_URL%><bean:write name='RingBitForm' property='settingsValueImage' />" />
                </div>
                <div>
                    <span class="btn green btn-outline btn-file">
                        <span class="fileinput-new"> Select image </span>
                        <span class="fileinput-exists"> Change image </span>
                        <html:file property="theFile" />
                    </span>
                    <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                    <span id="msgSpan" style="display: inline-block;">
                        <html:messages id="theFile" property="theFile">
                            <bean:write name="theFile"  filter="false"/>
                        </html:messages>
                    </span>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="form-actions">
    <div class="row">
        <div class="col-md-offset-3 col-md-4">
            <button type="submit" class="btn green" name="saveButton">Submit</button>
            <a href="showRingBitSettingsList.do" class="btn default">Cancel</a>
            <html:hidden property="ringBitImageId"/>
            <html:hidden property="ringBitTextId"/>
            <html:hidden property="dataType"/>
            <html:hidden property="settingsValueImage"/>
            <input type="hidden" name="action" value="<%=request.getSession(true).getAttribute("formType")%>" />
        </div>
    </div>
</div>