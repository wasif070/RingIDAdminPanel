<%-- 
    Document   : m_logSettingTable
    Created on : Feb 14, 2017, 4:21:36 PM
    Author     : ipvision
--%>

<div class="form-body">
    <div class="form-group">
        <label class="col-md-3 control-label">Log Setting Name</label>
        <div class="col-md-4">
            <html:text property="name" title="Log Setting Name" styleClass="form-control" />
            <html:messages id="name" property="name">
                <bean:write name="name"  filter="false"/>
            </html:messages>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label">Value</label>
        <div class="col-md-4">
            <html:select property="value" styleClass="form-control">
                <html:option value="0">Inactive</html:option>     
                <html:option value="1">Active</html:option>                                
            </html:select>
        </div>
    </div>
</div>
<div class="form-actions">
    <div class="row">
        <div class="col-md-offset-3 col-md-4">
            <button type="submit" class="btn green" name="saveButton">Submit</button>
            <a href="logSettingListInfo.do" class="btn default">Cancel</a>
            <html:hidden property="id"/>
            <input type="hidden" name="action" value="<%=request.getSession(true).getAttribute("formType")%>" />
        </div>
    </div>
</div>