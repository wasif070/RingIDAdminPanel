<%-- 
    Document   : ambassadorUI
    Created on : Apr 22, 2018, 11:12:13 AM
    Author     : mamun
--%>

<%@page import="org.ringid.pages.ServiceDTO"%>
<%@page import="org.ringid.pages.PageDTO"%>
<%@include file="/login/loginTimeExpiry.jsp"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.ringid.admin.utils.Constants"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@include  file="/shared/head.jsp" %>

<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Admin Panel | Add Ambassador</title>
        <%@include file="/shared/css-list.jsp"%>
    </head>
    <body class="page-container-bg-solid">
        <div class="page-wrapper">
            <div class="page-wrapper-row">
                <div class="page-wrapper-top">
                    <div class="page-header">
                        <%@include  file="/shared/menu.jsp"%>
                    </div>
                </div>
            </div>
            <div class="page-wrapper-row full-height">
                <div class="page-wrapper-middle">
                    <div class="page-container">
                        <div class="page-content-wrapper">
                            <div class="page-content">
                                <div class="container">
                                    <ul class="page-breadcrumb breadcrumb">
                                        <li>
                                            <a href="welcome.do">Admin Panel</a>
                                            <span style="font-size: 14px; margin: 0 auto;" class="fa fa-angle-right"></span>
                                        </li>
                                        <li>
                                            <a href="javascript:;"> Live / Channel </a>
                                            <span style="font-size: 14px; margin: 0 auto;" class="fa fa-angle-right"></span>
                                        </li>
                                        <li>
                                            <a href="showAmbassadorList.do">Ambassador</a>
                                            <i style="font-size: 14px; margin: 0 auto;" class="fa fa-angle-right"></i>
                                        </li>
                                        <li>
                                            <span id="features" data-class-active=".live-o-channel .ambassador">Add Ambassador</span>
                                        </li>
                                    </ul>
                                    <div class="page-content-inner">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="tabbable-line boxless tabbable-reversed">
                                                    <div class="tab-content" style="padding-top: 15px;">
                                                        <div class="portlet box green">
                                                            <div class="portlet-title">
                                                                <div class="caption">
                                                                    <i class="fa fa-plus"></i> Add Ambassador </div>
                                                                <div class="tools">
                                                                    <a href="javascript:;" class="collapse"> </a>
                                                                </div>
                                                            </div>
                                                            <div class="portlet-body form">
                                                                <html:form action="addAmbassador" styleClass="form-horizontal" method="POST"  acceptCharset="UTF-8">
                                                                    <div class="form-body">
                                                                        <div class="form-group">
                                                                            <label class="col-md-3 control-label">UserId</label>
                                                                            <div class="col-md-4">
                                                                                <input placeholder="userId or ringId" type="text" name="searchText" class="form-control" value='<bean:write name="AmbassadorForm" property="searchText"/>' />
                                                                            </div>
                                                                            <div class="col-md-4">
                                                                                <input type="submit" value="Search" class="btn green input-small" formaction="searchAmbassadorInfo.do"/>
                                                                            </div>
                                                                        </div>
                                                                        <logic:notEmpty name="AmbassadorForm" property="userName" >
                                                                            <div class="form-group">
                                                                                <label class="col-md-3 control-label">Full Name</label>
                                                                                <div class="col-md-4">
                                                                                    <label class="control-label"><bean:write name="AmbassadorForm" property="userName" /></label>
                                                                                </div>
                                                                            </div>
                                                                        </logic:notEmpty>
                                                                        <logic:greaterThan name="AmbassadorForm" property="userId" value="0">
                                                                            <div class="form-group">
                                                                                <label class="col-md-3 control-label">UserId</label>
                                                                                <div class="col-md-4">
                                                                                    <label class="control-label"><bean:write name="AmbassadorForm" property="userId" /></label>
                                                                                    <html:hidden property="userId" />
                                                                                </div>
                                                                            </div>
                                                                        </logic:greaterThan>
                                                                        <logic:greaterThan name="AmbassadorForm" property="ringId" value="0">
                                                                            <div class="form-group">
                                                                                <label class="col-md-3 control-label">RingId</label>
                                                                                <div class="col-md-4">
                                                                                    <label class="control-label"><bean:write name="AmbassadorForm" property="ringId" /></label>
                                                                                </div>
                                                                            </div>
                                                                        </logic:greaterThan>
                                                                        <logic:notEmpty name="AmbassadorForm" property="userTypeStr">
                                                                            <div class="form-group">
                                                                                <label class="col-md-3 control-label">User Type</label>
                                                                                <div class="col-md-4">
                                                                                    <label class="control-label"><bean:write name="AmbassadorForm" property="userTypeStr" /></label>
                                                                                </div>
                                                                            </div>
                                                                        </logic:notEmpty>
                                                                        <logic:equal name="AmbassadorForm" property="spamUser" value="true">
                                                                            <div class="form-group">
                                                                                <label class="col-md-3 control-label">Spammed</label>
                                                                                <div class="col-md-4">
                                                                                    <label class="control-label"><bean:write name="AmbassadorForm" property="spamUser" /></label>
                                                                                </div>
                                                                            </div>
                                                                        </logic:equal>
                                                                        <logic:greaterThan name="AmbassadorForm" property="userId" value="0">
                                                                            <div class="form-group">
                                                                                <label class="col-md-3 control-label">User Pages</label>
                                                                                <div class="col-md-4">
                                                                                    <html:select styleClass="form-control" property="ambassadors" size="10" multiple="true">
                                                                                        <%
                                                                                            List<ServiceDTO> userPageList = (List<ServiceDTO>) session.getAttribute("userPageList");
                                                                                            session.removeAttribute("userPageList");
                                                                                            if (userPageList != null && userPageList.size() > 0) {
                                                                                                //out.print(data.size());
                                                                                                for (int i = 0; i < userPageList.size(); i++) {
                                                                                                    ServiceDTO serviceDTO = (ServiceDTO) userPageList.get(i);
                                                                                        %>
                                                                                        <html:option value="<%=String.valueOf(serviceDTO.getPageId())%>"><%=serviceDTO.getServiceName()%></html:option>
                                                                                        <% }
                                                                                            }%>
                                                                                    </html:select>
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label class="col-md-3 control-label">Product Page</label>
                                                                                <div class="col-md-4">
                                                                                    <html:select styleClass="form-control" property="productPageId">
                                                                                        <%
                                                                                            List<PageDTO> productPages = (List<PageDTO>) session.getAttribute("productPages");
                                                                                            session.removeAttribute("productPages");
                                                                                            if (productPages != null && productPages.size() > 0) {
                                                                                                //out.print(data.size());
                                                                                                for (int i = 0; i < productPages.size(); i++) {
                                                                                                    PageDTO dto = (PageDTO) productPages.get(i);
                                                                                        %>
                                                                                        <html:option value="<%=String.valueOf(dto.getPageId())%>"><%=dto.getPageName()%></html:option>
                                                                                        <% }
                                                                                            }%>
                                                                                    </html:select>
                                                                                </div>
                                                                            </div>
                                                                        </logic:greaterThan>
                                                                    </div>
                                                                    <div class="form-actions">
                                                                        <div class="row">
                                                                            <div class="col-md-offset-3 col-md-9">
                                                                                <input type="hidden" name="action" value="<%=Constants.ADD%>" />
                                                                                <logic:notEqual name="AmbassadorForm" property="userType" value="1">
                                                                                    <button name="submitType" type="submit" class="btn green" value="USER">Add Ambassador</button>
                                                                                </logic:notEqual>
                                                                                <logic:equal name="AmbassadorForm" property="userType" value="1">
                                                                                    <button name="submitType" type="submit" class="btn green" value="USER">Add Ambassador With User</button>
                                                                                    &nbsp;<button name="submitType" type="submit" class="btn green" value="PAGE">Add Ambassador With Page</button>
                                                                                    &nbsp;<button name="submitType" type="submit" class="btn green" value="BOTH">Both</button>
                                                                                    &nbsp;<a href="showAmbassadorList.do" class="btn default">Cancel</a>
                                                                                </logic:equal>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </html:form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <%@include file="/shared/footer.jsp"%>
        </div>
        <%@include file="/shared/script-list.jsp"%>
    </body>
</html>
