<%-- 
    Document   : ambassadorList
    Created on : Apr 22, 2018, 11:12:04 AM
    Author     : mamun
--%>

<%@page import="org.ringid.pages.PageDTO"%>
<%@page import="com.ringid.admin.projectMenu.MenuNames"%>
<%@include file="/login/loginTimeExpiry.jsp"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.ringid.admin.utils.Constants"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@include  file="/shared/head.jsp" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Admin Panel | Ambassador List Info</title>
        <%@include file="/shared/css-list.jsp"%>
    </head>
    <body class="page-container-bg-solid">
        <div class="page-wrapper">
            <div class="page-wrapper-row">
                <div class="page-wrapper-top">
                    <div class="page-header">
                        <%@include  file="/shared/menu.jsp" %>
                        <%
                            String FEATURE_NAME = MenuNames.AMBASSADOR;
                            int TOTAL_RECORDS = 0, TOTAL_PAGES = 0, CURRENT_PAGE_NO = 0, STARTING_RECORD_NO = 0, ENDING_RECORD_NO = 0;
                            String disabledPrev = "", disabledNext = "";
                            if (request.getAttribute(Constants.TOTAL_RECORDS) != null) {
                                TOTAL_RECORDS = Integer.valueOf(request.getAttribute(Constants.TOTAL_RECORDS).toString());
                            }
                            if (request.getAttribute(Constants.TOTAL_PAGES) != null) {
                                TOTAL_PAGES = Integer.valueOf(request.getAttribute(Constants.TOTAL_PAGES).toString());
                            }
                            if (request.getAttribute(Constants.CURRENT_PAGE_NO) != null) {
                                CURRENT_PAGE_NO = Integer.valueOf(request.getAttribute(Constants.CURRENT_PAGE_NO).toString());
                            }
                            if (request.getAttribute(Constants.STARTING_RECORD_NO) != null) {
                                STARTING_RECORD_NO = Integer.valueOf(request.getAttribute(Constants.STARTING_RECORD_NO).toString());
                            }
                            if (request.getAttribute(Constants.ENDING_RECORD_NO) != null) {
                                ENDING_RECORD_NO = Integer.valueOf(request.getAttribute(Constants.ENDING_RECORD_NO).toString());
                            }
                            if (STARTING_RECORD_NO <= 0) {
                                disabledPrev = "disabled";
                                STARTING_RECORD_NO = 0;
                            }
                            if (ENDING_RECORD_NO >= TOTAL_RECORDS) {
                                disabledNext = "disabled";
                                ENDING_RECORD_NO = TOTAL_RECORDS;
                            }
                        %>
                    </div>
                </div>
            </div>
            <div class="page-wrapper-row full-height">
                <div class="page-wrapper-middle">
                    <div class="page-container">
                        <div class="page-content-wrapper">
                            <div class="page-content">
                                <div class="container">
                                    <ul class="page-breadcrumb breadcrumb">
                                        <li>
                                            <a href="welcome.do">Admin Panel</a>
                                            <span style="font-size: 14px; margin: 0 auto;" class="fa fa-angle-right"></span>
                                        </li>
                                        <li>
                                            <a href="javascript:;"> Live / Channel </a>
                                            <span style="font-size: 14px; margin: 0 auto;" class="fa fa-angle-right"></span>
                                        </li>
                                        <li>
                                            <a href="showAmbassadorList.do">Ambassador</a>
                                            <i style="font-size: 14px; margin: 0 auto;" class="fa fa-angle-right"></i>
                                        </li>
                                        <li>
                                            <span id="features" data-class-active=".live-o-channel .ambassador">Manage Ambassador</span>
                                        </li>
                                    </ul>
                                    <div class="page-content-inner">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="tabbable-line boxless tabbable-reversed">
                                                    <div class="tab-content" style="padding-top: 15px;">
                                                        <div class="portlet box green">
                                                            <div class="portlet-title">
                                                                <div class="caption">
                                                                    <i class="fa fa-list-ul"></i> Ambassador List Info </div>
                                                                <div class="tools">
                                                                    <a href="javascript:;" class="collapse"> </a>
                                                                    <a data-original-title="Reload" href="showAmbassadorList.do?ForceReload=1" style="width: 13px; background-image: url(assets/global/img/portlet-reload-icon-white.png);" class="tooltips"> </a>
                                                                    <%if ((loginDTO.getPermissionLevel() == Constants.SUPER_ADMIN) || (loginDTO.getFeauterMapList().get(FEATURE_NAME).getHasAddPermission())) {%>
                                                                    <a data-original-title="AddNew" style="color: white; width: 16px; background-image: url(assets/global/img/add-1.png);" href="admin/ambassador/ambassadorUI.jsp" class="tooltips"> </a>
                                                                    <% }%>
                                                                </div>
                                                            </div>
                                                            <div class="portlet-body form">
                                                                <html:form action="showAmbassadorList" styleClass="form-horizontal" method="POST"  acceptCharset="UTF-8">
                                                                    <div class="form-body">
                                                                        <div class="form-group">
                                                                            <div class="col-md-2 col-sm-2">
                                                                                <label>
                                                                                    <html:select styleClass="bs-select form-control input-sm input-xsmall input-inline" property="recordPerPage" onchange="javascript: submitform(1, 1, this);">
                                                                                        <html:option value="10">10</html:option>
                                                                                        <html:option value="25">25</html:option>
                                                                                        <html:option value="50">50</html:option>
                                                                                        <html:option value="100">100</html:option>
                                                                                        <html:option value="200">200</html:option>
                                                                                    </html:select>
                                                                                    records</label>
                                                                            </div>
                                                                            <label class="col-md-3 col-sm-3 control-label"> Product Page : </label>
                                                                            <div class="col-md-3 col-sm-3">
                                                                                <html:select styleClass="form-control" property="productPageId">
                                                                                    <html:option value="">-- Select Product Page --</html:option>
                                                                                    <%
                                                                                        List<PageDTO> productPages = (List<PageDTO>) session.getAttribute("productPages");
                                                                                        session.removeAttribute("productPages");
                                                                                        if (productPages != null && productPages.size() > 0) {
                                                                                            for (int i = 0; i < productPages.size(); i++) {
                                                                                                PageDTO dto = (PageDTO) productPages.get(i);
                                                                                    %>
                                                                                    <html:option value="<%=String.valueOf(dto.getPageId())%>"><%=dto.getPageName()%></html:option>
                                                                                    <% }
                                                                                        }%>
                                                                                </html:select>
                                                                            </div>
                                                                            <div class="col-md-4 col-sm-4">
                                                                                <div class="input-group" style="float: right;">
                                                                                    <input name="searchText" type="text" class="form-control input-sm input-small" placeholder="Search" value="<bean:write name="AmbassadorForm" property="searchText"/>">
                                                                                    <span>
                                                                                        <button class="btn green-soft uppercase bold input-sm" type="submit"><i class="icon-magnifier"></i></button>
                                                                                        <input name="action" type="hidden" value="<%=Constants.SEARCH%>" />
                                                                                        <html:hidden property="column" />
                                                                                        <html:hidden property="sort" />
                                                                                    </span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="table-scrollable">
                                                                            <table class="table table-striped table-bordered table-hover">
                                                                                <thead>
                                                                                    <tr>
                                                                                        <th class="text-center align-middle">
                                                                                            <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                                                                                <input type="checkbox" onchange="checkAll(this)" class="group-checkable" />
                                                                                                <span></span>
                                                                                            </label>
                                                                                        </th>
                                                                                        <th class="text-center ">PageId</th> <!-- sortable both/asc/desc/asc-disabled/desc-disabled -->
                                                                                        <th class="text-center ">PageRingId</th>
                                                                                        <th class="text-center ">PageOwner</th>
                                                                                        <th class="text-center ">PageName</th>
                                                                                        <th class="text-center ">PageType</th>
                                                                                        <th class="text-center ">Status</th>
                                                                                        <th class="text-center ">AutoFollow</th>
                                                                                        <th class="text-center ">Verified</th>
                                                                                        <th class="text-center ">Country</th>
                                                                                            <%if ((loginDTO.getPermissionLevel() == Constants.SUPER_ADMIN) || (loginDTO.getFeauterMapList().get(FEATURE_NAME).getHasModifyPermission()
                                                                                                        || loginDTO.getFeauterMapList().get(FEATURE_NAME).getHasDeletePermission())) {%>      
                                                                                        <th class="text-center">Action</th>
                                                                                            <% }%> 
                                                                                    </tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                    <%
                                                                                        List<PageDTO> data1 = (List<PageDTO>) session.getAttribute(Constants.DATA_ROWS);
                                                                                        session.removeAttribute(Constants.DATA_ROWS);
                                                                                        if (data1 != null && data1.size() > 0) {
                                                                                            String pageActiveStatus = "", autoFollow = "", pageType = "";
                                                                                            for (int i = STARTING_RECORD_NO; i < ENDING_RECORD_NO; i++) {
                                                                                                PageDTO pageDTO = data1.get(i);
                                                                                                if (pageDTO.isIsAutoFollowDisabled()) {
                                                                                                    autoFollow = "Disabled";
                                                                                                } else {
                                                                                                    autoFollow = "Enabled";
                                                                                                }
                                                                                                switch (pageDTO.getPageActiveStatus()) {
                                                                                                    case 0:
                                                                                                        pageActiveStatus = "Active";
                                                                                                        break;
                                                                                                    case 1:
                                                                                                        pageActiveStatus = "Deactive";
                                                                                                        break;
                                                                                                    case 2:
                                                                                                        pageActiveStatus = "Closed";
                                                                                                        break;
                                                                                                    default:
                                                                                                        pageActiveStatus = "" + pageDTO.getPageActiveStatus();
                                                                                                        break;
                                                                                                }

                                                                                                switch (pageDTO.getPageType()) {
                                                                                                    case 1:
                                                                                                        pageType = "DEFAULT USER";
                                                                                                        break;
                                                                                                    case 2:
                                                                                                        pageType = "SPECIAL CONTACT";
                                                                                                        break;
                                                                                                    case 3:
                                                                                                        pageType = "USER PAGE";
                                                                                                        break;
                                                                                                    case 4:
                                                                                                        pageType = "ROOM PAGE";
                                                                                                        break;
                                                                                                    case 5:
                                                                                                        pageType = "DONATION PAGE";
                                                                                                        break;
                                                                                                    case 7:
                                                                                                        pageType = "SPORTS PAGE";
                                                                                                        break;
                                                                                                    case 10:
                                                                                                        pageType = "CELEBRITY";
                                                                                                        break;
                                                                                                    case 15:
                                                                                                        pageType = "NEWSPORTAL";
                                                                                                        break;
                                                                                                    case 20:
                                                                                                        pageType = "MUSIC PAGE";
                                                                                                        break;
                                                                                                    case 25:
                                                                                                        pageType = "BUSINESS PAGE";
                                                                                                        break;
                                                                                                    case 35:
                                                                                                        pageType = "MEDIA PAGE";
                                                                                                        break;
                                                                                                    case 30:
                                                                                                        pageType = "PRODUCT_PAGE";
                                                                                                        break;
                                                                                                    default:
                                                                                                        pageType = "" + pageDTO.getPageType();
                                                                                                        break;
                                                                                                }

                                                                                    %>
                                                                                    <tr>
                                                                                        <td class="text-center align-middle">
                                                                                            <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                                                                                <input name="selectedIDs" type="checkbox" class="checkboxes" value="<%=String.valueOf(pageDTO.getPageId())%>" />
                                                                                                <span></span>
                                                                                            </label>
                                                                                        </td>
                                                                                        <td class="text-center"><%=pageDTO.getPageId()%></td>
                                                                                        <td class="text-center"><%=pageDTO.getPageRingId()%></td>
                                                                                        <td class="text-center"><%=pageDTO.getPageOwner()%></td>
                                                                                        <td class="text-center"><%=pageDTO.getPageName()%></td>
                                                                                        <td class="text-center"><%=pageType%></td>
                                                                                        <td class="text-center"><%=pageActiveStatus%></td>
                                                                                        <td class="text-center"><%=autoFollow%></td>
                                                                                        <td class="text-center"><%=pageDTO.getIsPageVerified()%></td>
                                                                                        <td class="text-center"><%=pageDTO.getCountry()%></td>
                                                                                        <%if ((loginDTO.getPermissionLevel() == Constants.SUPER_ADMIN) || (loginDTO.getFeauterMapList().get(FEATURE_NAME).getHasModifyPermission() || loginDTO.getFeauterMapList().get(FEATURE_NAME).getHasDeletePermission())) {%>
                                                                                        <td class="text-center">
                                                                                            <%if ((loginDTO.getPermissionLevel() == Constants.SUPER_ADMIN) || (loginDTO.getFeauterMapList().get(FEATURE_NAME).getHasDeletePermission())) {%>
                                                                                            &nbsp;<a href="deleteAmbassador.do?ambassadors=<%=pageDTO.getPageId() + "&action=" + Constants.DELETE%>&productPageId=<bean:write name="AmbassadorForm" property="productPageId" />" class="" title="Delete" onclick="javascript:return confirm('Are you sure you want to delete this content')"><span class="fa fa-remove"></span></a>
                                                                                                <% } %>  
                                                                                        </td>
                                                                                        <% } %>      
                                                                                    </tr>
                                                                                    <%
                                                                                            }
                                                                                        }
                                                                                    %>
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                        <div class="row">
                                                                            <div class="col-md-5 col-sm-12">
                                                                                <div>
                                                                                    Showing <%=(STARTING_RECORD_NO + 1)%> to <%=ENDING_RECORD_NO%> of <%=TOTAL_RECORDS%> entries
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-7 col-sm-12">
                                                                                <div style="float: right;">
                                                                                    <div> 
                                                                                        Page <a href="javascript: submitform(2, <%=(CURRENT_PAGE_NO - 1)%>, this);" class="btn btn-sm default prev <%=disabledPrev%>"><i class="fa fa-angle-left"></i></a>
                                                                                        <input name="pageNo" class="form-control input-sm input-inline input-mini" maxlenght="5" style="text-align:center; margin: 0 5px;" type="text" value="<%=CURRENT_PAGE_NO%>" onchange="javascript: submitform(2, 0, this);">
                                                                                        <a href="javascript: submitform(2, <%=(CURRENT_PAGE_NO + 1)%>, this);" class="btn btn-sm default next <%=disabledNext%>"><i class="fa fa-angle-right"></i></a> of <span><%=TOTAL_PAGES%></span>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-actions">
                                                                        <div class="row">
                                                                            <div class="col-md-offset-3 col-md-9">
                                                                                <input type="submit" class="btn green" value="Remove Ambassador" onclick="submitform(6, 0, this)" formaction="deleteAmbassador.do?action=6"/>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </html:form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <%@include file="/shared/footer.jsp"%>
        </div>
        <%@include file="/shared/script-list.jsp"%>
    </body>
</html>
