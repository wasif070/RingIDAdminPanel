<%-- 
    Document   : UpdateAppConstants-new
    Created on : Jan 22, 2018, 3:09:36 PM
    Author     : mamun
--%>

<%@include file="/login/loginTimeExpiry.jsp"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.ringid.admin.utils.Constants"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@include  file="/shared/head.jsp" %>

<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Admin Panel | Edit App Constants</title>
        <%@include file="/shared/css-list.jsp"%>
    </head>
    <body class="page-container-bg-solid">
        <div class="page-wrapper">
            <div class="page-wrapper-row">
                <div class="page-wrapper-top">
                    <div class="page-header">
                        <%@include  file="/shared/menu.jsp" %>
                        <%
                            String strAppConstantsData = "";
                            if (request.getAttribute("appconstantsdata") != null && request.getAttribute("appconstantsdata").toString().length() > 0) {
                                strAppConstantsData = request.getAttribute("appconstantsdata").toString();
                            }
                        %>
                    </div>
                </div>
            </div>
            <div class="page-wrapper-row full-height">
                <div class="page-wrapper-middle">
                    <div class="page-container">
                        <div class="page-content-wrapper">
                            <div class="page-content">
                                <div class="container">
                                    <ul class="page-breadcrumb breadcrumb">
                                        <li>
                                            <a href="welcome.do">Admin Panel</a>
                                            <span style="font-size: 14px; margin: 0 auto;" class="fa fa-angle-right"></span>
                                        </li>
                                        <li>
                                            <a href="javascript:;">Common</a>
                                            <i style="font-size: 14px; margin: 0 auto;" class="fa fa-angle-right"></i>
                                        </li>
                                        <li>
                                            <a href="appConstantsListInfo.do">App Constants</a>
                                            <i style="font-size: 14px; margin: 0 auto;" class="fa fa-angle-right"></i>
                                        </li>
                                        <li>
                                            <span id="features" data-class-active=".common .appConstants">Edit App Constants</span>
                                        </li>
                                    </ul>
                                    <div class="page-content-inner">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="tabbable-line boxless tabbable-reversed">
                                                    <div class="tab-content" style="padding-top: 15px;">
                                                        <div class="portlet box green">
                                                            <div class="portlet-title">
                                                                <div class="caption">
                                                                    <i class="fa fa-pencil"></i> Edit App Constants Info </div>
                                                                <div class="tools">
                                                                    <a href="javascript:;" class="collapse"> </a>
                                                                </div>
                                                            </div>
                                                            <div class="portlet-body form">
                                                                <html:form action="updateAppConstants" styleClass="form-horizontal" method="POST"  acceptCharset="UTF-8">
                                                                    <% request.getSession(true).setAttribute("formType", Constants.UPDATE);%>
                                                                    <%@include file="appConstantsTable.jsp" %>
                                                                </html:form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <%@include file="/shared/footer.jsp"%>
        </div>
        <%@include file="/shared/script-list.jsp"%>
        <script type="text/javascript">
            var appconstantsdata =<%=strAppConstantsData%>;
            function getAppConstantsData(entryOfVal) {
                if (entryOfVal == "2" || parseInt(entryOfVal) == 2) {
                    $('select[name=platform]').val(appconstantsdata["pro"]["platform"]);
                    $('input[name=appType]').val(appconstantsdata["pro"]["appType"]);
                    $('input[name=version]').val(appconstantsdata["pro"]["version"]);
                    $('input[name=authController]').val(appconstantsdata["pro"]["authController"]);
                    $('input[name=imageServer]').val(appconstantsdata["pro"]["imageServer"]);
                    $('input[name=imageResource]').val(appconstantsdata["pro"]["imageResource"]);
                    $('input[name=displayDigits]').val(appconstantsdata["pro"]["displayDigits"]);
                    $('input[name=officialUserId]').val(appconstantsdata["pro"]["officialUserId"]);
                    $('input[name=marketServer]').val(appconstantsdata["pro"]["marketServer"]);
                    $('input[name=marketResource]').val(appconstantsdata["pro"]["marketResource"]);
                    $('input[name=vodServer]').val(appconstantsdata["pro"]["vodServer"]);
                    $('input[name=vodResource]').val(appconstantsdata["pro"]["vodResource"]);
                    $('input[name=web]').val(appconstantsdata["pro"]["web"]);
                    $('input[name=constantsVersion]').val(appconstantsdata["pro"]["constantsVersion"]);
                } else {
                    $('select[name=platform]').val(appconstantsdata["dev"]["platform"]);
                    $('input[name=appType]').val(appconstantsdata["dev"]["appType"]);
                    $('input[name=version]').val(appconstantsdata["dev"]["version"]);
                    $('input[name=authController]').val(appconstantsdata["dev"]["authController"]);
                    $('input[name=imageServer]').val(appconstantsdata["dev"]["imageServer"]);
                    $('input[name=imageResource]').val(appconstantsdata["dev"]["imageResource"]);
                    $('input[name=displayDigits]').val(appconstantsdata["dev"]["displayDigits"]);
                    $('input[name=officialUserId]').val(appconstantsdata["dev"]["officialUserId"]);
                    $('input[name=marketServer]').val(appconstantsdata["dev"]["marketServer"]);
                    $('input[name=marketResource]').val(appconstantsdata["dev"]["marketResource"]);
                    $('input[name=vodServer]').val(appconstantsdata["dev"]["vodServer"]);
                    $('input[name=vodResource]').val(appconstantsdata["dev"]["vodResource"]);
                    $('input[name=web]').val(appconstantsdata["dev"]["web"]);
                    $('input[name=constantsVersion]').val(appconstantsdata["dev"]["constantsVersion"]);
                }
            }
            getAppConstantsData($('select[name=entryOf]').val());
        </script>
    </body>
</html>
