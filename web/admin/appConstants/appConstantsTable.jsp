<%-- 
    Document   : m_appConstantsTable
    Created on : Feb 14, 2017, 12:09:51 PM
    Author     : ipvision
--%>

<div class="form-body">
    <div class="form-group">
        <label class="col-md-3 control-label">Platform</label>
        <div class="col-md-4">
            <html:select property="platform" styleClass="form-control"> 
                <html:option  value="1">PC</html:option>     
                <html:option  value="2">Android</html:option>  
                <html:option  value="3">iOS</html:option>
                <html:option  value="4">Windows Phone</html:option>
                <html:option  value="5">Web</html:option>
            </html:select>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label">App Type</label>
        <div class="col-md-4">
            <html:text property="appType" title="App Type" styleClass="form-control" />
            <html:messages id="appType" property="appType">
                <bean:write name="appType"  filter="false"/>
            </html:messages>

        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label">Version</label>
        <div class="col-md-4">
            <html:text property="version" title="Version" styleClass="form-control" />
            <html:messages id="version" property="version">
                <bean:write name="version"  filter="false"/>
            </html:messages>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label">Entry Of</label>
        <div class="col-md-4">
            <html:select property="entryOf" styleClass="form-control" onchange="javascript: getAppConstantsData(this.value);">
                <html:option  value="1">Dev</html:option>
                <html:option  value="2">Pro</html:option>
            </html:select>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label">Auth Controller</label>
        <div class="col-md-4">
            <html:text property="authController" title="Auth Controller" styleClass="form-control" />
            <html:messages id="authController" property="authController">
                <bean:write name="authController"  filter="false"/>
            </html:messages>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label">Image Server</label>
        <div class="col-md-4">
            <html:text property="imageServer" title="Image Server" styleClass="form-control" />
            <html:messages id="imageServer" property="imageServer">
                <bean:write name="imageServer"  filter="false"/>
            </html:messages>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label">Image Resource</label>
        <div class="col-md-4">
            <html:text property="imageResource" title="Image Resource" styleClass="form-control" />
            <html:messages id="imageResource" property="imageResource">
                <bean:write name="imageResource"  filter="false"/>
            </html:messages>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label">Display Digits</label>
        <div class="col-md-4">
            <html:text property="displayDigits" title="Display Digits" styleClass="form-control" />
            <html:messages id="displayDigits" property="displayDigits">
                <bean:write name="displayDigits"  filter="false"/>
            </html:messages>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label">Official User ID</label>
        <div class="col-md-4">
            <html:text property="officialUserId" title="Official User ID" styleClass="form-control" />
            <html:messages id="officialUserId" property="officialUserId">
                <bean:write name="officialUserId"  filter="false"/>
            </html:messages>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label">Market Server</label>
        <div class="col-md-4">
            <html:text property="marketServer" title="Market Server" styleClass="form-control" />
            <html:messages id="marketServer" property="marketServer">
                <bean:write name="marketServer"  filter="false"/>
            </html:messages>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label">Market Resource</label>
        <div class="col-md-4">
            <html:text property="marketResource" title="Market Resource" styleClass="form-control" />
            <html:messages id="marketResource" property="marketResource">
                <bean:write name="marketResource"  filter="false"/>
            </html:messages>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label">VOD Server</label>
        <div class="col-md-4">
            <html:text property="vodServer" title="VOD Server" styleClass="form-control" />
            <html:messages id="vodServer" property="vodServer">
                <bean:write name="vodServer"  filter="false"/>
            </html:messages>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label">VOD Resource</label>
        <div class="col-md-4">
            <html:text property="vodResource" title="VOD Resource" styleClass="form-control" />
            <html:messages id="vodResource" property="vodResource">
                <bean:write name="vodResource"  filter="false"/>
            </html:messages>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label">Web</label>
        <div class="col-md-4">
            <html:text property="web" title="Web" styleClass="form-control" />
            <html:messages id="web" property="web">
                <bean:write name="web"  filter="false"/>
            </html:messages>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label">Payment Gateway</label>
        <div class="col-md-4">
            <html:text property="paymentGateway" title="paymentGateway" styleClass="form-control" />
            <html:messages id="web" property="paymentGateway">
                <bean:write name="paymentGateway"  filter="false"/>
            </html:messages>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label">Constants Version</label>
        <div class="col-md-4">
            <html:text property="constantsVersion" title="constantsVersion" styleClass="form-control" />
            <html:messages id="web" property="constantsVersion">
                <bean:write name="constantsVersion"  filter="false"/>
            </html:messages>
        </div>
    </div>
    <div class="form-group last">
        <label class="col-md-3 control-label"></label>
        <div class="col-md-4">
            <%
                if (request.getSession(true).getAttribute("error") == "2") {
            %>
            <html:messages id="message" property="message" ><bean:write name="msg"  filter="false"/></html:messages>
            <bean:write name="AuthServerForm" property="message" filter="false"/>
            <%}%>
        </div>
    </div>
</div>
<div class="form-actions">
    <div class="row">
        <div class="col-md-offset-3 col-md-4">
            <button type="submit" class="btn green" name="saveButton">Submit</button>
            <a href="appConstantsListInfo.do" class="btn default">Cancel</a>
            <html:hidden property="id"/>
            <input type="hidden" name="action" value="<%=request.getSession(true).getAttribute("formType")%>" />
        </div>
    </div>
</div>
