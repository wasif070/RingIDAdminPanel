<%-- 
    Document   : m_appConstantsTable
    Created on : Feb 14, 2017, 12:09:51 PM
    Author     : rabby
--%>

<%@page import="com.ringid.admin.actions.userFeatureManagement.UserFeatureTaskScheduler"%>
<%@page import="com.ringid.admin.dto.adminAuth.UserRoleDTO"%>
<%@page import="java.util.ArrayList"%>
<div class="form-body">
    <div class="form-group">
        <label class="col-md-3 control-label">User Name <span style="color: red">*</span></label>
        <div class="col-md-4">
            <html:text property="name" title="User Name" styleClass="form-control" />
            <html:messages id="name" property="name">
                <bean:write name="name"  filter="false"/>
            </html:messages>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label">Password <span style="color: red">*</span></label>
        <div class="col-md-4">
            <html:password property="password" styleClass="form-control"  />
            <html:messages id="password" property="password">
                <bean:write name="password"  filter="false"/>
            </html:messages>

        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label">User Full Name <span style="color: red">*</span></label>
        <div class="col-md-4">
            <html:text property="fullname" title="User Full Name" styleClass="form-control" />
            <html:messages id="fullname" property="fullname">
                <bean:write name="fullname"  filter="false"/>
            </html:messages>

        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label">Permission Level</label>
        <div class="col-md-4">
            <%
                ArrayList<UserRoleDTO> userRoleList = UserFeatureTaskScheduler.getInstance().getUserRoleList(new UserRoleDTO());
            %>
            <html:select property="permissionLevel" styleClass="form-control">
                <!--                <html:option value="3">Guest</html:option>     
                                <html:option value="2">Admin</html:option>-->
                <%
                    for (UserRoleDTO dto : userRoleList) {
                %>
                <html:option value="<%=String.valueOf(dto.getId())%>"><%=dto.getName()%></html:option>
                    <%}%>
            </html:select>   
            <html:messages id="permissionLevel" property="permissionLevel">
                <bean:write name="permissionLevel"  filter="false"/>
            </html:messages>
        </div>
    </div>

</div>
<div class="form-actions">
    <div class="row">
        <div class="col-md-offset-3 col-md-4">
            <button type="submit" class="btn green" name="saveButton">Submit</button>
            <a href="usersListInfo.do" class="btn default">Cancel</a>
            <html:hidden property="id"/>
            <input type="hidden" name="action" value="<%=request.getSession(true).getAttribute("formType")%>" />
        </div>
    </div>
</div>
