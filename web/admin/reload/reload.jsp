<%-- 
    Document   : m_reload
    Created on : Feb 15, 2017, 2:24:52 PM
    Author     : MAMUN
--%>
<%@include file="../../login/loginTimeExpiry.jsp"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="java.util.*,com.ringid.admin.utils.Constants"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@include  file="/shared/head.jsp" %>

<!DOCTYPE html>
<html>
    <head>
        <title>Admin Panel | Reload</title>
        <%@include file="/shared/css-list.jsp"%>
    </head>
    <body class="page-container-bg-solid">
        <div class="page-wrapper">
            <div class="page-wrapper-row">
                <div class="page-wrapper-top">
                    <div class="page-header">
                        <%@include  file="../../shared/menu.jsp" %>
                        <%
                            Boolean success = null;
                            if (session.getAttribute("successMessage") != null) {
                                success = ((Boolean) session.getAttribute("successMessage"));
                                session.removeAttribute("successMessage");
                            }
                        %>
                    </div>
                </div>
            </div>
            <div class="page-wrapper-row full-height">
                <div class="page-wrapper-middle">
                    <div class="page-container">
                        <div class="page-content-wrapper">

                            <div class="page-content">
                                <div class="container">

                                    <ul class="page-breadcrumb breadcrumb">
                                        <li>
                                            <a href="welcome.do">Admin Panel</a>
                                            <span style="font-size: 14px; margin: 0 auto;" class="fa fa-angle-right"></span>
                                        </li>
                                        <li>
                                            <a href="javascript:;">Users</a>
                                            <span style="font-size: 14px; margin: 0 auto;" class="fa fa-angle-right"></span>
                                        </li>
                                        <li>
                                            <span id="features" data-class-active=".notify .reload">Reload</span>
                                        </li>
                                    </ul>

                                    <div class="page-content-inner">
                                        <%if (success != null && success == true) {%>
                                        <div id="prefix_661213402591"
                                             class="custom-alerts alert alert-success fade in">
                                            <button type="button" class="close" data-dismiss="alert"
                                                    aria-hidden="true"></button>
                                            <strong> successfully reloaded ! </strong>
                                        </div>
                                        <% } else if (success != null && success == false) { %>
                                        <div id="prefix_661213402591"
                                             class="custom-alerts alert alert-danger fade in">
                                            <button type="button" class="close" data-dismiss="alert"
                                                    aria-hidden="true"></button>
                                            <strong> failed ! </strong>
                                        </div>
                                        <% }%>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="tabbable-line boxless tabbable-reversed">
                                                    <div class="portlet box green">
                                                        <div class="portlet-title">
                                                            <div class="caption">
                                                                Force Reload</div>
                                                        </div>
                                                        <div class="portlet-body form">
                                                            <html:form action="/reload" method="POST" acceptCharset="UTF-8" styleClass="form-horizontal">
                                                                <div class="form-body">
                                                                    <div class="form-group">
                                                                        <label class="col-md-3 control-label">Force Reload:</label>
                                                                        <div class="col-md-4">
                                                                            <html:select property="reloadType" styleClass="form-control">
                                                                                <html:option value="all">All</html:option>
                                                                                <html:option value="breakingNews">Breaking News</html:option>
                                                                            </html:select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="form-actions">
                                                                    <div class="row">
                                                                        <div class="col-md-offset-3 col-md-4">
                                                                            <button type="submit" class="btn green" name="reload">Reload</button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </html:form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="page-wrapper-row">
                <div class="page-wrapper-bottom">
                    <div class="page-footer">
                        <div class="container"> Copyright &copy; 2015 All right reserved by
                            <a target="_blank" href="http://www.ipvision.ca/">IPvision canada inc</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <%@include file="/shared/script-list.jsp"%>
    </body>
</html>
