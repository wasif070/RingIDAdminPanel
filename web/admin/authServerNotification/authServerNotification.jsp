<%-- 
    Document   : authServerNotification-new
    Created on : Jan 22, 2018, 5:42:08 PM
    Author     : mamun
--%>

<%@page import="com.ringid.admin.actions.servers.authServers.AuthServerLoader"%>
<%@page import="com.ringid.admin.actions.servers.authServers.AuthServerDTO"%>
<%@include file="/login/loginTimeExpiry.jsp"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.ringid.admin.utils.Constants"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@include  file="/shared/head.jsp" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Admin Panel | Notify Auth Server</title>
        <%@include file="/shared/css-list.jsp"%>
    </head>
    <body class="page-container-bg-solid">
        <div class="page-wrapper">
            <div class="page-wrapper-row">
                <div class="page-wrapper-top">
                    <div class="page-header">
                        <%@include  file="/shared/menu.jsp" %>
                    </div>
                </div>
            </div>
            <div class="page-wrapper-row full-height">
                <div class="page-wrapper-middle">
                    <div class="page-container">
                        <div class="page-content-wrapper">
                            <div class="page-content">
                                <div class="container">
                                    <ul class="page-breadcrumb breadcrumb">
                                        <li>
                                            <a href="welcome.do">Admin Panel</a>
                                            <span style="font-size: 14px; margin: 0 auto;" class="fa fa-angle-right"></span>
                                        </li>
                                        <li>
                                            <a href="javascript:;">Notify</a>
                                            <i style="font-size: 14px; margin: 0 auto;" class="fa fa-angle-right"></i>
                                        </li>
                                        <li>
                                            <span id="features" data-class-active=".notify .notifyAuthServer">Notify Auth Server</span>
                                        </li>
                                    </ul>
                                    <div class="page-content-inner">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="tabbable-line boxless tabbable-reversed">
                                                    <div class="tab-content" style="padding-top: 15px;">
                                                        <div class="portlet box green">
                                                            <div class="portlet-title">
                                                                <div class="caption">
                                                                    <i class="fa fa-bell-o"></i> Notify Auth Server </div>
                                                                <div class="tools">
                                                                    <a href="javascript:;" class="collapse"> </a>
                                                                </div>
                                                            </div>
                                                            <div class="portlet-body form">
                                                                <html:form action="notifyAuthServer" styleClass="form-horizontal" method="POST"  acceptCharset="UTF-8">
                                                                    <div class="form-body">
                                                                        <div class="form-group">
                                                                            <label class="col-md-3 control-label"> Force : </label>
                                                                            <div class="col-md-4">
                                                                                <html:select property="notificationType" styleClass="form-control">
                                                                                    <html:option value="<%=String.valueOf(Constants.RELOAD_VOICE_SERVERS)%>">Reload Voice Servers</html:option>
                                                                                    <html:option value="<%=String.valueOf(Constants.RELOAD_CHAT_SERVERS)%>">Reload Chat Servers</html:option>
                                                                                    <html:option value="<%=String.valueOf(Constants.RELOAD_LOG_SETTINGS)%>">Reload Log Settings</html:option>
                                                                                    <html:option value="<%=String.valueOf(Constants.RELOAD_SETTINGS)%>">Reload Settings</html:option>
                                                                                    <html:option value="<%=String.valueOf(Constants.RELOAD_AUTH_SERVERS)%>">Reload Auth Servers</html:option>
                                                                                    <%--<html:option value="<%=String.valueOf(Constants.RELOAD_RELAY_SERVERS)%>">RELOAD RELAY SERVERS</html:option>--%>
                                                                                    <html:option value="<%=String.valueOf(Constants.RELOAD_SELF_SETTINGS)%>">Reload Self Settings</html:option>
                                                                                    <%--<html:option value="<%=String.valueOf(Constants.RELOAD_USER_REPOSITORY)%>"> RELOAD USER REPOSITORY </html:option>--%>
                                                                                    <html:option value="<%=String.valueOf(Constants.RELOAD_FLOATING_MENU)%>">Reload Floating Menu</html:option>
                                                                                    <html:option value="<%=String.valueOf(Constants.RELOAD_BREAKING_FEEDS)%>">Reload Breaking Feeds</html:option>
                                                                                    <html:option value="<%=String.valueOf(Constants.RELOAD_PAGE_TYPES)%>">Reload Page Type</html:option>
                                                                                    <html:option value="<%=String.valueOf(Constants.RELOAD_PAGE_CATEGORIES)%>">Reload Page Categories</html:option>
                                                                                    <html:option value="<%=String.valueOf(Constants.RELOAD_OFFERS)%>">Reload Offers</html:option>
                                                                                    <html:option value="<%=String.valueOf(Constants.RELOAD_WALLET_PERMITTED_COUNTRY_LIST)%>">Reload Wallet Permitted Country List</html:option>
                                                                                    <html:option value="<%=String.valueOf(Constants.RELOAD_RINGSTUDIO_SETTINGS)%>">Reload Ring Studio Settings</html:option>
                                                                                    <html:option value="<%=String.valueOf(Constants.RELOAD_CELEBRITY_ROOM_DETAILS)%>">Reload Celebrity Room Details</html:option>
                                                                                    <html:option value="<%=String.valueOf(Constants.RELOAD_WALLET_FLEXI_BLACKLISTED_USERS)%>">Reload Wallet Flexi Blacklisted Users</html:option>
                                                                                    <html:option value="<%=String.valueOf(Constants.RELOAD_RINGSTORE_SETTINGS)%>">Reload Ringstore Settings</html:option>
                                                                                    <html:option value="<%=String.valueOf(Constants.RELOAD_HOME_DONATION_PAGE)%>">Reload Home Donation Page</html:option>
                                                                                    <html:option value="<%=String.valueOf(Constants.RELOAD_STREAMING_CODEC_SETTINGS)%>">Reload Streaming Codec Settings</html:option>
                                                                                    <html:option value="<%=String.valueOf(Constants.RESET_STATIC_FEEDBACK_TIME)%>">Reset Static Feedback Time</html:option>
                                                                                    <html:option value="<%=String.valueOf(Constants.RELOAD_SPORTS_SERVER_DATA)%>">Reload Sports Server Data</html:option>
                                                                                    <%--<html:option value="<%=String.valueOf(Constants.RELOAD_SPORTS_MATCH_LIST)%>"> RELOAD SPORTS MATCH LIST </html:option>--%>
                                                                                    <html:option value="<%=String.valueOf(Constants.RESET_SQL_EXCEPTION_COUNTER)%>">Reset SQL Exception Counter</html:option>
                                                                                    <html:option value="<%=String.valueOf(Constants.RELOAD_COIN_SERVER_DATA)%>">Reload Coin Server Data</html:option>
                                                                                    <html:option value="<%=String.valueOf(Constants.RELOAD_CHANNEL_CATEGORY)%>">Reload Channel Category</html:option>
                                                                                    <html:option value="<%=String.valueOf(Constants.RELOAD_LIVE_TO_CHANNEL)%>">Reload Live to Channel</html:option>
                                                                                    <html:option value="<%=String.valueOf(Constants.RELOAD_RINGBIT_SETTINGS)%>">Reload Ringbit Settings</html:option>
                                                                                    <html:option value="<%=String.valueOf(Constants.RELOAD_RINGID_DISTRIBUTOR)%>">Reload RingID Distributor</html:option>
                                                                                    <html:option value="<%=String.valueOf(Constants.RELOAD_DYNAMIC_MENU)%>">Reload Dynamic Menu</html:option>
                                                                                    <html:option value="<%=String.valueOf(Constants.RELOAD_SIGNUP_REWARD_SETTINGS)%>">Reload Signup Reward Settings</html:option>
                                                                                    <html:option value="<%=String.valueOf(Constants.RELOAD_SPAM_REASONS)%>">Reload Spam Reasons</html:option>
                                                                                    <html:option value="<%=String.valueOf(Constants.RELOAD_FEATURE_MANAGER)%>">Reload Feature Manager</html:option>
                                                                                    <html:option value="<%=String.valueOf(Constants.RELOAD_FIREBASE_IP_PORT)%>">Reload Firebase IP Port</html:option>
                                                                                    <html:option value="<%=String.valueOf(Constants.RELOAD_FIREBASE_COUNTRY_LIST)%>">Reload Firebase Country List</html:option>
                                                                                    <html:option value="<%=String.valueOf(Constants.RELOAD_SERVER_MESSAGES)%>">Reload Server Messages</html:option>
                                                                                    <html:option value="<%=String.valueOf(Constants.RELOAD_AD_SERVER_DATA)%>">Reload Ad Data</html:option>
                                                                                    <html:option value="<%=String.valueOf(Constants.RELOAD_STREAMING_SERVERS)%>">Reload Streaming Servers</html:option>
                                                                                    <html:option value="<%=String.valueOf(Constants.RELOAD_MANUAL_RECORDED_LIVES)%>">Reload Manual Recorded Lives</html:option>
                                                                                    <html:option value="<%=String.valueOf(Constants.RELOAD_USER_WALLETS)%>">Reload User Wallets</html:option>
                                                                                    <html:option value="<%=String.valueOf(Constants.RELOAD_STICKER_REPO)%>">Reload Sticker Repo</html:option>
                                                                                    <html:option value="<%=String.valueOf(Constants.RELOAD_STREAM_ADMINS_REPO)%>">Reload Stream Admins Repo</html:option>
                                                                                    <html:option value="<%=String.valueOf(Constants.RELOAD_APP_UPDATE_SETTINGS)%>">Reload App Update Settings</html:option>
                                                                                    <html:option value="<%=String.valueOf(Constants.FORCE_RELOAD_HOME_MESSAGE)%>">Reload Home Message</html:option>
                                                                                    <html:option value="<%=String.valueOf(Constants.FORCE_RELOAD_APP_UPDATE_URL)%>">Reload App Update Url</html:option>
                                                                                    <html:option value="<%=String.valueOf(Constants.SYNC_DUMMY_FOLLOWERS)%>">Sync Dummy Followers</html:option>
                                                                                </html:select>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label class="col-md-3 control-label">Auth Server IP</label>
                                                                            <div class="col-md-4">
                                                                                <%
                                                                                    ArrayList<AuthServerDTO> authServerList = AuthServerLoader.getInstance().getAuthServerList(new AuthServerDTO());
                                                                                %>
                                                                                <html:select property="id" styleClass="form-control">
                                                                                    <html:option value="">-- ALL --</html:option>
                                                                                    <%for (AuthServerDTO dto : authServerList) {%>
                                                                                    <html:option value="<%=String.valueOf(dto.getId())%>"><%=dto.getId()%>-<%=dto.getServerIP()%>:<%=dto.getStartPort()%></html:option>
                                                                                    <%}%>
                                                                                </html:select>
                                                                                <%--<html:text property="serverIP" styleId="serverIP" disabled="true" readonly="true" value=" --All--" styleClass="form-control"/>--%>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label class="col-md-3 control-label">Auth Server Response:</label>
                                                                            <div class="col-md-4">
                                                                                <html:text property="serverMessage" styleId="serverMessage" readonly="true" styleClass="form-control"/>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-actions">
                                                                        <div class="row">
                                                                            <div class="col-md-offset-3 col-md-9">
                                                                                <button type="submit" class="btn green" name="notify">Notify</button>
                                                                                <a href="checkLog.do" class="btn default">Check Log</a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </html:form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <%@include file="/shared/footer.jsp"%>
        </div>
        <%@include file="/shared/script-list.jsp"%>
    </body>
</html>