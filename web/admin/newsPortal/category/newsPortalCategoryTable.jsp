<%-- 
    Document   : m_appConstantsTable
    Created on : Feb 14, 2017, 12:09:51 PM
    Author     : rabby
--%>

<div class="form-body">
    <div class="form-group">
        <label class="col-md-3 control-label">Category Name <span style="color: red">*</span></label>
        <div class="col-md-4">
            <html:text property="name" title="Category Name" styleClass="form-control" />
            <html:messages id="name" property="name">
                <bean:write name="name"  filter="false"/>
            </html:messages>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label">Status</label>
        <div class="col-md-4">
            <html:select property="status" styleClass="form-control"> 
                <html:option  value="1">Active</html:option>     
                <html:option  value="0">Inactive</html:option>   
            </html:select>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label">Category Type</label>
        <div class="col-md-4">
            <html:select styleClass="form-control" property="type">
                <html:option value="15">News Portal Page</html:option>
                <html:option value="25">Business Page</html:option>
                <html:option value="20">Music Page</html:option>  
                <html:option value="7">Sports Page</html:option>
                <html:option value="30">Product Page</html:option>
                <html:option value="35">Media Page</html:option>
            </html:select>
        </div>
    </div>
    <div class="form-group last">
        <label class="col-md-3 control-label"></label>
        <div class="col-md-4">
            <%
                if (request.getSession(true).getAttribute("error") == "2") {
            %>  
            <html:messages id="message" property="message"><bean:write name="msg"  filter="false"/></html:messages>
            <bean:write name="NewsPortalCategoryForm" property="message" filter="false"/>
            <%}%>
        </div>
    </div>
</div>
<div class="form-actions">
    <div class="row">
        <div class="col-md-offset-3 col-md-4">
            <button type="submit" class="btn green" name="saveButton">Submit</button>
            <a href="newsPortalCategoryListInfo.do" class="btn default">Cancel</a>
            <html:hidden property="id"/>
            <html:hidden property="pageId"/>
            <input type="hidden" name="action" value="<%=request.getSession(true).getAttribute("formType")%>" />
        </div>
    </div>
</div>
