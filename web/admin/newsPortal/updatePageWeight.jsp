<%-- 
    Document   : updatePageWeight
    Created on : Nov 14, 2017, 7:08:46 PM
    Author     : mamun
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="com.ringid.admin.projectMenu.MenuNames"%>
<%@include file="/login/loginTimeExpiry.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@include  file="/shared/head.jsp" %>

<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Admin Panel | Edit Page Weight</title>
        <%@include file="/shared/css-list.jsp"%>
    </head>
    <body class="page-container-bg-solid">
        <div class="page-wrapper">
            <div class="page-wrapper-row">
                <div class="page-wrapper-top">
                    <div class="page-header">
                        <%@include  file="/shared/menu.jsp" %>
                        <%
                            String FEATURE_NAME = MenuNames.PAGES;
                            String msg = "";
                            if (request.getAttribute("message") != null && request.getAttribute("message").toString().length() > 0) {
                                msg = request.getAttribute("message").toString();
                            }
                        %>
                    </div>
                </div>
            </div>
            <div class="page-wrapper-row full-height">
                <div class="page-wrapper-middle">
                    <div class="page-container">
                        <div class="page-content-wrapper">
                            <div class="page-content">
                                <div class="container">
                                    <ul class="page-breadcrumb breadcrumb">
                                        <li>
                                            <a href="welcome.do">Admin Panel</a>
                                            <span style="font-size: 14px; margin: 0 auto;" class="fa fa-angle-right"></span>
                                        </li>
                                        <li>
                                            <a href="javascript:;">Page & Media</a>
                                            <span style="font-size: 14px; margin: 0 auto;" class="fa fa-angle-right"></span>
                                        </li>
                                        <li>
                                            <a href="newsPortalListInfo.do">Pages</a>
                                            <span style="font-size: 14px; margin: 0 auto;" class="fa fa-angle-right"></span>
                                        </li>
                                        <li>
                                            <span id="features" data-class-active=".page-o-media .pages">Edit Page Weight</span>
                                        </li>
                                    </ul>
                                    <%if (msg.length() > 0) {%>
                                    <div id="prefix_661213402591" class="custom-alerts alert alert-success fade in">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                        <strong> <%=msg%> </strong>
                                    </div>
                                    <% }%>
                                    <div class="page-content-inner">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="tabbable-line boxless tabbable-reversed">
                                                    <div class="tab-content" style="padding-top: 15px;">
                                                        <div class="portlet box green">
                                                            <div class="portlet-title">
                                                                <div class="caption">
                                                                    <i class="fa fa-pencil"></i>Edit Page Weight
                                                                </div>
                                                                <div class="tools">
                                                                    <a href="javascript:;" class="collapse"> </a>
                                                                </div>
                                                            </div>
                                                            <div class="portlet-body form">
                                                                <html:form action="/updatePageWeight" method="POST" acceptCharset="UTF-8" styleClass="form-horizontal" enctype="multipart/form-data">
                                                                    <div class="form-body">
                                                                        <div class="form-group">
                                                                            <label class="col-md-3 control-label">Page</label>
                                                                            <div class="col-md-4">
                                                                                <html:hidden property="pageId" />
                                                                                <html:hidden property="action" />
                                                                                <html:hidden property="pageType" />
                                                                                <html:hidden property="discoverable" />
                                                                                <label class="control-label"> <bean:write name="NewsPortalForm" property="pageName" /> </label>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label class="col-md-3 control-label">Weight <span class="required">*</span></label>
                                                                            <div class="col-md-4">
                                                                                <html:text property="weight" title="weight" styleClass="form-control" />
                                                                                <html:messages id="weight" property="weight">
                                                                                    <bean:write name="weight"  filter="false"/>
                                                                                </html:messages>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <logic:equal name="NewsPortalForm" property="pageType" value="30">
                                                                        <div class="form-group ">
                                                                            <label class="control-label col-md-3">
                                                                                Profile Image
                                                                                <span class="required" aria-required="true"> * </span>
                                                                            </label>
                                                                            <div class="col-md-9">
                                                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                                    <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px; line-height: 150px;">
                                                                                        <img src="<%=Constants.IMAGE_BASE_URL%>${NewsPortalForm.profileImageUrl}" />
                                                                                    </div>
                                                                                    <div>
                                                                                        <span class="btn green btn-outline btn-file">
                                                                                            <span class="fileinput-new"> Select image </span>
                                                                                            <span class="fileinput-exists"> Change image </span>
                                                                                            <html:file property="theFileProfileImage" onchange="removeSpanText()" />
                                                                                        </span>
                                                                                        <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                                                        <span id="msgSpan" style="display: inline-block;">
                                                                                            <html:messages id="theFileProfileImage" property="theFileProfileImage">
                                                                                                <bean:write name="theFileProfileImage"  filter="false"/>
                                                                                            </html:messages>
                                                                                        </span>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group ">
                                                                            <label class="control-label col-md-3">Cover Image<span style="color: red">*</span></label>
                                                                            <div class="col-md-9">
                                                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                                    <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px; line-height: 150px;">
                                                                                        <img src="<%=Constants.IMAGE_BASE_URL%>${NewsPortalForm.coverImageUrl}" />
                                                                                    </div>
                                                                                    <div>
                                                                                        <span class="btn green btn-outline btn-file">
                                                                                            <span class="fileinput-new"> Select image </span>
                                                                                            <span class="fileinput-exists"> Change image </span>
                                                                                            <html:file property="theFileCoverImage" onchange="removeSpanText()" />
                                                                                        </span>
                                                                                        <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                                                        <span id="msgSpan" style="display: inline-block;">
                                                                                            <html:messages id="theFileCoverImage" property="theFileCoverImage">
                                                                                                <bean:write name="theFileCoverImage"  filter="false"/>
                                                                                            </html:messages>
                                                                                        </span>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </logic:equal>
                                                                    <div class="form-actions">
                                                                        <div class="row">
                                                                            <div class="col-md-offset-3 col-md-9">
                                                                                <button type="submit" class="btn green" name="saveButton">Submit</button>
                                                                                <a href="newsPortalListInfo.do?pageType=<bean:write name="NewsPortalForm" property="pageType" />&discoverable=<bean:write name="NewsPortalForm" property="discoverable" />" class="btn default">Cancel</a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </html:form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <%@include file="/shared/footer.jsp"%>
        </div>
        <%@include file="/shared/script-list.jsp"%>
    </body>
</html>
