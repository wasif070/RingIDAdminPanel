<%-- 
    Document   : m_newsPortalList
    Created on : Feb 18, 2017, 10:42:42 AM
    Author     : MAMUN
--%>
<%@page import="com.ringid.admin.dto.newsportal.NewsPortalDTO"%>
<%@page import="com.ringid.admin.projectMenu.MenuNames"%>
<%@include file= "/login/loginTimeExpiry.jsp"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="java.util.*,com.ringid.admin.utils.Constants"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@include  file="/shared/head.jsp" %>

<!DOCTYPE html>
<html>
    <head>
        <title>Admin Panel | Page Information List</title>
        <%@include file="/shared/css-list.jsp"%>
    </head>
    <body class="page-container-bg-solid">
        <div class="page-wrapper">
            <div class="page-wrapper-row">
                <div class="page-wrapper-top">
                    <div class="page-header">
                        <%@include  file="/shared/menu.jsp" %>
                        <%
                            String FEATURE_NAME = MenuNames.PAGES;
                            int TOTAL_RECORDS = 0, TOTAL_PAGES = 0, CURRENT_PAGE_NO = 0, STARTING_RECORD_NO = 0, ENDING_RECORD_NO = 0;
                            String disabledPrev = "", disabledNext = "";
                            if (request.getAttribute(Constants.TOTAL_RECORDS) != null) {
                                TOTAL_RECORDS = Integer.valueOf(request.getAttribute(Constants.TOTAL_RECORDS).toString());
                            }
                            if (request.getAttribute(Constants.TOTAL_PAGES) != null) {
                                TOTAL_PAGES = Integer.valueOf(request.getAttribute(Constants.TOTAL_PAGES).toString());
                            }
                            if (request.getAttribute(Constants.CURRENT_PAGE_NO) != null) {
                                CURRENT_PAGE_NO = Integer.valueOf(request.getAttribute(Constants.CURRENT_PAGE_NO).toString());
                            }
                            if (request.getAttribute(Constants.STARTING_RECORD_NO) != null) {
                                STARTING_RECORD_NO = Integer.valueOf(request.getAttribute(Constants.STARTING_RECORD_NO).toString());
                            }
                            if (request.getAttribute(Constants.ENDING_RECORD_NO) != null) {
                                ENDING_RECORD_NO = Integer.valueOf(request.getAttribute(Constants.ENDING_RECORD_NO).toString());
                            }
                            if (STARTING_RECORD_NO <= 0) {
                                disabledPrev = "disabled";
                                STARTING_RECORD_NO = 0;
                            }
                            if (ENDING_RECORD_NO >= TOTAL_RECORDS) {
                                disabledNext = "disabled";
                                ENDING_RECORD_NO = TOTAL_RECORDS;
                            }
                        %>
                    </div>
                </div>
            </div>
            <div class="page-wrapper-row full-height">
                <div class="page-wrapper-middle">
                    <div class="page-container">
                        <div class="page-content-wrapper">

                            <div class="page-content">
                                <div class="container">

                                    <ul class="page-breadcrumb breadcrumb">
                                        <li>
                                            <a href="welcome.do">Admin Panel</a>
                                            <span style="font-size: 14px; margin: 0 auto;" class="fa fa-angle-right"></span>
                                        </li>
                                        <li>
                                            <a href="javascript:;">Page & Media</a>
                                            <span style="font-size: 14px; margin: 0 auto;" class="fa fa-angle-right"></span>
                                        </li>
                                        <li>
                                            <a href="newsPortalListInfo.do">Pages</a>
                                            <span style="font-size: 14px; margin: 0 auto;" class="fa fa-angle-right"></span>
                                        </li>
                                        <li>
                                            <span id="features" data-class-active=".page-o-media .pages">Manage Pages</span>
                                        </li>
                                    </ul>
                                    <div class="page-content-inner">
                                        <logic:messagesPresent message="false">
                                            <html:messages id="errormsg" message="false">
                                                <logic:present name="errormsg">
                                                    <div id="prefix_661213402591" class="custom-alerts alert alert-success fade in">
                                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                        <strong> <bean:write name="errormsg" filter="false" /> </strong>
                                                    </div>
                                                </logic:present>
                                            </html:messages>
                                        </logic:messagesPresent>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="tabbable-line boxless tabbable-reversed">
                                                    <div class="tab-content" style="padding-top: 15px;">
                                                        <div class="portlet box green">
                                                            <div class="portlet-title">
                                                                <div class="caption">
                                                                    <i class="fa fa-list-ul"></i> Page List Info
                                                                </div>
                                                                <div class="tools">
                                                                    <a href="javascript:;" class="collapse"> </a>

                                                                    <a data-original-title="Reload" href="newsPortalListInfo.do?ForceReload=1" style="width: 13px; background-image: url(assets/global/img/portlet-reload-icon-white.png);" class="tooltips"> </a>
                                                                </div>
                                                            </div>
                                                            <div class="portlet-body form">
                                                                <html:form action="/newsPortalListInfo" method="POST" styleClass="form-horizontal" acceptCharset="UTF-8" >
                                                                    <div class="form-body">
                                                                        <div class="form-group">
                                                                            <div class="col-md-12 col-sm-12">
                                                                                <label>
                                                                                    <html:select property="pageType" styleClass="bs-select form-control input-inline">
                                                                                        <html:option  value="-1">--Page Type--</html:option>
                                                                                        <html:option value="15">News Portal Page</html:option>
                                                                                        <html:option value="25">Business Page</html:option>
                                                                                        <html:option value="20">Music Page</html:option>  
                                                                                        <html:option value="7">Sports Page</html:option>
                                                                                        <html:option value="30">Product Page</html:option>
                                                                                        <html:option value="35">Media Page</html:option>  
                                                                                    </html:select>
                                                                                </label>
                                                                                <label>
                                                                                    <html:select property="discoverable" styleClass="bs-select form-control input-inline">
                                                                                        <html:option  value="-1">--Search Type</html:option>
                                                                                            <optgroup label="Discoverable">
                                                                                            <html:option  value="0">Both</html:option>
                                                                                            <html:option  value="1">True</html:option> 
                                                                                            <html:option  value="2">False</html:option>
                                                                                            </optgroup>
                                                                                            <optgroup label="Featured">
                                                                                            <html:option  value="3">Featured</html:option>
                                                                                            </optgroup>
                                                                                    </html:select>
                                                                                </label>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <div class="col-md-6 col-sm-6">
                                                                                <label>
                                                                                    <html:select styleClass="bs-select form-control input-sm input-xsmall input-inline" property="recordPerPage" onchange="javascript: submitform(1, 1, this);">
                                                                                        <html:option value="10">10</html:option>
                                                                                        <html:option value="25">25</html:option>
                                                                                        <html:option value="50">50</html:option>
                                                                                        <html:option value="100">100</html:option>
                                                                                        <html:option value="200">200</html:option>
                                                                                    </html:select>
                                                                                    records</label>
                                                                            </div>
                                                                            <div class="col-md-6 col-sm-6">
                                                                                <div class="input-group" style="float: right;">
                                                                                    <input name="searchText" type="text" value="<bean:write name="NewsPortalForm" property="searchText"/>" class="form-control input-sm input-small" placeholder="pageName">
                                                                                    <span>
                                                                                        <button class="btn green-soft uppercase bold input-sm" type="submit"><i class="icon-magnifier"></i></button>
                                                                                        <input name="action" type="hidden" value="<%=Constants.SEARCH%>" />
                                                                                        <html:hidden property="column" />
                                                                                        <html:hidden property="sort" />
                                                                                    </span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="table-scrollable">
                                                                            <table class="table table-striped table-bordered table-hover">
                                                                                <thead>
                                                                                    <tr>
                                                                                        <th class="text-center align-middle">
                                                                                            <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                                                                                <input type="checkbox" onchange="checkAll(this)" class="group-checkable">
                                                                                                <span></span>
                                                                                            </label>
                                                                                        </th>
                                                                                        <th id="col_2" class="text-center align-middle sortable">
                                                                                            PageId
                                                                                        </th>
                                                                                        <th class="text-center align-middle">
                                                                                            Page Name
                                                                                        </th>
                                                                                        <th id="col_1" class="text-center align-middle sortable">
                                                                                            Weight
                                                                                        </th>
                                                                                        <logic:lessThan name="NewsPortalForm" property="discoverable" value="3">
                                                                                            <th class="text-center align-middle">
                                                                                                Discoverable
                                                                                            </th>
                                                                                        </logic:lessThan>
                                                                                        <logic:equal name="NewsPortalForm" property="discoverable" value="3">
                                                                                            <th class="text-center align-middle">
                                                                                                OwnerId
                                                                                            </th>
                                                                                            <th class="text-center align-middle">
                                                                                                PageStatus
                                                                                            </th>
                                                                                            <th class="text-center align-middle">
                                                                                                AutoFollowStatus
                                                                                            </th>
                                                                                            <th class="text-center align-middle">
                                                                                                Country
                                                                                            </th>
                                                                                        </logic:equal>
                                                                                        <%if ((loginDTO.getPermissionLevel() == Constants.SUPER_ADMIN) || (loginDTO.getFeauterMapList().get(FEATURE_NAME).getHasModifyPermission())) { %>
                                                                                        <th class="text-center align-middle">Action</th>
                                                                                    </tr><%}%>
                                                                                </thead>
                                                                                <tbody>
                                                                                    <%
                                                                                        List<NewsPortalDTO> data1 = (List<NewsPortalDTO>) session.getAttribute(Constants.DATA_ROWS);
                                                                                        session.removeAttribute(Constants.DATA_ROWS);
                                                                                        if (data1 != null && data1.size() > 0) {
                                                                                            String userStatus = "";
                                                                                            for (int i = STARTING_RECORD_NO; i < ENDING_RECORD_NO; i++) {
                                                                                                NewsPortalDTO dto = (NewsPortalDTO) data1.get(i);
                                                                                                switch (dto.getPageActiveStatus()) {
                                                                                                    case 0:
                                                                                                        userStatus = "Active";
                                                                                                        break;
                                                                                                    case 1:
                                                                                                        userStatus = "Deactive";
                                                                                                        break;
                                                                                                    case 2:
                                                                                                        userStatus = "Closed";
                                                                                                        break;
                                                                                                    default:
                                                                                                        userStatus = "" + dto.getPageActiveStatus();
                                                                                                        break;
                                                                                                }
                                                                                    %>
                                                                                    <tr>
                                                                                        <td  class="text-center align-middle">
                                                                                            <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                                                                                <input type="checkbox" class="checkboxes" name="selectedIDs" value="<%=dto.getPageId()%>" />
                                                                                                <span></span>
                                                                                            </label>
                                                                                        </td>
                                                                                        <td class="text-center align-middle"><%=dto.getPageId()%></td>
                                                                                        <td class="text-center align-middle"><%=dto.getPageName()%></td>
                                                                                        <td class="text-center align-middle"><%=dto.getWeight()%></td>
                                                                                        <logic:lessThan name="NewsPortalForm" property="discoverable" value="3">
                                                                                            <% if (dto.getDiscoverable() == 1) {%>
                                                                                            <td class="text-center align-middle"> &#10004; </td>
                                                                                            <% } else { %>
                                                                                            <td class="text-center align-middle"> &nbsp; </td>
                                                                                            <%}%>
                                                                                        </logic:lessThan>
                                                                                        <logic:equal name="NewsPortalForm" property="discoverable" value="3">
                                                                                            <td class="text-center align-middle"> <%=dto.getPageOwnerId()%> </td>
                                                                                            <td class="text-center align-middle"> <%=userStatus%> </td>
                                                                                            <%if (dto.isIsAutoFollowDisabled()) {%>
                                                                                            <td class="text-center align-middle"> <a href="autoFollowSetting.do?pageId=<%=dto.getPageId()%>&autoFollowStatus=1&goback=newsportal&discoverable=3&pageType=<bean:write name='NewsPortalForm' property='pageType' />" class="btn btn-sm green" title="click to enable auto follow"><span class="fa fa-play"></span> Disabled </a> </td>
                                                                                            <% } else {%>
                                                                                            <td class="text-center align-middle"> <a href="autoFollowSetting.do?pageId=<%=dto.getPageId()%>&autoFollowStatus=0&goback=newsportal&discoverable=3&pageType=<bean:write name='NewsPortalForm' property='pageType' />" class="btn btn-sm green" title="click to disable auto follow"><span class="fa fa-stop"></span> Enabled </a> </td> </td>
                                                                                            <% }%>
                                                                                            <td class="text-center align-middle"> <%=dto.getCountryName()%> </td>
                                                                                            <%if ((loginDTO.getPermissionLevel() == Constants.SUPER_ADMIN) || (loginDTO.getFeauterMapList().get(FEATURE_NAME).getHasModifyPermission())) {%>
                                                                                            <td class="text-center align-middle">
                                                                                                <a href="editPageWeight.do?pageId=<%=dto.getPageId()%>&pageType=<bean:write name='NewsPortalForm' property='pageType' />&pageName=<%=dto.getPageName()%>&weight=<%=dto.getWeight()%>&action=4&discoverable=3" class="" title="Edit Page Weight"><span class="fa fa-edit"></span></a>
                                                                                                &nbsp;<a href="editPageFollowRules.do?pageId=<%=dto.getPageId()%>&pageType=<bean:write name='NewsPortalForm' property='pageType' />&discoverable=3&goback=newsportal" class="btn btn-sm green" title="Edit Page Rules"><span class="fa fa-edit"></span> Page Rules</a>
                                                                                            </td>
                                                                                            <%}%>
                                                                                        </logic:equal>
                                                                                        <logic:lessThan name="NewsPortalForm" property="discoverable" value="3">
                                                                                            <%if ((loginDTO.getPermissionLevel() == Constants.SUPER_ADMIN) || (loginDTO.getFeauterMapList().get(FEATURE_NAME).getHasModifyPermission())) {%>
                                                                                            <td class="text-center align-middle">
                                                                                                <a href="editPageWeight.do?pageId=<%=dto.getPageId()%>&pageType=<bean:write name='NewsPortalForm' property='pageType' />&pageName=<%=dto.getPageName()%>&weight=<%=dto.getWeight()%>&action=4&discoverable=<bean:write name='NewsPortalForm' property='discoverable' />" class="" title="Edit Page Weight"><span class="fa fa-edit"></span></a>
                                                                                            </td>
                                                                                            <%}%>
                                                                                        </logic:lessThan>
                                                                                    </tr>
                                                                                    <%
                                                                                            }
                                                                                        }
                                                                                    %>
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                        <div class="row">
                                                                            <div class="col-md-5 col-sm-12">
                                                                                <div>
                                                                                    Showing <%=(STARTING_RECORD_NO + 1)%> to <%=ENDING_RECORD_NO%> of <%=TOTAL_RECORDS%> entries
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-7 col-sm-12">
                                                                                <div style="float: right;">
                                                                                    <div> 
                                                                                        Page <a href="javascript: submitform(2, <%=(CURRENT_PAGE_NO - 1)%>, this);" class="btn btn-sm default prev <%=disabledPrev%>"><i class="fa fa-angle-left"></i></a>
                                                                                        <input name="pageNo" class="form-control input-sm input-inline input-mini" maxlenght="5" style="text-align:center; margin: 0 5px;" type="text" value="<%=CURRENT_PAGE_NO%>" onchange="javascript: submitform(2, 0, this);">
                                                                                        <a href="javascript: submitform(2, <%=(CURRENT_PAGE_NO + 1)%>, this);" class="btn btn-sm default next <%=disabledNext%>"><i class="fa fa-angle-right"></i></a> of <span><%=TOTAL_PAGES%></span>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-actions">
                                                                        <div class="row">
                                                                            <div class="col-md-offset-3 col-md-9">
                                                                                <logic:notEqual name="NewsPortalForm" property="discoverable" value="1">
                                                                                    <input type="submit" value="Make Discoverable" class="btn green" formaction="makeDiscoverable.do?submitType=Make Discoverable" />
                                                                                </logic:notEqual>
                                                                                <logic:notEqual name="NewsPortalForm" property="discoverable" value="2">
                                                                                    &nbsp;<input type="submit" value="Make Not Discoverable" class="btn green" formaction="makeDiscoverable.do?submitType=Make Not Discoverable" />
                                                                                </logic:notEqual>
                                                                                <logic:notEqual name="NewsPortalForm" property="discoverable" value="3">
                                                                                    &nbsp;<input type="submit" value="Make Featured" class="btn green" formaction="makeDiscoverable.do?submitType=Make Featured" />
                                                                                </logic:notEqual>
                                                                                <logic:equal name="NewsPortalForm" property="discoverable" value="3">
                                                                                    &nbsp;<input type="submit" value="Make Unfeatured" class="btn green" formaction="makeDiscoverable.do?submitType=Make Unfeatured" />
                                                                                </logic:equal>
                                                                            </div>
                                                                        </div>
                                                                        <input type="hidden" name="action" value="7" />
                                                                        <html:hidden property="pageName" />
                                                                        <html:hidden property="pageType" />
                                                                        <html:hidden property="discoverable" />
                                                                    </div>
                                                                </html:form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <%@include file="/shared/footer.jsp"%>
        </div>
        <%@include file="/shared/script-list.jsp"%>
    </body>
</html>
