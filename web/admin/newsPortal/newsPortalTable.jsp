<%-- 
    Document   : newsPortalTable
    Created on : Jun 10, 2017, 3:02:05 PM
    Author     : Rabby
--%>

<%@page import="com.ringid.admin.utils.Constants"%>
<%@page import="java.util.List"%>
<%
    int action = (Integer) request.getSession(true).getAttribute("formType");
%>
<div class="form-body">
    <div class="form-group">
        <label class="col-md-3 control-label">Page Name <span style="color: red">*</span></label>
        <div class="col-md-4">
            <html:text property="pageName" title="Page Name" styleClass="form-control" />
            <html:messages id="name" property="pageName">
                <bean:write name="pageName"  filter="false"/>
            </html:messages>
        </div>
    </div>
    <%if (!(action == Constants.UPDATE)) {%>
    <div class="form-group">
        <label class="col-md-3 control-label">Page Type <span style="color: red">*</span></label>
        <div class="col-md-4">
            <html:select property="pageType" styleClass="form-control"> 
                <html:option  value="3">User Page</html:option>
                <html:option  value="7">Sports Page</html:option>  
            </html:select>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label">Owner RingId <span style="color: red">*</span></label>
        <div class="col-md-4">
            <html:text property="ownerId" title="Owner RingId" styleClass="form-control" />
            <html:messages id="name" property="ownerId">
                <bean:write name="ownerId"  filter="false"/>
            </html:messages>
        </div>
    </div>
    <%}%>
    <div class="form-group">
        <%
            List<com.ringid.country.CountryDTO> countryDTOs = com.ringid.country.CountryLoader.getInstance().getCountryList();
        %>
        <label class="col-md-3 control-label">Country <span style="color: red">*</span></label>
        <div class="col-md-4">
            <html:select property="country" styleClass="form-control"> 
                <html:option  value=""> Select Country </html:option>     
                    <%
                        for (com.ringid.country.CountryDTO dto : countryDTOs) {
                    %>
                <html:option value="<%=dto.getName()%>"><%=dto.getName()%></html:option>
                    <% }%>  
            </html:select>
        </div>
    </div>
    <div class="form-group ">
        <label class="control-label col-md-3">
            Profile Image
            <span class="required" aria-required="true"> * </span>
        </label>
        <div class="col-md-9">
            <div class="fileinput fileinput-new" data-provides="fileinput">
                <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px; line-height: 150px;">
                    <img src="<%=Constants.IMAGE_BASE_URL%>${NewsPortalForm.profileImageUrl}" />
                </div>
                <div>
                    <span class="btn green btn-outline btn-file">
                        <span class="fileinput-new"> Select image </span>
                        <span class="fileinput-exists"> Change image </span>
                        <html:file property="theFileProfileImage" onchange="removeSpanText()" />
                    </span>
                    <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                    <span id="msgSpan" style="display: inline-block;">
                        <html:messages id="theFileProfileImage" property="theFileProfileImage">
                            <bean:write name="theFileProfileImage"  filter="false"/>
                        </html:messages>
                    </span>
                </div>
            </div>
        </div>
    </div>
    <div class="form-group ">
        <label class="control-label col-md-3">Cover Image<span style="color: red">*</span></label>
        <div class="col-md-9">
            <div class="fileinput fileinput-new" data-provides="fileinput">
                <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px; line-height: 150px;">
                    <img src="<%=Constants.IMAGE_BASE_URL%>${NewsPortalForm.coverImageUrl}" />
                </div>
                <div>
                    <span class="btn green btn-outline btn-file">
                        <span class="fileinput-new"> Select image </span>
                        <span class="fileinput-exists"> Change image </span>
                        <html:file property="theFileCoverImage" onchange="removeSpanText()" />
                    </span>
                    <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                    <span id="msgSpan" style="display: inline-block;">
                        <html:messages id="theFileCoverImage" property="theFileCoverImage">
                            <bean:write name="theFileCoverImage"  filter="false"/>
                        </html:messages>
                    </span>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="form-group last">
    <label class="col-md-3 control-label"></label>
    <div class="col-md-4">
        <%
            if (request.getSession(true).getAttribute("error") == "2") {
        %>  
        <html:messages id="message" property="message"><bean:write name="msg"  filter="false"/></html:messages>
        <bean:write name="NewsPortalForm" property="message" filter="false"/>
        <%}%>
    </div>
</div>
<div class="form-actions">
    <div class="row">
        <div class="col-md-offset-3 col-md-4">
            <button type="submit" class="btn green" name="saveButton">Submit</button>
            <a href="newsPortalListInfo.do" class="btn default">Cancel</a>
            <html:hidden property="id"/>
            <html:hidden property="pageId"/>
            <html:hidden property="pageRingId"/>
            <%if (action == Constants.UPDATE) {%>
            <html:hidden property="ownerId"/>
            <html:hidden property="pageType"/>
            <%}%>
            <input type="hidden" name="action" value="<%=request.getSession(true).getAttribute("formType")%>" />
        </div>
    </div>
</div>
<script type="text/javascript">
    function removeSpanText() {
        document.getElementById("msgSpan").innerHTML = "";
    }
</script>

