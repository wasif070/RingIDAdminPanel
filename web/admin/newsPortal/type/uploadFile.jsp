<%@include file="/login/loginTimeExpiry.jsp"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@include  file="/shared/head.jsp" %>

<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Admin Panel | Feed Type File Upload</title>
        <%@include file="/shared/css-list.jsp"%>
    </head>
    <body class="page-container-bg-solid">
        <div class="page-wrapper">
            <div class="page-wrapper-row">
                <div class="page-wrapper-top">
                    <div class="page-header">
                        <%@include  file="/shared/menu.jsp" %>
                    </div>
                </div>
            </div>
            <div class="page-wrapper-row full-height">
                <div class="page-wrapper-middle">
                    <div class="page-container">
                        <div class="page-content-wrapper">
                            <div class="page-content">
                                <div class="container">
                                    <ul class="page-breadcrumb breadcrumb">
                                        <li>
                                            <a href="welcome.do">Admin Panel</a>
                                            <span style="font-size: 14px; margin: 0 auto;" class="fa fa-angle-right"></span>
                                        </li>
                                        <li>
                                            <a href="javascript:;">Page & Media</a>
                                            <span style="font-size: 14px; margin: 0 auto;" class="fa fa-angle-right"></span>
                                        </li>
                                        <li>
                                            <a href="newsPortalTypeListInfo.do">Feed Type</a>
                                            <span style="font-size: 14px; margin: 0 auto;" class="fa fa-angle-right"></span>
                                        </li>
                                        <li>
                                            <span id="features" data-class-active=".page-o-media .feedType">Upload Feed Type</span>
                                        </li>
                                    </ul>
                                    <div class="page-content-inner">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="tabbable-line boxless tabbable-reversed">
                                                    <div class="portlet box green">
                                                        <div class="portlet-title">
                                                            <div class="caption">
                                                                Upload cvs format example
                                                            </div>
                                                        </div>
                                                        <div class="portlet-body form">
                                                            <html:form action="/uploadNewsPortalTypeFile" styleClass="form-horizontal form-bordered" method="POST"  acceptCharset="UTF-8" enctype="multipart/form-data">
                                                                <div class="form-body" >
                                                                    <div class="form-group">
                                                                        <table class="table table-striped table-bordered" id="pageableDataTables">
                                                                            <thead>
                                                                                <tr>
                                                                                    <th style="text-align: center;">Name</th>
                                                                                    <th style="text-align: center;">Status</th>
                                                                                </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td style="text-align: center;">Current Affairs</td>
                                                                                    <td style="text-align: center;">1</td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label class="control-label col-md-3">File Name</label>
                                                                        <div class="col-md-9">
                                                                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                                <span class="btn green btn-file">
                                                                                    <span class="fileinput-new"><i class="fa fa-upload"></i> Select file </span>
                                                                                    <span class="fileinput-exists"><i class="fa fa-upload"></i> Change </span>
                                                                                    <html:file property="theFile" accept=".csv" onchange="removeSpanText()"/>
                                                                                </span>
                                                                                <span class="fileinput-filename"> </span> &nbsp;
                                                                                <a href="javascript:;" class="close fileinput-exists" data-dismiss="fileinput"> </a>
                                                                                <span id="msgSpan" style="display: inline-block;"><html:errors/> </span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="form-actions">
                                                                    <div class="row">
                                                                        <div class="col-md-offset-3 col-md-9">
                                                                            <button type="submit" class="btn green">
                                                                                <i class="fa fa-check"></i>Upload File
                                                                            </button>
                                                                            <a href="newsPortalTypeListInfo.do" class="btn default">Cancel</a>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </html:form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="page-wrapper-row">
                <div class="page-wrapper-bottom">
                    <div class="page-footer">
                        <div class="container"> Copyright &copy; 2015 All right reserved by
                            <a target="_blank" href="http://www.ipvision.ca/">IPvision canada inc</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <%@include file="/shared/script-list.jsp"%>
    </body>
</html>
