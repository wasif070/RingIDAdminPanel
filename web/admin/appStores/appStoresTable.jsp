<%-- 
    Document   : m_appStoresTable
    Created on : Feb 14, 2017, 2:25:48 PM
    Author     : ipvision
--%>

<div class="form-body">
    <div class="form-group">
        <label class="col-md-3 control-label">Device Type</label>
        <div class="col-md-4">
            <html:select property="deviceType" styleClass="form-control"> 
                <html:option  value="1">PC</html:option>     
                <html:option  value="2">Android</html:option>  
                <html:option  value="3">iOS</html:option>
                <html:option  value="4">Windows Phone</html:option>
                <html:option  value="5">Web</html:option>
            </html:select>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label">Version <span style="color: red">*</span></label>
        <div class="col-md-4">
            <html:text property="version" title="VOD Server" styleClass="form-control" />
            <html:messages id="version" property="version">
                <bean:write name="version"  filter="false"/>
            </html:messages>
        </div>
    </div>
    <div class="form-group last">
        <label class="col-md-3 control-label"></label>
        <div class="col-md-4">
            <%
                if (request.getSession(true).getAttribute("error") == "2") {
            %>
            <td> <html:messages id="message" property="message"><bean:write name="msg"  filter="false"/></html:messages>
            <bean:write name="AuthServerForm" property="message" filter="false"/></td>
            <%}%>
        </div>
    </div>
</div>
<div class="form-actions">
    <div class="row">
        <div class="col-md-offset-3 col-md-4">
            <button type="submit" class="btn green" name="saveButton">Submit</button>
            <a href="appStoresListInfo.do" class="btn default">Cancel</a>
            <html:hidden property="id"/>
            <input type="hidden" name="action" value="<%=request.getSession(true).getAttribute("formType")%>" />
        </div>
    </div>
</div>
