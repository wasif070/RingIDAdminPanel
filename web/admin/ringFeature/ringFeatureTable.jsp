<%-- 
    Document   : featureTable
    Created on : Jan 28, 2018, 4:50:04 PM
    Author     : Rabby
--%>

<div class="form-body">
    <div class="form-group">
        <label class="col-md-3 control-label">Feature Name<span style="color: red">*</span></label>
        <div class="col-md-4">
            <html:text property="featureName" title="Feature Name" styleClass="form-control" />
            <html:messages id="newId" property="featureName">
                <bean:write name="featureName"  filter="false"/>
            </html:messages>
        </div>
    </div>
</div>
<div class="form-actions">
    <div class="row">
        <div class="col-md-offset-3 col-md-4">
            <button type="submit" class="btn green" name="saveButton">Submit</button>
            <a href="showRingFeatureList.do" class="btn default">Cancel</a>
            <html:hidden property="id"/>
            <input type="hidden" name="action" value="<%=request.getSession(true).getAttribute("formType")%>" />
        </div>
    </div>
</div>
