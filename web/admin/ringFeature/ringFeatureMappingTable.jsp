<%-- 
    Document   : featureMappingTable
    Created on : Jan 28, 2018, 4:51:01 PM
    Author     : Rabby
--%>

<%@page import="com.ringid.admin.service.RingFeatureService"%>
<%@page import="com.ringid.admin.dto.RingFeatureDTO"%>
<%@page import="java.util.List"%>
<%@page import="com.ringid.admin.utils.Constants"%>
<%
    int[] versions = null;
    if (request.getAttribute("versions") != null) {
        versions = (int[]) request.getAttribute("versions");
    }
%>
<div class="form-body">
    <div class="form-group">
        <label class="col-md-3 control-label">Feature Name:<span style="color: red">*</span></label>
        <div class="col-md-4">
            <%if (versions == null) {%>
            <html:select property="id" styleClass="form-control">
                <%
                    List<RingFeatureDTO> features = RingFeatureService.getInstance().getFeatureList(new RingFeatureDTO());
                    for (RingFeatureDTO dto : features) {
                %>
                <html:option  value="<%=String.valueOf(dto.getId())%>"><%=dto.getFeatureName()%></html:option> 
                    <%}%>
            </html:select>
            <%} else {%>
            <label class="col-md-3 control-label"><bean:write name="RingFeatureForm" property="featureName"/></label>
            <%}%>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label">App Type:<span style="color: red">*</span></label>
        <div class="col-md-4">
            <%if (versions == null) {%>
            <html:select property="appType" styleClass="form-control">
                <%for (int i = 1; i < Constants.APP_TYPE.length; i++) {%>
                <html:option  value="<%=String.valueOf(i)%>"><%=Constants.APP_TYPE[i]%></html:option> 
                    <%}%>
            </html:select>
            <%} else {%>
            <label class="col-md-3 control-label"><bean:write name="RingFeatureForm" property="appTypeName"/></label>
            <%}%>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label">Platform Info:</label>
        <div class="col-md-4">
            <table class="table table-striped table-bordered table-hover">
                <thead>
                    <tr>
                        <th class="text-center">Platform</th>
                        <th class="text-center">N/A</th>
                        <th class="text-center">Minimum Version</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="text-center">PC</td>
                        <td class="text-center">
                            <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                <input type="checkbox" name="pcVersionChk" <%if (!(versions != null && versions[0] >= 0)) {%>checked="true" value="false"<%} else {%>value="true"<%}%> onchange="disableCheckboxsMethod(0)"/>
                                <span></span>
                            </label>
                        </td>
                        <td class="text-center"><input type="text" name="pcVersion" class="form-control" <%if (!(versions != null && versions[0] >= 0)) {%>disabled="true"<%} else {%>value="<%=versions[0]%>"<%}%> /></td>
                    </tr>
                    <tr>
                        <td class="text-center">Android</td>
                        <td class="text-center">
                            <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                <input type="checkbox" name="androidVersionChk" <%if (!(versions != null && versions[1] >= 0)) {%>checked="true" value="false"<%} else {%>value="true"<%}%> onchange="disableCheckboxsMethod(1)"/>
                                <span></span>
                            </label>
                        </td>
                        <td class="text-center"><input type="text" name="androidVersion" class="form-control" <%if (!(versions != null && versions[1] >= 0)) {%>disabled="true"<%} else {%>value="<%=versions[1]%>"<%}%> /></td>
                    </tr>
                    <tr>
                        <td class="text-center">iPhone</td>
                        <td class="text-center">
                            <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                <input type="checkbox" name="iosVersionChk" <%if (!(versions != null && versions[2] >= 0)) {%>checked="true" value="false"<%} else {%>value="true"<%}%> onchange="disableCheckboxsMethod(2)"/>
                                <span></span>
                            </label>
                        </td>
                        <td class="text-center"><input type="text" name="iosVersion" class="form-control" <%if (!(versions != null && versions[2] >= 0)) {%>disabled="true"<%} else {%>value="<%=versions[2]%>"<%}%> /></td>
                    </tr>
                    <tr>
                        <td class="text-center">Windows</td>
                        <td class="text-center">
                            <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                <input type="checkbox" name="windowsVersionChk" <%if (!(versions != null && versions[3] >= 0)) {%>checked="true" value="false"<%} else {%>value="true"<%}%> onchange="disableCheckboxsMethod(3)"/>
                                <span></span>
                            </label>
                        </td>
                        <td class="text-center"><input type="text" name="windowsVersion" class="form-control" <%if (!(versions != null && versions[3] >= 0)) {%>disabled="true"<%} else {%>value="<%=versions[3]%>"<%}%> /></td>
                    </tr>
                    <tr>
                        <td class="text-center">Web</td>
                        <td class="text-center">
                            <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                <input type="checkbox" name="webVersionChk" <%if (!(versions != null && versions[4] >= 0)) {%>checked="true" value="false"<%} else {%>value="true"<%}%> onchange="disableCheckboxsMethod(4)"/>
                                <span></span>
                            </label>
                        </td>
                        <td class="text-center"><input type="text" name="webVersion" class="form-control" <%if (!(versions != null && versions[4] >= 0)) {%>disabled="true"<%} else {%>value="<%=versions[4]%>"<%}%> /></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="form-actions">
    <div class="row">
        <div class="col-md-offset-3 col-md-4">
            <button type="submit" class="btn green" name="saveButton">Submit</button>
            <a href="showRingFeatureMappingList.do" class="btn default">Cancel</a>
            <html:hidden property="id"/>
            <html:hidden property="appType"/>
            <input type="hidden" name="action" value="<%=request.getSession(true).getAttribute("formType")%>" />
        </div>
    </div>
</div>
<script type="text/javascript">
    function disableCheckboxsMethod(index) {
        var disableCheckbox;
        var minVersion;
        switch (index) {
            case 0:
                disableCheckbox = document.getElementsByName("pcVersionChk")[0];
                minVersion = document.getElementsByName("pcVersion")[0];
                break;
            case 1:
                disableCheckbox = document.getElementsByName("androidVersionChk")[0];
                minVersion = document.getElementsByName("androidVersion")[0];
                break;
            case 2:
                disableCheckbox = document.getElementsByName("iosVersionChk")[0];
                minVersion = document.getElementsByName("iosVersion")[0];
                break;
            case 3:
                disableCheckbox = document.getElementsByName("windowsVersionChk")[0];
                minVersion = document.getElementsByName("windowsVersion")[0];
                break;
            case 4:
                disableCheckbox = document.getElementsByName("webVersionChk")[0];
                minVersion = document.getElementsByName("webVersion")[0];
                break;
        }
        if (disableCheckbox.checked) {
            console.log("checked" + index);
            minVersion.setAttribute("disabled", true);
        } else {
            console.log("unchecked" + index);
            minVersion.removeAttribute("disabled");
        }
    }
</script>
