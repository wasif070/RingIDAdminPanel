<%-- 
    Document   : m_settingTable
    Created on : Feb 14, 2017, 4:21:36 PM
    Author     : ipvision
--%>
<%@page import="com.ringid.admin.actions.settings.DataType"%>
<%@page import="com.ringid.admin.dto.adminAuth.LoginDTO"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ page import="java.util.*,com.ringid.admin.utils.Constants"%>
<div class="form-body">
    <c:choose>
        <c:when test="${SettingForm.getName() == 'home-page-banner-type-seq'}">
            <div class="form-group" id="sortable_portlets">
                <label class="col-md-3 control-label">Settings Value</label>
                <div class="col-md-4 column sortable">
                    <c:forEach var="item" items="${SettingForm.getSettingDTOs()}">
                        <div class="portlet portlet-sortable box white" style="border: 1px solid #ededed">
                            <div class="portlet-title" style="color: black;">
                                <label style="margin-top: 10px;"><input name="selectedIDs" type="checkbox" class="icheck" value="${item.id}" <c:if test="${item.status == true}"> checked </c:if> /> ${item.settingsValue} </label>
                                </div>
                            </div>
                    </c:forEach>
                </div>
            </div>
        </c:when>
        <c:when test="${SettingForm.getName() == 'call-verification-enabled-country-dialing-codes' or SettingForm.getName() == 'marketplace-enabled-country-dialing-codes'}">
            <div class="form-group" id="sortable_portlets">
                <label class="col-md-3 control-label">Settings Value</label>
                <div class="col-md-4 column sortable" style="overflow-y: scroll; max-height: 400px; border: 1px solid #ededed;">
                    <c:forEach var="item" items="${SettingForm.getSettingDTOs()}">
                        <div class="portlet portlet-sortable box white" style="border: 1px solid #ededed; margin-top: 10px;">
                            <div class="portlet-title" style="color: black;">
                                <label style="margin-top: 10px;"><input name="selectedIDs" type="checkbox" class="icheck" value="${item.settingsValue}" <c:if test="${item.status == true}"> checked </c:if> /> ${item.name} </label>
                                </div>
                            </div>
                    </c:forEach>
                </div>
            </div>
        </c:when>
        <c:otherwise>
            <div class="form-group">
                <label class="col-md-3 control-label">Settings Value</label>
                <div class="col-md-4">
                    <html:text property="settingsValue" title="Settings Value" styleClass="form-control" />
                    <html:messages id="settingsValue" property="settingsValue">
                        <bean:write name="settingsValue"  filter="false"/>
                    </html:messages>
                </div>
            </div>
        </c:otherwise>
    </c:choose>
    <%LoginDTO loginDTOInSettingTablePage = (LoginDTO) request.getSession(true).getAttribute(com.ringid.admin.utils.Constants.LOGIN_DTO);%>
    <%if (loginDTOInSettingTablePage.getPermissionLevel() == Constants.SUPER_ADMIN) {%>
    <div style="border-top: 1px solid #dae2ea;margin-top: 25px;margin-bottom: 25px;"></div>
    <div class="form-group">
        <label class="col-md-3 control-label">Setting Name</label>
        <div class="col-md-4">
            <html:text property="name" title="Setting Name" styleClass="form-control" />
            <html:messages id="name" property="name">
                <bean:write name="name"  filter="false"/>
            </html:messages>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label">Data Type</label>
        <div class="col-md-4">
            <html:select property="dataType" styleClass="form-control">
                <%
                    ArrayList<Integer> types = DataType.getDataTypes();
                    for (int i = 0; i < types.size(); i++) {
                        Integer type = types.get(i);
                %>
                <html:option value="<%=String.valueOf(type)%>"><%=DataType.getDataTypeToName(type)%></html:option>     
                    <%
                        }
                    %>
            </html:select>
        </div>
    </div>
    <div class="form-group" >
        <label class="col-md-3 control-label">Permissions</label>
        <div class="col-md-9">
            <div class="input-group">
                <div class="icheck-list">
                    <c:forEach items="${SettingForm.getUserRoles()}" var="userRole">
                        <label>
                            <input name="permittedRoleIds" type="checkbox" class="icheck" value="${userRole.getId()}" <c:if test="${SettingForm.getPermittedRoles().contains(userRole.getId())}"> checked </c:if>/> ${userRole.getName()}
                            </label>
                    </c:forEach>
                </div>
            </div>
        </div>
    </div>
    <%} else {%>
    <html:hidden property="name" />
    <html:hidden property="dataType" />
    <% }%>
</div>
<div class="form-actions">
    <div class="row">
        <div class="col-md-offset-3 col-md-4">
            <button type="submit" class="btn green" name="saveButton">Submit</button>
            <a href="settingListInfo.do?searchText=<bean:write name='SettingForm' property='searchText' />" class="btn default">Cancel</a>
            <html:hidden property="id"/>
            <html:hidden property="searchText"/>
            <input type="hidden" name="action" value="<%=request.getSession(true).getAttribute("formType")%>" />
        </div>
    </div>
</div>