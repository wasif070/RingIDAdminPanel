<%-- 
    Document   : m_settingList
    Created on : Feb 14, 2017, 4:04:59 PM
    Author     : ipvision
--%>
<%@include file="/login/loginTimeExpiry.jsp"%>
<%@page import="com.ringid.admin.settings.SettingTaskScheduler"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="java.util.*,com.ringid.admin.utils.Constants,com.ringid.admin.settings.*"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@include  file="/shared/head.jsp" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Admin Panel | Setting List Table</title>
        <%@include file="/shared/css-list.jsp"%>s
    </head>
    <body class="page-container-bg-solid">
        <div class="page-wrapper">
            <div class="page-wrapper-row">
                <div class="page-wrapper-top">
                    <div class="page-header">
                        <%@include  file="/shared/menu.jsp" %>
                    </div>
                </div>
            </div>
            <div class="page-wrapper-row full-height">
                <div class="page-wrapper-middle">
                    <div class="page-container">
                        <div class="page-content-wrapper">
                            <div class="page-content">
                                <div class="container">
                                    <ul class="page-breadcrumb breadcrumb">
                                        <li>
                                            <a href="welcome.do">Admin Panel</a>
                                            <span style="font-size: 14px; margin: 0 auto;" class="fa fa-angle-right"></span>
                                        </li>
                                        <li>
                                            <a href="javascript:;">Common</a>
                                            <span style="font-size: 14px; margin: 0 auto;" class="fa fa-angle-right"></span>
                                        </li>
                                        <li>
                                            <a href="settingListInfo.do">Settings</a>
                                            <i style="font-size: 14px; margin: 0 auto;" class="fa fa-angle-right"></i>
                                        </li>
                                        <li>
                                            <span id="features" data-class-active=".common .settings">Edit Settings List Info</span>
                                        </li>
                                    </ul>
                                    <div class="page-content-inner">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="tabbable-line boxless tabbable-reversed">
                                                    <div class="tab-content" style="padding-top: 15px;">
                                                        <div class="portlet box green">
                                                            <div class="portlet-title">
                                                                <div class="caption">
                                                                    <i class="fa fa-pencil"></i> Edit Settings List  Info </div>
                                                                <div class="tools">
                                                                    <a href="javascript:;" class="collapse"> </a>
                                                                </div>
                                                            </div>
                                                            <div class="portlet-body form">
                                                                <html:form action="updateListSettingInfo" styleClass="form-horizontal" method="POST"   acceptCharset="UTF-8">
                                                                    <div class="form-body" >
                                                                        <div class="table-scrollable">
                                                                            <table class="table table-striped table-bordered table-hover">
                                                                                <thead>
                                                                                    <tr>
                                                                                        <th class="text-center">Name</th>
                                                                                        <th class="text-center">Settings Value</th>
                                                                                        <th class="text-center">Data Type</th>
                                                                                    </tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                    <logic:notEmpty name ="SettingListForm" property="listItems">
                                                                                        <logic:iterate id="listItems" name ="SettingListForm" property="listItems">
                                                                                            <tr>
                                                                                                <td class="text-center">
                                                                                                    <html:hidden name ="listItems" property="id" indexed="true"/>
                                                                                                    <html:text   name="listItems" property="name"  title="Name" styleClass="form-control" indexed="true" />
                                                                                                </td>
                                                                                                <td class="text-center">
                                                                                                    <html:text name="listItems" property="settingsValue"  title="Settings Value" styleClass="form-control" indexed="true" />
                                                                                                </td>
                                                                                                <td class="text-center">
                                                                                                    <html:select name="listItems" property="dataType" title="Data Type" styleClass="form-control" indexed="true">
                                                                                                        <%
                                                                                                            ArrayList<Integer> types = DataType.getDataTypes();
                                                                                                            for (int i = 0; i < types.size(); i++) {
                                                                                                                Integer type = types.get(i);
                                                                                                        %>
                                                                                                        <html:option value="<%=String.valueOf(type)%>"><%=DataType.getDataTypeToName(type)%></html:option>     
                                                                                                        <%
                                                                                                            }
                                                                                                        %>
                                                                                                    </html:select>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </logic:iterate>
                                                                                    </logic:notEmpty>
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-actions">
                                                                        <div class="row">
                                                                            <div class="col-md-offset-3 col-md-9">
                                                                                <html:submit styleClass="btn green" property="submitType" value="Save ALL" />
                                                                                <a href="settingListInfo.do" class="btn default">Cancel</a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </html:form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <%@include file="/shared/footer.jsp"%>
        </div>
        <%@include file="/shared/script-list.jsp"%>
    </body>
</html>
