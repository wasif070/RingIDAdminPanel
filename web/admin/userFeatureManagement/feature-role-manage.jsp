<%-- 
    Document   : UpdateFeature
    Created on : Apr 3, 2017, 3:15:31 PM
    Author     : Rabby
--%>

<%@page import="com.ringid.admin.actions.userFeatureManagement.userRole.UserRoleLoader"%>
<%@page import="com.ringid.admin.dto.adminAuth.UserRoleDTO"%>
<%@include file="/login/loginTimeExpiry.jsp"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ page import="java.util.*,com.ringid.admin.utils.Constants"%>
<%@include  file="/shared/head.jsp" %>
<%    String treeview;
    treeview = "";
    treeview = request.getAttribute("treeData").toString();
    int roleId = Integer.parseInt(request.getAttribute("roleId").toString());
%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Admin Panel | Feature Permission</title>
        <%@include file="/shared/css-list.jsp"%>
    </head>
    <body class="page-container-bg-solid">
        <div class="page-wrapper">
            <div class="page-wrapper-row">
                <div class="page-wrapper-top">
                    <div class="page-header">
                        <%@include  file="/shared/menu.jsp" %>
                    </div>
                </div>
            </div>
            <div class="page-wrapper-row full-height">
                <div class="page-wrapper-middle">
                    <div class="page-container">
                        <div class="page-content-wrapper">
                            <div class="page-content">
                                <div class="container">
                                    <ul class="page-breadcrumb breadcrumb">
                                        <li>
                                            <a href="welcome.do">Admin Panel</a>
                                            <span style="font-size: 14px; margin: 0 auto;" class="fa fa-angle-right"></span>
                                        </li>
                                        <li>
                                            <a href="showFeatureRole.do">Feature Role Mapping</a>
                                            <span style="font-size: 14px; margin: 0 auto;" class="fa fa-angle-right"></span>
                                        </li>
                                        <li>
                                            <span id="features"  data-class-active=".featureRoleMapping">Manage Feature Role Mapping</span>
                                        </li>
                                    </ul>
                                    <div class="page-content-inner">
                                        <!-- BEGIN PAGE CONTENT-->
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="tabbable-line boxless tabbable-reversed">
                                                    <div class="tab-content" style="padding-top: 15px;">
                                                        <div class="portlet box green">
                                                            <div class="portlet-title">
                                                                <div class="caption">
                                                                    <i class="icon-settings"></i> Feature Role Mapping </div>
                                                                <div class="tools">
                                                                    <a href="javascript:;" class="collapse"> </a>
                                                                    <a data-original-title="Reload" href="showFeatureRole.do?ForceReload=1" style="width: 13px; background-image: url(assets/global/img/portlet-reload-icon-white.png);" class="tooltips"> </a>
                                                                </div>
                                                            </div>
                                                            <div class="portlet-body form">
                                                                <html:form action="showFeatureRole" method="POST" acceptCharset="UTF-8" styleClass="form-horizontal">
                                                                    <input type="hidden" id="ids" value="[]" name="featureIds"/>
                                                                    <input type="hidden" id="operation" name="operation"/>
                                                                    <div class="form-body" >
                                                                        <div class="form-group">
                                                                            <div class="col-md-offset-4 col-md-4">
                                                                                <%
                                                                                    ArrayList<UserRoleDTO> dtoList = UserRoleLoader.getInstance().getUserRoleList(new UserRoleDTO());
                                                                                %>
                                                                                <html:select property="roleId" styleClass="form-control" onchange="submitform(1,1,this)"> 
                                                                                    <%for (UserRoleDTO dto : dtoList) {%>
                                                                                    <html:option  value="<%=String.valueOf(dto.getId())%>"><%=dto.getName()%></html:option>
                                                                                    <%}%>
                                                                                </html:select>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <div class="col-md-offset-3 col-md-6">
                                                                                <div id="tree" class="tree-demo"> </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-actions">
                                                                        <div class="row">
                                                                            <div class="col-md-offset-5 col-md-9">
                                                                                <button type="submit" class="btn green" name="saveButton" onclick="onclick2()">Submit</button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </html:form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <%@include file="/shared/footer.jsp"%>
        </div>
        <%@include file="/shared/script-list.jsp"%>
        <script type="text/javascript">
            $(document).ready(function () {
                $('#tree').jstree({
                    'plugins': ["wholerow", "checkbox", "types"],
                    'core': {
                        "themes": {
                            "responsive": false
                        },
                        'data': <%=treeview%>
                    },
                    "types": {
                        "default": {
                            "icon": false
                        }
                    }
                });
            });

            function onclick2() {
                var selectedElmsIds2 = $('#tree').jstree("get_selected", true);
                var value = [];
                var json = "[";
                for (var i = 0; i < selectedElmsIds2.length; i++) {
                    json += "{";
                    json += "\"id\": " + selectedElmsIds2[i].id + ",";
                    json += "\"text\": \"" + selectedElmsIds2[i].text + "\",";
                    var parent = selectedElmsIds2[i].parent;
                    json += "\"parent\": " + (parent == "#" ? 0 : parent) + ",";
                    json += "\"permissionLevel\": " + selectedElmsIds2[i]["state"]["selected"];
                    value[i] = selectedElmsIds2[i].id;
                    json += "}";
                    if (i != selectedElmsIds2.length - 1) {
                        json += ",";
                    }
                }
                json += "]";
                document.getElementById("ids").value = json;
                document.getElementById("operation").value = "update_feature";
            }
            function submitform(page, action, iobj)
            {
                document.forms[0].submit();
            }
        </script>
    </body>
</html>