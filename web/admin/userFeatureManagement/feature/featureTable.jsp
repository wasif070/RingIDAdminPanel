<%-- 
    Document   : featureTable
    Created on : Apr 3, 2017, 3:15:48 PM
    Author     : Rabby
--%>

<%@page import="com.ringid.admin.actions.userFeatureManagement.feature.FeatureLoader"%>
<%@page import="com.ringid.admin.dto.adminAuth.FeatureDTO"%>
<%@ page import="java.util.*,com.ringid.admin.utils.Constants"%>
<div class="form-body">
    <div class="form-group">
        <label class="col-md-3 control-label">Feature Name</label>
        <div class="col-md-4">
            <html:text property="featureName" title="Feature Name" styleClass="form-control" />
            <html:messages id="featureName" property="featureName">
                <bean:write name="featureName"  filter="false"/>
            </html:messages>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label">Feature Code</label>
        <div class="col-md-4">
            <html:text property="featureCode" title="Feature Code" styleClass="form-control" />
            <html:messages id="featureCode" property="featureCode">
                <bean:write name="featureCode"  filter="false"/>
            </html:messages>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label">Parent Feature</label>
        <div class="col-md-4">
            <%
                ArrayList<FeatureDTO> dtoList = FeatureLoader.getInstance().getFeatureList(new FeatureDTO());
            %>
            <html:select property="parentId" styleClass="form-control"> 
                <html:option  value="0">No Parent</html:option>
                    <%for (FeatureDTO dto : dtoList) {%>
                <html:option  value="<%=String.valueOf(dto.getId())%>"><%=dto.getFeatureName()%></html:option>
                    <%}%>
            </html:select>
            <html:messages id="parentId" property="parentId">
                <bean:write name="parentId"  filter="false"/>
            </html:messages>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label">Operations </label>
        <div class="col-md-9">
            <div class="mt-checkbox-inline">
                <label class="mt-checkbox mt-checkbox-outline mt-checkbox-disabled">
                    <input value="1" type="checkbox" disabled checked/> View
                    <span></span>
                </label>
                <label class="mt-checkbox mt-checkbox-outline">
                    <html:checkbox property="add"></html:checkbox>Add
                    <span></span>
                </label>
                <label class="mt-checkbox mt-checkbox-outline">
                    <html:checkbox property="modify"></html:checkbox>Modify
                    <span></span>
                </label>
                <label class="mt-checkbox mt-checkbox-outline">
                    <html:checkbox property="delete"></html:checkbox>Delete
                    <span></span>
                </label>
            </div>
        </div>
    </div>
</div>
<div class="form-actions">
    <div class="row">
        <div class="col-md-offset-3 col-md-4">
            <button type="submit" class="btn green" name="saveButton">Submit</button>
            <a href="showFeatureList.do" class="btn default">Cancel</a>
            <html:hidden property="id"/>
            <html:hidden property="project_name" value="configportal"/>
            <html:hidden property="view" value="true"/>
            <input type="hidden" name="action" value="<%=request.getSession(true).getAttribute("formType")%>" />
        </div>
    </div>
</div>