<%-- 
    Document   : userRoleTable
    Created on : Apr 1, 2017, 4:30:18 PM
    Author     : Rabby
--%>
<%@ page import="java.util.*,com.ringid.admin.utils.Constants"%>
<div class="form-body">
    <div class="form-group">
        <label class="col-md-3 control-label">Role Name</label>
        <div class="col-md-4">
            <html:text property="name" title="Role Name" styleClass="form-control" />
            <html:messages id="name" property="name">
                <bean:write name="name"  filter="false"/>
            </html:messages>
        </div>
    </div>
</div>
<div class="form-actions">
    <div class="row">
        <div class="col-md-offset-3 col-md-4">
            <button type="submit" class="btn green" name="saveButton">Submit</button>
            <a href="showUserRoleList.do" class="btn default">Cancel</a>
            <html:hidden property="id"/>
            <input type="hidden" name="action" value="<%=request.getSession(true).getAttribute("formType")%>" />
        </div>
    </div>
</div>