<%-- 
    Document   : paymentMethodTable
    Created on : Jan 31, 2018, 11:01:31 AM
    Author     : mamun
--%>

<div class="form-body">
    <logic:lessEqual name="PaymentMethodForm" property="id" value="0">
        <div class="form-group">
            <label class="col-md-3 control-label">ID</label>
            <div class="col-md-4">
                <html:text property="id" title="id" styleClass="form-control" />
                <html:messages id="id" property="id">
                    <bean:write name="id"  filter="false"/>
                </html:messages>
            </div>
        </div>
    </logic:lessEqual>
    <logic:greaterThan name="PaymentMethodForm" property="id" value="0">
        <html:hidden property="id"/>
    </logic:greaterThan>
    <div class="form-group">
        <label class="col-md-3 control-label">Name</label>
        <div class="col-md-4">
            <html:text property="name" title="name" styleClass="form-control" />
            <html:messages id="name" property="name">
                <bean:write name="name"  filter="false"/>
            </html:messages>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 control-label">IsOpen</label>
        <div class="col-md-4">
            <div class="input-group">
                <div class="icheck-inline">
                    <label>
                        <html:checkbox property="isOpen" title="is open" styleClass="icheck" /></label>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="form-actions">
    <div class="row">
        <div class="col-md-offset-3 col-md-4">
            <button type="submit" class="btn green" name="submitType">Submit</button>
            <a href="showPaymentMethodList.do" class="btn default">Cancel</a>
            <input type="hidden" name="action" value="<%=request.getSession(true).getAttribute("formType")%>" />
        </div>
    </div>
</div>