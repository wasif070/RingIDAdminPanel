<%-- 
    Document   : eventUI
    Created on : Nov 12, 2017, 11:00:27 AM
    Author     : mamun
--%>

<%@include file="/login/loginTimeExpiry.jsp"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.ringid.admin.utils.Constants"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@include  file="/shared/head.jsp" %>

<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Admin Panel | Add Event </title>
        <%@include file="/shared/css-list.jsp"%>
    </head>
    <body class="page-container-bg-solid">
        <div class="page-wrapper">
            <div class="page-wrapper-row">
                <div class="page-wrapper-top">
                    <div class="page-header">
                        <%@include  file="/shared/menu.jsp" %>
                    </div>
                </div>
            </div>
            <div class="page-wrapper-row full-height">
                <div class="page-wrapper-middle">
                    <div class="page-container">
                        <div class="page-content-wrapper">
                            <div class="page-content">
                                <div class="container">
                                    <ul class="page-breadcrumb breadcrumb">
                                        <li>
                                            <a href="welcome.do">Admin Panel</a>
                                            <span style="font-size: 14px; margin: 0 auto;" class="fa fa-angle-right"></span>
                                        </li>
                                        <li>
                                            <a href="javascript:;">Common</a>
                                            <span style="font-size: 14px; margin: 0 auto;" class="fa fa-angle-right"></span>
                                        </li>
                                        <li>
                                            <a href="showEventListInfo.do">Event</a>
                                            <span style="font-size: 14px; margin: 0 auto;" class="fa fa-angle-right"></span>
                                        </li>
                                        <li>
                                            <span id="features" data-class-active=".common .event .addEvent">Add Event</span>
                                        </li>
                                    </ul>
                                    <div class="page-content-inner">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="tabbable-line boxless tabbable-reversed">
                                                    <div class="tab-content" style="padding-top: 15px;">
                                                        <div class="portlet box green">
                                                            <div class="portlet-title">
                                                                <div class="caption">
                                                                    <i class="fa fa-plus"></i> Add Event Info </div>
                                                                <div class="tools">
                                                                    <a href="javascript:;" class="collapse"> </a>
                                                                </div>
                                                            </div>
                                                            <div class="portlet-body form">
                                                                <html:form action="addOrUpdateEvent" method="POST" acceptCharset="UTF-8" styleClass="form-horizontal" enctype="multipart/form-data">
                                                                    <div class="form-body">
                                                                        <div class="form-group">
                                                                            <label class="col-md-3 control-label">Name <span style="color: red">*</span> </label>
                                                                            <div class="col-md-4">
                                                                                <html:text property="name" title="name" styleClass="form-control" />
                                                                                <html:messages id="name" property="name">
                                                                                    <bean:write name="name"  filter="false"/>
                                                                                </html:messages>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label class="col-md-3 control-label">Event Type</label>
                                                                            <div class="col-md-4">
                                                                                <html:select property="eventType" styleClass="form-control"> 
                                                                                    <html:option  value="1">Ongoing Event</html:option>     
                                                                                    <html:option  value="2">Upcoming Event</html:option>  
                                                                                    <html:option  value="3">Past Event</html:option>
                                                                                </html:select>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label class="col-md-3 control-label">Owner RingId</label>
                                                                            <div class="col-md-4">
                                                                                <html:text property="ownerId" title="owner Id" styleClass="form-control" />
                                                                                <html:messages id="ownerId" property="ownerId">
                                                                                    <bean:write name="ownerId"  filter="false"/>
                                                                                </html:messages>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label class="col-md-3 control-label">Owner Type</label>
                                                                            <div class="col-md-4">
                                                                                <html:select property="ownerType" styleClass="form-control">
                                                                                    <html:option  value="3">User Page</html:option>
                                                                                </html:select>
                                                                                <html:messages id="ownerType" property="ownerType">
                                                                                    <bean:write name="ownerType"  filter="false"/>
                                                                                </html:messages>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group ">
                                                                            <label class="control-label col-md-3"> Profile Image </label>
                                                                            <div class="col-md-9">
                                                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                                    <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px; line-height: 150px;">
                                                                                        <img src="<%=Constants.IMAGE_BASE_URL%>${EventForm.profileImageUrl}" />
                                                                                    </div>
                                                                                    <div>
                                                                                        <span class="btn green btn-outline btn-file">
                                                                                            <span class="fileinput-new"> Select image </span>
                                                                                            <span class="fileinput-exists"> Change image </span>
                                                                                            <html:file property="theFileProfileImage" onchange="removeSpanText()" />
                                                                                        </span>
                                                                                        <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                                                        <span id="msgSpan" style="display: inline-block;">
                                                                                            <html:messages id="theFileProfileImage" property="theFileProfileImage">
                                                                                                <bean:write name="theFileProfileImage"  filter="false"/>
                                                                                            </html:messages>
                                                                                        </span>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group ">
                                                                            <label class="control-label col-md-3"> Cover Image </label>
                                                                            <div class="col-md-9">
                                                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                                    <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px; line-height: 150px;">
                                                                                        <img src="<%=Constants.IMAGE_BASE_URL%>${EventForm.coverImageUrl}" />
                                                                                    </div>
                                                                                    <div>
                                                                                        <span class="btn green btn-outline btn-file">
                                                                                            <span class="fileinput-new"> Select image </span>
                                                                                            <span class="fileinput-exists"> Change image </span>
                                                                                            <html:file property="theFileCoverImage" onchange="removeSpanText()" />
                                                                                        </span>
                                                                                        <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                                                        <span id="msgSpan" style="display: inline-block;">
                                                                                            <html:messages id="theFileCoverImage" property="theFileCoverImage">
                                                                                                <bean:write name="theFileCoverImage"  filter="false"/>
                                                                                            </html:messages>
                                                                                        </span>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label class="col-md-3 control-label">Start Datetime</label>
                                                                            <div class="col-md-4">
                                                                                <input id="datetime" type="text" size="16" name="startDate" placeholder="yyyy-mm-dd HH:mm" class="form-control date form_datetime" title="satrt date time" />
                                                                                <html:messages id="startDate" property="startDate">
                                                                                    <bean:write name="startDate"  filter="false"/>
                                                                                </html:messages>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label class="col-md-3 control-label">End Datetime</label>
                                                                            <div class="col-md-4">
                                                                                <input id="datetime" type="text" size="16" name="endDate" placeholder="yyyy-mm-dd HH:mm" class="form-control date form_datetime" title="end date time" />
                                                                                <html:messages id="endDate" property="endDate">
                                                                                    <bean:write name="endDate"  filter="false"/>
                                                                                </html:messages>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label class="col-md-3 control-label">Content</label>
                                                                            <div class="col-md-4">
                                                                                <html:text property="data" title="owner type" styleClass="form-control" />
                                                                                <html:messages id="data" property="data">
                                                                                    <bean:write name="data"  filter="false"/>
                                                                                </html:messages>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-actions">
                                                                        <div class="row">
                                                                            <div class="col-md-offset-3 col-md-8">
                                                                                <html:hidden property="id" />
                                                                                <logic:equal name="EventForm" property="action" value="5">
                                                                                    <input type="hidden" name="action" value="5" />
                                                                                </logic:equal>
                                                                                <logic:notEqual name="EventForm" property="action" value="5">
                                                                                    <input type="hidden" name="action" value="3" />
                                                                                </logic:notEqual>
                                                                                <button type="submit" class="btn green" name="saveButton">Submit</button>
                                                                                <a href="showEventListInfo.do" class="btn default">Cancel</a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </html:form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <%@include file="/shared/footer.jsp"%>
        </div>
        <%@include file="/shared/script-list.jsp"%>
        <script type="text/javascript">
            $(".form_datetime").datetimepicker({
                autoclose: true,
                format: "yyyy-mm-dd hh:ii"
            });

            function removeSpanText() {
                document.getElementById("msgSpan").innerHTML = "";
            }
        </script>
    </body>
</html>
