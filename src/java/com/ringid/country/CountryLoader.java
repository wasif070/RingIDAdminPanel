package com.ringid.country;

import com.ringid.admin.utils.log.RingLogger;
import authdbconnector.DBConnection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import org.apache.logging.log4j.Logger;

public class CountryLoader {

    private final static long LOADING_INTERVAL = 60 * 60 * 1000;
    private long loadingTime = 0;
    private HashMap<Integer, CountryDTO> countryHashMapList;
    private HashMap<String, CountryDTO> countryMap;
    static Logger logger = RingLogger.getConfigPortalLogger();

    private CountryLoader() {
    }

    private static class CountryLoaderHolder {

        private static final CountryLoader INSTANCE = new CountryLoader();
    }

    public static CountryLoader getInstance() {
        return CountryLoaderHolder.INSTANCE;
    }

    private void forceLoadData() {
        countryHashMapList = new HashMap<>();
        countryMap = new HashMap<>();
        DBConnection db = null;
        Statement stmt = null;
        ResultSet rs = null;
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            stmt = db.connection.createStatement();
            String sql = "SELECT id, code, name, dialingCode FROM countries;";
            rs = stmt.executeQuery(sql);
            while (rs.next()) {
                CountryDTO dto = new CountryDTO();
                dto.setId(rs.getInt("id"));
                dto.setDialingCode(rs.getString("dialingCode"));
                dto.setName(rs.getString("name"));
                dto.setCode(rs.getString("code"));
                countryHashMapList.put(dto.getId(), dto);
                countryMap.put(dto.getDialingCode(), dto);
            }
            logger.debug(sql + "::" + countryHashMapList.size());
        } catch (Exception e) {
            logger.debug("Country List Exception...", e);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
            }

            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }

            try {
                if (db.connection != null) {
                    authdbconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
        }
    }

    private void checkForReload() {
        long currentTime = System.currentTimeMillis();
        if (currentTime - loadingTime > LOADING_INTERVAL) {
            loadingTime = currentTime;
            forceLoadData();
        }
    }

    public synchronized ArrayList<CountryDTO> getCountryList() {
        checkForReload();
        ArrayList<CountryDTO> data = new ArrayList<>();
        Set set = countryHashMapList.entrySet();
        Iterator i = set.iterator();
        while (i.hasNext()) {
            Map.Entry me = (Map.Entry) i.next();
            CountryDTO dto = (CountryDTO) me.getValue();
            data.add(dto);
        }

        Collections.sort(data, (CountryDTO arg0, CountryDTO arg1) -> {
            return arg0.getId() - arg1.getId();
        });
        logger.debug("Data" + data.size());
        return data;
    }

    public synchronized ArrayList<CountryDTO> getCountryListForDownload() {
        checkForReload();
        ArrayList<CountryDTO> data = new ArrayList<>(countryHashMapList.values());
        return data;
    }

    public synchronized int getCountryId(String countryCode) {
        checkForReload();
        int id = 0;
        try {
            if (countryMap.containsKey(countryCode)) {
                return countryMap.get(countryCode).getId();
            }
        } catch (Exception e) {
            logger.error("Error in getCountryNameAndId(String countryCode) --> " + e);
        }
        return id;
    }

    public synchronized void forceReload() {
        loadingTime = System.currentTimeMillis();
        forceLoadData();
    }

    public synchronized CountryDTO getCountryDTO(int id) {
        checkForReload();
        if (countryHashMapList.containsKey(id)) {
            return countryHashMapList.get(id);
        }
        return null;
    }

    public synchronized String getCountryName(String mobilePhoneDialingCode) {
        checkForReload();
        if (countryMap.containsKey(mobilePhoneDialingCode)) {
            return countryMap.get(mobilePhoneDialingCode).getName();
        }
        return null;
    }

    public synchronized ArrayList<CountryDTO> getCountryCodeList() {
        checkForReload();
        ArrayList<CountryDTO> cCodeList = new ArrayList<>();
        countryHashMapList.entrySet().stream().forEach((entrySet) -> {
            CountryDTO cdto = new CountryDTO();
            CountryDTO value = entrySet.getValue();
            if (!(value.getName().equals(""))) {
                cdto.setId(value.getId());
                cdto.setCode(value.getCode());
                cdto.setName(value.getName());
                cCodeList.add(cdto);
            }
        });
        return cCodeList;
    }
}
