package com.ringid.admin.utils.log;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

public class RingLogger {

    private static final Logger CONFIGPORTAL_LOGGER;
    private static final Logger ACTIVITY_LOGGER;
    private static final Logger PERMISSION_LOGGER;
    private static int debug = 0;

    static {
        ACTIVITY_LOGGER = LogManager.getLogger("com.ringid.activity");
        CONFIGPORTAL_LOGGER = LogManager.getLogger("com.ringid.admin.newsPortal");
        PERMISSION_LOGGER = LogManager.getLogger("com.ringid.admin.permission");
    }

    public static Logger getActivityLogger() {
        return ACTIVITY_LOGGER;
    }

    public static Logger getConfigPortalLogger() {
        return CONFIGPORTAL_LOGGER;
    }

    public static Logger getPermissionLogger() {
        return PERMISSION_LOGGER;
    }

    public static int getDebug() {
        return debug;
    }

    public static void setDebug(int debug) {
        RingLogger.debug = debug;
    }

    public static boolean isDebug() {
        return debug == 1;
    }
}
