package com.ringid.admin.utils;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.ringid.admin.utils.log.RingLogger;
import java.io.StringReader;
import java.security.SecureRandom;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.ringid.utilities.AppConstants;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;

public class Utils {

    public Utils() {
    }

    public static int getRegistrationTypeByStreamingMediaType(int streaming_media_type) {
        int reg_type = AppConstants.PRIMITIVE_NULL;

        switch (streaming_media_type) {
            case AppConstants.StreamingTypes.AUDIO_STREAM:
                reg_type = AppConstants.LiveChannelRegistrationTypes.LIVE_STREAM_AUDIO;
                break;
            case AppConstants.StreamingTypes.VIDEO_STREAM:
                reg_type = AppConstants.LiveChannelRegistrationTypes.LIVE_STREAM_VIDEO;
                break;
            case AppConstants.StreamingTypes.CHANNEL_STREAM:
                reg_type = AppConstants.LiveChannelRegistrationTypes.VIDEO_CHANNEL;
                break;
            case AppConstants.StreamingTypes.TV_CHANNEL_STREAM:
                reg_type = AppConstants.LiveChannelRegistrationTypes.TV_CHANNEL;
                break;
            default:
                break;
        }

        return reg_type;
    }

    public static String listToString(ArrayList<Long> list) {
        StringBuilder sb = new StringBuilder();
        for (long s : list) {
            sb.append(",");
            sb.append(s);
        }
        if (sb.length() > 0) {
            sb.deleteCharAt(0);
            return new String(sb);
        }
        return "-1";
    }

    public static String removeBrackets(String s) {
        try {
            if (s.length() > 1) {
                StringBuilder sb = new StringBuilder(s);
                sb.deleteCharAt(0);
                sb.deleteCharAt(sb.length() - 1);
                return sb.toString();
            }
        } catch (Exception e) {
        }
        return s;
    }

//    public static String traduceUtf8(String _normalString) {
//        if (_normalString != null) {
//            try {
//                _normalString = new String(_normalString.getBytes("ISO-8859-1"), "UTF-8");
//            } catch (java.io.UnsupportedEncodingException e) {
//                System.err.println(e);
//            }
//        }
//        return _normalString;
//    }
    public static String base64_to_image(String arg) {
        String user_image = arg;
        user_image = user_image.replaceAll("-", "+");
        user_image = user_image.replaceAll("_", "/");
        switch (user_image.length() % 4) {
            case 0:
                break;
            case 2:
                user_image += "==";
                break;
            case 3:
                user_image += "=";
                break;
        }
        return user_image;
    }

    public static String getRandomPacketId(String userIdentity) {
        SecureRandom random = new SecureRandom();
        char[] chars = new char[8];
        for (int i = 0; i < chars.length; i++) {
            int v = random.nextInt(10 + 26 + 26);
            char c;
            if (v < 10) {
                c = (char) ('0' + v);
            } else if (v < 36) {
                c = (char) ('a' - 10 + v);
            } else {
                c = (char) ('A' - 36 + v);
            }
            chars[i] = c;
        }
        String key = new String(chars);
        if (userIdentity != null) {
            key = key + userIdentity;
        }
        return key;
    }

    public static String getCurrentDate() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/YYYY");
        Date date = new Date();

        return dateFormat.format(date);
    }

    public static Document convertStringToDocument(String xmlStr) {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder;
        try {
            builder = factory.newDocumentBuilder();
            Document doc = builder.parse(new InputSource(new StringReader(xmlStr)));
            return doc;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String traduceUtf8(String _normal) {
        if (_normal != null) {
            try {
                _normal = new String(_normal.getBytes(), "UTF-8");
            } catch (java.io.UnsupportedEncodingException e) {
                System.err.println(e);
            }
        }
        return _normal;
    }

    public static String detectIP(HttpServletRequest request) {
        String ipAddress = request.getHeader("x-forwarded-for");
        if (ipAddress == null) {
            ipAddress = request.getHeader("X_FORWARDED_FOR");
            if (ipAddress == null) {
                ipAddress = request.getRemoteAddr();
            }
        }
        return ipAddress;
    }

    public static MyAppError forceConstantAPIToReload() {
        MyAppError error = new MyAppError();
        JsonArray jsonArray = new JsonArray();
        for (String serverUrl : Constants.AUTH_CONTROLLER_CONSTANT_FORCE_RELOAD_URL) {
            String response = HTTPRequestProcessor.upload("GET", null, null, serverUrl);
            RingLogger.getConfigPortalLogger().debug("serverUrl : " + serverUrl + " --> Response [Constant] : " + response);
            JsonObject jsonObject = new JsonObject();
            try {
                if (response.toLowerCase().contains("reloaded properly")) {
                    jsonObject.addProperty("success", true);

                } else {
                    jsonObject.addProperty("success", false);
                }
            } catch (Exception e) {
                jsonObject.addProperty("success", false);
                RingLogger.getConfigPortalLogger().error("Server Reload failed! Exception : " + e);
            }
            jsonArray.add(jsonObject);
        }
        error.setErrorMessage(jsonArray.toString());
        return error;
    }

    public static MyAppError forceAuthServerAPIToReload() {
        MyAppError error = new MyAppError();
        JsonArray jsonArray = new JsonArray();
        for (String serverUrl : Constants.AUTH_CONTROLLER_AUTH_SERVER_FORCE_RELOAD_URL) {
            String response = HTTPRequestProcessor.upload("GET", null, null, serverUrl);
            RingLogger.getConfigPortalLogger().debug("serverUrl : " + serverUrl + " --> Response [Constant] : " + response);
            JsonObject jsonObject = new JsonObject();
            try {
                if (response.toLowerCase().contains("reloaded properly")) {
                    jsonObject.addProperty("success", true);

                } else {
                    jsonObject.addProperty("success", false);
                }
            } catch (Exception e) {
                jsonObject.addProperty("success", false);
                RingLogger.getConfigPortalLogger().error("Server Reload failed! Exception : " + e);
            }
            jsonArray.add(jsonObject);
        }
        error.setErrorMessage(jsonArray.toString());
        return error;
    }

    public static MyAppError forceVoiceSettingsAPIToReload() {
        MyAppError error = new MyAppError();
        JsonArray jsonArray = new JsonArray();
        for (String serverUrl : Constants.AUTH_CONTROLLER_VOICE_SETTINGS_FORCE_RELOAD_URL) {
            String response = HTTPRequestProcessor.upload("GET", null, null, serverUrl);
            RingLogger.getConfigPortalLogger().debug("serverUrl : " + serverUrl + " --> Response [Constant] : " + response);
            JsonObject jsonObject = new JsonObject();
            try {
                if (response.toLowerCase().contains("reloaded properly")) {
                    jsonObject.addProperty("success", true);

                } else {
                    jsonObject.addProperty("success", false);
                }
            } catch (Exception e) {
                jsonObject.addProperty("success", false);
                RingLogger.getConfigPortalLogger().error("Server Reload failed! Exception : " + e);
            }
            jsonArray.add(jsonObject);
        }
        error.setErrorMessage(jsonArray.toString());
        return error;
    }

    public static MyAppError forceAppUpdateUrlsAPIToReload() {
        MyAppError error = new MyAppError();
        JsonArray jsonArray = new JsonArray();
        for (String serverUrl : Constants.AUTH_CONTROLLER_UPDATE_URL_FORCE_RELOAD_URL) {
            String response = HTTPRequestProcessor.upload("POST", null, null, serverUrl);
            RingLogger.getConfigPortalLogger().debug("serverUrl : " + serverUrl + " --> Response [Constant] : " + response);
            JsonObject jsonObject = new JsonObject();
            try {
                if (response.toLowerCase().contains("1")) {
                    jsonObject.addProperty("success", true);

                } else {
                    jsonObject.addProperty("success", false);
                }
            } catch (Exception e) {
                jsonObject.addProperty("success", false);
                RingLogger.getConfigPortalLogger().error("Server Reload failed! Exception : " + e);
            }
            jsonArray.add(jsonObject);
        }
        error.setErrorMessage(jsonArray.toString());
        return error;
    }

    public static String getMessageForReasonCode(int reasonCode) {
        String message = "Operation Failed : ";
        switch (reasonCode) {
            case ReasonCode.ALREADY_VERIFIED:
                message += "Already Verified (rc: " + reasonCode + ")";
                break;
            case ReasonCode.INVALID_INFORMATION:
                message += "Invalid Information (rc : " + reasonCode + ")";
                break;
            case ReasonCode.NOT_FOUND:
                message += "Not Found (rc : " + reasonCode + ")";
                break;
            default:
                message += "(rc: " + reasonCode + ")";
                break;
        }

        return message;
    }
}
