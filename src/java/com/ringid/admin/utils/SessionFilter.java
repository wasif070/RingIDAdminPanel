/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.utils;

import com.ringid.admin.utils.log.RingLogger;
import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Kazi Nasir Uddin Oly
 * @Date Jun 18, 2016
 */
public class SessionFilter implements Filter {

    private static Logger logger = RingLogger.getConfigPortalLogger();

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpSession session = null;
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse res = (HttpServletResponse) response;
        try {
            session = req.getSession(false);
            if (session == null) {
                res.sendRedirect("/configportal/index.jsp"); //If the Active session is null ,we redirect to the timeout.jsp 
            }
            chain.doFilter(request, response);
        } catch (IOException io) {
            logger.debug("IOException raised in SimpleFilter");
        } catch (ServletException se) {
            logger.debug("ServletException raised in SimpleFilter " + se);
            se.printStackTrace();
        }
    }

    @Override
    public void destroy() {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
