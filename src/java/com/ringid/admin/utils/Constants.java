package com.ringid.admin.utils;

import com.ringid.admin.projectMenu.MenuNames;
import java.io.BufferedReader;
import java.util.HashMap;
import java.util.Properties;

public class Constants {

    public static int FEATURED_USER_DEFAULT_WEIGHT = 1;
    public static long TOP_STREAMERS_ROOM_ID;
    public static long CHARITY_ROOM_ID;

    // for heartBeatSender
    BufferedReader br = null;

    public static String SERVER;
    public static int VERSION;

    // decision for DevelopmentProfile/ProductionProfile
    static {
        try {
            Properties properties = new Properties();
            properties.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("Profile.txt"));
            if (properties.containsKey("SERVER")) {
                SERVER = properties.getProperty("SERVER").trim();
                switch (properties.getProperty("SERVER").trim()) {
                    case "local":
                    case "dev":
                        USERID = "2110100310";
                        PASSWORD = "abc123";
                        LOGGED_IN_USER_ID = 61534;
                        MEDIA_CLOUD_BASE_URL = "https://devmediacloud.ringid.com/";
                        IMAGE_BASE_URL = "https://devimagesres.ringid.com/";
                        IMAGE_UPLOAD_BASE_URL = "https://devimages.ringid.com/";
                        MEDIA_UPLOAD_BASE_URL = "https://devmediacloudapi.ringid.com/";
                        PROFILE_IMAGE_UPLOAD_URL = "http://devimages.ringid.com/ringmarket/ImageUploadHandler";
                        COVER_IMAGE_UPLOAD_URL = "http://devimages.ringid.com/ringmarket/CoverImageUploadHandler";
                        TOP_STREAMERS_ROOM_ID = 1329464;
                        CHARITY_ROOM_ID = 61680;
                        AUTH_CONTROLLER_CONSTANT = "https://devauth.ringid.com/rac/";
                        STICKER_MARKET_RESOURCE = "https://devstickerres.ringid.com/";
                        STICKER_MARKET_SERVER = "https://devsticker.ringid.com/";
                        WEB = "https://dev.ringid.com/";

                        AUTH_CONTROLLER_CONSTANT_FORCE_RELOAD_URL = new String[]{
                            "http://38.102.83.53:8080/reloadconstants",
                            "http://38.102.83.68:8080/reloadconstants"
                        };
                        AUTH_CONTROLLER_AUTH_SERVER_FORCE_RELOAD_URL = new String[]{
                            "http://38.102.83.53:8080/rac/reloadcomports",
                            "http://38.102.83.68:8080/rac/reloadcomports"
                        };
                        AUTH_CONTROLLER_VOICE_SETTINGS_FORCE_RELOAD_URL = new String[]{
                            "http://38.102.83.53:8080/common/ForceReload?rt=9",
                            "http://38.102.83.68:8080/common/ForceReload?rt=9"
                        };
                        AUTH_CONTROLLER_UPDATE_URL_FORCE_RELOAD_URL = new String[]{
                            "http://38.102.83.53:8080/rac/reloadUpdateUrls?rt=9",
                            "http://38.102.83.68:8080/rac/reloadUpdateUrls?rt=9"
                        };
                        break;
                    case "pro":
                        USERID = "2110000001";
                        PASSWORD = "p@assn3w";
                        LOGGED_IN_USER_ID = 1068907;
                        MEDIA_CLOUD_BASE_URL = "https://mediacloud.ringid.com/";
                        IMAGE_BASE_URL = "https://imagesres.ringid.com/";
                        IMAGE_UPLOAD_BASE_URL = "https://images.ringid.com/";
                        MEDIA_UPLOAD_BASE_URL = "https://mediacloudapi.ringid.com/";
                        PROFILE_IMAGE_UPLOAD_URL = "http://images.ringid.com/ringmarket/ImageUploadHandler";
                        COVER_IMAGE_UPLOAD_URL = "http://images.ringid.com/ringmarket/CoverImageUploadHandler";
                        TOP_STREAMERS_ROOM_ID = 1203767;
                        CHARITY_ROOM_ID = 1465530;
                        AUTH_CONTROLLER_CONSTANT = "https://auth.ringid.com/rac/";
                        STICKER_MARKET_RESOURCE = "https://stickerres.ringid.com/";
                        STICKER_MARKET_SERVER = "https://sticker.ringid.com/";
                        WEB = "https://www.ringid.com/";

                        AUTH_CONTROLLER_CONSTANT_FORCE_RELOAD_URL = new String[]{
                            "http://104.193.36.68:8081/reloadconstants",
                            "http://104.193.36.68:8082/reloadconstants",
                            "http://104.193.36.69:8081/reloadconstants",
                            "http://104.193.36.69:8082/reloadconstants",
                            "http://104.193.36.106:8081/reloadconstants",
                            "http://104.193.36.106:8082/reloadconstants"
                        };
                        AUTH_CONTROLLER_AUTH_SERVER_FORCE_RELOAD_URL = new String[]{
                            "http://104.193.36.68:8081/rac/reloadcomports",
                            "http://104.193.36.68:8082/rac/reloadcomports",
                            "http://104.193.36.69:8081/rac/reloadcomports",
                            "http://104.193.36.69:8082/rac/reloadcomports",
                            "http://104.193.36.106:8081/rac/reloadcomports",
                            "http://104.193.36.106:8082/rac/reloadcomports"
                        };
                        AUTH_CONTROLLER_VOICE_SETTINGS_FORCE_RELOAD_URL = new String[]{
                            "http://104.193.36.68:8081/common/ForceReload?rt=9",
                            "http://104.193.36.68:8082/common/ForceReload?rt=9",
                            "http://104.193.36.69:8081/common/ForceReload?rt=9",
                            "http://104.193.36.69:8082/common/ForceReload?rt=9",
                            "http://104.193.36.106:8081/common/ForceReload?rt=9",
                            "http://104.193.36.106:8082/common/ForceReload?rt=9"
                        };
                        AUTH_CONTROLLER_UPDATE_URL_FORCE_RELOAD_URL = new String[]{
                            "http://104.193.36.68:8081/rac/reloadUpdateUrls?rt=9",
                            "http://104.193.36.68:8082/rac/reloadUpdateUrls?rt=9",
                            "http://104.193.36.69:8081/rac/reloadUpdateUrls?rt=9",
                            "http://104.193.36.69:8082/rac/reloadUpdateUrls?rt=9",
                            "http://104.193.36.106:8081/rac/reloadUpdateUrls?rt=9",
                            "http://104.193.36.106:8082/rac/reloadUpdateUrls?rt=9"
                        };
                        break;
                }
            }
            if (properties.containsKey("VERSION")) {
                VERSION = Integer.parseInt(properties.getProperty("VERSION").trim());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // DevelopmentProfile/ProductionProfile constants
    public static String USERID;
    public static String PASSWORD;
    public static String MEDIA_CLOUD_BASE_URL;
    public static String IMAGE_UPLOAD_BASE_URL;
    public static String MEDIA_UPLOAD_BASE_URL;
    public static String IMAGE_BASE_URL;
    public static String PROFILE_IMAGE_UPLOAD_URL;
    public static String COVER_IMAGE_UPLOAD_URL;
    public static String AUTH_CONTROLLER_CONSTANT;
    public static String STICKER_MARKET_RESOURCE;
    public static String STICKER_MARKET_SERVER;
    public static String WEB;

    public static String[] AUTH_CONTROLLER_CONSTANT_FORCE_RELOAD_URL;

    public static String[] AUTH_CONTROLLER_AUTH_SERVER_FORCE_RELOAD_URL;

    public static String[] AUTH_CONTROLLER_VOICE_SETTINGS_FORCE_RELOAD_URL;
    public static long LOGGED_IN_USER_ID;

    public static String UPLOAD_LINK_PAGE_IMAGE = IMAGE_UPLOAD_BASE_URL + "ringmarket/PageImageHandler";
    public static String UPLOAD_LINK_PAGE_AUDIO = IMAGE_UPLOAD_BASE_URL + "ringmarket/PageMp3Handler";
    public static String UPLOAD_LINK_PAGE_VIDEO = IMAGE_UPLOAD_BASE_URL + "ringmarket/PageMp4Handler";
    public static String UPLOAD_LINK_CLOUD_MEDIA = MEDIA_UPLOAD_BASE_URL + "stream/LiveSessionMediaHandler";
    public static String DELETE_LINK_CLOUD_MEDIA = IMAGE_BASE_URL;
    public static String RESTORE_LINK_CLOUD_MEDIA = IMAGE_BASE_URL + "ringpost/restorehandler/";
    // 
    public static final long SLEEP_TIME_100MS = 100L;
    public static final long SLEEP_TIME_1SEC = 1000L;
    public static final int MAX_RESEND_COUNTER = 10;
    public static final int DEFAULT_PACKET_SIZE = 1024;
    // -----  ACTION TYPES------------
    public static final int TYPE_SESSION_VALIDATION = 76;
    public static final int TYPE_ALBUM_IMAGE_UPLOAD = 102;
    public static final int TYPE_ADD_COVER_IMAGE = 103;
    public static final int TYPE_UPDATE_IMAGE_FROM_WEB = 63;//”update_image_from_web” 
    public static final int IMAGE_TYPE_ALBUM = 1;
    public static final int IMAGE_TYPE_PROFILE = 2;
    public static final int IMAGE_TYPE_COVER = 3;
    //   ===============IMAGE UPLOAD VARIABLES ================
    public static String CLIENTS_IMAGE_FOLDER = "clients_image";
    public static String IMAGE_CONTEXT_PATH_ONE = "imageServer";
    public static String IMAGE_CONTEXT_PATH_TWO = "imageServer/";
    public static String DEFAULT_IMAGE = "default.png";
    public static String WEB_IMAGE_EXTENSION = "web";
    public static int IMG_WIDTH = 100;
    public static int IMG_HEIGHT = 100;
    public static int IMG_WIDTH_WEB = 400;
    public static int IMG_HEIGHT_WEB = 400;
    public static final int BUFFER_SIZE = 1024;
    //// FOR MESSAGE 

    public static final String SERVER_URL = "";
    public static final String COUNTRY_CODE = "countryCode";
    public static final String MOBILE_NUMBER = "mobileNumber";
    public static final String SMS_MESSAGE = "textMessage";
    public static final String RESPONSE_MESSAGE_ID = "rMessageId";
    public static final String VERIFIED_SEND = "verifiedSend";
    public static final String VERIFICATION_CODE = "vCode";
    public static final String BRAND_ID = "brandId";
    public static final int IMG_TYPE_COVER = 1;
    public static final int IMG_TYPE_PROFILE = 2;
    public static final int IMG_TYPE_ALBUM = 3;

    public static final String GNM = "gNm";

    public static String SERVER_IP;//own server ip
    public static int SERVER_PORT = 0;
    //public static InetAddress HEARTBEAT_SERVER_IP = null;
    //public static int HEARTBEAT_SERVER_PORT = 1001;
    //public static long HEARTBEAT_SEND_INTERVAL = 5000L;
    //public static final int SERVER_TYPE_IMAGE = 3;

    /// for admin 
    public final static String RECORD_PER_PAGE = "RECORD_PER_PAGE";
    public final static String LOGIN_DTO = "loginDTO";
    public final static String LOGIN_ID = "loginId";
    public final static long LOGIN_EXPIRE_TIME = 28800000;//8 * 60 * 60 * 1000;
    public final static String MESSAGE = "message";
    public final static String STARTING_RECORD_NO = "startingRecordNo";
    public final static String ENDING_RECORD_NO = "endingRecordNo";
    public final static String TOTAL_RECORDS = "totalRecords";
    public final static String TOTAL_PAGES = "totalPages";
    public final static String CURRENT_PAGE_NO = "currentPageNo";
    //Action
    public final static int RECORD_PER_PAGE_CHANGE = 1;
    public final static int PAGE_NAVIGATION = 2;
    public final static int ADD = 3;
    public final static int EDIT = 4;
    public final static int UPDATE = 5;
    public final static int DELETE = 6;
    public final static int SEARCH = 7;
    public final static int RESET = 13;
    /* Sorting style */

    public final static int ASC_SORT = 0;
    public final static int DESC_SORT = 1;
    public final static String DATA_ROWS = "DATA_ROWS";
    public final static String COLLECTION_LIST = "collectionList";

    /*------Sorting Column-----*/
    public final static int COLUMN_ONE = 1;
    public final static int COLUMN_TWO = 2;
    public final static int COLUMN_THREE = 3;
    public final static int COLUMN_FOUR = 4;
    public final static int COLUMN_FIVE = 5;
    public final static int COLUMN_SIX = 6;
    public final static int C = 6;

    ///////////////sticker market
    public static final int STICKER_CTG_TYPE_FREE = 1;
    public static final int STICKER_CTG_TYPE_TOP = 2;
    public static final int STICKER_CTG_TYPE_NEW = 3;

    public static final long STICKER_CTG_NEW_TIME_LIMIT = 1000 * 60 * 60 * 24 * 30;
    public static final String ALBUM_IMAGE_UPLOAD_URL = "http://localhost:8084/UploadSticker/";

    public static final String FAILED = "failed";
    public static final String SUCCESS = "success";

    public static final int VERIFIED = 1;
    public static final int DELIVERED = 2;

    public static final int NOT_VERIFIED = 1;
    public static final int UN_DELIVERED = 2;
    public static final int BOTH = 3;

    public static final String[] DELIVERY_STATUS_TEXT = {"FAILED", "PENDING", "DELIVERED"};

    public static final int SUPER_ADMIN = 1;
    public static final int ADMIN = 2;
    public static final int GUEST = 3;

    public static final int RELOAD_VOICE_SERVERS = 1;
    public static final int RELOAD_CHAT_SERVERS = 2;
    public static final int RELOAD_LOG_SETTINGS = 3;
    public static final int RELOAD_SETTINGS = 4;
    public static final int RELOAD_AUTH_SERVERS = 5;
//    public static final int RELOAD_RELAY_SERVERS = 6;
    public static final int RELOAD_SELF_SETTINGS = 7;
//    public static final int RELOAD_USER_REPOSITORY = 8;
    public static final int RELOAD_FLOATING_MENU = 11;
    public static final int RELOAD_BREAKING_FEEDS = 12;
    public static final int RELOAD_PAGE_TYPES = 20;
    public static final int RELOAD_PAGE_CATEGORIES = 21;
    public static final int RELOAD_OFFERS = 28;
    public static final int RELOAD_WALLET_PERMITTED_COUNTRY_LIST = 30;
    public static final int RELOAD_RINGSTUDIO_SETTINGS = 32;
    public static final int RELOAD_CELEBRITY_ROOM_DETAILS = 34;
    public static final int RELOAD_WALLET_FLEXI_BLACKLISTED_USERS = 35;
//    public static final int RELOAD_HOME_SPECIAL_ROOM = 38;
    public static final int RELOAD_RINGSTORE_SETTINGS = 38;
    public static final int RELOAD_HOME_DONATION_PAGE = 39;
    public static final int RELOAD_STREAMING_CODEC_SETTINGS = 43;
    public static final int RESET_STATIC_FEEDBACK_TIME = 45;
    public static final int RELOAD_SPORTS_SERVER_DATA = 46;
//    public static final int RELOAD_SPORTS_MATCH_LIST = 46;
    public static final int RESET_SQL_EXCEPTION_COUNTER = 47;
    public static final int RELOAD_COIN_SERVER_DATA = 48;
    public static final int RELOAD_CHANNEL_CATEGORY = 49;
    public static final int RELOAD_LIVE_TO_CHANNEL = 50;
    public static final int RELOAD_RINGBIT_SETTINGS = 51;
    public static final int RELOAD_DYNAMIC_MENU = 53;
    public static final int RELOAD_RINGID_DISTRIBUTOR = 54;
    public static final int RELOAD_SIGNUP_REWARD_SETTINGS = 57;
    public static final int RELOAD_SPAM_REASONS = 58;
    public static final int RELOAD_FEATURE_MANAGER = 59;
    public static final int RELOAD_FIREBASE_IP_PORT = 60;
    public static final int RELOAD_FIREBASE_COUNTRY_LIST = 61;
    public static final int RELOAD_SERVER_MESSAGES = 62;
    public static final int RELOAD_AD_SERVER_DATA = 64;
    public static final int RELOAD_STREAMING_SERVERS = 71;
    public static final int RELOAD_MANUAL_RECORDED_LIVES = 73;
    public static final int RELOAD_USER_WALLETS = 74;
    public static final int RELOAD_STICKER_REPO = 75;
    public static final int RELOAD_STREAM_ADMINS_REPO = 76;
    public static final int RELOAD_APP_UPDATE_SETTINGS = 77;
    public static final int FORCE_RELOAD_HOME_MESSAGE = 78;
    public static final int FORCE_RELOAD_APP_UPDATE_URL = 79;
    public static final int SYNC_DUMMY_FOLLOWERS = 80;

    //force reload both(notify auth server and reload)
    public static final int RELOAD_APP_UPDATE_URL = 79;
    public static String[] AUTH_CONTROLLER_UPDATE_URL_FORCE_RELOAD_URL;

    /*............SpamReason................*/
    //////////////SpamType
    public static final int SPAM_USER = 1;
    public static final int SPAM_FEED = 2;
    public static final int SPAM_IMAGE = 3;
    public static final int SPAM_MEDIA_CONTENT = 4;

    public static final String API_RELOAD_CONSTANT = "constant";
    public static final String API_RELOAD_AUTH_SERVER = "authServer";
    public static final String API_RELOAD_VOICE_SETTINGS = "voiceSettings";
    public static final String API_RELOAD_APP_UPDATE = "appUpdateUrls";

    //checklog delete or resend action
    public static final int LOG_RESEND = 1;
    public static final int LOG_DELETE = 2;

    public static final int REASIGN_CHANNEL_SERVER = 1;
    public static final int REASIGN_STREAM_SERVER = 2;

    public static class MEDIA_TYPE {

        public static final int IMAGE = 1;
        public static final int AUDIO = 2;
        public static final int VIDEO = 3;
    }

    // Device
    public static final String[] DEVICE = {
        "NONE",
        "PC",
        "Android",
        "iPhone",
        "Windows",
        "Web"
    };

    // App Type
    public static final String[] APP_TYPE = {"NONE", "RINGID_FULL", "RINGID_LITE", "RINGID_LIVE", "RINGID_NETCAFEE", "RINGID_MESSENGER"};

    public static final String OPERATION_VIEW = "View";
    public static final String OPERATION_ADD = "Add";
    public static final String OPERATION_MODIFY = "Modify";
    public static final String OPERATION_DELETE = "Delete";

    public static String MOBILE_NUMBER_VERIFY_PREFIX = "9999";
    public static final long SEVEN_DAY = 7 * 24 * 60 * 60 * 1000;

    // Live Room
    public static final int LIVE_ROOM_TOP = 1;
    public static final int LIVE_ROOM_DONATION = 2;

    // channel subscription type
    public static final long DAILY = 24 * 60 * 60 * 1000L;
    public static final long WEEKLY = 7 * 24 * 60 * 60 * 1000L;
    public static final long MONTHLY = 30 * 24 * 60 * 60 * 1000L;
    public static final long QUARTERLY = 3 * 30 * 24 * 60 * 60 * 1000L;
    public static final long HALF_YEARLY = 6 * 30 * 24 * 60 * 60 * 1000L;
    public static final long YEARLY = 365 * 24 * 60 * 60 * 1000L;

    public static final HashMap<String, String> featureMenuUrlList;
    public static final String ADD_PERMISSION = "#add";
    public static final String ADD_OR_MODIFY_PERMISSION = "#addOrModify";
    public static final String MODIFY_PERMISSION = "#modify";
    public static final String DELETE_PERMISSION = "#delete";

    static {
        featureMenuUrlList = new HashMap<>();

//        featureMenuUrlList.put("",MenuNames.SERVERS);  //0 server
        /**
         * 1 Authserver
         */
        featureMenuUrlList.put("/authServerListInfo.do", MenuNames.AUTH_SERVER);
        featureMenuUrlList.put("/admin/authServers/authServerList.jsp", MenuNames.AUTH_SERVER);
        featureMenuUrlList.put("/addAuthServerInfo.do", MenuNames.AUTH_SERVER + ADD_PERMISSION);
        featureMenuUrlList.put("/admin/authServers/authServerUI.jsp", MenuNames.AUTH_SERVER + ADD_PERMISSION);
        featureMenuUrlList.put("/editAuthServer.do", MenuNames.AUTH_SERVER + MODIFY_PERMISSION);
        featureMenuUrlList.put("/updateAuthServer.do", MenuNames.AUTH_SERVER + MODIFY_PERMISSION);
        featureMenuUrlList.put("/admin/authServers/UpdateAuthServer.jsp", MenuNames.AUTH_SERVER + MODIFY_PERMISSION);
        featureMenuUrlList.put("/deleteAuthServer.do", MenuNames.AUTH_SERVER + DELETE_PERMISSION);
        featureMenuUrlList.put("/fileUploadAuthServer.do", MenuNames.AUTH_SERVER);
        featureMenuUrlList.put("/uploadAuthServerFile.do", MenuNames.AUTH_SERVER);
        featureMenuUrlList.put("/admin/authServers/uploadFile.jsp", MenuNames.AUTH_SERVER);
        featureMenuUrlList.put("/Download/DownloadAuthServers", MenuNames.AUTH_SERVER);

        /**
         * 2 Chat Server
         */
        featureMenuUrlList.put("/chatServerListInfo.do", MenuNames.CHAT_SERVER);
        featureMenuUrlList.put("/admin/chatServers/chatServerList.jsp", MenuNames.CHAT_SERVER);
        featureMenuUrlList.put("/addChatServerInfo.do", MenuNames.CHAT_SERVER + ADD_PERMISSION);
        featureMenuUrlList.put("/admin/chatServers/chatServerUI.jsp", MenuNames.CHAT_SERVER + ADD_PERMISSION);
        featureMenuUrlList.put("/editChatServer.do", MenuNames.CHAT_SERVER + MODIFY_PERMISSION);
        featureMenuUrlList.put("/updateChatServer.do", MenuNames.CHAT_SERVER + MODIFY_PERMISSION);
        featureMenuUrlList.put("/admin/chatServers/UpdateChatServer.jsp", MenuNames.CHAT_SERVER + MODIFY_PERMISSION);
        featureMenuUrlList.put("/deleteChatServer.do", MenuNames.CHAT_SERVER + DELETE_PERMISSION);
        featureMenuUrlList.put("/Download/DownloadChatServers", MenuNames.CHAT_SERVER);

        /**
         * 3 Image Server
         */
        featureMenuUrlList.put("/imageServerListInfo.do", MenuNames.IMAGE_SERVER);
        featureMenuUrlList.put("/admin/imageServers/imageServerList.jsp", MenuNames.IMAGE_SERVER);
        featureMenuUrlList.put("/addImageServerInfo.do", MenuNames.IMAGE_SERVER + ADD_PERMISSION);
        featureMenuUrlList.put("/admin/imageServers/imageServerUI.jsp", MenuNames.IMAGE_SERVER + ADD_PERMISSION);
        featureMenuUrlList.put("/editImageServer.do", MenuNames.IMAGE_SERVER + MODIFY_PERMISSION);
        featureMenuUrlList.put("/updateImageServer.do", MenuNames.IMAGE_SERVER + MODIFY_PERMISSION);
        featureMenuUrlList.put("/admin/imageServers/UpdateImageServer.jsp", MenuNames.IMAGE_SERVER + MODIFY_PERMISSION);
        featureMenuUrlList.put("/deleteImageServer.do", MenuNames.IMAGE_SERVER + DELETE_PERMISSION);
        featureMenuUrlList.put("/Download/DownloadImageServers", MenuNames.IMAGE_SERVER);

        /**
         * 4 Offline Server
         */
//        featureMenuUrlList.put("/offlineServerListInfo.do", MenuNames.OFFLINE_SERVER);
//        featureMenuUrlList.put("/admin/offlineServers/offlineServerList.jsp", MenuNames.OFFLINE_SERVER);
//        featureMenuUrlList.put("/addOfflineServerInfo.do", MenuNames.OFFLINE_SERVER + ADD_PERMISSION);
//        featureMenuUrlList.put("/admin/offlineServers/offlineServerUI.jsp", MenuNames.OFFLINE_SERVER + ADD_PERMISSION);
//        featureMenuUrlList.put("/editOfflineServer.do", MenuNames.OFFLINE_SERVER + MODIFY_PERMISSION);
//        featureMenuUrlList.put("/updateOfflineServer.do", MenuNames.OFFLINE_SERVER + MODIFY_PERMISSION);
//        featureMenuUrlList.put("/admin/offlineServers/offlineServerList.jsp", MenuNames.OFFLINE_SERVER + MODIFY_PERMISSION);
//        featureMenuUrlList.put("/deleteOfflineServer.do", MenuNames.OFFLINE_SERVER + DELETE_PERMISSION);
//        featureMenuUrlList.put("/Download/DownloadOfflineServers", MenuNames.OFFLINE_SERVER);
        /**
         * 5 Relay Server
         */
//        featureMenuUrlList.put("/relayServerListInfo.do", MenuNames.RELAY_SERVER);
//        featureMenuUrlList.put("/admin/relayServers/relayServerList.jsp", MenuNames.RELAY_SERVER);
//        featureMenuUrlList.put("/addRelayServerInfo.do", MenuNames.RELAY_SERVER + ADD_PERMISSION);
//        featureMenuUrlList.put("/admin/relayServers/relayServerUI.jsp", MenuNames.RELAY_SERVER + ADD_PERMISSION);
//        featureMenuUrlList.put("/editRelayServer.do", MenuNames.RELAY_SERVER + MODIFY_PERMISSION);
//        featureMenuUrlList.put("/updateRelayServer.do", MenuNames.RELAY_SERVER + MODIFY_PERMISSION);
//        featureMenuUrlList.put("/admin/relayServers/UpdateRelayServer.jsp", MenuNames.RELAY_SERVER + MODIFY_PERMISSION);
//        featureMenuUrlList.put("/deleteRelayServer.do", MenuNames.RELAY_SERVER + DELETE_PERMISSION);
//        featureMenuUrlList.put("/Download/DownloadRelayServers", MenuNames.RELAY_SERVER);
        /**
         * 6 SMS Server
         */
        featureMenuUrlList.put("/smsServerListInfo.do", MenuNames.SMS_SERVER);
        featureMenuUrlList.put("/admin/smsServers/smsServerList.jsp", MenuNames.SMS_SERVER);
        featureMenuUrlList.put("/addSmsServerInfo.do", MenuNames.SMS_SERVER + ADD_PERMISSION);
        featureMenuUrlList.put("/admin/smsServers/smsServerUI.jsp", MenuNames.SMS_SERVER + ADD_PERMISSION);
        featureMenuUrlList.put("/editSmsServer.do", MenuNames.SMS_SERVER + MODIFY_PERMISSION);
        featureMenuUrlList.put("/updateSmsServer.do", MenuNames.SMS_SERVER + MODIFY_PERMISSION);
        featureMenuUrlList.put("/admin/smsServers/UpdateSmsServer.jsp", MenuNames.SMS_SERVER + MODIFY_PERMISSION);
        featureMenuUrlList.put("/deleteSmsServer.do", MenuNames.SMS_SERVER + DELETE_PERMISSION);
        featureMenuUrlList.put("/Download/DownloadSMSServers", MenuNames.SMS_SERVER);

        /**
         * 7 Voice Server
         */
        featureMenuUrlList.put("/voiceServerListInfo.do", MenuNames.VOICE_SERVER);
        featureMenuUrlList.put("/admin/voiceServers/voiceServerList.jsp", MenuNames.VOICE_SERVER);
        featureMenuUrlList.put("/addVoiceServerInfo.do", MenuNames.VOICE_SERVER + ADD_PERMISSION);
        featureMenuUrlList.put("/admin/voiceServers/voiceServerUI.jsp", MenuNames.VOICE_SERVER + ADD_PERMISSION);
        featureMenuUrlList.put("/editVoiceServer.do", MenuNames.VOICE_SERVER + MODIFY_PERMISSION);
        featureMenuUrlList.put("/updateVoiceServer.do", MenuNames.VOICE_SERVER + MODIFY_PERMISSION);
        featureMenuUrlList.put("/admin/voiceServers/UpdateVoiceServer.jsp", MenuNames.VOICE_SERVER + MODIFY_PERMISSION);
        featureMenuUrlList.put("/deleteVoiceServer.do", MenuNames.VOICE_SERVER + DELETE_PERMISSION);
        featureMenuUrlList.put("/Download/DownloadVoiceServers", MenuNames.VOICE_SERVER);

//        featureMenuUrlList.put("", MenuNames.PAGE_O_MEDIA);  //8 Page
        /**
         * 9 Manage Pages
         */
        featureMenuUrlList.put("/newsPortalListInfo.do", MenuNames.PAGES);
        featureMenuUrlList.put("/admin/newsPortal/newsPortalList.jsp", MenuNames.PAGES);
        featureMenuUrlList.put("/makeDiscoverable.do", MenuNames.PAGES);
        featureMenuUrlList.put("/editPageWeight.do", MenuNames.PAGES + MODIFY_PERMISSION);
        featureMenuUrlList.put("/updatePageWeight.do", MenuNames.PAGES + MODIFY_PERMISSION);
        featureMenuUrlList.put("/admin/newsPortal/updatePageWeight.jsp", MenuNames.PAGES + MODIFY_PERMISSION);

        /**
         * 10 Category
         */
        featureMenuUrlList.put("/newsPortalCategoryListInfo.do", MenuNames.CATEGORY);
        featureMenuUrlList.put("/admin/newsPortal/category/newsPortalCategoryList.jsp", MenuNames.CATEGORY);
        featureMenuUrlList.put("/addNewsPortalCategoryInfo.do", MenuNames.CATEGORY + ADD_PERMISSION);
        featureMenuUrlList.put("/admin/newsPortal/category/newsPortalCategoryUI.jsp", MenuNames.CATEGORY + ADD_PERMISSION);
        featureMenuUrlList.put("/editNewsPortalCategory.do", MenuNames.CATEGORY + MODIFY_PERMISSION);
        featureMenuUrlList.put("/updateNewsPortalCategory.do", MenuNames.CATEGORY + MODIFY_PERMISSION);
        featureMenuUrlList.put("/admin/newsPortal/category/UpdateNewsPortalCategory.jsp", MenuNames.CATEGORY + MODIFY_PERMISSION);
        featureMenuUrlList.put("/deleteNewsPortalCategory.do", MenuNames.CATEGORY + DELETE_PERMISSION);
        featureMenuUrlList.put("/Download/DownloadNewsPortalCategories", MenuNames.CATEGORY);

        /**
         * 11 Feed Type
         */
        featureMenuUrlList.put("/newsPortalTypeListInfo.do", MenuNames.FEED_TYPE);
        featureMenuUrlList.put("/admin/newsPortal/type/newsPortalTypeList.jsp", MenuNames.FEED_TYPE);
        featureMenuUrlList.put("/addNewsPortalTypeInfo.do", MenuNames.FEED_TYPE + ADD_PERMISSION);
        featureMenuUrlList.put("/admin/newsPortal/type/newsPortalTypeUI.jsp", MenuNames.FEED_TYPE + ADD_PERMISSION);
        featureMenuUrlList.put("/editNewsPortalType.do", MenuNames.FEED_TYPE + MODIFY_PERMISSION);
        featureMenuUrlList.put("/updateNewsPortalType.do", MenuNames.FEED_TYPE + MODIFY_PERMISSION);
        featureMenuUrlList.put("/admin/newsPortal/type/UpdateNewsPortalType.jsp", MenuNames.FEED_TYPE + MODIFY_PERMISSION);
        featureMenuUrlList.put("/deleteNewsPortalType.do", MenuNames.FEED_TYPE + DELETE_PERMISSION);
        featureMenuUrlList.put("/Download/DownloadNewsPortalTypes", MenuNames.FEED_TYPE);

        /**
         * 12 Breaking Feed
         */
        featureMenuUrlList.put("/breakingNewsListInfo.do", MenuNames.BREAKING_FEED);
        featureMenuUrlList.put("/admin/breakingNews/breakingNewsList.jsp", MenuNames.BREAKING_FEED);
        featureMenuUrlList.put("/declareOrRemoveBreaking.do", MenuNames.BREAKING_FEED + MODIFY_PERMISSION);

        /**
         * 13 Media
         */
        featureMenuUrlList.put("/mediaListInfo.do", MenuNames.MEDIA);
        featureMenuUrlList.put("/admin/media/mediaList.jsp", MenuNames.MEDIA);
        featureMenuUrlList.put("/makeTrending.do", MenuNames.MEDIA);
        featureMenuUrlList.put("/mediaDetails.do", MenuNames.MEDIA);
        featureMenuUrlList.put("/admin/media/mediaDetails.jsp", MenuNames.MEDIA);

//        featureMenuUrlList.put("", MenuNames.LIVE_O_CHANNEL);  //14 Live / Channel
        /**
         * 15 Channel
         */
        featureMenuUrlList.put("/channelList.do", MenuNames.CHANNEL);
        featureMenuUrlList.put("/admin/channel/channelList.jsp", MenuNames.CHANNEL);
        featureMenuUrlList.put("/ActivateOrDeactivateChannel.do", MenuNames.CHANNEL);
        featureMenuUrlList.put("/activeOrDeactiveChannel.do", MenuNames.CHANNEL);
        featureMenuUrlList.put("/channelMediaList.do", MenuNames.CHANNEL);
        featureMenuUrlList.put("/admin/channel/channelMediaList.jsp", MenuNames.CHANNEL);
        featureMenuUrlList.put("/editChannel.do", MenuNames.CHANNEL + MODIFY_PERMISSION);
        featureMenuUrlList.put("/updateChannel.do", MenuNames.CHANNEL + MODIFY_PERMISSION);
        featureMenuUrlList.put("/admin/channel/updateChannel.jsp", MenuNames.CHANNEL + MODIFY_PERMISSION);

        /**
         * 16 Live Room
         */
        featureMenuUrlList.put("/showLiveRoomList.do", MenuNames.LIVE_ROOM);
        featureMenuUrlList.put("/admin/liveRoom/liveRoomList.jsp", MenuNames.LIVE_ROOM);
        featureMenuUrlList.put("/addLiveRoom.do", MenuNames.LIVE_ROOM + ADD_PERMISSION);
        featureMenuUrlList.put("/admin/liveRoom/liveRoomUI.jsp", MenuNames.LIVE_ROOM + ADD_PERMISSION);
        featureMenuUrlList.put("/editLiveRoom.do", MenuNames.LIVE_ROOM + MODIFY_PERMISSION);
        featureMenuUrlList.put("/saveEditLiveRoomList.do", MenuNames.LIVE_ROOM + MODIFY_PERMISSION);
        featureMenuUrlList.put("/updateLiveRoom.do", MenuNames.LIVE_ROOM + MODIFY_PERMISSION);
        featureMenuUrlList.put("/admin/liveRoom/updateLiveRoom.jsp", MenuNames.LIVE_ROOM + MODIFY_PERMISSION);
        featureMenuUrlList.put("/deleteLiveRoom.do", MenuNames.LIVE_ROOM + DELETE_PERMISSION);

//        featureMenuUrlList.put("", MenuNames.COMMON);  //17 Common
        /**
         * 18 App Constants
         */
        featureMenuUrlList.put("/appConstantsListInfo.do", MenuNames.APP_CONSTANT);
        featureMenuUrlList.put("/admin/appConstants/appConstantsList.jsp", MenuNames.APP_CONSTANT);
        featureMenuUrlList.put("/addAppConstantsInfo.do", MenuNames.APP_CONSTANT + ADD_PERMISSION);
        featureMenuUrlList.put("/appConstantDefaultValue.do", MenuNames.APP_CONSTANT + ADD_PERMISSION);
        featureMenuUrlList.put("/admin/appConstants/appConstantsUI.jsp", MenuNames.APP_CONSTANT + ADD_PERMISSION);
        featureMenuUrlList.put("/editAppConstants.do", MenuNames.APP_CONSTANT + MODIFY_PERMISSION);
        featureMenuUrlList.put("/updateAppConstants.do", MenuNames.APP_CONSTANT + MODIFY_PERMISSION);
        featureMenuUrlList.put("/admin/appConstants/UpdateAppConstants.jsp", MenuNames.APP_CONSTANT + MODIFY_PERMISSION);
        featureMenuUrlList.put("/deleteAppConstants.do", MenuNames.APP_CONSTANT + DELETE_PERMISSION);
        featureMenuUrlList.put("/Download/DownloadAppConstants", MenuNames.APP_CONSTANT);

        /**
         * 19 App Stores
         */
        featureMenuUrlList.put("/appStoresListInfo.do", MenuNames.APP_STORE);
        featureMenuUrlList.put("/admin/appStores/appStoresList.jsp", MenuNames.APP_STORE);
        featureMenuUrlList.put("/addAppStoresInfo.do", MenuNames.APP_STORE + ADD_PERMISSION);
        featureMenuUrlList.put("/admin/appStores/appStoresUI.jsp", MenuNames.APP_STORE + ADD_PERMISSION);
        featureMenuUrlList.put("/editAppStores.do", MenuNames.APP_STORE + MODIFY_PERMISSION);
        featureMenuUrlList.put("/updateAppStores.do", MenuNames.APP_STORE + MODIFY_PERMISSION);
        featureMenuUrlList.put("/admin/appStores/UpdateAppStores.jsp", MenuNames.APP_STORE + MODIFY_PERMISSION);
        featureMenuUrlList.put("deleteAppStores.do", MenuNames.APP_STORE + DELETE_PERMISSION);

        /**
         * 20 Log Settings
         */
        featureMenuUrlList.put("/logSettingListInfo.do", MenuNames.LOG_SETTING);
        featureMenuUrlList.put("/admin/logSettings/logSettingList.jsp", MenuNames.LOG_SETTING);
        featureMenuUrlList.put("/addLogSettingInfo.do", MenuNames.LOG_SETTING + ADD_PERMISSION);
        featureMenuUrlList.put("/admin/logSettings/logSettingUI.jsp", MenuNames.LOG_SETTING + ADD_PERMISSION);
        featureMenuUrlList.put("/editLogSettingInfo.do", MenuNames.LOG_SETTING + MODIFY_PERMISSION);
        featureMenuUrlList.put("/updateLogSettingInfo.do", MenuNames.LOG_SETTING + MODIFY_PERMISSION);
        featureMenuUrlList.put("/admin/logSettings/UpdateLogSetting.jsp", MenuNames.LOG_SETTING + MODIFY_PERMISSION);
        featureMenuUrlList.put("/operationLogEdit.do", MenuNames.LOG_SETTING + MODIFY_PERMISSION);
        featureMenuUrlList.put("/deleteLogSettingInfo.do", MenuNames.LOG_SETTING + DELETE_PERMISSION);

        /**
         * 21 Spam Reasons
         */
        featureMenuUrlList.put("/spamReasonsListInfo.do", MenuNames.SPAM_REASON);
        featureMenuUrlList.put("/admin/spamReasons/spamReasonsList.jsp", MenuNames.SPAM_REASON);
        featureMenuUrlList.put("/addSpamReasonsInfo.do", MenuNames.SPAM_REASON + ADD_PERMISSION);
        featureMenuUrlList.put("/admin/spamReasons/spamReasonsUI.jsp", MenuNames.SPAM_REASON + ADD_PERMISSION);
        featureMenuUrlList.put("/spamReasonDefaultValue.do", MenuNames.SPAM_REASON + ADD_PERMISSION);
        featureMenuUrlList.put("/editSpamReasons.do", MenuNames.SPAM_REASON + MODIFY_PERMISSION);
        featureMenuUrlList.put("/updateSpamReasons.do", MenuNames.SPAM_REASON + MODIFY_PERMISSION);
        featureMenuUrlList.put("/admin/spamReasons/UpdateSpamReasons.jsp", MenuNames.SPAM_REASON + MODIFY_PERMISSION);
        featureMenuUrlList.put("/deleteSpamReasons.do", MenuNames.SPAM_REASON + DELETE_PERMISSION);

        /**
         * 22 Settings
         */
        featureMenuUrlList.put("/settingListInfo.do", MenuNames.SETTING);
        featureMenuUrlList.put("/addSettingInfo.do", MenuNames.SETTING);
        featureMenuUrlList.put("/admin/settings/settingUI.jsp", MenuNames.SETTING + ADD_PERMISSION);
        featureMenuUrlList.put("/admin/settings/settingList.jsp", MenuNames.SETTING + ADD_PERMISSION);
        featureMenuUrlList.put("/editSettingInfo.do", MenuNames.SETTING + MODIFY_PERMISSION);
        featureMenuUrlList.put("/editListSettingInfo.do", MenuNames.SETTING + MODIFY_PERMISSION);
        featureMenuUrlList.put("/updateSettingInfo.do", MenuNames.SETTING + MODIFY_PERMISSION);
        featureMenuUrlList.put("/updateListSettingInfo.do", MenuNames.SETTING + MODIFY_PERMISSION);
        featureMenuUrlList.put("/admin/settings/UpdateSetting.jsp", MenuNames.SETTING + MODIFY_PERMISSION);
        featureMenuUrlList.put("/deleteSettingInfo.do", MenuNames.SETTING + DELETE_PERMISSION);

        /**
         * 23 Channel Servers
         */
        featureMenuUrlList.put("/channelServerListInfo.do", MenuNames.CHANNEL_SERVER);
        featureMenuUrlList.put("/admin/channelservers/channelServersList.jsp", MenuNames.CHANNEL_SERVER);
        featureMenuUrlList.put("/reassignChannelServer.do", MenuNames.CHANNEL_SERVER);
        featureMenuUrlList.put("/channelServer.do", MenuNames.CHANNEL_SERVER + ADD_PERMISSION);
        featureMenuUrlList.put("/admin/channelservers/channelServerUI.jsp", MenuNames.CHANNEL_SERVER + ADD_PERMISSION);

        /**
         * 24 Streaming Servers
         */
        featureMenuUrlList.put("/streamingServerListInfo.do", MenuNames.CHANNEL_SERVER);
        featureMenuUrlList.put("/admin/streamingservers/streamingServersList.jsp", MenuNames.CHANNEL_SERVER);
        featureMenuUrlList.put("/reassignStreamingServer.do", MenuNames.CHANNEL_SERVER);

        /**
         * 25 Ring Studio
         */
        featureMenuUrlList.put("/ringStudioListInfo.do", MenuNames.RING_STUDIO);
        featureMenuUrlList.put("/admin/ringstudio/ringStudioList.jsp", MenuNames.RING_STUDIO);
        featureMenuUrlList.put("/ringStudio.do", MenuNames.RING_STUDIO + ADD_PERMISSION);
        featureMenuUrlList.put("/admin/ringstudio/ringStudioUI.jsp", MenuNames.RING_STUDIO + ADD_PERMISSION);

//        featureMenuUrlList.put("", MenuNames.NOTIFY);  //26 Notify
        /**
         * 27 Notify Auth Server
         */
        featureMenuUrlList.put("/authServerNotification.do", MenuNames.NOTIFY_AUTH_SERVER);
        featureMenuUrlList.put("/admin/authServerNotification/authServerNotification.jsp", MenuNames.NOTIFY_AUTH_SERVER);
        featureMenuUrlList.put("/notifyAuthServer.do", MenuNames.NOTIFY_AUTH_SERVER);

        /**
         * 28 Notify Auth Controller
         */
        featureMenuUrlList.put("/admin/authControllerReload/authControllerReload.jsp", MenuNames.NOTIFY_AUTH_CONTROLLER);
        featureMenuUrlList.put("/authControllerReload.do", MenuNames.NOTIFY_AUTH_CONTROLLER);

        /**
         * 29 Check Log
         */
        featureMenuUrlList.put("/reloadCheckLog.do", MenuNames.CHECK_LOG);
        featureMenuUrlList.put("/admin/checkLogInfo/checkLogList.jsp", MenuNames.CHECK_LOG);

//        featureMenuUrlList.put("", MenuNames.ADMIN_USER);  //30 Users
        /**
         * 31 Manage Users
         */
        featureMenuUrlList.put("/usersListInfo.do", MenuNames.ADMIN_USER);
        featureMenuUrlList.put("/admin/users/usersList.jsp", MenuNames.ADMIN_USER);
        featureMenuUrlList.put("/addUserInfo.do", MenuNames.ADMIN_USER + ADD_PERMISSION);
        featureMenuUrlList.put("/admin/users/userUI.jsp", MenuNames.ADMIN_USER + ADD_PERMISSION);
        featureMenuUrlList.put("/editUser.do", MenuNames.ADMIN_USER + MODIFY_PERMISSION);
        featureMenuUrlList.put("/updateUser.do", MenuNames.ADMIN_USER + MODIFY_PERMISSION);
        featureMenuUrlList.put("/admin/users/updateUser.jsp", MenuNames.ADMIN_USER + MODIFY_PERMISSION);
        featureMenuUrlList.put("/deleteUserInfo.do", MenuNames.ADMIN_USER + DELETE_PERMISSION);

        /**
         * 33 User Role
         */
        featureMenuUrlList.put("/showUserRoleList.do", MenuNames.ADMIN_USER_ROLE);
        featureMenuUrlList.put("/admin/userFeatureManagement/userRole/showUserRoleList.jsp", MenuNames.ADMIN_USER_ROLE);
        featureMenuUrlList.put("/addUseRole.do", MenuNames.ADMIN_USER_ROLE + ADD_PERMISSION);
        featureMenuUrlList.put("/admin/userFeatureManagement/userRole/userRoleUI.jsp", MenuNames.ADMIN_USER_ROLE + ADD_PERMISSION);
        featureMenuUrlList.put("/editUserRole.do", MenuNames.ADMIN_USER_ROLE + MODIFY_PERMISSION);
        featureMenuUrlList.put("/updateUserRole.do", MenuNames.ADMIN_USER_ROLE + MODIFY_PERMISSION);
        featureMenuUrlList.put("/admin/userFeatureManagement/userRole/UpdateUserRole.jsp", MenuNames.ADMIN_USER_ROLE + MODIFY_PERMISSION);
        featureMenuUrlList.put("/deleteUserRole.do", MenuNames.ADMIN_USER_ROLE + DELETE_PERMISSION);

        /**
         * 34 Feature
         */
        featureMenuUrlList.put("/showFeatureList.do", MenuNames.PROJECT_FEATURE);
        featureMenuUrlList.put("/admin/userFeatureManagement/feature/showFeatureList.jsp", MenuNames.PROJECT_FEATURE);
        featureMenuUrlList.put("/addFeature.do", MenuNames.PROJECT_FEATURE + ADD_PERMISSION);
        featureMenuUrlList.put("/admin/userFeatureManagement/feature/featureUI.jsp", MenuNames.PROJECT_FEATURE + ADD_PERMISSION);
        featureMenuUrlList.put("/editFeature.do", MenuNames.PROJECT_FEATURE + MODIFY_PERMISSION);
        featureMenuUrlList.put("/updateFeature.do", MenuNames.PROJECT_FEATURE + MODIFY_PERMISSION);
        featureMenuUrlList.put("/admin/userFeatureManagement/feature/UpdateFeature.jsp", MenuNames.PROJECT_FEATURE + MODIFY_PERMISSION);
        featureMenuUrlList.put("/deleteFeature.do", MenuNames.PROJECT_FEATURE + DELETE_PERMISSION);

        /**
         * 35 Feature Role Mapping
         */
        featureMenuUrlList.put("/showFeatureRole.do", MenuNames.FEATURE_ROLE_MAPPING);
        featureMenuUrlList.put("/admin/userFeatureManagement/feature-role-manage.jsp", MenuNames.FEATURE_ROLE_MAPPING);

        /**
         * 36 Reload
         */
        featureMenuUrlList.put("/reloadServers.do", MenuNames.RELOAD);
        featureMenuUrlList.put("/reload.do", MenuNames.RELOAD);
        featureMenuUrlList.put("/admin/reload/reload.jsp", MenuNames.RELOAD);

//        featureMenuUrlList.put("", MenuNames.UTILITY);  //37 Ring ID
        /**
         * 38 Reset Retry Limit
         */
        featureMenuUrlList.put("/admin/ringId/resetRetryLimit/reset-retry-limit.jsp", MenuNames.RESET_RETRY_LIMIT);
        featureMenuUrlList.put("/resetRetryLimit.do", MenuNames.RESET_RETRY_LIMIT);

        /**
         * 39 Reset Password
         */
        featureMenuUrlList.put("/admin/ringId/resetPassword/resetPassword.jsp", MenuNames.RESET_PASSWORD);
        featureMenuUrlList.put("/resetPassword.do", MenuNames.RESET_PASSWORD);

        /**
         * 40 Unverify Number
         */
        featureMenuUrlList.put("/admin/ringId/unverifyNumber/unverifyNumber.jsp", MenuNames.UNVERIFY_NUMBER);
        featureMenuUrlList.put("/unverifyNumber.do", MenuNames.UNVERIFY_NUMBER);

        /**
         * 41 Special User Accounts
         */
        featureMenuUrlList.put("/showCelebrityList.do", MenuNames.SPECIAL_USER_ACCOUNT);
        featureMenuUrlList.put("/admin/ringId/celebrity/celebrityList.jsp", MenuNames.SPECIAL_USER_ACCOUNT);
        featureMenuUrlList.put("/makeCelebrity.do", MenuNames.SPECIAL_USER_ACCOUNT + ADD_PERMISSION);
        featureMenuUrlList.put("/admin/ringId/celebrity/makeCelebrity.jsp", MenuNames.SPECIAL_USER_ACCOUNT + ADD_PERMISSION);
        featureMenuUrlList.put("/celebrityCategory.do", MenuNames.SPECIAL_USER_ACCOUNT + MODIFY_PERMISSION);
        featureMenuUrlList.put("/admin/ringId/celebrity/updateCelebrityCategory.jsp", MenuNames.SPECIAL_USER_ACCOUNT + MODIFY_PERMISSION);
        featureMenuUrlList.put("/makeCelebrityCategory.do", MenuNames.SPECIAL_USER_ACCOUNT + MODIFY_PERMISSION);
        featureMenuUrlList.put("/updateFeaturedUser.do", MenuNames.SPECIAL_USER_ACCOUNT + MODIFY_PERMISSION);
        featureMenuUrlList.put("/admin/ringId/celebrity/updateFeaturedStreamUser.jsp", MenuNames.SPECIAL_USER_ACCOUNT + MODIFY_PERMISSION);
        featureMenuUrlList.put("/editFeaturedUser.do", MenuNames.SPECIAL_USER_ACCOUNT + MODIFY_PERMISSION);
        featureMenuUrlList.put("/editCelebrityInfo.do", MenuNames.SPECIAL_USER_ACCOUNT + MODIFY_PERMISSION);
        featureMenuUrlList.put("/updateCelebrityInfo.do", MenuNames.SPECIAL_USER_ACCOUNT + MODIFY_PERMISSION);
        featureMenuUrlList.put("/deleteFeaturedUser.do", MenuNames.SPECIAL_USER_ACCOUNT + DELETE_PERMISSION);
        featureMenuUrlList.put("/updateMultipleFeaturedUser.do", MenuNames.SPECIAL_USER_ACCOUNT + MODIFY_PERMISSION);
        featureMenuUrlList.put("/admin/ringId/celebrity/updateMultipleFeaturedStreamUser.jsp", MenuNames.SPECIAL_USER_ACCOUNT + MODIFY_PERMISSION);

        /**
         * 42 Live Verified Settings
         */
        featureMenuUrlList.put("/updateLiveVerificationSettings.do", MenuNames.Live_VERIFIED_SETTING);
        featureMenuUrlList.put("/admin/ringId/liveVerifiedSettings/liveVerifiedSettings.jsp", MenuNames.Live_VERIFIED_SETTING);

        /**
         * 43 Live Info
         */
        /**
         * 44 Active Users
         */
        featureMenuUrlList.put("/admin/ringId/VerifiedUnverifiedLivestreamUser/livestreamUserList.jsp", MenuNames.ACTIVE_USERS);
        featureMenuUrlList.put("/showLivestreamUser.do", MenuNames.ACTIVE_USERS);
        featureMenuUrlList.put("/verifyLivestreamUser.do", MenuNames.ACTIVE_USERS);

        /**
         * 45 Live History
         */
        featureMenuUrlList.put("/admin/liveInfo/liveInfoListByRingId.jsp", MenuNames.LIVE_HISTORY);
        featureMenuUrlList.put("/admin/liveInfo/liveInfoDetails.jsp", MenuNames.LIVE_HISTORY);
        featureMenuUrlList.put("/showLiveInfoListByRingId.do", MenuNames.LIVE_HISTORY);
        featureMenuUrlList.put("/showLiveInfoDetailsList.do", MenuNames.LIVE_HISTORY);
        featureMenuUrlList.put("/showLiveInfoList.do", MenuNames.LIVE_HISTORY);
        featureMenuUrlList.put("/admin/liveInfo/liveInfoList.jsp", MenuNames.LIVE_HISTORY);

        /**
         * 47 Verify Number
         */
        featureMenuUrlList.put("/admin/ringId/verifyNumber/verifyNumber.jsp", MenuNames.VERIFY_NUMBER);
        featureMenuUrlList.put("/verifyNumber.do", MenuNames.VERIFY_NUMBER);

        /**
         * 48 Celebrity Schedule
         */
        featureMenuUrlList.put("/showCelebrityScheduleList.do", MenuNames.CELEBRITY_SCHEDULE);
        featureMenuUrlList.put("/admin/ringId/celebritySchedule/celebrityScheduleList.jsp", MenuNames.CELEBRITY_SCHEDULE);
//        featureMenuUrlList.put("/addCelebritySchedule.do", MenuNames.CELEBRITY_SCHEDULE + ADD_PERMISSION);
//        featureMenuUrlList.put("/admin/ringId/celebritySchedule/addCelebritySchedule.jsp", MenuNames.CELEBRITY_SCHEDULE + ADD_PERMISSION);
        featureMenuUrlList.put("/editCelebritySchedule.do", MenuNames.CELEBRITY_SCHEDULE + MODIFY_PERMISSION);
        featureMenuUrlList.put("/updateCelebritySchedule.do", MenuNames.CELEBRITY_SCHEDULE + MODIFY_PERMISSION);
        featureMenuUrlList.put("/admin/ringId/celebritySchedule/updateCelebritySchedule.jsp", MenuNames.CELEBRITY_SCHEDULE + MODIFY_PERMISSION);
        featureMenuUrlList.put("/daleteCelebritySchedule.do", MenuNames.CELEBRITY_SCHEDULE + DELETE_PERMISSION);

        /**
         * 49 Permitter IP
         */
        featureMenuUrlList.put("/permittedIpsListInfo.do", MenuNames.PERMITTED_IP);
        featureMenuUrlList.put("/admin/permittedIps/permittedIpsList.jsp", MenuNames.PERMITTED_IP);
        featureMenuUrlList.put("/addPermittedIps.do", MenuNames.PERMITTED_IP + ADD_PERMISSION);
        featureMenuUrlList.put("/admin/permittedIps/permittedIpsUI.jsp", MenuNames.PERMITTED_IP + ADD_PERMISSION);
        featureMenuUrlList.put("/editPermittedIps.do", MenuNames.PERMITTED_IP + MODIFY_PERMISSION);
        featureMenuUrlList.put("/updatePermittedIps.do", MenuNames.PERMITTED_IP + MODIFY_PERMISSION);
        featureMenuUrlList.put("/admin/permittedIps/UpdatePermittedIps.jsp", MenuNames.PERMITTED_IP + MODIFY_PERMISSION);
        featureMenuUrlList.put("/deletePermittedIps.do", MenuNames.PERMITTED_IP + DELETE_PERMISSION);

        /**
         * 50 Coin Server
         */
        featureMenuUrlList.put("/coinServerListInfo.do", MenuNames.COIN_SERVER);
        featureMenuUrlList.put("/admin/coinServers/coinServerList.jsp", MenuNames.COIN_SERVER);
        featureMenuUrlList.put("/addCoinServer.do", MenuNames.COIN_SERVER + ADD_PERMISSION);
        featureMenuUrlList.put("/admin/coinServers/coinServerUI.jsp", MenuNames.COIN_SERVER + ADD_PERMISSION);
        featureMenuUrlList.put("/editCoinServer.do", MenuNames.COIN_SERVER + MODIFY_PERMISSION);
        featureMenuUrlList.put("/updateCoinServer.do", MenuNames.COIN_SERVER + MODIFY_PERMISSION);
        featureMenuUrlList.put("/admin/coinServers/UpdateCoinServer.jsp", MenuNames.COIN_SERVER + MODIFY_PERMISSION);
        featureMenuUrlList.put("/deleteCoinServer.do", MenuNames.COIN_SERVER + DELETE_PERMISSION);

        /**
         * 51 Special Users
         */
        /**
         * 52 Change RingID
         */
        featureMenuUrlList.put("/admin/ringId/changeRingId/changeRingId.jsp", MenuNames.CHANGE_RINGID);
        featureMenuUrlList.put("/changeRingId.do", MenuNames.CHANGE_RINGID);

        /**
         * 53 Update Restriction Time
         */
        featureMenuUrlList.put("/admin/ringId/updateRestrictionTime/updateRestrictionTime.jsp", MenuNames.LIMIT_O_TIME_UPDATE);
        featureMenuUrlList.put("/updateRestrictionTime.do", MenuNames.LIMIT_O_TIME_UPDATE);

        /**
         * 54 Latest OTP
         */
        featureMenuUrlList.put("/admin/ringId/latestOTP/latestOTP.jsp", MenuNames.LATEST_OTP);
        featureMenuUrlList.put("/latestOTP.do", MenuNames.LATEST_OTP);
        /**
         * 55 User TableId From RingId
         */
        featureMenuUrlList.put("/admin/ringId/userTableIdFromRingId/userTableIdFromRingId.jsp", MenuNames.USERID_FROM_RINGID);
        featureMenuUrlList.put("/userTableIdFromRingId.do", MenuNames.USERID_FROM_RINGID);

        /**
         * 56 Tariff Add
         */
        featureMenuUrlList.put("/showTariffList.do", MenuNames.CALL_O_LIVE_SUBSCRIPTION_FEE);
        featureMenuUrlList.put("/admin/ringId/tariff/tariffList.jsp", MenuNames.CALL_O_LIVE_SUBSCRIPTION_FEE);
        featureMenuUrlList.put("/admin/ringId/tariff/tariffAdd.jsp", MenuNames.CALL_O_LIVE_SUBSCRIPTION_FEE + ADD_PERMISSION);
        featureMenuUrlList.put("/tariffAdd.do", MenuNames.CALL_O_LIVE_SUBSCRIPTION_FEE + ADD_PERMISSION);
        featureMenuUrlList.put("/deleteTariff.do", MenuNames.CALL_O_LIVE_SUBSCRIPTION_FEE + DELETE_PERMISSION);

        /**
         * 57 Search User
         */
        featureMenuUrlList.put("/admin/ringId/searchUser/searchUser.jsp", MenuNames.SEARCH_USER);
        featureMenuUrlList.put("/searchUser.do", MenuNames.SEARCH_USER);
        featureMenuUrlList.put("/searchUser2.do", MenuNames.SEARCH_USER);
        featureMenuUrlList.put("/admin/ringId/searchUser/ringIDUserProfile.jsp", MenuNames.SEARCH_USER);
        featureMenuUrlList.put("/searchUserContacts.do", MenuNames.SEARCH_USER);
        featureMenuUrlList.put("/admin/ringId/searchUser/searchUserContacts.jsp", MenuNames.SEARCH_USER);

        /**
         * 58 Deactivate Reasons
         */
        featureMenuUrlList.put("/deactivateReasonsListInfo.do", MenuNames.DEACTIVATE_REASON);
        featureMenuUrlList.put("/admin/deactivateReasons/deactivateReasonsList.jsp", MenuNames.DEACTIVATE_REASON);
        featureMenuUrlList.put("/admin/deactivateReasons/deactivateReasonsUI.jsp", MenuNames.DEACTIVATE_REASON + ADD_PERMISSION);
        featureMenuUrlList.put("/addDeactivateReasons.do", MenuNames.DEACTIVATE_REASON + ADD_PERMISSION);

        /**
         * 59 Live Summary
         */
        featureMenuUrlList.put("/liveSummary.do", MenuNames.LIVE_SUMMARY);
        featureMenuUrlList.put("/admin/liveInfo/liveInfoSummary.jsp", MenuNames.LIVE_SUMMARY + ADD_PERMISSION);

        /**
         * 60 Donation Pages
         */
        featureMenuUrlList.put("/showDonationPageList.do", MenuNames.DONATION_PAGE);
        featureMenuUrlList.put("/admin/donationPage/donationPageList.jsp", MenuNames.DONATION_PAGE);
        featureMenuUrlList.put("/admin/donationPage/donationPageUI.jsp", MenuNames.DONATION_PAGE + ADD_PERMISSION);
        featureMenuUrlList.put("/addDonationPage.do", MenuNames.DONATION_PAGE + ADD_PERMISSION);
        featureMenuUrlList.put("/editDonationPage.do", MenuNames.DONATION_PAGE + MODIFY_PERMISSION);
        featureMenuUrlList.put("/updateDonationPage.do", MenuNames.DONATION_PAGE + MODIFY_PERMISSION);
        featureMenuUrlList.put("/deactivateDonationPage.do", MenuNames.DONATION_PAGE + DELETE_PERMISSION);

        /**
         * 61 User Pages
         */
        featureMenuUrlList.put("/showUserPages.do", MenuNames.USER_PAGE);
        featureMenuUrlList.put("/admin/userPages/userPages.jsp", MenuNames.USER_PAGE);
        featureMenuUrlList.put("/addUserPage.do", MenuNames.USER_PAGE + ADD_PERMISSION);
        featureMenuUrlList.put("/admin/userPages/userPageUI.jsp", MenuNames.USER_PAGE + ADD_PERMISSION);
        featureMenuUrlList.put("/editUserPage.do", MenuNames.USER_PAGE + MODIFY_PERMISSION);
        featureMenuUrlList.put("/admin/userPages/updateUserPage.jsp", MenuNames.USER_PAGE + MODIFY_PERMISSION);
        featureMenuUrlList.put("/updateUserPage.do", MenuNames.USER_PAGE + MODIFY_PERMISSION);
        featureMenuUrlList.put("/deactiveUserPages.do", MenuNames.USER_PAGE + DELETE_PERMISSION);

        /**
         * 62 Channel Category
         */
        featureMenuUrlList.put("/showChannelCategory.do", MenuNames.CHANNEL_CATEGORY);
        featureMenuUrlList.put("/admin/channel/channelCategory/channelCategoryList.jsp", MenuNames.CHANNEL_CATEGORY);
        featureMenuUrlList.put("/addChannelCategory.do", MenuNames.CHANNEL_CATEGORY + ADD_PERMISSION);
        featureMenuUrlList.put("/admin/channel/channelCategory/channelCategoryUI.jsp", MenuNames.CHANNEL_CATEGORY + ADD_PERMISSION);
        featureMenuUrlList.put("/editChannelCategory.do", MenuNames.CHANNEL_CATEGORY + MODIFY_PERMISSION);
        featureMenuUrlList.put("/admin/channel/channelCategory/updateChannelCategory.jsp", MenuNames.USER_PAGE + MODIFY_PERMISSION);
        featureMenuUrlList.put("/updateChannelCategory.do", MenuNames.CHANNEL_CATEGORY + MODIFY_PERMISSION);
        featureMenuUrlList.put("/deleteChannelCategory.do", MenuNames.CHANNEL_CATEGORY + DELETE_PERMISSION);

        /**
         * 63 Page Follow Rules
         */
        featureMenuUrlList.put("/admin/pageFollowRules/pageFollowRulesList.jsp", MenuNames.PAGE_FOLLOW_RULE);
        featureMenuUrlList.put("/editPageFollowRules.do", MenuNames.PAGE_FOLLOW_RULE + MODIFY_PERMISSION);
        featureMenuUrlList.put("/admin/pageFollowRules/UpdatePageFollowRules.jsp", MenuNames.PAGE_FOLLOW_RULE + MODIFY_PERMISSION);
        featureMenuUrlList.put("/updatePageFollowRules.do", MenuNames.PAGE_FOLLOW_RULE + MODIFY_PERMISSION);
        featureMenuUrlList.put("/deletePageFollowRules.do", MenuNames.PAGE_FOLLOW_RULE + DELETE_PERMISSION);

        /**
         * 64 Special Room
         */
        featureMenuUrlList.put("/showSpecialRoomList.do", MenuNames.SPECIAL_ROOM);
        featureMenuUrlList.put("/admin/specialRoom/specialRoomList.jsp", MenuNames.SPECIAL_ROOM);
        featureMenuUrlList.put("/addSpecialRoom.do", MenuNames.SPECIAL_ROOM + ADD_PERMISSION);
        featureMenuUrlList.put("/admin/specialRoom/specialRoomUI.jsp", MenuNames.SPECIAL_ROOM + ADD_PERMISSION);
        featureMenuUrlList.put("/editSpecialRoom.do", MenuNames.SPECIAL_ROOM + MODIFY_PERMISSION);
        featureMenuUrlList.put("/updateSpecialRoom.do", MenuNames.SPECIAL_ROOM + MODIFY_PERMISSION);
        featureMenuUrlList.put("/admin/specialRoom/updateSpecialRoom.jsp", MenuNames.SPECIAL_ROOM + MODIFY_PERMISSION);
        featureMenuUrlList.put("/deleteSpecialRoom.do", MenuNames.SPECIAL_ROOM + DELETE_PERMISSION);

        /**
         * 66 Auto Follow Setting
         */
        featureMenuUrlList.put("/autoFollowSetting.do", MenuNames.AUTO_FOLLOW_SETTING);
        featureMenuUrlList.put("/admin/ringId/autoFollowSetting/autoFollowSetting.jsp", MenuNames.AUTO_FOLLOW_SETTING);

        /**
         * 67 Streaming Codec Settings
         */
        featureMenuUrlList.put("/streamingCodecSettingsList.do", MenuNames.STREAMING_CODEC_SETTING);
        featureMenuUrlList.put("/admin/streamingCodecSettings/streamingCodecSettingsList.jsp", MenuNames.STREAMING_CODEC_SETTING);
        featureMenuUrlList.put("/editStreamingCodecSettings.do", MenuNames.STREAMING_CODEC_SETTING + MODIFY_PERMISSION);
        featureMenuUrlList.put("/updateStreamingCodecSettings.do", MenuNames.STREAMING_CODEC_SETTING + MODIFY_PERMISSION);
        featureMenuUrlList.put("/admin/streamingCodecSettings/updatestreamingCodecSettings.jsp", MenuNames.STREAMING_CODEC_SETTING + MODIFY_PERMISSION);
        featureMenuUrlList.put("/deleteStreamingCodecSettings.do", MenuNames.STREAMING_CODEC_SETTING + DELETE_PERMISSION);

        /**
         * News Feed
         */
        featureMenuUrlList.put("/newsfeed.do", MenuNames.NEWS_FEED);
        featureMenuUrlList.put("/admin/ringId/newsfeed/newsfeed.jsp", MenuNames.NEWS_FEED);
        /**
         * News Feed
         */
        featureMenuUrlList.put("/postStatus.do", MenuNames.FEED_POST);
        featureMenuUrlList.put("/admin/ringId/postCelebrityStatus/postCelebrityStatus.jsp", MenuNames.FEED_POST);

        /**
         * 69 Feed Highlights
         */
        featureMenuUrlList.put("/showFeedHighlightsList.do", MenuNames.FEED_HIGHLIGHTS);
        featureMenuUrlList.put("/admin/ringId/feedHighlights/feedHighlightsList.jsp", MenuNames.FEED_HIGHLIGHTS);
        featureMenuUrlList.put("/addFeedHighlights.do", MenuNames.FEED_HIGHLIGHTS + ADD_PERMISSION);
        featureMenuUrlList.put("/editFeedHighlights.do", MenuNames.FEED_HIGHLIGHTS + MODIFY_PERMISSION);
        featureMenuUrlList.put("/updateFeedHighlights.do", MenuNames.FEED_HIGHLIGHTS + MODIFY_PERMISSION);
        featureMenuUrlList.put("/admin/ringId/feedHighlights/updateFeedHighlights.jsp", MenuNames.FEED_HIGHLIGHTS + MODIFY_PERMISSION);
        featureMenuUrlList.put("/deleteFeedHighlights.do", MenuNames.FEED_HIGHLIGHTS + DELETE_PERMISSION);

        /**
         * 70 Floating Menus
         */
        featureMenuUrlList.put("/showFloatingMenuList.do", MenuNames.FLOATING_MENU);
        featureMenuUrlList.put("/admin/floatingMenu/floatingMenuList.jsp", MenuNames.FLOATING_MENU);
        featureMenuUrlList.put("/addFloatingMenu.do", MenuNames.FLOATING_MENU + ADD_PERMISSION);
        featureMenuUrlList.put("/admin/floatingMenu/floatingMenuUI.jsp", MenuNames.FLOATING_MENU + ADD_PERMISSION);
        featureMenuUrlList.put("/editFloatingMenu.do", MenuNames.FLOATING_MENU + MODIFY_PERMISSION);
        featureMenuUrlList.put("/updateFloatingMenu.do", MenuNames.FLOATING_MENU + MODIFY_PERMISSION);
        featureMenuUrlList.put("/admin/floatingMenu/updateFloatingMenu.jsp", MenuNames.FLOATING_MENU + MODIFY_PERMISSION);
        featureMenuUrlList.put("/deleteFloatingMenu.do", MenuNames.FLOATING_MENU + DELETE_PERMISSION);

        /**
         * 71 Floating Menu Slide Item
         */
        featureMenuUrlList.put("/floatingMenuSlideItemList.do", MenuNames.FLOATING_MENU_SLIDE_ITEM);
        featureMenuUrlList.put("/floatingMenuSlideItem.do", MenuNames.FLOATING_MENU_SLIDE_ITEM);
        featureMenuUrlList.put("/admin/floationMenuSlideItem/floatingMenuSlideItemList.jsp", MenuNames.FLOATING_MENU_SLIDE_ITEM);

        /**
         * 72 Cash Wallet Permitted Country
         */
        featureMenuUrlList.put("/showCashWalletPermittedCountryList.do", MenuNames.CASH_WALLET_PERMITTED_COUNTRY);
        featureMenuUrlList.put("/admin/cashWallet/permittedCountryList.jsp", MenuNames.CASH_WALLET_PERMITTED_COUNTRY);
        featureMenuUrlList.put("/addOrUpdateCashWalletPermittedCountry.do", MenuNames.CASH_WALLET_PERMITTED_COUNTRY + ADD_OR_MODIFY_PERMISSION);
        featureMenuUrlList.put("/admin/cashWallet/permittedCountryUI.jsp", MenuNames.CASH_WALLET_PERMITTED_COUNTRY + ADD_PERMISSION);
        featureMenuUrlList.put("/editCashWalletPermittedCountry.do", MenuNames.CASH_WALLET_PERMITTED_COUNTRY + MODIFY_PERMISSION);
        featureMenuUrlList.put("/deleteCashWalletPermittedCountry.do", MenuNames.CASH_WALLET_PERMITTED_COUNTRY + DELETE_PERMISSION);

        /**
         * 74 Old Live History
         */
        featureMenuUrlList.put("/showOldLiveInfoList.do", MenuNames.OLD_LIVE_HISTORY);
        featureMenuUrlList.put("/admin/liveInfo/oldLiveInfoList.jsp", MenuNames.OLD_LIVE_HISTORY);
        featureMenuUrlList.put("/showOldLiveInfoDetailsList.do", MenuNames.OLD_LIVE_HISTORY);
        featureMenuUrlList.put("/admin/liveInfo/oldLiveInfoDetails.jsp", MenuNames.OLD_LIVE_HISTORY);

        /**
         * 75 Event
         */
        featureMenuUrlList.put("/showEventListInfo.do", MenuNames.EVENT);
        featureMenuUrlList.put("/admin/event/eventList.jsp", MenuNames.EVENT);
        featureMenuUrlList.put("/addOrUpdateEvent.do", MenuNames.EVENT + ADD_OR_MODIFY_PERMISSION);
        featureMenuUrlList.put("/admin/event/eventUI.jsp", MenuNames.EVENT + ADD_PERMISSION);
        featureMenuUrlList.put("/editEvent.do", MenuNames.EVENT + MODIFY_PERMISSION);
        featureMenuUrlList.put("/deleteEvent.do", MenuNames.EVENT + DELETE_PERMISSION);

        /**
         * 76 Channel Match Mapped
         */
        featureMenuUrlList.put("/channleMatchMappedInfo.do", MenuNames.MATCH_CHANNEL_MAPPED);
        featureMenuUrlList.put("/channleMatchMapping.do", MenuNames.MATCH_CHANNEL_MAPPED);
        featureMenuUrlList.put("/matchChannelMapping.do", MenuNames.MATCH_CHANNEL_MAPPED + ADD_PERMISSION);
        featureMenuUrlList.put("/admin/sports/matchChannelMapped/channelMatchMappingUI.jsp", MenuNames.MATCH_CHANNEL_MAPPED + ADD_PERMISSION);
        featureMenuUrlList.put("/admin/channel/match/channelMatchMappedList.jsp", MenuNames.MATCH_CHANNEL_MAPPED);
        featureMenuUrlList.put("/channleMatchMapping.do", MenuNames.MATCH_CHANNEL_MAPPED + ADD_PERMISSION);
        featureMenuUrlList.put("/admin/channel/match/channelMatchMappingUI.jsp", MenuNames.MATCH_CHANNEL_MAPPED + ADD_PERMISSION);
        featureMenuUrlList.put("/deleteChannleMatchMapped.do", MenuNames.MATCH_CHANNEL_MAPPED + DELETE_PERMISSION);

        /**
         * 77 Home Match Management
         */
        featureMenuUrlList.put("/homeMatchManage.do", MenuNames.HOME_MATCH_MANAGEMENT);
        featureMenuUrlList.put("/admin/channel/match/homeMatchManage.jsp", MenuNames.HOME_MATCH_MANAGEMENT);

        /**
         * 79 Delete vertual ringid
         */
        featureMenuUrlList.put("/virtualRingIDs.do", MenuNames.DELETE_VIRTUAL_RINGID);
        featureMenuUrlList.put("/admin/ringId/virtualRingId/deleteVirtualRingId.jsp", MenuNames.DELETE_VIRTUAL_RINGID);

        /**
         * 80 RingId Distributor
         */
        featureMenuUrlList.put("/ringIdDistributorList.do", MenuNames.RINGID_DISTRIBUTOR);
        featureMenuUrlList.put("/admin/ringIdDistributor/ringIdDistributorList.jsp", MenuNames.RINGID_DISTRIBUTOR);
        featureMenuUrlList.put("/addRingIdDistributor.do", MenuNames.RINGID_DISTRIBUTOR + ADD_PERMISSION);
        featureMenuUrlList.put("/admin/ringIdDistributor/ringIdDistributorUI.jsp", MenuNames.RINGID_DISTRIBUTOR + ADD_PERMISSION);
        featureMenuUrlList.put("/editRingIdDistributor.do", MenuNames.RINGID_DISTRIBUTOR + MODIFY_PERMISSION);
        featureMenuUrlList.put("/updateRingIdDistributor.do", MenuNames.RINGID_DISTRIBUTOR + MODIFY_PERMISSION);
        featureMenuUrlList.put("/admin/ringIdDistributor/updateRingIdDistributor.jsp", MenuNames.RINGID_DISTRIBUTOR + MODIFY_PERMISSION);
        featureMenuUrlList.put("/deleteRingIdDistributor.do", MenuNames.RINGID_DISTRIBUTOR + DELETE_PERMISSION);

        /**
         * 81 RingBit Log
         */
        featureMenuUrlList.put("/ringBitLogList.do", MenuNames.RINGBIT_LOG);
        featureMenuUrlList.put("/admin/ringBit/ringBitList.jsp", MenuNames.RINGBIT_LOG);

        /**
         * 82 Channel Subscription
         */
        featureMenuUrlList.put("/showChannelSubscription.do", MenuNames.CHANNEL_SUBSCRIPTION);
        featureMenuUrlList.put("/admin/channel/channelSubscription/channelSubscription.jsp", MenuNames.CHANNEL_SUBSCRIPTION);
        featureMenuUrlList.put("/admin/channel/channelSubscription/ChannelSubscriptionUI.jsp", MenuNames.CHANNEL_SUBSCRIPTION + ADD_PERMISSION);
        featureMenuUrlList.put("/addChannelSubscription.do", MenuNames.CHANNEL_SUBSCRIPTION + ADD_PERMISSION);
        featureMenuUrlList.put("/deleteChannelSubscription.do", MenuNames.CHANNEL_SUBSCRIPTION + DELETE_PERMISSION);

        /**
         * 83 Ring Feature
         */
        featureMenuUrlList.put("/showRingFeatureList.do", MenuNames.RING_FEATURE);
        featureMenuUrlList.put("/admin/ringFeature/ringFeatureList.jsp", MenuNames.RING_FEATURE);
        featureMenuUrlList.put("/admin/ringFeature/ringFeatureUI.jsp", MenuNames.RING_FEATURE + ADD_PERMISSION);
        featureMenuUrlList.put("/addRingFeature.do", MenuNames.RING_FEATURE + ADD_PERMISSION);
        featureMenuUrlList.put("/admin/ringFeature/ringFeatureUpdate.jsp", MenuNames.RING_FEATURE + MODIFY_PERMISSION);
        featureMenuUrlList.put("/updateRingFeature.do", MenuNames.RING_FEATURE + MODIFY_PERMISSION);
        featureMenuUrlList.put("/editRingFeature.do", MenuNames.RING_FEATURE + MODIFY_PERMISSION);
        featureMenuUrlList.put("/deleteRingFeature.do", MenuNames.RING_FEATURE + DELETE_PERMISSION);

        /**
         * Ring Feature Mapping
         */
        featureMenuUrlList.put("/showRingFeatureMappingList.do", MenuNames.RING_FEATURE_MAPPING);
        featureMenuUrlList.put("/addRingFeatureMapping.do", MenuNames.RING_FEATURE_MAPPING + ADD_PERMISSION);
        featureMenuUrlList.put("/editRingFeatureMapping.do", MenuNames.RING_FEATURE_MAPPING + MODIFY_PERMISSION);
        featureMenuUrlList.put("/admin/ringFeature/ringFeatureMappingUpdate.jsp", MenuNames.RING_FEATURE_MAPPING + MODIFY_PERMISSION);
        featureMenuUrlList.put("/updateRingFeatureMapping.do", MenuNames.RING_FEATURE_MAPPING + MODIFY_PERMISSION);
        featureMenuUrlList.put("/deleteRingFeatureMapping.do", MenuNames.RING_FEATURE_MAPPING + DELETE_PERMISSION);

        /**
         * 85 RingBit Image & Text
         */
        featureMenuUrlList.put("/showRingBitSettingsList.do", MenuNames.RINGBIT_IMAGE_O_TEXT);
        featureMenuUrlList.put("/admin/ringBit/ringBitSettingList.jsp", MenuNames.RINGBIT_IMAGE_O_TEXT);
        featureMenuUrlList.put("//admin/ringBit/ringBitSettingUI.jsp", MenuNames.RINGBIT_IMAGE_O_TEXT + ADD_PERMISSION);
        featureMenuUrlList.put("/addRingBitSetting.do", MenuNames.RINGBIT_IMAGE_O_TEXT + ADD_PERMISSION);
        featureMenuUrlList.put("/admin/ringbit/updateRingBitSetting.jsp", MenuNames.RINGBIT_IMAGE_O_TEXT + MODIFY_PERMISSION);
        featureMenuUrlList.put("/updateRingBitSetting.do", MenuNames.RINGBIT_IMAGE_O_TEXT + MODIFY_PERMISSION);
        featureMenuUrlList.put("/editRingBitSetting.do", MenuNames.RINGBIT_IMAGE_O_TEXT + MODIFY_PERMISSION);

        /**
         * 86 Payment Method
         */
        featureMenuUrlList.put("/showPaymentMethodList.do", MenuNames.PAYMENT_METHOD);
        featureMenuUrlList.put("/admin/coinExchange/paymentMethodList.jsp", MenuNames.PAYMENT_METHOD);
        featureMenuUrlList.put("/addPaymentMethod.do", MenuNames.PAYMENT_METHOD + ADD_PERMISSION);
        featureMenuUrlList.put("/admin/coinExchange/paymentMethodUI.jsp", MenuNames.PAYMENT_METHOD + ADD_PERMISSION);
        featureMenuUrlList.put("/updatePaymentMethod.do", MenuNames.PAYMENT_METHOD + MODIFY_PERMISSION);
        featureMenuUrlList.put("/admin/coinExchange/updatePaymentMethod.jsp", MenuNames.PAYMENT_METHOD + MODIFY_PERMISSION);
        featureMenuUrlList.put("/editPaymentMethod.do", MenuNames.PAYMENT_METHOD + MODIFY_PERMISSION);
        featureMenuUrlList.put("/deletePaymentMethod.do", MenuNames.PAYMENT_METHOD + DELETE_PERMISSION);

        /**
         * Statistic
         */
        featureMenuUrlList.put("/statistic.do", MenuNames.STATISTIC);
        featureMenuUrlList.put("/admin/statistic/registrationStatistic.jsp", MenuNames.STATISTIC);

        /**
         * Notify Auth Server and Controller
         */
        featureMenuUrlList.put("/admin/authControllerReload/notifyBoth.jsp", MenuNames.NOTIFY_BOTH);
        featureMenuUrlList.put("/authControllerReload.do", MenuNames.NOTIFY_BOTH);

        /**
         * AppUpdateUrl
         */
        featureMenuUrlList.put("/appUpdateUrlListInfo.do", MenuNames.APP_UPDATE_URL);
        featureMenuUrlList.put("/admin/appUpdateUrl/appUpdateUrlList.jsp", MenuNames.APP_UPDATE_URL);
        featureMenuUrlList.put("/addAppUpdateUrlInfo.do", MenuNames.APP_UPDATE_URL + ADD_PERMISSION);
        featureMenuUrlList.put("/admin/appUpdateUrl/appUpdateUrlUI.jsp", MenuNames.APP_UPDATE_URL + ADD_PERMISSION);
        featureMenuUrlList.put("/editAppUpdateUrlInfo.do", MenuNames.APP_UPDATE_URL + MODIFY_PERMISSION);
        featureMenuUrlList.put("/updateAppUpdateUrlInfo.do", MenuNames.APP_UPDATE_URL + MODIFY_PERMISSION);
        featureMenuUrlList.put("/admin/appUpdateUrl/updateAppUpdateUrl.jsp", MenuNames.APP_UPDATE_URL + MODIFY_PERMISSION);
        featureMenuUrlList.put("/deleteAppUpdateUrlInfo.do", MenuNames.APP_UPDATE_URL + DELETE_PERMISSION);

        /**
         * Ambassador
         */
        featureMenuUrlList.put("/showAmbassadorList.do", MenuNames.AMBASSADOR);
        featureMenuUrlList.put("/admin/ambassador/ambassadorList.jsp", MenuNames.AMBASSADOR);
        featureMenuUrlList.put("/addAmbassador.do", MenuNames.AMBASSADOR + ADD_PERMISSION);
        featureMenuUrlList.put("/admin/ambassador/ambassadorUI.jsp", MenuNames.AMBASSADOR + ADD_PERMISSION);
        featureMenuUrlList.put("/searchAmbassadorInfo.do", MenuNames.AMBASSADOR);
        featureMenuUrlList.put("/deleteAmbassador.do", MenuNames.AMBASSADOR + DELETE_PERMISSION);

        /**
         * Update Restriction Time
         */
        featureMenuUrlList.put("/admin/ringId/updateRestrictionTime/updateRestrictionTime.jsp", MenuNames.LIMIT_O_TIME_UPDATE);
        featureMenuUrlList.put("/updateRestrictionTime.do", MenuNames.LIMIT_O_TIME_UPDATE);

        /**
         * Past Recorded Live Feed
         */
        featureMenuUrlList.put("/postRecordedLiveFeed.do", MenuNames.POST_RECORDED_LIVE_FEED);
        featureMenuUrlList.put("/admin/ringId/postCelebrityStatus/postRecordedLiveFeed.jsp", MenuNames.POST_RECORDED_LIVE_FEED);
        featureMenuUrlList.put("/pastLiveFeedList.do", MenuNames.PAST_LIVE_FEED);
        featureMenuUrlList.put("/admin/pastLiveFeed/pastLiveFeedList.jsp", MenuNames.PAST_LIVE_FEED);

        /**
         * Live To Channel
         */
        featureMenuUrlList.put("/liveToChannelListInfo.do", MenuNames.LIVE_TO_CHANNEL);
        featureMenuUrlList.put("/admin/liveToChannel/liveToChannelList.jsp", MenuNames.LIVE_TO_CHANNEL);
        featureMenuUrlList.put("/addLiveToChannelInfo.do", MenuNames.LIVE_TO_CHANNEL + ADD_PERMISSION);
        featureMenuUrlList.put("/admin/liveToChannel/liveToChannelUI.jsp", MenuNames.LIVE_TO_CHANNEL + ADD_PERMISSION);
        featureMenuUrlList.put("/resetLiveTime.do", MenuNames.LIVE_TO_CHANNEL + MODIFY_PERMISSION);
        featureMenuUrlList.put("/editLiveToChannelInfo.do", MenuNames.LIVE_TO_CHANNEL + MODIFY_PERMISSION);
        featureMenuUrlList.put("/deleteLiveToChannelInfo.do", MenuNames.LIVE_TO_CHANNEL + DELETE_PERMISSION);

        /**
         * Profession
         */
        featureMenuUrlList.put("/showProfessionList.do", MenuNames.PROFESSION);
        featureMenuUrlList.put("/admin/profession/professionList.jsp", MenuNames.PROFESSION);
        featureMenuUrlList.put("/addProfessionInfo.do", MenuNames.PROFESSION + ADD_PERMISSION);
        featureMenuUrlList.put("/admin/profession/professionUI.jsp", MenuNames.PROFESSION + ADD_PERMISSION);
        featureMenuUrlList.put("/deleteProfession.do", MenuNames.PROFESSION + DELETE_PERMISSION);

        /**
         * Statistic
         */
        featureMenuUrlList.put("/statistic.do", MenuNames.STATISTIC);
        featureMenuUrlList.put("/admin/statistic/registrationStatistic.jsp", MenuNames.STATISTIC);

        /**
         * Admin Action
         */
        featureMenuUrlList.put("/adminAction.do", MenuNames.ADMIN_ACTION);
        featureMenuUrlList.put("/admin/ringId/adminAction/adminAction.jsp", MenuNames.ADMIN_ACTION);

        /**
         * AutoFollow Pages
         */
        featureMenuUrlList.put("/autoFollowPages.do", MenuNames.AUTO_FOLLOW_PAGES);
        featureMenuUrlList.put("/admin/ringId/autoFollowPages/autoFollowPageList.jsp", MenuNames.AUTO_FOLLOW_PAGES);

        /**
         * AssignPageRingId
         */
        featureMenuUrlList.put("/assignPageRingId.do", MenuNames.ASSIGN_PAGE_RINGID);
        featureMenuUrlList.put("/admin/ringId/assignPageRingId/assignPageRingId.jsp", MenuNames.ASSIGN_PAGE_RINGID);
    }
}
