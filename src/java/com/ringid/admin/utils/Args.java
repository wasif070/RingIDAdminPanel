/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.utils;

/**
 *
 * @author mamun
 */
public class Args {

    public static String getFirst(String args[]) {
        return args != null && args.length > 0 ? args[0] : null;
    }

    public static String getSecond(String args[]) {
        return args != null && args.length > 1 ? args[1] : null;
    }

    public static String getThird(String args[]) {
        return args != null && args.length > 2 ? args[2] : null;
    }

    public static String getFourth(String args[]) {
        return args != null && args.length > 3 ? args[3] : null;
    }
}
