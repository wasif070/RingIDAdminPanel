/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.utils.helper;

import com.google.gson.Gson;
import com.ringid.admin.cassandra.CassandraDAO;
import com.ringid.admin.utils.log.RingLogger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.Logger;
import org.ringid.feedbacks.NewsFeedFeedBack;
import org.ringid.newsfeeds.CassAlbumDTO;
import org.ringid.newsfeeds.CassImageDTO;
import org.ringid.newsfeeds.CassMultiMediaDTO;
import org.ringid.newsfeeds.FeedDTO;
import org.ringid.newsfeeds.ImageDTO;
import org.ringid.newsfeeds.LinkContent;
import org.ringid.newsfeeds.LinkDTO;
import org.ringid.newsfeeds.LocationDTO;
import org.ringid.newsfeeds.MediaAlbumDTO;
import org.ringid.newsfeeds.MediaContentDTO;
import org.ringid.newsfeeds.NewsFeed;
import org.ringid.newsfeeds.StatusTagDTO;
import org.ringid.users.UserBasicInfoDTO;
import org.ringid.utilities.AppConstants;

/**
 *
 * @author reefat
 */
public class Helper {
    private static Helper instance = null;

    public static Helper getInstance() {
        if (instance == null) {
            instance = new Helper();
        }
        return instance;
    }

    public NewsFeedFeedBack addStatus(long sessionUserID, int sessionUserType, NewsFeed newsFeed, long donationPageId) {
        NewsFeedFeedBack feedBack = new NewsFeedFeedBack();
        try {
            long activeUserId = sessionUserID;

            if (newsFeed.getPrivacy() == AppConstants.ZERO) {
                newsFeed.setPrivacy(AppConstants.CASS_PRIVACY_PUBLIC);
            }

            long time = System.currentTimeMillis();
            FeedDTO feedDTO = new FeedDTO();
            feedDTO.setContentType(newsFeed.getFeedType());

            feedDTO.setWallOwnerId(newsFeed.getWallOwnerId());
            feedDTO.setGuestWriterId(sessionUserID);
            feedDTO.setGuestWriterType(sessionUserType);
            feedDTO.setAddTime(time);
            feedDTO.setPrivacy(newsFeed.getPrivacy());
            if (newsFeed.getCommentPrivacy() > 0) {
                feedDTO.setCommentPrivacy(newsFeed.getCommentPrivacy());
            } else {
                feedDTO.setCommentPrivacy(newsFeed.getPrivacy());
            }
            if (newsFeed.getWallOwnerId() > 0) {
                //FnFLogger.getInstance(1).debug("## WallOwnerId : " + newsFeed.getWallOwnerId());
                UserBasicInfoDTO userBasicInfo = CassandraDAO.getInstance().getUserBasicInfo(newsFeed.getWallOwnerId());
                RingLogger.getConfigPortalLogger().debug("userBasicInfo => " + new Gson().toJson(userBasicInfo));
                //FnFLogger.getInstance(1).debug("## WallOwnerType : " + userBasicInfo.getUserType());
                newsFeed.setWallOwnerType(userBasicInfo.getUserType());
            }
            feedDTO.setWallOwnerType(newsFeed.getWallOwnerType());

            //if(feedDTO.getWallOwnerId isCeleb)
            //
            /**
             * Future Implementation. (Currently only Default user is supported
             * as GuestWriter)
             */
            /*
         if (newsFeed.getGuestWriterID() > 0) {
         newsFeed.setGuestWriterType(CassandraDAO.getInstance().getFeedDAO().getUserBasicInfo(newsFeed.getGuestWriterID()).getUserType());
         }*/
            feedDTO.setStatus(newsFeed.getStatus());
            switch (feedDTO.getWallOwnerType()) {
                default: {
                    if (feedDTO.getWallOwnerId() == 0 || feedDTO.getWallOwnerId() == feedDTO.getGuestWriterId()) {
                        feedDTO.setWallOwnerId(sessionUserID);
                        feedDTO.setWallOwnerType(sessionUserType);
                        feedDTO.setGuestWriterId(0);
                        feedDTO.setGuestWriterType(0);
                    }
                }
            }

            MediaAlbumDTO mediaAlbumDTO = newsFeed.getAlbumDTO();
            if (mediaAlbumDTO != null) {
                mediaAlbumDTO.setAddedTime(time);
                mediaAlbumDTO.setUserId(sessionUserID);
                mediaAlbumDTO.setPrivacy(feedDTO.getPrivacy());
                CassAlbumDTO albumDTO = NewsfeedConverter.mediaAlbumDTOToCass(mediaAlbumDTO);
                albumDTO.setInsertImage(newsFeed.getIsSameAlbum() == AppConstants.ZERO);

                if (newsFeed.getFeedType() == AppConstants.SINGLE_IMAGE || newsFeed.getFeedType() == AppConstants.SINGLE_AUDIO || newsFeed.getFeedType() == AppConstants.SINGLE_VIDEO) {
                    albumDTO.setAlbumType(AppConstants.STATIC_ALBUM);
                } else {
                    albumDTO.setAlbumType(AppConstants.CUSTOM_ALBUM);
                }

                List<ImageDTO> imageList = mediaAlbumDTO.getImageList();
                if (imageList != null && !imageList.isEmpty()) {
                    albumDTO.setContentCount(imageList.size());
                    int lastSerial = 0;
                    if (mediaAlbumDTO.getAlbumId() != null) {
                        CassAlbumDTO cassAlbumDTO = CassandraDAO.getInstance().getAlbumDetails(mediaAlbumDTO.getAlbumId(), activeUserId, AppConstants.MEDIA_TYPE_IMAGE, sessionUserID);
                        lastSerial = cassAlbumDTO.getLastSerial();
                    }

                    ArrayList<CassImageDTO> imageDTOList = new ArrayList<>();
                    for (ImageDTO imageDTO : imageList) {
                        CassImageDTO cassImageDTO = new CassImageDTO();
                        cassImageDTO.setImageId(imageDTO.getImageID());
                        cassImageDTO.setCaption(imageDTO.getCaption());
                        cassImageDTO.setImageUrl(imageDTO.getImageURL());
                        cassImageDTO.setImageHeight(imageDTO.getImageHeight());
                        cassImageDTO.setImageWidth(imageDTO.getImageWidth());
                        if (!albumDTO.getInsertImage()) {
                            cassImageDTO.setImageType(albumDTO.getImageType());
                        }
                        cassImageDTO.setSerial(++lastSerial);

                        imageDTOList.add(cassImageDTO);
                    }
                    albumDTO.setLastSerial(lastSerial);
                    albumDTO.setImgDTOs(imageDTOList);
                }

                List<MediaContentDTO> contentDTOList = mediaAlbumDTO.getMediaContentList();
                if (contentDTOList != null && !contentDTOList.isEmpty()) {
                    int lastSerial = 0;
                    if (mediaAlbumDTO.getAlbumId() != null) {
                        //FnFLogger.getInstance(1).debug("albumId --> " + mediaAlbumDTO.getAlbumId() + ", mediaType --> " + mediaAlbumDTO.getMediaType() + ", ownerId --> " + activeUserId + " ,sessionUserId --> " + sessionUserID);
                        CassAlbumDTO cassAlbumDTO = CassandraDAO.getInstance().getAlbumDetails(mediaAlbumDTO.getAlbumId(), activeUserId, mediaAlbumDTO.getMediaType(), activeUserId);
                        //FnFLogger.getInstance(1).debug("cassAlbumDTO --> " + (cassAlbumDTO == null ? "cassAlbumDTO is NULL" : new Gson().toJson(cassAlbumDTO)));
                        lastSerial = cassAlbumDTO.getLastSerial();
                    }
                    albumDTO.setContentCount(contentDTOList.size());
                    ArrayList<CassMultiMediaDTO> multiMediaList = new ArrayList<>();
                    for (MediaContentDTO contentDTO : contentDTOList) {
                        CassMultiMediaDTO cassContentDTO = new CassMultiMediaDTO();
                        cassContentDTO.setArtist(contentDTO.getMediaArtist());
                        cassContentDTO.setTitle(contentDTO.getMediaTitle());
                        cassContentDTO.setThumbnailUrl(contentDTO.getMediaThumbnailURL());
                        cassContentDTO.setMultiMediaUrl(contentDTO.getMediaStreamURL());
                        cassContentDTO.setDuration(contentDTO.getMediaDuration());
                        cassContentDTO.setThumbnailHeight(contentDTO.getThumbImageHeight());
                        cassContentDTO.setThumbnailWidth(contentDTO.getThumbImageWidth());
                        cassContentDTO.setHashTags(contentDTO.getHashTagList());
                        cassContentDTO.setSerial(++lastSerial);

                        multiMediaList.add(cassContentDTO);
                    }
                    albumDTO.setLastSerial(lastSerial);
                    albumDTO.setMultiMediaDTOs(multiMediaList);
                }
                feedDTO.setAlbumDTO(albumDTO);
            }

            LinkDTO linkDTO = newsFeed.getLinkDTO();
            if (linkDTO != null) {
                LinkContent linkContent = new LinkContent();
                linkContent.setLinkType(linkDTO.getLinkType());
                linkContent.setLinkTitle(linkDTO.getLinkTitle());
                linkContent.setLinkURL(linkDTO.getLinkURL());
                linkContent.setLinkDesc(linkDTO.getLinkDesc());
                linkContent.setLinkImageURL(linkDTO.getLinkImageURL());
                linkContent.setLinkDomain(linkDTO.getLinkDomain());

                feedDTO.setLinkContent(linkContent);
            }

            LocationDTO locationDTO = newsFeed.getLocationDTO();
            if (locationDTO != null) {
                feedDTO.setLatLonExists(true);
                if (locationDTO.getLocation() == null || locationDTO.getLocation().isEmpty()) {
                    locationDTO.setLocation(locationDTO.getLatitude() + "-" + locationDTO.getLongitude());
                } else {
                    feedDTO.setLocation(locationDTO.getLocation());
                }
                feedDTO.setLocationLat(locationDTO.getLatitude());
                feedDTO.setLocationLon(locationDTO.getLongitude());
            }

            if (feedDTO.getWallOwnerType() == AppConstants.UserTypeConstants.CELEBRITY) {
                feedDTO.setLive(newsFeed.getLive());
                feedDTO.setLiveType(newsFeed.getLiveType());
            }

            feedDTO.setParentFeedId(newsFeed.getSharedFeedID());
            feedDTO.setSaveTime(time);

            feedDTO.setValidity(newsFeed.getValidity());

            if (newsFeed.getMoodIds() != null) {
                List<Integer> feelingsList = new ArrayList<>();
                for (int moodID : newsFeed.getMoodIds()) {
                    feelingsList.add(moodID);
                }
                feedDTO.setFeelings(feelingsList);
            }

            List<StatusTagDTO> statusTagList = newsFeed.getStatusTags();
            if (statusTagList != null) {
                Map<Integer, Long> textTagMap = new HashMap<>();
                for (StatusTagDTO statusTagDTO : statusTagList) {
                    textTagMap.put(statusTagDTO.getPosition(), statusTagDTO.getUserId());
                }
                feedDTO.setTextTag(textTagMap);
            }

            feedDTO.setWithTag(newsFeed.getTagFriendIds());

            if (newsFeed.getSpecificFriends() != null && !newsFeed.getSpecificFriends().isEmpty()) {
                feedDTO.setSpecificFriend(newsFeed.getSpecificFriends());
            }

            if (feedDTO.getWallOwnerType() == AppConstants.UserTypeConstants.CELEBRITY) {
                feedDTO.setDonationPageId(donationPageId);
            }
            //FnFLogger.getInstance(1).debug("b4 --> " + new Gson().toJson(feedDTO) + " sessionDTO.getUserID() --> " + sessionUserID);
            /*boolean isWallOwnerCelebrity = false;
        UserBasicInfoDTO userBasicInfoDTO = CassandraDAO.getInstance().getUserDAO().getUserBasicInfo(feedDTO.getWallOwnerId());
        if (userBasicInfoDTO != null) {
            isWallOwnerCelebrity = userBasicInfoDTO.getUserType() == AppConstants.UserTypeConstants.CELEBRITY;
        }
             */
            switch (feedDTO.getWallOwnerType()) {
                case AppConstants.UserTypeConstants.NEWSPORTAL:
                    feedDTO.setPrivacy(AppConstants.CASS_PRIVACY_PUBLIC);
                    break;
            }

            RingLogger.getConfigPortalLogger().debug("FeedDTO to add status-->" + new Gson().toJson(feedDTO));
            int reasonCode = CassandraDAO.getInstance().addStatus(feedDTO);
            RingLogger.getConfigPortalLogger().debug("ReasonCode for add status--> " + reasonCode);

//            NewsFeedDTO newsFeedDTO = NewsfeedConverter.NewsfeedDTOFromCass(feedDTO, sessionUserID, AppConstants.ACTION_ADD_STATUS);
//            feedBack.setNewsFeed(newsFeedDTO);
            feedBack.setReasonCode(reasonCode);
            if (reasonCode > 0) {
                feedBack.setSuccess(false);
                return feedBack;
            }
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception [addStatus] --> ", e);
        }
        return feedBack;
    }

    public static void main(String[] args) {
        NewsFeed newsFeed = new NewsFeed();
        long sessionUserID = 58143;//from UI
        long donationPageId = 61854L;
        int sessionUserType = AppConstants.UserTypeConstants.CELEBRITY;

        Helper helper = new Helper();

//        newsFeed.setFeedContentType(AppConstants.NEWS_FEED_TYPE_STATUS);
        newsFeed.setCelebrityLiveTime(System.currentTimeMillis() + 1 * 60 * 60 * 1000);
        newsFeed.setStatus("This is sample text status");

        helper.addStatus(sessionUserID, sessionUserType, newsFeed, donationPageId);
    }
}
