package com.ringid.admin.utils.helper;

import com.ringid.admin.cassandra.CassandraDAO;
import org.ringid.newsfeeds.CassAlbumDTO;
import org.ringid.newsfeeds.MediaAlbumDTO;
import org.ringid.users.UserBasicInfoDTO;
import org.ringid.users.UserShortInfoForFeed;
import org.ringid.utilities.AppConstants;

public class NewsfeedConverter {

    public static CassAlbumDTO mediaAlbumDTOToCass(MediaAlbumDTO mediaAlbumDTO) {
        CassAlbumDTO cassAlbumDTO = new CassAlbumDTO();
        if (mediaAlbumDTO != null) {
            cassAlbumDTO.setUserId(mediaAlbumDTO.getUserId());
            cassAlbumDTO.setAddTime(mediaAlbumDTO.getAddedTime());
            cassAlbumDTO.setAlbumId(mediaAlbumDTO.getAlbumId());
            cassAlbumDTO.setAlbumName(mediaAlbumDTO.getAlbumName());
            cassAlbumDTO.setImageType(mediaAlbumDTO.getImageType());
            cassAlbumDTO.setMediaType(mediaAlbumDTO.getMediaType());
            cassAlbumDTO.setPrivacy(mediaAlbumDTO.getPrivacy());
            cassAlbumDTO.setHashTag(mediaAlbumDTO.getHashTagList());
            cassAlbumDTO.setContentCount(mediaAlbumDTO.getMediaCount());
            cassAlbumDTO.setAlbumCoverUrl(mediaAlbumDTO.getCoverImageURL());
            cassAlbumDTO.setCoverHeight(AppConstants.THUMB_HEIGTH);
            cassAlbumDTO.setCoverWidth(AppConstants.THUMB_WIDTH);
            cassAlbumDTO.setLastSerial(mediaAlbumDTO.getLastSerial());
        }
        return cassAlbumDTO;
    }

    public static MediaAlbumDTO cassAlbumDTOToMediaAlbumDTO(CassAlbumDTO cassAlbumDTO) {
        MediaAlbumDTO mediaAlbumDTO = new MediaAlbumDTO();
        if (cassAlbumDTO != null) {
            mediaAlbumDTO.setAlbumId(cassAlbumDTO.getAlbumId());
            mediaAlbumDTO.setAlbumName(cassAlbumDTO.getAlbumName());
            mediaAlbumDTO.setMediaType(cassAlbumDTO.getMediaType());
            mediaAlbumDTO.setImageType(cassAlbumDTO.getImageType());
            mediaAlbumDTO.setUserId(cassAlbumDTO.getUserId());
            mediaAlbumDTO.setCoverImageURL(cassAlbumDTO.getAlbumCoverUrl());
            mediaAlbumDTO.setCoverHeight(cassAlbumDTO.getCoverHeight());
            mediaAlbumDTO.setCoverWidth(cassAlbumDTO.getCoverWidth());
            mediaAlbumDTO.setHashTagList(cassAlbumDTO.getHashTag());
            mediaAlbumDTO.setMediaCount(cassAlbumDTO.getContentCount());
            if (cassAlbumDTO.getPrivacy() > 0) {
                mediaAlbumDTO.setPrivacy(cassAlbumDTO.getPrivacy());
            }
            mediaAlbumDTO.setLastSerial(cassAlbumDTO.getLastSerial());
            mediaAlbumDTO.setAlbumType(cassAlbumDTO.getAlbumType());
        }
        return mediaAlbumDTO;
    }

    public static UserShortInfoForFeed getShortInfoForFeed(long userTableId) {
        UserShortInfoForFeed userShortInfoForFeed = new UserShortInfoForFeed();
        try {
            UserBasicInfoDTO userBasicInfoDTO = CassandraDAO.getInstance().getUserBasicInfo(userTableId);
            userShortInfoForFeed.setUserTableID(userTableId);
            if (userBasicInfoDTO != null) {
                userShortInfoForFeed.setFullName(userBasicInfoDTO.getFirstName());
                userShortInfoForFeed.setProfileType(userBasicInfoDTO.getUserType());
                userShortInfoForFeed.setProfileImage(userBasicInfoDTO.getProfileImageURL());
                userShortInfoForFeed.setRingID(userBasicInfoDTO.getUserIdentity());
            }
        } catch (Exception e) {
        }
        return userShortInfoForFeed;
    }
    /*
    public static NewsFeedDTO NewsfeedDTOFromCass(FeedDTO feedDTO, long sessionUserId, int action) {
        NewsFeedDTO newsFeedDTO = null;
        if (feedDTO != null) {
            newsFeedDTO = new NewsFeedDTO();
            newsFeedDTO.setAddTime(feedDTO.getAddTime());
            newsFeedDTO.setTime(feedDTO.getUpdateTime());
            newsFeedDTO.setType(feedDTO.getContentType());
            newsFeedDTO.setNewsFeedID(feedDTO.getFeedId());
            newsFeedDTO.setEdited(feedDTO.isIsEdited());
            newsFeedDTO.setCelebrityLiveTime(feedDTO.getLiveTime());

            if (feedDTO.getWallOwnerType() == AppConstants.UserTypeConstants.GROUP) {
//                newsFeedDTO.setWallOwner(NewsfeedConverter.getGroupShortInfoForFeed(sessionUserId, feedDTO.getWallOwnerId()));
            } else {
                newsFeedDTO.setWallOwner(NewsfeedConverter.getShortInfoForFeed(feedDTO.getWallOwnerId()));
            }
            if (feedDTO.getGuestWriterId() > 0) {
                newsFeedDTO.setGuestWriter(NewsfeedConverter.getShortInfoForFeed(feedDTO.getGuestWriterId()));
            }

            AdditionalFeedContent additionalFeedContent = feedDTO.getAdditionalFeedContent();
            if (additionalFeedContent != null) {
                NewsPortalFeedInfoDTO newsPortalFeedInfoDTO = new NewsPortalFeedInfoDTO();
                newsPortalFeedInfoDTO.setId(feedDTO.getFeedId());
                newsPortalFeedInfoDTO.setAddedTime(feedDTO.getAddTime());
                newsPortalFeedInfoDTO.setNewsDescription(additionalFeedContent.getDescription());
                if (additionalFeedContent.getExternalURLOption() == null) {
                    if (AppConstants.DEBUG_QUERY) {
                        logger.debug("## FeedId --> " + feedDTO.getFeedId() + " --> additionalFeedContent --> " + new Gson().toJson(additionalFeedContent));
                    }
                } else {
                    newsPortalFeedInfoDTO.setExternalUrlOption(additionalFeedContent.getExternalURLOption().equals("true"));
                }
                newsPortalFeedInfoDTO.setNewsUrl(additionalFeedContent.getNewsURL());
                newsPortalFeedInfoDTO.setNewsShortDescription(additionalFeedContent.getShortDescription());
                newsPortalFeedInfoDTO.setNewsTitle(additionalFeedContent.getTitle());
                newsPortalFeedInfoDTO.setNewsCategoryId(additionalFeedContent.getPageFeedCategoryId());
                newsFeedDTO.setNewsPortalFeedInfoDTO(newsPortalFeedInfoDTO);
            }

            CassAlbumDTO albumDTO = feedDTO.getAlbumDTO();
            if (albumDTO != null) {
                long feedOwner = feedDTO.getGuestWriterId() > 0 ? feedDTO.getGuestWriterId() : feedDTO.getWallOwnerId();
                albumDTO.setUserId(feedOwner);
                albumDTO.setPrivacy(feedDTO.getPrivacy());

                ArrayList<CassImageDTO> imageList = albumDTO.getImgDTOs();
                if (imageList != null && !imageList.isEmpty()) {
                    List<ImageDTO> imageDTOList = new ArrayList<>();
                    for (CassImageDTO cassImageDTO : imageList) {
                        ImageDTO imageDTO = new ImageDTO();
                        imageDTO.setImageID(cassImageDTO.getImageId());
                        imageDTO.setImageURL(cassImageDTO.getImageUrl());
                        imageDTO.setCaption(cassImageDTO.getCaption());
                        imageDTO.setImageHeight(cassImageDTO.getImageHeight());
                        imageDTO.setImageWidth(cassImageDTO.getImageWidth());
                        imageDTO.setTime(cassImageDTO.getAddTime());
                        imageDTO.setSerial(cassImageDTO.getSerial());

                        if (action == AppConstants.ACTION_NEWSFEED_BY_ID) {
                            CassImageDTO detailImageDTO = CassandraDAO.getInstance().getImageDetails(cassImageDTO.getImageId(), sessionUserId);
                            if (detailImageDTO.getReasonCode() != ReasonCode.NOT_FOUND) {
                                imageDTO.setLikes(detailImageDTO.getLikeCount());
                                imageDTO.setComments(detailImageDTO.getCommentCount());
                                imageDTO.setiLike(detailImageDTO.getiLike() ? 1 : 0);
                                imageDTO.setiComment(detailImageDTO.getiComment() ? 1 : 0);
                            }
                        }
                        imageDTOList.add(imageDTO);
                    }

                    MediaAlbumDTO mediaAlbumDTO = cassAlbumDTOToMediaAlbumDTO(albumDTO);
                    mediaAlbumDTO.setImageList(imageDTOList);
                    newsFeedDTO.setAlbumDTO(mediaAlbumDTO);
                }

                ArrayList<CassMultiMediaDTO> multiMediaList = albumDTO.getMultiMediaDTOs();
                if (multiMediaList != null && !multiMediaList.isEmpty()) {
                    List<MediaContentDTO> contentDTOList = new ArrayList<>();

                    for (CassMultiMediaDTO cassMultiMediaDTO : multiMediaList) {
                        MediaContentDTO contentDTO = new MediaContentDTO();
                        contentDTO.setMediaStreamURL(cassMultiMediaDTO.getMultiMediaUrl());
                        contentDTO.setHashTagList(cassMultiMediaDTO.getHashTags());
                        contentDTO.setContentID(cassMultiMediaDTO.getMultiMediaId());
                        contentDTO.setMediaTitle(cassMultiMediaDTO.getTitle());
                        contentDTO.setMediaArtist(cassMultiMediaDTO.getArtist());
                        contentDTO.setMediaDuration(cassMultiMediaDTO.getDuration());
                        contentDTO.setMediaThumbnailURL(cassMultiMediaDTO.getThumbnailUrl());
                        contentDTO.setThumbImageHeight(cassMultiMediaDTO.getThumbnailHeight());
                        contentDTO.setThumbImageWidth(cassMultiMediaDTO.getThumbnailWidth());
                        contentDTO.setSerial(cassMultiMediaDTO.getSerial());

                        if (action == AppConstants.ACTION_NEWSFEED_BY_ID) {
                            CassMultiMediaDTO detailMediaDTO = CassandraDAO.getInstance().getFeedDAO().getMediaContentDetails(cassMultiMediaDTO.getMultiMediaId(), sessionUserId);
                            if (detailMediaDTO.getReasonCode() != ReasonCode.NOT_FOUND) {
                                contentDTO.setLikeCount(detailMediaDTO.getLikeCount());
                                contentDTO.setCommentCount(detailMediaDTO.getCommentCount());
                                contentDTO.setAccessCount(detailMediaDTO.getViewCount());
                                contentDTO.setiLike(detailMediaDTO.getiLike() ? 1 : 0);
                                contentDTO.setiComment(detailMediaDTO.getiComment() ? 1 : 0);
                            }
                            UserBasicInfoDTO userShortInfoDTO = CassandraDAO.getInstance().getUserDAO().getUserBasicInfo(contentDTO.getUserId());
                            if (userShortInfoDTO != null) {
                                contentDTO.setFullName(userShortInfoDTO.getFirstName());
                                contentDTO.setProfileImage(userShortInfoDTO.getProfileImageURL());
                            }
                        }
                        contentDTOList.add(contentDTO);
                    }

                    MediaAlbumDTO mediaAlbumDTO = cassAlbumDTOToMediaAlbumDTO(albumDTO);
                    mediaAlbumDTO.setMediaContentList(contentDTOList);
                    newsFeedDTO.setAlbumDTO(mediaAlbumDTO);
                }
                newsFeedDTO.setTotalMediaCount(albumDTO.getContentCount());
            }
            newsFeedDTO.setCommentPrivacy(feedDTO.getCommentPrivacy());

            if (feedDTO.getLatestCommenter() != null) {
                newsFeedDTO.setLatestCommenter(userBasicInfoDTOFromCass(feedDTO.getLatestCommenter()));
            }
            if (feedDTO.getLatestLiker() != null) {
                newsFeedDTO.setLatestLiker(userBasicInfoDTOFromCass(feedDTO.getLatestLiker()));
            }

            LinkContent linkContent = feedDTO.getLinkContent();
            if (linkContent != null) {
                if (!(linkContent.getLinkURL() == null || linkContent.getLinkURL().isEmpty())) {
                    LinkDTO linkDTO = new LinkDTO();
                    linkDTO.setLinkType(linkContent.getLinkType());
                    linkDTO.setLinkTitle(linkContent.getLinkTitle());
                    linkDTO.setLinkURL(linkContent.getLinkURL());
                    linkDTO.setLinkDesc(linkContent.getLinkDesc());
                    linkDTO.setLinkImageURL(linkContent.getLinkImageURL());
                    linkDTO.setLinkDomain(linkContent.getLinkDomain());
                    newsFeedDTO.setLinkDTO(linkDTO);
                }
            }

            if (feedDTO.getLatLonExists() != null && feedDTO.getLatLonExists()) {
                LocationDTO locationDTO = new LocationDTO();
                locationDTO.setLocation(feedDTO.getLocation());
                locationDTO.setLatitude(feedDTO.getLocationLat());
                locationDTO.setLongitude(feedDTO.getLocationLon());
                newsFeedDTO.setLocationDTO(locationDTO);
            }

            newsFeedDTO.setSharedFeedId(feedDTO.getParentFeedId());
//            if (feedDTO.getParentFeedId() != null) {
//                newsFeedDTO.setOriginalFeed(NewsfeedDTOFromCass(feedDTO.getParentFeedInfo(), sessionUserId, AppConstants.ACTION_NEWSFEED_BY_ID));
//                if (newsFeedDTO.getOriginalFeed() != null && (action == AppConstants.ACTION_NEWS_FEED || action == AppConstants.ACTION_ADD_STATUS)) {
//                    List<FeedBasicInfo> sharedFeeds = CassandraDAO.getInstance().getFeedDAO().getSharedFeeds(feedDTO.getParentFeedId(), null, AppConstants.SCROLL_DOWN, 3, sessionUserId);
//                    if (sharedFeeds != null && !sharedFeeds.isEmpty()) {
//                        for (FeedBasicInfo feedBasicInfo : sharedFeeds) {
//                            reformFeedBasicInfo(feedBasicInfo, sessionUserId);
//                        }
//                        newsFeedDTO.getOriginalFeed().setSharedFeedList(sharedFeeds);
//                    }
//                }
//            }
            newsFeedDTO.setPrivacy(feedDTO.getPrivacy());
            newsFeedDTO.setSavedFeedTime(feedDTO.getSaveTime());
//            newsFeedDTO.setSaved(CassandraDAO.getInstance().getFeedDAO().isFeedSaved(feedDTO.getFeedId(), sessionUserId));
            newsFeedDTO.setSpecificFriend(feedDTO.getSpecificFriend());
            newsFeedDTO.setStatus(feedDTO.getStatus());
            newsFeedDTO.setShowContinue(feedDTO.getShowContinue());
            newsFeedDTO.setNumberOfComments(feedDTO.getTotalComment());
            newsFeedDTO.setNumberOfLikes(feedDTO.getTotalLike());
            newsFeedDTO.setNumberOfShares(feedDTO.getTotalShare());
            newsFeedDTO.setValidity(feedDTO.getValidity());
            newsFeedDTO.setiLike(feedDTO.isiLike() ? 1 : 0);
            newsFeedDTO.setiComment(feedDTO.isiComment() ? 1 : 0);
            newsFeedDTO.setiShare(feedDTO.isiShare() ? 1 : 0);
            newsFeedDTO.setAccessCount(feedDTO.getTotalView());

            if (feedDTO.getLatestLiker() != null) {
                newsFeedDTO.setLatestLiker(userBasicInfoDTOFromCass(feedDTO.getLatestLiker()));
            }
            if (feedDTO.getLatestCommenter() != null) {
                newsFeedDTO.setLatestCommenter(userBasicInfoDTOFromCass(feedDTO.getLatestCommenter()));
            }
            if (feedDTO.getLatestSharer() != null) {
                newsFeedDTO.setLatestSharer(userBasicInfoDTOFromCass(feedDTO.getLatestSharer()));
            }

            List<Integer> feelingsList = feedDTO.getFeelings();
            if (feelingsList != null) {
                ArrayList<FeedMoodDTO> moodList = new ArrayList<>();
                for (int moodID : feelingsList) {
                    FeedMoodDTO moodDTO = CassandraDAO.getInstance().getFeedDAO().getFeedMoodDTO(moodID);
                    moodList.add(moodDTO);
                }
                newsFeedDTO.setMoodList(moodList);
            }

            Map<Integer, Long> textTagMap = feedDTO.getTextTag();
            if (textTagMap != null) {
                List<StatusTagDTO> statusTagList = new ArrayList<>();
                for (Map.Entry<Integer, Long> entry : textTagMap.entrySet()) {
                    StatusTagDTO statusTagDTO = new StatusTagDTO();
                    statusTagDTO.setPosition(entry.getKey());
                    statusTagDTO.setUserId(entry.getValue());
                    UserBasicInfoDTO userBasicInfoDTO = CassandraDAO.getInstance().getUserDAO().getUserBasicInfo(statusTagDTO.getUserId());
                    if (userBasicInfoDTO != null) {
                        statusTagDTO.setName(userBasicInfoDTO.getFirstName());
                        statusTagDTO.setProfileImage(userBasicInfoDTO.getProfileImageURL());
                        statusTagDTO.setProfileImageId(userBasicInfoDTO.getProfileImageId());
                        statusTagDTO.setProfileType(userBasicInfoDTO.getUserType());
                        statusTagDTO.setRingID(userBasicInfoDTO.getUserIdentity());
                    }
                    statusTagList.add(statusTagDTO);
                }
                newsFeedDTO.setStatusTagList(statusTagList);
            }

            List<Long> withIDList = feedDTO.getWithTag();
            if (withIDList != null) {
                List<FriendTagDTO> friendTagList = new ArrayList<>();
                for (long friendID : withIDList) {
                    FriendTagDTO friendTagDTO = new FriendTagDTO();
                    friendTagDTO.setUserID(friendID);
                    UserBasicInfoDTO userBasicInfoDTO = CassandraDAO.getInstance().getUserDAO().getUserBasicInfo(friendID);
                    if (userBasicInfoDTO != null) {
                        friendTagDTO.setName(userBasicInfoDTO.getFirstName());
                        friendTagDTO.setProfileImage(userBasicInfoDTO.getProfileImageURL());
                        friendTagDTO.setUserIdentity(userBasicInfoDTO.getUserIdentity());
                        friendTagDTO.setProfileType(userBasicInfoDTO.getUserType());
                    }
                    friendTagList.add(friendTagDTO);
                }
                newsFeedDTO.setFriendTagList(friendTagList);
                newsFeedDTO.setTaggedFriendCount(friendTagList.size());
            }
        }
        return newsFeedDTO;
    }
     */
}
