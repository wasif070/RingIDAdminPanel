/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.utils;

import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.dto.adminAuth.LoginDTO;
import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Rabby
 */
public class SecurityFilter implements Filter {

    private static String actualFeatureName = null;
    private static String operation = null;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpSession session;
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse res = (HttpServletResponse) response;
        request.setCharacterEncoding("UTF-8");
        response.setContentType("text/html; charset=UTF-8");
        try {
            getFeatureNameAndOperation(req);
            session = req.getSession(false);
            if (session != null) {
                LoginDTO loginDTO = null;
                if (session.getAttribute(Constants.LOGIN_DTO) != null) {
                    loginDTO = (LoginDTO) session.getAttribute(Constants.LOGIN_DTO);
                }
                if (loginDTO != null) {
                    boolean hasPermission = checkSecurity(loginDTO);
                    if (!hasPermission) {
                        req.getRequestDispatcher("/home/permissionDenied.jsp").forward(req, res);
                    }
                } else {
                    if (getUrl(req.getRequestURI().substring(req.getContextPath().length())).startsWith("/Download")) {
                        req.getRequestDispatcher("/home/permissionDenied.jsp").forward(req, res);
                    }
                }
            }
            chain.doFilter(request, response);
        } catch (UnsupportedOperationException | IOException | ServletException e) {
            RingLogger.getPermissionLogger().error(e);
            e.printStackTrace();
        }
    }

    @Override
    public void destroy() {
    }

    private void getFeatureNameAndOperation(HttpServletRequest req) {
        String resourceUri = req.getRequestURI();
        String contextPath = req.getContextPath();
        String url = getUrl(resourceUri.substring(contextPath.length()));
        String featureName = Constants.featureMenuUrlList.get(url);
        RingLogger.getPermissionLogger().debug("url --> " + url);
        RingLogger.getPermissionLogger().debug("Feature Name --> " + featureName);
        if (featureName != null) {
            if (featureName.contains("#")) {
                actualFeatureName = featureName.substring(0, featureName.lastIndexOf("#"));
                operation = featureName.substring(featureName.lastIndexOf("#") + 1);
            } else {
                actualFeatureName = featureName;
                operation = null;
            }
        } else {
            actualFeatureName = null;
            operation = null;
        }
        RingLogger.getPermissionLogger().debug("Actual Feature Name --> " + actualFeatureName);
        RingLogger.getPermissionLogger().debug("Actual Feature Operation --> " + operation);
    }

    private boolean checkSecurity(LoginDTO loginDTO) {
        boolean hasPermission = true;
        RingLogger.getPermissionLogger().debug("loginDTO.getPermissionLevel(): " + loginDTO.getPermissionLevel());
        RingLogger.getPermissionLogger().debug("actualFeatureName : " + actualFeatureName);
        if (loginDTO.getFeauterMapList().containsKey(actualFeatureName)) {
            RingLogger.getPermissionLogger().debug("loginDTO.getfeaturePermissionLevel: " + loginDTO.getFeauterMapList().get(actualFeatureName).getPermissionLevel());
        }
        switch (loginDTO.getPermissionLevel()) {
            case Constants.SUPER_ADMIN:
                break;
            default:
                if (loginDTO.getFeauterMapList().containsKey(actualFeatureName)) {
                    if (loginDTO.getFeauterMapList().get(actualFeatureName).getPermissionLevel() == 0) {
                        return false;
                    }
                    hasPermission = checkOperation(operation, loginDTO);
                }
                break;
        }
        return hasPermission;
    }

    private boolean checkOperation(String operation, LoginDTO loginDTO) {
        boolean hasPermission = true;
        if (operation != null) {
            RingLogger.getPermissionLogger().debug("UserFeatureMappingDTO -> " + loginDTO.getFeauterMapList().get(actualFeatureName));
            switch (operation) {
                case "add":
                    if (!loginDTO.getFeauterMapList().get(actualFeatureName).getHasAddPermission()) {
                        hasPermission = false;
                    }
                    break;
                case "modify":
                    if (!loginDTO.getFeauterMapList().get(actualFeatureName).getHasModifyPermission()) {
                        hasPermission = false;
                    }
                    break;
                case "delete":
                    if (!loginDTO.getFeauterMapList().get(actualFeatureName).getHasDeletePermission()) {
                        hasPermission = false;
                    }
                    break;
                case "view":
                    if (!loginDTO.getFeauterMapList().get(actualFeatureName).getHasViewPermission()) {
                        hasPermission = false;
                    }
                    break;
                default:
                    break;
            }
        }
        return hasPermission;
    }

    private String getUrl(String url) {
        if (url == null) {
            return null;
        }
        String actualUrl = "";
        for (int i = 0; i < url.length(); i++) {
            if (url.charAt(i) == '/' || url.charAt(i) == '\\') {
                if (i > 0 && (url.charAt(i - 1) == '/' || url.charAt(i - 1) == '\\')) {
                    continue;
                }
                actualUrl += url.charAt(i);
            } else {
                actualUrl += url.charAt(i);
            }
        }
        return actualUrl;
    }
}
