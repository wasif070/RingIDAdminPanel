/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.utils;

public class SmsServerConstant {

    public static final short CLICKATELL_API = 1;
    public static final short SMS_ROUTER = 2;
    public static final short TEXT_LOCAL = 3;
    public static final short SMS_CELLENT = 4;
    public static final short MOBI_SMS = 5;
    public static final short CS_NETWORKS = 6;
    public static final short INFOBIP_SMS = 7;
    public static final short ROBIAXIATA_SMS = 8;
    public static final short CLICKN_GO_BD = 9;
    public static final short WAVECELL = 10;
    public static final short PLIVO_API = 11;
    public static final short SMS_WAALA_API = 12;
    public static final short ACL = 13;
    public static final short SMS_COUNTRY = 14;
    public static final short NEXMO = 15;
    public static final short GP = 16;
}
