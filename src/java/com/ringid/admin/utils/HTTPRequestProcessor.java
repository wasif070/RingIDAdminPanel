/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.utils;

import com.ringid.admin.utils.log.RingLogger;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public class HTTPRequestProcessor {

    public static String uploader_method(
            String requestMethod,
            HashMap<String, Object> values,
            HashMap<String, Object> bodyValues,
            byte[] byteData,
            ArrayList<String> names,
            ArrayList<byte[]> dataValues,
            String serverurl,
            String contentType
    ) {
        HttpURLConnection conn = null;
        BufferedReader br = null;
        DataOutputStream dos = null;
        String lineEnd = "\r\n";
        String twoHyphens = "--";
        String boundary = "*****";
        try {
            URL url = new URL(serverurl);
            conn = (HttpURLConnection) url.openConnection();
            // Allow Inputs  
            conn.setDoInput(true);
            // Allow Outputs  
            conn.setDoOutput(true);
            // Don't use a cached copy.  
            conn.setUseCaches(false);
            // Use a post method.  
            conn.setRequestMethod(requestMethod);

            if (contentType != null) {
                conn.setRequestProperty("Connection", "Keep-Alive");
                conn.setRequestProperty("Content-Type", "multipart/form-data; boundary=" + boundary);
            }

            if (values != null) {
                for (Map.Entry<String, Object> entry : values.entrySet()) {
                    conn.setRequestProperty(entry.getKey(), entry.getValue().toString());
                }
            }

            dos = new DataOutputStream(conn.getOutputStream());

            if (bodyValues != null && bodyValues.size() > 0) {
                for (Map.Entry<String, Object> entry : bodyValues.entrySet()) {
                    dos.write((twoHyphens + boundary + lineEnd).getBytes(StandardCharsets.UTF_8));
                    dos.write(("Content-Disposition: form-data; name=\"" + entry.getKey() + "\"" + lineEnd + lineEnd + entry.getValue() + lineEnd).getBytes("UTF-8"));
                }
                for (int i = 0; i < names.size(); i++) {
                    dos.write((twoHyphens + boundary + lineEnd).getBytes(StandardCharsets.UTF_8));
//                    dos.write(("Content-Disposition: form-data; name=\"uploadedFile\";filename=\"" + names.get(i) + "\"" + lineEnd).getBytes("UTF-8"));
                    dos.write(("Content-Disposition: form-data; name=\"uploadedFile\";filename=\"" + names.get(i) + "\"" + lineEnd).getBytes("UTF-8"));

                    dos.write((lineEnd).getBytes(StandardCharsets.UTF_8));
                    dos.write(dataValues.get(i), 0, dataValues.get(i).length);
                    dos.writeBytes(lineEnd);
                    dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);
                }
            } else {
                if (byteData != null) {
                    dos.write(byteData);
                }
            }

            dos.flush();
            dos.close();
            RingLogger.getConfigPortalLogger().debug("## Write Done.");
        } catch (MalformedURLException ex) {
            RingLogger.getConfigPortalLogger().error("MalformedURLException --> " + ex);
        } catch (IOException ioe) {
            RingLogger.getConfigPortalLogger().error("IOException --> " + ioe);
        }
        //------------------ read the SERVER RESPONSE  
        String mainstr = "";
        JSONObject jSONObject = new JSONObject();
        try {
            if (conn != null) {
                jSONObject.put("code", conn.getResponseCode());
                jSONObject.put("mg", conn.getResponseMessage());
                System.out.println("code : " + conn.getResponseCode() + " responseMsg : " + conn.getResponseMessage());
                br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            }
            String str;

            while (br != null && (str = br.readLine()) != null) {
                mainstr += str;
            }
            jSONObject.put("mainstr", mainstr);
            RingLogger.getConfigPortalLogger().debug("## Response Received.");
        } catch (IOException ioex) {
            RingLogger.getConfigPortalLogger().error("error --> " + ioex);
        } catch (Exception ex) {
            RingLogger.getConfigPortalLogger().error("Exception --> " + ex);
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        RingLogger.getConfigPortalLogger().debug("## mainstr --> " + jSONObject.toString());
        return jSONObject.toString();
    }

    public static String upload(
            String requestMethod,
            HashMap<String, Object> headerValues,
            byte[] imageData,
            String serverurl
    ) {
        HttpURLConnection conn = null;
        BufferedReader br = null;
        DataOutputStream dos = null;
        try {
//            if (imageData == null) {
//                imageData = new byte[1];
//            }
            serverurl = serverurl.replaceAll(" ", "%20");
            URL url = new URL(serverurl);
            conn = (HttpURLConnection) url.openConnection();

            conn.setDoInput(true);  // Allow Inputs              
            if (!requestMethod.equals("GET")) {
                conn.setDoOutput(true); // Allow Outputs              
            }
            conn.setUseCaches(false);   // Don't use a cached copy.              
            conn.setRequestMethod(requestMethod);   // Use a post method.  
            conn.setRequestProperty("Accept-Charset", "UTF-8");
            if (imageData != null) {
                conn.setRequestProperty("Content-Length", String.valueOf(imageData.length));
            }

            if (headerValues != null) {
                for (Map.Entry<String, Object> entry : headerValues.entrySet()) {
                    conn.addRequestProperty(entry.getKey(), entry.getValue().toString());
                }
            }

            if (imageData != null) {
                dos = new DataOutputStream(conn.getOutputStream());
                dos.write(imageData);
                dos.flush();
                dos.close();
            }
        } catch (MalformedURLException ex) {
            System.err.println("" + ex);
            RingLogger.getConfigPortalLogger().error("MalformedURLException [serverurl] -> " + serverurl + " --> " + ex);
        } catch (IOException ioe) {
            System.err.println("" + ioe);
            RingLogger.getConfigPortalLogger().error("IOException [serverurl]->" + serverurl + " --> " + ioe);
        } catch (Exception e) {
            System.err.println("" + e);
            RingLogger.getConfigPortalLogger().error("Exception occured while uploading [serverurl]->" + serverurl + " --> " + e);
        }
        //------------------ read the SERVER RESPONSE  ------------------
        String mainstr = "";
        try {
            br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String str;

            while ((str = br.readLine()) != null) {
                mainstr += str;
            }
//            System.out.println("" + mainstr);
//            httpCustomLogger.debug("mainstr --> " + mainstr);
        } catch (IOException ioex) {
            mainstr = ioex.toString();
            RingLogger.getConfigPortalLogger().error("IOException while reading response --> " + ioex);
        } catch (Exception ex) {
            mainstr = ex.toString();
            RingLogger.getConfigPortalLogger().error("Exception occured while reading response --> " + ex);
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (conn != null) {
                try {
                    conn.disconnect();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return mainstr;
    }

//    public static void main(String[] args) {
//        String userId = "2110072255";
//
//        RingTaskScheduler.getInstance().login(userId, "abc123");
//        try {
//            File file = new File("D:/chocolate.png");
//            byte[] data1 = Files.readAllBytes(file.toPath());
//
//            String serverUrl = "http://devimages.ringid.com/ringmarket/ImageUploadHandler";
////            String serverUrl = "http://192.168.8.106:8080/ringmarket/ImageUploadHandler";
//
//            HashMap<String, Object> headerValues = new HashMap<>();
//            headerValues.put("access-control-allow-origin", "*");
//
//            HashMap<String, Object> bodyValues = new HashMap<>();
//            bodyValues.put("cimX", 100);
//            bodyValues.put("cimY", 100);
//            bodyValues.put("ih", 168);
//            bodyValues.put("iw", 300);
//
//            InfoDTo infoDTo = RingTaskScheduler.getInstance().getInfo(userId);
//
//            bodyValues.put("sId", infoDTo.getSessionId());
//            bodyValues.put("uId", infoDTo.getUserId());
//            bodyValues.put("authServer", infoDTo.getAuthIP());
//            bodyValues.put("comPort", infoDTo.getAuthPORT());
//            bodyValues.put("x-app-version", 149);
//
//            ArrayList<String> names = new ArrayList<>();
//            names.add("1.jpg");
//
//            ArrayList<byte[]> dataValues = new ArrayList<>();
//            dataValues.add(data1);
//
//            HTTPRequestProcessor.uploader_method("POST", headerValues, bodyValues, null, names, dataValues, serverUrl, "");
//
//        } catch (Exception e) {
//            System.err.println("" + e);
//        }
//        RingTaskScheduler.getInstance().logout("2110072255");
//    }
}
