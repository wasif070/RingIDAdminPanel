/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin;


import authdbconnector.DBConnection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

public abstract class BaseDAO {

    protected DBConnection db = null;
    protected PreparedStatement ps = null;
    protected Statement stmt = null;
    protected ResultSet rs = null;

    public void close() {
        try {
            if (rs != null) {
                rs.close();
            }
        } catch (Exception e) {
        }

        try {
            if (stmt != null) {
                stmt.close();
            }
        } catch (Exception e) {
        }

        if (ps != null) {
            try {
                ps.close();
            } catch (Exception e) {
            }
        }

        try {
            if (db != null && db.connection != null) {
                authdbconnector.DBConnector.getInstance().freeConnection(db);
            }
        } catch (Exception e) {
        }
    }
}
