/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.authServerCommunicator;

import com.google.gson.JsonObject;
import com.google.gson.stream.JsonReader;
import java.io.IOException;

/**
 *
 * @author ipvision
 */
public class Parser {

    public static int getInt(byte[] data_bytes, int index, int length) {
        switch (length) {
            case 1: {
                return (data_bytes[index++] & 0xFF);
            }
            case 2: {
                return (data_bytes[index++] & 0xFF) << 8 | (data_bytes[index++] & 0xFF);
            }
            case 4: {
                return (data_bytes[index++] & 0xFF) << 24 | (data_bytes[index++] & 0xFF) << 16 | (data_bytes[index++] & 0xFF) << 8 | (data_bytes[index++] & 0xFF);
            }
        }
        return 0;
    }

    public static String getString(byte[] data_bytes, int index, int length) {
        byte[] str_bytes = new byte[length];
        System.arraycopy(data_bytes, index, str_bytes, 0, length);

        return new String(str_bytes);
    }

    public static String getBigString(byte[] data_bytes, int index, int length) {
        byte[] str_bytes = new byte[length];
        System.arraycopy(data_bytes, index, str_bytes, 0, length);

        return new String(str_bytes);
    }

    public static byte[] getBytes(byte[] received_bytes, int index, int length) {
        byte[] data_bytes = new byte[length];
        System.arraycopy(received_bytes, index, data_bytes, 0, length);
        return data_bytes;
    }

    public static boolean getBool(byte[] data_bytes, int index) {
        int result = (data_bytes[index++] & 0xFF);
        return result == 1;
    }

    public static long getLong(byte[] data_bytes, int index, int length) {
//        switch (length) {
//            case 4: {
//                return (data_bytes[index++] & 0xFF) << 24 | (data_bytes[index++] & 0xFF) << 16 | (data_bytes[index++] & 0xFF) << 8 | (data_bytes[index++] & 0xFF);
//            }
//            case 6: {
//                return (data_bytes[index++] & 0xFFL) << 40 | (data_bytes[index++] & 0xFFL) << 32 | (data_bytes[index++] & 0xFF) << 24 | (data_bytes[index++] & 0xFF) << 16 | (data_bytes[index++] & 0xFF) << 8 | (data_bytes[index++] & 0xFF);
//            }
//            case 8: {
//                return (data_bytes[index++] & 0xFFL) << 56 | (data_bytes[index++] & 0xFFL) << 48 | (data_bytes[index++] & 0xFFL) << 40 | (data_bytes[index++] & 0xFFL) << 32 | (data_bytes[index++] & 0xFF) << 24 | (data_bytes[index++] & 0xFF) << 16 | (data_bytes[index++] & 0xFF) << 8 | (data_bytes[index++] & 0xFF);
//            }
//        }
//        return 0;
        long result = 0;
        for (int i = (length - 1); i > -1; i--) {
            result |= (data_bytes[index++] & 0xFFL) << (8 * i);
        }
        return result;
    }

    public static HeaderAttributes parseHeader(byte[] bytes, int offset) {
        HeaderAttributes values = new HeaderAttributes();

        for (int index = offset; index < bytes.length; index++) {
            int attribute = (bytes[index++] & 0xFF) << 8 | (bytes[index++] & 0xFF);

            int length = 0;

            switch (attribute) {
                case AttributeCodes.ACTION: {
                    length = (bytes[index++] & 0xFF);
                    values.setAction(getInt(bytes, index, length));
                    break;
                }
                case AttributeCodes.SERVER_PACKET_ID: {
                    length = (bytes[index++] & 0xFF);
                    values.setServerPacketID(getLong(bytes, index, length));
                    break;
                }
                case AttributeCodes.CLIENT_PACKET_ID: {
                    length = (bytes[index++] & 0xFF);
                    values.setClientPacketID(getString(bytes, index, length));
                    break;
                }
                case AttributeCodes.WEB_UNIQUE_KEY: {
                    length = getInt(bytes, index++, 1);
                    break;
                }
                case AttributeCodes.WEB_TAB_ID: {
                    length = getInt(bytes, index++, 1);
                    break;
                }
                default:
                    values.setHeaderLength(index - 2);
                    return values;
            }
            index += length - 1;
        }
        return values;
    }

    static JsonObject parseAsJsonObject(JsonReader reader) {
        JsonObject jsonObject = new JsonObject();
        try {
            reader.setLenient(true);
            reader.beginObject();
            while (reader.hasNext()) {
                String name = reader.nextName();
                switch (name) {
                    case "actn":
                        jsonObject.addProperty("actn", reader.nextInt());
                        break;
                    case "mg":
                        jsonObject.addProperty("mg", reader.nextString());
                        break;
                    case "success":
                        jsonObject.addProperty("success", reader.nextBoolean());
                        break;
                    case "pckId":
                        jsonObject.addProperty("pckId", reader.nextString());
                        break;
                    case "dvc":
                        jsonObject.addProperty("dvc", reader.nextInt());
                        break;
                    case "rc":
                        jsonObject.addProperty("rc", reader.nextInt());
                        break;
                    default:
                        reader.skipValue();
                }
            }
            reader.endObject();
            reader.close();
        } catch (IOException e) {

        }
        return jsonObject;
    }
}
