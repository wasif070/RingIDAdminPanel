/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.authServerCommunicator;

import com.google.gson.JsonObject;
import com.google.gson.JsonSyntaxException;
import com.google.gson.stream.JsonReader;
import com.ringid.admin.actions.servers.authServers.AuthServerDTO;
import com.ringid.admin.actions.servers.authServers.AuthServerTaskScheduler;
import com.ringid.admin.dto.CheckLogDTO;
import com.ringid.admin.utils.log.RingLogger;
import java.io.IOException;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicLong;

/**
 *
 * @author Kazi Nasir Uddin Oly
 * @Date Jun 18, 2016
 */
public class AuthServerCommunicator {

    private DatagramSocket datagramSocket = null;
    private static HashMap<String, CheckLogDTO> checkLogHashMap;
    private static AtomicLong uniqueId;
    private static int sendCount = 0;
    private static final Set<String> PACKET_SET = new HashSet<>();

    public AuthServerCommunicator() throws SocketException {
        datagramSocket = new DatagramSocket();
        datagramSocket.setSoTimeout(300);
        checkLogHashMap = new HashMap<>();
        uniqueId = new AtomicLong();
    }

    private byte[] createUdpMessage(int action, String serverIP) throws UnsupportedEncodingException {
        Long requestTime = System.currentTimeMillis();
        String pckId = getPckId() + String.valueOf(requestTime);

        CheckLogDTO checkLogDTO = new CheckLogDTO();
        checkLogDTO.setRequestTime(requestTime);
        checkLogDTO.setRequestDate(requestTime);
        checkLogDTO.setResponseDate(0L);
        checkLogDTO.setServerIP(serverIP);
        checkLogDTO.setServerType(action);
        checkLogDTO.setStatus(CheckLogDTO.PENDING);
        checkLogDTO.setResponseMSG("");
        checkLogDTO.setPckId(pckId);
        checkLogHashMap.put(pckId, checkLogDTO);

        JsonObject jsonObject = new JsonObject();

        jsonObject.addProperty("actn", (byte) action);
        jsonObject.addProperty("key", "5R8x4y21e74sMrN0");
        jsonObject.addProperty("pckId", pckId);

        byte[] jsonBytes = jsonObject.toString().getBytes();
        byte[] ret = new byte[jsonBytes.length + 2];
        ret[0] = (byte) 9;
        System.arraycopy(jsonBytes, 0, ret, 2, jsonBytes.length);
        return ret;
    }

    private void sendUdpMessage(String ipAddress, int port, byte[] message) throws IOException {
        try {
            InetAddress serverIpAddress = InetAddress.getByName(ipAddress);
            DatagramPacket dataGramPacket = new DatagramPacket(message, message.length, serverIpAddress, port);
            RingLogger.getConfigPortalLogger().info("AuthServerCommunicator Start UDP Sending --> " + ipAddress + " --> port : " + port);
            datagramSocket.send(dataGramPacket);

            RingLogger.getConfigPortalLogger().info("AuthServerCommunicator End UDP Sending --> " + ipAddress);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("AuthServerCommunicator Exception in sending UDP Packet --> " + e);
        }
    }

    public void startReceiver() {
        Runnable r = () -> {
            if (datagramSocket != null) {
                int receivedPacketCount = 0;
                long id = System.currentTimeMillis();
                byte[] receiveData = new byte[1024];
                DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
                RingLogger.getConfigPortalLogger().debug("UDP Packet Receiver thread started : " + id);
                long startTime = System.currentTimeMillis();
                while (System.currentTimeMillis() - startTime < 10_000) {
                    try {
                        datagramSocket.receive(receivePacket);
                        RingLogger.getConfigPortalLogger().debug("packetReceived --> " + ++receivedPacketCount);
                        Long responseTime = System.currentTimeMillis();
                        byte[] received_bytes = receivePacket.getData();
                        RingLogger.getConfigPortalLogger().debug("packet received ...");
                        RingLogger.getConfigPortalLogger().debug("received_bytes --> " + received_bytes.length);
                        HeaderAttributes headers = Parser.parseHeader(received_bytes, 1);
                        String receivedResponse = new String(received_bytes, headers.getHeaderLength(), received_bytes.length - headers.getHeaderLength(), "UTF-8").trim();
                        RingLogger.getConfigPortalLogger().debug("receivedResponse --> " + receivedResponse);
                        JsonReader reader = new JsonReader(new StringReader(receivedResponse));
                        JsonObject responseJsonObject = Parser.parseAsJsonObject(reader);
                        String pckId = responseJsonObject.get("pckId").getAsString();
                        PACKET_SET.add(pckId);
                        CheckLogDTO checkLogDTO = checkLogHashMap.get(pckId);
                        if (responseJsonObject.get("rc").getAsInt() == 0) {
                            checkLogDTO.setStatus(CheckLogDTO.SUCCESS);
                        } else {
                            checkLogDTO.setStatus(CheckLogDTO.FAILURE);
                        }
                        checkLogDTO.setResponseMSG(responseJsonObject.get("mg").getAsString());
                        checkLogDTO.setResponseTime(responseTime);
                        checkLogDTO.setResponseDate(responseTime);
                        checkLogHashMap.put(pckId, checkLogDTO);
                        RingLogger.getConfigPortalLogger().debug("## CHECK sendCount --> " + sendCount + " --> packetSet --> " + PACKET_SET.size());
                        if (sendCount > 0 && PACKET_SET.size() == sendCount) {
                            RingLogger.getConfigPortalLogger().debug("## sendCount EXIT --> " + sendCount);
                            Thread.sleep(1000);
                            if (sendCount == PACKET_SET.size()) {
                                break;
                            }
                        }
                    } catch (JsonSyntaxException | IOException | NumberFormatException | InterruptedException e) {
                        RingLogger.getConfigPortalLogger().error("Exception : " + e.getMessage());
                    }
                }
                RingLogger.getConfigPortalLogger().debug("[" + id + "] Receiver thread ended : " + id);
                datagramSocket.close();
                updateResponseStatus();
            } else {
                RingLogger.getConfigPortalLogger().error("startReceiver --> datagramSocket is NULL");
            }
        };
        new Thread(r).start();
    }

    public synchronized void notifyAuthServers(int serverType, String serverIP) throws IOException {
        if (datagramSocket != null) {
            ArrayList<AuthServerDTO> authServerList = AuthServerTaskScheduler.getInstance().getAuthServerList(new AuthServerDTO());
            if (serverIP == null || serverIP.isEmpty()) {
                for (AuthServerDTO authServerDTO : authServerList) {
                    if (authServerDTO.getServerStatus() == 1) {
                        sendUdpMessage(authServerDTO.getServerIP(), authServerDTO.getStartPort(), createUdpMessage(serverType, authServerDTO.getServerIP()));
                        sendCount++;
                        sleep(100);
                    }
                }
                RingLogger.getConfigPortalLogger().debug("## AFTER SEND MSG : sendCount --> " + sendCount);
            } else {
                for (AuthServerDTO authServerDTO : authServerList) {
                    if (authServerDTO.getServerIP().equals(serverIP)) {
                        sendUdpMessage(serverIP, authServerDTO.getStartPort(), createUdpMessage(serverType, authServerDTO.getServerIP()));
                        sendCount++;
                        break;
                    }
                }
            }
        } else {
            RingLogger.getConfigPortalLogger().error("AuthServerCommunicator datagramSocket is NULL");
        }
    }

    public synchronized void notifyAuthServers(List<CheckLogDTO> checkLogDTOs) throws IOException {
        if (datagramSocket != null) {
            ArrayList<AuthServerDTO> authServerList = AuthServerTaskScheduler.getInstance().getAuthServerList(new AuthServerDTO());
            for (CheckLogDTO dto : checkLogDTOs) {
                for (AuthServerDTO authServerDTO : authServerList) {
                    if (authServerDTO.getServerIP().equals(dto.getServerIP())) {
                        sendUdpMessage(authServerDTO.getServerIP(), authServerDTO.getStartPort(), createUdpMessage(dto.getServerType(), authServerDTO.getServerIP()));
                        sleep(1000);
                        sendCount++;
                        break;
                    }
                }
            }
        } else {
            RingLogger.getConfigPortalLogger().error("AuthServerCommunicator datagramSocket is NULL");
        }
    }

    public synchronized void notifyAuthServers(int action, int serverId) throws IOException {
        if (datagramSocket != null) {
            ArrayList<AuthServerDTO> authServerList = AuthServerTaskScheduler.getInstance().getAuthServerList(new AuthServerDTO());
            if (serverId <= 0) {
                for (AuthServerDTO authServerDTO : authServerList) {
                    if (authServerDTO.getServerStatus() == 1) {
                        sendUdpMessage(authServerDTO.getServerIP(), authServerDTO.getStartPort(), createUdpMessage(action, authServerDTO.getServerIP()));
                        sendCount++;
                        sleep(100);
                    }
                }
                RingLogger.getConfigPortalLogger().debug("## AFTER SEND MSG : sendCount --> " + sendCount);
            } else {
                for (AuthServerDTO authServerDTO : authServerList) {
                    if (authServerDTO.getId() == serverId) {
                        sendUdpMessage(authServerDTO.getServerIP(), authServerDTO.getStartPort(), createUdpMessage(action, authServerDTO.getServerIP()));
                        sendCount++;
                        break;
                    }
                }

            }
        } else {
            RingLogger.getConfigPortalLogger().error("AuthServerCommunicator datagramSocket is NULL");
        }
    }

    public static Long getPckId() {
        return uniqueId.incrementAndGet();
    }

    private void updateResponseStatus() {
        Set set = checkLogHashMap.entrySet();
        Iterator i = set.iterator();
        while (i.hasNext()) {
            Map.Entry map = (Map.Entry) i.next();
            CheckLogDTO dto = (CheckLogDTO) map.getValue();
            if (dto.getStatus().equals(CheckLogDTO.PENDING)) {
                dto.setStatus(CheckLogDTO.TIMEOUT);
                dto.setResponseMSG("Receive timed out");
                checkLogHashMap.put(map.getKey().toString(), dto);
            }
        }
    }

    public static HashMap<String, CheckLogDTO> getCheckLogList() {
        RingLogger.getConfigPortalLogger().info("checkLogHashMap size : " + checkLogHashMap.size());
        return AuthServerCommunicator.checkLogHashMap;
    }

    private void sleep(int time) {
        try {
            Thread.sleep(time);
        } catch (Exception e) {
        }
    }
}
