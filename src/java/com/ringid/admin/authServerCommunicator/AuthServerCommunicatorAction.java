/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.authServerCommunicator;

import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author Kazi Nasir Uddin Oly
 * @Date Jun 19, 2016
 */
public class AuthServerCommunicatorAction extends BaseAction {

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = checkAuthentication(mapping, form, request);
        AuthServerCommunicatorForm authServerCommunicatorForm = (AuthServerCommunicatorForm) form;
        try {
            if (target.equals(SUCCESS)) {
                int action = authServerCommunicatorForm.getNotificationType();
                RingLogger.getConfigPortalLogger().debug("AuthServer Communicator Action: " + action + " " + authServerCommunicatorForm.getId());
                notifyAuthServers(action, authServerCommunicatorForm.getId());
                authServerCommunicatorForm.setNotificationType(action);
                authServerCommunicatorForm.setServerMessage("SUCCESS");
            }
        } catch (Exception e) {
            target = FAILURE;
            authServerCommunicatorForm.setServerMessage("FAILURE");
            RingLogger.getConfigPortalLogger().debug("Auth Server Communcation error: " + e.getMessage());
        }
        return mapping.findForward(target);
    }

}
