/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.authServerCommunicator;

/**
 *
 * @author ipvision
 */
public class HeaderAttributes {

    private int action;
    private long serverPacketID;
    private String clientPacketID;
    private int headerLength;

    public int getHeaderLength() {
        return headerLength;
    }

    public void setHeaderLength(int headerLength) {
        this.headerLength = headerLength;
    }

    public int getAction() {
        return action;
    }

    public void setAction(int action) {
        this.action = action;
    }

    public long getServerPacketID() {
        return serverPacketID;
    }

    public void setServerPacketID(long serverPacketID) {
        this.serverPacketID = serverPacketID;
    }

    public String getClientPacketID() {
        return clientPacketID;
    }

    public void setClientPacketID(String clientPacketID) {
        this.clientPacketID = clientPacketID;
    }

    @Override
    public String toString() {
        return "HeaderAttributes{" + "action=" + action + ", serverPacketID=" + serverPacketID + ", clientPacketID=" + clientPacketID + ", headerLength=" + headerLength + '}';
    }

}
