/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.authServerCommunicator;

import com.ringid.admin.utils.log.RingLogger;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Rabby
 */
public class AuthServerCommunicatorServlet extends HttpServlet {

    private static final String FAILED = "{\"sucs\": false, \"ms\": \"Operation failed\"}";
    private static final String SUCCESS = "{\"sucs\": true, \"ms\": \"Operation successfull\"}";
    private static final String EXC = "{\"sucs\": false, \"ms\": \"Exception..\"}";
    private static final String INVALID_DATA = "{\"sucs\": false, \"ms\": \"Invalid data\"}";
    private static final String RELOAD_SUCCESSFULLY = "{\"sucs\": true, \"ms\": \"Reload Successfully\"}";

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doProcess(request, response);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doProcess(req, resp);
    }
    // </editor-fold>

    private void doProcess(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("application/json;charset=UTF-8");
        String result = FAILED;
        try {
            int action = 0, serverId = 0;
            if (containsParameter(request, "actn")) {
                action = Integer.parseInt(request.getParameter("actn"));
            }
            if (containsParameter(request, "serverId")) {
                serverId = Integer.parseInt(request.getParameter("serverId"));
            }
            notifyAuthServers(action, serverId);
            result = SUCCESS;

        } catch (Exception ex) {
            RingLogger.getConfigPortalLogger().error("Error in AuthServerCommunicatorServlet: " + ex.getMessage());
            response.getWriter().write(EXC);
            return;
        }
        response.getWriter().write(result);
    }

    private boolean containsParameter(HttpServletRequest request, String parameter) {
        return request.getParameter(parameter) != null && request.getParameter(parameter).trim().length() > 0;
    }

    protected void notifyAuthServers(final int serverType, final int serverId) throws IOException {
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    AuthServerCommunicator authServerCommunicator = new AuthServerCommunicator();
                    authServerCommunicator.startReceiver();
                    authServerCommunicator.notifyAuthServers(serverType, serverId);
                } catch (Exception e) {
                }
            }
        });
        t.start();
    }
}
