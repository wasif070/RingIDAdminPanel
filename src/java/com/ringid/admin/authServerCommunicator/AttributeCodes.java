/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.authServerCommunicator;

/**
 *
 * @author ipvision
 */
public class AttributeCodes {
    public static final int ACTION = 1;
    public static final int SERVER_PACKET_ID = 2;
    public static final int SESSION_ID = 3;
    public static final int TOTAL_PACKET = 4;
    public static final int PACKET_NUMBER = 5;
    public static final int UNIQUE_KEY = 6;
    public static final int USER_ID = 7;
    public static final int FRIEND_ID = 8;
    public static final int DEVICE = 9;
    public static final int CLIENT_PACKET_ID = 10;
    public static final int CALL_ID = 11;
    public static final int FRIEND_IDENTITY = 12;
    public static final int CALL_TIME = 13;
    public static final int PRESENCE = 14;
    public static final int IS_DEVIDED_PACKET = 15;
    public static final int USER_IDENTITY = 16;
    public static final int USER_NAME = 17;
    public static final int TOTAL_RECORDS = 18;
    public static final int USER_TABLE_IDS = 19;
    public static final int SUCCESS = 20;
    public static final int MESSAGE = 21;
    public static final int REASON_CODE = 22;
    public static final int STATUS = 23;
    public static final int DELETED = 24;
    public static final int WEB_UNIQUE_KEY = 28;
    public static final int WEB_TAB_ID = 29;
    public static final int DATA = 127;

    /* Contact list constants */
    public static final int CONTACT = 101;
    public static final int CONTACT_TYPE = 102;
    public static final int NEW_CONTACT_TYPE = 103;
    public static final int FRIENDSHIP_STATUS = 104;
    public static final int BLOCK_VALUE = 105;
    public static final int CHANGE_REQUESTER = 106;
    public static final int CONTACT_UPDATE_TIME = 107;
    // USER DETAILS /
    public static final int PASSWORD = 128;
    public static final int RESET_PASSWORD = 129;
    public static final int EMAIL = 130;
    public static final int DEVICE_UNIQUE_ID = 131;
    public static final int DEVICE_TOKEN = 132;
    public static final int MOBILE_PHONE = 133;
    public static final int BIRTH_DATE = 134;
    public static final int MARRIAGE_DAY = 135;
    public static final int GENDER = 136;
    public static final int COUNTRY_ID = 137;
    public static final int CURRENT_CITY = 138;
    public static final int HOME_CITY = 139;
    public static final int LANGUAGE_ID = 140;
    public static final int REGISTRATION_DATE = 141;
    public static final int DIALING_CODE = 142;
    public static final int IS_MY_NUMBER_VERIFIED = 143;
    public static final int IS_EMAIL_VERIFIED = 145;
    public static final int MY_NUMBER_VERIFICATION_CODE = 146;
    public static final int MYNUMBER_VERIFICATION_CODE_SENT_TIME = 147;
    public static final int EMAIL_VERIFICATION_CODE = 148;
    public static final int RECOVERY_VERIFICATION_CODE = 149;
    public static final int RECOVERY_VERIFICATION_CODE_SENT_TIME = 150;
    public static final int PROFILE_IMAGE = 151;
    public static final int PROFILE_IMAGE_ID = 152;
    public static final int COVER_IMAGE = 153;
    public static final int COVER_IMAGE_ID = 154;
    public static final int COVER_IMAGE_X = 155;
    public static final int COVER_IMAGE_Y = 156;
    public static final int ABOUT_ME = 157;
    public static final int TOTAL_FRIENDS = 158;
    public static final int RING_EMAIL = 159;
    public static final int UPDATE_TIME = 160;
    public static final int NOTIFICATION_VALIDITY = 161;
    public static final int WEB_LOGIN_ENABLED = 162;
    public static final int PC_LOGIN_ENABLED = 163;
    public static final int COMMON_FRIEND_SUGGESTION = 164;
    public static final int PHONE_NUMBER_SUGGESTION = 165;
    public static final int CONTACT_LIST_SUGGESTION = 166;
    
    
    // ########  web server to client
    
    public static final int AUTH_SERVER_IP = 1001;
    public static final int AUTH_SERVER_PORT = 1002;
    
}
