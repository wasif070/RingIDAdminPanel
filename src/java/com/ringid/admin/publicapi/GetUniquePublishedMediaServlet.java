package com.ringid.admin.publicapi;

import com.ringid.admin.utils.log.RingLogger;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;

public class GetUniquePublishedMediaServlet extends HttpServlet {

    Logger logger = RingLogger.getConfigPortalLogger();
    private static final String FAILED = "{\"sucs\": false, \"ms\": \"Operation failed\"}";
    private static final String exc = "{\"sucs\": false, \"ms\": \"Exception..\"}";

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doProcess(request, response);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doProcess(req, resp);
    }

    private void doProcess(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String result = FAILED;
        try {
            JSONObject allUniquePublishedMedia = CloudSupportController.getInstance().getAllUniquePublishedMedia();
            result = allUniquePublishedMedia.toString();
        } catch (Exception ex) {
            logger.error("Error in SpamMarkUnmarkStreamServlet: " + ex.getMessage());
            response.getWriter().write(exc);
            return;
        }
        response.getWriter().write(result);
    }
}
