/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.publicapi;

import com.ringid.admin.utils.log.RingLogger;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author reefat
 */
public class SetDebug extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doProcess(req, resp);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doProcess(req, resp);
    }

    private void doProcess(HttpServletRequest req, HttpServletResponse resp) {
        String debug = null;
        Object attribute = req.getAttribute("debug");
        if (attribute != null) {
            RingLogger.getConfigPortalLogger().debug("## attribute --> " + attribute);
            debug = attribute.toString();
        } else {
            String parameter = req.getParameter("debug");
            if (parameter != null) {
                RingLogger.getConfigPortalLogger().debug("## parameter --> " + parameter);
                debug = parameter;
            }
        }
        PrintWriter writer = null;
        try {
            writer = resp.getWriter();
        } catch (IOException ex) {
            RingLogger.getConfigPortalLogger().debug("IOException --> ", ex);
        }
        if (writer != null) {
            try {
                if (debug != null) {
                    RingLogger.setDebug(Integer.valueOf(debug));
                    writer.write(1);
                } else {
                    writer.write(0);
                }

            } catch (Exception e) {
            } finally {
                writer.close();
            }

        } else {
            RingLogger.getConfigPortalLogger().debug("## writer is NULL");
        }
    }
}
