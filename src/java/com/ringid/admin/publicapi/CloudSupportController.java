package com.ringid.admin.publicapi;

import com.ringid.admin.cassandra.CassandraDAO;
import com.ringid.admin.actions.servers.channelservers.ChannelServerDAO;
import com.ringid.admin.actions.servers.channelservers.ChannelServerDTO;
import com.ringid.admin.utils.log.RingLogger;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.UUID;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;

public class CloudSupportController {

    Logger logger = RingLogger.getConfigPortalLogger();
    private static CloudSupportController instance = null;

    public static CloudSupportController getInstance() {
        if (instance == null) {
            createInstance();
        }
        return instance;
    }

    public synchronized static void createInstance() {
        if (instance == null) {
            instance = new CloudSupportController();
        }
    }

    public JSONObject getAllUniquePublishedMedia() {
        JSONObject jSONObject = new JSONObject();
        ChannelServerDAO channelServerDAO = new ChannelServerDAO();
        ArrayList<ChannelServerDTO> channelServerList = channelServerDAO.getChannelServerList(new ChannelServerDTO());
        HashSet<String> list = new HashSet<>();
        try {
            if (channelServerList != null && channelServerList.size() > 0) {
                for (ChannelServerDTO channelServerDTO : channelServerList) {
                    List<UUID> serverChannels = CassandraDAO.getInstance().getServerChannels(channelServerDTO.getServerIP());
                    if (serverChannels != null) {
                        for (UUID channelId : serverChannels) {
                            HashSet<String> allUniquePublishedMedia = CassandraDAO.getInstance().getAllUniquePublishedMedia(channelId);
                            list.addAll(allUniquePublishedMedia);
                        }
                    }
                }
            } else {
                try {
                    jSONObject.put("sucs", false);
                    jSONObject.put("msg", "ChannelServerList empty");
                } catch (Exception ex) {
                }
            }

            if (list.isEmpty()) {
                try {
                    jSONObject.put("sucs", false);
                    jSONObject.put("msg", "No data found.");
                } catch (Exception ex) {
                }
            } else {
                String ids = "";
                for (String id : list) {
                    ids += (ids.length() > 0 ? "," : "") + id;
                }
                try {
                    jSONObject.put("sucs", true);
                    jSONObject.put("ids", ids);
                } catch (Exception ex) {
                }
            }
        } catch (Exception e) {
            try {
                logger.error("Exception [CloudSupportController - getAllUniquePublishedMedia] --> ", e);
                jSONObject.put("sucs", false);
                jSONObject.put("msg", "Exception occured.");
            } catch (Exception ex) {
            }
        }
        return jSONObject;
    }
}
