/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.publicapi;

import com.ringid.admin.dto.DashboardFeedbackDTO;
import com.ringid.admin.dto.RegistrationSummaryDTO;
import com.ringid.admin.dao.DashboardDAO;
import com.google.gson.Gson;
import com.ringid.admin.utils.log.RingLogger;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Rabby
 */
public class DashboardServlet extends HttpServlet {

    private static final String FAILED = "{\"sucs\": false, \"ms\": \"Operation failed\"}";
    private static final String SUCCESS = "{\"sucs\": true, \"ms\": \"Operation successfull\"}";
    private static final String EXC = "{\"sucs\": false, \"ms\": \"Exception..\"}";
    private static final String INVALID_DATA = "{\"sucs\": false, \"ms\": \"Invalid data\"}";
    SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy");
    long sevenDay = 7 * 24 * 60 * 60 * 1000;

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doProcess(request, response);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doProcess(req, resp);
    }
    // </editor-fold>

    private void doProcess(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("application/json;charset=UTF-8");
        sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
        String result = FAILED;
        try {
            long startTime = 0;
            long endTime = 0;
            String startDate, endDate;

            if (containsParameter(request, "et")) {
                endDate = request.getParameter("et");
            } else {
                endDate = sdf.format(new Date(System.currentTimeMillis()));
            }
            endTime = sdf.parse(endDate).getTime();

            if (containsParameter(request, "st")) {
                startDate = request.getParameter("st");
            } else {
                startDate = sdf.format(new Date(endTime - sevenDay));
            }
            startTime = sdf.parse(startDate).getTime();

            List<RegistrationSummaryDTO> list = DashboardDAO.getInstance().getUserCount(startTime, endTime);
            DashboardFeedbackDTO feedbackDTO = new DashboardFeedbackDTO();
            if (list != null) {
                long sum = 0;
                for (RegistrationSummaryDTO dto : list) {
                    sum += dto.getRegistrationCount();
                }
                feedbackDTO.setRegistrationStatistic(list);
                feedbackDTO.setTotalUser(sum);
                feedbackDTO.setSucs(true);

            } else {
                feedbackDTO.setSucs(false);
                feedbackDTO.setMessage("No data found");
            }
            feedbackDTO.setStartdate(startDate);
            feedbackDTO.setEndDate(endDate);
            result = new Gson().toJson(feedbackDTO);
        } catch (Exception ex) {
            RingLogger.getConfigPortalLogger().error("Error in DashboardServlet: " + ex.getMessage());
            response.getWriter().write(EXC);
            return;
        }
        response.getWriter().write(result);
    }

    private boolean containsParameter(HttpServletRequest request, String parameter) {
        return request.getParameter(parameter) != null && request.getParameter(parameter).trim().length() > 0;
    }
}
