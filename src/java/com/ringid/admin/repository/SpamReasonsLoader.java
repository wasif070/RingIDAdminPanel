/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.repository;

import com.ringid.admin.BaseDAO;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.dto.SpamReasonsDTO;
import com.ringid.admin.utils.Constants;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author mamun
 * @Date Feb 7, 2017
 */
public class SpamReasonsLoader extends BaseDAO {

    private final static long LOADING_INTERVAL = 60 * 60 * 1000;
    private long loadingTime = 0;
    private HashMap<Integer, SpamReasonsDTO> spamReasonsHashMapList;
    static Logger logger = RingLogger.getConfigPortalLogger();

    private SpamReasonsLoader() {

    }

    private static class SpamReasonsLoaderHolder {

        private static final SpamReasonsLoader INSTANCE = new SpamReasonsLoader();
    }

    public static SpamReasonsLoader getInstance() {
        return SpamReasonsLoaderHolder.INSTANCE;
    }

    private void forceLoadData() {
        spamReasonsHashMapList = new HashMap<>();
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            stmt = db.connection.createStatement();
            String sql = "SELECT id, spam_type, reason FROM spamreasons;";
            rs = stmt.executeQuery(sql);
            while (rs.next()) {
                SpamReasonsDTO dto = new SpamReasonsDTO();
                dto.setId(rs.getInt("id"));
                dto.setSpamType(rs.getInt("spam_type"));
                dto.setReason(rs.getString("reason"));
                spamReasonsHashMapList.put(dto.getId(), dto);
            }
            logger.debug(sql + "::" + spamReasonsHashMapList.size());

        } catch (Exception e) {
            logger.debug("SpamReasons List Exception...", e);
        } finally {
            close();
        }
    }

    private void checkForReload() {
        long currentTime = System.currentTimeMillis();
        if (currentTime - loadingTime > LOADING_INTERVAL) {
            loadingTime = currentTime;
            forceLoadData();
        }
    }

    public synchronized void forceReload() {
        loadingTime = System.currentTimeMillis();
        forceLoadData();
    }

    private boolean containsAny(SpamReasonsDTO dto, String queryString) {
        String[] spamType = {"SPAM USER", "SPAM FEED", "SPAM IMAGE", "SPAM MEDIA CONTENT", "SPAM CHANNEL", "SPAM LIVE STREAM", "SPAM PAGE", "SPAM COMMENT"};
        return (dto.getSpamType() > 0 && dto.getSpamType() <= spamType.length && spamType[dto.getSpamType() - 1].toLowerCase().contains(queryString))
                || (dto.getReason().toLowerCase().contains(queryString));
    }

    private ArrayList<SpamReasonsDTO> getSpamReasonsData(SpamReasonsDTO sdto) {
        checkForReload();
        ArrayList<SpamReasonsDTO> data = new ArrayList<>();
        Set set = spamReasonsHashMapList.entrySet();
        Iterator i = set.iterator();
        while (i.hasNext()) {
            Map.Entry me = (Map.Entry) i.next();
            SpamReasonsDTO dto = (SpamReasonsDTO) me.getValue();
            if (sdto.getQueryString() != null && sdto.getQueryString().length() > 0 && !containsAny(dto, sdto.getQueryString())) {
                continue;
            }
            data.add(dto);
        }
        return data;
    }

    public synchronized ArrayList<SpamReasonsDTO> getSpamReasonsList(SpamReasonsDTO sdto) {
        ArrayList<SpamReasonsDTO> spamReasonsList = getSpamReasonsData(sdto);
        Collections.sort(spamReasonsList, new SpamReasonsDTO.CompIdDSC());
        logger.debug("SpamReasons List Size: " + spamReasonsList.size());
        return spamReasonsList;
    }

    public synchronized ArrayList<SpamReasonsDTO> getSpamReasonsList(int columnValue, int sortType, SpamReasonsDTO sdto) {
        ArrayList<SpamReasonsDTO> spamReasonsList = getSpamReasonsData(sdto);
        Collections.sort(spamReasonsList, (sortType == Constants.ASC_SORT ? new SpamReasonsDTO.CompSpamTypeASC() : new SpamReasonsDTO.CompSpamTypeDSC()));

        logger.debug("SpamReasons List Size: " + spamReasonsList.size());
        return spamReasonsList;
    }

    public synchronized ArrayList<SpamReasonsDTO> getSpamReasonsListForDownload() {
        checkForReload();
        ArrayList<SpamReasonsDTO> data = new ArrayList<>(spamReasonsHashMapList.values());
        return data;
    }

    public synchronized SpamReasonsDTO getSpamReasonsDTO(int id) {
        checkForReload();
        if (spamReasonsHashMapList.containsKey(id)) {
            return spamReasonsHashMapList.get(id);
        }
        return null;
    }

    private ArrayList<SpamReasonsDTO> getSpamReasonsSearchData(SpamReasonsDTO sdto) {
        checkForReload();
        ArrayList<SpamReasonsDTO> data = new ArrayList<>();
        Set set = spamReasonsHashMapList.entrySet();
        Iterator i = set.iterator();
        while (i.hasNext()) {
            Map.Entry me = (Map.Entry) i.next();
            SpamReasonsDTO dto = (SpamReasonsDTO) me.getValue();
//            if (dto.getSpamType() == sdto.getSpamType()) {
//                data.add(dto);
//            }
            data.add(dto);
        }
        return data;
    }

    public ArrayList<SpamReasonsDTO> getSpamReasonsSearchList(SpamReasonsDTO spamReasonsDTO) {
        ArrayList<SpamReasonsDTO> spamReasonsList = getSpamReasonsSearchData(spamReasonsDTO);
        Collections.sort(spamReasonsList, new SpamReasonsDTO.CompIdDSC());
        logger.debug("SpamReasons List Size: " + spamReasonsList.size());
        return spamReasonsList;
    }
}
