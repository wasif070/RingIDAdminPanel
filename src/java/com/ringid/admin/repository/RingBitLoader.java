/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.repository;

import com.ringid.admin.dao.RingBitDAO;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.dto.RingBitDTO;
import java.util.ArrayList;
import java.util.List;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Rabby
 */
public class RingBitLoader {

    private long loadingtime;
    private final long LOADING_INTERVARL = 60 * 60 * 1000;
    private List<RingBitDTO> ringBitList;
    static Logger logger = RingLogger.getConfigPortalLogger();

    private RingBitLoader() {
        ringBitList = new ArrayList<>();
    }

    private static class RingBitLoaderHolder {

        private static final RingBitLoader INSTANCE = new RingBitLoader();
    }

    public static RingBitLoader getInstance() {
        return RingBitLoaderHolder.INSTANCE;
    }

    private void loadData() {
        ringBitList = RingBitDAO.getInstance().getRingBitLogList();
    }

    public void forceLoad() {
        loadingtime = System.currentTimeMillis();
        loadData();
    }

    private void checkForReload() {
        long currentTime = System.currentTimeMillis();
        if (currentTime - loadingtime > LOADING_INTERVARL) {
            loadingtime = currentTime;
            loadData();
        }
    }

    public List<RingBitDTO> getRingBitList(String searchText) {
        checkForReload();
        List<RingBitDTO> list = new ArrayList<>();
        for (RingBitDTO dto : ringBitList) {
            if (searchText != null && searchText.length() > 0) {
                if (!(dto.getUserName().toLowerCase().contains(searchText.trim().toLowerCase())
                        || dto.getRingId().toLowerCase().contains(searchText.trim().toLowerCase())
                        || String.valueOf(dto.getUserId()).toLowerCase().contains(searchText.trim().toLowerCase())
                        || dto.getAddTimeStr().toLowerCase().contains(searchText.trim().toLowerCase()))) {
                    continue;
                }
            }
            list.add(dto);
        }
        return list;
    }

//    private List<RingBitDTO> getRingBitSettingsData(RingBitDTO ringBitDTO) {
//        checkForReload();
//        List<RingBitDTO> data = new ArrayList<>();
//        Set set = ringBitSettingMap.entrySet();
//        Iterator i = set.iterator();
//        while (i.hasNext()) {
//            Map.Entry me = (Map.Entry) i.next();
//            RingBitDTO dto = (RingBitDTO) me.getValue();
//            if (ringBitDTO.getSearchText() != null && ringBitDTO.getSearchText().length() > 0 && !containsAny(dto, ringBitDTO.getSearchText())) {
//                continue;
//            }
//            data.add(dto);
//        }
//        return data;
//    }
//    public List<RingBitDTO> getRingBitSettingsList(RingBitDTO ringBitDTO) {
//        List<RingBitDTO> ringBitDTOs = getRingBitSettingsData(ringBitDTO);
//        if (ringBitDTO.getSortType() == Constants.ASC_SORT) {
//            Collections.sort(ringBitDTOs, new RingBitDTO.CompDSC());
//        } else {
//            Collections.sort(ringBitDTOs, new RingBitDTO.CompASC());
//        }
//        logger.debug("[getRingBitSettingsList] ringBitDTOs Size: " + ringBitDTOs.size());
//        return ringBitDTOs;
//    }
}
