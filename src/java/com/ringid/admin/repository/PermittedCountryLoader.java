/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.repository;

import com.ringid.admin.dto.PermittedCountryDTO;
import com.ringid.admin.dao.CoinDAO;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.utils.Constants;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author mamun
 */
public class PermittedCountryLoader {

    private final static long LOADING_INTERVAL = 60 * 60 * 1000;
    private long loadingTime = 0;
    private Map<String, PermittedCountryDTO> permittedCountryDTOMap;

    private PermittedCountryLoader() {
        this.permittedCountryDTOMap = new HashMap<>();
    }

    private static class PermittedCountryLoaderHolder {

        private static final PermittedCountryLoader INSTANCE = new PermittedCountryLoader();
    }

    public static PermittedCountryLoader getInstance() {
        return PermittedCountryLoaderHolder.INSTANCE;
    }

    private void forceLoadData() {
        permittedCountryDTOMap.clear();
        try {
            permittedCountryDTOMap = CoinDAO.getInstance().getPermittedCountryList();
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().debug("[PermittedCountryLoader] forceLoadData Exception...", e);
        }
    }

    private void checkForReload() {
        long currentTime = System.currentTimeMillis();
        if (currentTime - loadingTime > LOADING_INTERVAL) {
            loadingTime = currentTime;
            forceLoadData();
        }
    }

    public synchronized void forceReload() {
        loadingTime = System.currentTimeMillis();
        forceLoadData();
    }

    private boolean containsAny(PermittedCountryDTO dto, String queryString) {
        boolean flag = false;
        String[] supportedPlatform = dto.getSupportedPlatform().split(",");
        for (String supportedPlatform1 : supportedPlatform) {
            if (supportedPlatform1 != null && supportedPlatform1.length() > 0) {
                int platform = Integer.parseInt(supportedPlatform1);
                if (platform > 0 && platform < Constants.DEVICE.length && Constants.DEVICE[platform].toLowerCase().contains(queryString)) {
                    flag = true;
                    break;
                }
            }
        }
        return flag || (dto.getCurrency() != null && dto.getCurrency().toLowerCase().contains(queryString))
                || (String.valueOf(dto.getCashoutlimit()).toLowerCase().contains(queryString));
    }

    public synchronized ArrayList<PermittedCountryDTO> getPermittedCountryList(String dialingCode, int status, String queryString) {
        checkForReload();
        ArrayList<PermittedCountryDTO> data = new ArrayList<>();
        permittedCountryDTOMap.entrySet()
                .stream()
                .map(pair -> (PermittedCountryDTO) pair.getValue())
                .filter(dto -> !(dialingCode != null && dialingCode.length() > 0 && !dto.getMobilePhoneDialingCode().equals(dialingCode)))
                .filter(dto -> !(status != -1 && dto.getStatus() != status))
                .filter(dto -> !(queryString != null && queryString.length() > 0 && !containsAny(dto, queryString)))
                .forEach(dto -> {
                    data.add(dto);
                });
        RingLogger.getConfigPortalLogger().debug("Data" + data.size());
        return data;
    }

    public synchronized PermittedCountryDTO getPermittedCountryInfoByCountryShortCode(String countryShortCode) {

        if (permittedCountryDTOMap.containsKey(countryShortCode)) {
            return permittedCountryDTOMap.get(countryShortCode);
        }
        return null;
    }
}
