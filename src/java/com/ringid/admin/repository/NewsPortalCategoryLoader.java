/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.repository;

import com.ringid.admin.BaseDAO;
import com.ringid.admin.cassandra.CassandraDAO;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.dto.newsportal.NewsPortalCategoryDTO;
import com.ringid.admin.actions.newsPortal.category.NewsPortalCategoryForm;
import com.ringid.admin.utils.Constants;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import org.ringid.pages.PageCategoryDTO;
import org.ringid.utilities.AppConstants;

/**
 *
 * @author Kazi Nasir Uddin Oly
 * @Date Jun 20, 2016
 */
public class NewsPortalCategoryLoader extends BaseDAO {

    private final static long LOADING_INTERVAL = 60 * 60 * 1000;
    private long loadingTime = 0;
    private HashMap<UUID, NewsPortalCategoryDTO> newsPortalCategoryHashMapList;
    private ArrayList<NewsPortalCategoryDTO> pageCategoriesList;
    private static List<PageCategoryDTO> defaultPageCategoryDTOs;
    private static NewsPortalCategoryLoader newsPortalCategoryLoader = null;

    public NewsPortalCategoryLoader() {

    }

    public static NewsPortalCategoryLoader getInstance() {
        if (newsPortalCategoryLoader == null) {
            createLoader();
        }
        return newsPortalCategoryLoader;
    }

    private synchronized static void createLoader() {
        if (newsPortalCategoryLoader == null) {
            newsPortalCategoryLoader = new NewsPortalCategoryLoader();
        }
    }

    private void forceLoadData() {
        newsPortalCategoryHashMapList = new HashMap<>();
        pageCategoriesList = new ArrayList<>();
        try {
            List<PageCategoryDTO> list = CassandraDAO.getInstance().getPageCategories();
            list.forEach((pageCategoryDTO) -> {
                NewsPortalCategoryDTO dto = new NewsPortalCategoryDTO();
                dto.setId(pageCategoryDTO.getPageCategoryId());
                dto.setName(pageCategoryDTO.getCategoryName());
                dto.setStatus(1);
                dto.setType(pageCategoryDTO.getPageType());
                pageCategoriesList.add(dto);
                newsPortalCategoryHashMapList.put(dto.getId(), dto);
            });
            defaultPageCategoryDTOs = CassandraDAO.getInstance().getDefaultCategories();
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().debug("NewsPortalCategoryLoader List Exception...", e);
        } finally {
            close();
        }
    }

    private void checkForReload() {
        long currentTime = System.currentTimeMillis();
        if (currentTime - loadingTime > LOADING_INTERVAL) {
            loadingTime = currentTime;
            forceLoadData();
        }
    }

    private boolean contains(NewsPortalCategoryDTO dto, String searchText) {
        return (dto.getName() != null && dto.getName().toLowerCase().contains(searchText));
    }

    private ArrayList<NewsPortalCategoryDTO> getNewsPortalCategoryData(NewsPortalCategoryDTO sdto) {
        checkForReload();
        ArrayList<NewsPortalCategoryDTO> data = new ArrayList<>();
        for (NewsPortalCategoryDTO dto : pageCategoriesList) {
            if (sdto.getSearchText() != null && sdto.getSearchText().length() > 0 && !contains(dto, sdto.getSearchText())) {
                continue;
            }
            if (sdto.isIsSearchByType() && dto.getType() != sdto.getType()) {
                continue;
            }
            data.add(dto);
        }
        if (sdto.getSortType() == Constants.DESC_SORT) {
            Collections.sort(data, new NewsPortalCategoryDTO.CompDSC());
        } else {
            Collections.sort(data, new NewsPortalCategoryDTO.CompASC());
        }
        return data;
    }

    public synchronized ArrayList<NewsPortalCategoryDTO> getNewsPortalCategoryList(NewsPortalCategoryDTO sdto) {
        ArrayList<NewsPortalCategoryDTO> newsPortalCategoryList = getNewsPortalCategoryData(sdto);
        RingLogger.getConfigPortalLogger().debug("getNewsPortalCategoryList Size: " + newsPortalCategoryList.size());
        return newsPortalCategoryList;
    }

    public synchronized ArrayList<NewsPortalCategoryDTO> getNewsPortalCategoryList(int columnValue, int sortType, NewsPortalCategoryDTO sdto) {
        ArrayList<NewsPortalCategoryDTO> newsPortalCategoryList = getNewsPortalCategoryData(sdto);
        RingLogger.getConfigPortalLogger().debug("getNewsPortalCategoryList Size: " + newsPortalCategoryList.size());
        return newsPortalCategoryList;
    }

    public synchronized void forceReload() {
        loadingTime = System.currentTimeMillis();
        forceLoadData();
    }

    public synchronized NewsPortalCategoryDTO getNewsPortalCategoryDTO(UUID id) {
        checkForReload();
        if (newsPortalCategoryHashMapList.containsKey(id)) {
            return newsPortalCategoryHashMapList.get(id);
        }
        return null;
    }

    public synchronized ArrayList<NewsPortalCategoryDTO> getNewsPortalCategoryForDownload() {
        checkForReload();
        ArrayList<NewsPortalCategoryDTO> data = new ArrayList<>(pageCategoriesList);
        return data;
    }

    public PageCategoryDTO getProductPageDefaultCategoryDTO() {
        checkForReload();
        for (PageCategoryDTO dto : defaultPageCategoryDTOs) {
            if (dto.getPageType() == AppConstants.UserTypeConstants.PRODUCT_PAGE) {
                return dto;
            }
        }
        return new PageCategoryDTO();
    }

    public NewsPortalCategoryDTO getNewsPortalCategoryDTO(NewsPortalCategoryForm newsPortalCategoryForm) {
        NewsPortalCategoryDTO newsPortalCategoryDTO = new NewsPortalCategoryDTO();
        if (newsPortalCategoryForm.getPageId() != null && newsPortalCategoryForm.getPageId().length() > 0) {
            newsPortalCategoryDTO.setPageId(UUID.fromString(newsPortalCategoryForm.getPageId()));
        }
        newsPortalCategoryDTO.setName(newsPortalCategoryForm.getName());
        if (!newsPortalCategoryForm.isNull(newsPortalCategoryForm.getStatus())) {
            newsPortalCategoryDTO.setStatus(Integer.valueOf(newsPortalCategoryForm.getStatus()));
        }
        newsPortalCategoryDTO.setType(newsPortalCategoryForm.getType());
        return newsPortalCategoryDTO;
    }
}
