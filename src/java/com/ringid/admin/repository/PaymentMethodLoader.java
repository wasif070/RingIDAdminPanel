/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.repository;

import com.ringid.admin.dto.PaymentMethodDTO;
import com.ringid.admin.dao.CoinDAO;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.utils.Constants;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author mamun
 */
public class PaymentMethodLoader {

    private final static long LOADING_INTERVAL = 24 * 60 * 60 * 1000;
    private long loadingTime = 0;
    private Map<Long, PaymentMethodDTO> paymentMethodHashMap;
    static Logger logger = RingLogger.getConfigPortalLogger();

    private PaymentMethodLoader() {
        paymentMethodHashMap = new HashMap<>();
    }

    private static class PaymentMethodLoaderHolder {

        private static final PaymentMethodLoader INSTANCE = new PaymentMethodLoader();
    }

    public static PaymentMethodLoader getInstance() {
        return PaymentMethodLoaderHolder.INSTANCE;
    }

    private void forceLoadData() {
        paymentMethodHashMap = CoinDAO.getInstance().getPaymentMethodHashMap();
    }

    private void checkForReload() {
        long currentTime = System.currentTimeMillis();
        if (currentTime - loadingTime > LOADING_INTERVAL) {
            loadingTime = currentTime;
            forceLoadData();
        }
    }

    public synchronized void forceReload() {
        loadingTime = System.currentTimeMillis();
        forceLoadData();
    }

    public PaymentMethodDTO getPaymentMethodDTOById(long id) {
        PaymentMethodDTO dto = new PaymentMethodDTO();
        if (paymentMethodHashMap.containsKey(id)) {
            dto = paymentMethodHashMap.get(id);
        }
        return dto;
    }

    private boolean containsAny(PaymentMethodDTO dto, String searchText) {
        return dto.getName() != null && dto.getName().toLowerCase().contains(searchText);
    }

    private List<PaymentMethodDTO> getPaymentMethodListData(PaymentMethodDTO paymentMethodDTO) {
        checkForReload();
        List<PaymentMethodDTO> data = new ArrayList<>();
        Set set = paymentMethodHashMap.entrySet();
        Iterator i = set.iterator();
        while (i.hasNext()) {
            Map.Entry me = (Map.Entry) i.next();
            PaymentMethodDTO dto = (PaymentMethodDTO) me.getValue();
            if (paymentMethodDTO.getSearchText() != null && paymentMethodDTO.getSearchText().length() > 0 && !containsAny(dto, paymentMethodDTO.getSearchText())) {
                continue;
            }
            data.add(dto);
        }
        return data;
    }

    public List<PaymentMethodDTO> getPaymentMethodList(PaymentMethodDTO paymentMethodDTO) {
        List<PaymentMethodDTO> paymentMethodDTOs = getPaymentMethodListData(paymentMethodDTO);
        if (paymentMethodDTO.getSortType() == Constants.ASC_SORT) {
            Collections.sort(paymentMethodDTOs, new PaymentMethodDTO.CompDSC());
        } else {
            Collections.sort(paymentMethodDTOs, new PaymentMethodDTO.CompASC());
        }
        logger.debug("AppConstants List Size: " + paymentMethodDTOs.size());
        return paymentMethodDTOs;
    }
}
