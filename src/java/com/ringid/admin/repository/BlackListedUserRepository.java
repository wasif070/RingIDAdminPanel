/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.repository;

import com.ringid.admin.BaseDAO;
import com.ringid.admin.dto.BlackListedUserDTO;
import com.ringid.admin.utils.log.RingLogger;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author Rabby
 */
public class BlackListedUserRepository extends BaseDAO {

    private static BlackListedUserRepository instance = null;
    private long LAST_LOADED_TIME = 0;
    private final long LOADING_INTERVAL = 24 * 60 * 60 * 1000;
    private final HashMap<Long, BlackListedUserDTO> blackListedUsers;

    private BlackListedUserRepository() {
        blackListedUsers = new HashMap<>();
    }

    public static BlackListedUserRepository getInstance() {
        if (instance == null) {
            createInstance();
        }
        return instance;
    }

    private synchronized static void createInstance() {
        if (instance == null) {
            instance = new BlackListedUserRepository();
        }
    }

    private void loadData() {
        blackListedUsers.clear();
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            String sql = "SELECT userId, ringId, addTime FROM blacklisteduser;";
            ps = db.connection.prepareCall(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                BlackListedUserDTO dto = new BlackListedUserDTO();
                dto.setUserId(rs.getLong("userId"));
                dto.setRingId(rs.getLong("ringId"));
                dto.setAddTime(rs.getLong("addTime"));
                blackListedUsers.put(dto.getUserId(), dto);
            }
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in <BlackListedUserLoader> <loadData> -> " + e);
        } finally {
            close();
        }
    }

    private void checkForReload() {
        long currentTime = System.currentTimeMillis();
        if (currentTime - LAST_LOADED_TIME > LOADING_INTERVAL) {
            LAST_LOADED_TIME = currentTime;
            loadData();
        }
    }

    public void forceReload() {
        LAST_LOADED_TIME = System.currentTimeMillis();
        loadData();
    }

    public ArrayList<BlackListedUserDTO> getBlackListedUsers(String searchText) {
        checkForReload();
        ArrayList<BlackListedUserDTO> list = new ArrayList<>();
        return list;
    }

}
