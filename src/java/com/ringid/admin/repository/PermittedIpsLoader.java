/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.repository;

import com.ringid.admin.BaseDAO;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.dto.PermittedIpsDTO;
import com.ringid.admin.utils.Constants;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Rabby
 */
public class PermittedIpsLoader extends BaseDAO {

    private final static long LOADING_INTERVAL = 60 * 60 * 1000;
    private long loadingTime = 0;
    private HashMap<Integer, PermittedIpsDTO> permittedIpsHashMapList;
    static Logger logger = RingLogger.getConfigPortalLogger();

    private PermittedIpsLoader() {

    }

    private static class PermittedIpsLoaderHolder {

        private static final PermittedIpsLoader INSTANCE = new PermittedIpsLoader();
    }

    public static PermittedIpsLoader getInstance() {
        return PermittedIpsLoaderHolder.INSTANCE;
    }

    private void forceLoadData() {
        permittedIpsHashMapList = new HashMap<>();
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            stmt = db.connection.createStatement();
            String sql = "SELECT id, ip, status, update_time FROM permittedips order by id desc";
            rs = stmt.executeQuery(sql);
            while (rs.next()) {
                PermittedIpsDTO dto = new PermittedIpsDTO();
                dto.setId(rs.getInt("id"));
                dto.setIp(rs.getString("ip"));
                dto.setStatus(rs.getInt("status"));
                dto.setUpdateTime(rs.getLong("update_time"));
                permittedIpsHashMapList.put(dto.getId(), dto);
            }
            logger.debug(sql + "::" + permittedIpsHashMapList.size());

        } catch (Exception e) {
            logger.debug("Permitted Ips List Exception...", e);
        } finally {
            close();
        }
    }

    private void checkForReload() {
        long currentTime = System.currentTimeMillis();
        if (currentTime - loadingTime > LOADING_INTERVAL) {
            loadingTime = currentTime;
            forceLoadData();
        }
    }

    public synchronized void forceReload() {
        loadingTime = System.currentTimeMillis();
        forceLoadData();
    }

    private boolean containsAny(PermittedIpsDTO dto, String queryString) {
        String[] status = {"active", "inactive"};
        return (dto.getIp() != null && dto.getIp().contains(queryString))
                || (dto.getStatus() > 0 && dto.getStatus() <= status.length && status[dto.getStatus() - 1].contains(queryString));
    }

    private ArrayList<PermittedIpsDTO> getPermittedIpsData(PermittedIpsDTO sdto) {
        checkForReload();
        ArrayList<PermittedIpsDTO> data = new ArrayList<>();
        Set set = permittedIpsHashMapList.entrySet();
        Iterator i = set.iterator();
        while (i.hasNext()) {
            Map.Entry me = (Map.Entry) i.next();
            PermittedIpsDTO dto = (PermittedIpsDTO) me.getValue();
            if (sdto.getQueryString() != null && sdto.getQueryString().length() > 0 && !containsAny(dto, sdto.getQueryString())) {
                continue;
            }
            data.add(dto);
        }
        return data;
    }

    public synchronized ArrayList<PermittedIpsDTO> getPermittedIpsList(PermittedIpsDTO sdto) {
        ArrayList<PermittedIpsDTO> chatServerList = getPermittedIpsData(sdto);
        Collections.sort(chatServerList, new PermittedIpsDTO.CompIdDSC());
        logger.debug("getPermittedIpsList Size: " + chatServerList.size());
        return chatServerList;
    }

    public synchronized ArrayList<PermittedIpsDTO> getPermittedIpsList(int columnValue, int sortType, PermittedIpsDTO sdto) {
        ArrayList<PermittedIpsDTO> chatServerList = getPermittedIpsData(sdto);
        switch (columnValue) {
            case Constants.COLUMN_ONE:
                Collections.sort(chatServerList, (sortType == Constants.ASC_SORT ? new PermittedIpsDTO.CompServerIpASC() : new PermittedIpsDTO.CompServerIpDSC()));
                break;
            default:
                Collections.sort(chatServerList, new PermittedIpsDTO.CompIdDSC());
        }
        logger.debug("getPermittedIpsList Size: " + chatServerList.size());
        return chatServerList;
    }

    public synchronized ArrayList<PermittedIpsDTO> getPermittedIpsListForDownload() {
        checkForReload();
        ArrayList<PermittedIpsDTO> data = new ArrayList<>(permittedIpsHashMapList.values());
        return data;
    }

    public synchronized PermittedIpsDTO getPermittedIpsDTO(int id) {
        checkForReload();
        if (permittedIpsHashMapList.containsKey(id)) {
            return permittedIpsHashMapList.get(id);
        }
        return null;
    }
}
