/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.repository;

import com.ringid.admin.cassandra.CassandraDAO;
import com.ringid.admin.dto.ChannelVerificationDTO;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.utils.Constants;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import org.ringid.channel.ChannelDTO;
import org.ringid.utilities.AppConstants;

/**
 *
 * @author mamun
 */
public class ChannelRepository {

    private final static long LOADING_INTERVAL = 60 * 60 * 1000;
    private long loadingTime = 0;
    private HashMap<UUID, ChannelVerificationDTO> unverifiedChannelHashMapList;
    private HashMap<UUID, ChannelVerificationDTO> verifiedChannelHashMapList;
    private HashMap<UUID, ChannelVerificationDTO> featuredChannelHashMapList;
    private HashMap<UUID, ChannelVerificationDTO> officiallyVerifiedHashMapList;

    private ChannelRepository() {

    }

    private static class ChannelRepositoryHolder {

        private static final ChannelRepository REPOSITORY = new ChannelRepository();
    }

    public static ChannelRepository getInstance() {
        return ChannelRepositoryHolder.REPOSITORY;
    }

    private HashMap<UUID, ChannelVerificationDTO> getUnverifiedChannels() {
        HashMap<UUID, ChannelVerificationDTO> channelHashMap = new HashMap<>();
        try {
            List<ChannelDTO> channelList = CassandraDAO.getInstance().getUnverifiedChannels();
            if (channelList != null) {
                for (ChannelDTO channelDTO : channelList) {
                    ChannelVerificationDTO dto = new ChannelVerificationDTO();
                    dto.setChannelBooster(channelDTO.getChannelBooster());
                    dto.setChannelId(channelDTO.getChannelId());
                    dto.setMediaIds("n/A");
                    dto.setTitle(channelDTO.getTitle());
                    dto.setUserId(channelDTO.getOwnerId());
                    dto.setChannelType(channelDTO.getChannelType());
                    dto.setCountries(channelDTO.getCountries());
                    dto.setProfileImageUrl(channelDTO.getProfileImgUrl());
                    dto.setUserIdentity(CassandraDAO.getInstance().getRingID(channelDTO.getOwnerId()) + "");
                    dto.setChannelIP(channelDTO.getChannelIP());
                    dto.setChannelPort(channelDTO.getChannelPort());
                    dto.setStreamingServerIP(channelDTO.getStreamingServerIP());
                    dto.setStreamingServerPort(channelDTO.getStreamingServerPort());
                    dto.setChannelSharable(channelDTO.isIsChannelSharable());
                    dto.setChannelStatus(channelDTO.getChannelStatus());
                    switch (channelDTO.getChannelStatus()) {
                        case AppConstants.ACTIVATE_CHANNEL:
                            dto.setChannelStatusStr("Active");
                            break;
                        case AppConstants.DEACTIVATE_CHANNEL:
                            dto.setChannelStatusStr("Inactive");
                            break;
                        default:
                            dto.setChannelStatusStr("" + channelDTO.getChannelStatus());
                            break;
                    }
                    switch (dto.getChannelType()) {
                        case AppConstants.ChannelTypes.AUDIO_CHANNEL:
                            dto.setChannelTypeStr("AUDIO CHANNEL");
                            break;
                        case AppConstants.ChannelTypes.VIDEO_CHANNEL:
                            dto.setChannelTypeStr("VIDEO CHANNEL");
                            break;
                        case AppConstants.ChannelTypes.TV_CHANNEL:
                            dto.setChannelTypeStr("TV CHANNEL");
                            break;
                        case AppConstants.ChannelTypes.LIVE_AUDIO_CHANNEL:
                            dto.setChannelTypeStr("LIVE AUDIO CHANNEL");
                            break;
                        case AppConstants.ChannelTypes.LIVE_VIDEO_CHANNEL:
                            dto.setChannelTypeStr("LIVE VIDEO CHANNEL");
                            break;
                        default:
                            dto.setChannelTypeStr("" + dto.getChannelType());
                            break;
                    }
                    channelHashMap.put(dto.getChannelId(), dto);
                }
            }
            RingLogger.getConfigPortalLogger().debug("unverifiedChannelHashMapList.size() ::" + channelHashMap.size());

        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().debug("unverifiedChannelHashMapList List Exception...", e);
        }
        return channelHashMap;
    }

    private HashMap<UUID, ChannelVerificationDTO> getVerifiedChannels() {
        HashMap<UUID, ChannelVerificationDTO> channelHashMap = new HashMap<>();
        try {
            List<ChannelDTO> channelList = CassandraDAO.getInstance().getVerifiedChannels();
            if (channelList != null) {
                for (ChannelDTO channelDTO : channelList) {
                    ChannelVerificationDTO dto = new ChannelVerificationDTO();
                    dto.setChannelBooster(channelDTO.getChannelBooster());
                    dto.setChannelId(channelDTO.getChannelId());
                    dto.setMediaIds("n/A");
                    dto.setTitle(channelDTO.getTitle());
                    dto.setUserId(channelDTO.getOwnerId());
                    dto.setChannelType(channelDTO.getChannelType());
                    dto.setCountries(channelDTO.getCountries());
                    dto.setProfileImageUrl(channelDTO.getProfileImgUrl());
                    dto.setUserIdentity(CassandraDAO.getInstance().getRingID(channelDTO.getOwnerId()) + "");
                    dto.setChannelIP(channelDTO.getChannelIP());
                    dto.setChannelPort(channelDTO.getChannelPort());
                    dto.setStreamingServerIP(channelDTO.getStreamingServerIP());
                    dto.setStreamingServerPort(channelDTO.getStreamingServerPort());
                    dto.setChannelSharable(channelDTO.isIsChannelSharable());
                    dto.setChannelStatus(channelDTO.getChannelStatus());
                    dto.setChannelStatus(channelDTO.getChannelStatus());
                    switch (channelDTO.getChannelStatus()) {
                        case AppConstants.ACTIVATE_CHANNEL:
                            dto.setChannelStatusStr("Active");
                            break;
                        case AppConstants.DEACTIVATE_CHANNEL:
                            dto.setChannelStatusStr("Inactive");
                            break;
                        default:
                            dto.setChannelStatusStr("" + channelDTO.getChannelStatus());
                            break;
                    }
                    switch (dto.getChannelType()) {
                        case AppConstants.ChannelTypes.AUDIO_CHANNEL:
                            dto.setChannelTypeStr("AUDIO CHANNEL");
                            break;
                        case AppConstants.ChannelTypes.VIDEO_CHANNEL:
                            dto.setChannelTypeStr("VIDEO CHANNEL");
                            break;
                        case AppConstants.ChannelTypes.TV_CHANNEL:
                            dto.setChannelTypeStr("TV CHANNEL");
                            break;
                        case AppConstants.ChannelTypes.LIVE_AUDIO_CHANNEL:
                            dto.setChannelTypeStr("LIVE AUDIO CHANNEL");
                            break;
                        case AppConstants.ChannelTypes.LIVE_VIDEO_CHANNEL:
                            dto.setChannelTypeStr("LIVE VIDEO CHANNEL");
                            break;
                        default:
                            dto.setChannelTypeStr("" + dto.getChannelType());
                            break;
                    }
                    channelHashMap.put(dto.getChannelId(), dto);
                }
            }
            RingLogger.getConfigPortalLogger().debug("verifiedChannelHashMapList.size() ::" + channelHashMap.size());

        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().debug("verifiedChannelHashMapList List Exception...", e);
        }
        return channelHashMap;
    }

    private HashMap<UUID, ChannelVerificationDTO> getFeaturedChannels() {
        HashMap<UUID, ChannelVerificationDTO> channelHashMap = new HashMap<>();
        try {
            List<ChannelDTO> channelList = CassandraDAO.getInstance().getFeaturedChannels();
            if (channelList != null) {
                for (ChannelDTO channelDTO : channelList) {
                    ChannelVerificationDTO dto = new ChannelVerificationDTO();
                    dto.setChannelBooster(channelDTO.getChannelBooster());
                    dto.setChannelId(channelDTO.getChannelId());
                    dto.setMediaIds("n/A");
                    dto.setTitle(channelDTO.getTitle());
                    dto.setUserId(channelDTO.getOwnerId());
                    dto.setChannelType(channelDTO.getChannelType());
                    dto.setCountries(channelDTO.getCountries());
                    dto.setProfileImageUrl(channelDTO.getProfileImgUrl());
                    dto.setUserIdentity(CassandraDAO.getInstance().getRingID(channelDTO.getOwnerId()) + "");
                    dto.setChannelIP(channelDTO.getChannelIP());
                    dto.setChannelPort(channelDTO.getChannelPort());
                    dto.setStreamingServerIP(channelDTO.getStreamingServerIP());
                    dto.setStreamingServerPort(channelDTO.getStreamingServerPort());
                    dto.setChannelSharable(channelDTO.isIsChannelSharable());
                    dto.setChannelStatus(channelDTO.getChannelStatus());
                    dto.setChannelStatus(channelDTO.getChannelStatus());
                    switch (channelDTO.getChannelStatus()) {
                        case AppConstants.ACTIVATE_CHANNEL:
                            dto.setChannelStatusStr("Active");
                            break;
                        case AppConstants.DEACTIVATE_CHANNEL:
                            dto.setChannelStatusStr("Inactive");
                            break;
                        default:
                            dto.setChannelStatusStr("" + channelDTO.getChannelStatus());
                            break;
                    }
                    switch (dto.getChannelType()) {
                        case AppConstants.ChannelTypes.AUDIO_CHANNEL:
                            dto.setChannelTypeStr("AUDIO CHANNEL");
                            break;
                        case AppConstants.ChannelTypes.VIDEO_CHANNEL:
                            dto.setChannelTypeStr("VIDEO CHANNEL");
                            break;
                        case AppConstants.ChannelTypes.TV_CHANNEL:
                            dto.setChannelTypeStr("TV CHANNEL");
                            break;
                        case AppConstants.ChannelTypes.LIVE_AUDIO_CHANNEL:
                            dto.setChannelTypeStr("LIVE AUDIO CHANNEL");
                            break;
                        case AppConstants.ChannelTypes.LIVE_VIDEO_CHANNEL:
                            dto.setChannelTypeStr("LIVE VIDEO CHANNEL");
                            break;
                        default:
                            dto.setChannelTypeStr("" + dto.getChannelType());
                            break;
                    }
                    channelHashMap.put(dto.getChannelId(), dto);
                }
            }
            RingLogger.getConfigPortalLogger().debug("featuredChannelHashMapList.size() ::" + channelHashMap.size());

        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().debug("getFeaturedChannels List Exception...", e);
        }
        return channelHashMap;
    }

    private HashMap<UUID, ChannelVerificationDTO> getOfficiallyVerifiedChannels() {
        HashMap<UUID, ChannelVerificationDTO> channelHashMap = new HashMap<>();
        try {
            List<ChannelDTO> channelList = CassandraDAO.getInstance().getOfficiallyVerifiedChannels();
            if (channelList != null) {
                for (ChannelDTO channelDTO : channelList) {
                    ChannelVerificationDTO dto = new ChannelVerificationDTO();
                    dto.setChannelBooster(channelDTO.getChannelBooster());
                    dto.setChannelId(channelDTO.getChannelId());
                    dto.setMediaIds("n/A");
                    dto.setTitle(channelDTO.getTitle());
                    dto.setUserId(channelDTO.getOwnerId());
                    dto.setChannelType(channelDTO.getChannelType());
                    dto.setCountries(channelDTO.getCountries());
                    dto.setProfileImageUrl(channelDTO.getProfileImgUrl());
                    dto.setUserIdentity(CassandraDAO.getInstance().getRingID(channelDTO.getOwnerId()) + "");
                    dto.setChannelIP(channelDTO.getChannelIP());
                    dto.setChannelPort(channelDTO.getChannelPort());
                    dto.setStreamingServerIP(channelDTO.getStreamingServerIP());
                    dto.setStreamingServerPort(channelDTO.getStreamingServerPort());
                    dto.setChannelSharable(channelDTO.isIsChannelSharable());
                    dto.setChannelStatus(channelDTO.getChannelStatus());
                    dto.setChannelStatus(channelDTO.getChannelStatus());
                    switch (channelDTO.getChannelStatus()) {
                        case AppConstants.ACTIVATE_CHANNEL:
                            dto.setChannelStatusStr("Active");
                            break;
                        case AppConstants.DEACTIVATE_CHANNEL:
                            dto.setChannelStatusStr("Inactive");
                            break;
                        default:
                            dto.setChannelStatusStr("" + channelDTO.getChannelStatus());
                            break;
                    }
                    switch (dto.getChannelType()) {
                        case AppConstants.ChannelTypes.AUDIO_CHANNEL:
                            dto.setChannelTypeStr("AUDIO CHANNEL");
                            break;
                        case AppConstants.ChannelTypes.VIDEO_CHANNEL:
                            dto.setChannelTypeStr("VIDEO CHANNEL");
                            break;
                        case AppConstants.ChannelTypes.TV_CHANNEL:
                            dto.setChannelTypeStr("TV CHANNEL");
                            break;
                        case AppConstants.ChannelTypes.LIVE_AUDIO_CHANNEL:
                            dto.setChannelTypeStr("LIVE AUDIO CHANNEL");
                            break;
                        case AppConstants.ChannelTypes.LIVE_VIDEO_CHANNEL:
                            dto.setChannelTypeStr("LIVE VIDEO CHANNEL");
                            break;
                        default:
                            dto.setChannelTypeStr("" + dto.getChannelType());
                            break;
                    }
                    channelHashMap.put(dto.getChannelId(), dto);
                }
            }
            RingLogger.getConfigPortalLogger().debug("officiallyVerifiedHashMapList.size() ::" + channelHashMap.size());

        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().debug("getOfficiallyVerifiedChannels List Exception...", e);
        }
        return channelHashMap;
    }

    private void forceLoadData() {
        unverifiedChannelHashMapList = getUnverifiedChannels();
        verifiedChannelHashMapList = getVerifiedChannels();
        featuredChannelHashMapList = getFeaturedChannels();
        officiallyVerifiedHashMapList = getOfficiallyVerifiedChannels();
    }

    private void checkForReload() {
        long currentTime = System.currentTimeMillis();
        if (currentTime - loadingTime > LOADING_INTERVAL) {
            loadingTime = currentTime;
            forceLoadData();
        }
    }

    public synchronized void forceReload() {
        loadingTime = System.currentTimeMillis();
        forceLoadData();
    }

    private ArrayList<ChannelVerificationDTO> getChannelVerificationData(ChannelVerificationDTO sdto) {
        checkForReload();
        ArrayList<ChannelVerificationDTO> data = new ArrayList<>();
        Set set = new HashSet();
        switch (sdto.getChannelType()) {
            case 1:
                set = unverifiedChannelHashMapList.entrySet();
                break;
            case 2:
                set = verifiedChannelHashMapList.entrySet();
                break;
            case 3:
                set = featuredChannelHashMapList.entrySet();
                break;
            case 4:
                set = officiallyVerifiedHashMapList.entrySet();
                break;
        }
        Iterator i = set.iterator();
        while (i.hasNext()) {
            Map.Entry me = (Map.Entry) i.next();
            ChannelVerificationDTO dto = (ChannelVerificationDTO) me.getValue();
            if (sdto.getSearchText() != null && sdto.getSearchText().length() > 0) {
                if (!(dto.getTitle().toLowerCase().contains(sdto.getSearchText().trim().toLowerCase())
                        || dto.getChannelId().toString().toLowerCase().contains(sdto.getSearchText().trim().toLowerCase())
                        || dto.getUserIdentity().contains(sdto.getSearchText().trim()))) {
                    continue;
                }
            }
            data.add(dto);
        }

        if (sdto.getSortType() == Constants.ASC_SORT) {
            Collections.sort(data, new ChannelVerificationDTO.CompASC());
        } else {
            Collections.sort(data, new ChannelVerificationDTO.CompDSC());
        }
        return data;
    }

    public synchronized ArrayList<ChannelVerificationDTO> getChannelVerificationList(ChannelVerificationDTO sdto) {
        ArrayList<ChannelVerificationDTO> channelList = getChannelVerificationData(sdto);
        RingLogger.getConfigPortalLogger().debug("ChannelList Size: " + channelList.size());
        return channelList;
    }

    public synchronized ChannelVerificationDTO getChannelVerificationDTO(UUID id, int channelType) {
        checkForReload();
        switch (channelType) {
            case 1:
                if (unverifiedChannelHashMapList.containsKey(id)) {
                    return unverifiedChannelHashMapList.get(id);
                }
                break;
            case 2:
                if (verifiedChannelHashMapList.containsKey(id)) {
                    return verifiedChannelHashMapList.get(id);
                }
                break;
            case 3:
                if (featuredChannelHashMapList.containsKey(id)) {
                    return featuredChannelHashMapList.get(id);
                }
                break;
            case 4:
                if (officiallyVerifiedHashMapList.containsKey(id)) {
                    return officiallyVerifiedHashMapList.get(id);
                }
                break;
        }
        return null;
    }

    public List<ChannelVerificationDTO> getChannelVerificationDTOList(List<UUID> channelIds) {
        checkForReload();
        List<ChannelVerificationDTO> list = new ArrayList<>();
        channelIds
                .forEach(channelId -> {
                    if (unverifiedChannelHashMapList.containsKey(channelId)) {
                        list.add(unverifiedChannelHashMapList.get(channelId));
                    } else if (verifiedChannelHashMapList.containsKey(channelId)) {
                        list.add(verifiedChannelHashMapList.get(channelId));
                    } else if (featuredChannelHashMapList.containsKey(channelId)) {
                        list.add(featuredChannelHashMapList.get(channelId));
                    } else if (officiallyVerifiedHashMapList.containsKey(channelId)) {
                        list.add(officiallyVerifiedHashMapList.get(channelId));
                    }
                });
        RingLogger.getConfigPortalLogger().debug("ChannelList Size: " + list.size());
        return list;
    }
}
