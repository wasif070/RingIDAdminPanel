/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.repository;

import com.ringid.admin.cassandra.CassandraDAO;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.dto.newsportal.NewsPortalTypeDTO;
import com.ringid.admin.actions.newsPortal.type.NewsPortalTypeForm;
import com.ringid.admin.utils.Constants;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import org.ringid.pages.PageCategoryDTO;

/**
 *
 * @author Kazi Nasir Uddin Oly
 * @Date Jun 20, 2016
 */
public class NewsPortalTypeLoader {

    private final static long LOADING_INTERVAL = 60 * 60 * 1000;
    private long loadingTime = 0;
    private static ArrayList<NewsPortalTypeDTO> newsPortalTypeList;

    private NewsPortalTypeLoader() {
        newsPortalTypeList = null;
    }

    private static class NewsPortalTypeTaskSchedulerHolder {

        private static final NewsPortalTypeLoader INSTANCE = new NewsPortalTypeLoader();
    }

    public static NewsPortalTypeLoader getInstance() {
        return NewsPortalTypeTaskSchedulerHolder.INSTANCE;
    }

    private void loadData() {
        newsPortalTypeList = new ArrayList<>();
        try {
            List<PageCategoryDTO> list = CassandraDAO.getInstance().getDefaultCategories();
            list
                    .forEach(pageCategoryDTO -> {
                        NewsPortalTypeDTO newsPortalTypeDTO = new NewsPortalTypeDTO();
                        newsPortalTypeDTO.setId(pageCategoryDTO.getDefaultPageCategoryId());
                        newsPortalTypeDTO.setName(pageCategoryDTO.getCategoryName());
                        newsPortalTypeDTO.setType(pageCategoryDTO.getPageType());
                        newsPortalTypeList.add(newsPortalTypeDTO);
                    });
        } catch (Exception ex) {
            RingLogger.getConfigPortalLogger().error(ex);
        }
    }

    public synchronized void forceLoadData() {
        loadingTime = System.currentTimeMillis();
        loadData();
    }

    private void checkForReload() {
        long currentTime = System.currentTimeMillis();
        if (currentTime - loadingTime > LOADING_INTERVAL) {
            loadingTime = currentTime;
            forceLoadData();
        }
    }

    public ArrayList<NewsPortalTypeDTO> getNewsPortalTypeList(NewsPortalTypeDTO dto) {
        checkForReload();
        ArrayList<NewsPortalTypeDTO> newsPortalList = new ArrayList<>();
        try {
            newsPortalTypeList
                    .stream()
                    .filter(newsPortalTypeDTO -> {
                        if (dto.getSearchText() != null && dto.getSearchText().length() > 0 && !newsPortalTypeDTO.getName().toLowerCase().contains(dto.getSearchText())) {
                            return false;
                        }
                        return !(dto.isIsSearchByType() && dto.getType() != newsPortalTypeDTO.getType());
                    })
                    .forEach(newsPortalTypeDTO -> {
                        newsPortalList.add(newsPortalTypeDTO);
                    });
        } catch (Exception ex) {
            RingLogger.getConfigPortalLogger().error(ex);
        }
        if (dto.getSortType() == Constants.DESC_SORT) {
            Collections.sort(newsPortalList, new NewsPortalTypeDTO.CompDSC());
        } else {
            Collections.sort(newsPortalList, new NewsPortalTypeDTO.CompASC());
        }
        return newsPortalList;
    }

    public ArrayList<NewsPortalTypeDTO> getNewsPortalTypeList(int columnValue, int sortType, NewsPortalTypeDTO dto) {
        return newsPortalTypeList;
    }

    public NewsPortalTypeDTO getNewsPortalTypeInfoById(UUID id) {
        for (NewsPortalTypeDTO dto : newsPortalTypeList) {
            if (dto.getId().equals(id)) {
                return dto;
            }
        }
        return null;
    }

    public ArrayList<NewsPortalTypeDTO> getNewsPortalTypeListForDownload() {
        return newsPortalTypeList;
    }

    public NewsPortalTypeDTO getNewsPortalTypeDTO(NewsPortalTypeForm newsPortalTypeForm) {
        NewsPortalTypeDTO newsPortalTypeDTO = new NewsPortalTypeDTO();
        if (newsPortalTypeForm.getPageId() != null && newsPortalTypeForm.getPageId().length() > 0) {
            newsPortalTypeDTO.setId(UUID.fromString(newsPortalTypeForm.getPageId()));
        }
        newsPortalTypeDTO.setName(newsPortalTypeForm.getName());
        if (!newsPortalTypeForm.isNull(newsPortalTypeForm.getStatus())) {
            newsPortalTypeDTO.setStatus(Integer.valueOf(newsPortalTypeForm.getStatus()));
        }
        newsPortalTypeDTO.setType(newsPortalTypeForm.getType());
        return newsPortalTypeDTO;
    }
}
