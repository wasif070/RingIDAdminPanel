/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.repository;

import com.ringid.admin.cassandra.CassandraDAO;
import com.ringid.admin.dto.ChannelVerificationDTO;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.utils.Constants;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.ringid.channel.ChannelCategoryDTO;

/**
 *
 * @author mamun
 */
public class ChannelCategoryLoader {

    private final static long LOADING_INTERVAL = 60 * 60 * 1000;
    private long loadingTime = 0;
    private HashMap<Integer, ChannelCategoryDTO> channelCatHashMap;
    private static ChannelCategoryLoader instance = null;

    public static ChannelCategoryLoader getInstance() {
        if (instance == null) {
            createLoader();
        }
        return instance;
    }

    private synchronized static void createLoader() {
        if (instance == null) {
            instance = new ChannelCategoryLoader();
        }
    }

    private void forceLoadData() {
        channelCatHashMap = getChannelCategory();
    }

    private void checkForReload() {
        long currentTime = System.currentTimeMillis();
        if (currentTime - loadingTime > LOADING_INTERVAL) {
            loadingTime = currentTime;
            forceLoadData();
        }
    }

    public synchronized void forceReload() {
        loadingTime = System.currentTimeMillis();
        forceLoadData();
    }

    private HashMap<Integer, ChannelCategoryDTO> getChannelCategory() {
        HashMap<Integer, ChannelCategoryDTO> chanlCatHashMap = new HashMap<>();
        try {
            List<ChannelCategoryDTO> channelCatList = CassandraDAO.getInstance().getChannelCategoryList();
            if (channelCatList != null) {
                channelCatList
                        .forEach(channelCatDTO -> {
                            chanlCatHashMap.put(channelCatDTO.getCategoryId(), channelCatDTO);
                        });
            }
            RingLogger.getConfigPortalLogger().debug("getChannelCategory.size() ::" + chanlCatHashMap.size());

        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().debug("getChannelCategory List Exception...", e);
        }
        return chanlCatHashMap;
    }

    private boolean contains(ChannelCategoryDTO dto, String searchText) {
        return (String.valueOf(dto.getCategoryId()).contains(searchText))
                || (dto.getCategory() != null && dto.getCategory().toLowerCase().contains(searchText))
                || (dto.getDescription() != null && dto.getDescription().toLowerCase().contains(searchText))
                || (String.valueOf(dto.getVerticalWeight()).contains(searchText))
                || (String.valueOf(dto.getHorizontalWeight()).contains(searchText))
                || (String.valueOf(dto.getChannelCount()).contains(searchText));
    }

    public List<ChannelCategoryDTO> getChannelCategoryList(ChannelVerificationDTO sdto) {
        checkForReload();
        List<ChannelCategoryDTO> data = new ArrayList<>();
        Set set = channelCatHashMap.entrySet();
        Iterator i = set.iterator();
        while (i.hasNext()) {
            Map.Entry me = (Map.Entry) i.next();
            ChannelCategoryDTO dto = (ChannelCategoryDTO) me.getValue();
            if (sdto.getSearchText() != null && sdto.getSearchText().length() > 0 && !contains(dto, sdto.getSearchText())) {
                continue;
            }
            data.add(dto);
        }
        if (sdto.getSortType() == Constants.DESC_SORT) {
            Collections.sort(data, new ChannelVerificationDTO.CompCatDSC());
        } else {
            Collections.sort(data, new ChannelVerificationDTO.CompCatASC());
        }
        return data;
    }

    public ChannelCategoryDTO getChannelCategoryDTOById(int channelCategoryId) {
        if (channelCatHashMap.containsKey(channelCategoryId)) {
            return channelCatHashMap.get(channelCategoryId);
        }
        return null;
    }
}
