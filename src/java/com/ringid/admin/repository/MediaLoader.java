/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.repository;

import com.google.gson.Gson;
import com.ringid.admin.cassandra.CassandraDAO;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.dto.MediaDTO;
import com.ringid.admin.utils.Constants;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import org.ringid.newsfeeds.CassMultiMediaDTO;

/**
 *
 * @author Kazi Nasir Uddin Oly
 * @Date Dec 10, 2016
 */
public class MediaLoader {

    private final static long LOADING_INTERVAL = 15 * 60 * 1000;
    private long loadingTime = 0;
    private List<CassMultiMediaDTO> mediaList;
    private List<CassMultiMediaDTO> trendingMediaList;
    private static MediaLoader mediaLoader = null;

    private MediaLoader() {

    }

    public static MediaLoader getInstance() {
        if (mediaLoader == null) {
            createLoader();
        }
        return mediaLoader;
    }

    private synchronized static void createLoader() {
        if (mediaLoader == null) {
            mediaLoader = new MediaLoader();
        }
    }

    public void forceReload() {
        loadingTime = System.currentTimeMillis();
        forceLoadData();
    }

    private void checkForReload() {
        long currentTime = System.currentTimeMillis();
        if (currentTime - loadingTime > LOADING_INTERVAL) {
            loadingTime = currentTime;
            forceLoadData();
        }
    }

    private void forceLoadData() {
        try {
            mediaList = CassandraDAO.getInstance().getRecentMediaList();
            trendingMediaList = CassandraDAO.getInstance().getTrendingMediaList();
            RingLogger.getConfigPortalLogger().debug("## MediaList Size: " + (mediaList != null ? mediaList.size() : " MediaList is NULL"));
            RingLogger.getConfigPortalLogger().debug("## Trending Media List Size: " + (trendingMediaList != null ? trendingMediaList.size() : " trendingMediaList is NULL"));
        } catch (Exception ex) {
            RingLogger.getConfigPortalLogger().error("Exception : MediaLoader [forceLoadData] --> " + ex);
            resetData();
        } catch (Throwable ex) {
            RingLogger.getConfigPortalLogger().error("Throwable : MediaLoader [forceLoadData] --> " + ex);
            resetData();
        }
    }

    private void resetData() {
        mediaList = new ArrayList<>();
        trendingMediaList = new ArrayList<>();
    }

    private boolean contains(CassMultiMediaDTO dto, String searchText) {
        return (String.valueOf(dto.getUserId()).contains(searchText))
                || (dto.getFullName() != null && dto.getFullName().toLowerCase().contains(searchText))
                || (dto.getTitle() != null && dto.getTitle().toLowerCase().contains(searchText));
    }

    public List<CassMultiMediaDTO> getRecentMedias(MediaDTO sdto) {
        checkForReload();
        List<CassMultiMediaDTO> list = new ArrayList<>();
        mediaList
                .stream()
                .filter(cassMultiMediaDTO -> {
                    return !(sdto.getSearchText() != null && sdto.getSearchText().length() > 0 && !contains(cassMultiMediaDTO, sdto.getSearchText()));
                })
                .forEach(cassMultiMediaDTO -> {
                    list.add(cassMultiMediaDTO);
                });
        if (sdto.getSortType() == Constants.DESC_SORT) {
            Collections.sort(list, new MediaDTO.CompDSC());
        } else {
            Collections.sort(list, new MediaDTO.CompASC());
        }
        return list;
    }

    public List<CassMultiMediaDTO> getTrendingMedia(MediaDTO sdto) {
        checkForReload();
        List<CassMultiMediaDTO> list = new ArrayList<>();
        trendingMediaList
                .stream()
                .filter(cassMultiMediaDTO -> {
                    return !(sdto.getSearchText() != null && sdto.getSearchText().length() > 0 && !contains(cassMultiMediaDTO, sdto.getSearchText()));
                })
                .forEach(cassMultiMediaDTO -> {
                    list.add(cassMultiMediaDTO);
                });
        if (sdto.getSortType() == Constants.DESC_SORT) {
            Collections.sort(list, new MediaDTO.CompDSC());
        } else {
            Collections.sort(list, new MediaDTO.CompASC());
        }
        return list;
    }

    public CassMultiMediaDTO getTrendingMediaDetails(int index) {
        return trendingMediaList.get(index);
    }

    public CassMultiMediaDTO getMediaDetails(int index) {
        return mediaList.get(index);
    }

    public List<CassMultiMediaDTO> getTrendingMutliMediaDTOsFromId(ArrayList<UUID> mediaIds) {
        List<CassMultiMediaDTO> mediaDTOs = new ArrayList<>();
        for (UUID mediaId : mediaIds) {
            for (CassMultiMediaDTO mediaDTO : trendingMediaList) {
                if (mediaDTO.getMultiMediaId().equals(mediaId)) {
                    mediaDTOs.add(mediaDTO);
                    break;
                }
            }
        }
        return mediaDTOs;
    }

    public boolean makeTrending(ArrayList<UUID> mediaIds) throws Exception {
        RingLogger.getConfigPortalLogger().debug("Make Trending MediaIds " + new Gson().toJson(mediaIds));
        boolean ret = CassandraDAO.getInstance().makeTrendingMedia(mediaIds);
        RingLogger.getConfigPortalLogger().debug("Make Trending ReasonCode: " + ret);
        loadingTime = 0;
        forceLoadData();
        return ret;
    }

    public boolean removeTrending(List<CassMultiMediaDTO> mediaDTos) throws Exception {
        RingLogger.getConfigPortalLogger().debug("removeTrending MediaIds " + new Gson().toJson(mediaDTos));
        boolean ret = CassandraDAO.getInstance().removeTrendingMedia(mediaDTos);
        RingLogger.getConfigPortalLogger().debug("Remove Trending ReasonCode: " + ret);
        loadingTime = 0;
        forceLoadData();
        return ret;
    }

    public List<CassMultiMediaDTO> getRecentMediasByRingId(Long ringId) {
        long userId = getUserIdByRingId(ringId);
        List<CassMultiMediaDTO> mediaDTOs = new ArrayList<>();
        mediaList
                .stream()
                .filter(mediaDTO -> {
                    return mediaDTO.getUserId() == userId;
                })
                .forEach(mediaDTO -> {
                    mediaDTOs.add(mediaDTO);
                });
        return mediaDTOs;
    }

    public List<CassMultiMediaDTO> getTrendingMediasByRingId(Long ringId) {
        long userId = getUserIdByRingId(ringId);
        List<CassMultiMediaDTO> mediaDTOs = new ArrayList<>();
        trendingMediaList
                .stream()
                .filter(mediaDTO -> {
                    return mediaDTO.getUserId() == userId;
                })
                .forEach(mediaDTO -> {
                    mediaDTOs.add(mediaDTO);
                });
        return mediaDTOs;
    }

    private long getUserIdByRingId(long ringId) {
        long userId = 0;
        long mod = (long) Math.pow(10, 8);
        String ringIdStringValue = "21" + (ringId % mod);
        try {
            userId = CassandraDAO.getInstance().getUserTableId(Long.valueOf(ringIdStringValue));
        } catch (Exception ex) {
            RingLogger.getConfigPortalLogger().error("Exception in [getUserIdByRingId] -> " + ex);
        }
        return userId;
    }
}
