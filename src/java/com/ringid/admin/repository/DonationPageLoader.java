/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.repository;

import com.ringid.admin.cassandra.CassandraDAO;
import com.ringid.admin.dto.DonationPageDTO;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.utils.Constants;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.logging.log4j.Logger;
import org.ringid.pages.PageDTO;

/**
 *
 * @author mamun
 */
public class DonationPageLoader {

    private final static long LOADING_INTERVAL = 60 * 60 * 1000;
    private long loadingTime = 0;
    private HashMap<Long, PageDTO> donationPageMapList;
    private static DonationPageLoader donationPageLoader = null;
    static Logger logger = RingLogger.getConfigPortalLogger();

    public DonationPageLoader() {

    }

    public static DonationPageLoader getInstance() {
        if (donationPageLoader == null) {
            createLoader();
        }
        return donationPageLoader;
    }

    private synchronized static void createLoader() {
        if (donationPageLoader == null) {
            donationPageLoader = new DonationPageLoader();
        }
    }

    private void forceLoadData() {
        donationPageMapList = new HashMap<>();
        try {
            List<Long> pageIds = CassandraDAO.getInstance().getDonationPages();
            for (Long id : pageIds) {
                PageDTO pageDTO = CassandraDAO.getInstance().getPageInfo(id);
                donationPageMapList.put(id, pageDTO);
            }
        } catch (Exception e) {
            logger.debug("DonationPageLoader List Exception...", e);
        }
    }

    private void checkForReload() {
        long currentTime = System.currentTimeMillis();
        if (currentTime - loadingTime > LOADING_INTERVAL) {
            loadingTime = currentTime;
            forceLoadData();
        }
    }

    public synchronized void forceReload() {
        loadingTime = System.currentTimeMillis();
        forceLoadData();
    }

    private boolean contains(PageDTO dto, String searchText) {
        return (dto.getPageName() != null && dto.getPageName().toLowerCase().contains(searchText))
                || (String.valueOf(dto.getPageRingId()).contains(searchText))
                || (String.valueOf(dto.getPageId()).contains(searchText));
    }

    private ArrayList<PageDTO> getDonationPageData(DonationPageDTO sdto) {
        checkForReload();
        ArrayList<PageDTO> data = new ArrayList<>();
        Set set = donationPageMapList.entrySet();
        Iterator i = set.iterator();
        while (i.hasNext()) {
            Map.Entry me = (Map.Entry) i.next();
            PageDTO dto = (PageDTO) me.getValue();
            if (sdto.getSearchText() != null && sdto.getSearchText().length() > 0 && !contains(dto, sdto.getSearchText())) {
                continue;
            }
            data.add(dto);
        }
        if (sdto.getSortType() == Constants.DESC_SORT) {
            Collections.sort(data, new DonationPageDTO.CompDSC());
        } else {
            Collections.sort(data, new DonationPageDTO.CompASC());
        }
        return data;
    }

    public synchronized ArrayList<PageDTO> getDonationPageList(DonationPageDTO sdto) {
        ArrayList<PageDTO> dPageList = getDonationPageData(sdto);
        logger.debug("donationPageList Size: " + dPageList.size());
        return dPageList;
    }

    public PageDTO getDonationPageById(long pageId) {
        if (donationPageMapList.containsKey(pageId)) {
            return donationPageMapList.get(pageId);
        }
        return null;
    }
}
