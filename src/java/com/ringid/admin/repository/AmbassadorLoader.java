/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.repository;

import com.ringid.admin.cassandra.CassandraDAO;
import com.ringid.admin.utils.log.RingLogger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.ringid.pages.PageDTO;

/**
 *
 * @author mamun
 */
public class AmbassadorLoader {

    private final static long LOADING_INTERVAL = 24 * 60 * 60 * 1000;
    private long LOADING_TIME = 0;
    private Map<Long, List<PageDTO>> productPageOfAmbassadorMap;

    private AmbassadorLoader() {
        productPageOfAmbassadorMap = new HashMap<>();
    }

    private static class AmbassadorLoaderHolder {

        private static final AmbassadorLoader INSTANCE = new AmbassadorLoader();
    }

    public static AmbassadorLoader getInstance() {
        return AmbassadorLoaderHolder.INSTANCE;
    }

    private void forceLoadData() {
        productPageOfAmbassadorMap.clear();
    }

    private void checkForReload() {
        long currentTime = System.currentTimeMillis();
        if (currentTime - LOADING_TIME > LOADING_INTERVAL) {
            LOADING_TIME = currentTime;
            forceLoadData();
        }
    }

    public synchronized void forceReload(long productPageId) {
        productPageOfAmbassadorMap.remove(productPageId);
    }

    public synchronized void forceReload() {
        LOADING_TIME = System.currentTimeMillis();
        forceLoadData();
    }

    public List<PageDTO> getAmbassadorsOfProductPage(long productPageId) {
        checkForReload();
        List<PageDTO> ambasadorList = new ArrayList<>();
        if (productPageId > 0) {
            try {
                if (productPageOfAmbassadorMap.containsKey(productPageId)) {
                    ambasadorList = productPageOfAmbassadorMap.get(productPageId);
                } else {
                    List<Long> ambasadorPageIds = CassandraDAO.getInstance().getAmbassadorsOfProductPage(productPageId);
                    if (ambasadorPageIds != null && ambasadorPageIds.size() > 0) {
                        ambasadorList = CassandraDAO.getInstance().getPageDetailsForAdmin(ambasadorPageIds, false);
                    }
                    if (ambasadorList != null) {
                        productPageOfAmbassadorMap.put(productPageId, ambasadorList);
                    }
                }
            } catch (Exception ex) {
                RingLogger.getConfigPortalLogger().error("[AmbassadorLoader] Exception in getAmbassadorsOfProductPage ", ex);
            }
        } else {
            for (Map.Entry<Long, List<PageDTO>> entry : productPageOfAmbassadorMap.entrySet()) {
                ambasadorList.addAll(entry.getValue());
            }
        }
        if (ambasadorList != null) {
            RingLogger.getConfigPortalLogger().debug("[AmbassadorLoader] getAmbassadorsOfProductPage ambasadorList.size : " + ambasadorList.size());
        } else {
            RingLogger.getConfigPortalLogger().debug("[AmbassadorLoader] getAmbassadorsOfProductPage  ambasadorList null ");
        }
        return ambasadorList;
    }
}
