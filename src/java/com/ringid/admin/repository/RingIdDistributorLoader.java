/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.repository;

import com.ringid.admin.BaseDAO;
import com.ringid.admin.actions.servers.authServers.AuthServerLoader;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.dto.RingIdDistributorDTO;
import com.ringid.admin.utils.Constants;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Rabby
 */
public class RingIdDistributorLoader extends BaseDAO {

    private static RingIdDistributorLoader instance;
    private final static long LOADING_INTERVAL = 60 * 60 * 1000;
    private long loadingTime = 0;
    static Logger logger = RingLogger.getConfigPortalLogger();
    private List<RingIdDistributorDTO> ringIdDistributorList;

    private RingIdDistributorLoader() {
        ringIdDistributorList = new ArrayList<>();
    }

    private static class RingIdDistributorLoaderHolder {

        private static final RingIdDistributorLoader INSTANCE = new RingIdDistributorLoader();
    }

    public static RingIdDistributorLoader getInstance() {
        return RingIdDistributorLoaderHolder.INSTANCE;
    }

    private void loadData() {
        ringIdDistributorList = new ArrayList<>();
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            stmt = db.connection.createStatement();
            String sql = "SELECT serverID, startRingId, endRingId, status FROM ringiddistributor";
            rs = stmt.executeQuery(sql);
            while (rs.next()) {
                RingIdDistributorDTO dto = new RingIdDistributorDTO();
                dto.setServerId(rs.getInt("serverID"));
                dto.setStartRingId(rs.getLong("startRingId"));
                dto.setEndRingId(rs.getLong("endRingID"));
                dto.setStatus(rs.getInt("status"));
                dto.setServerIP(AuthServerLoader.getInstance().getAuthServerDTOByServerID(dto.getServerId()).getServerIP());

                ringIdDistributorList.add(dto);
            }
        } catch (Exception e) {
            logger.error("Exception in RingIdDistributorLoader --> " + e);
        } finally {
            close();
        }
        logger.info("[RingIdDistributorLoader] ringIdDistributorList.size() --> " + ringIdDistributorList.size());
    }

    public void forceReload() {
        loadingTime = System.currentTimeMillis();
        loadData();
    }

    private void checkForReload() {
        long currentTime = System.currentTimeMillis();
        if (currentTime - loadingTime > LOADING_INTERVAL) {
            loadingTime = currentTime;
            loadData();
        }
    }

    private boolean containsAny(RingIdDistributorDTO dto, String queryString) {
        String[] status = {"active", "inactive"};
        return (dto.getServerIP() != null && dto.getServerIP().contains(queryString))
                || (String.valueOf(dto.getStartRingId()).contains(queryString))
                || (String.valueOf(dto.getEndRingId()).contains(queryString))
                || (dto.getStatus() == 1 && status[0].contains(queryString))
                || (dto.getStatus() == 0 && status[1].contains(queryString));
    }

    public List<RingIdDistributorDTO> getRingIdDistributorList(RingIdDistributorDTO sdto) {
        checkForReload();
        List<RingIdDistributorDTO> list = new ArrayList<>();
        for (RingIdDistributorDTO dto : ringIdDistributorList) {
            if (sdto.getSearchText() != null && sdto.getSearchText().length() > 0 && !containsAny(dto, sdto.getSearchText())) {
                continue;
            }
            list.add(dto);
        }
        if (sdto.getSortType() == Constants.DESC_SORT) {
            Collections.sort(list, new RingIdDistributorDTO.CompDSC());
        } else {
            Collections.sort(list, new RingIdDistributorDTO.CompASC());
        }
        return list;
    }

    public RingIdDistributorDTO getRingIdDistributorDTO(RingIdDistributorDTO sdto) {
        checkForReload();
        RingIdDistributorDTO ringIdDistributorDTO = new RingIdDistributorDTO();
        for (RingIdDistributorDTO dto : ringIdDistributorList) {
            if (dto.getServerId() == sdto.getServerId() && dto.getStartRingId() == sdto.getStartRingId() && dto.getEndRingId() == sdto.getEndRingId()) {
                ringIdDistributorDTO = dto;
                break;
            }
        }
        return ringIdDistributorDTO;
    }
}
