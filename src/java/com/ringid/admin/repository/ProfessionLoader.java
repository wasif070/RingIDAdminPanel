/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.repository;

import com.ringid.admin.cassandra.CassandraDAO;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.dto.ProfessionDTO;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.UUID;

/**
 *
 * @author mamun
 */
public class ProfessionLoader {

    private final static long LOADING_INTERVAL = 24 * 60 * 60 * 1000;
    private long loadingTime = 0;
    private List<ProfessionDTO> professionDTOs;

    private ProfessionLoader() {
        professionDTOs = new ArrayList<>();
    }

    private static class ProfessionLoaderHolder {

        private static final ProfessionLoader INSTANCE = new ProfessionLoader();
    }

    public static ProfessionLoader getInstance() {
        return ProfessionLoaderHolder.INSTANCE;
    }

    private void checkForReload() {
        long currentTime = System.currentTimeMillis();
        if (currentTime - loadingTime > LOADING_INTERVAL) {
            loadingTime = currentTime;
            forceLoadData();
        }
    }

    public synchronized void forceReload() {
        loadingTime = System.currentTimeMillis();
        forceLoadData();
    }

    private void forceLoadData() {
        professionDTOs.clear();
        try {
            LinkedHashMap<UUID, String> professionLinkedHashMap = new LinkedHashMap<>();
            int start = 0, limit = 1000;
            do {
                professionLinkedHashMap.putAll(CassandraDAO.getInstance().getProfessionList(start, limit));
                start = start + limit;
            } while (professionLinkedHashMap.size() == limit);
            professionLinkedHashMap.entrySet().stream().map((entry) -> {
                ProfessionDTO dto = new ProfessionDTO();
                dto.setProfessionId(entry.getKey().toString());
                dto.setProfession(entry.getValue());
                return dto;
            }).forEach((dto) -> {
                professionDTOs.add(dto);
            });
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().debug("ProfessionLoader List Exception...", e);
        }
    }

    private List<ProfessionDTO> getProfessionData(ProfessionDTO sdto) {
        checkForReload();
        List<ProfessionDTO> data = new ArrayList<>();
        professionDTOs.stream().filter((dto) -> !(sdto.getSearchText() != null && sdto.getSearchText().length() > 0 && dto.getProfession().toLowerCase().contains(sdto.getSearchText()))).forEach((dto) -> {
            data.add(dto);
        });
        return data;
    }

    public List<ProfessionDTO> getProfessionList(ProfessionDTO sdto) {
        List<ProfessionDTO> professionDTOList = getProfessionData(sdto);
        RingLogger.getConfigPortalLogger().debug("getProfessionList List Size: " + professionDTOList.size());
        return professionDTOList;
    }
}
