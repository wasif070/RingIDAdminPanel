/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.repository;

import com.google.gson.Gson;
import com.ringid.admin.cassandra.CassandraDAO;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.dto.newsportal.NewsPortalDTO;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.Logger;
import org.ringid.pages.PageDTO;
import org.ringid.pages.PageSearchDTO;

/**
 *
 * @author mamun
 */
public class NewsPortalLoader {

    private final static long LOADING_INTERVAL = 60 * 60 * 1000;
    private long loadingTime = 0;
    private static NewsPortalLoader instance;
    private Map<Integer, List<NewsPortalDTO>> newsPortalMap;
    private final int LIMIT_ONE_THOUSAND = 1000;

    private static final Logger logger = RingLogger.getConfigPortalLogger();

    private NewsPortalLoader() {
        newsPortalMap = new HashMap<>();
    }

    public static NewsPortalLoader getInstance() {
        if (instance == null) {
            instance = new NewsPortalLoader();
        }
        return instance;
    }

    private void loadData(int pageType) {
        List<NewsPortalDTO> newsPortalDTOs = new ArrayList<>();
        List<PageSearchDTO> pageSearchDTOs = new ArrayList<>();
        List<PageSearchDTO> searchResult;
        List<PageDTO> pageDTOs = new ArrayList<>();
        List<Long> discoverablepageIds = new ArrayList<>();
        List<Long> notDiscoverablepageIds = new ArrayList<>();
        int start = 0;
        try {
            if (!newsPortalMap.containsKey(pageType)) {
                do {
                    searchResult = CassandraDAO.getInstance().searchPage(null, pageType, null, start, LIMIT_ONE_THOUSAND);
                    if (searchResult.size() > 0) {
                        pageSearchDTOs.addAll(searchResult);
                    }
                    start = LIMIT_ONE_THOUSAND;
                } while (searchResult.size() == LIMIT_ONE_THOUSAND);

                for (PageSearchDTO dto : pageSearchDTOs) {
                    if (dto.isIsDiscoverable()) {
                        discoverablepageIds.add(dto.getPageId());
                    } else {
                        notDiscoverablepageIds.add(dto.getPageId());
                    }
                }
                pageDTOs = CassandraDAO.getInstance().getPageDetailsForAdmin(discoverablepageIds, false);
                for (PageDTO pageDTO : pageDTOs) {
                    NewsPortalDTO dto = new NewsPortalDTO();
                    dto.setPageId(pageDTO.getPageId());
                    dto.setPageName(pageDTO.getPageName());
                    dto.setCountryName(pageDTO.getCountry());
                    dto.setPageCatName(pageDTO.getPageCategoryName());
                    dto.setPageRingId(pageDTO.getPageRingId());
                    dto.setPageActiveStatus(pageDTO.getPageActiveStatus());
                    dto.setPageOwnerId(pageDTO.getPageOwner());
                    if (pageDTO.isIsAutoFollowDisabled() != null) {
                        dto.setIsAutoFollowDisabled(pageDTO.isIsAutoFollowDisabled());
                    } else {
                        dto.setIsAutoFollowDisabled(true);
                    }
                    dto.setWeight(pageDTO.getWeight());
                    dto.setDiscoverable(1);

                    newsPortalDTOs.add(dto);
                }

                pageDTOs.clear();
                pageDTOs = CassandraDAO.getInstance().getPageDetailsForAdmin(notDiscoverablepageIds, false);
                for (PageDTO pageDTO : pageDTOs) {
                    NewsPortalDTO dto = new NewsPortalDTO();
                    dto.setPageId(pageDTO.getPageId());
                    dto.setPageName(pageDTO.getPageName());
                    dto.setCountryName(pageDTO.getCountry());
                    dto.setPageCatName(pageDTO.getPageCategoryName());
                    dto.setPageRingId(pageDTO.getPageRingId());
                    dto.setPageActiveStatus(pageDTO.getPageActiveStatus());
                    dto.setPageOwnerId(pageDTO.getPageOwner());
                    if (pageDTO.isIsAutoFollowDisabled() != null) {
                        dto.setIsAutoFollowDisabled(pageDTO.isIsAutoFollowDisabled());
                    } else {
                        dto.setIsAutoFollowDisabled(true);
                    }
                    dto.setWeight(pageDTO.getWeight());
                    dto.setDiscoverable(2);

                    newsPortalDTOs.add(dto);
                }
                logger.debug("NewsPortalLoader pageType --> " + pageType + " newsPortalDTOList size --> " + newsPortalDTOs.size());
                newsPortalMap.put(pageType, newsPortalDTOs);
            }
        } catch (Exception ex) {
            logger.debug("Exception in [NewsPortalLoader] loadData --> ", ex);
        }
    }

    public void forceReload(int pageType) {
        newsPortalMap.remove(pageType);
    }

    public void forceReload() {
        loadingTime = System.currentTimeMillis();
        newsPortalMap.clear();
    }

    private void checkForReload() {
        long currentTime = System.currentTimeMillis();
        if (currentTime - loadingTime > LOADING_INTERVAL) {
            forceReload();
        }
    }

    public List<NewsPortalDTO> getNewsPortalDTOList(NewsPortalDTO sdto) {
        checkForReload();
        List<NewsPortalDTO> newsPortalDTOs = new ArrayList<>();
        try {
            if (!newsPortalMap.containsKey(sdto.getPageType())) {
                loadData(sdto.getPageType());
            }
            List<NewsPortalDTO> temp = newsPortalMap.get(sdto.getPageType());
            for (NewsPortalDTO newsPortalDTO : temp) {
                if (sdto.getDiscoverable() != 0 && newsPortalDTO.getDiscoverable() != sdto.getDiscoverable()) {
                    continue;
                }
                if (sdto.getSearchText() != null && sdto.getSearchText().length() > 0) {
                    if (!(newsPortalDTO.getPageName().toLowerCase().contains(sdto.getSearchText().trim().toLowerCase())
                            || String.valueOf(newsPortalDTO.getPageId()).toLowerCase().contains(sdto.getSearchText().trim().toLowerCase())
                            || newsPortalDTO.getCountryName().toLowerCase().contains(sdto.getSearchText().trim().toLowerCase()))) {
                        continue;
                    }
                }
                newsPortalDTOs.add(newsPortalDTO);
            }
        } catch (Exception e) {
            logger.debug("Exception in [NewsPortalLoader] getNewsPortalDTOList --> " + e);
        }
        logger.debug("[getNewsPortalDTOList] pageType --> " + sdto.getPageType() + " newsPortalDTOs size --> " + newsPortalDTOs.size());
        return newsPortalDTOs;
    }
}
