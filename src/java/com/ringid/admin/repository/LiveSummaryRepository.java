/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.repository;

import com.ringid.admin.dao.LiveStreamingDAO;
import com.ringid.admin.dto.live.LiveInfoDTO;
import com.ringid.admin.dto.live.LiveSummaryDTO;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.service.LiveStreamingService;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author mamun
 */
public class LiveSummaryRepository {

    private Map<Long, List<LiveSummaryDTO>> liveSummaryMap = new HashMap<>();
    private final long ONE_DAY = 86_400_000;
    private SimpleDateFormat sdf;

    private LiveSummaryRepository() {
        sdf = new SimpleDateFormat("yyyy-MM-dd");
    }

    private static class LiveSummaryLoaderHolder {

        private static final LiveSummaryRepository INSTANCE = new LiveSummaryRepository();
    }

    public static LiveSummaryRepository getInstance() {
        return LiveSummaryLoaderHolder.INSTANCE;
    }

    private void loadLiveSummary(long fromDate, long toDate) {
        List<LiveSummaryDTO> liveSummaryDTOs = LiveStreamingDAO.getInstance().getLiveSummaryList(fromDate, toDate);
        for (LiveSummaryDTO dto : liveSummaryDTOs) {
            if (liveSummaryMap.containsKey(dto.getStreamDate())) {
                liveSummaryMap.get(dto.getStreamDate()).add(dto);
                continue;
            }
            List<LiveSummaryDTO> list = new ArrayList<>();
            list.add(dto);
            liveSummaryMap.put(dto.getStreamDate(), list);
        }
        RingLogger.getConfigPortalLogger().debug("[loadLiveSummary] liveSummaryMap --> " + liveSummaryMap.size());
    }

    public List<LiveSummaryDTO> getLiveSummaryList(LiveInfoDTO liveInfoDTO) {
        List<LiveSummaryDTO> liveSummaryDTOs = new ArrayList<>();
        List<LiveSummaryDTO> liveSummaryList;
        List<String> ringIds = liveInfoDTO.getRingId() != null && liveInfoDTO.getRingId().length() > 0 ? new ArrayList<>(Arrays.asList(liveInfoDTO.getRingId().split(","))) : null;
        long fromDate = liveInfoDTO.getStartTime();
        long toDate = liveInfoDTO.getEndTime();
        long today = 0L;

        try {
            today = sdf.parse(sdf.format(new Date())).getTime();
        } catch (Exception ex) {
        }

        for (long date = fromDate; date <= toDate; date += ONE_DAY) {
            if (today == date) {
                liveSummaryList = getTodayLiveHistory(liveInfoDTO, date);
                for (LiveSummaryDTO dto : liveSummaryList) {
                    if (ringIds != null && ringIds.size() > 0 && !ringIds.contains(dto.getRingId())) {
                        continue;
                    }
                    if (liveInfoDTO.getCountryId() > 0 && liveInfoDTO.getCountryId() != dto.getCountryId()) {
                        continue;
                    }
                    String hms = String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(dto.getTotalLiveDuration()),
                            TimeUnit.MILLISECONDS.toMinutes(dto.getTotalLiveDuration()) % TimeUnit.HOURS.toMinutes(1),
                            TimeUnit.MILLISECONDS.toSeconds(dto.getTotalLiveDuration()) % TimeUnit.MINUTES.toSeconds(1));

                    dto.setTotalLiveDurationStr(hms);
                    LiveSummaryDTO clone;
                    clone = getLiveSummaryDTO(dto);
                    liveSummaryDTOs.add(clone);
                }
                continue;
            }
            if (!liveSummaryMap.containsKey(date)) {
                LiveStreamingDAO.getInstance().addLiveSummary(date, date);
                loadLiveSummary(date, date);
            }
            if (liveSummaryMap.containsKey(date)) {
                liveSummaryList = liveSummaryMap.get(date);
                for (LiveSummaryDTO dto : liveSummaryList) {
                    if (ringIds != null && ringIds.size() > 0 && !ringIds.contains(dto.getRingId())) {
                        continue;
                    }
                    if (liveInfoDTO.getCountryId() > 0 && liveInfoDTO.getCountryId() != dto.getCountryId()) {
                        continue;
                    }
                    String hms = String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(dto.getTotalLiveDuration()),
                            TimeUnit.MILLISECONDS.toMinutes(dto.getTotalLiveDuration()) % TimeUnit.HOURS.toMinutes(1),
                            TimeUnit.MILLISECONDS.toSeconds(dto.getTotalLiveDuration()) % TimeUnit.MINUTES.toSeconds(1));

                    dto.setTotalLiveDurationStr(hms);
                    LiveSummaryDTO clone;
                    clone = getLiveSummaryDTO(dto);
                    liveSummaryDTOs.add(clone);
                }
            }
        }

        RingLogger.getConfigPortalLogger().info("[getLiveSummaryList] liveSummaryDTOs :: " + liveSummaryDTOs != null ? liveSummaryDTOs.size() : "NULL");
        return liveSummaryDTOs;
    }

    private LiveSummaryDTO getLiveSummaryDTO(LiveSummaryDTO sdto) {
        LiveSummaryDTO dto = new LiveSummaryDTO();
        dto.setId(sdto.getId());
        dto.setRingId(sdto.getRingId());
        dto.setName(sdto.getName());
        dto.setTotalLiveDuration(sdto.getTotalLiveDuration());
        dto.setTotalLiveDurationStr(sdto.getTotalLiveDurationStr());
        dto.setNoOfAppearance(sdto.getNoOfAppearance());
        dto.setDevice(sdto.getDevice());
        dto.setCountry(sdto.getCountry());
        dto.setCountryId(sdto.getCountryId());
        dto.setStreamDate(sdto.getStreamDate());
        dto.setStreamDateStr(sdto.getStreamDateStr());

        return dto;

    }

    private List<LiveSummaryDTO> getTodayLiveHistory(LiveInfoDTO liveInfoDTO, long date) {
        List<LiveSummaryDTO> liveSummaryDTOs = new ArrayList<>();
        List<LiveInfoDTO> liveInfoDTOs;
        LiveInfoDTO newLiveInfoDTO = liveInfoDTO;
        newLiveInfoDTO.setStartDate(sdf.format(new Date(date)));
        newLiveInfoDTO.setEndDate(sdf.format(new Date(date)));
        liveInfoDTOs = LiveStreamingService.getInstance().getLiveInfoList(newLiveInfoDTO, null);
        for (LiveInfoDTO dto : liveInfoDTOs) {
            LiveSummaryDTO sdto = new LiveSummaryDTO();
            sdto.setStreamId(dto.getStreamId());
            sdto.setRingId(dto.getRingId());
            sdto.setRoomId(dto.getRoomId());
            sdto.setDevice(dto.getDevice());
            try {
                sdto.setStreamDate(sdf.parse(dto.getStreamDate()).getTime());
            } catch (Exception ex) {
            }
            sdto.setStreamDateStr(dto.getStreamDate());
            sdto.setNoOfAppearance(dto.getNoOfAppearance());
            sdto.setTotalLiveDuration(dto.getDuration());
            long millis = dto.getDuration();
            String hms = String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(millis),
                    TimeUnit.MILLISECONDS.toMinutes(millis) % TimeUnit.HOURS.toMinutes(1),
                    TimeUnit.MILLISECONDS.toSeconds(millis) % TimeUnit.MINUTES.toSeconds(1));
            sdto.setTotalLiveDurationStr(hms);
            sdto.setCountry(dto.getCountry());
            sdto.setCountryId(dto.getCountryId());
            sdto.setName(dto.getFirstName());

            liveSummaryDTOs.add(sdto);
        }
        return liveSummaryDTOs;
    }
}
