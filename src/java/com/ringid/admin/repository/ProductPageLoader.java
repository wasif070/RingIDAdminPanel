/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.repository;

import com.ringid.admin.cassandra.CassandraDAO;
import com.ringid.admin.utils.log.RingLogger;
import java.util.ArrayList;
import java.util.List;
import org.ringid.pages.PageDTO;

/**
 *
 * @author mamun
 */
public class ProductPageLoader {

    private final static long LOADING_INTERVAL = 24 * 60 * 60 * 1000;
    private long LOADING_TIME = 0;
    private List<PageDTO> productPageDTOs;
    private List<Long> productPageIds;

    private ProductPageLoader() {
        productPageDTOs = new ArrayList<>();
        productPageIds = new ArrayList<>();
    }

    private static class ProductPageLoaderHolder {

        private static final ProductPageLoader INSTANCE = new ProductPageLoader();
    }

    public static ProductPageLoader getInstance() {
        return ProductPageLoaderHolder.INSTANCE;
    }

    private void forceLoadData() {
        productPageDTOs.clear();
        productPageIds.clear();
        try {
            long pivotPageId = 0L;
            List<Long> tempProductPage = new ArrayList<>();
            int limit = 100;
            do {
                tempProductPage = CassandraDAO.getInstance().getAllProductPageIds(pivotPageId, limit);
                if (tempProductPage.size() > 0) {
                    productPageIds.addAll(tempProductPage);
                    pivotPageId = tempProductPage.get(tempProductPage.size() - 1);
                }
            } while (tempProductPage.size() > 0 && tempProductPage.size() == limit);

            productPageDTOs = CassandraDAO.getInstance().getPageDetailsForAdmin(productPageIds, false);

        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().debug("ProductPageLoader Exception...", e);
        }
    }

    private void checkForReload() {
        long currentTime = System.currentTimeMillis();
        if (currentTime - LOADING_TIME > LOADING_INTERVAL) {
            LOADING_TIME = currentTime;
            forceLoadData();
        }
    }

    public synchronized void forceReload() {
        LOADING_TIME = System.currentTimeMillis();
        forceLoadData();
    }

    public List<Long> getAllProductPageIds() {
        checkForReload();
        return productPageIds;
    }

    public List<PageDTO> getAllProductPageDTOs() {
        checkForReload();
        return productPageDTOs;
    }
}
