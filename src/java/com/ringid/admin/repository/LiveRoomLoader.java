/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.repository;

import com.ringid.admin.cassandra.CassandraDAO;
import com.ringid.admin.dto.live.LiveRoomDTO;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.utils.Constants;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.Logger;
import org.ringid.livestream.StreamRoomDTO;

/**
 *
 * @author Rabby
 */
public class LiveRoomLoader {

    private final static long LOADING_INTERVAL = 60 * 60 * 1000;
    private long loadingTime = 0;
    private HashMap<Long, StreamRoomDTO> liveRoomListMap;
    static Logger logger = RingLogger.getConfigPortalLogger();

    private LiveRoomLoader() {

    }

    private static class LiveRoomLoaderHolder {

        private static final LiveRoomLoader INSTANCE = new LiveRoomLoader();
    }

    public static LiveRoomLoader getInstance() {
        return LiveRoomLoaderHolder.INSTANCE;
    }

    private HashMap<Long, StreamRoomDTO> loadLiveRoomList() {
        HashMap<Long, StreamRoomDTO> listMap = new HashMap<>();
        try {
            List<StreamRoomDTO> liveRoomList = CassandraDAO.getInstance().getLiveRoomDetails();
            for (StreamRoomDTO dto : liveRoomList) {
                listMap.put(dto.getRoomId(), dto);
            }
        } catch (Exception e) {
            logger.error(e);
        }
        return listMap;
    }

    private void forceLoadData() {
        liveRoomListMap = loadLiveRoomList();
    }

    private void checkForReload() {
        forceLoadData();
    }

    public synchronized void forceReload() {
        loadingTime = System.currentTimeMillis();
        forceLoadData();
    }

    private boolean contains(StreamRoomDTO dto, String searchText) {
        return (String.valueOf(dto.getRoomId()).contains(searchText))
                || (dto.getRoomName() != null && dto.getRoomName().toLowerCase().contains(searchText))
                || (dto.getDescription() != null && dto.getDescription().toLowerCase().contains(searchText))
                || (String.valueOf(dto.getWeight()).contains(searchText));
    }

    public ArrayList<StreamRoomDTO> getLiveRoomList(LiveRoomDTO sdto) {
        checkForReload();
        ArrayList<StreamRoomDTO> list = new ArrayList<>();
        for (Map.Entry<Long, StreamRoomDTO> entry : liveRoomListMap.entrySet()) {
            StreamRoomDTO dto = entry.getValue();
            if (sdto.getSearchText() != null && sdto.getSearchText().length() > 0 && !contains(dto, sdto.getSearchText())) {
                continue;
            }
            list.add(entry.getValue());
        }
        if (sdto.getSortType() == Constants.DESC_SORT) {
            Collections.sort(list, new LiveRoomDTO.CompDSC());
        } else {
            Collections.sort(list, new LiveRoomDTO.CompASC());
        }
        return list;
    }

    public StreamRoomDTO getLiveRoomById(long id) {
        if (liveRoomListMap.containsKey(id)) {
            return liveRoomListMap.get(id);
        }
        return null;
    }

    public List<StreamRoomDTO> getCheckRoomList(String[] roomIds) {
        checkForReload();

        List<StreamRoomDTO> checkStreamRoomDTOs = new ArrayList<>();
        for (String roomId : roomIds) {
            StreamRoomDTO checkStreamRoomDTO = liveRoomListMap.get(Long.parseLong(roomId));
            checkStreamRoomDTOs.add(checkStreamRoomDTO);
        }
        logger.debug("CheckRoom List Size: " + checkStreamRoomDTOs.size());
        return checkStreamRoomDTOs;
    }
}
