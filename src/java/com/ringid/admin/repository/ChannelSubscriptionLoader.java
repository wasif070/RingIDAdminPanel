/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.repository;

import com.ringid.admin.cassandra.CassandraDAO;
import com.ringid.admin.utils.log.RingLogger;
import java.util.ArrayList;
import java.util.List;
import org.ringid.channel.ChannelSubscriptionDTO;
import org.ringid.utilities.ListReturnDTO;

/**
 *
 * @author mamun
 */
public class ChannelSubscriptionLoader {

    private final static long LOADING_INTERVAL = 60 * 60 * 1000;
    private long loadingTime = 0;
    private static ChannelSubscriptionLoader instance = null;
    private List<ChannelSubscriptionDTO> channelSubscriptionDTOs;

    private ChannelSubscriptionLoader() {
        channelSubscriptionDTOs = new ArrayList<>();
    }

    public static ChannelSubscriptionLoader getInstance() {
        if (instance == null) {
            createLoader();
        }
        return instance;
    }

    private synchronized static void createLoader() {
        if (instance == null) {
            instance = new ChannelSubscriptionLoader();
        }
    }

    private void forceLoadData() {
        try {
            ListReturnDTO<ChannelSubscriptionDTO> listReturnDTO = CassandraDAO.getInstance().getChannelSubscriptionTypes();
            if (listReturnDTO != null && listReturnDTO.getList() != null) {
                channelSubscriptionDTOs = listReturnDTO.getList();
            }
        } catch (Exception ex) {
            RingLogger.getConfigPortalLogger().error("Exception in [ChannelSubscriptionLoader] " + ex);
        }
    }

    private void checkForReload() {
        long currentTime = System.currentTimeMillis();
        if (currentTime - loadingTime > LOADING_INTERVAL) {
            loadingTime = currentTime;
            forceLoadData();
        }
    }

    public synchronized void forceReload() {
        loadingTime = System.currentTimeMillis();
        forceLoadData();
    }

    public synchronized List<ChannelSubscriptionDTO> getChannelSubscriptionList() {
        checkForReload();
        RingLogger.getConfigPortalLogger().debug("ChannelList Size: " + channelSubscriptionDTOs.size());
        return channelSubscriptionDTOs;
    }
}
