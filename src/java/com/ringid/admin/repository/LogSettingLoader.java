/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.repository;

import static com.ipvision.ringid.storage.utils.ScheduledSettingsLoader.close;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.dao.LogSettingDAO;
import com.ringid.admin.dto.LogSettingDTO;
import com.ringid.admin.utils.Constants;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author mamun
 */
public class LogSettingLoader {

    private final static long LOADING_INTERVAL = 24 * 60 * 60 * 1000;
    private long loadingTime = 0;
    static Logger logger = RingLogger.getConfigPortalLogger();
    private Map<Integer, LogSettingDTO> logSettingHashMapList;

    private LogSettingLoader() {
        logSettingHashMapList = new HashMap<>();
    }

    private static class LogSettingLoaderHolder {

        private static final LogSettingLoader INSTANCE = new LogSettingLoader();
    }

    public static LogSettingLoader getInstance() {
        return LogSettingLoaderHolder.INSTANCE;
    }

    private void checkForReload() {
        long currentTime = System.currentTimeMillis();
        if (currentTime - loadingTime > LOADING_INTERVAL) {
            loadingTime = currentTime;
            loadData();
        }
    }

    public synchronized void forceReload() {
        loadingTime = System.currentTimeMillis();
        loadData();
    }

    private void loadData() {
        logSettingHashMapList = new HashMap<>();
        try {
            logSettingHashMapList = LogSettingDAO.getInstance().getLogSettingHashMap();
        } catch (Exception e) {
            logger.debug("loadData[LogSettingLoader] Exception...", e);
        } finally {
            close();
        }
    }

    private List<LogSettingDTO> getLogSettingData(LogSettingDTO sdto) {
        checkForReload();
        List<LogSettingDTO> data = new ArrayList<>();
        Set set = logSettingHashMapList.entrySet();
        Iterator i = set.iterator();
        while (i.hasNext()) {
            Map.Entry me = (Map.Entry) i.next();
            LogSettingDTO dto = (LogSettingDTO) me.getValue();
            if (sdto.getSearchText() != null && sdto.getSearchText().length() > 0 && !dto.getName().toLowerCase().contains(sdto.getSearchText())) {
                continue;
            }
            data.add(dto);
        }
        return data;
    }

    public List<LogSettingDTO> getLogSettingList(LogSettingDTO sdto) {
        List<LogSettingDTO> logSettingList = getLogSettingData(sdto);
        if (sdto.getSortType() == Constants.DESC_SORT) {
            Collections.sort(logSettingList, new LogSettingDTO.CompDSC());
        } else {
            Collections.sort(logSettingList, new LogSettingDTO.CompASC());
        }
        logger.debug("getLogSettingList Size: " + logSettingList.size());
        return logSettingList;
    }

    public LogSettingDTO getLogSettingDTO(int id) {
        checkForReload();
        if (logSettingHashMapList.containsKey(id)) {
            return logSettingHashMapList.get(id);
        }
        return null;
    }

    public List<LogSettingDTO> getLogSettingDTOList(int[] ids) {
        checkForReload();
        List<LogSettingDTO> list = new ArrayList<>();
        for (int id : ids) {
            if (logSettingHashMapList.containsKey(id)) {
                list.add(logSettingHashMapList.get(id));
            }
        }
        return list;
    }
}
