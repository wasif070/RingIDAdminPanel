/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.repository;

import com.ringid.admin.dao.SettingDAO;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.dto.adminAuth.LoginDTO;
import com.ringid.admin.dto.SettingDTO;
import com.ringid.admin.utils.Constants;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author mamun
 */
public class SettingLoader {

    private final static long LOADING_INTERVAL = 24 * 60 * 60 * 1000;
    private long loadingTime = 0;
    private Map<Long, SettingDTO> settingHashMapList;
    private Map<Long, Set<Integer>> settingPermissionMap;

    private SettingLoader() {
        settingHashMapList = new HashMap<>();
        settingPermissionMap = new HashMap<>();
    }

    private static class SettingLoaderHolder {

        private static final SettingLoader INSTANCE = new SettingLoader();
    }

    public static SettingLoader getInstance() {
        return SettingLoaderHolder.INSTANCE;
    }

    private void forceLoadData() {
        settingHashMapList.clear();
        settingPermissionMap.clear();
        try {
            settingHashMapList = SettingDAO.getInstance().getSettingList();
            settingPermissionMap = SettingDAO.getInstance().getSettingPermissionMap();
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().debug("[SettingLoader] forceLoadData Exception...", e);
        }
    }

    private void checkForReload() {
        long currentTime = System.currentTimeMillis();
        if (currentTime - loadingTime > LOADING_INTERVAL) {
            loadingTime = currentTime;
            forceLoadData();
        }
    }

    public void forceReload() {
        loadingTime = System.currentTimeMillis();
        forceLoadData();
    }

    private boolean containsAny(SettingDTO dto, String queryString) {
        return (dto.getName() != null && dto.getName().toLowerCase().contains(queryString))
                || (dto.getSettingsValue() != null && dto.getSettingsValue().toLowerCase().contains(queryString));
    }

    private List<SettingDTO> getSettingData(SettingDTO sdto, LoginDTO loginDTO) {
        checkForReload();
        ArrayList<SettingDTO> data = new ArrayList<>();
        Set set = settingHashMapList.entrySet();
        Iterator i = set.iterator();
        settingHashMapList.entrySet().stream()
                .map(entry -> (SettingDTO) entry.getValue())
                .filter(dto -> {
                    if (loginDTO.getPermissionLevel() == Constants.SUPER_ADMIN) {
                        return true;
                    }
                    if (settingPermissionMap.containsKey(dto.getId())) {
                        if (settingPermissionMap.get(dto.getId()).contains(loginDTO.getPermissionLevel())) {
                            return true;
                        }
                    }
                    return false;
                })
                .filter(dto -> !((sdto.getSearchText() != null && sdto.getSearchText().length() > 0) && !containsAny(dto, sdto.getSearchText())))
                .forEach(dto -> {
                    data.add(dto);
                });
        return data;
    }

    public List<SettingDTO> getSettingList(SettingDTO sdto, LoginDTO loginDTO) {
        List<SettingDTO> settingList = getSettingData(sdto, loginDTO);
        RingLogger.getConfigPortalLogger().debug("getSettingList Size: " + settingList.size());
        return settingList;
    }

    public List<SettingDTO> getSettingDTOList(String[] selectedIDs) {
        List<SettingDTO> dtoLists = new ArrayList<>();
        long id = 0;
        for (String str : selectedIDs) {
            id = Long.valueOf(str);
            if (settingHashMapList.containsKey(id)) {
                dtoLists.add(settingHashMapList.get(id));
            }
        }
        return dtoLists;
    }

    public SettingDTO getSettingDTO(long id, LoginDTO loginDTO) {
        SettingDTO dto = null;
        if (settingHashMapList.containsKey(id)) {
            if (loginDTO.getPermissionLevel() == Constants.SUPER_ADMIN) {
                return settingHashMapList.get(id);
            }
            if (settingPermissionMap.containsKey(id)) {
                if (settingPermissionMap.get(id).contains(loginDTO.getPermissionLevel())) {
                    return settingHashMapList.get(id);
                }
            }
        }
        return dto;
    }
}
