/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.repository;

import com.google.gson.Gson;
import com.ringid.admin.dto.BreakingNewsDTO;
import com.ringid.admin.cassandra.CassandraDAO;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.utils.Constants;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import org.ringid.newsportals.NPBasicFeedInfoDTO;

/**
 *
 * @author Kazi Nasir Uddin Oly
 * @Date Jan 1, 2017
 */
public class BreakingNewsRepository {

    private final static long LOADING_INTERVAL = 15 * 60 * 100;
    private long loadingTime = 0;

    private List<NPBasicFeedInfoDTO> breakingNewsFromPages;
    private List<NPBasicFeedInfoDTO> commonBreakingNews;

    private BreakingNewsRepository() {
        breakingNewsFromPages = new ArrayList<>();
        commonBreakingNews = new ArrayList<>();
    }

    private static class BreakingNewsLoaderHolder {

        private static final BreakingNewsRepository INSTANCE = new BreakingNewsRepository();
    }

    public static BreakingNewsRepository getInstance() {
        return BreakingNewsLoaderHolder.INSTANCE;
    }

    private void checkForReload() {
        long currentTime = System.currentTimeMillis();
        if (currentTime - loadingTime > LOADING_INTERVAL) {
            loadingTime = currentTime;
            forceLoadData();
        }
    }

    private void loadBreakingNewsFromPages() {
        try {
            List<Long> pageIds = CassandraDAO.getInstance().getPageIds();
            breakingNewsFromPages = CassandraDAO.getInstance().getBreakingNewsListFromPages(pageIds);
        } catch (Exception ex) {
            RingLogger.getConfigPortalLogger().error("Load BreakingNews from pages Exception " + ex);
        }
    }

    private void loadGlobalBreakingNews() {
        try {
            commonBreakingNews = CassandraDAO.getInstance().getCommonBreakingNewsList();
        } catch (Exception ex) {
            RingLogger.getConfigPortalLogger().error("Load Global BreakingNews Exception " + ex);
        }
    }

    public void forceLoadData() {
        loadBreakingNewsFromPages();
        loadGlobalBreakingNews();

    }

    private boolean contains(NPBasicFeedInfoDTO dto, String searchText) {
        return (dto.getPageTitle() != null && dto.getPageTitle().toLowerCase().contains(searchText))
                || (dto.getNewsTitle() != null && dto.getNewsTitle().toLowerCase().contains(searchText))
                || (dto.getId() != null && dto.getId().toString().contains(searchText));
    }

    public List<NPBasicFeedInfoDTO> getBreakingNewsListFromPages(BreakingNewsDTO sdto) {
        checkForReload();
        List<NPBasicFeedInfoDTO> list = new ArrayList<>();
        breakingNewsFromPages
                .stream()
                .filter((dto) -> !(sdto.getSearchText() != null && sdto.getSearchText().length() > 0 && !contains(dto, sdto.getSearchText())))
                .forEach((dto) -> {
                    list.add(dto);
                });
        if (sdto.getSortType() == Constants.DESC_SORT) {
            Collections.sort(list, new BreakingNewsDTO.CompDSC());
        } else {
            Collections.sort(list, new BreakingNewsDTO.CompASC());
        }
        return list;
    }

    public List<NPBasicFeedInfoDTO> getCommonBreakingNewsList(BreakingNewsDTO sdto) {
        checkForReload();
        List<NPBasicFeedInfoDTO> list = new ArrayList<>();
        commonBreakingNews
                .stream()
                .filter((dto) -> !(sdto.getSearchText() != null && sdto.getSearchText().length() > 0 && !contains(dto, sdto.getSearchText())))
                .forEach((dto) -> {
                    list.add(dto);
                });
        if (sdto.getSortType() == Constants.DESC_SORT) {
            Collections.sort(list, new BreakingNewsDTO.CompDSC());
        } else {
            Collections.sort(list, new BreakingNewsDTO.CompASC());
        }
        return list;
    }

    public int makeCommonBreaking(Map<UUID, Integer> feedIdDuration) {
        int ret = 0;
        try {
            ret = CassandraDAO.getInstance().makeCommonBreaking(feedIdDuration);
            RingLogger.getConfigPortalLogger().debug("Make Common Breaking ReasonCode: " + ret);
        } catch (Exception ex) {
            RingLogger.getConfigPortalLogger().error("Exception [makeCommonBreaking] --> " + ex);
        }
        forceLoadData();
        return ret;
    }

    public int removeFromCommonBreaking(List<UUID> feedId) {
        int ret = 0;
        try {
            RingLogger.getConfigPortalLogger().debug("Remove from Common Breaking feedIds --> " + new Gson().toJson(feedId));
            ret = CassandraDAO.getInstance().removeFromCommonBreaking(feedId);
            RingLogger.getConfigPortalLogger().debug("Remove from Common Breaking ReasonCode: " + ret);
        } catch (Exception ex) {
            RingLogger.getConfigPortalLogger().error("Exception [removeFromCommonBreaking] --> " + ex);
        }
        forceLoadData();
        return ret;
    }
}
