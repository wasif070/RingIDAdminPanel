/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.repository;

import com.google.gson.Gson;
import com.ringid.admin.BaseDAO;
import com.ringid.admin.dao.AppConstantsDAO;
import com.ringid.admin.dto.AppConstantsDTO;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.utils.Constants;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;
import org.ringid.utilities.AppConstants;

/**
 *
 * @author Kazi Nasir Uddin Oly
 * @Date Jun 14, 2016
 */
public class AppConstantsLoader extends BaseDAO {

    private final static long LOADING_INTERVAL = 24 * 60 * 60 * 1000;
    private long loadingTime = 0;
    private HashMap<Integer, AppConstantsDTO> appConstantsHashMapList;

    private AppConstantsLoader() {

    }

    private static class AppConstantsLoaderHolder {

        private static final AppConstantsLoader INSTANCE = new AppConstantsLoader();
    }

    public static AppConstantsLoader getInstance() {
        return AppConstantsLoaderHolder.INSTANCE;
    }

    private void forceLoadData() {
        appConstantsHashMapList = new HashMap<>();
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            stmt = db.connection.createStatement();
            String sql = "SELECT id, platform, app_type, version,"
                    + "auth_controller, image_server, image_resource, display_digits, official_user_id,"
                    + "market_server, market_resource, vod_server, vod_resource, web, payment_gateway, constants_version FROM appconstants;";
            rs = stmt.executeQuery(sql);
            while (rs.next()) {
                AppConstantsDTO dto = new AppConstantsDTO();
                dto.setId(rs.getInt("id"));
                dto.setPlatform(rs.getInt("platform"));
                dto.setAppType(rs.getInt("app_type"));
                dto.setVersion(rs.getInt("version"));
                dto.setAuthController(rs.getString("auth_controller"));
                dto.setImageServer(rs.getString("image_server"));
                dto.setImageResource(rs.getString("image_resource"));
                dto.setDisplayDigits(rs.getInt("display_digits"));
                dto.setOfficialUserId(rs.getInt("official_user_id"));
                dto.setMarketServer(rs.getString("market_server"));
                dto.setMarketResource(rs.getString("market_resource"));
                dto.setVodServer(rs.getString("vod_server"));
                dto.setVodResource(rs.getString("vod_resource"));
                dto.setWeb(rs.getString("web"));
                dto.setPaymentGateway(rs.getString("payment_gateway"));
                dto.setConstantsVersion(rs.getInt("constants_version"));
                if (dto.getPlatform() < 6) {
                    dto.setPlatformStr(AppConstants.DEVICES[dto.getPlatform()]);
                } else {
                    dto.setPlatformStr("" + dto.getPlatform());
                }

                switch (dto.getAppType()) {
                    case AppConstants.APP_TYPES.FULL:
                        dto.setAppTypeStr("FULL");
                        break;
                    case AppConstants.APP_TYPES.LITE:
                        dto.setAppTypeStr("LITE");
                        break;
                    case AppConstants.APP_TYPES.LIVE:
                        dto.setAppTypeStr("LIVE");
                        break;
                    case AppConstants.APP_TYPES.NETCAFE:
                        dto.setAppTypeStr("NETCAFE");
                        break;
                    case AppConstants.APP_TYPES.MESSENGER:
                        dto.setAppTypeStr("MESSENGER");
                        break;
                    case AppConstants.APP_TYPES.RING_MARKET:
                        dto.setAppTypeStr("RING_MARKET");
                        break;
                    default:
                        dto.setAppTypeStr("" + dto.getAppType());
                        break;
                }
                appConstantsHashMapList.put(dto.getId(), dto);
            }
            RingLogger.getConfigPortalLogger().debug(sql + "::" + appConstantsHashMapList.size());

        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().debug("AppConstants List Exception...", e);
        } finally {
            close();
        }
    }

    private void checkForReload() {
        long currentTime = System.currentTimeMillis();
        if (currentTime - loadingTime > LOADING_INTERVAL) {
            loadingTime = currentTime;
            forceLoadData();
        }
    }

    public synchronized void forceReload() {
        loadingTime = System.currentTimeMillis();
        forceLoadData();
    }

    private boolean containsAny(String query, AppConstantsDTO dto) {
        return (dto.getPlatform() > 0 && dto.getPlatform() < Constants.DEVICE.length && Constants.DEVICE[dto.getPlatform()].toLowerCase().contains(query))
                || (dto.getAppType() > 0 && String.valueOf(dto.getAppType()).contains(query))
                || (dto.getVersion() > 0 && String.valueOf(dto.getVersion()).contains(query))
                || (dto.getAuthController() != null && dto.getAuthController().toLowerCase().contains(query))
                || (dto.getImageServer() != null && dto.getImageServer().toLowerCase().contains(query))
                || (dto.getImageResource() != null && dto.getImageResource().toLowerCase().contains(query))
                || (dto.getOfficialUserId() > 0 && String.valueOf(dto.getOfficialUserId()).contains(query))
                || (dto.getMarketServer() != null && dto.getMarketServer().toLowerCase().contains(query))
                || (dto.getMarketResource() != null && dto.getMarketResource().toLowerCase().contains(query))
                || (dto.getVodServer() != null && dto.getVodServer().toLowerCase().contains(query))
                || (dto.getVodResource() != null && dto.getVodResource().toLowerCase().contains(query))
                || (dto.getWeb() != null && dto.getWeb().toLowerCase().contains(query));
    }

    private ArrayList<AppConstantsDTO> getAppConstantsData(AppConstantsDTO sdto) {
        checkForReload();
        ArrayList<AppConstantsDTO> data = new ArrayList<>();
        Set set = appConstantsHashMapList.entrySet();
        Iterator i = set.iterator();
        while (i.hasNext()) {
            Map.Entry me = (Map.Entry) i.next();
            AppConstantsDTO dto = (AppConstantsDTO) me.getValue();
            if (sdto.getSearchText() != null && sdto.getSearchText().length() > 0 && !containsAny(sdto.getSearchText(), dto)) {
                continue;
            }
            data.add(dto);
        }
        return data;
    }

    private AppConstantsDTO getMaxVersion(int platform, int appType, ArrayList<AppConstantsDTO> temp) {
        AppConstantsDTO resultDTO = new AppConstantsDTO();
        for (AppConstantsDTO dto : temp) {
            if (platform == dto.getPlatform() && appType == dto.getAppType() && dto.getVersion() < 1057 && dto.getVersion() > resultDTO.getVersion()) {
                resultDTO = dto;
            }
        }
        return resultDTO;
    }

    private ArrayList<AppConstantsDTO> getMaxAppConstantsData() {
        checkForReload();
        ArrayList<AppConstantsDTO> temp = new ArrayList<>();
        ArrayList<AppConstantsDTO> data = new ArrayList<>();
        Set<Integer> platform = new HashSet<>();
        Set<Integer> appType = new HashSet<>();
        Set set = appConstantsHashMapList.entrySet();
        Iterator i = set.iterator();
        while (i.hasNext()) {
            Map.Entry me = (Map.Entry) i.next();
            AppConstantsDTO dto = (AppConstantsDTO) me.getValue();
            temp.add(dto);
            platform.add(dto.getPlatform());
            appType.add(dto.getAppType());
        }
        for (int pltf : platform) {
            for (int apt : appType) {
                if (pltf != 100 && apt != 100) {
                    AppConstantsDTO dto = getMaxVersion(pltf, apt, temp);
                    if (dto != null && dto.getVersion() > 0) {
                        data.add(dto);
                    }
                }
            }
        }
        return data;
    }

    public ArrayList<AppConstantsDTO> getAppConstantsList(AppConstantsDTO sdto) {
        ArrayList<AppConstantsDTO> appConstantsList = getAppConstantsData(sdto);
        if (sdto.getSortType() == Constants.DESC_SORT) {
            Collections.sort(appConstantsList, new AppConstantsDTO.CompDSC());
        } else {
            Collections.sort(appConstantsList, new AppConstantsDTO.CompASC());
        }
        RingLogger.getConfigPortalLogger().debug("AppConstants List Size: " + appConstantsList.size());
        return appConstantsList;
    }

    public ArrayList<AppConstantsDTO> getMaxAppConstantsListInfo() {
        ArrayList<AppConstantsDTO> appConstantsList = getMaxAppConstantsData();
        Collections.sort(appConstantsList, new AppConstantsDTO.CompASC());
        RingLogger.getConfigPortalLogger().debug("AppConstants List Size: " + appConstantsList.size());
        return appConstantsList;
    }

    public ArrayList<AppConstantsDTO> getAppConstantsListForDownload() {
        checkForReload();
        ArrayList<AppConstantsDTO> data = new ArrayList<>(appConstantsHashMapList.values());
        return data;
    }

    public AppConstantsDTO getAppConstantsDTO(int id) {
        checkForReload();
        if (appConstantsHashMapList.containsKey(id)) {
            return appConstantsHashMapList.get(id);
        }
        return null;
    }
}
