/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.tomcat;

import com.ringid.admin.cassandra.CassandraDAO;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.utils.Constants;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Enumeration;
import java.util.Set;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author reefat
 */
@WebListener
public class ServerActivityListener implements ServletContextListener {

    private static Logger logger = RingLogger.getConfigPortalLogger();

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        logger.debug("================== Initialized =======================");
        try {
            if (!Constants.SERVER.equals("local")) {
                CassandraDAO.getInstance();
            }
        } catch (Exception e) {
            System.err.println("Exception in [StorageConnection.getInstance()] --> " + e);
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        try {
            logger.debug("########### close [Instance] #################");
            CassandraDAO.getInstance().destroy();

            cleanMySQLAndThreads();
            System.out.println("----- DONE -----------");
        } catch (Exception e) {
            System.err.println("Exception [StorageConnection.getInstance().close()] --> " + e);
        }
    }

    private void cleanMySQLAndThreads() {
        Enumeration<Driver> drivers = DriverManager.getDrivers();
        Driver d = null;
        while (drivers.hasMoreElements()) {
            try {
                d = drivers.nextElement();
                DriverManager.deregisterDriver(d);
            } catch (SQLException ex) {
                System.err.println("Exception - deregisterDriver - [cleanMySQLAndThreads ] " + ex);
            }
        }
        System.out.println(" --- deregisterDriver done ---");
//        Set<Thread> threadSet = Thread.getAllStackTraces().keySet();
//        Thread[] threadArray = threadSet.toArray(new Thread[threadSet.size()]);
//        System.out.println(" --- threadArray size : " + threadArray.length + " ---");
//        for (Thread t : threadArray) {
//            if (t.getName().contains("Abandoned connection cleanup thread")) {
//                synchronized (t) {
//                    t.stop();
//                }
//            }
//        }
//        System.out.println(" --- Stopping thread done ---");
    }
}
