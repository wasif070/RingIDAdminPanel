/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.dto;

import java.util.Comparator;

/**
 *
 * @author Rabby
 */
public class PermittedIpsDTO {

    private int id;
    private String ip;
    private int status;
    private long updateTime;
    private boolean isSearchByIp = false;
    private String queryString;

    public String getQueryString() {
        return queryString;
    }

    public void setQueryString(String queryString) {
        this.queryString = queryString;
    }

    public boolean isIsSearchByIp() {
        return isSearchByIp;
    }

    public void setIsSearchByIp(boolean isSearchByIp) {
        this.isSearchByIp = isSearchByIp;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public long getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(long updateTime) {
        this.updateTime = updateTime;
    }

    public static class CompIdDSC implements Comparator<PermittedIpsDTO> {

        @Override
        public int compare(PermittedIpsDTO o1, PermittedIpsDTO o2) {
            return o2.id - o1.id;
        }
    }

    public static class CompIdASC implements Comparator<PermittedIpsDTO> {

        @Override
        public int compare(PermittedIpsDTO o1, PermittedIpsDTO o2) {
            return o1.id - o2.id;
        }
    }

    public static class CompServerIpDSC implements Comparator<PermittedIpsDTO> {

        @Override
        public int compare(PermittedIpsDTO o1, PermittedIpsDTO o2) {
            return o2.ip.compareTo(o1.ip);
        }
    }

    public static class CompServerIpASC implements Comparator<PermittedIpsDTO> {

        @Override
        public int compare(PermittedIpsDTO o1, PermittedIpsDTO o2) {
            return o1.ip.compareTo(o2.ip);
        }
    }

}
