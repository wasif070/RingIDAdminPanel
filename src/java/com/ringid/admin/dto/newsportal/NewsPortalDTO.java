/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.dto.newsportal;

import com.ringid.admin.BaseDTO;
import com.ringid.admin.utils.Constants;
import java.util.Comparator;

/**
 *
 * @author Kazi Nasir Uddin Oly
 * @Date Oct 2, 2016
 */
public class NewsPortalDTO extends BaseDTO {

    private long pageId;
    private String pageName;
    private int discoverable;
    private int trending;
    private int pageType;
    private String countryName;
    private String pageCatName;
    private long pageRingId;
    private int pageActiveStatus;
    private long pageOwnerId;
    private boolean isAutoFollowDisabled;
    private int weight;
    private String profileImageUrl;
    private String coverImageUrl;
    private String slogan;

    public long getPageId() {
        return pageId;
    }

    public void setPageId(long pageId) {
        this.pageId = pageId;
    }

    public String getPageName() {
        return pageName;
    }

    public void setPageName(String pageName) {
        this.pageName = pageName;
    }

    public int getPageType() {
        return pageType;
    }

    public void setPageType(int pageType) {
        this.pageType = pageType;
    }

    public String getProfileImageUrl() {
        return profileImageUrl;
    }

    public void setProfileImageUrl(String profileImageUrl) {
        this.profileImageUrl = profileImageUrl;
    }

    public String getCoverImageUrl() {
        return coverImageUrl;
    }

    public void setCoverImageUrl(String coverImageUrl) {
        this.coverImageUrl = coverImageUrl;
    }

    public String getSlogan() {
        return slogan;
    }

    public void setSlogan(String slogan) {
        this.slogan = slogan;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public String getPageCatName() {
        return pageCatName;
    }

    public void setPageCatName(String pageCatName) {
        this.pageCatName = pageCatName;
    }

    public long getPageRingId() {
        return pageRingId;
    }

    public void setPageRingId(long pageRingId) {
        this.pageRingId = pageRingId;
    }

    public int getPageActiveStatus() {
        return pageActiveStatus;
    }

    public void setPageActiveStatus(int pageActiveStatus) {
        this.pageActiveStatus = pageActiveStatus;
    }

    public long getPageOwnerId() {
        return pageOwnerId;
    }

    public void setPageOwnerId(long pageOwnerId) {
        this.pageOwnerId = pageOwnerId;
    }

    public boolean isIsAutoFollowDisabled() {
        return isAutoFollowDisabled;
    }

    public void setIsAutoFollowDisabled(boolean isAutoFollowDisabled) {
        this.isAutoFollowDisabled = isAutoFollowDisabled;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public int getTrending() {
        return trending;
    }

    public void setTrending(int trending) {
        this.trending = trending;
    }

    public int getDiscoverable() {
        return discoverable;
    }

    public void setDiscoverable(int discoverable) {
        this.discoverable = discoverable;
    }

    public static class CompASC implements Comparator<NewsPortalDTO> {

        @Override
        public int compare(NewsPortalDTO o1, NewsPortalDTO o2) {
            switch (column) {
                case Constants.COLUMN_ONE:
                    return o1.getWeight() - o2.getWeight();
                case Constants.COLUMN_TWO:
                    return Long.compare(o1.getPageId(), o2.getPageId());
                default:
                    return o1.getWeight() - o2.getWeight();
            }
        }
    }

    public static class CompDSC implements Comparator<NewsPortalDTO> {

        @Override
        public int compare(NewsPortalDTO o1, NewsPortalDTO o2) {
            switch (column) {
                case Constants.COLUMN_ONE:
                    return o2.getWeight() - o1.getWeight();
                case Constants.COLUMN_TWO:
                    return Long.compare(o2.getPageId(), o1.getPageId());
                default:
                    return o2.getWeight() - o1.getWeight();
            }
        }
    }
}
