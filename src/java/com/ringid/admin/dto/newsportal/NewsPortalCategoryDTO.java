/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.dto.newsportal;

import com.ringid.admin.BaseDTO;
import com.ringid.admin.utils.Constants;
import java.util.Comparator;
import java.util.UUID;

/**
 *
 * @author Kazi Nasir Uddin Oly
 * @Date Jun 20, 2016
 */
public class NewsPortalCategoryDTO extends BaseDTO {

    private UUID id;
    private String name;
    private int status;
    private int type;
    private boolean isSearchByName = false;
    private boolean isSearchByType = false;
    private UUID pageId;

    public UUID getPageId() {
        return pageId;
    }

    public void setPageId(UUID pageId) {
        this.pageId = pageId;
    }

    public boolean isIsSearchByType() {
        return isSearchByType;
    }

    public void setIsSearchByType(boolean isSearchByType) {
        this.isSearchByType = isSearchByType;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public boolean isIsSearchByName() {
        return isSearchByName;
    }

    public void setIsSearchByName(boolean isSearchByName) {
        this.isSearchByName = isSearchByName;
    }

    public static class CompASC implements Comparator<NewsPortalCategoryDTO> {

        @Override
        public int compare(NewsPortalCategoryDTO o1, NewsPortalCategoryDTO o2) {
            switch (column) {
                case Constants.COLUMN_ONE:
                    return o1.getName().compareTo(o2.getName());
                case Constants.COLUMN_TWO:
                    return o1.getType() - o2.getType();
                default:
                    return o1.getName().compareTo(o2.getName());
            }
        }
    }

    public static class CompDSC implements Comparator<NewsPortalCategoryDTO> {

        @Override
        public int compare(NewsPortalCategoryDTO o1, NewsPortalCategoryDTO o2) {
            switch (column) {
                case Constants.COLUMN_ONE:
                    return o2.getName().compareTo(o1.getName());
                case Constants.COLUMN_TWO:
                    return o2.getType() - o1.getType();
                default:
                    return o2.getName().compareTo(o1.getName());
            }
        }
    }
}
