/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.dto;

import com.ringid.admin.BaseDTO;
import com.ringid.admin.utils.Constants;
import java.util.Comparator;

/**
 *
 * @author Rabby
 */
public class LogSettingDTO extends BaseDTO {

    private int id;
    private String name;
    private int value;

    private boolean isSearchByName;

    public boolean isIsSearchByName() {
        return isSearchByName;
    }

    public void setIsSearchByName(boolean isSearchByName) {
        this.isSearchByName = isSearchByName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String string) {
        name = string;
    }

    public static class CompASC implements Comparator<LogSettingDTO> {

        @Override
        public int compare(LogSettingDTO arg0, LogSettingDTO arg1) {
            switch (column) {
                case Constants.COLUMN_ONE:
                    return arg0.getName().toLowerCase().compareTo(arg1.getName().toLowerCase());
                default:
                    return arg0.getName().toLowerCase().compareTo(arg1.getName().toLowerCase());
            }
        }
    }

    public static class CompDSC implements Comparator<LogSettingDTO> {

        @Override
        public int compare(LogSettingDTO arg0, LogSettingDTO arg1) {
            switch (column) {
                case Constants.COLUMN_ONE:
                    return arg1.getName().toLowerCase().compareTo(arg0.getName().toLowerCase());
                default:
                    return arg1.getName().toLowerCase().compareTo(arg0.getName().toLowerCase());
            }
        }
    }

    @Override
    public String toString() {
        return "LogSettingDTO{" + "id=" + id + ", name=" + name + ", value=" + value + ", isSearchByName=" + isSearchByName + '}';
    }

}
