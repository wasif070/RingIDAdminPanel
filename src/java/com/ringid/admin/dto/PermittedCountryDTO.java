/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.dto;

/**
 *
 * @author mamun
 */
public class PermittedCountryDTO {

    private String countryName;
    private String countryShortCode;
    private String currency;
    private String mobilePhoneDialingCode;
    private String supportedPlatform;
    private int signupCash;
    private int signupCoin;
    private int signupRingbit;
    private int referralCash;
    private int referralCoin;
    private int referralRingbit;
    private int cashoutlimit;
    private int status;

    public int getSignupCash() {
        return signupCash;
    }

    public void setSignupCash(int signupCash) {
        this.signupCash = signupCash;
    }

    public int getSignupCoin() {
        return signupCoin;
    }

    public void setSignupCoin(int signupCoin) {
        this.signupCoin = signupCoin;
    }

    public int getSignupRingbit() {
        return signupRingbit;
    }

    public void setSignupRingbit(int signupRingbit) {
        this.signupRingbit = signupRingbit;
    }

    public int getReferralCash() {
        return referralCash;
    }

    public void setReferralCash(int referralCash) {
        this.referralCash = referralCash;
    }

    public int getReferralCoin() {
        return referralCoin;
    }

    public void setReferralCoin(int referralCoin) {
        this.referralCoin = referralCoin;
    }

    public int getReferralRingbit() {
        return referralRingbit;
    }

    public void setReferralRingbit(int referralRingbit) {
        this.referralRingbit = referralRingbit;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getCountryShortCode() {
        return countryShortCode;
    }

    public void setCountryShortCode(String countryShortCode) {
        this.countryShortCode = countryShortCode;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getMobilePhoneDialingCode() {
        return mobilePhoneDialingCode;
    }

    public void setMobilePhoneDialingCode(String mobilePhoneDialingCode) {
        this.mobilePhoneDialingCode = mobilePhoneDialingCode;
    }

    public String getSupportedPlatform() {
        return supportedPlatform;
    }

    public void setSupportedPlatform(String supportedPlatform) {
        this.supportedPlatform = supportedPlatform;
    }

    public int getCashoutlimit() {
        return cashoutlimit;
    }

    public void setCashoutlimit(int cashoutlimit) {
        this.cashoutlimit = cashoutlimit;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
