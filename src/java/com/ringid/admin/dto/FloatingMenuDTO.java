/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.dto;

import com.ringid.admin.BaseDTO;
import com.ringid.admin.utils.Constants;
import java.util.Comparator;

/**
 *
 * @author mamun
 */
public class FloatingMenuDTO extends BaseDTO {

    private int tab;
    private int type;
    private String name;
    private int weight;
    private double slideTime;
    private long redirectId;
    private int btnType;
    private String btnBgColor;
    private String btnTxtColor;

    public int getTab() {
        return tab;
    }

    public void setTab(int tab) {
        this.tab = tab;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public double getSlideTime() {
        return slideTime;
    }

    public void setSlideTime(double slideTime) {
        this.slideTime = slideTime;
    }

    public long getRedirectId() {
        return redirectId;
    }

    public void setRedirectId(long redirectId) {
        this.redirectId = redirectId;
    }

    public int getBtnType() {
        return btnType;
    }

    public void setBtnType(int btnType) {
        this.btnType = btnType;
    }

    public String getBtnBgColor() {
        return btnBgColor;
    }

    public void setBtnBgColor(String btnBgColor) {
        this.btnBgColor = btnBgColor;
    }

    public String getBtnTxtColor() {
        return btnTxtColor;
    }

    public void setBtnTxtColor(String btnTxtColor) {
        this.btnTxtColor = btnTxtColor;
    }

    public static class CompASC implements Comparator<FloatingMenuDTO> {

        @Override
        public int compare(FloatingMenuDTO arg0, FloatingMenuDTO arg1) {
            int returnVal = 0;

            switch (column) {
                case Constants.COLUMN_ONE:
                    returnVal = arg0.getTab() - arg1.getTab();
                    break;
                case Constants.COLUMN_TWO:
                    returnVal = arg0.getType() - arg1.getType();
                    break;
                case Constants.COLUMN_THREE:
                    returnVal = arg0.getName().toLowerCase().compareTo(arg1.getName().toLowerCase());
                    break;
                case Constants.COLUMN_FOUR:
                    returnVal = arg0.getWeight() - arg1.getWeight();
                    break;
                default:
                    returnVal = arg0.getTab() - arg1.getTab();
                    break;
            }
            return returnVal;
        }
    }

    public static class CompDSC implements Comparator<FloatingMenuDTO> {

        @Override
        public int compare(FloatingMenuDTO arg0, FloatingMenuDTO arg1) {
            int returnVal = 0;

            switch (column) {
                case Constants.COLUMN_ONE:
                    returnVal = arg1.getTab() - arg0.getTab();
                    break;
                case Constants.COLUMN_TWO:
                    returnVal = arg1.getType() - arg0.getType();
                    break;
                case Constants.COLUMN_THREE:
                    returnVal = arg1.getName().toLowerCase().compareTo(arg0.getName().toLowerCase());
                    break;
                case Constants.COLUMN_FOUR:
                    returnVal = arg1.getWeight() - arg0.getWeight();
                    break;
                default:
                    returnVal = arg1.getTab() - arg0.getTab();
                    break;
            }
            return returnVal;
        }
    }
}
