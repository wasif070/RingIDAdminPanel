/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.dto;

import com.ringid.admin.BaseDTO;

/**
 *
 * @author Rabby
 */
public class RingBitDTO extends BaseDTO {

    private long userId;
    private String ringId;
    private String userName;
    private double coinAmount;
    private long addTime;
    private String addTimeStr;
    private int status;
    private String settingValueImage;
    private String settingValueText;
    private int dataType;
    private long ringBitImageId;
    private long ringBitTextId;

    public long getRingBitImageId() {
        return ringBitImageId;
    }

    public void setRingBitImageId(long ringBitImageId) {
        this.ringBitImageId = ringBitImageId;
    }

    public long getRingBitTextId() {
        return ringBitTextId;
    }

    public void setRingBitTextId(long ringBitTextId) {
        this.ringBitTextId = ringBitTextId;
    }

    public String getSettingValueImage() {
        return settingValueImage;
    }

    public void setSettingValueImage(String settingValueImage) {
        this.settingValueImage = settingValueImage;
    }

    public String getSettingValueText() {
        return settingValueText;
    }

    public void setSettingValueText(String settingValueText) {
        this.settingValueText = settingValueText;
    }

    public int getDataType() {
        return dataType;
    }

    public void setDataType(int dataType) {
        this.dataType = dataType;
    }

    public String getAddTimeStr() {
        return addTimeStr;
    }

    public void setAddTimeStr(String addTimeStr) {
        this.addTimeStr = addTimeStr;
    }

    public String getRingId() {
        return ringId;
    }

    public void setRingId(String ringId) {
        this.ringId = ringId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public double getCoinAmount() {
        return coinAmount;
    }

    public void setCoinAmount(double coinAmount) {
        this.coinAmount = coinAmount;
    }

    public long getAddTime() {
        return addTime;
    }

    public void setAddTime(long addTime) {
        this.addTime = addTime;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
