/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.dto;

import com.ringid.admin.BaseDTO;
import com.ringid.admin.utils.Constants;
import java.util.Comparator;
import java.util.UUID;
import org.ringid.newsportals.NPBasicFeedInfoDTO;

/**
 *
 * @author Kazi Nasir Uddin Oly
 * @Date Dec 8, 2016
 */
public class BreakingNewsDTO extends BaseDTO {

    private int id;
    private UUID feedId;
    private String pageName;
    private int pageType;
    private String newsTitle;
    private boolean breaking;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public UUID getFeedId() {
        return feedId;
    }

    public void setFeedId(UUID feedId) {
        this.feedId = feedId;
    }

    public String getPageName() {
        return pageName;
    }

    public void setPageName(String pageName) {
        this.pageName = pageName;
    }

    public int getPageType() {
        return pageType;
    }

    public void setPageType(int pageType) {
        this.pageType = pageType;
    }

    public String getNewsTitle() {
        return newsTitle;
    }

    public void setNewsTitle(String newsTitle) {
        this.newsTitle = newsTitle;
    }

    public boolean isBreaking() {
        return breaking;
    }

    public void setBreaking(boolean isBreaking) {
        this.breaking = isBreaking;
    }

    public static class CompASC implements Comparator<NPBasicFeedInfoDTO> {

        @Override
        public int compare(NPBasicFeedInfoDTO o1, NPBasicFeedInfoDTO o2) {
            switch (column) {
                case Constants.COLUMN_ONE:
                    return o1.getId().compareTo(o2.getId());
                case Constants.COLUMN_TWO:
                    return o1.getPageTitle().compareTo(o2.getPageTitle());
                default:
                    return o1.getId().compareTo(o2.getId());
            }
        }
    }

    public static class CompDSC implements Comparator<NPBasicFeedInfoDTO> {

        @Override
        public int compare(NPBasicFeedInfoDTO o1, NPBasicFeedInfoDTO o2) {
            switch (column) {
                case Constants.COLUMN_ONE:
                    return o2.getId().compareTo(o1.getId());
                case Constants.COLUMN_TWO:
                    return o2.getPageTitle().compareTo(o1.getPageTitle());
                default:
                    return o2.getId().compareTo(o1.getId());
            }
        }
    }
}
