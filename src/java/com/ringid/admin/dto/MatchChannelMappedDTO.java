/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.dto;

import java.util.List;
import org.ringid.channel.ChannelDTO;
import org.ringid.sports.cricket.MatchInfoDTO;

/**
 *
 * @author mamun
 */
public class MatchChannelMappedDTO {

    private MatchInfoDTO matchInfoDTO;
    private List<ChannelDTO> channelDTOList;

    public List<ChannelDTO> getChannelDTOList() {
        return channelDTOList;
    }

    public void setChannelDTOList(List<ChannelDTO> channelDTOList) {
        this.channelDTOList = channelDTOList;
    }

    public MatchInfoDTO getMatchInfoDTO() {
        return matchInfoDTO;
    }

    public void setMatchInfoDTO(MatchInfoDTO matchInfoDTO) {
        this.matchInfoDTO = matchInfoDTO;
    }

}
