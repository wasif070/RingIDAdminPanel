/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.dto;

import com.ringid.admin.BaseDTO;

/**
 *
 * @author ipvision
 */
public class SettingDTO extends BaseDTO {

    private long id;
    private String name;
    private String settingsValue;
    private Integer dataType = -1;
    private boolean isSearchByName;
    private boolean status;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public boolean isIsSearchByName() {
        return isSearchByName;
    }

    public void setIsSearchByName(boolean isSearchByName) {
        this.isSearchByName = isSearchByName;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSettingsValue() {
        return settingsValue;
    }

    public void setSettingsValue(String settingsValue) {
        this.settingsValue = settingsValue;
    }

    public Integer getDataType() {
        return dataType;
    }

    public void setDataType(Integer dataType) {
        this.dataType = dataType;
    }

    @Override
    public String toString() {
        return "SettingDTO{" + "id=" + id + ", name=" + name + ", settingsValue=" + settingsValue + ", dataType=" + dataType + ", isSearchByName=" + isSearchByName + '}';
    }

}
