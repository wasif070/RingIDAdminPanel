/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.dto;

import com.ringid.admin.BaseDTO;
import com.ringid.admin.utils.Constants;
import java.util.Comparator;

/**
 *
 * @author mamun
 */
public class PaymentMethodDTO extends BaseDTO {

    private long id;
    private String name;
    private boolean isOpen;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isIsOpen() {
        return isOpen;
    }

    public void setIsOpen(boolean isOpen) {
        this.isOpen = isOpen;
    }

    public static class CompASC implements Comparator<PaymentMethodDTO> {

        @Override
        public int compare(PaymentMethodDTO arg0, PaymentMethodDTO arg1) {
            int returnVal = 0;

            switch (column) {
                case Constants.COLUMN_ONE:
                    returnVal = Long.compare(arg0.getId(), arg1.getId());
                    break;
                default:
                    returnVal = Long.compare(arg0.getId(), arg1.getId());
                    break;
            }
            return returnVal;
        }
    }

    public static class CompDSC implements Comparator<PaymentMethodDTO> {

        @Override
        public int compare(PaymentMethodDTO arg0, PaymentMethodDTO arg1) {
            int returnVal = 0;

            switch (column) {
                case Constants.COLUMN_ONE:
                    returnVal = Long.compare(arg1.getId(), arg0.getId());
                    break;
                default:
                    returnVal = Long.compare(arg1.getId(), arg0.getId());
                    break;
            }
            return returnVal;
        }
    }

}
