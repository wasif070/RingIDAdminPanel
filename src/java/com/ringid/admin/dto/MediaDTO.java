/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.dto;

import com.ringid.admin.BaseDTO;
import com.ringid.admin.utils.Constants;
import java.util.Comparator;
import org.ringid.newsfeeds.CassMultiMediaDTO;

/**
 *
 * @author mamun
 */
public class MediaDTO extends BaseDTO {

    private int totalDataSize;
    private String mediaType;
    private Long ringId;

    public int getTotalDataSize() {
        return totalDataSize;
    }

    public void setTotalDataSize(int totalDataSize) {
        this.totalDataSize = totalDataSize;
    }

    public String getMediaType() {
        return mediaType;
    }

    public void setMediaType(String mediaType) {
        this.mediaType = mediaType;
    }

    public Long getRingId() {
        return ringId;
    }

    public void setRingId(Long ringId) {
        this.ringId = ringId;
    }

    public static class CompASC implements Comparator<CassMultiMediaDTO> {

        @Override
        public int compare(CassMultiMediaDTO o1, CassMultiMediaDTO o2) {
            switch (column) {
                case Constants.COLUMN_ONE:
                    return Long.compare(o1.getUserId(), o2.getUserId());
                case Constants.COLUMN_TWO:
                    return o1.getFullName().compareTo(o2.getFullName());
                default:
                    return Long.compare(o1.getUserId(), o2.getUserId());
            }
        }
    }

    public static class CompDSC implements Comparator<CassMultiMediaDTO> {

        @Override
        public int compare(CassMultiMediaDTO o1, CassMultiMediaDTO o2) {
            switch (column) {
                case Constants.COLUMN_ONE:
                    return Long.compare(o2.getUserId(), o1.getUserId());
                case Constants.COLUMN_TWO:
                    return o2.getFullName().compareTo(o1.getFullName());
                default:
                    return Long.compare(o2.getUserId(), o1.getUserId());
            }
        }
    }
}
