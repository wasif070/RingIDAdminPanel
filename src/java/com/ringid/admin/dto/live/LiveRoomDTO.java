/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.dto.live;

import com.ringid.admin.BaseDTO;
import com.ringid.admin.utils.Constants;
import java.util.Comparator;
import org.ringid.livestream.StreamRoomDTO;

/**
 *
 * @author user
 */
public class LiveRoomDTO extends BaseDTO {

    private long roomId;
    private String roomName;
    private String description;
    private String banner;
    private long liveUserCount;
    private Integer weight;

    public long getRoomId() {
        return roomId;
    }

    public void setRoomId(long roomId) {
        this.roomId = roomId;
    }

    public String getRoomName() {
        return roomName;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getBanner() {
        return banner;
    }

    public void setBanner(String banner) {
        this.banner = banner;
    }

    public long getLiveUserCount() {
        return liveUserCount;
    }

    public void setLiveUserCount(long liveUserCount) {
        this.liveUserCount = liveUserCount;
    }

    public Integer getWeight() {
        return weight;
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }

    public static class CompASC implements Comparator<StreamRoomDTO> {

        @Override
        public int compare(StreamRoomDTO arg0, StreamRoomDTO arg1) {
            switch (column) {
                case Constants.COLUMN_ONE:
                    return arg0.getWeight() - arg1.getWeight();
                case Constants.COLUMN_TWO:
                    return Long.compare(arg0.getRoomId(), arg1.getWeight());
                default:
                    return arg0.getWeight() - arg1.getWeight();
            }
        }
    }

    public static class CompDSC implements Comparator<StreamRoomDTO> {

        @Override
        public int compare(StreamRoomDTO arg0, StreamRoomDTO arg1) {
            switch (column) {
                case Constants.COLUMN_ONE:
                    return arg1.getWeight() - arg0.getWeight();
                case Constants.COLUMN_TWO:
                    return Long.compare(arg1.getWeight(), arg0.getWeight());
                default:
                    return arg1.getWeight() - arg0.getWeight();
            }
        }
    }
}
