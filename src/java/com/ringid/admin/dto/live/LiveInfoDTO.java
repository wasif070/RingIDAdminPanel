/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.dto.live;

/**
 *
 * @author ipvision
 */
public class LiveInfoDTO {

    private String streamId;
    private long userId;
    private String ringId;
    private long ownId;
    private long startTime;
    private long endTime;
    private long duration;
    private int roomId;
    private int device;
    private String streamDate;
    private int noOfAppearance;
    private String startDate;
    private String endDate;
    private String totalLiveDurationStr;
    private String fn;
    private String country;
    private int countryId;
    private long totalLiveDuration;

    public int getCountryId() {
        return countryId;
    }

    public void setCountryId(int countryId) {
        this.countryId = countryId;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getFirstName() {
        return fn;
    }

    public void setFirstName(String fn) {
        this.fn = fn;
    }

    public String getTotalLiveDurationStr() {
        return totalLiveDurationStr;
    }

    public void setTotalLiveDurationStr(String totalLiveDurationStr) {
        this.totalLiveDurationStr = totalLiveDurationStr;
    }

    public long getTotalLiveDuration() {
        return totalLiveDuration;
    }

    public void setTotalLiveDuration(long totalLiveDuration) {
        this.totalLiveDuration = totalLiveDuration;
    }

    public String getStreamId() {
        return streamId;
    }

    public void setStreamId(String streamId) {
        this.streamId = streamId;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getRingId() {
        return ringId;
    }

    public void setRingId(String ringId) {
        this.ringId = ringId;
    }

    public long getOwnId() {
        return ownId;
    }

    public void setOwnId(long ownId) {
        this.ownId = ownId;
    }

    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public long getEndTime() {
        return endTime;
    }

    public void setEndTime(long endTime) {
        this.endTime = endTime;
    }

    public long getDuration() {
        return duration;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }

    public int getRoomId() {
        return roomId;
    }

    public void setRoomId(int roomId) {
        this.roomId = roomId;
    }

    public int getDevice() {
        return device;
    }

    public void setDevice(int device) {
        this.device = device;
    }

    public String getStreamDate() {
        return streamDate;
    }

    public void setStreamDate(String streamDate) {
        this.streamDate = streamDate;
    }

    public int getNoOfAppearance() {
        return noOfAppearance;
    }

    public void setNoOfAppearance(int noOfAppearance) {
        this.noOfAppearance = noOfAppearance;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

}
