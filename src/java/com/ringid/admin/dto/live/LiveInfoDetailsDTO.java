/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.dto.live;

import java.util.Comparator;

/**
 *
 * @author ipvision
 */
//  `streamId` varchar(36) NOT NULL DEFAULT '',
//  `userId` bigint(20) DEFAULT NULL,
//  `ringId` varchar(20) DEFAULT NULL,
//  `ownId` bigint(20) DEFAULT NULL,
//  `startTime` bigint(20) DEFAULT NULL,
//  `endTime` bigint(20) DEFAULT NULL,
//  `duration` bigint(20) DEFAULT NULL,
//  `roomId` int(11) DEFAULT NULL,
//  `device` int(5) DEFAULT NULL,
//  `streamDate` datetime DEFAULT NULL,
public class LiveInfoDetailsDTO {

    private String streamId;
    private long userId;
    private String ringId;
    private long ownId;
    private long startTime;
    private long endTime;
    private long duration;
    private int roomId;
    private int device;
    private String streamDate;
    private String startDate;
    private String endDate;
    private String liveDuration;
    private String fn;

    public String getFirstName() {
        return fn;
    }

    public void setFirstName(String fn) {
        this.fn = fn;
    }

    public String getLiveDuration() {
        return liveDuration;
    }

    public void setLiveDuration(String liveDuration) {
        this.liveDuration = liveDuration;
    }

    public String getStreamId() {
        return streamId;
    }

    public void setStreamId(String streamId) {
        this.streamId = streamId;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getRingId() {
        return ringId;
    }

    public void setRingId(String ringId) {
        this.ringId = ringId;
    }

    public long getOwnId() {
        return ownId;
    }

    public void setOwnId(long ownId) {
        this.ownId = ownId;
    }

    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public long getEndTime() {
        return endTime;
    }

    public void setEndTime(long endTime) {
        this.endTime = endTime;
    }

    public long getDuration() {
        return duration;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }

    public int getRoomId() {
        return roomId;
    }

    public void setRoomId(int roomId) {
        this.roomId = roomId;
    }

    public int getDevice() {
        return device;
    }

    public void setDevice(int device) {
        this.device = device;
    }

    public String getStreamDate() {
        return streamDate;
    }

    public void setStreamDate(String streamDate) {
        this.streamDate = streamDate;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public static class CompStartDateDSC implements Comparator<LiveInfoDetailsDTO> {

        @Override
        public int compare(LiveInfoDetailsDTO arg0, LiveInfoDetailsDTO arg1) {
            return arg1.startDate.compareTo(arg0.startDate);
        }
    }

    public static class CompStartDateASC implements Comparator<LiveInfoDetailsDTO> {

        @Override
        public int compare(LiveInfoDetailsDTO arg0, LiveInfoDetailsDTO arg1) {
            return arg0.startDate.compareTo(arg1.startDate);
        }
    }

    public static class CompEndDateDSC implements Comparator<LiveInfoDetailsDTO> {

        @Override
        public int compare(LiveInfoDetailsDTO arg0, LiveInfoDetailsDTO arg1) {
            return arg1.endDate.compareTo(arg0.endDate);
        }
    }

    public static class CompEndDateASC implements Comparator<LiveInfoDetailsDTO> {

        @Override
        public int compare(LiveInfoDetailsDTO arg0, LiveInfoDetailsDTO arg1) {
            return arg0.endDate.compareTo(arg1.endDate);
        }
    }

    public static class CompDurationDSC implements Comparator<LiveInfoDetailsDTO> {

        @Override
        public int compare(LiveInfoDetailsDTO arg0, LiveInfoDetailsDTO arg1) {
            return Long.valueOf(arg1.duration).compareTo(arg0.duration);
        }
    }

    public static class CompDurationASC implements Comparator<LiveInfoDetailsDTO> {

        @Override
        public int compare(LiveInfoDetailsDTO arg0, LiveInfoDetailsDTO arg1) {
            return Long.valueOf(arg0.duration).compareTo(arg1.duration);
        }
    }

    public static class CompStreamDateDSC implements Comparator<LiveInfoDetailsDTO> {

        @Override
        public int compare(LiveInfoDetailsDTO arg0, LiveInfoDetailsDTO arg1) {
            return arg1.streamDate.compareTo(arg0.streamDate);
        }
    }

    public static class CompStreamDateASC implements Comparator<LiveInfoDetailsDTO> {

        @Override
        public int compare(LiveInfoDetailsDTO arg0, LiveInfoDetailsDTO arg1) {
            return arg0.streamDate.compareTo(arg1.streamDate);
        }
    }

}
