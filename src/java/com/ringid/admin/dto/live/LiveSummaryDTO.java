/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.dto.live;

import java.util.Comparator;

/**
 *
 * @author mamun
 */
public class LiveSummaryDTO {

    private long id;
    private String streamId;
    private long userId;
    private String ringId;
    private int roomId;
    private int device;
    private long streamDate;
    private int noOfAppearance;
    private long totalLiveDuration;
    private int countryId;
    private String totalLiveDurationStr;
    private String name;
    private String country;
    private String streamDateStr;

    public String getStreamDateStr() {
        return streamDateStr;
    }

    public void setStreamDateStr(String streamDateStr) {
        this.streamDateStr = streamDateStr;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTotalLiveDurationStr() {
        return totalLiveDurationStr;
    }

    public void setTotalLiveDurationStr(String totalLiveDurationStr) {
        this.totalLiveDurationStr = totalLiveDurationStr;
    }

    public int getCountryId() {
        return countryId;
    }

    public void setCountryId(int countryId) {
        this.countryId = countryId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getTotalLiveDuration() {
        return totalLiveDuration;
    }

    public void setTotalLiveDuration(long totalLiveDuration) {
        this.totalLiveDuration = totalLiveDuration;
    }

    public String getStreamId() {
        return streamId;
    }

    public void setStreamId(String streamId) {
        this.streamId = streamId;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getRingId() {
        return ringId;
    }

    public void setRingId(String ringId) {
        this.ringId = ringId;
    }

    public int getRoomId() {
        return roomId;
    }

    public void setRoomId(int roomId) {
        this.roomId = roomId;
    }

    public int getDevice() {
        return device;
    }

    public void setDevice(int device) {
        this.device = device;
    }

    public long getStreamDate() {
        return streamDate;
    }

    public void setStreamDate(long streamDate) {
        this.streamDate = streamDate;
    }

    public int getNoOfAppearance() {
        return noOfAppearance;
    }

    public void setNoOfAppearance(int noOfAppearance) {
        this.noOfAppearance = noOfAppearance;
    }

    public LiveSummaryDTO getClone() throws CloneNotSupportedException {
        return (LiveSummaryDTO) super.clone();
    }

    public static class CompRingIdDSC implements Comparator<LiveSummaryDTO> {

        @Override
        public int compare(LiveSummaryDTO arg0, LiveSummaryDTO arg1) {
            return arg1.ringId.compareTo(arg0.ringId);
        }
    }

    public static class CompRingIdASC implements Comparator<LiveSummaryDTO> {

        @Override
        public int compare(LiveSummaryDTO arg0, LiveSummaryDTO arg1) {
            return arg0.ringId.compareTo(arg1.ringId);
        }
    }

    public static class CompNameDSC implements Comparator<LiveSummaryDTO> {

        @Override
        public int compare(LiveSummaryDTO arg0, LiveSummaryDTO arg1) {
            return arg1.name.compareTo(arg0.name);
        }
    }

    public static class CompNameASC implements Comparator<LiveSummaryDTO> {

        @Override
        public int compare(LiveSummaryDTO arg0, LiveSummaryDTO arg1) {
            return arg0.name.compareTo(arg1.name);
        }
    }

    public static class CompCountryDSC implements Comparator<LiveSummaryDTO> {

        @Override
        public int compare(LiveSummaryDTO arg0, LiveSummaryDTO arg1) {
            return arg1.country.compareTo(arg0.country);
        }
    }

    public static class CompCountryASC implements Comparator<LiveSummaryDTO> {

        @Override
        public int compare(LiveSummaryDTO arg0, LiveSummaryDTO arg1) {
            return arg0.country.compareTo(arg1.country);
        }
    }

    public static class CompTotalLiveDurationDSC implements Comparator<LiveSummaryDTO> {

        @Override
        public int compare(LiveSummaryDTO arg0, LiveSummaryDTO arg1) {
            return Long.compare(arg1.totalLiveDuration, arg0.totalLiveDuration);
        }
    }

    public static class CompTotalLiveDurationASC implements Comparator<LiveSummaryDTO> {

        @Override
        public int compare(LiveSummaryDTO arg0, LiveSummaryDTO arg1) {
            return Long.compare(arg0.totalLiveDuration, arg1.totalLiveDuration);
        }
    }

    public static class CompNoOfAppearanceDSC implements Comparator<LiveSummaryDTO> {

        @Override
        public int compare(LiveSummaryDTO arg0, LiveSummaryDTO arg1) {
            return arg1.noOfAppearance - arg0.noOfAppearance;
        }
    }

    public static class CompNoOfAppearanceASC implements Comparator<LiveSummaryDTO> {

        @Override
        public int compare(LiveSummaryDTO arg0, LiveSummaryDTO arg1) {
            return arg0.noOfAppearance - arg1.noOfAppearance;
        }
    }

    public static class CompStreamDateDSC implements Comparator<LiveSummaryDTO> {

        @Override
        public int compare(LiveSummaryDTO arg0, LiveSummaryDTO arg1) {
            return Long.compare(arg1.streamDate, arg0.streamDate);
        }
    }

    public static class CompStreamDateASC implements Comparator<LiveSummaryDTO> {

        @Override
        public int compare(LiveSummaryDTO arg0, LiveSummaryDTO arg1) {
            return Long.compare(arg0.streamDate, arg1.streamDate);
        }
    }
}
