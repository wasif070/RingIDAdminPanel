/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.dto;

import com.ringid.admin.BaseDTO;
import com.ringid.admin.utils.Constants;
import java.util.Comparator;
import java.util.List;
import java.util.UUID;
import org.ringid.channel.ChannelCategoryDTO;

/**
 *
 * @author reefat
 */
public class ChannelVerificationDTO extends BaseDTO {

    private UUID channelId;
    private String title;
    private long userId;
    private String mediaIds;
    private String userIdentity;
    private int channelType;
    private List<String> countries;
    private String prIm;
    private long channelBooster;
    private String channelIP;
    private int channelPort;
    private String streamingServerIP;
    private int streamingServerPort;
    private boolean channelSharable;
    private String channelTypeStr;
    private int channelStatus;
    private String channelStatusStr;

    public String getChannelStatusStr() {
        return channelStatusStr;
    }

    public void setChannelStatusStr(String channelStatusStr) {
        this.channelStatusStr = channelStatusStr;
    }

    public int getChannelStatus() {
        return channelStatus;
    }

    public void setChannelStatus(int channelStatus) {
        this.channelStatus = channelStatus;
    }

    public String getChannelTypeStr() {
        return channelTypeStr;
    }

    public void setChannelTypeStr(String channelTypeStr) {
        this.channelTypeStr = channelTypeStr;
    }

    public boolean isChannelSharable() {
        return channelSharable;
    }

    public void setChannelSharable(boolean channelSharable) {
        this.channelSharable = channelSharable;
    }

    public String getChannelIP() {
        return channelIP;
    }

    public void setChannelIP(String channelIP) {
        this.channelIP = channelIP;
    }

    public int getChannelPort() {
        return channelPort;
    }

    public void setChannelPort(int channelPort) {
        this.channelPort = channelPort;
    }

    public String getStreamingServerIP() {
        return streamingServerIP;
    }

    public void setStreamingServerIP(String streamingServerIP) {
        this.streamingServerIP = streamingServerIP;
    }

    public int getStreamingServerPort() {
        return streamingServerPort;
    }

    public void setStreamingServerPort(int streamingServerPort) {
        this.streamingServerPort = streamingServerPort;
    }

    public long getChannelBooster() {
        return channelBooster;
    }

    public void setChannelBooster(long channelBooster) {
        this.channelBooster = channelBooster;
    }

    public int getChannelType() {
        return channelType;
    }

    public void setChannelType(int channelType) {
        this.channelType = channelType;
    }

    public List<String> getCountries() {
        return countries;
    }

    public void setCountries(List<String> countries) {
        this.countries = countries;
    }

    public String getProfileImageUrl() {
        return prIm;
    }

    public void setProfileImageUrl(String prIm) {
        this.prIm = prIm;
    }

    public String getUserIdentity() {
        return userIdentity;
    }

    public void setUserIdentity(String userIdentity) {
        this.userIdentity = userIdentity;
    }

    public UUID getChannelId() {
        return channelId;
    }

    public void setChannelId(UUID channelId) {
        this.channelId = channelId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getMediaIds() {
        return mediaIds;
    }

    public void setMediaIds(String mediaIds) {
        this.mediaIds = mediaIds;
    }

    public static class CompASC implements Comparator<ChannelVerificationDTO> {

        @Override
        public int compare(ChannelVerificationDTO arg0, ChannelVerificationDTO arg1) {
            int returnVal = 0;

            switch (column) {
                case Constants.COLUMN_ONE:
                    returnVal = arg0.getTitle().toLowerCase().compareTo(arg1.getTitle().toLowerCase());
                    break;
                case Constants.COLUMN_TWO:
                    returnVal = arg0.getUserIdentity().toLowerCase().compareTo(arg1.getUserIdentity().toLowerCase());
                    break;
                case Constants.COLUMN_THREE:
                    returnVal = Long.compare(arg0.channelBooster, arg1.channelBooster);
                    break;
                default:
                    returnVal = Long.compare(arg0.channelBooster, arg1.channelBooster);
                    break;
            }
            return returnVal;
        }
    }

    public static class CompDSC implements Comparator<ChannelVerificationDTO> {

        @Override
        public int compare(ChannelVerificationDTO arg0, ChannelVerificationDTO arg1) {
            int returnVal = 0;

            switch (column) {
                case Constants.COLUMN_ONE:
                    returnVal = arg1.getTitle().toLowerCase().compareTo(arg0.getTitle().toLowerCase());
                    break;
                case Constants.COLUMN_TWO:
                    returnVal = arg1.getUserIdentity().toLowerCase().compareTo(arg0.getUserIdentity().toLowerCase());
                    break;
                case Constants.COLUMN_THREE:
                    returnVal = Long.compare(arg1.channelBooster, arg0.channelBooster);
                    break;
                default:
                    returnVal = Long.compare(arg1.channelBooster, arg0.channelBooster);
                    break;
            }
            return returnVal;
        }
    }

    public static class CompCatASC implements Comparator<ChannelCategoryDTO> {

        @Override
        public int compare(ChannelCategoryDTO arg0, ChannelCategoryDTO arg1) {
            switch (column) {
                case Constants.COLUMN_ONE:
                    return arg0.getCategoryId() - arg1.getCategoryId();
                case Constants.COLUMN_TWO:
                    return arg0.getCategory().toLowerCase().compareTo(arg1.getCategory().toLowerCase());
                case Constants.COLUMN_THREE:
                    return arg0.getVerticalWeight() - arg1.getVerticalWeight();
                case Constants.COLUMN_FOUR:
                    return arg0.getHorizontalWeight() - arg1.getHorizontalWeight();
                default:
                    return arg0.getCategoryId() - arg1.getCategoryId();
            }
        }
    }

    public static class CompCatDSC implements Comparator<ChannelCategoryDTO> {

        @Override
        public int compare(ChannelCategoryDTO arg0, ChannelCategoryDTO arg1) {
            switch (column) {
                case Constants.COLUMN_ONE:
                    return arg1.getCategoryId() - arg0.getCategoryId();
                case Constants.COLUMN_TWO:
                    return arg1.getCategory().toLowerCase().compareTo(arg0.getCategory().toLowerCase());
                case Constants.COLUMN_THREE:
                    return arg1.getVerticalWeight() - arg0.getVerticalWeight();
                case Constants.COLUMN_FOUR:
                    return arg1.getHorizontalWeight() - arg0.getHorizontalWeight();
                default:
                    return arg1.getCategoryId() - arg0.getCategoryId();
            }
        }
    }
}
