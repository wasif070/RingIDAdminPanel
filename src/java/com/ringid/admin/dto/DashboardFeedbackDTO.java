/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.dto;

import com.ringid.admin.dto.RegistrationSummaryDTO;
import com.ringid.admin.BaseFeedbackDTO;
import java.util.List;

/**
 *
 * @author Rabby
 */
public class DashboardFeedbackDTO extends BaseFeedbackDTO {

    private long totalUser;
    private List<RegistrationSummaryDTO> registrationStatistic;
    private String startdate;
    private String endDate;

    public String getStartdate() {
        return startdate;
    }

    public void setStartdate(String startdate) {
        this.startdate = startdate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public long getTotalUser() {
        return totalUser;
    }

    public void setTotalUser(long totalUser) {
        this.totalUser = totalUser;
    }

    public List<RegistrationSummaryDTO> getRegistrationStatistic() {
        return registrationStatistic;
    }

    public void setRegistrationStatistic(List<RegistrationSummaryDTO> registrationStatistic) {
        this.registrationStatistic = registrationStatistic;
    }

}
