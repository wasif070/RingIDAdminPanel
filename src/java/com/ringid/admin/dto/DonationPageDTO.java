/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.dto;

import com.ringid.admin.BaseDTO;
import com.ringid.admin.utils.Constants;
import java.util.Comparator;
import org.apache.struts.upload.FormFile;
import org.ringid.pages.PageDTO;

/**
 *
 * @author mamun
 */
public class DonationPageDTO extends BaseDTO {

    private long pageId;
    private String pageName;
    private String description;
    private Integer weight;
    private FormFile profileImage;
    private FormFile coverImage;
    private String bannerImageUrl;
    private int visible;

    public String getBannerImageUrl() {
        return bannerImageUrl;
    }

    public void setBannerImageUrl(String bannerImageUrl) {
        this.bannerImageUrl = bannerImageUrl;
    }

    public int getVisible() {
        return visible;
    }

    public void setVisible(int visible) {
        this.visible = visible;
    }

    public long getPageId() {
        return pageId;
    }

    public void setPageId(long pageId) {
        this.pageId = pageId;
    }

    public String getPageName() {
        return pageName;
    }

    public void setPageName(String pageName) {
        this.pageName = pageName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getWeight() {
        return weight;
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }

    public FormFile getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(FormFile profileImage) {
        this.profileImage = profileImage;
    }

    public FormFile getCoverImage() {
        return coverImage;
    }

    public void setCoverImage(FormFile coverImage) {
        this.coverImage = coverImage;
    }

    public static class CompASC implements Comparator<PageDTO> {

        @Override
        public int compare(PageDTO o1, PageDTO o2) {
            switch (column) {
                case Constants.COLUMN_ONE:
                    return Long.compare(o1.getPageId(), o2.getPageId());
                case Constants.COLUMN_TWO:
                    return Long.compare(o1.getPageRingId(), o2.getPageRingId());
                default:
                    return Long.compare(o1.getPageId(), o2.getPageId());
            }
        }
    }

    public static class CompDSC implements Comparator<PageDTO> {

        @Override
        public int compare(PageDTO o1, PageDTO o2) {
            switch (column) {
                case Constants.COLUMN_ONE:
                    return Long.compare(o2.getPageId(), o1.getPageId());
                case Constants.COLUMN_TWO:
                    return Long.compare(o2.getPageRingId(), o1.getPageRingId());
                default:
                    return Long.compare(o2.getPageId(), o1.getPageId());
            }
        }
    }
}
