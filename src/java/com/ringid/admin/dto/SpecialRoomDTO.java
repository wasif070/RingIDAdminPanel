/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.dto;

/**
 *
 * @author mamun
 */
public class SpecialRoomDTO {

    private long roomId;
    private String name;
    private String description;
    private String banner;
    private long votingStartTime;
    private long votingEndTime;
    private long pageId;
    private int pageProfileType;
    private int visible;

    public long getRoomId() {
        return roomId;
    }

    public void setRoomId(long roomId) {
        this.roomId = roomId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getBanner() {
        return banner;
    }

    public void setBanner(String banner) {
        this.banner = banner;
    }

    public long getVotingStartTime() {
        return votingStartTime;
    }

    public void setVotingStartTime(long votingStartTime) {
        this.votingStartTime = votingStartTime;
    }

    public long getVotingEndTime() {
        return votingEndTime;
    }

    public void setVotingEndTime(long votingEndTime) {
        this.votingEndTime = votingEndTime;
    }

    public long getPageId() {
        return pageId;
    }

    public void setPageId(long pageId) {
        this.pageId = pageId;
    }

    public int getPageProfileType() {
        return pageProfileType;
    }

    public void setPageProfileType(int pageProfileType) {
        this.pageProfileType = pageProfileType;
    }

    public int getVisible() {
        return visible;
    }

    public void setVisible(int visible) {
        this.visible = visible;
    }
}
