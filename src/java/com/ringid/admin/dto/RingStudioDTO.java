package com.ringid.admin.dto;

import com.google.gson.Gson;
import com.ringid.admin.BaseDTO;
import java.util.Comparator;

public class RingStudioDTO extends BaseDTO {

    private int id;
    private int device;
    private int appType;
    private String title;
    private String url;
    private String imageName;
    private int displayTitle;
    private int displayFloatingMenu;
    private String countryCode;
    private String queryString;
    private String supportedCountries[];

    public String[] getSupportedCountries() {
        return supportedCountries;
    }

    public void setSupportedCountries(String[] supportedCountries) {
        this.supportedCountries = supportedCountries;
    }

    public String getQueryString() {
        return queryString;
    }

    public void setQueryString(String queryString) {
        this.queryString = queryString;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getDevice() {
        return device;
    }

    public void setDevice(int device) {
        this.device = device;
    }

    public int getAppType() {
        return appType;
    }

    public void setAppType(int appType) {
        this.appType = appType;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public int getDisplayTitle() {
        return displayTitle;
    }

    public void setDisplayTitle(int displayTitle) {
        this.displayTitle = displayTitle;
    }

    public int getDisplayFloatingMenu() {
        return displayFloatingMenu;
    }

    public void setDisplayFloatingMenu(int displayFloatingMenu) {
        this.displayFloatingMenu = displayFloatingMenu;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }

    public static class CompIdDSC implements Comparator<RingStudioDTO> {

        @Override
        public int compare(RingStudioDTO o1, RingStudioDTO o2) {
            return o2.id - o1.id;
        }
    }

    public static class CompIdASC implements Comparator<RingStudioDTO> {

        @Override
        public int compare(RingStudioDTO o1, RingStudioDTO o2) {
            return o1.id - o2.id;
        }
    }

}
