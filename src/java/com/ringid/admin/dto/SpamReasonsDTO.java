/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.dto;

import java.util.Comparator;

/**
 *
 * @author mamun
 * @Date Feb 7, 2017
 */
public class SpamReasonsDTO {

    private int id;
    private int spamType;
    private String reason;
    private String queryString;

    public String getQueryString() {
        return queryString;
    }

    public void setQueryString(String queryString) {
        this.queryString = queryString;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSpamType() {
        return spamType;
    }

    public void setSpamType(int spamType) {
        this.spamType = spamType;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public boolean isNull(String value) {
        return (value == null || value.length() == 0);
    }

    public static class CompIdDSC implements Comparator<SpamReasonsDTO> {

        @Override
        public int compare(SpamReasonsDTO o1, SpamReasonsDTO o2) {
            return o2.id - o1.id;
        }
    }

    public static class CompIdASC implements Comparator<SpamReasonsDTO> {

        @Override
        public int compare(SpamReasonsDTO o1, SpamReasonsDTO o2) {
            return o1.id - o2.id;
        }
    }

    public static class CompSpamTypeDSC implements Comparator<SpamReasonsDTO> {

        @Override
        public int compare(SpamReasonsDTO o1, SpamReasonsDTO o2) {
            return o2.spamType - o1.spamType;
        }
    }

    public static class CompSpamTypeASC implements Comparator<SpamReasonsDTO> {

        @Override
        public int compare(SpamReasonsDTO o1, SpamReasonsDTO o2) {
            return o1.spamType - o2.spamType;
        }
    }

    @Override
    public String toString() {
        return "SpamReasonsDTO{" + "id=" + id + ", spamType=" + spamType + ", reason=" + reason + '}';
    }
}
