/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.dto;

import com.ringid.admin.BaseDTO;

/**
 *
 * @author Rabby
 */
public class BlackListedUserDTO extends BaseDTO {

    private long userId;
    private long ringId;
    private long addTime;

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public long getRingId() {
        return ringId;
    }

    public void setRingId(long ringId) {
        this.ringId = ringId;
    }

    public long getAddTime() {
        return addTime;
    }

    public void setAddTime(long addTime) {
        this.addTime = addTime;
    }

}
