/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.dto;

import com.ringid.admin.BaseDTO;
import com.ringid.admin.utils.Constants;
import java.util.Comparator;

/**
 *
 * @author mamun
 */
public class UserPageDTO extends BaseDTO {

    private long pageId;
    private long pageRingId;
    private String pageName;
    private int pageType;
    private String profileImageUrl;
    private String pageActiveStatus;
    private boolean pageAutoFollow;

    public boolean isPageAutoFollow() {
        return pageAutoFollow;
    }

    public void setPageAutoFollow(boolean pageAutoFollow) {
        this.pageAutoFollow = pageAutoFollow;
    }

    public String getPageActiveStatus() {
        return pageActiveStatus;
    }

    public void setPageActiveStatus(String pageActiveStatus) {
        this.pageActiveStatus = pageActiveStatus;
    }

    public long getPageRingId() {
        return pageRingId;
    }

    public void setPageRingId(long pageRingId) {
        this.pageRingId = pageRingId;
    }

    public int getPageType() {
        return pageType;
    }

    public void setPageType(int pageType) {
        this.pageType = pageType;
    }

    public long getPageId() {
        return pageId;
    }

    public void setPageId(long pageId) {
        this.pageId = pageId;
    }

    public String getPageName() {
        return pageName;
    }

    public void setPageName(String pageName) {
        this.pageName = pageName;
    }

    public String getProfileImageUrl() {
        return profileImageUrl;
    }

    public void setProfileImageUrl(String profileImageUrl) {
        this.profileImageUrl = profileImageUrl;
    }

    public static class CompASC implements Comparator<UserPageDTO> {

        @Override
        public int compare(UserPageDTO o1, UserPageDTO o2) {
            switch (column) {
                case Constants.COLUMN_ONE:
                    return Long.compare(o1.getPageId(), o2.getPageId());
                default:
                    return Long.compare(o1.getPageId(), o2.getPageId());
            }
        }
    }

    public static class CompDSC implements Comparator<UserPageDTO> {

        @Override
        public int compare(UserPageDTO o1, UserPageDTO o2) {
            switch (column) {
                case Constants.COLUMN_ONE:
                    return Long.compare(o2.getPageId(), o1.getPageId());
                default:
                    return Long.compare(o2.getPageId(), o1.getPageId());
            }
        }
    }
}
