/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.dto;

import java.util.Comparator;

/**
 *
 * @author mamun
 * @Date Jan 28, 2017
 */
public class AppStoresDTO {

    private int id;
    private String version;
    private int device;
    private String queryString;
    private String deviceStr;

    public int getDevice() {
        return device;
    }

    public void setDevice(int device) {
        this.device = device;
    }

    public String getDeviceStr() {
        return deviceStr;
    }

    public void setDeviceStr(String deviceStr) {
        this.deviceStr = deviceStr;
    }

    public String getQueryString() {
        return queryString;
    }

    public void setQueryString(String queryString) {
        this.queryString = queryString;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    @Override
    public String toString() {
        return "AppStoresDTO{" + "id=" + id + ", version=" + version + ", deviceType=" + device + '}';
    }

    public static class CompIdDSC implements Comparator<AppStoresDTO> {

        @Override
        public int compare(AppStoresDTO o1, AppStoresDTO o2) {
            return o2.id - o1.id;
        }
    }

    public static class CompIdASC implements Comparator<AppStoresDTO> {

        @Override
        public int compare(AppStoresDTO o1, AppStoresDTO o2) {
            return o1.id - o2.id;
        }
    }

}
