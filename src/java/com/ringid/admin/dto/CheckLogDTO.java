/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.dto;

import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Date;

/**
 *
 * @author ipvision
 */
public class CheckLogDTO {

    private Integer serverType;
    private String serverIP;
    private String status;
    private Long requestTime;
    private String responseMSG;
    private Long responseTime;
    private String requestDate;
    private String responseDate;
    private String pckId;
    private int reasonCode;
    public static final String SUCCESS = "SUCCESS";
    public static final String PENDING = "PENDING";
    public static final String FAILURE = "FAILED";
    public static final String TIMEOUT = "TIME-OUT";

    public static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm a");

    public Integer getServerType() {
        return serverType;
    }

    public void setServerType(Integer serverType) {
        this.serverType = serverType;
    }

    public String getServerIP() {
        return serverIP;
    }

    public void setServerIP(String serverIP) {
        this.serverIP = serverIP;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getRequestTime() {
        return requestTime;
    }

    public void setRequestTime(Long requestTime) {
        this.requestTime = requestTime;
    }

    public String getResponseMSG() {
        return responseMSG;
    }

    public void setResponseMSG(String responseMSG) {
        this.responseMSG = responseMSG;
    }

    public Long getResponseTime() {
        return responseTime;
    }

    public void setResponseTime(Long responseTime) {
        this.responseTime = responseTime;
    }

    public String getRequestDate() {
        return requestDate;
    }

    public void setRequestDate(Long requestDate) {
        this.requestDate = "";
        if (requestDate > 0) {
            Date date = new Date(requestDate);
            this.requestDate = sdf.format(date);
        }
    }

    public String getResponseDate() {
        return responseDate;
    }

    public void setResponseDate(Long responseDate) {
        this.responseDate = "";
        if (responseDate > 0) {
            Date date = new Date(responseDate);
            this.responseDate = sdf.format(date);
        }
    }

    public String getPckId() {
        return pckId;
    }

    public void setPckId(String pckId) {
        this.pckId = pckId;
    }

    public int getReasonCode() {
        return reasonCode;
    }

    public void setReasonCode(int reasonCode) {
        this.reasonCode = reasonCode;
    }

//    public JsonObject getJsonObject() {
//        try {
//            JsonObject json = new JsonObject();
//            json.addProperty("serverType", serverType);
//            json.addProperty("serverIP", serverIP);
//            json.addProperty("status", status);
//            json.addProperty("requestTime", requestTime);
//            return json;
//        } catch (Exception ex) {
//            return null;
//        }
//    }
    public static class CompRequestTimeDSC implements Comparator<CheckLogDTO> {

        @Override
        public int compare(CheckLogDTO o1, CheckLogDTO o2) {
            return (int) (o2.requestTime - o1.requestTime);
        }
    }

    public static class CompRequestTimeASC implements Comparator<CheckLogDTO> {

        @Override
        public int compare(CheckLogDTO o1, CheckLogDTO o2) {
            return (int) (o1.requestTime - o2.requestTime);
        }
    }

    @Override
    public String toString() {
        return "CheckLogDTO{" + "serverType=" + serverType + ", serverIP=" + serverIP + ", status=" + status + ", requestTime=" + requestTime + ", responseMSG=" + responseMSG + ", responseTime=" + responseTime + ", requestDate=" + requestDate + ", responseDate=" + responseDate + '}';
    }
}
