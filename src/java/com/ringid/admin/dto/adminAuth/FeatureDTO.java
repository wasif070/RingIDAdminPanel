/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.dto.adminAuth;

import com.ringid.admin.BaseDTO;

/**
 *
 * @author Rabby
 */
public class FeatureDTO extends BaseDTO {

    private int id;
    private String featureName;
    private String featureCode;
    private int parentId;
    private String parentName;
    private String projectName;
    private boolean isSearchByFeatureName;
    private int permissionLevel;
    private boolean hasPermission;
    private int operationType;

    public int getOperationType() {
        return operationType;
    }

    public void setOperationType(int operationType) {
        this.operationType = operationType;
    }

    public int getPermissionLevel() {
        return permissionLevel;
    }

    public void setPermissionLevel(int permissionLevel) {
        this.permissionLevel = permissionLevel;
    }

    public boolean getHasPermission() {
        return hasPermission;
    }

    public void setHasPermission(boolean hasPermission) {
        this.hasPermission = hasPermission;
    }

    public String getParentName() {
        return parentName;
    }

    public void setParentName(String parentName) {
        this.parentName = parentName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFeatureName() {
        return featureName;
    }

    public void setFeatureName(String featureName) {
        this.featureName = featureName;
    }

    public String getFeatureCode() {
        return featureCode;
    }

    public void setFeatureCode(String featureCode) {
        this.featureCode = featureCode;
    }

    public int getParentId() {
        return parentId;
    }

    public void setParentId(int parentId) {
        this.parentId = parentId;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public boolean getIsSearchByFeatureName() {
        return isSearchByFeatureName;
    }

    public void setIsSearchByFeatureName(boolean isSearchByFeatureName) {
        this.isSearchByFeatureName = isSearchByFeatureName;
    }

}
