package com.ringid.admin.dto.adminAuth;

import java.util.HashMap;

public class LoginDTO {

    private long userId;
    private String userName;
    private String password;
    private String fullName;
    private int userType;
    private int permissionLevel;
    private String email;
    private String profileImageUrl;
    private HashMap<String, UserFeatureMappingDTO> feauterMapList;

    public HashMap<String, UserFeatureMappingDTO> getFeauterMapList() {
        return feauterMapList;
    }

    public void setFeauterMapList(HashMap<String, UserFeatureMappingDTO> feauterMapList) {
        this.feauterMapList = feauterMapList;
    }

    private long loginTime;

    public LoginDTO() {

    }

    public String getProfileImageUrl() {
        return profileImageUrl;
    }

    public void setProfileImageUrl(String profileImageUrl) {
        this.profileImageUrl = profileImageUrl;
    }

    public long getLoginTime() {
        return loginTime;
    }

    public void setLoginTime(long loginTime) {
        this.loginTime = loginTime;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public int getUserType() {
        return userType;
    }

    public void setUserType(int userType) {
        this.userType = userType;
    }

    public int getPermissionLevel() {
        return permissionLevel;
    }

    public void setPermissionLevel(int permissionLevel) {
        this.permissionLevel = permissionLevel;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

}
