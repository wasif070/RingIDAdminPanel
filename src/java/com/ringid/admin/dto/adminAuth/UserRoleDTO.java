/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.dto.adminAuth;

import com.ringid.admin.BaseDTO;

/**
 *
 * @author Rabby
 */
public class UserRoleDTO extends BaseDTO {

    private int id;
    private String name;
    private boolean isSearchByName;

    public boolean getIsSearchByName() {
        return isSearchByName;
    }

    public void setIsSearchByName(boolean isSearchByName) {
        this.isSearchByName = isSearchByName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
