/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.dto.adminAuth;

/**
 *
 * @author Rabby
 */
public class UserFeatureMappingDTO {

    private int featureId;
    private String featureName;
    private int permissionLevel;
    private boolean hasChildPermission;
    private boolean hasViewPermission;
    private boolean hasAddPermission;
    private boolean hasModifyPermission;
    private boolean hasDeletePermission;

    public int getFeatureId() {
        return featureId;
    }

    public void setFeatureId(int featureId) {
        this.featureId = featureId;
    }

    public boolean getHasChildPermission() {
        return hasChildPermission;
    }

    public void setHasChildPermission(boolean hasChildPermission) {
        this.hasChildPermission = hasChildPermission;
    }

    public String getFeatureName() {
        return featureName;
    }

    public void setFeatureName(String featureName) {
        this.featureName = featureName;
    }

    public int getPermissionLevel() {
        return permissionLevel;
    }

    public void setPermissionLevel(int permissionLevel) {
        this.permissionLevel = permissionLevel;
    }

    public boolean getHasViewPermission() {
        return hasViewPermission;
    }

    public void setHasViewPermission(boolean hasViewPermission) {
        this.hasViewPermission = hasViewPermission;
    }

    public boolean getHasAddPermission() {
        return hasAddPermission;
    }

    public void setHasAddPermission(boolean hasAddPermission) {
        this.hasAddPermission = hasAddPermission;
    }

    public boolean getHasModifyPermission() {
        return hasModifyPermission;
    }

    public void setHasModifyPermission(boolean hasModifyPermission) {
        this.hasModifyPermission = hasModifyPermission;
    }

    public boolean getHasDeletePermission() {
        return hasDeletePermission;
    }

    public void setHasDeletePermission(boolean hasDeletePermission) {
        this.hasDeletePermission = hasDeletePermission;
    }

    @Override
    public String toString() {
        return "UserFeatureMappingDTO{" + "featureId=" + featureId + ", featureName=" + featureName + ", permissionLevel=" + permissionLevel + ", hasChildPermission=" + hasChildPermission + ", hasViewPermission=" + hasViewPermission + ", hasAddPermission=" + hasAddPermission + ", hasModifyPermission=" + hasModifyPermission + ", hasDeletePermission=" + hasDeletePermission + '}';
    }

}
