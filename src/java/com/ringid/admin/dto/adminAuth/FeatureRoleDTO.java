/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.dto.adminAuth;

import java.util.HashMap;
import java.util.List;

/**
 *
 * @author Rabby
 */
public class FeatureRoleDTO {

    private int roleId;
    private String roleName;
    List<FeatureDTO> featureList;
    List<FeatureDTO> rootFeatureList;
    HashMap<Integer, FeatureDTO> featureListMap;

    public HashMap<Integer, FeatureDTO> getFeatureListMap() {
        return featureListMap;
    }

    public void setFeatureListMap(HashMap<Integer, FeatureDTO> featureListMap) {
        this.featureListMap = featureListMap;
    }

    public List<FeatureDTO> getRootFeatureList() {
        return rootFeatureList;
    }

    public void setRootFeatureList(List<FeatureDTO> rootFeatureList) {
        this.rootFeatureList = rootFeatureList;
    }

    public int getRoleId() {
        return roleId;
    }

    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public List<FeatureDTO> getFeatureList() {
        return featureList;
    }

    public void setFeatureList(List<FeatureDTO> featureList) {
        this.featureList = featureList;
    }

}
