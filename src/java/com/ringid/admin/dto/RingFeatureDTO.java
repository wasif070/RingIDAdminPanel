/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.dto;

import com.ringid.admin.BaseDTO;
import com.ringid.admin.utils.Constants;
import java.util.Comparator;

/**
 *
 * @author Rabby
 */
public class RingFeatureDTO extends BaseDTO {

    private int id;
    private String featureName;
    private int appType;
    private String appTypeName;
    private int platform;
    private String platformName;
    private int minVersion;
    private int status;
    private String platformMinVersionStr;

    public String getPlatformMinVersionStr() {
        return platformMinVersionStr;
    }

    public void setPlatformMinVersionStr(String platformMinVersionStr) {
        this.platformMinVersionStr = platformMinVersionStr;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getAppTypeName() {
        return appTypeName;
    }

    public void setAppTypeName(String appTypeName) {
        this.appTypeName = appTypeName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFeatureName() {
        return featureName;
    }

    public void setFeatureName(String featureName) {
        this.featureName = featureName;
    }

    public int getAppType() {
        return appType;
    }

    public void setAppType(int appType) {
        this.appType = appType;
    }

    public int getPlatform() {
        return platform;
    }

    public void setPlatform(int platform) {
        this.platform = platform;
    }

    public String getPlatformName() {
        return platformName;
    }

    public void setPlatformName(String platformName) {
        this.platformName = platformName;
    }

    public int getMinVersion() {
        return minVersion;
    }

    public void setMinVersion(int minVersion) {
        this.minVersion = minVersion;
    }

    public static class CompASC implements Comparator<RingFeatureDTO> {

        @Override
        public int compare(RingFeatureDTO arg0, RingFeatureDTO arg1) {
            int returnVal = 0;

            switch (column) {
                case Constants.COLUMN_ONE:
                    returnVal = arg0.getId() - arg1.getId();
                    break;
                case Constants.COLUMN_TWO:
                    returnVal = arg0.getFeatureName().toLowerCase().compareTo(arg1.getFeatureName().toLowerCase());
                    break;
                case Constants.COLUMN_THREE:
                    returnVal = arg0.getPlatform() - arg1.getPlatform();
                    break;
                default:
                    returnVal = arg0.getId() - arg1.getId();
                    break;
            }
            return returnVal;
        }
    }

    public static class CompDSC implements Comparator<RingFeatureDTO> {

        @Override
        public int compare(RingFeatureDTO arg0, RingFeatureDTO arg1) {
            int returnVal = 0;

            switch (column) {
                case Constants.COLUMN_ONE:
                    returnVal = arg1.getId() - arg0.getId();
                    break;
                case Constants.COLUMN_TWO:
                    returnVal = arg1.getFeatureName().toLowerCase().compareTo(arg0.getFeatureName().toLowerCase());
                    break;
                case Constants.COLUMN_THREE:
                    returnVal = arg1.getPlatform() - arg0.getPlatform();
                    break;
                default:
                    returnVal = arg1.getId() - arg0.getId();
                    break;
            }
            return returnVal;
        }
    }
}
