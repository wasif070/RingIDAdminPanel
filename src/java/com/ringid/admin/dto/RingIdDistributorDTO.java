/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.dto;

import com.ringid.admin.BaseDTO;
import com.ringid.admin.utils.Constants;
import java.util.Comparator;

/**
 *
 * @author Rabby
 */
public class RingIdDistributorDTO extends BaseDTO {

    private int serverId;
    private long startRingId;
    private long endRingId;
    private int status;
    private String serverIP;

    public String getServerIP() {
        return serverIP;
    }

    public void setServerIP(String serverIP) {
        this.serverIP = serverIP;
    }

    public int getServerId() {
        return serverId;
    }

    public void setServerId(int serverId) {
        this.serverId = serverId;
    }

    public long getStartRingId() {
        return startRingId;
    }

    public void setStartRingId(long startRingId) {
        this.startRingId = startRingId;
    }

    public long getEndRingId() {
        return endRingId;
    }

    public void setEndRingId(long endRingId) {
        this.endRingId = endRingId;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public static class CompASC implements Comparator<RingIdDistributorDTO> {

        @Override
        public int compare(RingIdDistributorDTO arg0, RingIdDistributorDTO arg1) {
            int returnVal = 0;

            switch (column) {
                case Constants.COLUMN_ONE:
                    returnVal = arg0.getServerId() - arg1.getServerId();
                    break;
                case Constants.COLUMN_TWO:
                    returnVal = Long.compare(arg0.getStartRingId(), arg1.getStartRingId());
                    break;
                case Constants.COLUMN_THREE:
                    returnVal = Long.compare(arg0.getEndRingId(), arg1.getEndRingId());
                    break;
                default:
                    returnVal = arg0.getServerId() - arg1.getServerId();
                    break;
            }
            return returnVal;
        }
    }

    public static class CompDSC implements Comparator<RingIdDistributorDTO> {

        @Override
        public int compare(RingIdDistributorDTO arg0, RingIdDistributorDTO arg1) {
            int returnVal = 0;

            switch (column) {
                case Constants.COLUMN_ONE:
                    returnVal = arg1.getServerId() - arg0.getServerId();
                    break;
                case Constants.COLUMN_TWO:
                    returnVal = Long.compare(arg1.getStartRingId(), arg0.getStartRingId());
                    break;
                case Constants.COLUMN_THREE:
                    returnVal = Long.compare(arg1.getEndRingId(), arg0.getEndRingId());
                    break;
                default:
                    returnVal = arg1.getServerId() - arg0.getServerId();
                    break;
            }
            return returnVal;
        }
    }
}
