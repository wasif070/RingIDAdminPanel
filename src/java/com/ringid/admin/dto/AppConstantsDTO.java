/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.dto;

import com.ringid.admin.BaseDTO;
import com.ringid.admin.utils.Constants;
import java.util.Comparator;

/**
 *
 * @author Kazi Nasir Uddin Oly
 * @Date Jun 14, 2016
 */
public class AppConstantsDTO extends BaseDTO {

    private int id;
    private int platform;
    private int appType;
    private int version;
    private String authController;
    private String imageResource;
    private String imageServer;
    private int displayDigits;
    private long officialUserId;
    private String marketServer;
    private String marketResource;
    private String vodServer;
    private String vodResource;
    private String web;
    private String paymentGateway;
    private int constantsVersion;
    private String PlatformStr;
    private String appTypeStr;

    public String getAppTypeStr() {
        return appTypeStr;
    }

    public void setAppTypeStr(String appTypeStr) {
        this.appTypeStr = appTypeStr;
    }

    public String getPlatformStr() {
        return PlatformStr;
    }

    public void setPlatformStr(String PlatformStr) {
        this.PlatformStr = PlatformStr;
    }

    public String getPaymentGateway() {
        return paymentGateway;
    }

    public void setPaymentGateway(String paymentGateway) {
        this.paymentGateway = paymentGateway;
    }

    public int getConstantsVersion() {
        return constantsVersion;
    }

    public void setConstantsVersion(int constantsVersion) {
        this.constantsVersion = constantsVersion;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPlatform() {
        return platform;
    }

    public void setPlatform(int platform) {
        this.platform = platform;
    }

    public int getAppType() {
        return appType;
    }

    public void setAppType(int appType) {
        this.appType = appType;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public String getAuthController() {
        return authController;
    }

    public void setAuthController(String authController) {
        this.authController = authController;
    }

    public String getImageResource() {
        return imageResource;
    }

    public void setImageResource(String imageResource) {
        this.imageResource = imageResource;
    }

    public String getImageServer() {
        return imageServer;
    }

    public void setImageServer(String imageServer) {
        this.imageServer = imageServer;
    }

    public int getDisplayDigits() {
        return displayDigits;
    }

    public void setDisplayDigits(int displayDigits) {
        this.displayDigits = displayDigits;
    }

    public long getOfficialUserId() {
        return officialUserId;
    }

    public void setOfficialUserId(long officialUserId) {
        this.officialUserId = officialUserId;
    }

    public String getMarketServer() {
        return marketServer;
    }

    public void setMarketServer(String marketServer) {
        this.marketServer = marketServer;
    }

    public String getMarketResource() {
        return marketResource;
    }

    public void setMarketResource(String marketResource) {
        this.marketResource = marketResource;
    }

    public String getVodServer() {
        return vodServer;
    }

    public void setVodServer(String vodServer) {
        this.vodServer = vodServer;
    }

    public String getVodResource() {
        return vodResource;
    }

    public void setVodResource(String vodResource) {
        this.vodResource = vodResource;
    }

    public String getWeb() {
        return web;
    }

    public void setWeb(String web) {
        this.web = web;
    }

    public static class CompASC implements Comparator<AppConstantsDTO> {

        @Override
        public int compare(AppConstantsDTO arg0, AppConstantsDTO arg1) {
            switch (column) {
                case Constants.COLUMN_ONE:
                    return arg0.getPlatform() - arg1.getPlatform();
                case Constants.COLUMN_TWO:
                    return arg0.getAppType() - arg1.getAppType();
                case Constants.COLUMN_THREE:
                    return arg0.getVersion() - arg1.getVersion();
                default:
                    return arg0.getPlatform() - arg1.getPlatform();
            }
        }
    }

    public static class CompDSC implements Comparator<AppConstantsDTO> {

        @Override
        public int compare(AppConstantsDTO arg0, AppConstantsDTO arg1) {
            switch (column) {
                case Constants.COLUMN_ONE:
                    return arg1.getPlatform() - arg0.getPlatform();
                case Constants.COLUMN_TWO:
                    return arg1.getAppType() - arg0.getAppType();
                case Constants.COLUMN_THREE:
                    return arg1.getVersion() - arg0.getVersion();
                default:
                    return arg1.getPlatform() - arg0.getPlatform();
            }
        }
    }
}
