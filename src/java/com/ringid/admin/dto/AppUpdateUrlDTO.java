/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.dto;

import com.ringid.admin.BaseDTO;

/**
 *
 * @author mamun
 */
public class AppUpdateUrlDTO extends BaseDTO {

    private int id;
    private int platform;
    private int appType;
    private String updateUrl;
    private String appName;
    private String platformStr;
    private String appTypeStr;

    public String getPlatformStr() {
        return platformStr;
    }

    public void setPlatformStr(String platformStr) {
        this.platformStr = platformStr;
    }

    public String getAppTypeStr() {
        return appTypeStr;
    }

    public void setAppTypeStr(String appTypeStr) {
        this.appTypeStr = appTypeStr;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPlatform() {
        return platform;
    }

    public void setPlatform(int platform) {
        this.platform = platform;
    }

    public int getAppType() {
        return appType;
    }

    public void setAppType(int appType) {
        this.appType = appType;
    }

    public String getUpdateUrl() {
        return updateUrl;
    }

    public void setUpdateUrl(String updateUrl) {
        this.updateUrl = updateUrl;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }
}
