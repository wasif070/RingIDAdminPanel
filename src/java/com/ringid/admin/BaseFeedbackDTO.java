/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin;

/**
 *
 * @author Rabby
 */
public class BaseFeedbackDTO {

    private boolean sucs;
    private String ms;
    private int rc;

    public int getReasonCode() {
        return rc;
    }

    public void setReasonCode(int rc) {
        this.rc = rc;
    }

    public BaseFeedbackDTO() {
    }

    public boolean isSucs() {
        return sucs;
    }

    public void setSucs(boolean sucs) {
        this.sucs = sucs;
    }

    public String getMessage() {
        return ms;
    }

    public void setMessage(String ms) {
        this.ms = ms;
    }
}
