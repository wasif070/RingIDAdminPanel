/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.dao;

import com.ringid.admin.dto.DonationPageDTO;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.dto.newsportal.NewsPortalDTO;
import com.ringid.admin.actions.newsPortal.NewsPortalFeedBack;
import com.ringid.admin.dto.UserPageDTO;
import com.ringid.admin.utils.MyAppError;
import authdbconnector.DBConnection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import org.ringid.pages.PageDTO;

/**
 *
 * @author Rabby
 */
public class PageDAO {

    private static PageDAO instance = null;

    private PageDAO() {
    }

    public static PageDAO getInstance() {
        if (instance == null) {
            createInstance();
        }
        return instance;
    }

    private synchronized static void createInstance() {
        if (instance == null) {
            instance = new PageDAO();
        }
    }

    public List<UserPageDTO> getAutoFollowPageStatusInfo() {
        List<UserPageDTO> pageIdAndStatusInfo = new ArrayList<>();
        DBConnection db = null;
        Statement stmt = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            String sql = "SELECT pageId, isEnable FROM autoFollowPages;";
            ps = db.connection.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                UserPageDTO dto = new UserPageDTO();
                dto.setPageId(rs.getLong("pageId"));
                dto.setPageAutoFollow(rs.getBoolean("isEnable"));
                pageIdAndStatusInfo.add(dto);
            }
        } catch (SQLException e) {
            RingLogger.getConfigPortalLogger().debug("getAutoFollowPageStatusInfo SQL Exception --> " + e);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().debug("getAutoFollowPageStatusInfo Exception --> " + e);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (db != null) {
                    authdbconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
        }
        return pageIdAndStatusInfo;
    }

    public MyAppError updateAutoFollowPageStatusInfo(long pageId, boolean isEnable) {
        MyAppError error = new MyAppError();
        DBConnection db = null;
        Statement stmt = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        int i=1;
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            String query = "INSERT INTO autoFollowPages (pageId, isEnable) VALUES(?, ?) ON DUPLICATE KEY UPDATE isEnable=?;";
            ps = db.connection.prepareStatement(query);
            ps.setLong(i++, pageId);
            ps.setBoolean(i++, isEnable);
            ps.setBoolean(i++, isEnable);
            ps.execute();
            RingLogger.getConfigPortalLogger().debug(query);
        } catch (SQLException e) {
            error.setERROR_TYPE(error.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            RingLogger.getConfigPortalLogger().debug("updateAutoFollowPageStatusInfo SQL Exception --> " + e);
        } catch (Exception e) {
            error.setERROR_TYPE(error.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            RingLogger.getConfigPortalLogger().debug("updateAutoFollowPageStatusInfo Exception --> " + e);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (db != null) {
                    authdbconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError addDonationPageBannerImageAndVisibility(long pageId, String bannerUrl, int visible) {
        MyAppError error = new MyAppError();
        DBConnection db = null;
        Statement stmt = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            String sql = "DELETE FROM donationpages;";
            stmt = db.connection.createStatement();
            int a = stmt.executeUpdate(sql);
            RingLogger.getConfigPortalLogger().debug("donationpages delete success");
            String query = "INSERT INTO donationpages (pageId, banner, isVisible) VALUES (?, ?, ?);";
            ps = db.connection.prepareStatement(query);
            ps.setLong(1, pageId);
            ps.setString(2, bannerUrl);
            ps.setInt(3, visible);
            ps.execute();
            RingLogger.getConfigPortalLogger().debug(query);
        } catch (SQLException e) {
            error.setERROR_TYPE(error.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            RingLogger.getConfigPortalLogger().debug("addDonationPageBannerImageAndVisibility SQL Exception --> " + e);
        } catch (Exception e) {
            error.setERROR_TYPE(error.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            RingLogger.getConfigPortalLogger().debug("addDonationPageBannerImageAndVisibility Exception --> " + e);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (db != null) {
                    authdbconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public DonationPageDTO getDonationPage() {
        DonationPageDTO donationPageDTO = new DonationPageDTO();
        DBConnection db = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            String sql = "SELECT pageId, banner, isVisible FROM donationpages;";
            ps = db.connection.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                donationPageDTO.setPageId(rs.getLong("pageId"));
                donationPageDTO.setBannerImageUrl(rs.getString("banner"));
                donationPageDTO.setVisible(rs.getInt("isVisible"));
            }
            RingLogger.getConfigPortalLogger().debug(sql + "::" + donationPageDTO.getPageId());
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().debug("getChannelServerDTO List Exception...", e);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (db != null) {
                    authdbconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
        }
        return donationPageDTO;
    }

    public NewsPortalFeedBack getPageList(NewsPortalDTO sdto, int start, int limit) {
        NewsPortalFeedBack newsPortalFeedBack = new NewsPortalFeedBack();
        ArrayList<NewsPortalDTO> pageList = new ArrayList<>();
        newsPortalFeedBack.setNewsPortalList(pageList);
        DBConnection db = null;
        Statement stmt = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            stmt = db.connection.createStatement();
            String sql = "SELECT count(id) AS c FROM page where type = " + sdto.getPageType();
            if (sdto.getPageName() != null && sdto.getPageName().length() > 0) {
                sql += " AND name like '%" + sdto.getPageName() + "%'";
            }
            if (sdto.getDiscoverable() == 1 || sdto.getDiscoverable() == 2) {
                sql += " AND discoverable = " + (sdto.getDiscoverable() == 1 ? "1" : "0");
            }
            rs = stmt.executeQuery(sql);
            if (rs.next()) {
                newsPortalFeedBack.setTotalResult(rs.getInt("c"));
            }

            sql = "SELECT id, name,discoverable,type,trending FROM page where type = " + sdto.getPageType();
            if (sdto.getPageName() != null && sdto.getPageName().length() > 0) {
                sql += " AND name like '%" + sdto.getPageName() + "%'";
            }
            if (sdto.getDiscoverable() == 1 || sdto.getDiscoverable() == 2) {
                sql += " AND discoverable = " + (sdto.getDiscoverable() == 1 ? "1" : "0");
            }
            sql += " limit " + start + ", " + limit;
            rs = stmt.executeQuery(sql);
            while (rs.next()) {
                NewsPortalDTO dto = new NewsPortalDTO();
                dto.setPageId(rs.getInt("id"));
                dto.setPageName(rs.getString("name"));
                dto.setDiscoverable(rs.getInt("discoverable"));
                dto.setPageType(rs.getInt("type"));
                dto.setTrending(rs.getInt("trending"));
                pageList.add(dto);
            }
            RingLogger.getConfigPortalLogger().debug(sql + "::" + pageList.size());
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().debug("Page List Exception...", e);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (db != null) {
                    authdbconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
        }
        return newsPortalFeedBack;
    }

    /**
     * For adding page details
     *
     * @param pageDTO
     * @return true/false
     */
    public boolean addPage(PageDTO pageDTO) {
        boolean result = false;
        DBConnection db = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();

            String sql = "INSERT INTO pages SET pageId = ?, pageRingID = ?, pageName = ?, pageType = ?, pageUserId = ?, addedTime = ?";
            RingLogger.getConfigPortalLogger().info("[addPage] query -> " + sql);
            ps = db.connection.prepareStatement(sql);
            int i = 1;
            ps.setLong(i++, pageDTO.getPageId());
            ps.setLong(i++, pageDTO.getPageRingId());
            ps.setString(i++, pageDTO.getPageName());
            ps.setInt(i++, pageDTO.getPageType());
            ps.setLong(i++, pageDTO.getPageOwner());
            ps.setLong(i++, System.currentTimeMillis());
            if (ps.executeUpdate() > 0) {
                result = true;
            }
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in addPage " + pageDTO.getPageId() + " -->", e);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (db != null) {
                    authdbconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
        }
        return result;
    }

    /**
     * For update page details
     *
     * @param pageDTO
     * @return true/false
     */
    public boolean updatePage(PageDTO pageDTO) {
        boolean returnValue = false;
        DBConnection db = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();

            int index = 0;
            String sql = "update pages set updateTime=" + System.currentTimeMillis();
            if (pageDTO.getPageName() != null && pageDTO.getPageName().length() > 0) {
                sql += ",pageName=?";
                index++;
            }
            if (pageDTO.getPageType() > 0) {
                sql += ",pageType=?";
                index++;
            }
            sql += " where pageId=" + pageDTO.getPageId();
            if (index == 0) {
                return false;
            }

            index = 1;
            ps = db.connection.prepareStatement(sql);
            if (pageDTO.getPageName() != null && pageDTO.getPageName().length() > 0) {
                ps.setString(index++, pageDTO.getPageName());
            }
            if (pageDTO.getPageType() > 0) {
                ps.setLong(index++, pageDTO.getPageType());
            }
            if (ps.executeUpdate() > 0) {
                returnValue = true;
            }
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in updatePage " + pageDTO.getPageId() + " -->", e);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (db != null) {
                    authdbconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
        }
        return returnValue;
    }

    /**
     * For update page details
     *
     * @param pageId
     * @param status
     * @return true/false
     */
    public boolean updatePageStatus(long pageId, int status) {
        boolean returnValue = false;
        DBConnection db = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            String sql = "update pages set status=" + status + ",updateTime=" + System.currentTimeMillis() + " where pageId=" + pageId;
            ps = db.connection.prepareStatement(sql);
            if (ps.executeUpdate() > 0) {
                returnValue = true;
            }
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in updatePageStatus " + pageId + " -->", e);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (db != null) {
                    authdbconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
        }
        return returnValue;
    }

    public MyAppError updatePageTrending(String[] pageIds, boolean trending) {
        MyAppError error = new MyAppError();
        StringBuilder stringBuilder = new StringBuilder();
        for (String pageId : pageIds) {
            stringBuilder.append(pageId);
            stringBuilder.append(",");
        }
        String ids = stringBuilder.toString();
        ids = ids.substring(0, ids.length() - 1);
        DBConnection db = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            String query = "UPDATE page SET trending = ? WHERE id in (" + ids + ")";
            ps = db.connection.prepareStatement(query);
            if (trending) {
                ps.setInt(1, 1);
            } else {
                ps.setInt(1, 0);
            }
            ps.execute();
        } catch (SQLException e) {
            error.setERROR_TYPE(MyAppError.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            RingLogger.getConfigPortalLogger().debug("updatePageTrending SQL Exception --> " + e);
        } catch (Exception e) {
            error.setERROR_TYPE(MyAppError.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            RingLogger.getConfigPortalLogger().debug("updatePageTrending Exception --> " + e);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (db != null) {
                    authdbconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError updatePageDiscoverable(long[] pageIds, boolean discoverable) {
        MyAppError error = new MyAppError();
        StringBuilder stringBuilder = new StringBuilder();
        for (long l : pageIds) {
            stringBuilder.append(l);
            stringBuilder.append(",");
        }
        String ids = stringBuilder.toString();
        ids = ids.substring(0, ids.length() - 1);
        DBConnection db = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            String query = "UPDATE page SET discoverable = ? WHERE id in (" + ids + ")";
            ps = db.connection.prepareStatement(query);
            if (discoverable) {
                ps.setInt(1, 1);
            } else {
                ps.setInt(1, 0);
            }
            ps.execute();
        } catch (SQLException e) {
            error.setERROR_TYPE(MyAppError.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            RingLogger.getConfigPortalLogger().debug("updatePageDiscoverable Map SQL Exception --> " + e);
        } catch (Exception e) {
            error.setERROR_TYPE(MyAppError.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            RingLogger.getConfigPortalLogger().debug("updatePageDiscoverable Map Exception --> " + e);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (db != null) {
                    authdbconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }
}
