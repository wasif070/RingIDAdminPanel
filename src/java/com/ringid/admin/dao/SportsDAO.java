/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.dao;

import com.google.gson.Gson;
import com.ringid.admin.BaseDAO;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.utils.MyAppError;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author mamun
 */
public class SportsDAO extends BaseDAO {

    private static SportsDAO instance = null;

    private SportsDAO() {
    }

    public static SportsDAO getInstance() {
        if (instance == null) {
            createInstance();
        }
        return instance;
    }

    private synchronized static void createInstance() {
        if (instance == null) {
            instance = new SportsDAO();
        }
    }

    public List<String> getHomeMatchList() {
        List<String> list = new ArrayList<>();
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            stmt = db.connection.createStatement();
            String sql = "SELECT matchId FROM home_match_list";
            rs = stmt.executeQuery(sql);
            while (rs.next()) {
                String matchId = rs.getString("matchId");
                list.add(matchId);
            }
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in getHomeMatchList --> " + e);
        }
        return list;
    }

    public MyAppError addHomeMatch(String matchId) {
        MyAppError error = new MyAppError();
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            stmt = db.connection.createStatement();
            String sql = "SELECT matchId FROM home_match_list WHERE matchId = '" + matchId + "'";
            rs = stmt.executeQuery(sql);
            if (rs.next()) {
                error.setERROR_TYPE(MyAppError.OTHERERROR);
                error.setErrorMessage("Duplicate content.");
                return error;
            }
            sql = "INSERT INTO home_match_list(matchId) VALUES(?)";
            ps = db.connection.prepareStatement(sql);
            int i = 1;
            ps.setString(i++, matchId);
            ps.execute();
            error.setErrorMessage("Successfully Inserted");
        } catch (Exception e) {
            error.setERROR_TYPE(MyAppError.DBERROR);
            error.setErrorMessage(e.getMessage());
            RingLogger.getConfigPortalLogger().error("Exception in addHomeMatch --> " + e);
        }
        return error;
    }

    public MyAppError deleteHomeMatch(String matchId) {
        MyAppError error = new MyAppError();
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            String sql = "DELETE FROM home_match_list WHERE matchId = ?";
            ps = db.connection.prepareStatement(sql);
            int i = 1;
            ps.setString(i++, matchId);
            ps.execute();
            error.setErrorMessage("Successfully Deleted");
        } catch (Exception e) {
            error.setERROR_TYPE(MyAppError.DBERROR);
            error.setErrorMessage(e.getMessage());
            RingLogger.getConfigPortalLogger().error("Exception in deleteHomeMatch --> " + e);
        }
        return error;
    }

    public MyAppError addChannelMatchMapped(String matchId, String[] channelIds) {
        RingLogger.getConfigPortalLogger().debug("[addChannelMatchMapped] matchId --> " + matchId + " channelIds --> " + new Gson().toJson(channelIds));
        MyAppError error = new MyAppError();
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            String query = "INSERT INTO channel_match_mapping (matchId, channelId) "
                    + " SELECT * FROM (SELECT ?,?) AS tmp  WHERE NOT EXISTS "
                    + " ( SELECT matchId, channelId FROM channel_match_mapping WHERE matchId = ? AND  channelId = ? ) "
                    + " LIMIT 1;";
            ps = db.connection.prepareStatement(query);  // create the mysql insert preparedstatement  
            int i = 1;
            ps.setString(1, matchId);
            ps.setString(3, matchId);
            for (String channelId : channelIds) {
                ps.setString(2, channelId);
                ps.setString(4, channelId);
                ps.addBatch();
                i++;
                if ((i + 1) % 1000 == 0) {
                    ps.executeBatch(); // Execute every 1000 items.
                }
            }
            ps.executeBatch(); // execute the preparedstatement
        } catch (SQLException e) {
            error.setERROR_TYPE(MyAppError.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            RingLogger.getConfigPortalLogger().debug("" + e);
        } catch (Exception e) {
            error.setERROR_TYPE(MyAppError.DBERROR);
            RingLogger.getConfigPortalLogger().error("Error in addCountriesInfo --> " + e);
        } finally {
            close();
        }
        return error;
    }

    public MyAppError deleteChannelMatchMapped(String matchId, String[] channelIds) {
        RingLogger.getConfigPortalLogger().debug("[deleteChannelMatchMapped] matchId --> " + matchId + " channelIds --> " + new Gson().toJson(channelIds));
        MyAppError error = new MyAppError();
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            String query = "DELETE FROM channel_match_mapping WHERE matchId = ? ";
            if (channelIds != null && channelIds.length > 0) {
                query += " AND channelId = ?;";
            }
            ps = db.connection.prepareStatement(query);  // create the mysql insert preparedstatement
            if (channelIds != null && channelIds.length > 0) {
                int i = 1;
                for (String channelId : channelIds) {
                    ps.setString(1, matchId);
                    ps.setString(2, channelId);
                    ps.addBatch();
                    i++;
                    if ((i + 1) % 1000 == 0) {
                        ps.executeBatch(); // Execute every 1000 items.
                    }
                }
                ps.executeBatch(); // execute the preparedstatement
            } else {
                ps.setString(1, matchId);
                ps.executeUpdate();
            }
        } catch (SQLException e) {
            error.setERROR_TYPE(MyAppError.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            RingLogger.getConfigPortalLogger().debug("" + e);
        } catch (Exception e) {
            error.setERROR_TYPE(MyAppError.DBERROR);
            RingLogger.getConfigPortalLogger().error("Error in deleteChannelMatchMapped --> " + e);
        } finally {
            close();
        }
        return error;
    }

    public MyAppError deleteChannelMatchMapped(String[] matchIds, String channelId) {
        RingLogger.getConfigPortalLogger().debug("[deleteChannelMatchMapped] matchId --> " + new Gson().toJson(matchIds) + " channelId --> " + channelId);
        MyAppError error = new MyAppError();
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            String query = "DELETE FROM channel_match_mapping WHERE matchId = ? ";
            if (channelId != null && channelId.length() > 0) {
                query += " AND channelId = ?;";
            }
            ps = db.connection.prepareStatement(query);  // create the mysql insert preparedstatement
            if (channelId != null && channelId.length() > 0) {
                ps.setString(2, channelId);
            }
            if (matchIds != null && matchIds.length > 0) {
                int i = 1;
                for (String matchId : matchIds) {
                    ps.setString(1, matchId);
                    ps.addBatch();
                    i++;
                    if ((i + 1) % 1000 == 0) {
                        ps.executeBatch(); // Execute every 1000 items.
                    }
                }
                ps.executeBatch(); // execute the preparedstatement
            }
            RingLogger.getConfigPortalLogger().debug("[deleteChannelMatchMapped] sql --> " + query);
        } catch (SQLException e) {
            error.setERROR_TYPE(MyAppError.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            RingLogger.getConfigPortalLogger().debug("" + e);
        } catch (Exception e) {
            error.setERROR_TYPE(MyAppError.DBERROR);
            RingLogger.getConfigPortalLogger().error("Error in deleteChannelMatchMapped --> " + e);
        } finally {
            close();
        }
        return error;
    }

    public List<String> getChannelMappedInfoByMatchId(String matchId) {
        List<String> channelIds = new ArrayList<>();
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            String sql = "SELECT channelId FROM channel_match_mapping WHERE matchId = ?;";
            if (matchId != null && matchId.length() > 0) {
                ps = db.connection.prepareStatement(sql);
                ps.setString(1, matchId);
                rs = ps.executeQuery();
                while (rs.next()) {
                    channelIds.add(rs.getString("channelId"));
                }
                RingLogger.getConfigPortalLogger().debug(sql + "::" + channelIds.size());
            }
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().debug("getChannelMappedInfoByMatchId List Exception...", e);
        } finally {
            close();
        }
        return channelIds;
    }

    public Map<String, List<String>> getMatchChannelMappedInfo() {
        Map<String, List<String>> matchChannelMappedInfo = new HashMap<>();
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            String sql = "SELECT matchId, channelId FROM channel_match_mapping ";
            ps = db.connection.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                if (matchChannelMappedInfo.containsKey(rs.getString("matchId"))) {
                    matchChannelMappedInfo.get(rs.getString("matchId")).add(rs.getString("channelId"));
                } else {
                    List<String> channelIds = new ArrayList<>();
                    channelIds.add(rs.getString("channelId"));
                    matchChannelMappedInfo.put(rs.getString("matchId"), channelIds);
                }
            }
            RingLogger.getConfigPortalLogger().debug(sql + "::" + matchChannelMappedInfo.size());
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().debug("getMatchChannelMappedInfo List Exception...", e);
        } finally {
            close();
        }
        return matchChannelMappedInfo;
    }
}
