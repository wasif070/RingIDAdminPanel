/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.dao.adminAuth;

import com.ringid.admin.BaseDAO;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.dto.adminAuth.UserFeatureMappingDTO;
import com.ringid.admin.dto.adminAuth.FeatureDTO;
import com.ringid.admin.utils.Constants;
import com.ringid.admin.utils.MyAppError;
import authdbconnector.DBConnection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Rabby
 */
public class FeatureRoleDAO extends BaseDAO {

    private static boolean hasRootPermission = false;

    Logger logger = RingLogger.getConfigPortalLogger();

    public int getPermissionLevel(int featureId, int roleId) {
        int permissionLevel = 0;
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            stmt = db.connection.createStatement();
            String sql = "SELECT permission_level FROM feature_role_mapping WHERE feature_id = " + featureId + " and role_id = " + roleId + ";";
            rs = stmt.executeQuery(sql);
            if (rs.next()) {
                permissionLevel = rs.getInt("permission_level");
            }
        } catch (SQLException e) {
            logger.error("getPermissionLevel SQLException : " + e);
        } catch (Exception e) {
            logger.error("getPermissionLevel Exception : " + e);
        } finally {
            close();
        }
        return permissionLevel;
    }

    public HashMap<String, UserFeatureMappingDTO> getUserFeatureMappingByRoleId(int roleId) {
        HashMap<String, UserFeatureMappingDTO> featureMappingList = new HashMap<>();
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            stmt = db.connection.createStatement();
            String sql;
            if (roleId == Constants.SUPER_ADMIN) {
                sql = "SELECT id fr_id, feature_name fr_name, operation_type op_code FROM features_info;";
            } else {
                sql = "SELECT fr.id fr_id, fr.feature_name fr_name, fm.permission_level op_code FROM features_info fr INNER JOIN feature_role_mapping fm ON fr.id = fm.feature_id WHERE fm.role_id = " + roleId + ";";
            }

            rs = stmt.executeQuery(sql);
            while (rs.next()) {
                UserFeatureMappingDTO dto = new UserFeatureMappingDTO();
                dto.setFeatureId(rs.getInt("fr_id"));
                dto.setFeatureName(rs.getString("fr_name"));
                int permissionLevel = rs.getInt("op_code");
                dto.setPermissionLevel(permissionLevel);
                if ((permissionLevel & 1) == 1) {
                    dto.setHasViewPermission(true);
                }
                if (((permissionLevel >> 1) & 1) == 1) {
                    dto.setHasAddPermission(true);
                }
                if (((permissionLevel >> 2) & 1) == 1) {
                    dto.setHasModifyPermission(true);
                }
                if (((permissionLevel >> 3) & 1) == 1) {
                    dto.setHasDeletePermission(true);
                }
                dto.setHasChildPermission(hasPermissionOfAnyChild(dto.getFeatureId(), roleId));
                featureMappingList.put(dto.getFeatureName(), dto);

            }
        } catch (SQLException e) {
            logger.error("getPermissionLevel SQLException : " + e);
        } catch (Exception e) {
            logger.error("getPermissionLevel Exception : " + e);
        } finally {
            close();
        }
        return featureMappingList;
    }

    public MyAppError updateFeaturePermissionForRole(ArrayList<FeatureDTO> featureList, int roleId) {
        MyAppError error = new MyAppError();
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            stmt = db.connection.createStatement();
            String sql;
            for (FeatureDTO dto : featureList) {
                sql = "SELECT permission_level FROM feature_role_mapping WHERE feature_id = " + dto.getId() + " and role_id = " + roleId + ";";
                rs = stmt.executeQuery(sql);
                if (rs.next()) {
                    sql = "UPDATE feature_role_mapping SET permission_level = " + dto.getPermissionLevel() + " WHERE feature_id = " + dto.getId() + " and role_id = " + roleId + ";";
                    stmt.executeUpdate(sql);
                } else {
                    sql = "INSERT INTO feature_role_mapping(feature_id, role_id, permission_level) VALUES (?,?,?);";
                    ps = db.connection.prepareStatement(sql);
                    int i = 1;
                    ps.setInt(i++, dto.getId());
                    ps.setInt(i++, roleId);
                    ps.setInt(i++, dto.getPermissionLevel());
                    ps.execute();
                }
            }

        } catch (SQLException e) {
            error.setERROR_TYPE(MyAppError.DBERROR);
            error.setErrorMessage(e.getMessage());
            logger.error(e);
        } catch (Exception e) {
            error.setERROR_TYPE(MyAppError.OTHERERROR);
            error.setErrorMessage(e.getMessage());
            logger.error(e);
        } finally {
            close();
        }
        return error;
    }

    public boolean hasPermissionOfAnyChild(int parentId, int roleId) {
        hasRootPermission = false;
        featureChildTravarse(parentId, roleId);
        return hasRootPermission;
    }

    private void featureChildTravarse(int parentId, int roleId) {
        if (hasRootPermission == true) {
            return;
        }
        int permissionLevel = 0;
        ArrayList<FeatureDTO> childFeatureList = new FeatureDAO().getFeatureListOfParent(parentId);
        DBConnection db = null;
        Statement stmt = null;
        ResultSet rs = null;
        for (FeatureDTO dto : childFeatureList) {
            try {
                db = authdbconnector.DBConnector.getInstance().makeConnection();
                stmt = db.connection.createStatement();

                String sql = "SELECT permission_level FROM feature_role_mapping WHERE feature_id = " + dto.getId() + " and role_id = " + roleId + ";";
                rs = stmt.executeQuery(sql);
                if (rs.next()) {
                    permissionLevel = rs.getInt("permission_level");
                }
            } catch (SQLException e) {
                logger.error("getPermissionLevel SQLException : " + e);
            } catch (Exception e) {
                logger.error("getPermissionLevel Exception : " + e);
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                } catch (Exception e) {
                }

                try {
                    if (stmt != null) {
                        stmt.close();
                    }
                } catch (Exception e) {
                }

                try {
                    if (db != null && db.connection != null) {
                        authdbconnector.DBConnector.getInstance().freeConnection(db);
                    }
                } catch (Exception e) {
                }
            }

            if (permissionLevel != 0) {
                hasRootPermission = true;
            }
            featureChildTravarse(dto.getId(), roleId);
        }

    }
}
