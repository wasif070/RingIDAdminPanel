/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.dao.adminAuth;

import com.ringid.admin.BaseDAO;
import com.ringid.admin.dto.adminAuth.FeatureDTO;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.utils.MyAppError;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Rabby
 */
public class FeatureDAO extends BaseDAO {

    public ArrayList<FeatureDTO> getFeatureList(FeatureDTO sdto) {
        ArrayList<FeatureDTO> featureLsit = new ArrayList<>();
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            stmt = db.connection.createStatement();
            String sql = "SELECT id, feature_name, feature_code, parent_id, project_name, operation_type FROM features_info WHERE  project_name = 'configportal' ORDER BY id;";
            rs = stmt.executeQuery(sql);
            while (rs.next()) {
                FeatureDTO featureDTO = new FeatureDTO();
                featureDTO.setId(rs.getInt("id"));
                featureDTO.setFeatureName(rs.getString("feature_name"));
                featureDTO.setFeatureCode(rs.getString("feature_code"));
                featureDTO.setParentId(rs.getInt("parent_id"));
                featureDTO.setProjectName(rs.getString("project_name"));
                featureDTO.setOperationType(rs.getInt("operation_type"));
                featureLsit.add(featureDTO);
            }
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("[getFeatureList] Execption: " + e.getMessage());
        } finally {
            close();
        }
        RingLogger.getConfigPortalLogger().info("[getFeatureList] featureList size--> " + featureLsit.size());
        return featureLsit;
    }

    public ArrayList<FeatureDTO> getFeatureListOfParent(int parentId) {
//        RingLogger.getConfigPortalLogger().debug("[getFeatureListOfParent] parentId --> " + parentId);
        ArrayList<FeatureDTO> featureLsit = new ArrayList<>();
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            stmt = db.connection.createStatement();
            String sql = "SELECT id, feature_name, feature_code, parent_id, project_name, operation_type FROM features_info WHERE parent_id = '" + parentId + "' and project_name = 'configportal' ORDER BY id;";
            rs = stmt.executeQuery(sql);
            while (rs.next()) {
                FeatureDTO featureDTO = new FeatureDTO();
                featureDTO.setId(rs.getInt("id"));
                featureDTO.setFeatureName(rs.getString("feature_name"));
                featureDTO.setFeatureCode(rs.getString("feature_code"));
                featureDTO.setParentId(rs.getInt("parent_id"));
                featureDTO.setProjectName(rs.getString("project_name"));
                featureDTO.setOperationType(rs.getInt("operation_type"));
                featureLsit.add(featureDTO);
            }
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("[getFeatureListOfParent] Execption: " + e.getMessage());
        } finally {
            close();
        }
//        RingLogger.getConfigPortalLogger().info("[getFeatureListOfParent] featureList size--> " + featureLsit.size());
        return featureLsit;
    }

    public FeatureDTO getFeatureById(int featureId) {
        RingLogger.getConfigPortalLogger().info("[getFeatureById] featureId --> " + featureId);
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            stmt = db.connection.createStatement();
            String sql = "SELECT id, feature_name, feature_code, parent_id, project_name, operation_type FROM features_info WHERE id = " + featureId + " and project_name = 'configportal';";
            rs = stmt.executeQuery(sql);
            if (rs.next()) {
                FeatureDTO featureDTO = new FeatureDTO();
                featureDTO.setId(rs.getInt("id"));
                featureDTO.setFeatureName(rs.getString("feature_name"));
                featureDTO.setFeatureCode(rs.getString("feature_code"));
                featureDTO.setParentId(rs.getInt("parent_id"));
                featureDTO.setProjectName(rs.getString("project_name"));
                featureDTO.setOperationType(rs.getInt("operation_type"));
                return featureDTO;
            }
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().debug("[getFeatureById] Exception: " + e.getMessage());
        } finally {
            close();
        }
        return null;
    }

    public FeatureDTO getFeatureByName(String featureName) {
        RingLogger.getConfigPortalLogger().info("[getFeatureByName] featureName --> " + featureName);
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            stmt = db.connection.createStatement();
            String sql = "SELECT id, feature_name, feature_code, parent_id, project_name, operation_type FROM features_info WHERE feature_name = '" + featureName + "' and project_name = 'configportal';";
            rs = stmt.executeQuery(sql);
            if (rs.next()) {
                FeatureDTO featureDTO = new FeatureDTO();
                featureDTO.setId(rs.getInt("id"));
                featureDTO.setFeatureName(rs.getString("feature_name"));
                featureDTO.setFeatureCode(rs.getString("feature_code"));
                featureDTO.setParentId(rs.getInt("parent_id"));
                featureDTO.setProjectName(rs.getString("project_name"));
                featureDTO.setOperationType(rs.getInt("operation_type"));
                return featureDTO;
            }
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().debug("[getFeatureById] Exception: " + e.getMessage());
        } finally {
            close();
        }
        return null;
    }

    public MyAppError addFeature(FeatureDTO sdto) {
        MyAppError error = new MyAppError();
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
//            String sql = "SELECT id FROM features_info WHERE feature_name = ?";
//            ps = db.connection.prepareStatement(sql);
//            ps.setString(1, sdto.getFeatureName());
//            rs = ps.executeQuery();
//            if (rs.next()) {
//                error.setERROR_TYPE(MyAppError.OTHERERROR);
//                error.setErrorMessage("Duplicate content.");
//                return error;
//            }
            String sql = "INSERT INTO features_info(feature_name, feature_code, parent_id, project_name, operation_type) VALUES (?,?,?,?,?);";
            ps = db.connection.prepareStatement(sql);
            int i = 1;
            ps.setString(i++, sdto.getFeatureName());
            ps.setString(i++, sdto.getFeatureCode());
            ps.setInt(i++, sdto.getParentId());
            ps.setString(i++, "configportal");
            ps.setInt(i++, sdto.getOperationType());
            ps.execute();
        } catch (SQLException e) {
            error.setERROR_TYPE(MyAppError.DBERROR);
            error.setErrorMessage(e.getMessage());
            RingLogger.getConfigPortalLogger().debug("addFeature SQL exception --> " + e.getMessage());
        } catch (Exception e) {
            error.setERROR_TYPE(MyAppError.OTHERERROR);
            error.setErrorMessage(e.getMessage());
            RingLogger.getConfigPortalLogger().debug("addFeature Exception --> " + e.getMessage());
        } finally {
            close();
        }
        return error;
    }

    public MyAppError updateFeature(FeatureDTO sdto) {
        MyAppError error = new MyAppError();
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
//            String sql = "SELECT id FROM features_info WHERE feature_name = ?";
//            ps = db.connection.prepareStatement(sql);
//            ps.setString(1, sdto.getFeatureName());
//            rs = ps.executeQuery();
//            if (rs.next()) {
//                error.setERROR_TYPE(MyAppError.OTHERERROR);
//                error.setErrorMessage("Duplicate content.");
//                return error;
//            }
            String sql = "UPDATE features_info SET feature_name = ?, feature_code = ?, parent_id = ?, operation_type = ? WHERE id = ?;";
            ps = db.connection.prepareStatement(sql);
            int i = 1;
            ps.setString(i++, sdto.getFeatureName());
            ps.setString(i++, sdto.getFeatureCode());
            ps.setInt(i++, sdto.getParentId());
            ps.setInt(i++, sdto.getOperationType());
            ps.setInt(i++, sdto.getId());
            ps.execute();
        } catch (SQLException e) {
            error.setERROR_TYPE(MyAppError.DBERROR);
            error.setErrorMessage(e.getMessage());
            RingLogger.getConfigPortalLogger().debug("updateFeature SQL Exception --> " + e.getMessage());
        } catch (Exception e) {
            error.setERROR_TYPE(MyAppError.OTHERERROR);
            error.setErrorMessage(e.getMessage());
            RingLogger.getConfigPortalLogger().debug("updateFeature Exception --> " + e.getMessage());
        } finally {
            close();
        }
        return error;
    }

    public MyAppError deleteFeature(int featureId) {
        MyAppError error = new MyAppError();
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            stmt = db.connection.createStatement();
            String sql = "DELETE FROM features_info WHERE id = " + featureId + ";";
            stmt.executeUpdate(sql);
        } catch (SQLException e) {
            error.setERROR_TYPE(MyAppError.DBERROR);
            error.setErrorMessage(e.getMessage());
            RingLogger.getConfigPortalLogger().debug("deleteFeature SQL Exception --> " + e.getMessage());
        } catch (Exception e) {
            error.setERROR_TYPE(MyAppError.OTHERERROR);
            error.setErrorMessage(e.getMessage());
            RingLogger.getConfigPortalLogger().debug("deleteFeature Execption --> " + e.getMessage());
        } finally {
            close();
        }
        return error;
    }
}
