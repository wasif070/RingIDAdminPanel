package com.ringid.admin.dao.adminAuth;

import com.ringid.admin.utils.MyAppError;
import org.apache.logging.log4j.Logger;
import com.ringid.admin.BaseDAO;
import com.ringid.admin.dto.adminAuth.LoginDTO;
import com.ringid.admin.utils.log.RingLogger;

public class LoginDAO extends BaseDAO {

    private static Logger logger = RingLogger.getConfigPortalLogger();

    public LoginDAO() {
    }

    public MyAppError checkLoginInfo(LoginDTO loginDTO) {
        MyAppError errors = new MyAppError();
        String userName = "";
        String password = "";
//        stmt = null;
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            stmt = db.connection.createStatement();
            String sql;
            sql = "SELECT id,user_name,user_password, permission_level FROM admin_users WHERE user_name  = '" + loginDTO.getUserName() + "' AND user_password = '" + loginDTO.getPassword() + "'";
            rs = stmt.executeQuery(sql);
            if (rs.next()) {
                errors.setId(rs.getInt("id"));
                loginDTO.setUserId(rs.getInt("id"));
                loginDTO.setPermissionLevel(rs.getInt("permission_level"));
            } else {
                errors.setERROR_TYPE(MyAppError.VALIDATIONERROR);
            }

        } catch (Exception e) {
            logger.error("checkLoginInfo error" + e);
            errors.setERROR_TYPE(MyAppError.DBERROR);
            errors.setErrorMessage(e.getMessage() + " for username: " + userName + " & password :" + password);
        } finally {
            close();
        }
        return errors;
    }
}
