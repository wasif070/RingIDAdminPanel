/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.dao.adminAuth;

import com.ringid.admin.BaseDAO;
import com.ringid.admin.dto.adminAuth.UserRoleDTO;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.utils.MyAppError;
import java.sql.SQLException;
import java.util.ArrayList;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Rabby
 */
public class UserRoleDAO extends BaseDAO {

    static Logger logger = RingLogger.getConfigPortalLogger();

    public ArrayList<UserRoleDTO> getUserRoleList(UserRoleDTO sdto) {
        ArrayList<UserRoleDTO> userRoleDTOList = new ArrayList<>();
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            stmt = db.connection.createStatement();
            String sql = "SELECT id, name FROM users_role;";
            rs = stmt.executeQuery(sql);
            while (rs.next()) {
                UserRoleDTO dto = new UserRoleDTO();
                dto.setId(rs.getInt("id"));
                dto.setName(rs.getString("name"));
                userRoleDTOList.add(dto);
            }
            logger.debug(sql + "::" + userRoleDTOList.size());
        } catch (Exception e) {
            logger.debug("UserRole List Exception...", e);
        } finally {
            close();
        }
        return userRoleDTOList;
    }

    public UserRoleDTO getUserRoleById(int id) {
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            stmt = db.connection.createStatement();
            String sql = "SELECT id, name FROM users_role WHERE id = " + id + " ;";
            rs = stmt.executeQuery(sql);
            while (rs.next()) {
                UserRoleDTO dto = new UserRoleDTO();
                dto.setId(rs.getInt("id"));
                dto.setName(rs.getString("name"));
                return dto;
            }
        } catch (Exception e) {
            logger.debug("UserRole Exception...", e);
        } finally {
            close();
        }
        return null;
    }

    public MyAppError addUserRole(UserRoleDTO dto) {
        MyAppError error = new MyAppError();
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            stmt = db.connection.createStatement();
            String query = "INSERT INTO users_role(name) VALUES (?);";
            ps = db.connection.prepareStatement(query);
            int i = 1;
            ps.setString(i++, dto.getName());
            ps.execute();
            ps.close();
        } catch (SQLException e) {
            error.setERROR_TYPE(MyAppError.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            logger.debug("Single addUserRole SQL Exception --> " + e);
        } catch (Exception e) {
            error.setERROR_TYPE(MyAppError.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            logger.debug("Single addUserRole Exception --> " + e);
        } finally {
            close();
        }
        return error;
    }

    public MyAppError updateUserRole(UserRoleDTO dto) {
        MyAppError error = new MyAppError();
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            String query = "UPDATE users_role SET name = ? WHERE id = ?;";
            ps = db.connection.prepareStatement(query);
            int i = 1;
            ps.setString(i++, dto.getName());
            ps.setInt(i++, dto.getId());
            ps.execute();
        } catch (SQLException e) {
            error.setERROR_TYPE(MyAppError.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            logger.debug("updateUserRole SQL Exception  --> " + e);
        } catch (Exception e) {
            error.setERROR_TYPE(MyAppError.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            logger.debug("updateUserRole Exception --> " + e);
        } finally {
            close();
        }
        return error;
    }

    public MyAppError deleteUserRole(int id) {
        MyAppError error = new MyAppError();
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            stmt = db.connection.createStatement();
            String sql = "DELETE FROM users_role WHERE id = " + id + ";";
            int a = stmt.executeUpdate(sql);
        } catch (SQLException e) {
            error.setERROR_TYPE(MyAppError.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            logger.debug("deleteUserRole SQL Exception --> " + e);
        } catch (Exception e) {
            error.setERROR_TYPE(MyAppError.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            logger.debug("deleteUserRole Exception --> " + e);
        } finally {
            close();
        }
        return error;
    }

}
