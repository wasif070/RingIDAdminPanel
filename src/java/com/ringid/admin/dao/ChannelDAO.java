/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.dao;

/**
 *
 * @author Rabby
 */
public class ChannelDAO {

    private static ChannelDAO instance = null;

    private ChannelDAO() {
    }

    public static ChannelDAO getInstance() {
        if (instance == null) {
            createInstance();
        }
        return instance;
    }

    private synchronized static void createInstance() {
        if (instance == null) {
            instance = new ChannelDAO();
        }
    }
}
