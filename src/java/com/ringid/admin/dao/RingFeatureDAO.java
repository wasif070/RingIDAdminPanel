/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.dao;

import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.dto.RingFeatureDTO;
import com.ringid.admin.utils.Constants;
import com.ringid.admin.utils.MyAppError;
import authdbconnector.DBConnection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javafx.util.Pair;

/**
 *
 * @author Rabby
 */
public class RingFeatureDAO {

    private static RingFeatureDAO instance = null;

    private RingFeatureDAO() {
    }

    public static RingFeatureDAO getInstance() {
        if (instance == null) {
            createInstance();
        }
        return instance;
    }

    private synchronized static void createInstance() {
        if (instance == null) {
            instance = new RingFeatureDAO();
        }
    }

    public List<RingFeatureDTO> getFeatureList(RingFeatureDTO sdto) {
        List<RingFeatureDTO> featureDTOs = new ArrayList<>();
        DBConnection db = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            String sql = "SELECT id, featureName FROM feature ORDER BY id";
            ps = db.connection.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                RingFeatureDTO dto = new RingFeatureDTO();
                dto.setId(rs.getInt("id"));
                dto.setFeatureName(rs.getString("featureName"));
                if (sdto.getSearchText() != null && sdto.getSearchText().length() > 0) {
                    if (!(dto.getFeatureName().toLowerCase().contains(sdto.getSearchText().trim().toLowerCase()))) {
                        continue;
                    }
                }
                featureDTOs.add(dto);
            }
            if (sdto.getSortType() == Constants.ASC_SORT) {
                Collections.sort(featureDTOs, new RingFeatureDTO.CompASC());
            } else {
                Collections.sort(featureDTOs, new RingFeatureDTO.CompDSC());
            }
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in getFeatureList() -> " + e);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (db != null) {
                    authdbconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
        }
        return featureDTOs;
    }

    public RingFeatureDTO getFeatureById(int id) {
        RingFeatureDTO featureDTO = new RingFeatureDTO();
        DBConnection db = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            String sql = "SELECT id, featureName FROM feature WHERE id = ?";
            ps = db.connection.prepareStatement(sql);
            int index = 1;
            ps.setInt(index++, id);
            rs = ps.executeQuery();
            if (rs.next()) {
                featureDTO.setId(id);
                featureDTO.setFeatureName(rs.getString("featureName"));
            }
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in getFeatureById() -> " + e);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (db != null) {
                    authdbconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
        }
        return featureDTO;
    }

    public MyAppError addFeature(RingFeatureDTO sdto) {
        MyAppError error = new MyAppError();
        DBConnection db = null;
        PreparedStatement ps = null;
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            String sql = "INSERT INTO feature(featureName) VALUES(?)";
            ps = db.connection.prepareStatement(sql);
            int index = 1;
            ps.setString(index++, sdto.getFeatureName());
            ps.execute();
        } catch (Exception e) {
            error.setERROR_TYPE(MyAppError.DBERROR);
            error.setErrorMessage("Insertion failed. (Database Error)");
            RingLogger.getConfigPortalLogger().error("Exception in addFeature -> " + e);
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (db != null) {
                    authdbconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError updateFeature(RingFeatureDTO sdto) {
        MyAppError error = new MyAppError();
        DBConnection db = null;
        PreparedStatement ps = null;
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            String sql = "UPDATE feature SET featureName = ? WHERE id = ?";
            ps = db.connection.prepareStatement(sql);
            int index = 1;
            ps.setString(index++, sdto.getFeatureName());
            ps.setInt(index++, sdto.getId());
            ps.execute();
        } catch (Exception e) {
            error.setERROR_TYPE(MyAppError.DBERROR);
            error.setErrorMessage("Update Failed. (Database Error)");
            RingLogger.getConfigPortalLogger().error("Exception in updateFeature() -> " + e);
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (db != null) {
                    authdbconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError deleteFeature(int id) {
        MyAppError error = new MyAppError();
        DBConnection db = null;
        PreparedStatement ps = null;
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            String sql = "DELETE FROM feature WHERE id = ?";
            ps = db.connection.prepareStatement(sql);
            int index = 1;
            ps.setInt(index++, id);
            ps.execute();
            deleteFeatureMappingByFeatureId(id);
        } catch (Exception e) {
            error.setERROR_TYPE(MyAppError.DBERROR);
            error.setErrorMessage("Delete Failed. (Database error)");
            RingLogger.getConfigPortalLogger().error("Exception in deleteFeature() -> " + e);
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (db != null) {
                    authdbconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public List<RingFeatureDTO> getFeatureMappingList(RingFeatureDTO sdto) {
        List<RingFeatureDTO> featureMappingDTOs = new ArrayList<>();
        DBConnection db = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            String sql = "SELECT featureId, appType, platform, minVersion, featureName FROM featuremapping JOIN feature WHERE featureId = id ORDER BY featureId;";
            ps = db.connection.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                RingFeatureDTO dto = new RingFeatureDTO();
                dto.setId(rs.getInt("featureId"));
                dto.setAppType(rs.getInt("appType"));
                dto.setPlatform(rs.getInt("platform"));
                dto.setMinVersion(rs.getInt("minVersion"));
                dto.setFeatureName(rs.getString("featureName"));
                dto.setPlatformName(Constants.DEVICE[dto.getPlatform()]);
                dto.setAppTypeName(Constants.APP_TYPE[dto.getAppType()]);
                featureMappingDTOs.add(dto);
            }
            Map<Pair<Integer, Integer>, RingFeatureDTO> featureMap = new HashMap<>();
            for (RingFeatureDTO dto : featureMappingDTOs) {
                Pair<Integer, Integer> pair = new Pair<>(dto.getId(), dto.getAppType());
                if (featureMap.containsKey(pair)) {
                    String platVersion = featureMap.get(pair).getPlatformMinVersionStr() + ", " + dto.getPlatformName() + "(" + dto.getMinVersion() + ")";
                    featureMap.get(pair).setPlatformMinVersionStr(platVersion);
                } else {
                    String platVersion = "" + dto.getPlatformName() + "(" + dto.getMinVersion() + ")";
                    dto.setPlatformMinVersionStr(platVersion);
                    featureMap.put(pair, dto);
                }
            }
            featureMappingDTOs = new ArrayList<>();
            for (Map.Entry<Pair<Integer, Integer>, RingFeatureDTO> entry : featureMap.entrySet()) {
                RingFeatureDTO featureDTO = new RingFeatureDTO();
                featureDTO.setId(entry.getKey().getKey());
                featureDTO.setAppType(entry.getKey().getValue());
                featureDTO.setAppTypeName(Constants.APP_TYPE[featureDTO.getAppType()]);
                featureDTO.setFeatureName(entry.getValue().getFeatureName());
                featureDTO.setPlatformMinVersionStr(entry.getValue().getPlatformMinVersionStr());
                if (sdto.getSearchText() != null && sdto.getSearchText().length() > 0) {
                    if (!(featureDTO.getFeatureName().toLowerCase().contains(sdto.getSearchText().trim().toLowerCase())
                            || featureDTO.getAppTypeName().toLowerCase().contains(sdto.getSearchText().trim().toLowerCase())
                            || featureDTO.getPlatformMinVersionStr().toLowerCase().contains(sdto.getSearchText().trim().toLowerCase()))) {
                        continue;
                    }
                }
                featureMappingDTOs.add(featureDTO);
            }
            if (sdto.getSortType() == Constants.ASC_SORT) {
                Collections.sort(featureMappingDTOs, new RingFeatureDTO.CompASC());
            } else {
                Collections.sort(featureMappingDTOs, new RingFeatureDTO.CompDSC());
            }
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in getFeatureMappingList() -> " + e);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (db != null) {
                    authdbconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
        }
        return featureMappingDTOs;
    }

    public List<RingFeatureDTO> getFeatureMappingByFeatureId(int featureId, int appType) {
        List<RingFeatureDTO> list = new ArrayList<>();
        DBConnection db = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            String sql = "SELECT featureId, appType, platform, minVersion, featureName FROM featuremapping JOIN feature WHERE featureId = id AND featureId = ? AND appType = ?;";
            ps = db.connection.prepareStatement(sql);
            int index = 1;
            ps.setInt(index++, featureId);
            ps.setInt(index++, appType);
            rs = ps.executeQuery();
            while (rs.next()) {
                RingFeatureDTO dto = new RingFeatureDTO();
                dto.setId(rs.getInt("featureId"));
                dto.setAppType(rs.getInt("appType"));
                dto.setPlatform(rs.getInt("platform"));
                dto.setMinVersion(rs.getInt("minVersion"));
                dto.setFeatureName(rs.getString("featureName"));
                dto.setPlatformName(Constants.DEVICE[dto.getPlatform()]);
                dto.setAppTypeName(Constants.APP_TYPE[dto.getAppType()]);
                list.add(dto);
            }
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in getFeatureMappingByFeatureId() -> " + e);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (db != null) {
                    authdbconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
        }
        return list;
    }

    public MyAppError addOrUpdateFeatureMapping(RingFeatureDTO sdto) {
        MyAppError error = new MyAppError();
        DBConnection db = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            String sql = "SELECT featureId FROM featuremapping WHERE featureId = ? AND appType = ? AND platform = ?;";
            ps = db.connection.prepareStatement(sql);
            int index = 1;
            ps.setInt(index++, sdto.getId());
            ps.setInt(index++, sdto.getAppType());
            ps.setInt(index++, sdto.getPlatform());
            rs = ps.executeQuery();
            if (rs.next()) {
                error = updateFeatureMapping(sdto);
                return error;
            }
            sql = "INSERT INTO featuremapping(featureId, appType, platform, minVersion) VALUES(?, ?, ?, ?)";
            ps = db.connection.prepareStatement(sql);
            index = 1;
            ps.setInt(index++, sdto.getId());
            ps.setInt(index++, sdto.getAppType());
            ps.setInt(index++, sdto.getPlatform());
            ps.setInt(index++, sdto.getMinVersion());
            ps.execute();
        } catch (Exception e) {
            error.setERROR_TYPE(MyAppError.DBERROR);
            error.setErrorMessage("Insertion failed.(Database error)");
            RingLogger.getConfigPortalLogger().error("Exception in addFeatureMapping() -> " + e);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (db != null) {
                    authdbconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError updateFeatureMapping(RingFeatureDTO sdto) {
        MyAppError error = new MyAppError();
        DBConnection db = null;
        PreparedStatement ps = null;
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            String sql = "UPDATE featuremapping SET minVersion = ? WHERE featureId = ? AND appType = ? AND platform = ?;";
            ps = db.connection.prepareStatement(sql);
            int index = 1;
            ps.setInt(index++, sdto.getMinVersion());
            ps.setInt(index++, sdto.getId());
            ps.setInt(index++, sdto.getAppType());
            ps.setInt(index++, sdto.getPlatform());
            ps.execute();
        } catch (Exception e) {
            error.setERROR_TYPE(MyAppError.DBERROR);
            error.setErrorMessage("Database Error");
            RingLogger.getConfigPortalLogger().error("Exception in updateFeatureMapping() -> " + e);
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (db != null) {
                    authdbconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError deleteFeatureMapping(RingFeatureDTO sdto) {
        MyAppError error = new MyAppError();
        DBConnection db = null;
        PreparedStatement ps = null;
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            String sql = "DELETE FROM featuremapping WHERE featureId = ? AND appType = ? AND platform = ?;";
            ps = db.connection.prepareStatement(sql);
            int index = 1;
            ps.setInt(index++, sdto.getId());
            ps.setInt(index++, sdto.getAppType());
            ps.setInt(index++, sdto.getPlatform());
            ps.execute();
        } catch (Exception e) {
            error.setERROR_TYPE(MyAppError.DBERROR);
            error.setErrorMessage("Database error");
            RingLogger.getConfigPortalLogger().error("Exception in deleteFeatureMapping() -> " + e);
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (db != null) {
                    authdbconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError deleteFeatureMappingById(int featureId, int appType) {
        MyAppError error = new MyAppError();
        DBConnection db = null;
        PreparedStatement ps = null;
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            String sql = "DELETE FROM featuremapping WHERE featureId = ? AND appType = ?;";
            ps = db.connection.prepareStatement(sql);
            int index = 1;
            ps.setInt(index++, featureId);
            ps.setInt(index++, appType);
            ps.execute();
        } catch (Exception e) {
            error.setERROR_TYPE(MyAppError.DBERROR);
            error.setErrorMessage("Database error");
            RingLogger.getConfigPortalLogger().error("Exception in deleteFeatureMapping() -> " + e);
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (db != null) {
                    authdbconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    private MyAppError deleteFeatureMappingByFeatureId(int featureId) {
        MyAppError error = new MyAppError();
        DBConnection db = null;
        PreparedStatement ps = null;
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            String sql = "DELETE FROM featuremapping WHERE featureId = ?";
            ps = db.connection.prepareStatement(sql);
            int index = 1;
            ps.setInt(index++, featureId);
            ps.execute();
        } catch (Exception e) {
            error.setERROR_TYPE(MyAppError.DBERROR);
            error.setErrorMessage("Database error");
            RingLogger.getConfigPortalLogger().error("Exception in deleteFeatureMappingByFeatureId() -> " + e);
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (db != null) {
                    authdbconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }
}
