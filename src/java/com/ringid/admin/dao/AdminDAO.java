/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.dao;

import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.utils.MyAppError;
import authdbconnector.DBConnection;
import com.ringid.admin.dto.AppUpdateUrlDTO;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import org.ringid.utilities.AppConstants;

/**
 *
 * @author Rabby
 */
public class AdminDAO {

    private static AdminDAO instance = null;

    private AdminDAO() {
    }

    public static AdminDAO getInstance() {
        if (instance == null) {
            createInstance();
        }
        return instance;
    }

    private synchronized static void createInstance() {
        if (instance == null) {
            instance = new AdminDAO();
        }
    }

    public List<AppUpdateUrlDTO> getAppUpdateUrlListInfo() {
        List<AppUpdateUrlDTO> appUpdateUrlDTOs = new ArrayList<>();
        DBConnection db = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        int i = 1;
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            String query = "SELECT id, platform, app_type, update_url, app_name FROM appupdateurls;";
            ps = db.connection.prepareStatement(query);
            rs = ps.executeQuery();
            while (rs.next()) {
                AppUpdateUrlDTO dto = new AppUpdateUrlDTO();
                dto.setId(rs.getInt("id"));
                dto.setPlatform(rs.getInt("platform"));
                dto.setAppType(rs.getInt("app_type"));
                dto.setUpdateUrl(rs.getString("update_url"));
                dto.setAppName(rs.getString("app_name"));
                if (dto.getPlatform() < AppConstants.DEVICES.length) {
                    dto.setPlatformStr(AppConstants.DEVICES[dto.getPlatform()]);
                } else {
                    dto.setPlatformStr("" + dto.getPlatform());
                }
                switch (dto.getAppType()) {
                    case AppConstants.APP_TYPES.FULL:
                        dto.setAppTypeStr("FULL");
                        break;
                    case AppConstants.APP_TYPES.LITE:
                        dto.setAppTypeStr("LITE");
                        break;
                    case AppConstants.APP_TYPES.LIVE:
                        dto.setAppTypeStr("LIVE");
                        break;
                    case AppConstants.APP_TYPES.NETCAFE:
                        dto.setAppTypeStr("NETCAFE");
                        break;
                    case AppConstants.APP_TYPES.MESSENGER:
                        dto.setAppTypeStr("MESSENGER");
                        break;
                    case AppConstants.APP_TYPES.RING_MARKET:
                        dto.setAppTypeStr("RING_MARKET");
                        break;
                    default:
                        dto.setAppTypeStr("" + dto.getAppType());
                        break;
                }
                appUpdateUrlDTOs.add(dto);
            }
        } catch (SQLException e) {
            RingLogger.getConfigPortalLogger().debug("getAppUpdateUrlListInfo SQL Exception --> " + e);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().debug("getAppUpdateUrlListInfo Exception --> " + e);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (db != null) {
                    authdbconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
        }
        return appUpdateUrlDTOs;
    }

    public MyAppError addAppUpdateUrlInfo(AppUpdateUrlDTO dto) {
        MyAppError error = new MyAppError();
        int id = 0;
        DBConnection db = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        int i = 1;
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            String query = "SELECT MAX(id) FROM appupdateurls;";

            ps = db.connection.prepareStatement(query);
            rs = ps.executeQuery();
            if (rs.next()) {
                id = rs.getInt(1) + 1;
            }

            query = "INSERT INTO appupdateurls (id, platform, app_type, update_url, app_name) values(?, ?, ?, ?, ?);";
            ps = db.connection.prepareStatement(query);
            ps.setInt(i++, id);
            ps.setInt(i++, dto.getPlatform());
            ps.setInt(i++, dto.getAppType());
            ps.setString(i++, dto.getUpdateUrl());
            ps.setString(i++, dto.getAppName());

            if (ps.executeUpdate() <= 0) {
                error.setERROR_TYPE(MyAppError.VALIDATIONERROR);
                error.setErrorMessage("Insert Failed");
            }
        } catch (SQLException e) {
            error.setERROR_TYPE(MyAppError.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            RingLogger.getConfigPortalLogger().debug("addAppUpdateUrlInfo SQL Exception --> " + e);
        } catch (Exception e) {
            error.setERROR_TYPE(MyAppError.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            RingLogger.getConfigPortalLogger().debug("addAppUpdateUrlInfo Exception --> " + e);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (db != null) {
                    authdbconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError updateAppUpdateUrlInfo(int id, String updateUrl) {
        MyAppError error = new MyAppError();
        DBConnection db = null;
        PreparedStatement ps = null;
        int i = 1;
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            String query = "UPDATE appupdateurls SET update_url = ? WHERE id = ?;";
            ps = db.connection.prepareStatement(query);
            ps.setString(i++, updateUrl);
            ps.setInt(i++, id);
            if (ps.executeUpdate() <= 0) {
                error.setERROR_TYPE(MyAppError.VALIDATIONERROR);
                error.setErrorMessage("Update Failed");
            }
        } catch (SQLException e) {
            error.setERROR_TYPE(MyAppError.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            RingLogger.getConfigPortalLogger().debug("updateAppUpdateUrlInfo SQL Exception --> " + e);
        } catch (Exception e) {
            error.setERROR_TYPE(MyAppError.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            RingLogger.getConfigPortalLogger().debug("updateAppUpdateUrlInfo Exception --> " + e);
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (db != null) {
                    authdbconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError deleteAppUpdateUrlInfo(int id) {
        MyAppError error = new MyAppError();
        DBConnection db = null;
        PreparedStatement ps = null;
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            String sql = "DELETE FROM appupdateurls WHERE id = ?;";
            ps = db.connection.prepareStatement(sql);
            ps.setInt(1, id);
            if (ps.executeUpdate() <= 0) {
                error.setERROR_TYPE(MyAppError.VALIDATIONERROR);
                error.setErrorMessage("Delete Failed");
            }
        } catch (SQLException e) {
            error.setERROR_TYPE(MyAppError.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            RingLogger.getConfigPortalLogger().debug("deleteAppUpdateUrlInfo[1] --> " + e);
        } catch (Exception e) {
            error.setERROR_TYPE(MyAppError.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            RingLogger.getConfigPortalLogger().debug("deleteAppUpdateUrlInfo --> " + e);
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (db != null) {
                    authdbconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public int addDeactivateReasonsInfo(String description) {
        int id = -1;
        DBConnection db = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            String query = "INSERT INTO deactiveReasons (description) values(?);";
            ps = db.connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, description);

            if (ps.executeUpdate() > 0) {
                rs = ps.getGeneratedKeys();
                if (rs.next()) {
                    id = rs.getInt(1);
                }
            }
            ps.close();
        } catch (SQLException e) {
            id = -1;
            RingLogger.getConfigPortalLogger().debug("addDeactivateReasonsInfo SQL Exception --> " + e);
        } catch (Exception e) {
            id = -1;
            RingLogger.getConfigPortalLogger().debug("addDeactivateReasonsInfo Exception --> " + e);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (db != null) {
                    authdbconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
        }
        return id;
    }

    public MyAppError deleteAllVirtualRingIdBeforeDate(long sendDate) {
        MyAppError error = new MyAppError();
        DBConnection db = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            String sql = "DELETE FROM virtualringidgenerator WHERE creationTime < ?;";
            RingLogger.getConfigPortalLogger().debug("[deleteAllVirtualRingIdBeforeDate] sql --> DELETE FROM virtualringidgenerator WHERE creationTime < '" + sendDate + "'");
            ps = db.connection.prepareStatement(sql);
            ps.setLong(1, sendDate);
            ps.execute();
        } catch (SQLException e) {
            error.setERROR_TYPE(MyAppError.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            RingLogger.getConfigPortalLogger().debug("deleteAllVirtualRingIdBeforeDate[1] --> " + e);
        } catch (Exception e) {
            error.setERROR_TYPE(MyAppError.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            RingLogger.getConfigPortalLogger().debug("deleteAllVirtualRingIdBeforeDate --> " + e);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (db != null) {
                    authdbconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public int getAllVirtualRingIdCountBeforeDate(long sendDate) {
        int result = 0;
        DBConnection db = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            String sql = "SELECT count(1) AS ringIdCount FROM virtualringidgenerator WHERE creationTime < ?";
            ps = db.connection.prepareStatement(sql);
            ps.setLong(1, sendDate);
            rs = ps.executeQuery();
            if (rs.next()) {
                result = rs.getInt("ringIdCount");
            }
            RingLogger.getConfigPortalLogger().debug("[getAllVirtualRingIdCountBeforeDate] sql --> SELECT count(1) AS ringIdCount FROM virtualringidgenerator WHERE creationTime < '" + sendDate + "' :: " + result);
        } catch (SQLException e) {
            result = -1;
            RingLogger.getConfigPortalLogger().debug("getAllVirtualRingIdCountBeforeDate[1] --> " + e);
        } catch (Exception e) {
            result = -1;
            RingLogger.getConfigPortalLogger().debug("getAllVirtualRingIdCountBeforeDate --> " + e);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (db != null) {
                    authdbconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
        }
        return result;
    }
}
