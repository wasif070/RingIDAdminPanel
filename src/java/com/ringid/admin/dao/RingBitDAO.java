/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.dao;

import com.ringid.admin.cassandra.CassandraDAO;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.dto.RingBitDTO;
import com.ringid.admin.utils.MyAppError;
import authdbconnector.DBConnection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.ringid.users.UserBasicInfoDTO;

/**
 *
 * @author mamun
 */
public class RingBitDAO {

    private static RingBitDAO instance = null;
    private final SimpleDateFormat SDF;

    private RingBitDAO() {
        SDF = new SimpleDateFormat("yyyy-MM-dd HH:mm");
    }

    public static RingBitDAO getInstance() {
        if (instance == null) {
            createInstance();
        }
        return instance;
    }

    private synchronized static void createInstance() {
        if (instance == null) {
            instance = new RingBitDAO();
        }
    }

    public RingBitDTO getRingBitSettingImageAndTextInfo() {
        RingBitDTO ringBitDTO = new RingBitDTO();
        DBConnection db = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            String sql = "SELECT id, name, settingsValue, dataType FROM ringbitsettings WHERE name = ? OR name = ?;";
            ps = db.connection.prepareStatement(sql);
            int i = 1;
            ps.setString(i++, "ringbit_mining_image");
            ps.setString(i++, "ringbit_mining_text");
            rs = ps.executeQuery();
            while (rs.next()) {
                if (rs.getString("name") != null && rs.getString("name").trim().equals("ringbit_mining_image")) {
                    ringBitDTO.setSettingValueImage(rs.getString("settingsValue"));
                    ringBitDTO.setRingBitImageId(rs.getLong("id"));
                    ringBitDTO.setDataType(rs.getInt("dataType"));
                    continue;
                }
                if (rs.getString("name") != null && rs.getString("name").trim().equals("ringbit_mining_text")) {
                    ringBitDTO.setSettingValueText(rs.getString("settingsValue"));
                    ringBitDTO.setRingBitTextId(rs.getLong("id"));
                    ringBitDTO.setDataType(rs.getInt("dataType"));
                }
            }
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().debug("RingBitDAO [getRingBitSettingsHashMap] List Exception...", e);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (db != null) {
                    authdbconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
        }
        return ringBitDTO;
    }

    public List<RingBitDTO> getRingBitLogList() {
        List<RingBitDTO> ringBitLogList = new ArrayList<>();
        DBConnection db = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        Statement stmt = null;
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            stmt = db.connection.createStatement();
            String sql = "SELECT userId, coinAmount, addTime, status FROM preorder_ringbit_log";
            rs = stmt.executeQuery(sql);
            while (rs.next()) {
                RingBitDTO dto = new RingBitDTO();
                dto.setUserId(rs.getLong("userId"));
                dto.setCoinAmount(rs.getDouble("coinAmount"));
                dto.setAddTime(rs.getLong("addTime"));
                dto.setStatus(rs.getInt("status"));
                UserBasicInfoDTO basicInfoDTO = CassandraDAO.getInstance().getUserBasicInfo(dto.getUserId());
                dto.setUserName(basicInfoDTO.getFirstName());
                dto.setRingId(basicInfoDTO.getUserIdentity());
                dto.setAddTimeStr(SDF.format(new Date(dto.getAddTime())));
                ringBitLogList.add(dto);
            }
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().debug("RingBitDAO [getRingBitLogList] List Exception...", e);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (db != null) {
                    authdbconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
        }
        return ringBitLogList;
    }

    public MyAppError addRingBitSettingInfo(RingBitDTO dto) {
        MyAppError error = new MyAppError();
        DBConnection db = null;
        PreparedStatement ps = null;
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            String query = "INSERT INTO ringbitsettings (name, settingsValue, dataType) VALUES (?, ?, ?);";
            ps = db.connection.prepareStatement(query);
            int i = 1;
            if (dto.getSettingValueImage() != null && dto.getSettingValueImage().length() > 0) {
                ps.setString(i++, "ringbit_mining_image");
                ps.setString(i++, dto.getSettingValueImage());
                ps.setInt(i++, dto.getDataType());
                ps.executeUpdate();
            }
            i = 1;
            if (dto.getSettingValueText() != null && dto.getSettingValueText().length() > 0) {
                ps.setString(i++, "ringbit_mining_text");
                ps.setString(i++, dto.getSettingValueText());
                ps.setInt(i++, dto.getDataType());
                ps.executeUpdate();
            }
        } catch (SQLException e) {
            error.setERROR_TYPE(MyAppError.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            RingLogger.getConfigPortalLogger().debug("addRingBitSettingInfo SQL Exception --> " + e);
        } catch (Exception e) {
            error.setERROR_TYPE(MyAppError.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            RingLogger.getConfigPortalLogger().debug("addRingBitSettingInfo Exception --> " + e);
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (db != null) {
                    authdbconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError updateRingBitSettingInfo(RingBitDTO dto) {
        MyAppError error = new MyAppError();
        DBConnection db = null;
        PreparedStatement ps = null;
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            String query = "UPDATE ringbitsettings SET settingsValue = ? WHERE id = ?;";
            ps = db.connection.prepareStatement(query);
            int i = 1;
            if (dto.getRingBitImageId() > 0) {
                ps.setString(i++, dto.getSettingValueImage());
                ps.setLong(i++, dto.getRingBitImageId());
                ps.executeUpdate();
            }
            i = 1;
            if (dto.getRingBitTextId() > 0) {
                ps.setString(i++, dto.getSettingValueText());
                ps.setLong(i++, dto.getRingBitTextId());
                ps.executeUpdate();
            }
        } catch (SQLException e) {
            error.setERROR_TYPE(MyAppError.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            RingLogger.getConfigPortalLogger().debug("updateRingBitSettingInfo SQL Exception --> " + e);
        } catch (Exception e) {
            error.setERROR_TYPE(MyAppError.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            RingLogger.getConfigPortalLogger().debug("updateRingBitSettingInfo Exception --> " + e);
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (db != null) {
                    authdbconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }
}
