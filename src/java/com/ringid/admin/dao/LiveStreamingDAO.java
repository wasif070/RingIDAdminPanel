/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.dao;

import com.ringid.admin.dto.live.LiveInfoDetailsDTO;
import com.ringid.admin.dto.live.LiveInfoDTO;
import com.ringid.admin.dto.live.LiveSummaryDTO;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.actions.ringId.celebritySchedule.LiveToChannelDTO;
import com.ringid.admin.dto.SpecialRoomDTO;
import com.ringid.admin.dto.StreamingCodecSettingsDTO;
import com.ringid.admin.utils.MyAppError;
import authdbconnector.DBConnection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import org.ringid.livestream.StreamingDTO;

/**
 *
 * @author Rabby
 */
public class LiveStreamingDAO {

    private static LiveStreamingDAO instance = null;
    private SimpleDateFormat sdf = null;

    private LiveStreamingDAO() {
        sdf = new SimpleDateFormat("yyyy-MM-dd");
    }

    public static LiveStreamingDAO getInstance() {
        if (instance == null) {
            createInstance();
        }
        return instance;
    }

    private synchronized static void createInstance() {
        if (instance == null) {
            instance = new LiveStreamingDAO();
        }
    }

    public MyAppError updateManualRecordedLive(UUID streamId, StreamingDTO stream) {
        MyAppError error = new MyAppError();
        DBConnection db = null;
        PreparedStatement ps = null;
        int i = 1;
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            String query = "INSERT INTO manual_recorded_live (streamId, stream) VALUES(?, ?) ON DUPLICATE KEY UPDATE stream = ?;";
            ps = db.connection.prepareStatement(query);
            ps.setString(i++, streamId.toString());
            ps.setObject(i++, stream);
            ps.setObject(i++, stream);
            ps.execute();
        } catch (SQLException e) {
            error.setERROR_TYPE(MyAppError.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            RingLogger.getConfigPortalLogger().debug("updateManualRecordedLive SQL Exception --> " + e);
        } catch (Exception e) {
            error.setERROR_TYPE(MyAppError.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            RingLogger.getConfigPortalLogger().debug("updateManualRecordedLive Exception --> " + e);
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (db != null) {
                    authdbconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public ArrayList<StreamingCodecSettingsDTO> getStreamingCodecSettingsList() {
        ArrayList<StreamingCodecSettingsDTO> list = new ArrayList<>();
        DBConnection db = null;
        Statement stmt = null;
        ResultSet rs = null;
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            stmt = db.connection.createStatement();
            String sql = "SELECT streamType, audioCodec FROM streamingcodecsettings";
            rs = stmt.executeQuery(sql);
            while (rs.next()) {
                StreamingCodecSettingsDTO dto = new StreamingCodecSettingsDTO();
                dto.setStreamType(rs.getInt("streamType"));
                dto.setAudioCodec(rs.getInt("audioCodec"));
                list.add(dto);
            }
            RingLogger.getConfigPortalLogger().debug(sql + "::" + list.size());
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().debug("getStreamingCodecSettingsList List Exception...", e);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (db != null) {
                    authdbconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
        }
        return list;
    }

    public StreamingCodecSettingsDTO getStreamingCodecSettingsByType(int type) {
        StreamingCodecSettingsDTO dto = new StreamingCodecSettingsDTO();
        DBConnection db = null;
        Statement stmt = null;
        ResultSet rs = null;
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            stmt = db.connection.createStatement();
            String sql = "SELECT streamType, audioCodec FROM streamingcodecsettings WHERE streamType = " + type;
            rs = stmt.executeQuery(sql);
            if (rs.next()) {
                dto = new StreamingCodecSettingsDTO();
                dto.setStreamType(rs.getInt("streamType"));
                dto.setAudioCodec(rs.getInt("audioCodec"));
            }
            RingLogger.getConfigPortalLogger().debug(sql + "::" + dto.toString());
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().debug("getStreamingCodecSettingsByType Exception...", e);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (db != null) {
                    authdbconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
        }
        return dto;
    }

    public MyAppError updateStreamingCodecSettings(StreamingCodecSettingsDTO sdto) {
        MyAppError error = new MyAppError();
        DBConnection db = null;
        Statement stmt = null;
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            stmt = db.connection.createStatement();
            String sql = "UPDATE streamingcodecsettings SET audioCodec = " + sdto.getAudioCodec() + " WHERE streamType = " + sdto.getStreamType();
            int result = stmt.executeUpdate(sql);
            RingLogger.getConfigPortalLogger().debug(sql + "::" + result);
        } catch (Exception e) {
            error.setERROR_TYPE(MyAppError.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            RingLogger.getConfigPortalLogger().debug("updateStreamingCodecSettingsByType Exception...", e);
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (db != null) {
                    authdbconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError deleteStreamingCodecSettings(StreamingCodecSettingsDTO dto) {
        MyAppError error = new MyAppError();
        DBConnection db = null;
        Statement stmt = null;
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            stmt = db.connection.createStatement();
            String sql = "DELETE FROM streamingcodecsettings WHERE streamType = " + dto.getStreamType();
            int result = stmt.executeUpdate(sql);
            RingLogger.getConfigPortalLogger().debug(sql + "::" + result);
        } catch (Exception e) {
            error.setERROR_TYPE(MyAppError.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            RingLogger.getConfigPortalLogger().debug("deleteStreamingCodecSettings Exception...", e);
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (db != null) {
                    authdbconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError addStreamingCodecSettings(StreamingCodecSettingsDTO dto) {
        MyAppError error = new MyAppError();
        DBConnection db = null;
        PreparedStatement ps = null;
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            String query = "INSERT INTO streamingcodecsettings(streamType, audioCodec) VALUES (?, ?);";
            ps = db.connection.prepareStatement(query);
            ps.setInt(1, dto.getStreamType());
            ps.setInt(2, dto.getAudioCodec());

            ps.execute();
        } catch (SQLException e) {
            error.setERROR_TYPE(MyAppError.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            RingLogger.getConfigPortalLogger().debug("add Single addStreamingCodecSettings SQL Exception --> " + e);
        } catch (Exception e) {
            error.setERROR_TYPE(MyAppError.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            RingLogger.getConfigPortalLogger().debug("add Single addStreamingCodecSettings Exception --> " + e);
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (db != null) {
                    authdbconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public List<SpecialRoomDTO> getSpecialRoomList(String searchText) {
        List<SpecialRoomDTO> specialRoomDTOs = new ArrayList<>();
        DBConnection db = null;
        Statement stmt = null;
        ResultSet rs = null;
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            stmt = db.connection.createStatement();
            String sql = "SELECT * FROM specialrooms;";
            rs = stmt.executeQuery(sql);
            while (rs.next()) {
                SpecialRoomDTO dto = new SpecialRoomDTO();
                dto.setRoomId(rs.getLong("roomId"));
                dto.setName(rs.getString("name"));
                dto.setDescription(rs.getString("description"));
                dto.setBanner(rs.getString("banner"));
                dto.setVotingStartTime(rs.getLong("votingStartTime"));
                dto.setVotingEndTime(rs.getLong("votingEndTime"));
                dto.setPageId(rs.getLong("pageId"));
                dto.setPageProfileType(rs.getInt("pageProfileType"));
                dto.setVisible(rs.getInt("isVisible"));

                if (searchText != null && searchText.length() > 0) {
                    if (!(dto.getName().toLowerCase().contains(searchText.trim().toLowerCase())
                            || (dto.getDescription() != null && dto.getDescription().toLowerCase().contains(searchText.trim().toLowerCase()))
                            || String.valueOf(dto.getRoomId()).toLowerCase().contains(searchText.trim().toLowerCase())
                            || String.valueOf(dto.getPageId()).toLowerCase().contains(searchText.trim().toLowerCase()))) {
                        continue;
                    }
                }
                specialRoomDTOs.add(dto);
            }
            RingLogger.getConfigPortalLogger().debug(sql + "::" + specialRoomDTOs.size());

        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().debug("[getSpecialRoomList] Exception...", e);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (db != null) {
                    authdbconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
        }
        return specialRoomDTOs;
    }

    public SpecialRoomDTO getSpecialRoomByRoomId(long roomId) {
        SpecialRoomDTO dto = new SpecialRoomDTO();
        DBConnection db = null;
        Statement stmt = null;
        ResultSet rs = null;
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            stmt = db.connection.createStatement();
            String sql = "SELECT * FROM specialrooms where roomId = " + roomId + ";";
            rs = stmt.executeQuery(sql);
            if (rs.next()) {
                dto.setRoomId(rs.getLong("roomId"));
                dto.setName(rs.getString("name"));
                dto.setDescription(rs.getString("description"));
                dto.setBanner(rs.getString("banner"));
                dto.setVotingStartTime(rs.getLong("votingStartTime"));
                dto.setVotingEndTime(rs.getLong("votingEndTime"));
                dto.setPageId(rs.getLong("pageId"));
                dto.setPageProfileType(rs.getInt("pageProfileType"));
                dto.setVisible(rs.getInt("isVisible"));
            }
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().debug("[getSpecialRoomByRoomId] Exception...", e);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (db != null) {
                    authdbconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
        }
        return dto;
    }

    public MyAppError addSpecialRoom(SpecialRoomDTO dto) {
        MyAppError error = new MyAppError();
        DBConnection db = null;
        PreparedStatement ps = null;
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            String query = "INSERT INTO specialrooms (name, description, banner, votingStartTime, votingEndTime, pageId, pageProfileType, isVisible) values(?, ?, ?, ?, ?, ?, ?, ?);";
            ps = db.connection.prepareStatement(query);
            ps.setString(1, dto.getName());
            ps.setString(2, dto.getDescription());
            ps.setString(3, dto.getBanner());
            ps.setLong(4, dto.getVotingStartTime());
            ps.setLong(5, dto.getVotingEndTime());
            ps.setLong(6, dto.getPageId());
            ps.setInt(7, dto.getPageProfileType());
            ps.setInt(8, dto.getVisible());
            ps.execute();
        } catch (SQLException e) {
            error.setERROR_TYPE(MyAppError.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            RingLogger.getConfigPortalLogger().debug("addSpecialRoom --> " + e);
        } catch (Exception e) {
            error.setERROR_TYPE(MyAppError.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            RingLogger.getConfigPortalLogger().debug("[addSpecialRoom] --> " + e);
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (db != null) {
                    authdbconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError updateSpecialRoom(SpecialRoomDTO dto) {
        MyAppError error = new MyAppError();
        DBConnection db = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            String query = "UPDATE specialrooms SET name = ?, description = ?, banner = ?, votingStartTime = ?, votingEndTime = ?, pageId = ?, pageProfileType = ?, isVisible = ? WHERE roomId = ?;";
            ps = db.connection.prepareStatement(query);
            ps.setString(1, dto.getName());
            ps.setString(2, dto.getDescription());
            ps.setString(3, dto.getBanner());
            ps.setLong(4, dto.getVotingStartTime());
            ps.setLong(5, dto.getVotingEndTime());
            ps.setLong(6, dto.getPageId());
            ps.setInt(7, dto.getPageProfileType());
            ps.setInt(8, dto.getVisible());
            ps.setLong(9, dto.getRoomId());
            ps.execute();
        } catch (SQLException e) {
            error.setERROR_TYPE(MyAppError.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            RingLogger.getConfigPortalLogger().debug("[updateSpecialRoom] --> " + e);
        } catch (Exception e) {
            error.setERROR_TYPE(MyAppError.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            RingLogger.getConfigPortalLogger().debug("[updateSpecialRoom] --> " + e);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (db != null) {
                    authdbconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError deleteSpecialRoom(long roomId) {
        MyAppError error = new MyAppError();
        DBConnection db = null;
        Statement stmt = null;
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            stmt = db.connection.createStatement();
            String sql = "DELETE FROM specialrooms WHERE roomId = " + roomId + ";";
            int a = stmt.executeUpdate(sql);
            RingLogger.getConfigPortalLogger().debug("delete success roomId : " + roomId);
        } catch (SQLException e) {
            error.setERROR_TYPE(MyAppError.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            RingLogger.getConfigPortalLogger().debug("[deleteSpecialRoom] --> " + e);
        } catch (Exception e) {
            error.setERROR_TYPE(MyAppError.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            RingLogger.getConfigPortalLogger().debug("[deleteSpecialRoom] --> " + e);
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (db != null) {
                    authdbconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError addLiveSummary(long startDate, long endDate) {
        MyAppError myAppError = new MyAppError();
        boolean isGenerate = true;
        if (startDate == endDate) {
            DBConnection db = null;
            Statement stmt = null;
            ResultSet rs = null;
            try {
                String sql;
                db = authdbconnector.DBConnector.getInstance().makeConnection();
                stmt = db.connection.createStatement();
                sql = "SELECT * from streaminghistorysummary WHERE streamDate =" + startDate;
                rs = stmt.executeQuery(sql);
                if (rs.next()) {
                    isGenerate = false;
                }
            } catch (ParseException ex) {
                RingLogger.getConfigPortalLogger().debug(ex.toString().substring(ex.toString().lastIndexOf(":") + 1));
            } catch (Exception ex) {
                RingLogger.getConfigPortalLogger().debug(ex.toString().substring(ex.toString().lastIndexOf(":") + 1));
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                } catch (SQLException e) {
                }
                try {
                    if (stmt != null) {
                        stmt.close();
                    }
                } catch (Exception e) {
                }
                try {
                    if (db != null) {
                        authdbconnector.DBConnector.getInstance().freeConnection(db);
                    }
                } catch (Exception e) {
                }
            }
        }
        if (isGenerate) {
            deleteLiveSummary(startDate, endDate);
            myAppError = generateLiveHistorySummary(startDate, endDate);
        }

        return myAppError;
    }

    public int deleteLiveSummary(long startDate, long endDate) {
        int result = 0;
        DBConnection db = null;
        Statement stmt = null;
        ResultSet rs = null;
        try {
            //delete previous live summary
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            stmt = db.connection.createStatement();
            String sql = "DELETE FROM streaminghistorysummary WHERE streamDate BETWEEN '" + startDate + "' AND '" + endDate + "' ";
            int a = stmt.executeUpdate(sql);
            if (a > 0) {
                RingLogger.getConfigPortalLogger().debug("delete success");
            }
            RingLogger.getConfigPortalLogger().debug(sql + "::" + a);
        } catch (Exception ex) {
            result = -1;
            RingLogger.getConfigPortalLogger().error("Exception in [deleteLiveSummary] --> " + ex);
            RingLogger.getConfigPortalLogger().debug("SQLException in [deleteLiveSummary] " + ex.toString().lastIndexOf(":") + 1);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (db != null) {
                    authdbconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
        }
        return result;
    }

    private MyAppError generateLiveHistorySummary(long startDate, long endDate) {
        MyAppError myAppError = new MyAppError();
        DBConnection db = null;
        Statement stmt = null;
        ResultSet rs = null;
        PreparedStatement ps = null;
        try {
            String sql;
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            stmt = db.connection.createStatement();
            Calendar startCalendar = Calendar.getInstance();
            startCalendar.setTime(new Date(startDate));
            Calendar endCalendar = Calendar.getInstance();
            endCalendar.setTime(new Date(endDate));
            endCalendar.add(Calendar.DATE, 1);
            while (startCalendar.before(endCalendar)) {

                sql = "SELECT ringId, name, device, country, countryId, SUM(duration) AS totalLiveDuration,  COUNT(*) AS noOfAppearance"
                        + " FROM streaminghistory"
                        + " WHERE streamDate = '" + sdf.format(startCalendar.getTime()) + "' "
                        + " group by ringId, name, device, country, countryId order by ringId asc;";
                RingLogger.getConfigPortalLogger().debug(sql + "::");
                try {
                    rs = stmt.executeQuery(sql);
                    int i = 1, count = 1;
                    String query = "INSERT INTO streaminghistorysummary(ringId, name, totalLiveDuration, noOfAppearance, device, country, countryId, streamDate) VALUES(?, ?, ?, ?, ?, ?, ?, ?)";
                    ps = db.connection.prepareStatement(query);
                    while (rs.next()) {
                        i = 1;
                        ps.setString(i++, rs.getString("ringId"));
                        if (rs.getString("name") != null) {
                            ps.setString(i++, rs.getString("name"));
                        } else {
                            ps.setString(i++, "n/a");
                            RingLogger.getConfigPortalLogger().debug("[generateLiveHistorySummary] name not found !!!");
                        }
                        ps.setLong(i++, rs.getLong("totalLiveDuration"));
                        ps.setInt(i++, rs.getInt("noOfAppearance"));
                        ps.setInt(i++, rs.getInt("device"));

                        if (rs.getString("country") != null) {
                            ps.setString(i++, rs.getString("country"));
                        } else {
                            ps.setString(i++, "n/a");
                            RingLogger.getConfigPortalLogger().debug("[generateLiveHistorySummary] country not found !!!");
                        }

                        ps.setLong(i++, rs.getInt("countryId"));
                        ps.setLong(i++, startCalendar.getTimeInMillis());
                        ps.addBatch();
                        if (count++ % 1000 == 0) {
                            ps.executeBatch();
                            count = 0;
                        }
                    }
                    if (count > 0) {
                        ps.executeBatch();
                    }
                } catch (SQLException ex) {
                    myAppError.setERROR_TYPE(MyAppError.OTHERERROR);
                    myAppError.setErrorMessage(ex.toString().substring(ex.toString().lastIndexOf(":") + 1));
                    RingLogger.getConfigPortalLogger().debug("SQLException in [generateLiveHistorySummary] ", ex);
                }
                startCalendar.add(Calendar.DATE, 1);
            }
        } catch (ParseException ex) {
            myAppError.setERROR_TYPE(MyAppError.OTHERERROR);
            myAppError.setErrorMessage(ex.toString().substring(ex.toString().lastIndexOf(":") + 1));
            RingLogger.getConfigPortalLogger().debug("ParseException in [generateLiveHistorySummary] ", ex);
        } catch (Exception ex) {
            myAppError.setERROR_TYPE(MyAppError.OTHERERROR);
            myAppError.setErrorMessage(ex.toString().substring(ex.toString().lastIndexOf(":") + 1));
            RingLogger.getConfigPortalLogger().debug("Exception in [generateLiveHistorySummary] ", ex);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (db != null) {
                    authdbconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
        }
        return myAppError;
    }

    public List<LiveSummaryDTO> getLiveSummaryList(long fromDate, long toDate) {
        List<LiveSummaryDTO> liveSummaryDTOs = new ArrayList<>();
        long millis = 0L;
        DBConnection db = null;
        Statement stmt = null;
        ResultSet rs = null;
        try {
            String sql;
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            stmt = db.connection.createStatement();
            sql = "SELECT * from streaminghistorysummary WHERE streamDate BETWEEN " + fromDate + " AND " + toDate;
            rs = stmt.executeQuery(sql);
            while (rs.next()) {
                LiveSummaryDTO dto = new LiveSummaryDTO();
//                dto.setId(rs.getLong("id"));
                dto.setRingId(rs.getString("ringId"));
//                millis = rs.getLong("totalLiveDuration");
////                String hms = String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(millis),
////                        TimeUnit.MILLISECONDS.toMinutes(millis) % TimeUnit.HOURS.toMinutes(1),
////                        TimeUnit.MILLISECONDS.toSeconds(millis) % TimeUnit.MINUTES.toSeconds(1));
                dto.setTotalLiveDuration(rs.getLong("totalLiveDuration"));
                dto.setNoOfAppearance(rs.getInt("noOfAppearance"));
                dto.setDevice(rs.getInt("device"));
                dto.setName(rs.getString("name"));
                dto.setCountry(rs.getString("country"));
                dto.setStreamDate(rs.getLong("streamdate"));
                dto.setCountryId(rs.getInt("countryId"));
                dto.setStreamDateStr(sdf.format(new Date(rs.getLong("streamdate"))));

                liveSummaryDTOs.add(dto);
            }
            RingLogger.getConfigPortalLogger().debug(sql + " :: " + liveSummaryDTOs.size());
        } catch (Exception ex) {
            RingLogger.getConfigPortalLogger().debug(ex.toString().substring(ex.toString().lastIndexOf(":") + 1));
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (db != null) {
                    authdbconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
        }
        return liveSummaryDTOs;
    }

    public List<LiveInfoDetailsDTO> getLiveInfoDetailsList(LiveInfoDetailsDTO liveInfoDetailsDTO, String tableName) {
        List<LiveInfoDetailsDTO> liveInfoDetailsDTOs = new ArrayList<>();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String table = tableName != null && tableName.length() > 0 ? tableName : "streaminghistory";
        DBConnection db = null;
        Statement stmt = null;
        ResultSet rs = null;
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            stmt = db.connection.createStatement();
            String sql;
            String firstPartOfSql = "SELECT streamId, userId, ringId, name, ownId, startTime, endTime, duration, roomId, device, streamDate FROM " + table;
            String optionalSql = "";

            if (liveInfoDetailsDTO.getRingId() != null && liveInfoDetailsDTO.getRingId().length() > 0) {
                optionalSql += getClause(optionalSql) + " ringId in (" + liveInfoDetailsDTO.getRingId() + ")";
            }

            optionalSql = prepareDateLimitation(optionalSql, liveInfoDetailsDTO);
            sql = firstPartOfSql + optionalSql;

            rs = stmt.executeQuery(sql);
            while (rs.next()) {
                LiveInfoDetailsDTO dto = new LiveInfoDetailsDTO();
                dto.setStreamId(rs.getString("streamId"));
                dto.setUserId(rs.getLong("userId"));
                dto.setRingId(rs.getString("ringId"));
                if (rs.getString("name") != null) {
                    dto.setFirstName(rs.getString("name"));
                } else {
                    dto.setFirstName("n/a");
                    RingLogger.getConfigPortalLogger().debug("[getLiveInfoDetailsList] name not found !!!");
                }
                dto.setOwnId(rs.getLong("ownId"));
                dto.setStartTime(rs.getLong("startTime"));
                dto.setEndTime(rs.getLong("endTime"));
                dto.setStartDate(sdf.format(new Date(rs.getLong("startTime"))));
                dto.setEndDate(sdf.format(new Date(rs.getLong("endTime"))));
                dto.setDuration(rs.getLong("duration"));
                dto.setRoomId(rs.getInt("roomId"));
                dto.setDevice(rs.getInt("device"));
                dto.setStreamDate(rs.getDate("streamDate").toString());

                long millis = rs.getLong("duration");
                String hms = String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(millis),
                        TimeUnit.MILLISECONDS.toMinutes(millis) % TimeUnit.HOURS.toMinutes(1),
                        TimeUnit.MILLISECONDS.toSeconds(millis) % TimeUnit.MINUTES.toSeconds(1));

                dto.setLiveDuration(hms);
                liveInfoDetailsDTOs.add(dto);
            }
            RingLogger.getConfigPortalLogger().debug(sql + "::" + liveInfoDetailsDTOs.size());
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().debug("getLiveInfoDetailsList List Exception...", e);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (db != null) {
                    authdbconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
        }
        return liveInfoDetailsDTOs;
    }

    public List<LiveInfoDTO> getLiveInfoList(LiveInfoDTO liveInfoDTO, String tableName) {

        List<LiveInfoDTO> liveInfoDTOList = new ArrayList<>();

        String table = tableName != null && tableName.length() > 0 ? tableName : "streaminghistory";
        DBConnection db = null;
        Statement stmt = null;
        ResultSet rs = null;
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            stmt = db.connection.createStatement();
            String sql;
            String firstPartOfSql = "SELECT ringId, name, streamDate, countryId, country, device, "
                    + " SUM(duration) AS totalLiveDuration, "
                    + " COUNT(*) AS noOfAppearance "
                    + " FROM " + table;
            String optionalSql = "";

            if (liveInfoDTO.getRingId() != null && liveInfoDTO.getRingId().length() > 0) {
                optionalSql += getClause(optionalSql) + " ringId in (" + liveInfoDTO.getRingId() + ")";
            }

            optionalSql = prepareDateLimitation(optionalSql, liveInfoDTO);
            sql = firstPartOfSql + optionalSql + "  group by ringId, name, streamDate, countryId, country, device order by streamDate desc, ringId asc;";
            RingLogger.getConfigPortalLogger().debug("## getLiveInfoListSQL --> " + sql);
            rs = stmt.executeQuery(sql);
            while (rs.next()) {
                LiveInfoDTO dto = new LiveInfoDTO();

                dto.setRingId(rs.getString("ringId"));
                if (rs.getString("name") != null) {
                    dto.setFirstName(rs.getString("name"));
                } else {
                    dto.setFirstName("n/a");
                    RingLogger.getConfigPortalLogger().debug("[getLiveInfoList] name not found !!!");
                }
                dto.setStreamDate(sdf.format(rs.getDate("streamDate")));
                dto.setDuration(rs.getLong("totalLiveDuration"));
                dto.setNoOfAppearance(rs.getInt("noOfAppearance"));
                dto.setCountryId(rs.getInt("countryId"));
                dto.setCountry(rs.getString("country"));
                dto.setDevice(rs.getInt("device"));

                long millis = rs.getLong("totalLiveDuration");
                String hms = String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(millis),
                        TimeUnit.MILLISECONDS.toMinutes(millis) % TimeUnit.HOURS.toMinutes(1),
                        TimeUnit.MILLISECONDS.toSeconds(millis) % TimeUnit.MINUTES.toSeconds(1));
                dto.setTotalLiveDuration(millis);
                dto.setTotalLiveDurationStr(hms);

                liveInfoDTOList.add(dto);
            }
            RingLogger.getConfigPortalLogger().debug("## getLiveInfoListSQL ::" + liveInfoDTOList.size());
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("getLiveInfoList List Exception...", e);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (db != null) {
                    authdbconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
        }
        return liveInfoDTOList;
    }

    public List<LiveInfoDTO> getLiveInfoListByRingId(LiveInfoDTO liveInfoDTO, String tableName) {
        List<LiveInfoDTO> liveInfoDTOList = new ArrayList<>();
        String table = tableName != null && tableName.length() > 0 ? tableName : "streaminghistory";
        DBConnection db = null;
        Statement stmt = null;
        ResultSet rs = null;
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            stmt = db.connection.createStatement();
            String sql;
            String firstPartOfSql = "SELECT ringId, name, country, countryId, device, "
                    + " SUM(duration) AS totalLiveDuration, "
                    + " COUNT(*) AS noOfAppearance "
                    + " FROM  " + table;
            String optionalSql = "";

            if (liveInfoDTO.getRingId() != null && liveInfoDTO.getRingId().length() > 0) {
                optionalSql += getClause(optionalSql) + " ringId in (" + liveInfoDTO.getRingId() + ")";
            }

            if (liveInfoDTO.getCountryId() > 0) {
                optionalSql += getClause(optionalSql) + " countryId = " + liveInfoDTO.getCountryId();
            }

            optionalSql = prepareDateLimitation(optionalSql, liveInfoDTO);
            sql = firstPartOfSql + optionalSql + "  group by ringId, name, country, countryId, device order by ringId asc;";
            RingLogger.getConfigPortalLogger().debug("## getLiveInfoListByRingId sql --> " + sql);
            rs = stmt.executeQuery(sql);
            while (rs.next()) {
                LiveInfoDTO dto = new LiveInfoDTO();

                dto.setRingId(rs.getString("ringId"));
                if (rs.getString("name") != null) {
                    dto.setFirstName(rs.getString("name"));
                } else {
                    dto.setFirstName("n/a");
                    RingLogger.getConfigPortalLogger().debug("[getLiveInfoListByRingId] name not found !!!");
                }
                dto.setDuration(rs.getLong("totalLiveDuration"));
                dto.setNoOfAppearance(rs.getInt("noOfAppearance"));
                dto.setCountryId(rs.getInt("countryId"));
                dto.setCountry(rs.getString("country"));
                dto.setDevice(rs.getInt("device"));

                long millis = rs.getLong("totalLiveDuration");
                String hms = String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(millis),
                        TimeUnit.MILLISECONDS.toMinutes(millis) % TimeUnit.HOURS.toMinutes(1),
                        TimeUnit.MILLISECONDS.toSeconds(millis) % TimeUnit.MINUTES.toSeconds(1));
                dto.setTotalLiveDuration(millis);
                dto.setTotalLiveDurationStr(hms);
                liveInfoDTOList.add(dto);
            }
            RingLogger.getConfigPortalLogger().debug("## getLiveInfoListByRingId ::" + liveInfoDTOList.size());

        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("getLiveInfoListByRingId List Exception...", e);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (db != null) {
                    authdbconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
        }
        return liveInfoDTOList;
    }

    public List<LiveToChannelDTO> getLiveToChannelListInfo() {
        List<LiveToChannelDTO> list = new ArrayList<>();
        DBConnection db = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            String sql = "SELECT pageId, channelId, liveTime, liveNow, title, description FROM livetochannel";
            ps = db.connection.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                LiveToChannelDTO dto = new LiveToChannelDTO();
                dto.setPageId(rs.getLong("pageId"));
                dto.setChannelId(rs.getString("channelId"));
                dto.setLiveTime(rs.getLong("liveTime"));
                dto.setLiveNow(rs.getInt("liveNow"));
                dto.setTitle(rs.getString("title"));
                dto.setDescription(rs.getString("description"));
                list.add(dto);
            }
        } catch (SQLException e) {
            RingLogger.getConfigPortalLogger().debug("getLiveToChannelListInfo --> " + e);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().debug("getLiveToChannelListInfo --> " + e);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (db != null) {
                    authdbconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
        }
        return list;
    }

    public LiveToChannelDTO getLiveToChannelInfo(long pageId) {
        LiveToChannelDTO liveToChannelDTO = new LiveToChannelDTO();
        DBConnection db = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            String sql = "SELECT pageId, channelId, liveTime, liveNow, title, description FROM livetochannel WHERE pageId = ?";
            ps = db.connection.prepareStatement(sql);
            ps.setLong(1, pageId);
            rs = ps.executeQuery();
            if (rs.next()) {
                liveToChannelDTO.setPageId(rs.getLong("pageId"));
                liveToChannelDTO.setChannelId(rs.getString("channelId"));
                liveToChannelDTO.setLiveTime(rs.getLong("liveTime"));
                liveToChannelDTO.setLiveNow(rs.getInt("liveNow"));
                liveToChannelDTO.setTitle(rs.getString("title"));
                liveToChannelDTO.setDescription(rs.getString("description"));
            }
        } catch (SQLException e) {
            RingLogger.getConfigPortalLogger().debug("getLiveToChannelInfo --> " + e);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().debug("getLiveToChannelInfo --> " + e);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (db != null) {
                    authdbconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
        }
        return liveToChannelDTO;
    }

    public int addOrUpdateLiveToChannelInfo(LiveToChannelDTO dto) {
        int result = 0, i = 1, exist = 0;
        DBConnection db = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            String sql = "SELECT count(pageId) AS pageIdCount FROM livetochannel WHERE pageId = ?";
            ps = db.connection.prepareStatement(sql);
            ps.setLong(1, dto.getPageId());

            rs = ps.executeQuery();
            if (rs.next()) {
                exist = rs.getInt("pageIdCount");
            }
            if (exist > 0) {
                sql = "UPDATE livetochannel SET channelId = ?, liveTime = ?, liveNow = ?, title = ?, description = ? WHERE pageId = ?;";
                ps = db.connection.prepareStatement(sql);
                ps.setString(i++, dto.getChannelId());
                ps.setLong(i++, dto.getLiveTime());
                ps.setInt(i++, dto.getLiveNow());
                ps.setString(i++, dto.getTitle());
                ps.setString(i++, dto.getDescription());
                ps.setLong(i++, dto.getPageId());
            } else {
                sql = "INSERT INTO livetochannel (pageId, channelId, liveNow, liveTime, title, description) values(?, ?, ?, ?, ?, ?);";
                ps = db.connection.prepareStatement(sql);
                ps.setLong(i++, dto.getPageId());
                ps.setString(i++, dto.getChannelId());
                ps.setInt(i++, dto.getLiveNow());
                ps.setLong(i++, dto.getLiveTime());
                ps.setString(i++, dto.getTitle());
                ps.setString(i++, dto.getDescription());
            }
            ps.execute();
        } catch (SQLException e) {
            result = -1;
            RingLogger.getConfigPortalLogger().debug("addOrUpdateLiveToChannelInfo --> " + e);
        } catch (Exception e) {
            result = -1;
            RingLogger.getConfigPortalLogger().debug("addOrUpdateLiveToChannelInfo --> " + e);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (db != null) {
                    authdbconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
        }
        return result;
    }

    public int deleteLiveToChannelInfo(Long pageId) {
        int result = 0, i = 1;
        DBConnection db = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            String sql = "DELETE FROM livetochannel WHERE pageId = ?;";
            ps = db.connection.prepareStatement(sql);
            ps.setLong(i++, pageId);
            ps.execute();
            RingLogger.getConfigPortalLogger().debug("[deleteLiveToChannelInfo] sql --> DELETE FROM livetochannel WHERE pageId = " + pageId + " :: " + result);
        } catch (SQLException e) {
            result = -1;
            RingLogger.getConfigPortalLogger().debug("deleteLiveToChannelInfo[1] --> " + e);
        } catch (Exception e) {
            result = -1;
            RingLogger.getConfigPortalLogger().debug("deleteLiveToChannelInfo --> " + e);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (db != null) {
                    authdbconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
        }
        return result;
    }

    public int resetLiveTime(long pageId) {
        int result = 0, i = 1;
        DBConnection db = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            String sql = "UPDATE livetochannel SET liveTime = 0 WHERE pageId = ?;";
            ps = db.connection.prepareStatement(sql);
            ps.setLong(i++, pageId);
            ps.execute();
            RingLogger.getConfigPortalLogger().debug("[resetLiveTime] sql --> UPDATE livetochannel SET liveTime = 0 WHERE pageId = " + pageId + " :: " + result);
        } catch (SQLException e) {
            result = -1;
            RingLogger.getConfigPortalLogger().debug("resetLiveTime[1] --> " + e);
        } catch (Exception e) {
            result = -1;
            RingLogger.getConfigPortalLogger().debug("resetLiveTime --> " + e);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (db != null) {
                    authdbconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
        }
        return result;
    }

    private String prepareDateLimitation(String optionalSql, LiveInfoDTO liveInfoDTO) throws ParseException {

        Calendar cal = Calendar.getInstance();
        Date maxToDate = cal.getTime();
        String maxToDateStr = sdf.format(maxToDate);
        cal.add(Calendar.DATE, -30);
        Date minFromDate = cal.getTime();
        String minFromDateStr = sdf.format(minFromDate);

        String fromDate, toDate;

        if (liveInfoDTO.getStartDate() != null && liveInfoDTO.getStartDate().length() > 0) {
            fromDate = liveInfoDTO.getStartDate();
        } else {
            fromDate = minFromDateStr;
        }
        if (liveInfoDTO.getEndDate() != null && liveInfoDTO.getEndDate().length() > 0) {
            toDate = liveInfoDTO.getEndDate();
        } else {
            toDate = maxToDateStr;
        }

        if (liveInfoDTO.getStartDate() == null && liveInfoDTO.getEndDate() == null) {
            optionalSql += getClause(optionalSql) + " streamDate BETWEEN '" + minFromDateStr + "' AND '" + maxToDateStr + "'";
        } else if (liveInfoDTO.getStartDate() != null && liveInfoDTO.getStartDate().length() > 0 && liveInfoDTO.getEndDate() != null && liveInfoDTO.getEndDate().length() > 0) {
            optionalSql += getClause(optionalSql) + " streamDate BETWEEN '" + fromDate + "' AND '" + toDate + "'";
        } else if (liveInfoDTO.getStartDate() != null && liveInfoDTO.getStartDate().length() > 0) {
            optionalSql += getClause(optionalSql) + " streamDate >= '" + fromDate + "' ";
        } else if (liveInfoDTO.getEndDate() != null && liveInfoDTO.getEndDate().length() > 0) {
            optionalSql += getClause(optionalSql) + " streamDate <= '" + toDate + "'";
        }
        return optionalSql;
    }

    private String prepareDateLimitation(String optionalSql, LiveInfoDetailsDTO liveInfoDetailsDTO) throws ParseException {

        Calendar cal = Calendar.getInstance();
        Date maxToDate = cal.getTime();
        String maxToDateStr = sdf.format(maxToDate);
        cal.add(Calendar.DATE, -30);
        Date minFromDate = cal.getTime();
        String minFromDateStr = sdf.format(minFromDate);

        String fromDate, toDate;

        if (liveInfoDetailsDTO.getStartDate() != null && liveInfoDetailsDTO.getStartDate().length() > 0) {
            fromDate = liveInfoDetailsDTO.getStartDate();
        } else {
            fromDate = minFromDateStr;
        }
        if (liveInfoDetailsDTO.getEndDate() != null && liveInfoDetailsDTO.getEndDate().length() > 0) {
            toDate = liveInfoDetailsDTO.getEndDate();
        } else {
            toDate = maxToDateStr;
        }

        if (liveInfoDetailsDTO.getStartDate() == null && liveInfoDetailsDTO.getEndDate() == null) {
            optionalSql += getClause(optionalSql) + " streamDate BETWEEN '" + minFromDateStr + "' AND '" + maxToDateStr + "'";
        } else if (liveInfoDetailsDTO.getStartDate() != null && liveInfoDetailsDTO.getStartDate().length() > 0 && liveInfoDetailsDTO.getEndDate() != null && liveInfoDetailsDTO.getEndDate().length() > 0) {
            optionalSql += getClause(optionalSql) + " streamDate BETWEEN '" + fromDate + "' AND '" + toDate + "'";
        } else if (liveInfoDetailsDTO.getStartDate() != null && liveInfoDetailsDTO.getStartDate().length() > 0) {
            optionalSql += getClause(optionalSql) + " streamDate >= '" + fromDate + "' ";
        } else if (liveInfoDetailsDTO.getEndDate() != null && liveInfoDetailsDTO.getEndDate().length() > 0) {
            optionalSql += getClause(optionalSql) + " streamDate <= '" + toDate + "'";
        }
        return optionalSql;
    }

    private String getClause(String optionalSql) {
        return (optionalSql.length() < 1 ? " WHERE " : " AND ");
    }
}
