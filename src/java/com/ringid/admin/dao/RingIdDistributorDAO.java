/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.dao;

import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.dto.RingIdDistributorDTO;
import com.ringid.admin.utils.MyAppError;
import authdbconnector.DBConnection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Rabby
 */
public class RingIdDistributorDAO {

    private static RingIdDistributorDAO instance = null;

    private RingIdDistributorDAO() {
    }

    public static RingIdDistributorDAO getInstance() {
        if (instance == null) {
            createInstance();
        }
        return instance;
    }

    private synchronized static void createInstance() {
        if (instance == null) {
            instance = new RingIdDistributorDAO();
        }
    }

    public RingIdDistributorDTO getRingIdDistributorDTO(RingIdDistributorDTO sdto) {
        RingIdDistributorDTO dto = new RingIdDistributorDTO();
        DBConnection db = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            String sql = "SELECT serverID, startRingID, endRingID, status FROM ringiddistributor WHERE serverID = ? AND startRingID = ? AND endRingID = ?";
            RingLogger.getConfigPortalLogger().info("sql --> " + sql);
            ps = db.connection.prepareStatement(sql);
            int i = 1;
            ps.setInt(i++, sdto.getServerId());
            ps.setLong(i++, sdto.getStartRingId());
            ps.setLong(i++, sdto.getEndRingId());
            rs = ps.executeQuery();
            if (rs.next()) {
                dto.setServerId(rs.getInt("serverID"));
                dto.setStartRingId(rs.getLong("startRingID"));
                dto.setEndRingId(rs.getLong("endRingID"));
                dto.setServerId(rs.getInt("status"));
            }
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in RingIdDistributorDAO --> " + e);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (db != null) {
                    authdbconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
        }
        return dto;
    }

    public MyAppError addRingIdDistributor(RingIdDistributorDTO sdto) {
        MyAppError error = new MyAppError();
        DBConnection db = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            String sql = "SELECT serverID FROM ringiddistributor WHERE serverID = ? AND startRingID = ? AND endRingID = ?";
            ps = db.connection.prepareStatement(sql);
            int i = 1;
            ps.setInt(i++, sdto.getServerId());
            ps.setLong(i++, sdto.getStartRingId());
            ps.setLong(i++, sdto.getEndRingId());
            rs = ps.executeQuery();
            if (rs.next()) {
                error.setERROR_TYPE(MyAppError.OTHERERROR);
                error.setErrorMessage("Duplicate Content.");
                RingLogger.getConfigPortalLogger().error("Duplicate Content");
                return error;
            }
            sql = "INSERT INTO ringiddistributor(serverID, startRingID, endRingID, status) VALUES(?, ?, ?, ?)";
            ps = db.connection.prepareStatement(sql);
            i = 1;
            ps.setInt(i++, sdto.getServerId());
            ps.setLong(i++, sdto.getStartRingId());
            ps.setLong(i++, sdto.getEndRingId());
            ps.setInt(i++, sdto.getStatus());
            ps.execute();
        } catch (Exception e) {
            error.setERROR_TYPE(MyAppError.DBERROR);
            error.setErrorMessage(e.getMessage());
            RingLogger.getConfigPortalLogger().error("Exception in addRingIdDistributor --> " + e);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (db != null) {
                    authdbconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError updateRingIdDistributor(RingIdDistributorDTO oldDTO, RingIdDistributorDTO newDTO) {
        MyAppError error = new MyAppError();
        DBConnection db = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            String sql = "SELECT serverID FROM ringiddistributor WHERE serverID = ? AND startRingID = ? AND endRingID = ?";
            ps = db.connection.prepareStatement(sql);
            int i = 1;
            ps.setInt(i++, newDTO.getServerId());
            ps.setLong(i++, newDTO.getStartRingId());
            ps.setLong(i++, newDTO.getEndRingId());
            rs = ps.executeQuery();
            if (!isSameInfo(oldDTO, newDTO) && rs.next()) {
                error.setERROR_TYPE(MyAppError.OTHERERROR);
                error.setErrorMessage("Duplicate Content.");
                RingLogger.getConfigPortalLogger().error("Duplicate Content");
                return error;
            }
            sql = "UPDATE ringiddistributor SET serverID = ?, startRingID = ?, endRingID = ?, status = ? WHERE serverID = ? AND startRingID = ? AND endRingID = ?";
            ps = db.connection.prepareStatement(sql);
            i = 1;
            ps.setInt(i++, newDTO.getServerId());
            ps.setLong(i++, newDTO.getStartRingId());
            ps.setLong(i++, newDTO.getEndRingId());
            ps.setInt(i++, newDTO.getStatus());
            ps.setInt(i++, oldDTO.getServerId());
            ps.setLong(i++, oldDTO.getStartRingId());
            ps.setLong(i++, oldDTO.getEndRingId());
            ps.execute();
        } catch (Exception e) {
            error.setERROR_TYPE(MyAppError.DBERROR);
            error.setErrorMessage(e.getMessage());
            RingLogger.getConfigPortalLogger().error("Excepion in updateRingIdDistributor --> " + e);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (db != null) {
                    authdbconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError deleteRingIdDistributor(RingIdDistributorDTO dto) {
        MyAppError error = new MyAppError();
        DBConnection db = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            String sql = "DELETE FROM ringiddistributor WHERE serverID = ? AND startRingID = ? AND endRingID = ?";
            ps = db.connection.prepareStatement(sql);
            int i = 1;
            ps.setInt(i++, dto.getServerId());
            ps.setLong(i++, dto.getStartRingId());
            ps.setLong(i++, dto.getEndRingId());
            ps.execute();
        } catch (Exception e) {
            error.setERROR_TYPE(MyAppError.DBERROR);
            error.setErrorMessage(e.getMessage());
            RingLogger.getConfigPortalLogger().error("Exception in deleteRingIdDistributor --> " + e);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (db != null) {
                    authdbconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    private boolean isSameInfo(RingIdDistributorDTO oldDTO, RingIdDistributorDTO newDTO) {
        return (oldDTO.getServerId() == newDTO.getServerId()
                && oldDTO.getStartRingId() == newDTO.getStartRingId()
                && oldDTO.getEndRingId() == newDTO.getEndRingId());
    }

}
