/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.dao;

import com.google.gson.Gson;
import com.ringid.admin.dto.FloatingMenuDTO;
import com.ringid.admin.dto.FloatingMenuSlideItemDTO;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.utils.MyAppError;
import authdbconnector.DBConnection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javafx.util.Pair;

/**
 *
 * @author mamun
 */
public class FloatingMenuDAO {

    private static FloatingMenuDAO instance = null;

    private FloatingMenuDAO() {
    }

    public static FloatingMenuDAO getInstance() {
        if (instance == null) {
            createInstance();
        }
        return instance;
    }

    private synchronized static void createInstance() {
        if (instance == null) {
            instance = new FloatingMenuDAO();
        }
    }

    public List<FloatingMenuDTO> getFloatingMenuList() {
        List<FloatingMenuDTO> floatingMenuDTOs = new ArrayList<>();
        DBConnection db = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        Statement stmt = null;
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            stmt = db.connection.createStatement();
            String sql = "SELECT tab, type, name, weight, slideTime, redirectId, btnType, btnBgColor, btnTxtColor FROM floating_menu_mapping;";
            rs = stmt.executeQuery(sql);
            while (rs.next()) {
                FloatingMenuDTO dto = new FloatingMenuDTO();
                dto.setTab(rs.getInt("tab"));
                dto.setType(rs.getInt("type"));
                dto.setName(rs.getString("name"));
                dto.setWeight(rs.getInt("weight"));
                dto.setSlideTime(rs.getDouble("slideTime"));
                dto.setRedirectId(rs.getLong("redirectId"));
                dto.setBtnType(rs.getInt("btnType"));
                dto.setBtnBgColor(rs.getString("btnBgColor"));
                dto.setBtnTxtColor(rs.getString("btnTxtColor"));

                floatingMenuDTOs.add(dto);
            }
            RingLogger.getConfigPortalLogger().debug(sql + " [getFloatingMenuList] --> :: " + floatingMenuDTOs.size());
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().debug("getFloatingMenuList Exception...", e);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (db != null) {
                    authdbconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
        }
        return floatingMenuDTOs;
    }

    public FloatingMenuDTO getFloatingMenuByType(int tab, int type, String name) {
        FloatingMenuDTO dto = new FloatingMenuDTO();
        DBConnection db = null;
        ResultSet rs = null;
        PreparedStatement ps = null;
        int i = 1;
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            String sql = "SELECT tab, type, name, weight, slideTime, redirectId, btnType, btnBgColor, btnTxtColor FROM floating_menu_mapping WHERE type = ? AND tab = ? AND name = ?;";
            ps = db.connection.prepareStatement(sql);
            ps.setInt(i++, type);
            ps.setInt(i++, tab);
            ps.setString(i++, name);
            rs = ps.executeQuery();
            while (rs.next()) {
                dto.setTab(rs.getInt("tab"));
                dto.setType(rs.getInt("type"));
                dto.setName(rs.getString("name"));
                dto.setWeight(rs.getInt("weight"));
                dto.setSlideTime(rs.getDouble("slideTime"));
                dto.setRedirectId(rs.getLong("redirectId"));
                dto.setBtnType(rs.getInt("btnType"));
                dto.setBtnBgColor(rs.getString("btnBgColor"));
                dto.setBtnTxtColor(rs.getString("btnTxtColor"));
            }
            RingLogger.getConfigPortalLogger().debug(sql + " [getFloatingMenuByType] :: " + dto.getType());
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().debug("getFloatingMenuByType Exception...", e);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (db != null) {
                    authdbconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
        }
        return dto;
    }

    public MyAppError addFloatingMenuInfo(FloatingMenuDTO dto) {
        MyAppError error = new MyAppError();
        DBConnection db = null;
        PreparedStatement ps = null;
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            String query = "INSERT INTO floating_menu_mapping(tab, type, name, weight, slideTime, redirectId, btnType, btnBgColor, btnTxtColor) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?);";
            int i = 1;
            ps = db.connection.prepareStatement(query);
            ps.setInt(i++, dto.getTab());
            ps.setInt(i++, dto.getType());
            ps.setString(i++, dto.getName());
            ps.setInt(i++, dto.getWeight());
            ps.setDouble(i++, dto.getSlideTime());
            ps.setLong(i++, dto.getRedirectId());
            ps.setInt(i++, dto.getBtnType());
            ps.setString(i++, dto.getBtnBgColor());
            ps.setString(i++, dto.getBtnTxtColor());
            int a = ps.executeUpdate();
            if (a <= 0) {
                error.setERROR_TYPE(MyAppError.OTHERERROR);
            }
        } catch (SQLException e) {
            error.setERROR_TYPE(MyAppError.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            RingLogger.getConfigPortalLogger().debug("add Single addFloatingMenuInfo SQL Exception --> " + e);
        } catch (Exception e) {
            error.setERROR_TYPE(MyAppError.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            RingLogger.getConfigPortalLogger().debug("add Single addFloatingMenuInfo Exception --> " + e);
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (db != null) {
                    authdbconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError updateFloatingMenuInfo(FloatingMenuDTO dto, String name) {
        MyAppError error = new MyAppError();
        DBConnection db = null;
        PreparedStatement ps = null;
        int i = 1;
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            String query = "UPDATE floating_menu_mapping SET name = ?, weight = ?, slideTime = ?, redirectId = ?, btnType = ?, btnBgColor = ?, btnTxtColor = ? WHERE tab = ? AND type = ? AND name = ?;";
            ps = db.connection.prepareStatement(query);
            ps.setString(i++, dto.getName());
            ps.setInt(i++, dto.getWeight());
            ps.setDouble(i++, dto.getSlideTime());
            ps.setLong(i++, dto.getRedirectId());
            ps.setInt(i++, dto.getBtnType());
            ps.setString(i++, dto.getBtnBgColor());
            ps.setString(i++, dto.getBtnTxtColor());
            ps.setInt(i++, dto.getTab());
            ps.setInt(i++, dto.getType());
            ps.setString(i++, name);
            int a = ps.executeUpdate();
            if (a <= 0) {
                error.setERROR_TYPE(MyAppError.OTHERERROR);
            }
        } catch (SQLException e) {
            error.setERROR_TYPE(MyAppError.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            RingLogger.getConfigPortalLogger().debug("updateFloatingMenuInfo SQL Exception --> " + e);
        } catch (Exception e) {
            error.setERROR_TYPE(MyAppError.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            RingLogger.getConfigPortalLogger().debug("updateFloatingMenuInfo Exception --> " + e);
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (db != null) {
                    authdbconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError deleteFloatingMenu(int type) {
        MyAppError error = new MyAppError();
        return error;
    }

    public Map<Pair<Integer, String>, FloatingMenuSlideItemDTO> getFloationMenuSlideItemList(int type) {
        Map<Pair<Integer, String>, FloatingMenuSlideItemDTO> mapList = new HashMap<>();
        DBConnection db = null;
        ResultSet rs = null;
        Statement stmt = null;
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            stmt = db.connection.createStatement();
            String sql = "SELECT featureType, featureId FROM floating_menu_slide_items";
            String optStr = type != 0 ? " WHERE featureType = " + type : ";";
            sql += optStr;
            rs = stmt.executeQuery(sql);
            while (rs.next()) {
                FloatingMenuSlideItemDTO dto = new FloatingMenuSlideItemDTO();
                dto.setFeatureType(rs.getInt("featureType"));
                dto.setFeatureId(rs.getString("featureId"));
                mapList.put(new Pair<>(dto.getFeatureType(), dto.getFeatureId()), dto);
            }
            RingLogger.getConfigPortalLogger().debug(sql + "::" + mapList.size());
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().debug("##getFloationMenuSlideItemList Exception...", e);
        }
        return mapList;
    }

    public MyAppError addFloatingMenuSlideItem(int featureType, String featureId) {
        MyAppError error = new MyAppError();
        DBConnection db = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        Statement stmt = null;
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            stmt = db.connection.createStatement();
            String sql = "SELECT * from floating_menu_slide_items WHERE featureType = " + featureType + " AND featureId= '" + featureId + "'";
            rs = stmt.executeQuery(sql);
            if (rs.next()) {
                error.setERROR_TYPE(MyAppError.OTHERERROR);
                error.setErrorMessage("Duplicate Content");
                RingLogger.getConfigPortalLogger().debug("Duplicate Content");
                return error;
            }
            sql = "INSERT INTO floating_menu_slide_items(featureType, featureId) VALUES(?, ?);";
            int i = 1;
            ps = db.connection.prepareStatement(sql);
            ps.setInt(i++, featureType);
            ps.setString(i++, featureId);
            ps.execute();
        } catch (SQLException e) {
            error.setERROR_TYPE(MyAppError.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            RingLogger.getConfigPortalLogger().debug("addFloationMenuSlideItem SQL Exception --> " + e);
        } catch (Exception e) {
            error.setERROR_TYPE(MyAppError.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            RingLogger.getConfigPortalLogger().debug("addFloationMenuSlideItem Exception --> " + e);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (db != null) {
                    authdbconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError deleteFloatingMenuSlideItem(int featureType, String featureId) {
        MyAppError error = new MyAppError();
        DBConnection db = null;
        Statement stmt = null;
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            stmt = db.connection.createStatement();
            String sql = "DELETE FROM floating_menu_slide_items WHERE featureType = " + featureType + " AND featureId = '" + featureId + "';";
            int a = stmt.executeUpdate(sql);
            RingLogger.getConfigPortalLogger().debug("delete success");
        } catch (SQLException e) {
            error.setERROR_TYPE(MyAppError.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            RingLogger.getConfigPortalLogger().debug("deleteFloationMenuSlideItem SQL Exception --> " + e);
        } catch (Exception e) {
            error.setERROR_TYPE(MyAppError.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            RingLogger.getConfigPortalLogger().debug("deleteFloationMenuSlideItem Exception --> " + e);
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (db != null) {
                    authdbconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }
}
