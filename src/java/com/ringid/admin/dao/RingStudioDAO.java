package com.ringid.admin.dao;

import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.dto.RingStudioDTO;
import com.ringid.admin.utils.MyAppError;
import authdbconnector.DBConnection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class RingStudioDAO {

    private static RingStudioDAO instance = null;

    private RingStudioDAO() {
    }

    public static RingStudioDAO getInstance() {
        if (instance == null) {
            createInstance();
        }
        return instance;
    }

    private synchronized static void createInstance() {
        if (instance == null) {
            instance = new RingStudioDAO();
        }
    }

    public ArrayList<RingStudioDTO> getImageAndCountryCodeList() {
        ArrayList<RingStudioDTO> ringStudioDTOs = new ArrayList<>();
        DBConnection db = null;
        Statement stmt = null;
        ResultSet rs = null;
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            stmt = db.connection.createStatement();
            String sql = "SELECT id, imageName, countryCode FROM ringstudioimage where status = 1;";
            rs = stmt.executeQuery(sql);
            while (rs.next()) {
                RingStudioDTO dto = new RingStudioDTO();
                dto.setId(rs.getInt("id"));
                dto.setImageName(rs.getString("imageName"));
                dto.setCountryCode(rs.getString("countryCode"));
                ringStudioDTOs.add(dto);
            }
            RingLogger.getConfigPortalLogger().debug(sql + "ringStudioDTOs ::" + ringStudioDTOs.size());

        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().debug("ringStudioDTOs List Exception...", e);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (db != null) {
                    authdbconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
        }
        return ringStudioDTOs;
    }

    public MyAppError addRingStudioInfo(RingStudioDTO dto) {
        MyAppError error = new MyAppError();
        DBConnection db = null;
        PreparedStatement ps = null;
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            String query = "INSERT INTO ringstudio(device, title, appType, countryCode, url, imageName, displayTitle, displayFloatingMenu, supported_countries) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?);";
            int i = 1;

            ps = db.connection.prepareStatement(query);
            ps.setInt(i++, dto.getDevice());
            ps.setString(i++, dto.getTitle());
            ps.setInt(i++, dto.getAppType());
            ps.setString(i++, dto.getCountryCode());
            ps.setString(i++, dto.getUrl());
            ps.setString(i++, dto.getImageName());
            ps.setInt(i++, dto.getDisplayTitle());
            ps.setInt(i++, dto.getDisplayFloatingMenu());
            String countries = "";
            if (dto.getSupportedCountries() != null) {
                int j = 0;
                for (String cnty : dto.getSupportedCountries()) {
                    if (j != 0) {
                        countries += ",";
                    }
                    j = 1;
                    countries += cnty;
                }
            }
            ps.setString(i++, countries);

            ps.executeUpdate();
        } catch (SQLException e) {
            error.setERROR_TYPE(MyAppError.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            RingLogger.getConfigPortalLogger().debug("add Single addRingStudioInfo SQL Exception --> " + e);
        } catch (Exception e) {
            error.setERROR_TYPE(MyAppError.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            RingLogger.getConfigPortalLogger().debug("add Single addRingStudioInfo Exception --> " + e);
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (db != null) {
                    authdbconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError updateRingStudio(RingStudioDTO dto) {
        MyAppError error = new MyAppError();
        DBConnection db = null;
        PreparedStatement ps = null;
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            String query = "UPDATE ringstudio SET title = ?, url = ?, displayTitle = ?, displayFloatingMenu = ?, countryCode = ?, imageName = ?, supported_countries = ? WHERE id = ?;";
            int i = 1;

            ps = db.connection.prepareStatement(query);
            ps.setString(i++, dto.getTitle());
            ps.setString(i++, dto.getUrl());
            ps.setInt(i++, dto.getDisplayTitle());
            ps.setInt(i++, dto.getDisplayFloatingMenu());
            ps.setString(i++, dto.getCountryCode());
            ps.setString(i++, dto.getImageName());
            String countries = "";
            if (dto.getSupportedCountries() != null) {
                int j = 0;
                for (String cnty : dto.getSupportedCountries()) {
                    if (j != 0) {
                        countries += ",";
                    }
                    j = 1;
                    countries += cnty;
                }
            }
            ps.setString(i++, countries);
            ps.setInt(i++, dto.getId());
            ps.executeUpdate();
        } catch (SQLException e) {
            error.setERROR_TYPE(error.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            RingLogger.getConfigPortalLogger().debug("updateRingStudio SQL Exception --> " + e);
        } catch (Exception e) {
            error.setERROR_TYPE(error.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            RingLogger.getConfigPortalLogger().debug("updateRingStudio Exception --> " + e);
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (db != null) {
                    authdbconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError deleteRingStudio(int id) {
        MyAppError error = new MyAppError();
        DBConnection db = null;
        Statement stmt = null;
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            stmt = db.connection.createStatement();
            String sql = "DELETE FROM ringstudio WHERE id = " + id + ";";
            int a = stmt.executeUpdate(sql);
            if (a > 0) {
                RingLogger.getConfigPortalLogger().debug("delete success");
            } else {
                RingLogger.getConfigPortalLogger().debug("delete failed : " + id);
            }
        } catch (SQLException e) {
            error.setERROR_TYPE(MyAppError.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            RingLogger.getConfigPortalLogger().debug("deleteRingStudio SQL Exception --> " + e);
        } catch (Exception e) {
            error.setERROR_TYPE(MyAppError.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            RingLogger.getConfigPortalLogger().debug("deleteRingStudio Exception --> " + e);
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (db != null) {
                    authdbconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public ArrayList<RingStudioDTO> getRingStudioList() {
        ArrayList<RingStudioDTO> ringStudioDTOs = new ArrayList<>();
        DBConnection db = null;
        Statement stmt = null;
        ResultSet rs = null;
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            stmt = db.connection.createStatement();
            String sql = "SELECT id, device, title, appType, countryCode, url, imageName, displayTitle, displayFloatingMenu, supported_countries FROM ringstudio;";
            rs = stmt.executeQuery(sql);
            while (rs.next()) {
                RingStudioDTO dto = new RingStudioDTO();
                dto.setId(rs.getInt("id"));
                dto.setDevice(rs.getInt("device"));
                dto.setTitle(rs.getString("title"));
                dto.setAppType(rs.getInt("appType"));
                dto.setUrl(rs.getString("url"));
                dto.setImageName(rs.getString("imageName"));
                dto.setDisplayTitle(rs.getInt("displayTitle"));
                dto.setDisplayFloatingMenu(rs.getInt("displayFloatingMenu"));
                dto.setCountryCode(rs.getString("countryCode"));
                String supporedCountries = rs.getString("supported_countries");
                if (supporedCountries != null && supporedCountries.length() > 0) {
                    dto.setSupportedCountries(supporedCountries.split(","));
                }
                ringStudioDTOs.add(dto);
            }
            RingLogger.getConfigPortalLogger().debug(sql + "ringStudioDTOs ::" + ringStudioDTOs.size());

        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().debug("ringStudioDTOs List Exception...", e);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (db != null) {
                    authdbconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
        }
        return ringStudioDTOs;
    }

    public RingStudioDTO getRingStudioDTO(int id) {
        RingStudioDTO dto = new RingStudioDTO();
        DBConnection db = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {

            db = authdbconnector.DBConnector.getInstance().makeConnection();
            String sql = "SELECT device, title, appType, countryCode, url, imageName, displayTitle, displayFloatingMenu, supported_countries FROM ringstudio where id = ? ;";
            ps = db.connection.prepareStatement(sql);
            ps.setInt(1, id);
            rs = ps.executeQuery();

            if (rs.next()) {
                dto.setId(id);
                dto.setDevice(rs.getInt("device"));
                dto.setTitle(rs.getString("title"));
                dto.setAppType(rs.getInt("appType"));
                dto.setUrl(rs.getString("url"));
                dto.setImageName(rs.getString("imageName"));
                dto.setDisplayTitle(rs.getInt("displayTitle"));
                dto.setDisplayFloatingMenu(rs.getInt("displayFloatingMenu"));
                dto.setCountryCode(rs.getString("countryCode"));
                String supporedCountries = rs.getString("supported_countries");
                if (supporedCountries != null && supporedCountries.length() > 0) {
                    dto.setSupportedCountries(supporedCountries.split(","));
                }
            }
            RingLogger.getConfigPortalLogger().debug(sql + "getRingStudioDTO ::" + dto.getId());

        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().debug("getRingStudioDTO Exception...", e);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (db != null) {
                    authdbconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
        }
        return dto;
    }
}
