/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.dao;

import com.ringid.admin.dto.AppStoresDTO;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.utils.Constants;

import com.ringid.admin.utils.MyAppError;
import authdbconnector.DBConnection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import org.ringid.utilities.AppConstants;

/**
 *
 * @author mamun
 * @Date Jan 28, 2017
 */
public class AppStoresDAO {

    private static AppStoresDAO instance = null;

    private AppStoresDAO() {
    }

    public static AppStoresDAO getInstance() {
        if (instance == null) {
            createInstance();
        }
        return instance;
    }

    private synchronized static void createInstance() {
        if (instance == null) {
            instance = new AppStoresDAO();
        }
    }

    private String getClause(String optionalSql) {
        return (optionalSql.length() < 1 ? " WHERE " : " OR ");
    }

    public ArrayList<AppStoresDTO> getAppStoresList(String queryString) {
        ArrayList<AppStoresDTO> appStoresDTOs = new ArrayList<>();
        int deviceType = 0;
        if (queryString != null && queryString.length() > 0) {
            for (int i = 1; i < Constants.DEVICE.length; i++) {
                if (Constants.DEVICE[i].toLowerCase().contains(queryString)) {
                    deviceType = i;
                }
            }
        }
        DBConnection db = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            String sql = "SELECT id, device_type, version FROM appstore ", optionalSql = "";
            if (deviceType > 0) {
                optionalSql += getClause(optionalSql) + " device_type = ?";
            }
            if (queryString != null && queryString.length() > 0) {
                optionalSql += getClause(optionalSql) + " version LIKE ?";
            }
            sql += optionalSql;
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            ps = db.connection.prepareStatement(sql);
            if (deviceType > 0) {
                ps.setInt(1, deviceType);
            }
            if (queryString != null && queryString.length() > 0) {
                ps.setString(2, "%" + queryString + "%");
            }
            rs = ps.executeQuery();
            while (rs.next()) {
                AppStoresDTO dto = new AppStoresDTO();
                dto.setId(rs.getInt("id"));
                dto.setDevice(rs.getInt("device_type"));
                dto.setVersion(rs.getString("version"));
                if (dto.getDevice() < 6) {
                    dto.setDeviceStr(AppConstants.DEVICES[dto.getDevice()]);
                } else {
                    dto.setDeviceStr("" + dto.getDevice());
                }

                appStoresDTOs.add(dto);
            }
            RingLogger.getConfigPortalLogger().debug(sql + "::" + appStoresDTOs.size());
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().debug("AppStores List Exception...", e);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (db != null) {
                    authdbconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
        }
        return appStoresDTOs;
    }

    public MyAppError addAppStoresInfo(AppStoresDTO dto) {
        MyAppError error = new MyAppError();
        DBConnection db = null;
        PreparedStatement ps = null;
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            String query = "INSERT INTO appstore(device_type, version) VALUES (?, ?);";
            ps = db.connection.prepareStatement(query);
            ps.setInt(1, dto.getDevice());
            ps.setString(2, dto.getVersion());
            ps.execute();
        } catch (SQLException e) {
            error.setERROR_TYPE(MyAppError.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            RingLogger.getConfigPortalLogger().debug("add Single AppStoresInfo SQL Exception --> " + e);
        } catch (Exception e) {
            error.setERROR_TYPE(MyAppError.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            RingLogger.getConfigPortalLogger().debug("add Single AppStoresInfo Exception --> " + e);
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (db != null) {
                    authdbconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError updateAppStores(AppStoresDTO dto) {
        MyAppError error = new MyAppError();
        DBConnection db = null;
        PreparedStatement ps = null;
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            String query = "UPDATE appstore SET device_type = ?, version = ? WHERE id = ?;";
            ps = db.connection.prepareStatement(query);
            ps.setInt(1, dto.getDevice());
            ps.setString(2, dto.getVersion());
            ps.setInt(3, dto.getId());
            ps.execute();
        } catch (SQLException e) {
            error.setERROR_TYPE(MyAppError.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            RingLogger.getConfigPortalLogger().debug("updateAppStores SQL Exception --> " + e);
        } catch (Exception e) {
            error.setERROR_TYPE(MyAppError.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            RingLogger.getConfigPortalLogger().debug("updateAppStores Exception --> " + e);
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (db != null) {
                    authdbconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError deleteAppStores(int id) {
        MyAppError error = new MyAppError();
        DBConnection db = null;
        Statement stmt = null;
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            stmt = db.connection.createStatement();
            String sql = "DELETE FROM appstore WHERE id = " + id + ";";
            int a = stmt.executeUpdate(sql);
            RingLogger.getConfigPortalLogger().debug("delete success");
        } catch (SQLException e) {
            error.setERROR_TYPE(MyAppError.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            RingLogger.getConfigPortalLogger().debug("deleteAppStores SQL Exception --> " + e);
        } catch (Exception e) {
            error.setERROR_TYPE(MyAppError.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            RingLogger.getConfigPortalLogger().debug("deleteAppStores Exception --> " + e);
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (db != null) {
                    authdbconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public ArrayList<AppStoresDTO> getAppStoresList() {
        ArrayList<AppStoresDTO> appStoresDTOs = new ArrayList<>();
        DBConnection db = null;
        Statement stmt = null;
        ResultSet rs = null;
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            stmt = db.connection.createStatement();
            String sql = "SELECT id, device_type, version FROM appstore;";
            rs = stmt.executeQuery(sql);
            while (rs.next()) {
                AppStoresDTO dto = new AppStoresDTO();
                dto.setId(rs.getInt("id"));
                dto.setDevice(rs.getInt("device_type"));
                dto.setVersion(rs.getString("version"));
                switch (dto.getDevice()) {
                    case AppConstants.PC:
                        dto.setDeviceStr("PC");
                        break;
                    case AppConstants.ANDROID:
                        dto.setDeviceStr("ANDROID");
                        break;
                    case AppConstants.I_PHONE:
                        dto.setDeviceStr("I_PHONE");
                        break;
                    case AppConstants.WINDOWS:
                        dto.setDeviceStr("WINDOWS");
                        break;
                    case AppConstants.WEB:
                        dto.setDeviceStr("WEB");
                        break;
                    default:
                        dto.setDeviceStr("" + dto.getDevice());
                        break;
                }

                appStoresDTOs.add(dto);
            }
            RingLogger.getConfigPortalLogger().debug(sql + "::" + appStoresDTOs.size());
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().debug("AppStores List Exception...", e);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (db != null) {
                    authdbconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
        }
        return appStoresDTOs;
    }

    public AppStoresDTO getAppStoresDTO(int id) {
        AppStoresDTO dto = new AppStoresDTO();
        DBConnection db = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            String sql = "SELECT id, device_type, version FROM appstore where id = ? ;";
            ps = db.connection.prepareStatement(sql);
            ps.setInt(1, id);
            rs = ps.executeQuery();
            while (rs.next()) {
                dto.setId(rs.getInt("id"));
                dto.setDevice(rs.getInt("device_type"));
                dto.setVersion(rs.getString("version"));
                switch (dto.getDevice()) {
                    case AppConstants.PC:
                        dto.setDeviceStr("PC");
                        break;
                    case AppConstants.ANDROID:
                        dto.setDeviceStr("ANDROID");
                        break;
                    case AppConstants.I_PHONE:
                        dto.setDeviceStr("I_PHONE");
                        break;
                    case AppConstants.WINDOWS:
                        dto.setDeviceStr("WINDOWS");
                        break;
                    case AppConstants.WEB:
                        dto.setDeviceStr("WEB");
                        break;
                    default:
                        dto.setDeviceStr("" + dto.getDevice());
                        break;
                }
            }
            RingLogger.getConfigPortalLogger().debug(sql + "::" + dto.getId());

        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().debug("AppStores List Exception...", e);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (db != null) {
                    authdbconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
        }
        return dto;
    }
}
