/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.dao;

import com.google.gson.Gson;
import com.ringid.admin.dto.RegistrationSummaryDTO;
import com.ringid.admin.utils.log.RingLogger;
import authdbconnector.DBConnection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

/**
 *
 * @author Rabby
 */
public class DashboardDAO {

    private static DashboardDAO instance;

    private DashboardDAO() {
    }

    public static DashboardDAO getInstance() {
        if (instance == null) {
            createInstance();
        }
        return instance;
    }

    private static synchronized void createInstance() {
        if (instance == null) {
            instance = new DashboardDAO();
        }
    }

    public List<RegistrationSummaryDTO> getUserCount(long startTime, long endTime) {
        RingLogger.getConfigPortalLogger().debug("[getUserCount] StartTime -> " + startTime + " endTime -> " + endTime);
        List<RegistrationSummaryDTO> registrationList = new ArrayList<>();
        DBConnection db = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            String sql = "SELECT (registrationDate DIV (24*60*60*1000))*(24*60*60*1000) AS registrationDateNew, count(*) AS total_count "
                    + "FROM users "
                    + "WHERE (registrationDate DIV (24*60*60*1000))*(24*60*60*1000) BETWEEN " + startTime + " AND " + endTime + " "
                    + "GROUP BY registrationDateNew;";
            RingLogger.getConfigPortalLogger().debug("[getUserCount] sql -> " + sql);
            ps = db.connection.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                RegistrationSummaryDTO dto = new RegistrationSummaryDTO();
                dto.setTime(rs.getLong("registrationDateNew"));
                dto.setRegistrationCount(rs.getLong("total_count"));
                dto.setDate(parseTime(dto.getTime()));
                registrationList.add(dto);
            }
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [getUserCount]-> " + e);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (db != null) {
                    authdbconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
        }
        RingLogger.getConfigPortalLogger().debug("[getUserCount] registrationList.size -> " + registrationList.size());
        return registrationList;
    }

    private static long parseDate(String text) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
        return sdf.parse(text).getTime();
    }

    private static String parseTime(long time) throws ParseException {
        Date date = new Date(time);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
        return sdf.format(date);
    }

    public static void main(String args[]) {
        try {
            DashboardDAO dao = new DashboardDAO();
            long startTime = parseDate("01/01/2018");
            long endTime = parseDate("12/03/2018");
            List<RegistrationSummaryDTO> registrationList = dao.getUserCount(startTime, endTime);
            System.out.println("Time                 Date             RegistrationCount");
            long sumCount = 0;
            for (RegistrationSummaryDTO dto : registrationList) {
                System.out.println(dto.getTime() + "        " + dto.getDate() + "       " + dto.getRegistrationCount());
                sumCount += dto.getRegistrationCount();
            }
            System.out.println("\nTotalRow -> " + registrationList.size() + " TotalRegistration -> " + sumCount);
            System.out.println("List.json -> " + new Gson().toJson(registrationList));
        } catch (Exception e) {
            System.out.println("Exception in main -> " + e);
        }
    }
}
