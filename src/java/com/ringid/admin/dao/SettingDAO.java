/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.dao;

import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.dto.SettingDTO;
import com.ringid.admin.actions.userFeatureManagement.UserFeatureTaskScheduler;
import com.ringid.admin.dto.adminAuth.UserRoleDTO;
import com.ringid.admin.utils.Constants;
import com.ringid.admin.utils.MyAppError;
import authdbconnector.DBConnection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import ringid.LoginTaskScheduler;
import ringid.command.CommandTaskScheduler;

/**
 *
 * @author ipvision
 */
public class SettingDAO {

    private static SettingDAO instance = null;

    private SettingDAO() {
    }

    public static SettingDAO getInstance() {
        if (instance == null) {
            createInstance();
        }
        return instance;
    }

    private synchronized static void createInstance() {
        if (instance == null) {
            instance = new SettingDAO();
        }
    }

    public MyAppError addSettingPermission(long id, int roleIds[]) {
        MyAppError error = new MyAppError();
        DBConnection db = null;
        Statement stmt = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            stmt = db.connection.createStatement();
            Set<Integer> ids = new HashSet<>();
            if (roleIds != null) {
                for (int i = 0; i < roleIds.length; i++) {
                    ids.add(roleIds[i]);
                }
            }
            List<UserRoleDTO> userRoles = UserFeatureTaskScheduler.getInstance().getUserRoleList(new UserRoleDTO());
            for (UserRoleDTO userRoleDTO : userRoles) {
                String sql = "SELECT * FROM settings_permission_map WHERE id = " + id + " AND role_id = " + userRoleDTO.getId();
                rs = stmt.executeQuery(sql);
                if (rs.next()) {
                    if (!ids.contains(userRoleDTO.getId())) {
                        sql = "DELETE FROM settings_permission_map WHERE id = " + id + " AND role_id = " + userRoleDTO.getId();
                        stmt.execute(sql);
                    }
                } else if (ids.contains(userRoleDTO.getId())) {
                    sql = "INSERT INTO settings_permission_map(id, role_id) VALUES(?, ?)";
                    ps = db.connection.prepareStatement(sql);
                    int index = 1;
                    ps.setLong(index++, id);
                    ps.setInt(index++, userRoleDTO.getId());
                    ps.execute();
                }
            }
        } catch (Exception e) {
            error.setERROR_TYPE(MyAppError.OTHERERROR);
            error.setErrorMessage("Insertion failed");
            RingLogger.getConfigPortalLogger().error("Exception in [addSettingPermission] -> " + e);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (db != null) {
                    authdbconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public Set<Integer> getSettingPermissionById(long id) {
        Set<Integer> roleIds = new HashSet<>();
        DBConnection db = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            String sql = "SELECT * FROM settings_permission_map WHERE id = ?";
            ps = db.connection.prepareStatement(sql);
            int index = 1;
            ps.setLong(index++, id);
            rs = ps.executeQuery();
            while (rs.next()) {
                roleIds.add(rs.getInt("role_id"));
            }
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [getSettingPermissionById] -> " + e);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (db != null) {
                    authdbconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
        }
        return roleIds;
    }

    public Map<Long, Set<Integer>> getSettingPermissionMap() {
        Map<Long, Set<Integer>> permissionMap = new HashMap<>();
        DBConnection db = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            String sql = "SELECT * FROM settings_permission_map";
            ps = db.connection.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                if (permissionMap.containsKey(rs.getLong("id"))) {
                    permissionMap.get(rs.getLong("id")).add(rs.getInt("role_id"));
                } else {
                    Set<Integer> roleSet = new HashSet<>();
                    roleSet.add(rs.getInt("role_id"));
                    permissionMap.put(rs.getLong("id"), roleSet);
                }
            }
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [getSettingPermissionMap] -> " + e);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (db != null) {
                    authdbconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
        }
        return permissionMap;
    }

    public Map<Long, SettingDTO> getSettingList() {
        Map<Long, SettingDTO> settingHashMapList = new HashMap<>();
        DBConnection db = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            String sql = "SELECT * FROM settings;";
            ps = db.connection.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                SettingDTO dto = new SettingDTO();
                dto.setId(rs.getLong("id"));
                dto.setName(rs.getString("name"));
                dto.setSettingsValue(rs.getString("settingsValue"));
                dto.setDataType(rs.getInt("dataType"));
                settingHashMapList.put(dto.getId(), dto);
            }
            RingLogger.getConfigPortalLogger().debug(sql + "::" + settingHashMapList.size());
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().debug("Log Setting List Exception...", e);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (db != null) {
                    authdbconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
        }
        return settingHashMapList;
    }

    public MyAppError addSettingInfo(SettingDTO dto) {
        MyAppError error = new MyAppError();
        DBConnection db = null;
        PreparedStatement ps = null;
        try {
            LoginTaskScheduler.getInstance().initSessionLessCommunication(Constants.USERID);
            long generatedId = CommandTaskScheduler.getInstance().getGeneralTableId(Constants.USERID, "settings");
            RingLogger.getConfigPortalLogger().debug("addSettingInfo generatedId --> " + generatedId);
            if (generatedId > 0) {
                db = authdbconnector.DBConnector.getInstance().makeConnection();
                String query = "INSERT INTO settings (id, name, settingsValue, dataType) VALUES (?, ?, ?, ?);";
                int i = 1;
                ps = db.connection.prepareStatement(query);
                ps.setLong(i++, generatedId);
                ps.setString(i++, dto.getName());
                ps.setString(i++, dto.getSettingsValue());
                ps.setInt(i++, dto.getDataType());
                if (ps.executeUpdate() > 0) {
                    RingLogger.getConfigPortalLogger().debug("[addSettingInfo] sql --> " + query + " :: success");
                } else {
                    error.setERROR_TYPE(MyAppError.VALIDATIONERROR);
                    error.setErrorMessage("VALIDATIONERROR");
                }
            } else {
                error.setERROR_TYPE(MyAppError.OTHERERROR);
                error.setErrorMessage("General Table Id " + generatedId);
            }
        } catch (SQLException e) {
            error.setERROR_TYPE(MyAppError.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            RingLogger.getConfigPortalLogger().debug("addSettingInfo SQL Exception --> " + e);
        } catch (Exception e) {
            error.setERROR_TYPE(MyAppError.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            RingLogger.getConfigPortalLogger().debug("addSettingInfo Exception --> " + e);
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (db != null) {
                    authdbconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
            try {
                LoginTaskScheduler.getInstance().destroySessionLessCommunication(Constants.USERID);
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError deleteSettingInfo(long id) {
        MyAppError error = new MyAppError();
        DBConnection db = null;
        PreparedStatement ps = null;
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            String sql = "DELETE FROM settings WHERE id = ?;";
            ps = db.connection.prepareStatement(sql);
            ps.setLong(1, id);
            int a = ps.executeUpdate();
        } catch (SQLException e) {
            error.setERROR_TYPE(MyAppError.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            RingLogger.getConfigPortalLogger().debug("deleteSetting SQL Exception --> " + e);
        } catch (Exception e) {
            error.setERROR_TYPE(MyAppError.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            RingLogger.getConfigPortalLogger().debug("deleteSetting Exception --> " + e);
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (db != null) {
                    authdbconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError updateSettingInfo(SettingDTO dto) {
        MyAppError error = new MyAppError();
        DBConnection db = null;
        PreparedStatement ps = null;
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            String query = "UPDATE settings SET name = ?, settingsValue = ?, dataType = ? WHERE id = ?;";
            int i = 1;
            ps = db.connection.prepareStatement(query);
            ps.setString(i++, dto.getName());
            ps.setString(i++, dto.getSettingsValue());
            ps.setInt(i++, dto.getDataType());
            ps.setLong(i++, dto.getId());
            ps.executeUpdate();
        } catch (SQLException e) {
            error.setERROR_TYPE(MyAppError.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            RingLogger.getConfigPortalLogger().debug("updateSetting SQL Exception --> " + e);
        } catch (Exception e) {
            error.setERROR_TYPE(MyAppError.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            RingLogger.getConfigPortalLogger().debug("updateSetting Exception --> " + e);
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (db != null) {
                    authdbconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError updateSettingInfoList(List<SettingDTO> dtoList) {
        MyAppError error = new MyAppError();
        DBConnection db = null;
        PreparedStatement ps = null;
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            String query = "UPDATE settings SET name = ?, settingsValue = ?, dataType = ? WHERE id = ?;";
            ps = db.connection.prepareStatement(query);
            int i = 1, count = 1;
            for (SettingDTO dto : dtoList) {
                i = 1;
                ps.setString(i++, dto.getName());
                ps.setString(i++, dto.getSettingsValue());
                ps.setInt(i++, dto.getDataType());
                ps.setLong(i++, dto.getId());
                ps.addBatch();
                if (count++ % 1000 == 0) {
                    ps.executeBatch();
                    count = 0;
                }
            }
            if (count > 0) {
                ps.executeBatch();
            }
        } catch (SQLException e) {
            error.setERROR_TYPE(MyAppError.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            RingLogger.getConfigPortalLogger().debug("updateSetting SQL Exception --> " + e);
        } catch (Exception e) {
            error.setERROR_TYPE(MyAppError.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            RingLogger.getConfigPortalLogger().debug("updateSetting Exception --> " + e);
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (db != null) {
                    authdbconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }
}
