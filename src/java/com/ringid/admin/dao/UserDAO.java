/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.dao;

import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.utils.MyAppError;
import com.ringid.admin.utils.Utils;
import authdbconnector.DBConnection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Rabby
 */
public class UserDAO {

    private static UserDAO instance = null;

    private UserDAO() {
    }

    public static UserDAO getInstance() {
        if (instance == null) {
            createInstance();
        }
        return instance;
    }

    private synchronized static void createInstance() {
        if (instance == null) {
            instance = new UserDAO();
        }
    }

    public MyAppError updateRingId(String oldRingId, String newRingId) {
        MyAppError error = new MyAppError();
        DBConnection db = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            String sql = "UPDATE users SET ringID=? WHERE ringID = ?;";
            RingLogger.getConfigPortalLogger().debug("[updatePassword] sql --> UPDATE users SET ringID=" + newRingId + " WHERE ringID = '" + oldRingId + "'");
            ps = db.connection.prepareStatement(sql);
            ps.setLong(2, Long.parseLong(newRingId));
            ps.setLong(3, Long.parseLong(oldRingId));
            ps.execute();
        } catch (SQLException e) {
            error.setERROR_TYPE(MyAppError.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            RingLogger.getConfigPortalLogger().debug("updateRingId[1] --> " + e);
        } catch (Exception e) {
            error.setERROR_TYPE(MyAppError.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            RingLogger.getConfigPortalLogger().debug("updateRingId --> " + e);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (db != null) {
                    authdbconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError updatePassword(String ringID, String password) {
        MyAppError error = new MyAppError();
        DBConnection db = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            String sql = "UPDATE users SET password = ? WHERE ringID = ?;";
            RingLogger.getConfigPortalLogger().debug("[updatePassword] sql --> UPDATE users SET password = '" + password + "' WHERE ringID = '" + ringID + "'");
            ps = db.connection.prepareStatement(sql);
            ps.setString(1, password);
            ps.setLong(2, Long.parseLong(ringID));
            ps.execute();
        } catch (SQLException e) {
            error.setERROR_TYPE(MyAppError.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            RingLogger.getConfigPortalLogger().debug("updatePassword[1] --> " + e);
        } catch (Exception e) {
            error.setERROR_TYPE(MyAppError.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            RingLogger.getConfigPortalLogger().debug("updatePassword --> " + e);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (db != null) {
                    authdbconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError updateMyPhoneNumberInfo(String mobilePhone, String mobilePhoneDialingCode, String ringID, int isMyNumberVerified) {
        MyAppError error = new MyAppError();
        DBConnection db = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            String sql = "UPDATE users SET isMyNumberVerified = ?, mobilePhone = ?, mobilePhoneDialingCode = ? WHERE ringID = ?;";
            RingLogger.getConfigPortalLogger().debug("[updateMyPhoneNumberInfo] sql --> UPDATE users SET isMyNumberVerified = " + isMyNumberVerified + ", mobilePhone = '" + mobilePhone + "', mobilePhoneDialingCode = '" + mobilePhoneDialingCode + " WHERE ringID = '" + ringID + "'");
            ps = db.connection.prepareStatement(sql);
            ps.setInt(1, isMyNumberVerified);
            ps.setString(2, mobilePhone);
            ps.setString(3, mobilePhoneDialingCode);
            ps.setLong(4, Long.parseLong(ringID));
            boolean result = ps.execute();
            if (result && isMyNumberVerified == 1) {
                unverifyOthersNumber(mobilePhone, mobilePhoneDialingCode, Long.parseLong(ringID));
            }
        } catch (Exception e) {
            error.setERROR_TYPE(MyAppError.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            RingLogger.getConfigPortalLogger().debug("updateMyPhoneNumberInfo --> " + e);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (db != null) {
                    authdbconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    private void unverifyOthersNumber(String mobilePhone, String mobilePhoneDialingCode, Long ringID) {
        DBConnection db = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            ArrayList<Long> idList = new ArrayList<>();
            String sql = "SELECT ringID FROM users WHERE mobilePhone= ? AND mobilePhoneDialingCode = ? AND isMyNumberVerified = 1";
            ps = db.connection.prepareStatement(sql);
            int i = 1;
            ps.setString(i++, mobilePhone);
            ps.setString(i++, mobilePhoneDialingCode);
            rs = ps.executeQuery();
            while (rs.next()) {
                if (rs.getLong("ringID") != ringID) {
                    idList.add(rs.getLong("ringID"));
                }
            }
            if (!idList.isEmpty()) {
                sql = "UPDATE users SET isMyNumberVerified = 0, updateTime=" + System.currentTimeMillis() + " WHERE ringID IN(" + Utils.listToString(idList) + ")";
                ps = db.connection.prepareStatement(sql);
                ps.execute();
            }
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().debug("unverifyOthersNumber[2] --> " + e);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (db != null) {
                    authdbconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
        }
    }

    public int resetSignupRetryLimit(String dialingCode, String mobileNo) {
        int result = -1;
        DBConnection db = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            String sql = "UPDATE virtualringidgenerator SET vcSentTime = 0 where dilaingCode = ? and mobileNo = ?;";
            ps = db.connection.prepareStatement(sql);
            ps.setString(1, dialingCode);
            ps.setString(2, mobileNo);
            int a = ps.executeUpdate();
            if (a > 0) {
                result = 0;
            }
            RingLogger.getConfigPortalLogger().debug("[resetSignupRetryLimit] sql --> UPDATE virtualringidgenerator SET vcSentTime = 0 WHERE dilaingCode = '" + dialingCode + "' and mobileNo = '" + mobileNo + "' :: " + result);
        } catch (SQLException e) {
            result = -1;
            RingLogger.getConfigPortalLogger().debug("resetSignupRetryLimit[1] --> " + e);
        } catch (Exception e) {
            result = -1;
            RingLogger.getConfigPortalLogger().debug("resetSignupRetryLimit --> " + e);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (db != null) {
                    authdbconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
        }
        return result;
    }
}
