/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.dao;

import com.ringid.admin.dto.PermittedCountryDTO;
import com.ringid.admin.dto.PaymentMethodDTO;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.utils.MyAppError;
import authdbconnector.DBConnection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Rabby
 */
public class CoinDAO {

    private static CoinDAO instance = null;

    private CoinDAO() {
    }

    public static CoinDAO getInstance() {
        if (instance == null) {
            createInstance();
        }
        return instance;
    }

    private synchronized static void createInstance() {
        if (instance == null) {
            instance = new CoinDAO();
        }
    }

    public Map<Long, PaymentMethodDTO> getPaymentMethodHashMap() {
        Map<Long, PaymentMethodDTO> paymentMethodHashMap = new HashMap<>();
        DBConnection db = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            String sql = "SELECT id, name, isOpen FROM coinExchangePaymentMethodList;";
            ps = db.connection.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                PaymentMethodDTO dto = new PaymentMethodDTO();
                dto.setId(rs.getLong("id"));
                dto.setName(rs.getString("name"));
                dto.setIsOpen(rs.getBoolean("isOpen"));
                paymentMethodHashMap.put(dto.getId(), dto);
            }
            RingLogger.getConfigPortalLogger().debug(sql + "::" + paymentMethodHashMap.size());

        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().debug("getPaymentMethodHashMap List Exception...", e);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (db != null) {
                    authdbconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
        }
        return paymentMethodHashMap;
    }

    public MyAppError addPaymentMethod(PaymentMethodDTO dto) {
        MyAppError error = new MyAppError();
        DBConnection db = null;
        PreparedStatement ps = null;
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            String query = "INSERT INTO coinExchangePaymentMethodList (id, name, isOpen) VALUES (?, ?, ?);";
            ps = db.connection.prepareStatement(query);
            int i = 1;
            ps.setLong(i++, dto.getId());
            ps.setString(i++, dto.getName());
            ps.setBoolean(i++, dto.isIsOpen());
            ps.executeUpdate();
        } catch (SQLException e) {
            error.setERROR_TYPE(MyAppError.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            RingLogger.getConfigPortalLogger().debug("addPaymentMethod SQL Exception --> " + e);
        } catch (Exception e) {
            error.setERROR_TYPE(MyAppError.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            RingLogger.getConfigPortalLogger().debug("addPaymentMethod Exception --> " + e);
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (db != null) {
                    authdbconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError updatePaymentMethod(PaymentMethodDTO dto) {
        MyAppError error = new MyAppError();
        DBConnection db = null;
        PreparedStatement ps = null;
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            String query = "UPDATE coinExchangePaymentMethodList SET name = ?, isOpen = ? WHERE id = ?;";
            ps = db.connection.prepareStatement(query);
            int i = 1;
            ps.setString(i++, dto.getName());
            ps.setBoolean(i++, dto.isIsOpen());
            ps.setLong(i++, dto.getId());
            ps.executeUpdate();
        } catch (SQLException e) {
            error.setERROR_TYPE(error.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            RingLogger.getConfigPortalLogger().debug("updatePaymentMethod SQL Exception --> " + e);
        } catch (Exception e) {
            error.setERROR_TYPE(error.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            RingLogger.getConfigPortalLogger().debug("updatePaymentMethod Exception --> " + e);
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (db != null) {
                    authdbconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError deletePaymentMethodById(long id) {
        MyAppError error = new MyAppError();
        DBConnection db = null;
        PreparedStatement ps = null;
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            String sql = "DELETE FROM coinExchangePaymentMethodList WHERE id = ?;";
            ps = db.connection.prepareStatement(sql);
            ps.setLong(1, id);
            ps.executeUpdate();
            RingLogger.getConfigPortalLogger().debug("delete success");
        } catch (SQLException e) {
            error.setERROR_TYPE(MyAppError.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            RingLogger.getConfigPortalLogger().debug("deletePaymentMethodById SQL Exception --> " + e);
        } catch (Exception e) {
            error.setERROR_TYPE(MyAppError.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            RingLogger.getConfigPortalLogger().debug("deletePaymentMethodById Exception --> " + e);
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (db != null) {
                    authdbconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public Map<String, PermittedCountryDTO> getPermittedCountryList() {
        Map<String, PermittedCountryDTO> permittedCountryDTOMap = new HashMap<>();
        DBConnection db = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            String sql = "SELECT countryName, countryShortCode, currency, mobilePhoneDialingCode, signupcash, signupcoin, "
                    + " signupringbit, referralcash, referralcoin, referralringbit, cashoutlimit, status, cashSupportedPlatform "
                    + " FROM rewardpermittedcountry";
            ps = db.connection.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                PermittedCountryDTO dto = new PermittedCountryDTO();
                dto.setCountryName(rs.getString("countryName"));
                dto.setCountryShortCode(rs.getString("countryShortCode"));
                dto.setCurrency(rs.getString("currency"));
                dto.setMobilePhoneDialingCode(rs.getString("mobilePhoneDialingCode"));
                dto.setSignupCash(rs.getInt("signupcash"));
                dto.setSignupCoin(rs.getInt("signupcoin"));
                dto.setSignupRingbit(rs.getInt("signupringbit"));
                dto.setReferralCash(rs.getInt("referralcash"));
                dto.setReferralCoin(rs.getInt("referralcoin"));
                dto.setReferralRingbit(rs.getInt("referralringbit"));
                dto.setCashoutlimit(rs.getInt("cashoutlimit"));
                dto.setStatus(rs.getInt("status"));
                dto.setSupportedPlatform(rs.getString("cashSupportedPlatform"));

                permittedCountryDTOMap.put(dto.getCountryShortCode(), dto);
            }
            RingLogger.getConfigPortalLogger().debug(sql + "::" + permittedCountryDTOMap.size());
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().debug("[CoinDAO] getPermittedCountryList Exception...", e);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (db != null) {
                    authdbconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
        }
        return permittedCountryDTOMap;
    }

    public int getTotalCountOfWalletPermittedCountry() {
        int total = 0;
        DBConnection db = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            String sql = "SELECT count(countryShortCode) as total from rewardpermittedcountry;";
            RingLogger.getConfigPortalLogger().debug("## sql --> " + sql);
            ps = db.connection.prepareStatement(sql);
            rs = ps.executeQuery();
            if (rs.next()) {
                total = rs.getInt("total");
            }
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("[CoinDAO] Exception [getTotalCount] --> " + e);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (db != null) {
                    authdbconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
        }
        return total;
    }

    public MyAppError updateRewardPermittedCountryInfo(PermittedCountryDTO dto) {
        MyAppError error = new MyAppError();
        DBConnection db = null;
        PreparedStatement ps = null;
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            String query = "INSERT INTO rewardpermittedcountry "
                    + " (countryName, countryShortCode, currency, mobilePhoneDialingCode, signupcash, signupcoin, signupringbit, "
                    + " referralcash, referralcoin, referralringbit, cashoutlimit, status, cashSupportedPlatform) "
                    + " VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) "
                    + " ON DUPLICATE KEY UPDATE currency = ?, mobilePhoneDialingCode = ?, signupcash = ?, signupcoin = ?, "
                    + " signupringbit = ?, referralcash = ?, referralcoin = ?, referralringbit = ?, cashoutlimit = ?, status = ?, cashSupportedPlatform = ?;";
            ps = db.connection.prepareStatement(query);   // create the mysql insert preparedstatement
            int i = 1;
            ps.setString(i++, dto.getCountryName());
            ps.setString(i++, dto.getCountryShortCode());
            ps.setString(i++, dto.getCurrency());
            ps.setString(i++, dto.getMobilePhoneDialingCode());
            ps.setInt(i++, dto.getSignupCash());
            ps.setInt(i++, dto.getSignupCoin());
            ps.setInt(i++, dto.getSignupRingbit());
            ps.setInt(i++, dto.getReferralCash());
            ps.setInt(i++, dto.getReferralCoin());
            ps.setInt(i++, dto.getReferralRingbit());
            ps.setInt(i++, dto.getCashoutlimit());
            ps.setInt(i++, dto.getStatus());
            ps.setString(i++, dto.getSupportedPlatform());

            ps.setString(i++, dto.getCurrency());
            ps.setString(i++, dto.getMobilePhoneDialingCode());
            ps.setInt(i++, dto.getSignupCash());
            ps.setInt(i++, dto.getSignupCoin());
            ps.setInt(i++, dto.getSignupRingbit());
            ps.setInt(i++, dto.getReferralCash());
            ps.setInt(i++, dto.getReferralCoin());
            ps.setInt(i++, dto.getReferralRingbit());
            ps.setInt(i++, dto.getCashoutlimit());
            ps.setInt(i++, dto.getStatus());
            ps.setString(i++, dto.getSupportedPlatform());

            ps.executeUpdate();
        } catch (SQLException e) {
            RingLogger.getConfigPortalLogger().debug("updateRewardPermittedCountryInfo --> " + e);
            error.setERROR_TYPE(MyAppError.DBERROR);
            error.setErrorMessage(e.toString());
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().debug("updateRewardPermittedCountryInfo --> " + e);
            error.setERROR_TYPE(MyAppError.OTHERERROR);
            error.setErrorMessage(e.toString());
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (db != null) {
                    authdbconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError deleteRewardPermittedCountryInfo(String countryShortCode) {
        MyAppError error = new MyAppError();
        DBConnection db = null;
        PreparedStatement ps = null;
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            String query = "DELETE FROM rewardpermittedcountry WHERE countryShortCode = ?;";
            ps = db.connection.prepareStatement(query);

            int i = 1;
            ps.setString(i++, countryShortCode);

            ps.executeUpdate();
        } catch (SQLException e) {
            RingLogger.getConfigPortalLogger().debug("deleteRewardPermittedCountryInfo --> " + e);
            error.setERROR_TYPE(MyAppError.DBERROR);
            error.setErrorMessage(e.toString());
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().debug("deleteRewardPermittedCountryInfo --> " + e);
            error.setERROR_TYPE(MyAppError.OTHERERROR);
            error.setErrorMessage(e.toString());
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (db != null) {
                    authdbconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }
}
