/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.dao;

import com.ringid.admin.dto.AppConstantsDTO;
import com.ringid.admin.utils.log.RingLogger;

import com.ringid.admin.utils.MyAppError;
import authdbconnector.DBConnection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Kazi Nasir Uddin Oly
 * @Date Jun 14, 2016
 */
public class AppConstantsDAO {

    private static AppConstantsDAO instance = null;

    private AppConstantsDAO() {
    }

    public static AppConstantsDAO getInstance() {
        if (instance == null) {
            createInstance();
        }
        return instance;
    }

    private synchronized static void createInstance() {
        if (instance == null) {
            instance = new AppConstantsDAO();
        }
    }

    public MyAppError addAppConstantsInfo(AppConstantsDTO dto) {
        MyAppError error = new MyAppError();
        DBConnection db = null;
        PreparedStatement ps = null;
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            String query = "INSERT INTO appconstants(platform, app_type, version,"
                    + "auth_controller, image_server, image_resource, display_digits, official_user_id,"
                    + "market_server, market_resource, vod_server, vod_resource, web, payment_gateway, constants_version)"
                    + " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
            ps = db.connection.prepareStatement(query);
            int index = 1;
            ps.setInt(index++, dto.getPlatform());
            ps.setInt(index++, dto.getAppType());
            ps.setInt(index++, dto.getVersion());
            ps.setString(index++, dto.getAuthController());
            ps.setString(index++, dto.getImageServer());
            ps.setString(index++, dto.getImageResource());
            ps.setInt(index++, dto.getDisplayDigits());
            ps.setLong(index++, dto.getOfficialUserId());
            ps.setString(index++, dto.getMarketServer());
            ps.setString(index++, dto.getMarketResource());
            ps.setString(index++, dto.getVodServer());
            ps.setString(index++, dto.getVodResource());
            ps.setString(index++, dto.getWeb());
            ps.setString(index++, dto.getPaymentGateway());
            ps.setInt(index++, dto.getConstantsVersion());
            ps.execute();
        } catch (SQLException e) {
            error.setERROR_TYPE(MyAppError.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            RingLogger.getConfigPortalLogger().debug("add Single AppConstantsInfo SQL Exception --> " + e);
        } catch (Exception e) {
            error.setERROR_TYPE(MyAppError.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            RingLogger.getConfigPortalLogger().debug("add Single AppConstantsInfo Exception --> " + e);
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (db != null) {
                    authdbconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError addAppConstantsInfo(ArrayList<AppConstantsDTO> maps) {
        MyAppError error = new MyAppError();
        DBConnection db = null;
        PreparedStatement ps = null;
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            String query = "INSERT INTO appconstants (platform, app_type, version,"
                    + "     auth_controller, image_server, image_resource, display_digits, official_user_id,"
                    + "     market_server, market_resource, vod_server, vod_resource, web, payment_gateway, constants_version)                                         "
                    + "         SELECT * FROM (SELECT ? AS c1, ? AS c2, ? AS c3, ? AS c4, ? AS c5, ? AS c6, ? AS c7, ? AS c8,"
                    + "          ? AS c9, ? AS c10, ? AS c11, ? AS c12, ? AS c13, ? AS c14, ? AS c15) AS tmp                                                                       "
                    + "         WHERE 1";
            ps = db.connection.prepareStatement(query);
            int i = 1, index;
            for (AppConstantsDTO dto : maps) {
                index = 1;
                ps.setInt(index++, dto.getPlatform());
                ps.setInt(index++, dto.getAppType());
                ps.setInt(index++, dto.getVersion());
                ps.setString(index++, dto.getAuthController());
                ps.setString(index++, dto.getImageServer());
                ps.setString(index++, dto.getImageResource());
                ps.setInt(index++, dto.getDisplayDigits());
                ps.setLong(index++, dto.getOfficialUserId());
                ps.setString(index++, dto.getMarketServer());
                ps.setString(index++, dto.getMarketResource());
                ps.setString(index++, dto.getVodServer());
                ps.setString(index++, dto.getVodResource());
                ps.setString(index++, dto.getWeb());
                ps.setString(index++, dto.getPaymentGateway());
                ps.setInt(index++, dto.getConstantsVersion());
                ps.addBatch();
                i++;
                if ((i + 1) % 1000 == 0) {
                    ps.executeBatch();
                }
            }
            ps.executeBatch();
        } catch (SQLException e) {
            error.setERROR_TYPE(MyAppError.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            RingLogger.getConfigPortalLogger().debug("addAppConstantsInfo Map SQL Exception --> " + e);
        } catch (Exception e) {
            error.setERROR_TYPE(MyAppError.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            RingLogger.getConfigPortalLogger().debug("addAppConstantsInfo Map Exception --> " + e);
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (db != null) {
                    authdbconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError updateAppConstants(AppConstantsDTO dto) {
        MyAppError error = new MyAppError();
        DBConnection db = null;
        PreparedStatement ps = null;
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            String query = "UPDATE appconstants SET platform = ?, app_type = ?, version = ?,"
                    + "     auth_controller = ?, image_server = ?, image_resource = ?, display_digits = ?, official_user_id = ?,"
                    + "     market_server = ?, market_resource = ?, vod_server = ?, vod_resource = ?, web = ?, payment_gateway = ?, constants_version = ? "
                    + "     WHERE id = ?;";
            ps = db.connection.prepareStatement(query);
            int index = 1;
            ps.setInt(index++, dto.getPlatform());
            ps.setInt(index++, dto.getAppType());
            ps.setInt(index++, dto.getVersion());
            ps.setString(index++, dto.getAuthController());
            ps.setString(index++, dto.getImageServer());
            ps.setString(index++, dto.getImageResource());
            ps.setInt(index++, dto.getDisplayDigits());
            ps.setLong(index++, dto.getOfficialUserId());
            ps.setString(index++, dto.getMarketServer());
            ps.setString(index++, dto.getMarketResource());
            ps.setString(index++, dto.getVodServer());
            ps.setString(index++, dto.getVodResource());
            ps.setString(index++, dto.getWeb());
            ps.setString(index++, dto.getPaymentGateway());
            ps.setInt(index++, dto.getConstantsVersion());
            ps.setInt(index++, dto.getId());
            ps.execute();
        } catch (SQLException e) {
            error.setERROR_TYPE(MyAppError.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            RingLogger.getConfigPortalLogger().debug("updateAppConstants SQL Exception --> " + e);
        } catch (Exception e) {
            error.setERROR_TYPE(MyAppError.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            RingLogger.getConfigPortalLogger().debug("updateAppConstants Exception --> " + e);
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (db != null) {
                    authdbconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError deleteAppConstants(int id) {
        MyAppError error = new MyAppError();
        DBConnection db = null;
        Statement stmt = null;
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            stmt = db.connection.createStatement();
            String sql = "DELETE FROM appconstants WHERE id = " + id + ";";
            int a = stmt.executeUpdate(sql);
            RingLogger.getConfigPortalLogger().debug("delete success");
        } catch (SQLException e) {
            error.setERROR_TYPE(MyAppError.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            RingLogger.getConfigPortalLogger().debug("deleteAppConstants SQL Exception --> " + e);
        } catch (Exception e) {
            error.setERROR_TYPE(MyAppError.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            RingLogger.getConfigPortalLogger().debug("deleteAppConstants Exception --> " + e);
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (db != null) {
                    authdbconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }
}
