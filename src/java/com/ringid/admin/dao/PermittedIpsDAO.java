/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.dao;

import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.dto.PermittedIpsDTO;
import com.ringid.admin.utils.MyAppError;
import authdbconnector.DBConnection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author Rabby
 */
public class PermittedIpsDAO {

    private static PermittedIpsDAO instance = null;

    private PermittedIpsDAO() {
    }

    public static PermittedIpsDAO getInstance() {
        if (instance == null) {
            createInstance();
        }
        return instance;
    }

    private synchronized static void createInstance() {
        if (instance == null) {
            instance = new PermittedIpsDAO();
        }
    }

    public MyAppError addPermittedIps(PermittedIpsDTO dto) {
        MyAppError error = new MyAppError();
        DBConnection db = null;
        PreparedStatement ps = null;
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            String query = "INSERT INTO permittedips(ip, status, update_time) VALUES (?, ?, ?);";
            ps = db.connection.prepareStatement(query);
            ps.setString(1, dto.getIp());
            ps.setInt(2, dto.getStatus());
            ps.setLong(3, dto.getUpdateTime());
            ps.execute();
            ps.close();
        } catch (SQLException e) {
            error.setERROR_TYPE(error.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            RingLogger.getConfigPortalLogger().debug("addPermittedIps SQL Exception --> " + e);
        } catch (Exception e) {
            error.setERROR_TYPE(error.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            RingLogger.getConfigPortalLogger().debug("addPermittedIps Exception --> " + e);
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (db != null) {
                    authdbconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError updatePermittedIps(PermittedIpsDTO dto) {
        MyAppError error = new MyAppError();
        DBConnection db = null;
        PreparedStatement ps = null;
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            String query = "UPDATE permittedips SET ip = ?, status = ?, update_time = ? WHERE id = ?;";
            ps = db.connection.prepareStatement(query);
            ps.setString(1, dto.getIp());
            ps.setInt(2, dto.getStatus());
            ps.setLong(3, dto.getUpdateTime());
            ps.setInt(4, dto.getId());
            ps.execute();
        } catch (SQLException e) {
            error.setERROR_TYPE(error.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            RingLogger.getConfigPortalLogger().debug("updatePermittedIps SQL Exception --> " + e);
        } catch (Exception e) {
            error.setERROR_TYPE(error.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            RingLogger.getConfigPortalLogger().debug("updatePermittedIps Exception --> " + e);
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException e) {
            }
            try {
                if (db != null) {
                    authdbconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError deletePermittedIps(int id) {
        MyAppError error = new MyAppError();
        DBConnection db = null;
        Statement stmt = null;
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            stmt = db.connection.createStatement();
            String sql = "DELETE FROM permittedips WHERE id = " + id + ";";
            int a = stmt.executeUpdate(sql);
        } catch (SQLException e) {
            error.setERROR_TYPE(error.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            RingLogger.getConfigPortalLogger().debug("deletePermittedIps SQL Exception --> " + e);
        } catch (Exception e) {
            error.setERROR_TYPE(error.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            RingLogger.getConfigPortalLogger().debug("deletePermittedIps Exception --> " + e);
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (db != null) {
                    authdbconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }
}
