/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.projectMenu;

import com.ringid.admin.BaseDTO;
import java.util.Comparator;

/**
 *
 * @author Rabby
 */
public class MenuDTO extends BaseDTO {

    private int id;
    private String featureName;
    private String url;
    private int menuType;
    private String menuClass;
    private int parentId;

    public MenuDTO(int id, String featureName, String url, int menuType, String menuClass, int parentId) {
        this.id = id;
        this.featureName = featureName;
        this.url = url;
        this.menuType = menuType;
        this.menuClass = menuClass;
        this.parentId = parentId;
    }

    public MenuDTO() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFeatureName() {
        return featureName;
    }

    public void setFeatureName(String featureName) {
        this.featureName = featureName;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getParentId() {
        return parentId;
    }

    public void setParentId(int parentId) {
        this.parentId = parentId;
    }

    public int getMenuType() {
        return menuType;
    }

    public void setMenuClass(int menuType) {
        this.menuType = menuType;
    }

    public String getMenuClass() {
        return menuClass;
    }

    public void setMenuClass(String menuClass) {
        this.menuClass = menuClass;
    }

    public static class CompASC implements Comparator<MenuDTO> {

        @Override
        public int compare(MenuDTO arg0, MenuDTO arg1) {
            int returnVal = arg0.getId() - arg1.getId();
            return returnVal;
        }
    }
}
