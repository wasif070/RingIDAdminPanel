/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.projectMenu;

import com.ringid.admin.dto.adminAuth.LoginDTO;
import com.ringid.admin.dto.adminAuth.UserFeatureMappingDTO;
import com.ringid.admin.utils.Constants;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Rabby
 */
public class MenuGenerator {

    // parentMenu
    public static int NO_SUB_MENU = 0;
    public static int CLASSIC_MENU_DROPDOWN = 1;
    public static int MEGA_MENU_DROPDOWN = 2;
    public static int DROPDOWN_MENU = 3;
    public static int DROPDOWN_SUB_MENU = 4;
    //SubMenuClass
    public static String NO_CLASS = " ";
    public static String CLASSIC_MENU_DROPDOWN_CLASS = "classic-menu-dropdown ";
    public static String MEGA_MENU_DROPDOWN_CLASS = "mega-menu-dropdown ";
    public static String DROPDOWN_MENU_CLASS = "dropdown-menu ";
    public static String DROPDOWN_SUB_MENU_CLASS = "dropdown-submenu ";

    //FeatureId
    int NO_PARENT = 0;
    int index = 1;

    //root
    int HOME = index++;
    int SERVERS = index++;
    int PAGE_O_MEDIA = index++;
    int LIVE_O_CHANNEL = index++;
    int COMMON = index++;
    int NOTIFY = index++;
    int UTILITY = index++;
    int SPECIAL_USERS = index++;

    //servers
    int AUTH_SERVER = index++;
    int MANAGE_AUTH_SERVER = index++;
    int ADD_AUTH_SERVER = index++;
    int CHAT_SERVER = index++;
    int MANAGE_CHAT_SERVER = index++;
    int ADD_CHAT_SERVER = index++;
    int IMAGE_SERVER = index++;
    int MANAGE_IMAGE_SERVER = index++;
    int ADD_IMAGE_SERVER = index++;
    int OFFLINE_SERVER = index++;
    int MANAGE_OFFLINE_SERVER = index++;
    int ADD_OFFLINE_SERVER = index++;
    int RELAY_SERVER = index++;
    int RELAY_SERVER_MANAGE = index++;
    int RELAY_SERVER_ADD = index++;
    int SMS_SERVER = index++;
    int SMS_SERVER_MANAGE = index++;
    int SMS_SERVER_ADD = index++;
    int VOICE_SERVER = index++;
    int VOICE_SERVER_MANAGE = index++;
    int VOICE_SERVER_ADD = index++;
    int COIN_SERVER = index++;
    int COIN_SERVER_MANAGE = index++;
    int COIN_SERVER_ADD = index++;
    int CHANNEL_SERVER = index++;
    int CHANNEL_SERVER_MANAGE = index++;
    int CHANNEL_SERVER_ADD = index++;
    int STREAMING_SERVER = index++;
    //page & media
    int PAGES = index++;
    int USER_PAGES = index++;
    int USER_PAGES_MANAGE = index++;
    int USER_PAGES_ADD = index++;
    int DONATION_PAGES = index++;
    int DONATION_PAGES_MANAGE = index++;
    int DONATION_PAGES_ADD = index++;
//    int CREAT_PAGE = index++;
    int CATEGORY = index++;
    int CATEGORY_MANAGE = index++;
    int CATEGORY_ADD = index++;
    int FEED_TYPE = index++;
    int FEED_TYPE_MANAGE = index++;
    int FEED_TYPE_ADD = index++;
    int BREAKING_FEED = index++;
    int MEDIA = index++;
    //live / channel
    int CHANNEL = index++;
    int CHANNEL_CATEGORY = index++;
    int CHANNEL_CATEGORY_MANAGE = index++;
    int CHANNEL_CATEGORY_ADD = index++;
    int LIVE_ROOM = index++;
    int LIVE_ROOM_MANAGE = index++;
    int LIVE_ROOM_ADD = index++;
    int SPECIAL_ROOM = index++;
    int SPECIAL_ROOM_MANAGE = index++;
    int SPECIAL_ROOM_ADD = index++;
    int LIVE_INFO = index++;
    int ACTIVE_USERS = index++;
    int LIVE_HISTORY = index++;
    int OLD_LIVE_HISTORY = index++;
    int LIVE_SUMMARY = index++;
    int STREAMING_CODEC_SETTINGS = index++;
    int STREAMING_CODEC_SETTINGS_MANAGE = index++;
    int STREAMING_CODEC_SETTINGS_ADD = index++;
    int CALL_O_LIVE_SUBSCRIPTION_FEES = index++;
    int CALL_O_LIVE_SUBSCRIPTION_FEES_MANAGE = index++;
    int CALL_O_LIVE_SUBSCRIPTION_FEES_ADD = index++;
    int CHANNEL_SUBSCRIPTION = index++;
    int CHANNEL_SUBSCRIPTION_MANAGE = index++;
    int CHANNEL_SUBSCRIPTION_ADD = index++;
    int POST_RECORDED_LIVE_FEED = index++;
    int PAST_LIVE_FEED = index++;
    int AMBASSADOR = index++;
    //common
    int APP_CONSTANTS = index++;
    int APP_CONSTANTS_MANAGE = index++;
    int APP_CONSTANTS_ADD = index++;
    int APP_STORES = index++;
    int APP_STORES_MANAGE = index++;
    int APP_STORES_ADD = index++;
    int LOG_SETTINGS = index++;
    int LOG_SETTINGS_MANAGE = index++;
    int LOG_SETTINGS_ADD = index++;
    int SPAM_REASONS = index++;
    int SPAM_REASONS_MANAGE = index++;
    int SPAM_REASONS_ADD = index++;
    int SETTINGS = index++;
    int SETTINGS_MANAGE = index++;
    int SETTINGS_ADD = index++;
    int HOME_PAGE_BANNER_TYPE_SEQ = index++;
    int LIVE_TO_CAHNNEL = index++;
    int RING_STUDIO = index++;
    int RING_STUDIO_MANAGE = index++;
    int RING_STUDIO_ADD = index++;
    int PERMITTED_IPS = index++;
    int PERMITTED_IPS_MANAGE = index++;
    int PERMITTED_IPS_ADD = index++;
    int DEACTIVATE_REASONS = index++;
    int DEACTIVATE_REASONS_MANAGE = index++;
    int DEACTIVATE_REASONS_ADD = index++;
    int FLOATING_MENUS = index++;
    int FLOATING_MENUS_MANAGE = index++;
    int FLOATING_MENUS_ADD = index++;
    int FLOATING_MENU_SLIDE_ITEM = index++;
    int CASH_WALLET_PERMITTED_COUNTRY = index++;
    int CASH_WALLET_PERMITTED_COUNTRY_MANAGE = index++;
    int CASH_WALLET_PERMITTED_COUNTRY_ADD = index++;
    int EVENT = index++;
    int EVENT_MANAGE = index++;
    int EVENT_ADD = index++;
    int SPORTS = index++;
    int SPORTS_MATCH_CHANNEL_MAPPED = index++;
    int SPORTS_HOME_MATCH_MANAGEMENT = index++;
    int RINGID_DISTRIBUTOR = index++;
    int RINGID_DISTRIBUTOR_MANAGE = index++;
    int RINGID_DISTRIBUTOR_ADD = index++;
    int RING_FEATURE = index++;
    int RING_FEATURE_MANAGE = index++;
    int RING_FEATURE_MAPPING = index++;
    int COIN_EXCHANGE = index++;
    int PAYMENT_METHOD = index++;
    int PAYMENT_METHOD_ADD = index++;
    int RINGBIT = index++;
    int RINGBIT_IMAGE_O_TEXT = index++;
    int PROFESSION = index++;
    int STATISTIC = index++;
    int APP_UPDATE_URL = index++;
    //notify
    int NOTIFY_AUTH_SERVER = index++;
    int NOTIFY_AUTH_CONTROLLER = index++;
    int NOTIFY_BOTH = index++;
    int RELOAD = index++;
    int CHECK_LOG = index++;
    //manage operations
    int RESET_RETRY_LIMIT = index++;
    int RESET_PASSWORD = index++;
    int UNVERIFY_NUMBER = index++;
    int VERIFY_NUMBER = index++;
    int LIVE_VERIFIED_SETTINGS = index++;
    int CHANGE_RINGID = index++;
    int LIMIT_O_TIME_UPDATE = index++;
    int LATEST_OTP = index++;
    int USERID_FROM_RINGID = index++;
    int SEARCH_USER = index++;
    int SEARCH_USER_CONTACTS = index++;
    int AUTO_FOLLOW_SETTING = index++;
    int NEWS_FEED = index++;
    int FEED_HIGHLIGHTS = index++;
    int DELETE_VIRTUAL_RINGIDS = index++;
    int RINGBIT_LOG = index++;
    int ADMIN_ACTION = index++;
    int AUTO_FOLLOW_PAGES = index++;
    int ASSIGN_PAGE_RINGID = index++;
    //special users
    int SPECIAL_USER_ACCOUNTS = index++;
    int SPECIAL_USER_ACCOUNTS_MANAGE = index++;
    int SPECIAL_USER_ACCOUNTS_ADD = index++;
    int CELEBRITY_SCHEDULE = index++;
    int FEED_POST = index++;
//    int FEATURED_PAST_LIVE = index++;

    public List<MenuDTO> generateMenu(LoginDTO loginDTO) {
        List<MenuDTO> menuList = getMenuList();
        HashMap<String, MenuDTO> menuMap = new HashMap<>();
        for (MenuDTO dto : menuList) {
            menuMap.put(dto.getFeatureName(), dto);
        }
        List<MenuDTO> finalMenuList = new ArrayList<>();
        HashMap<String, UserFeatureMappingDTO> feauterMapList = loginDTO.getFeauterMapList();
        for (Map.Entry<String, MenuDTO> entry : menuMap.entrySet()) {
            if (loginDTO.getPermissionLevel() == Constants.SUPER_ADMIN || entry.getKey().equals("Home")) {
                finalMenuList.add(entry.getValue());
            } else if (feauterMapList.containsKey(entry.getKey()) && (feauterMapList.get(entry.getKey()).getPermissionLevel() > 0 || feauterMapList.get(entry.getKey()).getHasChildPermission())) {
                finalMenuList.add(entry.getValue());
                if (menuMap.containsKey("Manage " + entry.getKey())) {
                    finalMenuList.add(menuMap.get("Manage " + entry.getKey()));
                }
                if (feauterMapList.get(entry.getKey()).getHasAddPermission()) {
                    if (menuMap.containsKey("Add " + entry.getKey())) {
                        finalMenuList.add(menuMap.get("Add " + entry.getKey()));
                    }
                }
            }
        }
        Collections.sort(finalMenuList, new MenuDTO.CompASC());
        return finalMenuList;
    }

    public List<MenuDTO> getMenuList() {
        List<MenuDTO> menuList = new ArrayList<>();

        //root
        menuList.add(new MenuDTO(HOME, MenuNames.HOME, "welcome.do", CLASSIC_MENU_DROPDOWN, CLASSIC_MENU_DROPDOWN_CLASS + "featureHome", NO_PARENT));
        menuList.add(new MenuDTO(SERVERS, MenuNames.SERVERS, "javascript:;", CLASSIC_MENU_DROPDOWN, CLASSIC_MENU_DROPDOWN_CLASS + "servers", NO_PARENT));
        menuList.add(new MenuDTO(PAGE_O_MEDIA, MenuNames.PAGE_O_MEDIA, "javascript:;", CLASSIC_MENU_DROPDOWN, CLASSIC_MENU_DROPDOWN_CLASS + "page-o-media", NO_PARENT));
        menuList.add(new MenuDTO(LIVE_O_CHANNEL, MenuNames.LIVE_O_CHANNEL, "javascript:;", CLASSIC_MENU_DROPDOWN, CLASSIC_MENU_DROPDOWN_CLASS + "live-o-channel", NO_PARENT));
//        menuList.add(new MenuDTO(COMMON, MenuNames.COMMON, "javascript:;", CLASSIC_MENU_DROPDOWN, CLASSIC_MENU_DROPDOWN_CLASS + "common", NO_PARENT));
        menuList.add(new MenuDTO(COMMON, MenuNames.COMMON, "javascript:;", MEGA_MENU_DROPDOWN, MEGA_MENU_DROPDOWN_CLASS + "common", NO_PARENT));
        menuList.add(new MenuDTO(NOTIFY, MenuNames.NOTIFY, "javascript:;", CLASSIC_MENU_DROPDOWN, CLASSIC_MENU_DROPDOWN_CLASS + "notify", NO_PARENT));
        menuList.add(new MenuDTO(UTILITY, MenuNames.UTILITY, "javascript:;", MEGA_MENU_DROPDOWN, MEGA_MENU_DROPDOWN_CLASS + "manageOperations", NO_PARENT));
        menuList.add(new MenuDTO(SPECIAL_USERS, MenuNames.SPECIAL_USER, "javascript:;", CLASSIC_MENU_DROPDOWN, CLASSIC_MENU_DROPDOWN_CLASS + "specialUsers", NO_PARENT));

        //server
        menuList.add(new MenuDTO(AUTH_SERVER, MenuNames.AUTH_SERVER, "javascript:;", DROPDOWN_SUB_MENU, DROPDOWN_SUB_MENU_CLASS + "authServer", SERVERS));
        menuList.add(new MenuDTO(MANAGE_AUTH_SERVER, "Manage " + MenuNames.AUTH_SERVER, "authServerListInfo.do", NO_SUB_MENU, NO_CLASS + "manageAuthServer", AUTH_SERVER));
        menuList.add(new MenuDTO(ADD_AUTH_SERVER, "Add " + MenuNames.AUTH_SERVER, "admin/servers/authServers/authServerUI.jsp", NO_SUB_MENU, NO_CLASS + "addAuthServer", AUTH_SERVER));
        menuList.add(new MenuDTO(CHAT_SERVER, MenuNames.CHAT_SERVER, "javascript:;", DROPDOWN_SUB_MENU, DROPDOWN_SUB_MENU_CLASS + "chatServer", SERVERS));
        menuList.add(new MenuDTO(MANAGE_CHAT_SERVER, "Manage " + MenuNames.CHAT_SERVER, "chatServerListInfo.do", NO_SUB_MENU, NO_CLASS + "manageChatServer", CHAT_SERVER));
        menuList.add(new MenuDTO(ADD_CHAT_SERVER, "Add " + MenuNames.CHAT_SERVER, "admin/servers/chatServers/chatServerUI.jsp", NO_SUB_MENU, NO_CLASS + "addChatServer", CHAT_SERVER));
        menuList.add(new MenuDTO(IMAGE_SERVER, MenuNames.IMAGE_SERVER, "javascript:;", DROPDOWN_SUB_MENU, DROPDOWN_SUB_MENU_CLASS + "imageServer", SERVERS));
        menuList.add(new MenuDTO(MANAGE_IMAGE_SERVER, "Manage " + MenuNames.IMAGE_SERVER, "imageServerListInfo.do", NO_SUB_MENU, NO_CLASS + "manageImageServer", IMAGE_SERVER));
        menuList.add(new MenuDTO(ADD_IMAGE_SERVER, "Add " + MenuNames.IMAGE_SERVER, "admin/servers/imageServers/imageServerUI.jsp", NO_SUB_MENU, NO_CLASS + "addImageServer", IMAGE_SERVER));
//        menuList.add(new MenuDTO(OFFLINE_SERVER, MenuNames.OFFLINE_SERVER, "javascript:;", DROPDOWN_SUB_MENU, DROPDOWN_SUB_MENU_CLASS + "offlineServer", SERVERS));
//        menuList.add(new MenuDTO(MANAGE_OFFLINE_SERVER, "Manage " + MenuNames.OFFLINE_SERVER, "offlineServerListInfo.do", NO_SUB_MENU, NO_CLASS + "manageOfflineServer", OFFLINE_SERVER));
//        menuList.add(new MenuDTO(ADD_OFFLINE_SERVER, "Add " + MenuNames.OFFLINE_SERVER, "admin/servers/offlineServers/offlineServerUI.jsp", NO_SUB_MENU, NO_CLASS + "addOfflineServer", OFFLINE_SERVER));
//        menuList.add(new MenuDTO(RELAY_SERVER, MenuNames.RELAY_SERVER, "javascript:;", DROPDOWN_SUB_MENU, DROPDOWN_SUB_MENU_CLASS + "relayServer", SERVERS));
//        menuList.add(new MenuDTO(RELAY_SERVER_MANAGE, "Manage " + MenuNames.RELAY_SERVER, "relayServerListInfo.do", NO_SUB_MENU, NO_CLASS + "manageRelayServer", RELAY_SERVER));
//        menuList.add(new MenuDTO(RELAY_SERVER_ADD, "Add " + MenuNames.RELAY_SERVER, "admin/servers/relayServers/relayServerUI.jsp", NO_SUB_MENU, NO_CLASS + "addRelayServer", RELAY_SERVER));
        menuList.add(new MenuDTO(SMS_SERVER, MenuNames.SMS_SERVER, "javascript:;", DROPDOWN_SUB_MENU, DROPDOWN_SUB_MENU_CLASS + "smsServer", SERVERS));
        menuList.add(new MenuDTO(SMS_SERVER_MANAGE, "Manage " + MenuNames.SMS_SERVER, "smsServerListInfo.do", NO_SUB_MENU, NO_CLASS + "manageSmsServer", SMS_SERVER));
        menuList.add(new MenuDTO(SMS_SERVER_ADD, "Add " + MenuNames.SMS_SERVER, "admin/servers/smsServers/smsServerUI.jsp", NO_SUB_MENU, NO_CLASS + "addSmsServer", SMS_SERVER));
        menuList.add(new MenuDTO(VOICE_SERVER, MenuNames.VOICE_SERVER, "javascript:;", DROPDOWN_SUB_MENU, DROPDOWN_SUB_MENU_CLASS + "voiceServer", SERVERS));
        menuList.add(new MenuDTO(VOICE_SERVER_MANAGE, "Manage " + MenuNames.VOICE_SERVER, "voiceServerListInfo.do", NO_SUB_MENU, NO_CLASS + "manageVoiceServer", VOICE_SERVER));
        menuList.add(new MenuDTO(VOICE_SERVER_ADD, "Add " + MenuNames.VOICE_SERVER, "admin/servers/voiceServers/voiceServerUI.jsp", NO_SUB_MENU, NO_CLASS + "addVoiceServer", VOICE_SERVER));
        menuList.add(new MenuDTO(COIN_SERVER, MenuNames.COIN_SERVER, "javascript:;", DROPDOWN_SUB_MENU, DROPDOWN_SUB_MENU_CLASS + "coinServer", SERVERS));
        menuList.add(new MenuDTO(COIN_SERVER_MANAGE, "Manage " + MenuNames.COIN_SERVER, "coinServerListInfo.do", NO_SUB_MENU, NO_CLASS + "manageCoinServer", COIN_SERVER));
        menuList.add(new MenuDTO(COIN_SERVER_ADD, "Add " + MenuNames.COIN_SERVER, "admin/servers/coinServers/coinServerUI.jsp", NO_SUB_MENU, NO_CLASS + "addCoinServer", COIN_SERVER));
        menuList.add(new MenuDTO(CHANNEL_SERVER, MenuNames.CHANNEL_SERVER, "javascript:;", DROPDOWN_SUB_MENU, DROPDOWN_SUB_MENU_CLASS + "channelServer", SERVERS));
        menuList.add(new MenuDTO(CHANNEL_SERVER_MANAGE, "Manage " + MenuNames.CHANNEL_SERVER, "channelServerListInfo.do", NO_SUB_MENU, NO_CLASS + "manageChannelServer", CHANNEL_SERVER));
        menuList.add(new MenuDTO(CHANNEL_SERVER_ADD, "Add " + MenuNames.CHANNEL_SERVER, "admin/servers/channelservers/channelServerUI.jsp", NO_SUB_MENU, NO_CLASS + "addChannelServer", CHANNEL_SERVER));
        menuList.add(new MenuDTO(STREAMING_SERVER, MenuNames.STREAMING_SERVER, "streamingServerListInfo.do", NO_SUB_MENU, NO_CLASS + "streamingServer", SERVERS));

        //page & media
        menuList.add(new MenuDTO(PAGES, MenuNames.PAGES, "newsPortalListInfo.do", NO_SUB_MENU, NO_CLASS + "pages", PAGE_O_MEDIA));
        menuList.add(new MenuDTO(USER_PAGES, MenuNames.USER_PAGE, "javascript:;", DROPDOWN_SUB_MENU, DROPDOWN_SUB_MENU_CLASS + "userPages", PAGE_O_MEDIA));
        menuList.add(new MenuDTO(USER_PAGES_MANAGE, "Manage " + MenuNames.USER_PAGE, "admin/userPages/userPages.jsp", NO_SUB_MENU, NO_CLASS + "manageUserPages", USER_PAGES));
        menuList.add(new MenuDTO(USER_PAGES_ADD, "Add " + MenuNames.USER_PAGE, "admin/userPages/userPageUI.jsp", NO_SUB_MENU, NO_CLASS + "addPage", USER_PAGES));
        menuList.add(new MenuDTO(DONATION_PAGES, MenuNames.DONATION_PAGE, "javascript:;", DROPDOWN_SUB_MENU, DROPDOWN_SUB_MENU_CLASS + "donationPages", PAGE_O_MEDIA));
        menuList.add(new MenuDTO(DONATION_PAGES_MANAGE, "Manage " + MenuNames.DONATION_PAGE, "showDonationPageList.do", NO_SUB_MENU, NO_CLASS + "manageDonationPages", DONATION_PAGES));
        menuList.add(new MenuDTO(DONATION_PAGES_ADD, "Add " + MenuNames.DONATION_PAGE, "admin/donationPage/donationPageUI.jsp", NO_SUB_MENU, NO_CLASS + "addDonationPage", DONATION_PAGES));
        menuList.add(new MenuDTO(CATEGORY, MenuNames.CATEGORY, "javascript:;", DROPDOWN_SUB_MENU, DROPDOWN_SUB_MENU_CLASS + "category", PAGE_O_MEDIA));
        menuList.add(new MenuDTO(CATEGORY_MANAGE, "Manage " + MenuNames.CATEGORY, "newsPortalCategoryListInfo.do", NO_SUB_MENU, NO_CLASS + "manageCategory", CATEGORY));
        menuList.add(new MenuDTO(CATEGORY_ADD, "Add " + MenuNames.CATEGORY, "admin/newsPortal/category/newsPortalCategoryUI.jsp", NO_SUB_MENU, NO_CLASS + "addCategory", CATEGORY));
        menuList.add(new MenuDTO(FEED_TYPE, MenuNames.FEED_TYPE, "javascript:;", DROPDOWN_SUB_MENU, DROPDOWN_SUB_MENU_CLASS + "feedType", PAGE_O_MEDIA));
        menuList.add(new MenuDTO(FEED_TYPE_MANAGE, "Manage " + MenuNames.FEED_TYPE, "newsPortalTypeListInfo.do", NO_SUB_MENU, NO_CLASS + "manageFeedType", FEED_TYPE));
        menuList.add(new MenuDTO(FEED_TYPE_ADD, "Add " + MenuNames.FEED_TYPE, "admin/newsPortal/type/newsPortalTypeUI.jsp", NO_SUB_MENU, NO_CLASS + "addFeedType", FEED_TYPE));
        menuList.add(new MenuDTO(BREAKING_FEED, MenuNames.BREAKING_FEED, "breakingNewsListInfo.do", NO_SUB_MENU, NO_CLASS + "breakingFeed", PAGE_O_MEDIA));
        menuList.add(new MenuDTO(MEDIA, MenuNames.MEDIA, "mediaListInfo.do", NO_SUB_MENU, NO_CLASS + "Media", PAGE_O_MEDIA));

        //live / channel
        menuList.add(new MenuDTO(CHANNEL, MenuNames.CHANNEL, "channelList.do", NO_SUB_MENU, NO_CLASS + "channel", LIVE_O_CHANNEL));
        menuList.add(new MenuDTO(CHANNEL_CATEGORY, MenuNames.CHANNEL_CATEGORY, "javascript:;", DROPDOWN_SUB_MENU, DROPDOWN_SUB_MENU_CLASS + "channelCategory", LIVE_O_CHANNEL));
        menuList.add(new MenuDTO(CHANNEL_CATEGORY_MANAGE, "Manage " + MenuNames.CHANNEL_CATEGORY, "showChannelCategory.do", NO_SUB_MENU, NO_CLASS + "manageChannelCategory", CHANNEL_CATEGORY));
        menuList.add(new MenuDTO(CHANNEL_CATEGORY_ADD, "Add " + MenuNames.CHANNEL_CATEGORY, "admin/channel/channelCategory/channelCategoryUI.jsp", NO_SUB_MENU, NO_CLASS + "addChannelCategory", CHANNEL_CATEGORY));
        menuList.add(new MenuDTO(LIVE_ROOM, MenuNames.LIVE_ROOM, "javascript:;", DROPDOWN_SUB_MENU, DROPDOWN_SUB_MENU_CLASS + "liveRoom", LIVE_O_CHANNEL));
        menuList.add(new MenuDTO(LIVE_ROOM_MANAGE, "Manage " + MenuNames.LIVE_ROOM, "showLiveRoomList.do", NO_SUB_MENU, NO_CLASS + "manageLiveRoom", LIVE_ROOM));
        menuList.add(new MenuDTO(LIVE_ROOM_ADD, "Add " + MenuNames.LIVE_ROOM, "admin/liveRoom/liveRoomUI.jsp", NO_SUB_MENU, NO_CLASS + "addLiveRoom", LIVE_ROOM));
        menuList.add(new MenuDTO(SPECIAL_ROOM, MenuNames.SPECIAL_ROOM, "javascript:;", DROPDOWN_SUB_MENU, DROPDOWN_SUB_MENU_CLASS + "specialRoom", LIVE_O_CHANNEL));
        menuList.add(new MenuDTO(SPECIAL_ROOM_MANAGE, "Manage " + MenuNames.SPECIAL_ROOM, "showSpecialRoomList.do", NO_SUB_MENU, NO_CLASS + "manageSpecialRoom", SPECIAL_ROOM));
        menuList.add(new MenuDTO(SPECIAL_ROOM_ADD, "Add " + MenuNames.SPECIAL_ROOM, "admin/specialRoom/specialRoomUI.jsp", NO_SUB_MENU, NO_CLASS + "addSpecialRoom", SPECIAL_ROOM));
        menuList.add(new MenuDTO(LIVE_INFO, MenuNames.LIVE_INFO, "javascript:;", DROPDOWN_SUB_MENU, DROPDOWN_SUB_MENU_CLASS + "liveInfo", LIVE_O_CHANNEL));
        menuList.add(new MenuDTO(ACTIVE_USERS, MenuNames.ACTIVE_USERS, "showLivestreamUser.do", NO_SUB_MENU, NO_CLASS + "activeUsers", LIVE_INFO));
        menuList.add(new MenuDTO(LIVE_HISTORY, MenuNames.LIVE_HISTORY, "showLiveInfoListByRingId.do", NO_SUB_MENU, NO_CLASS + "liveHistory", LIVE_INFO));
        menuList.add(new MenuDTO(OLD_LIVE_HISTORY, MenuNames.OLD_LIVE_HISTORY, "admin/liveInfo/oldLiveInfoList.jsp", NO_SUB_MENU, NO_CLASS + "oldLiveHistory", LIVE_INFO));
        menuList.add(new MenuDTO(LIVE_SUMMARY, MenuNames.LIVE_SUMMARY, "admin/liveInfo/liveInfoSummary.jsp", NO_SUB_MENU, NO_CLASS + "liveSummary", LIVE_INFO));
        menuList.add(new MenuDTO(STREAMING_CODEC_SETTINGS, MenuNames.STREAMING_CODEC_SETTING, "javascript:;", DROPDOWN_SUB_MENU, DROPDOWN_SUB_MENU_CLASS + "streamingCodecSettings", LIVE_O_CHANNEL));
        menuList.add(new MenuDTO(STREAMING_CODEC_SETTINGS_MANAGE, "Manage " + MenuNames.STREAMING_CODEC_SETTING, "streamingCodecSettingsList.do", NO_SUB_MENU, NO_CLASS + "manageStreamingCodecSettings", STREAMING_CODEC_SETTINGS));
        menuList.add(new MenuDTO(STREAMING_CODEC_SETTINGS_ADD, "Add " + MenuNames.STREAMING_CODEC_SETTING, "admin/streamingCodecSettings/streamingCodecSettingsUI.jsp", NO_SUB_MENU, NO_CLASS + "addStreamingCodecSettings", STREAMING_CODEC_SETTINGS));
        menuList.add(new MenuDTO(CALL_O_LIVE_SUBSCRIPTION_FEES, MenuNames.CALL_O_LIVE_SUBSCRIPTION_FEE, "javascript:;", DROPDOWN_SUB_MENU, DROPDOWN_SUB_MENU_CLASS + "clsf", LIVE_O_CHANNEL));
        menuList.add(new MenuDTO(CALL_O_LIVE_SUBSCRIPTION_FEES_MANAGE, "Manage " + MenuNames.CALL_O_LIVE_SUBSCRIPTION_FEE, "showTariffList.do?type=1", NO_SUB_MENU, NO_CLASS + "manageclsf", CALL_O_LIVE_SUBSCRIPTION_FEES));
        menuList.add(new MenuDTO(CALL_O_LIVE_SUBSCRIPTION_FEES_ADD, "Add " + MenuNames.CALL_O_LIVE_SUBSCRIPTION_FEE, "admin/ringId/tariff/tariffAdd.jsp", NO_SUB_MENU, NO_CLASS + "addclsf", CALL_O_LIVE_SUBSCRIPTION_FEES));
        menuList.add(new MenuDTO(CHANNEL_SUBSCRIPTION, MenuNames.CHANNEL_SUBSCRIPTION, "javascript:;", DROPDOWN_SUB_MENU, DROPDOWN_SUB_MENU_CLASS + "channelSubscription", LIVE_O_CHANNEL));
        menuList.add(new MenuDTO(CHANNEL_SUBSCRIPTION_MANAGE, "Manage " + MenuNames.CHANNEL_SUBSCRIPTION, "showChannelSubscription.do", NO_SUB_MENU, NO_CLASS + "manageChannelSubscription", CHANNEL_SUBSCRIPTION));
        menuList.add(new MenuDTO(CHANNEL_SUBSCRIPTION_ADD, "Add " + MenuNames.CHANNEL_SUBSCRIPTION, "admin/channel/channelSubscription/ChannelSubscriptionUI.jsp", NO_SUB_MENU, NO_CLASS + "addChannelSubscription", CHANNEL_SUBSCRIPTION));
        menuList.add(new MenuDTO(POST_RECORDED_LIVE_FEED, MenuNames.POST_RECORDED_LIVE_FEED, "admin/ringId/postCelebrityStatus/postRecordedLiveFeed.jsp", NO_SUB_MENU, NO_CLASS + " postRecordedLiveFeed", LIVE_O_CHANNEL));
        menuList.add(new MenuDTO(PAST_LIVE_FEED, MenuNames.PAST_LIVE_FEED, "pastLiveFeedList.do", NO_SUB_MENU, NO_CLASS + "pastLiveFeed", LIVE_O_CHANNEL));
        menuList.add(new MenuDTO(AMBASSADOR, MenuNames.AMBASSADOR, "showAmbassadorList.do", NO_SUB_MENU, NO_CLASS + "ambassador", LIVE_O_CHANNEL));
//        //common classic-menu
//        menuList.add(new MenuDTO(APP_CONSTANTS, MenuNames.APP_CONSTANT, "javascript:;", DROPDOWN_SUB_MENU, DROPDOWN_SUB_MENU_CLASS + "appConstants", COMMON));
//        menuList.add(new MenuDTO(APP_CONSTANTS_MANAGE, "Manage " + MenuNames.APP_CONSTANT, "appConstantsListInfo.do", NO_SUB_MENU, NO_CLASS + "manageAppConstants", APP_CONSTANTS));
//        menuList.add(new MenuDTO(APP_CONSTANTS_ADD, "Add " + MenuNames.APP_CONSTANT, "appConstantDefaultValue.do", NO_SUB_MENU, NO_CLASS + "addAppConstants", APP_CONSTANTS));
//        menuList.add(new MenuDTO(APP_STORES, MenuNames.APP_STORE, "javascript:;", DROPDOWN_SUB_MENU, DROPDOWN_SUB_MENU_CLASS + "appStores", COMMON));
//        menuList.add(new MenuDTO(APP_STORES_MANAGE, "Manage " + MenuNames.APP_STORE, "appStoresListInfo.do", NO_SUB_MENU, NO_CLASS + "manageAppStores", APP_STORES));
//        menuList.add(new MenuDTO(APP_STORES_ADD, "Add " + MenuNames.APP_STORE, "addAppStoresInfo.do", NO_SUB_MENU, NO_CLASS + "addAppStore", APP_STORES));
//        menuList.add(new MenuDTO(LOG_SETTINGS, MenuNames.LOG_SETTING, "javascript:;", DROPDOWN_SUB_MENU, DROPDOWN_SUB_MENU_CLASS + "logSettings", COMMON));
//        menuList.add(new MenuDTO(LOG_SETTINGS_MANAGE, "Manage " + MenuNames.LOG_SETTING, "logSettingListInfo.do", NO_SUB_MENU, NO_CLASS + "manageLogSettings", LOG_SETTINGS));
//        menuList.add(new MenuDTO(LOG_SETTINGS_ADD, "Add " + MenuNames.LOG_SETTING, "admin/logSettings/logSettingUI.jsp", NO_SUB_MENU, NO_CLASS + "addLogSetting", LOG_SETTINGS));
//        menuList.add(new MenuDTO(SPAM_REASONS, MenuNames.SPAM_REASON, "javascript:;", DROPDOWN_SUB_MENU, DROPDOWN_SUB_MENU_CLASS + "spamReasons", COMMON));
//        menuList.add(new MenuDTO(SPAM_REASONS_MANAGE, "Manage " + MenuNames.SPAM_REASON, "spamReasonsListInfo.do", NO_SUB_MENU, NO_CLASS + "manageSpamReasons", SPAM_REASONS));
//        menuList.add(new MenuDTO(SPAM_REASONS_ADD, "Add " + MenuNames.SPAM_REASON, "spamReasonDefaultValue.do", NO_SUB_MENU, NO_CLASS + "addSpamReason", SPAM_REASONS));
//        menuList.add(new MenuDTO(SETTINGS, MenuNames.SETTING, "javascript:;", DROPDOWN_SUB_MENU, DROPDOWN_SUB_MENU_CLASS + "settings", COMMON));
//        menuList.add(new MenuDTO(SETTINGS_MANAGE, "Manage " + MenuNames.SETTING, "settingListInfo.do", NO_SUB_MENU, NO_CLASS + "manageSettings", SETTINGS));
//        menuList.add(new MenuDTO(SETTINGS_ADD, "Add " + MenuNames.SETTING, "admin/settings/settingUI.jsp", NO_SUB_MENU, NO_CLASS + "addSetting", SETTINGS));
//        menuList.add(new MenuDTO(RING_STUDIO, MenuNames.RING_STUDIO, "javascript:;", DROPDOWN_SUB_MENU, DROPDOWN_SUB_MENU_CLASS + "ringStudio", COMMON));
//        menuList.add(new MenuDTO(RING_STUDIO_MANAGE, "Manage " + MenuNames.RING_STUDIO, "ringStudioListInfo.do", NO_SUB_MENU, NO_CLASS + "manageRingStudio", RING_STUDIO));
//        menuList.add(new MenuDTO(RING_STUDIO_ADD, "Add " + MenuNames.RING_STUDIO, "admin/ringstudio/ringStudioUI.jsp", NO_SUB_MENU, NO_CLASS + "addRingStudio", RING_STUDIO));
//        menuList.add(new MenuDTO(PERMITTED_IPS, MenuNames.PERMITTED_IP, "javascript:;", DROPDOWN_SUB_MENU, DROPDOWN_SUB_MENU_CLASS + "permittedIPs", COMMON));
//        menuList.add(new MenuDTO(PERMITTED_IPS_MANAGE, "Manage " + MenuNames.PERMITTED_IP, "permittedIpsListInfo.do", NO_SUB_MENU, NO_CLASS + "managePermittedIPs", PERMITTED_IPS));
//        menuList.add(new MenuDTO(PERMITTED_IPS_ADD, "Add " + MenuNames.PERMITTED_IP, "admin/permittedIps/permittedIpsUI.jsp", NO_SUB_MENU, NO_CLASS + "addPermittedIP", PERMITTED_IPS));
//        menuList.add(new MenuDTO(DEACTIVATE_REASONS, MenuNames.DEACTIVATE_REASON, "javascript:;", DROPDOWN_SUB_MENU, DROPDOWN_SUB_MENU_CLASS + "deactivateReasons", COMMON));
//        menuList.add(new MenuDTO(DEACTIVATE_REASONS_MANAGE, "Manage " + MenuNames.DEACTIVATE_REASON, "deactivateReasonsListInfo.do", NO_SUB_MENU, NO_CLASS + "manageDeactivateReasons", DEACTIVATE_REASONS));
//        menuList.add(new MenuDTO(DEACTIVATE_REASONS_ADD, "Add " + MenuNames.DEACTIVATE_REASON, "admin/deactivateReasons/deactivateReasonsUI.jsp", NO_SUB_MENU, NO_CLASS + "addDeactivateReason", DEACTIVATE_REASONS));
//        menuList.add(new MenuDTO(FLOATING_MENUS, MenuNames.FLOATING_MENU, "javascript:;", DROPDOWN_SUB_MENU, DROPDOWN_SUB_MENU_CLASS + "floatingMenus", COMMON));
//        menuList.add(new MenuDTO(FLOATING_MENUS_MANAGE, "Manage " + MenuNames.FLOATING_MENU, "showFloatingMenuList.do", NO_SUB_MENU, NO_CLASS + "manageFloatingMenus", FLOATING_MENUS));
//        menuList.add(new MenuDTO(FLOATING_MENU_SLIDE_ITEM, MenuNames.FLOATING_MENU_SLIDE_ITEM, "floatingMenuSlideItemList.do", NO_SUB_MENU, NO_CLASS + "floatingMenuSlide", FLOATING_MENUS));
//        menuList.add(new MenuDTO(FLOATING_MENUS_ADD, "Add " + MenuNames.FLOATING_MENU, "admin/floatingMenu/floatingMenuUI.jsp", NO_SUB_MENU, NO_CLASS + "addFloatingMenu", FLOATING_MENUS));
//        menuList.add(new MenuDTO(CASH_WALLET_PERMITTED_COUNTRY, MenuNames.CASH_WALLET_PERMITTED_COUNTRY, "javascript:;", DROPDOWN_SUB_MENU, DROPDOWN_SUB_MENU_CLASS + "cashWalletPermittedCountry", COMMON));
//        menuList.add(new MenuDTO(CASH_WALLET_PERMITTED_COUNTRY_MANAGE, "Manage " + MenuNames.CASH_WALLET_PERMITTED_COUNTRY, "showCashWalletPermittedCountryList.do", NO_SUB_MENU, NO_CLASS + "manageCashWalletPermittedCountry", CASH_WALLET_PERMITTED_COUNTRY));
//        menuList.add(new MenuDTO(CASH_WALLET_PERMITTED_COUNTRY_ADD, "Add " + MenuNames.CASH_WALLET_PERMITTED_COUNTRY, "admin/cashWallet/permittedCountryUI.jsp", NO_SUB_MENU, NO_CLASS + "addCashWalletPermittedCountry", CASH_WALLET_PERMITTED_COUNTRY));
//        menuList.add(new MenuDTO(EVENT, MenuNames.EVENT, "javascript:;", DROPDOWN_SUB_MENU, DROPDOWN_SUB_MENU_CLASS + "event", COMMON));
//        menuList.add(new MenuDTO(EVENT_MANAGE, "Manage " + MenuNames.EVENT, "showEventListInfo.do", NO_SUB_MENU, NO_CLASS + "manageEvent", EVENT));
//        menuList.add(new MenuDTO(EVENT_ADD, "Add " + MenuNames.EVENT, "admin/event/eventUI.jsp", NO_SUB_MENU, NO_CLASS + "addEvent", EVENT));
//        menuList.add(new MenuDTO(SPORTS, MenuNames.SPORTS, "javascript:;", DROPDOWN_SUB_MENU, DROPDOWN_SUB_MENU_CLASS + "sports", COMMON));
//        menuList.add(new MenuDTO(SPORTS_MATCH_CHANNEL_MAPPED, MenuNames.MATCH_CHANNEL_MAPPED, "channleMatchMappedInfo.do", NO_SUB_MENU, NO_CLASS + "matchChannelMapped", SPORTS));
//        menuList.add(new MenuDTO(SPORTS_HOME_MATCH_MANAGEMENT, MenuNames.HOME_MATCH_MANAGEMENT, "homeMatchManage.do", NO_SUB_MENU, NO_CLASS + "homeMatchManagement", SPORTS));
//        menuList.add(new MenuDTO(RINGID_DISTRIBUTOR, MenuNames.RINGID_DISTRIBUTOR, "javascript:;", DROPDOWN_SUB_MENU, DROPDOWN_SUB_MENU_CLASS + "ringIdDistributor", COMMON));
//        menuList.add(new MenuDTO(RINGID_DISTRIBUTOR_MANAGE, "Manage " + MenuNames.RINGID_DISTRIBUTOR, "ringIdDistributorList.do", NO_SUB_MENU, NO_CLASS + "manageRingIdDistributor", RINGID_DISTRIBUTOR));
//        menuList.add(new MenuDTO(RINGID_DISTRIBUTOR_ADD, "Add " + MenuNames.RINGID_DISTRIBUTOR, "admin/ringIdDistributor/ringIdDistributorUI.jsp", NO_SUB_MENU, NO_CLASS + "addRingIdDistributor", RINGID_DISTRIBUTOR));
//        menuList.add(new MenuDTO(RING_FEATURE, MenuNames.RING_FEATURE, "javascript:;", DROPDOWN_SUB_MENU, DROPDOWN_SUB_MENU_CLASS + "ringFeature", COMMON));
//        menuList.add(new MenuDTO(RING_FEATURE_MANAGE, "Manage " + MenuNames.RING_FEATURE, "showRingFeatureList.do", NO_SUB_MENU, NO_CLASS + "manageRingFeature", RING_FEATURE));
//        menuList.add(new MenuDTO(RING_FEATURE_MAPPING, MenuNames.RING_FEATURE_MAPPING, "showRingFeatureMappingList.do", NO_SUB_MENU, NO_CLASS + "ringFeatureMapping", RING_FEATURE));
//        menuList.add(new MenuDTO(COIN_EXCHANGE, MenuNames.COIN_EXCHANGE, "javascript:;", DROPDOWN_SUB_MENU, DROPDOWN_SUB_MENU_CLASS + "coinExchange", COMMON));
//        menuList.add(new MenuDTO(PAYMENT_METHOD, MenuNames.PAYMENT_METHOD, "showPaymentMethodList.do", NO_SUB_MENU, NO_CLASS + "paymentMethod", COIN_EXCHANGE));
//        menuList.add(new MenuDTO(PAYMENT_METHOD_ADD, "Add " + MenuNames.PAYMENT_METHOD, "admin/coinExchange/paymentMethodUI.jsp", NO_SUB_MENU, NO_CLASS + "addPaymentMethod", COIN_EXCHANGE));
//        menuList.add(new MenuDTO(RINGBIT, MenuNames.RINGBIT, "javascript:;", DROPDOWN_SUB_MENU, DROPDOWN_SUB_MENU_CLASS + "ringBit", COMMON));
//        menuList.add(new MenuDTO(RINGBIT_IMAGE_O_TEXT, MenuNames.RINGBIT_IMAGE_O_TEXT, "showRingBitSettingsList.do", NO_SUB_MENU, NO_CLASS + "manageRingBitSettings", RINGBIT));
        //common mega-menu
        menuList.add(new MenuDTO(APP_CONSTANTS, MenuNames.APP_CONSTANT, "appConstantsListInfo.do", NO_SUB_MENU, NO_CLASS + "appConstants", COMMON));
        menuList.add(new MenuDTO(APP_STORES, MenuNames.APP_STORE, "appStoresListInfo.do", NO_SUB_MENU, NO_CLASS + "appStores", COMMON));
        menuList.add(new MenuDTO(LOG_SETTINGS, MenuNames.LOG_SETTING, "logSettingListInfo.do", NO_SUB_MENU, NO_CLASS + "logSettings", COMMON));
        menuList.add(new MenuDTO(SPAM_REASONS, MenuNames.SPAM_REASON, "spamReasonsListInfo.do", NO_SUB_MENU, NO_CLASS + "spamReasons", COMMON));
        menuList.add(new MenuDTO(SETTINGS, MenuNames.SETTING, "settingListInfo.do", NO_SUB_MENU, NO_CLASS + "settings", COMMON));
//        menuList.add(new MenuDTO(HOME_PAGE_BANNER_TYPE_SEQ, MenuNames.HOME_PAGE_BANNER_TYPE_SEQ, "settingListInfo.do?searchText=home-page-banner-type-seq", NO_SUB_MENU, NO_CLASS + "home-page-banner-type-seq", COMMON));
        menuList.add(new MenuDTO(LIVE_TO_CAHNNEL, MenuNames.LIVE_TO_CHANNEL, "liveToChannelListInfo.do", NO_SUB_MENU, NO_CLASS + "live-to-channel", COMMON));
        menuList.add(new MenuDTO(RING_STUDIO, MenuNames.RING_STUDIO, "ringStudioListInfo.do", NO_SUB_MENU, NO_CLASS + "ringStudio", COMMON));
        menuList.add(new MenuDTO(PERMITTED_IPS, MenuNames.PERMITTED_IP, "permittedIpsListInfo.do", NO_SUB_MENU, NO_CLASS + "permittedIPs", COMMON));
        menuList.add(new MenuDTO(DEACTIVATE_REASONS, MenuNames.DEACTIVATE_REASON, "deactivateReasonsListInfo.do", NO_SUB_MENU, NO_CLASS + "deactivateReasons", COMMON));
        menuList.add(new MenuDTO(FLOATING_MENUS, MenuNames.FLOATING_MENU, "showFloatingMenuList.do", NO_SUB_MENU, NO_CLASS + "floatingMenus", COMMON));
        menuList.add(new MenuDTO(FLOATING_MENU_SLIDE_ITEM, MenuNames.FLOATING_MENU_SLIDE_ITEM, "floatingMenuSlideItemList.do", NO_SUB_MENU, NO_CLASS + "floatingMenuSlide", COMMON));
        menuList.add(new MenuDTO(CASH_WALLET_PERMITTED_COUNTRY, MenuNames.CASH_WALLET_PERMITTED_COUNTRY, "showCashWalletPermittedCountryList.do", NO_SUB_MENU, NO_CLASS + "cashWalletPermittedCountry", COMMON));
        menuList.add(new MenuDTO(EVENT, MenuNames.EVENT, "showEventListInfo.do", NO_SUB_MENU, NO_CLASS + "event", COMMON));
        menuList.add(new MenuDTO(SPORTS_MATCH_CHANNEL_MAPPED, MenuNames.MATCH_CHANNEL_MAPPED, "channleMatchMappedInfo.do", NO_SUB_MENU, NO_CLASS + "matchChannelMapped", COMMON));
        menuList.add(new MenuDTO(SPORTS_HOME_MATCH_MANAGEMENT, MenuNames.HOME_MATCH_MANAGEMENT, "homeMatchManage.do", NO_SUB_MENU, NO_CLASS + "homeMatchManagement", COMMON));
        menuList.add(new MenuDTO(RINGID_DISTRIBUTOR, MenuNames.RINGID_DISTRIBUTOR, "ringIdDistributorList.do", NO_SUB_MENU, NO_CLASS + "ringIdDistributor", COMMON));
        menuList.add(new MenuDTO(RING_FEATURE, MenuNames.RING_FEATURE, "showRingFeatureList.do", NO_SUB_MENU, NO_CLASS + "ringFeature", COMMON));
        menuList.add(new MenuDTO(RING_FEATURE_MAPPING, MenuNames.RING_FEATURE_MAPPING, "showRingFeatureMappingList.do", NO_SUB_MENU, NO_CLASS + "ringFeatureMapping", COMMON));
        menuList.add(new MenuDTO(PAYMENT_METHOD, MenuNames.PAYMENT_METHOD, "showPaymentMethodList.do", NO_SUB_MENU, NO_CLASS + "paymentMethod", COMMON));
        menuList.add(new MenuDTO(RINGBIT_IMAGE_O_TEXT, MenuNames.RINGBIT_IMAGE_O_TEXT, "showRingBitSettingsList.do", NO_SUB_MENU, NO_CLASS + "manageRingBitSettings", COMMON));
        menuList.add(new MenuDTO(PROFESSION, MenuNames.PROFESSION, "showProfessionList.do", NO_SUB_MENU, NO_CLASS + " profession", COMMON));
        menuList.add(new MenuDTO(STATISTIC, MenuNames.STATISTIC, "statistic.do", NO_SUB_MENU, NO_CLASS + "statistic", COMMON));
        menuList.add(new MenuDTO(APP_UPDATE_URL, MenuNames.APP_UPDATE_URL, "appUpdateUrlListInfo.do", NO_SUB_MENU, NO_CLASS + "appUpdateUrl", COMMON));

        //notify
        menuList.add(new MenuDTO(NOTIFY_AUTH_SERVER, MenuNames.NOTIFY_AUTH_SERVER, "authServerNotification.do", NO_SUB_MENU, NO_CLASS + " notifyAuthServer", NOTIFY));
        menuList.add(new MenuDTO(NOTIFY_AUTH_CONTROLLER, MenuNames.NOTIFY_AUTH_CONTROLLER, "admin/authControllerReload/authControllerReload.jsp", NO_SUB_MENU, NO_CLASS + " notifyAuthController", NOTIFY));
        menuList.add(new MenuDTO(NOTIFY_BOTH, MenuNames.NOTIFY_BOTH, "admin/authControllerReload/notifyBoth.jsp", NO_SUB_MENU, NO_CLASS + " notifyBoth", NOTIFY));
        menuList.add(new MenuDTO(RELOAD, MenuNames.RELOAD, "reloadServers.do", NO_SUB_MENU, NO_CLASS + " reload", NOTIFY));
        menuList.add(new MenuDTO(CHECK_LOG, MenuNames.CHECK_LOG, "reloadCheckLog.do", NO_SUB_MENU, NO_CLASS + " checkLog", NOTIFY));

        //Utility
        menuList.add(new MenuDTO(RESET_RETRY_LIMIT, MenuNames.RESET_RETRY_LIMIT, "admin/ringId/resetRetryLimit/reset-retry-limit.jsp", NO_SUB_MENU, NO_CLASS + " resetRetryLimit", UTILITY));
        menuList.add(new MenuDTO(RESET_PASSWORD, MenuNames.RESET_PASSWORD, "admin/ringId/resetPassword/resetPassword.jsp", NO_SUB_MENU, NO_CLASS + " resetPassword", UTILITY));
        menuList.add(new MenuDTO(UNVERIFY_NUMBER, MenuNames.UNVERIFY_NUMBER, "admin/ringId/unverifyNumber/unverifyNumber.jsp", NO_SUB_MENU, NO_CLASS + " unverifyNumber", UTILITY));
        menuList.add(new MenuDTO(VERIFY_NUMBER, MenuNames.VERIFY_NUMBER, "admin/ringId/verifyNumber/verifyNumber.jsp", NO_SUB_MENU, NO_CLASS + " verifyNumber", UTILITY));
        menuList.add(new MenuDTO(LIVE_VERIFIED_SETTINGS, MenuNames.Live_VERIFIED_SETTING, "admin/ringId/liveVerifiedSettings/liveVerifiedSettings.jsp", NO_SUB_MENU, NO_CLASS + " liveVerifiedSettings", UTILITY));
        menuList.add(new MenuDTO(CHANGE_RINGID, MenuNames.CHANGE_RINGID, "admin/ringId/changeRingId/changeRingId.jsp", NO_SUB_MENU, NO_CLASS + " changeRingID", UTILITY));
        menuList.add(new MenuDTO(LIMIT_O_TIME_UPDATE, MenuNames.LIMIT_O_TIME_UPDATE, "updateRestrictionTime.do", NO_SUB_MENU, NO_CLASS + " updateRestrictionTime", UTILITY));
        menuList.add(new MenuDTO(LATEST_OTP, MenuNames.LATEST_OTP, "admin/ringId/latestOTP/latestOTP.jsp", NO_SUB_MENU, NO_CLASS + " latestOTP", UTILITY));
        menuList.add(new MenuDTO(USERID_FROM_RINGID, MenuNames.USERID_FROM_RINGID, "admin/ringId/userTableIdFromRingId/userTableIdFromRingId.jsp", NO_SUB_MENU, NO_CLASS + " userIdFromRingId", UTILITY));
        menuList.add(new MenuDTO(SEARCH_USER, MenuNames.SEARCH_USER, "searchUserContacts.do", NO_SUB_MENU, NO_CLASS + " searchUser", UTILITY));
//        menuList.add(new MenuDTO(SEARCH_USER_CONTACTS, MenuNames.SEARCH_USER_CONTACTS, "searchUserContacts.do", NO_SUB_MENU, NO_CLASS + " searchUserContacts", UTILITY));
        menuList.add(new MenuDTO(AUTO_FOLLOW_SETTING, MenuNames.AUTO_FOLLOW_SETTING, "admin/ringId/autoFollowSetting/autoFollowSetting.jsp", NO_SUB_MENU, NO_CLASS + " autoFollowSetting", UTILITY));
        menuList.add(new MenuDTO(NEWS_FEED, MenuNames.NEWS_FEED, "admin/ringId/newsfeed/newsfeed.jsp", NO_SUB_MENU, NO_CLASS + " newsFeed", UTILITY));
        menuList.add(new MenuDTO(FEED_HIGHLIGHTS, MenuNames.FEED_HIGHLIGHTS, "showFeedHighlightsList.do", NO_SUB_MENU, NO_CLASS + " feedHighlights", UTILITY));
        menuList.add(new MenuDTO(DELETE_VIRTUAL_RINGIDS, MenuNames.DELETE_VIRTUAL_RINGID, "virtualRingIDs.do", NO_SUB_MENU, NO_CLASS + " deleteVirtualRingIDs", UTILITY));
        menuList.add(new MenuDTO(RINGBIT_LOG, MenuNames.RINGBIT_LOG, "ringBitLogList.do", NO_SUB_MENU, NO_CLASS + " ringBitLog", UTILITY));
        menuList.add(new MenuDTO(ADMIN_ACTION, MenuNames.ADMIN_ACTION, "admin/ringId/adminAction/adminAction.jsp", NO_SUB_MENU, NO_CLASS + " adminAction", UTILITY));
        menuList.add(new MenuDTO(AUTO_FOLLOW_PAGES, MenuNames.AUTO_FOLLOW_PAGES, "autoFollowPages.do", NO_SUB_MENU, NO_CLASS + " autoFollow", UTILITY));
        menuList.add(new MenuDTO(ASSIGN_PAGE_RINGID, MenuNames.ASSIGN_PAGE_RINGID, "admin/ringId/assignPageRingId/assignPageRingId.jsp", NO_SUB_MENU, NO_CLASS + " assignPageRingId", UTILITY));

        //special users
        menuList.add(new MenuDTO(SPECIAL_USER_ACCOUNTS, MenuNames.SPECIAL_USER_ACCOUNT, "javascript:;", DROPDOWN_SUB_MENU, DROPDOWN_SUB_MENU_CLASS + " specialUsersAccounts", SPECIAL_USERS));
        menuList.add(new MenuDTO(SPECIAL_USER_ACCOUNTS_MANAGE, "Manage " + MenuNames.SPECIAL_USER_ACCOUNT, "showCelebrityList.do", NO_SUB_MENU, NO_CLASS + " manageSpecialUsersAccounts", SPECIAL_USER_ACCOUNTS));
        menuList.add(new MenuDTO(SPECIAL_USER_ACCOUNTS_ADD, "Add " + MenuNames.SPECIAL_USER_ACCOUNT, "admin/ringId/celebrity/makeCelebrity.jsp", NO_SUB_MENU, NO_CLASS + " addSpecialUsersAccount", SPECIAL_USER_ACCOUNTS));
        menuList.add(new MenuDTO(CELEBRITY_SCHEDULE, MenuNames.CELEBRITY_SCHEDULE, "showCelebrityScheduleList.do", NO_SUB_MENU, NO_CLASS + " celebritySchedule", SPECIAL_USERS));
        menuList.add(new MenuDTO(FEED_POST, MenuNames.FEED_POST, "admin/ringId/postCelebrityStatus/postCelebrityStatus.jsp", NO_SUB_MENU, NO_CLASS + " feedPost", SPECIAL_USERS));
        return menuList;
    }

    public HashMap<Integer, List<MenuDTO>> getSubMenuMap(List<MenuDTO> menuList) {
        HashMap<Integer, List<MenuDTO>> subMenuMap = new HashMap<>();
        for (MenuDTO dto : menuList) {
            if (dto.getParentId() > 0) {
                if (subMenuMap.containsKey(dto.getParentId())) {
                    List<MenuDTO> list = subMenuMap.get(dto.getParentId());
                    list.add(dto);
                } else {
                    List<MenuDTO> list = new ArrayList<>();
                    list.add(dto);
                    subMenuMap.put(dto.getParentId(), list);
                }
            }
        }
        return subMenuMap;
    }
}
