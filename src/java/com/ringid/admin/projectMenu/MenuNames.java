/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.projectMenu;

/**
 *
 * @author Rabby
 */
public class MenuNames {

    public static String HOME = "Home";

    /*Servers*/
    public static String SERVERS = "Servers";
    public static String AUTH_SERVER = "Auth Server";
    public static String CHAT_SERVER = "Chat Server";
    public static String IMAGE_SERVER = "Image Server";
//    public static String OFFLINE_SERVER = "Offline Server";
//    public static String RELAY_SERVER = "Relay Server";
    public static String SMS_SERVER = "SMS Server";
    public static String VOICE_SERVER = "Voice Server";
    public static String COIN_SERVER = "Coin Server";
    public static String CHANNEL_SERVER = "Channel Server";
    public static String STREAMING_SERVER = "Streaming Server";

    /*Page & Media*/
    public static String PAGE_O_MEDIA = "Page & Media";
    public static String PAGES = "Pages";
    public static String USER_PAGE = "User Page";
//    public static String CREATE_PAGE = "Create Page";
    public static String DONATION_PAGE = "Donation Page";
    public static String CATEGORY = "Category";
    public static String FEED_TYPE = "Feed Type";
    public static String BREAKING_FEED = "Breaking Feed";
    public static String MEDIA = "Media";

    /*Live / Channel*/
    public static String LIVE_O_CHANNEL = "Live / Channel";
    public static String CHANNEL = "Channel";
    public static String CHANNEL_CATEGORY = "Channel Category";
    public static String LIVE_ROOM = "Live Room";
    public static String SPECIAL_ROOM = "Special Room";
    public static String LIVE_INFO = "Live Info";
    public static String ACTIVE_USERS = "Active Users";
    public static String LIVE_HISTORY = "Live History";
    public static String OLD_LIVE_HISTORY = "Old Live History";
    public static String LIVE_SUMMARY = "Live Summary";
    public static String STREAMING_CODEC_SETTING = "Streaming Codec Setting";
    public static String CALL_O_LIVE_SUBSCRIPTION_FEE = "Call & Live Subscription Fee";
    public static String CHANNEL_SUBSCRIPTION = "Channel Subscription";
    public static String PAST_LIVE_FEED = "Past Live Feed";
    public static String POST_RECORDED_LIVE_FEED = "Post Recorded Live Feed";
    public static String AMBASSADOR = "Ring Market Ambassador";

    /*Common*/
    public static String COMMON = "Common";
    public static String APP_CONSTANT = "App Constant";
    public static String APP_STORE = "App Store";
    public static String LOG_SETTING = "Log Setting";
    public static String SPAM_REASON = "Spam Reason";
    public static String SETTING = "Setting";
    public static String HOME_PAGE_BANNER_TYPE_SEQ = "Home Page Banner Type Seq";
    public static String LIVE_TO_CHANNEL = "Live to Channel";
    public static String RING_STUDIO = "Ring Studio";
    public static String PERMITTED_IP = "Permitted IP";
    public static String DEACTIVATE_REASON = "Deactivate Reason";
    public static String FLOATING_MENU = "Floating Menu";
    public static String FLOATING_MENU_SLIDE_ITEM = "Floating Menu Slide Item";
    public static String CASH_WALLET_PERMITTED_COUNTRY = "Reward Permitted Country";
    public static String EVENT = "Event";
    public static String SPORTS = "Sports";
    public static String MATCH_CHANNEL_MAPPED = "Match Channel Mapped";
    public static String HOME_MATCH_MANAGEMENT = "Home Match Management";
    public static String RINGID_DISTRIBUTOR = "RingId Distributor";
    public static String RING_FEATURE = "Ring Feature";
    public static String RING_FEATURE_MAPPING = "Ring Feature Mapping";
    public static String COIN_EXCHANGE = "Coin Exchange";
    public static String PAYMENT_METHOD = "Payment Method";
    public static String RINGBIT = "RingBit";
    public static String RINGBIT_IMAGE_O_TEXT = "RingBit Image & Text";
    public static String PROFESSION = "Profession";
    public static String STATISTIC = "Statistic";
    public static String APP_UPDATE_URL = "App Update Url";

    /*Notify*/
    public static String NOTIFY = "Notify";
    public static String NOTIFY_AUTH_SERVER = "Notify Auth Server";
    public static String NOTIFY_AUTH_CONTROLLER = "Notify Auth Controller";
    public static String NOTIFY_BOTH = "Notify Auth Server and Controller";
    public static String RELOAD = "Reload";
    public static String CHECK_LOG = "Check Log";

    /*Utility*/
    public static String UTILITY = "Utility";
    public static String RESET_RETRY_LIMIT = "Reset Retry Limit";
    public static String RESET_PASSWORD = "Reset Password";
    public static String UNVERIFY_NUMBER = "Unverify Number";
    public static String VERIFY_NUMBER = "Verify Number";
    public static String Live_VERIFIED_SETTING = "Live Verified Setting";
    public static String CHANGE_RINGID = "Change RingID";
    public static String LIMIT_O_TIME_UPDATE = "Limit / Time Update";
    public static String LATEST_OTP = "Latest OTP";
    public static String USERID_FROM_RINGID = "UserId From RingId";
    public static String SEARCH_USER = "Search User";
    public static String SEARCH_USER_CONTACTS = "Search User Contacts";
    public static String AUTO_FOLLOW_SETTING = "Auto Follow Setting";
    public static String NEWS_FEED = "News Feed";
    public static String FEED_HIGHLIGHTS = "Feed Highlights";
    public static String DELETE_VIRTUAL_RINGID = "Delete Virtual RingID";
    public static String RINGBIT_LOG = "RingBit Log";
    public static String ADMIN_ACTION = "Admin Action";
    public static String AUTO_FOLLOW_PAGES = "Auto Follow Pages";
    public static String ASSIGN_PAGE_RINGID = "Assign Page RingId";

    /*Special User*/
    public static String SPECIAL_USER = "Special User";
    public static String SPECIAL_USER_ACCOUNT = "Special User Account";
    public static String CELEBRITY_SCHEDULE = "Celebrity Schedule";
    public static String FEED_POST = "Feed Post";

    /*Others*/
    public static String PAGE_FOLLOW_RULE = "Page Follow Rule";

    /*Super Admin Features*/
    public static String ADMIN_USER = "Admin User";
    public static String ADMIN_USER_ROLE = "Admin User Role";
    public static String PROJECT_FEATURE = "Project Feature";
    public static String FEATURE_ROLE_MAPPING = "Feature Role Mapping";

}
