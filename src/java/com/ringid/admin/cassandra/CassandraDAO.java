package com.ringid.admin.cassandra;

import auth.com.utils.Constants;
import org.ringid.channel.ChannelDTO;
import com.google.gson.Gson;
import com.ipvision.ringid.storage.adminstorage.AdminTransaction;
import com.ipvision.ringid.storage.channelstorage.ChannelTransaction;
import com.ipvision.ringid.storage.coin.connection.CoinStorageConnection;
import com.ipvision.ringid.storage.coin.dto.CoinLogDTO;
import com.ipvision.ringid.storage.connection.StorageConnection;
import com.ipvision.ringid.storage.livestorage.LiveStorageTransaction;
import com.ipvision.ringid.storage.solr.LiveStreamSearch;
import com.ipvision.ringid.storage.solr.RingIDSolrSearch;
import com.ipvision.ringid.storage.tx.StorageTransaction;
import com.ipvision.ringid.storage.utils.Pair;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.utils.ReasonCode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import org.ringid.celebrities.CelebrityCategoryDTO;
import org.ringid.channel.ChannelCategoryDTO;
import org.ringid.channel.ChannelMediaDTO;
import org.ringid.channel.ChannelSubscriptionDTO;
import org.ringid.contacts.FollowRuleDTO;
import org.ringid.contacts.SearchedContactDTO;
import org.ringid.event.EventDTO;
import org.ringid.feedbacks.ChannelFeedBack;
import org.ringid.livestream.StreamRoomDTO;
import org.ringid.livestream.StreamingDTO;
import org.ringid.livestream.StreamingTariffDTO;
import org.ringid.newsfeeds.CassAlbumDTO;
import org.ringid.newsfeeds.CassImageDTO;
import org.ringid.newsfeeds.CassMultiMediaDTO;
import org.ringid.newsfeeds.FeedDTO;
import org.ringid.newsfeeds.FeedMoodDTO;
import org.ringid.newsfeeds.FeedReturnDTO;
import org.ringid.newsfeeds.HighlightedFeedDTO;
import org.ringid.newsportals.NPBasicFeedInfoDTO;
import org.ringid.pages.CustomCategoryDTO;
import org.ringid.pages.PageCategoryDTO;
import org.ringid.pages.PageDTO;
import org.ringid.pages.PageFeedBackDTO;
import org.ringid.pages.PageSearchDTO;
import org.ringid.pages.ServiceDTO;
import org.ringid.users.DeactiveReasonDTO;
import org.ringid.users.LiveUserDetailsDTO;
import org.ringid.users.UserBasicInfoDTO;
import org.ringid.users.UserDTO;
import org.ringid.users.UserDeactivationDTO;
import org.ringid.users.UserDeviceDTO;
import org.ringid.users.UserLoginDetailsDTO;
import org.ringid.users.UserProfileDetailsDTO;
import org.ringid.users.UserSettingsDTO;
import org.ringid.users.UserShortInfoDTO;
import org.ringid.utilities.AppConstants;
import static org.ringid.utilities.AppConstants.SCROLL_DOWN;
import org.ringid.utilities.ListReturnDTO;
import org.ringid.utilities.ObjectReturnDTO;
import org.ringid.utilities.PivotedListReturnDTO;
import org.ringid.utilities.ReturnDTO;

public class CassandraDAO {

    private static CassandraDAO instance = null;
    private static AdminTransaction transaction = null;
    private static LiveStorageTransaction liveStorageTransaction = null;
    private static StorageTransaction storageTransaction = null;
    private static ChannelTransaction channelTransaction = null;
    private static CoinStorageConnection coinStorageConnection = null;
    private static final int PAGE_MEDIA_TYPE = 20;
    private final static int TRENDING_LIMIT = 100;
//    private final int GLOBAL_COUNTRY_CODE = 1000;//880 for local;
    private final int COMMON_BREAKING_LIST_LIMIT = 1000;
    private final int LIMIT = 100;
    private final int LIMIT_ONE_THOUSAND = 1000;

    private CassandraDAO() throws Exception {
        transaction = AdminTransaction.getInstance();
        liveStorageTransaction = LiveStorageTransaction.getInstance();
        storageTransaction = StorageConnection.getInstance().getTransaction();
        channelTransaction = ChannelTransaction.getInstance();
        coinStorageConnection = CoinStorageConnection.getInstance();
    }

    public static CassandraDAO getInstance() throws Exception {
        if (instance == null) {
            createInstance();
        }
        return instance;
    }

    private static synchronized void createInstance() throws Exception {
        if (instance == null) {
            instance = new CassandraDAO();
        }
    }

    public Pair<Long, Integer> userLastSeen(long userId) {
        Pair<Long, Integer> userLastSeenInfo = null;
        try {
            RingLogger.getConfigPortalLogger().debug("[userLastSeen] --> userId --> " + userId);
            userLastSeenInfo = storageTransaction.userLastSeen(userId);
            RingLogger.getConfigPortalLogger().debug("[userLastSeen] userLastSeenInfo --> " + new Gson().toJson(userLastSeenInfo));
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [userLastSeen] --> " + e.getMessage());
        }
        return userLastSeenInfo;
    }

    public int assignPageRingId(long userId, long ringId) {
        int reasonCode = -1;
        try {
            RingLogger.getConfigPortalLogger().debug("[assignPageRingId] --> userId --> " + userId + " ringId --> " + ringId);
            reasonCode = storageTransaction.assignPageRingId(userId, ringId);
            RingLogger.getConfigPortalLogger().debug("[assignPageRingId] registrationDate --> " + reasonCode);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [assignPageRingId] --> " + e.getMessage());
        }
        return reasonCode;
    }

    public long getRegistrationDate(long userId) {
        long registrationDate = 0L;
        try {
            RingLogger.getConfigPortalLogger().debug("[getRegistrationDate] --> userId --> " + userId);
            registrationDate = storageTransaction.getRegistrationDate(userId);
            RingLogger.getConfigPortalLogger().debug("[getRegistrationDate] registrationDate --> " + registrationDate);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [getRegistrationDate] --> " + e.getMessage());
        }
        return registrationDate;
    }

    public int makePageDefault(long ownerId, long pageId) {
        int reasonCode = -1;
        try {
            RingLogger.getConfigPortalLogger().debug("[makePageDefault] --> ownerId --> " + ownerId + " pageId --> " + pageId);
            reasonCode = storageTransaction.makePageDefault(ownerId, pageId);
            RingLogger.getConfigPortalLogger().debug("[makePageDefault] reasonCode --> " + reasonCode);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [makePageDefault] --> " + e.getMessage());
        }
        return reasonCode;
    }

    public int addVerifiedTopPastLiveFeed(UUID liveTimeUUID, UUID feedId) {
        int reasonCode = -1;
        try {
            RingLogger.getConfigPortalLogger().debug("[addVerifiedTopPastLiveFeed] --> liveTimeUUID --> " + liveTimeUUID + " feedId --> " + feedId);
            reasonCode = storageTransaction.addVerifiedTopPastLiveFeed(liveTimeUUID, feedId);
            RingLogger.getConfigPortalLogger().debug("[addVerifiedTopPastLiveFeed] reasonCode --> " + reasonCode);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [addVerifiedTopPastLiveFeed] --> " + e.getMessage());
        }
        return reasonCode;
    }

    public int removeVerifiedTopPastLiveFeed(UUID liveTimeUUID, UUID feedId) {
        int reasonCode = -1;
        try {
            RingLogger.getConfigPortalLogger().debug("[removeVerifiedTopPastLiveFeed] --> liveTimeUUID --> " + liveTimeUUID + " feedId --> " + feedId);
            reasonCode = storageTransaction.removeVerifiedTopPastLiveFeed(liveTimeUUID, feedId);
            RingLogger.getConfigPortalLogger().debug("[removeVerifiedTopPastLiveFeed] reasonCode --> " + reasonCode);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [removeVerifiedTopPastLiveFeed] --> " + e.getMessage());
        }
        return reasonCode;
    }

    public int addVerifiedCelebrityPastLiveFeed(UUID liveTimeUUID, UUID feedId) {
        int reasonCode = -1;
        try {
            RingLogger.getConfigPortalLogger().debug("[addVerifiedCelebrityPastLiveFeed] --> liveTimeUUID --> " + liveTimeUUID + " feedId --> " + feedId);
            reasonCode = storageTransaction.addVerifiedCelebrityPastLiveFeed(liveTimeUUID, feedId);
            RingLogger.getConfigPortalLogger().debug("[addVerifiedCelebrityPastLiveFeed] reasonCode --> " + reasonCode);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [addVerifiedCelebrityPastLiveFeed] --> " + e.getMessage());
        }
        return reasonCode;
    }

    public int removeVerifiedCelebrityPastLiveFeed(UUID liveTimeUUID, UUID feedId) {
        int reasonCode = -1;
        try {
            RingLogger.getConfigPortalLogger().debug("[removeVerifiedCelebrityPastLiveFeed] --> liveTimeUUID --> " + liveTimeUUID + " feedId --> " + feedId);
            reasonCode = storageTransaction.removeVerifiedCelebrityPastLiveFeed(liveTimeUUID, feedId);
            RingLogger.getConfigPortalLogger().debug("[removeVerifiedCelebrityPastLiveFeed] reasonCode --> " + reasonCode);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [removeVerifiedCelebrityPastLiveFeed] --> " + e.getMessage());
        }
        return reasonCode;
    }

    public List<CoinLogDTO> getCoinTransaction(long userId, UUID pivotLogId, boolean isGift, int scrollDir, int limit) {
        RingLogger.getConfigPortalLogger().debug("[getCoinTransaction] userId -> " + userId + " pivotLogId -> " + pivotLogId + " isGift --> " + isGift + " scrollDir --> " + scrollDir + " limit --> " + limit);
        List<CoinLogDTO> coinLogDTOs = new ArrayList<>();
        try {
            coinLogDTOs = coinStorageConnection.getTransaction().getCoinTransaction(userId, pivotLogId, isGift, scrollDir, limit);
            if (coinLogDTOs != null && coinLogDTOs.size() > 0) {
                RingLogger.getConfigPortalLogger().debug("[getCoinTransaction] coinLogDTOs -> " + new Gson().toJson(coinLogDTOs));
            } else {
                RingLogger.getConfigPortalLogger().debug("[getCoinTransaction] coinLogDTOs -> null");
            }
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().debug("Exception in [getCoinTransaction] -> " + e);
        }
        return coinLogDTOs;
    }

    public List<Long> getAllProductPageIds(long pivotPageId, int limit) {
        RingLogger.getConfigPortalLogger().debug("[getAllProductPageIds] pivotPageId -> " + pivotPageId + " limit -> " + limit);
        List<Long> productPageList = new ArrayList<>();
        try {
            productPageList = transaction.getAllProductPageIds(pivotPageId, limit);
            RingLogger.getConfigPortalLogger().debug("[getAllProductPageIds] productPageList -> " + new Gson().toJson(productPageList));
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().debug("Exception in [getAllProductPageIds] -> " + e);
        }
        return productPageList;
    }

    public List<Long> getAmbassadorsOfProductPage(long prodcutPageId) {
        RingLogger.getConfigPortalLogger().debug("[getAmbassadorsOfProductPage] prodcutPageId -> " + prodcutPageId);
        List<Long> ambassadorList = new ArrayList<>();
        try {
            ambassadorList = transaction.getAmbassadorsOfProductPage(prodcutPageId);
            RingLogger.getConfigPortalLogger().debug("[getAmbassadorsOfProductPage] ambassadorList -> " + new Gson().toJson(ambassadorList));
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().debug("Exception in [getAmbassadorsOfProductPage] -> " + e);
        }
        return ambassadorList;
    }

    public List<Long> addAmbassador(long productPageId, List<Long> ambassadorList) {
        RingLogger.getConfigPortalLogger().debug("[addAmbassador] productPageId -> " + productPageId + " ambassadorList -> " + new Gson().toJson(ambassadorList));
        List<Long> unsuccessfulList = new ArrayList<>();
        try {
            unsuccessfulList = transaction.addAmbassador(productPageId, ambassadorList);
            RingLogger.getConfigPortalLogger().debug("[addAmbassador] unsuccessfulList -> " + new Gson().toJson(unsuccessfulList));
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().debug("Exception in [addAmbassador] -> " + e);
        }
        return unsuccessfulList;
    }

    public List<Long> removeAmbassador(long productPageId, List<Long> ambassadorList) {
        RingLogger.getConfigPortalLogger().debug("[removeAmbassador] productPageId -> " + productPageId + " ambassadorList -> " + new Gson().toJson(ambassadorList));
        List<Long> unsuccessfulList = new ArrayList<>();
        try {
            unsuccessfulList = transaction.removeAmbassador(productPageId, ambassadorList);
            RingLogger.getConfigPortalLogger().debug("[removeAmbassador] unsuccessfulList -> " + new Gson().toJson(unsuccessfulList));
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().debug("Exception in [removeAmbassador] -> " + e);
        }
        return unsuccessfulList;
    }

    public int verifyPage(long pageId, int pageType) {
        RingLogger.getConfigPortalLogger().debug("[verifyPage] pageId -> " + pageId + " pageType -> " + pageType);
        int reasonCode = -1;
        try {
            reasonCode = transaction.verifyPage(pageId, pageType);
            RingLogger.getConfigPortalLogger().debug("[verifyPage] reasonCode -> " + reasonCode);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().debug("Exception in [verifyPage] -> " + e);
        }
        return reasonCode;
    }

    public int unverifyPage(long pageId, int pageType) {
        RingLogger.getConfigPortalLogger().debug("[unverifyPage] pageId -> " + pageId + " pageType -> " + pageType);
        int reasonCode = -1;
        try {
            reasonCode = transaction.unverifyPage(pageId, pageType);
            RingLogger.getConfigPortalLogger().debug("[unverifyPage] reasonCode -> " + reasonCode);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().debug("Exception in [unverifyPage] -> " + e);
        }
        return reasonCode;
    }

    public List<Long> getVerifiedPages(int pageType, long pivotId, int limit) {
        RingLogger.getConfigPortalLogger().debug("[getVerifiedPages] pageType -> " + pageType + " pivotId -> " + pivotId + " limit -> " + limit);
        List<Long> pageIds = new ArrayList<>();
        try {
            pageIds = transaction.getVerifiedPages(pageType, pivotId, limit);
            RingLogger.getConfigPortalLogger().debug("[getVerifiedPages] pageIds -> " + new Gson().toJson(pageIds));
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().debug("Exception in [getVerifiedPages] -> " + e);
        }
        return pageIds;
    }

    public List<String> addProfessionData(List<String> professions) {
        RingLogger.getConfigPortalLogger().debug("[addProfessionData] professions -> " + professions);
        List<String> professionsList = new ArrayList<>();
        try {
            professionsList = transaction.addProfessionData(professions);
            RingLogger.getConfigPortalLogger().debug("[addProfessionData] reasonCode -> " + professionsList.size());
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().debug("Exception in [addProfessionData] -> " + e);
        }
        return professionsList;
    }

    public LinkedHashMap<UUID, String> getProfessionList(int start, int limit) {
        RingLogger.getConfigPortalLogger().debug("[getProfessionList] start -> " + start + " limit --> " + limit);
        LinkedHashMap<UUID, String> professionsLinkedHashMap = new LinkedHashMap<>();
        try {
            professionsLinkedHashMap = transaction.getProfessionList(start, limit);
            RingLogger.getConfigPortalLogger().debug("[getProfessionList] professionsList.size -> " + new Gson().toJson(professionsLinkedHashMap));
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().debug("Exception in [getProfessionList] -> " + e);
        }
        return professionsLinkedHashMap;
    }

    public int deleteProfession(UUID professionId) {
        RingLogger.getConfigPortalLogger().debug("[deleteProfession] professionId -> " + professionId);
        int reasonCode = -1;
        try {
            reasonCode = transaction.deleteProfession(professionId);
            RingLogger.getConfigPortalLogger().debug("[deleteProfession] reasonCode -> " + reasonCode);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().debug("Exception in [deleteProfession] -> " + e);
        }
        return reasonCode;
    }

    public int addAutoFollowForUserPage(long pageId, FollowRuleDTO dto) {
        RingLogger.getConfigPortalLogger().debug("[addAutoFollowForUserPage] pageId -> " + pageId + " followRuleDTO -> " + new Gson().toJson(dto));
        int reasonCode = -1;
        try {
            reasonCode = transaction.addAutoFollowForUserPage(pageId, dto);
            RingLogger.getConfigPortalLogger().debug("[addAutoFollowForUserPage] reasonCode -> " + reasonCode);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().debug("Exception in [addAutoFollowForUserPage] -> " + e);
        }
        return reasonCode;
    }

    public PageFeedBackDTO addUserDefinedPageCategory(long pageId, CustomCategoryDTO customDTO, long sessionUserId) {
        RingLogger.getConfigPortalLogger().debug("[addUserDefinedPageCategory] pageId -> " + pageId + " customDTO -> " + new Gson().toJson(customDTO) + " sessionUserId -> " + sessionUserId);
        PageFeedBackDTO pageFeedBackDTO = new PageFeedBackDTO();
        try {
            pageFeedBackDTO = storageTransaction.addUserDefinedPageCategory(pageId, customDTO, sessionUserId);
            RingLogger.getConfigPortalLogger().debug("[addUserDefinedPageCategory] reasonCode -> " + new Gson().toJson(pageFeedBackDTO));
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().debug("Exception in [addUserDefinedPageCategory] -> " + e);
        }
        return pageFeedBackDTO;
    }

    public int updatePageProfileImage(long sessionUserId, long pageId, UUID profileImageId, String imageUrl) {
        RingLogger.getConfigPortalLogger().debug("[updatePageProfileImage] sessionUserId -> " + sessionUserId + " pageId -> " + pageId + " profileImageId -> " + profileImageId + " imageUrl -> " + imageUrl);
        int reasonCode = -1;
        try {
            reasonCode = storageTransaction.updatePageProfileImage(sessionUserId, pageId, profileImageId, imageUrl);
            RingLogger.getConfigPortalLogger().debug("[updatePageProfileImage] reasonCode -> " + reasonCode);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().debug("Exception in [updatePageProfileImage] -> " + e);
        }
        return reasonCode;
    }

    public int updatePageCoverImage(long sessionUserId, long pageId, UUID profileImageId, String imageUrl, int x, int y) {
        RingLogger.getConfigPortalLogger().debug("[updatePageCoverImage]sessionUserId -> " + sessionUserId + "  pageId -> " + pageId + " profileImageId -> " + profileImageId + " imageUrl -> " + imageUrl + " x -> " + x + " y -> " + y);
        int reasonCode = -1;
        try {
            reasonCode = storageTransaction.updatePageCoverImage(sessionUserId, pageId, profileImageId, imageUrl, x, y);
            RingLogger.getConfigPortalLogger().debug("[updatePageCoverImage] reasonCode -> " + reasonCode);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().debug("Exception in [updatePageCoverImage] -> " + e);
        }
        return reasonCode;
    }

    public int updateOTPRetryLimit(int limit) {
        int reasonCode = -1;
        try {
            RingLogger.getConfigPortalLogger().debug("[updateOTPRetryLimit] --> limit --> " + limit);
            boolean result = transaction.updateOTPRetryLimit(limit);
            if (result == true) {
                reasonCode = 0;
            }
            RingLogger.getConfigPortalLogger().debug("[updateOTPRetryLimit] reasonCode --> " + reasonCode);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [updateOTPRetryLimit] --> " + e.getMessage());
        }
        return reasonCode;
    }

    public int updateOTPRetryResetTime(long time) {
        int reasonCode = -1;
        try {
            RingLogger.getConfigPortalLogger().debug("[updateOTPRetryResetTime] --> time --> " + time);
            boolean result = transaction.updateOTPRetryResetTime(time);
            if (result == true) {
                reasonCode = 0;
            }
            RingLogger.getConfigPortalLogger().debug("[updateOTPRetryResetTime] reasonCode --> " + reasonCode);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [updateOTPRetryResetTime] --> " + e.getMessage());
        }
        return reasonCode;
    }

    public int getOTPRetryLimit() {
        int limit = 0;
        try {
            limit = transaction.getOTPRetryLimit();
            RingLogger.getConfigPortalLogger().debug("[getOTPRetryLimit] limit --> " + limit);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [getOTPRetryLimit] --> " + e.getMessage());
        }
        return limit;
    }

    public long getOTPRetryLimitResetTime() {
        long time = 0L;
        try {
            time = transaction.getOTPRetryLimitResetTime();
            RingLogger.getConfigPortalLogger().debug("[getOTPRetryLimitResetTime ] time --> " + time);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [getOTPRetryLimitResetTime ] --> " + e.getMessage());
        }
        return time;
    }

    public int getAnonymousCallStatus(long userId, int appType) {
        int anonymousCallStatus = 0;
        try {
            RingLogger.getConfigPortalLogger().debug("[getAnonymousCallStatus] --> userId --> " + userId + " appType --> " + appType);
            anonymousCallStatus = storageTransaction.getAnonymousCallStatus(userId, appType);
            RingLogger.getConfigPortalLogger().debug("[getAnonymousCallStatus ] anonymousCallStatus --> " + anonymousCallStatus);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [getAnonymousCallStatus ] --> " + e.getMessage());
        }
        return anonymousCallStatus;
    }

    public boolean getGlobalCallStatus(long userId, int appType) {
        boolean globalCallStatus = false;
        try {
            RingLogger.getConfigPortalLogger().debug("[getGlobalCallStatus] --> userId --> " + userId + " appType --> " + appType);
            globalCallStatus = storageTransaction.getGlobalCallStatus(userId, appType);
            RingLogger.getConfigPortalLogger().debug("[getGlobalCallStatus ] anonymousCallStatus --> " + globalCallStatus);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [getGlobalCallStatus ] --> " + e.getMessage());
        }
        return globalCallStatus;
    }

    public UserLoginDetailsDTO getLoginUserByRingId(long ringId, int appType, int devicePlatform) {
        UserLoginDetailsDTO dto = new UserLoginDetailsDTO();
        try {
            RingLogger.getConfigPortalLogger().debug("[getLoginUserByRingId] --> ringId --> " + ringId + " appType --> " + appType + " devicePlatform --> " + devicePlatform);
            dto = storageTransaction.getLoginUserByRingId(ringId, appType, devicePlatform);
            RingLogger.getConfigPortalLogger().debug("[getLoginUserByRingId] userLoginDetailsDTO --> " + new Gson().toJson(dto));
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [getLoginUserByRingId] --> " + e.getMessage());
        }
        return dto;
    }

    public int getPendingFriendLimit() {
        int limit = 0;
        try {
            limit = transaction.getPendingFriendLimit();
            RingLogger.getConfigPortalLogger().debug("[getPendingFriendLimit ] limit --> " + limit);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [getPendingFriendLimit ] --> " + e.getMessage());
        }
        return limit;
    }

    public int getTotalFriendLimit() {
        int limit = 0;
        try {
            limit = transaction.getTotalFriendLimit();
            RingLogger.getConfigPortalLogger().debug("[getTotalFriendLimit ] limit --> " + limit);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [getTotalFriendLimit ] --> " + e.getMessage());
        }
        return limit;
    }

    public int updateIsChannelSharable(UUID channelId, boolean isChannelSharable) {
        int reasonCode = -1;
        try {
            RingLogger.getConfigPortalLogger().debug("[updateIsChannelSharable] --> channelId --> " + channelId + " isChannelSharable --> " + isChannelSharable);
            reasonCode = transaction.updateIsChannelSharable(channelId, isChannelSharable);
            RingLogger.getConfigPortalLogger().debug("[updateIsChannelSharable] reasonCode --> " + reasonCode);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [updateIsChannelSharable] --> " + e.getMessage());
        }
        return reasonCode;
    }

    public ArrayList<UserDTO> getFriendList(long userId, long pivotFriendId, int limit) {
        RingLogger.getConfigPortalLogger().info("[getFriendCount] userId -> " + userId + " pivotFriendId -> " + pivotFriendId + " limit -> " + limit);
        ArrayList<UserDTO> friendList = null;
        try {
            friendList = storageTransaction.getFriends(userId, pivotFriendId, limit);
            if (Constants.SERVER.equals("dev")) {
                RingLogger.getConfigPortalLogger().info("[getFriendCount] friendList -> " + new Gson().toJson(friendList));
            } else {
                RingLogger.getConfigPortalLogger().info("[getFriendCount] friendList.size -> " + friendList.size());
            }
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [getFriendList] -> " + e);
        }
        return friendList;
    }

    public long getFriendCount(long userId) {
        RingLogger.getConfigPortalLogger().info("[getFriendCount] userId -> " + userId);
        long friendCount = 0;
        try {
            friendCount = storageTransaction.getFriendCount(userId);
            RingLogger.getConfigPortalLogger().info("[getFriendCount] friendCount -> " + friendCount);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [getFriendCount] -> " + e);
        }
        return friendCount;
    }

    public FeedMoodDTO getFeedMoodDTO(int moodId) {
        FeedMoodDTO dto = null;
        RingLogger.getConfigPortalLogger().error("[getFeedMoodDTO] moodId -> " + moodId);
        try {
            dto = storageTransaction.getFeedMoodDTO(moodId);
            RingLogger.getConfigPortalLogger().error("[getFeedMoodDTO] feedMoodDTO -> " + new Gson().toJson(dto));
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [getFeedMoodDTO] --> " + e.getMessage());
        }
        return dto;
    }

    public List<FeedMoodDTO> getFeedMoodDTOList(long time) {
        List<FeedMoodDTO> list = new ArrayList<>();
        RingLogger.getConfigPortalLogger().error("[getFeedMoodDTOList] time -> " + time);
        try {
            list = storageTransaction.getFeedMoodDTOList(time);
            if (Constants.SERVER.equals("dev")) {
                RingLogger.getConfigPortalLogger().error("[getFeedMoodDTOList] feedMoodList -> " + new Gson().toJson(list));
            } else {
                RingLogger.getConfigPortalLogger().error("[getFeedMoodDTOList] feedMoodList.size -> " + list.size());
            }
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [getFeedMoodDTOList] --> " + e.getMessage());
        }
        return list;
    }

    public int addFeaturedPastLiveFeed(UUID liveTimeUUID, UUID feedId) {
        int reasonCode = -1;
        try {
            RingLogger.getConfigPortalLogger().debug("[addFeaturedPastLiveFeed] --> liveTimeUUID --> " + liveTimeUUID + " feedId --> " + feedId);
            reasonCode = storageTransaction.addFeaturedPastLiveFeed(liveTimeUUID, feedId);
            RingLogger.getConfigPortalLogger().debug("[addFeaturedPastLiveFeed] reasonCode --> " + reasonCode);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [addFeaturedPastLiveFeed] --> " + e.getMessage());
        }
        return reasonCode;
    }

    public int removeFeaturedPastLiveFeed(UUID liveTimeUUID, UUID feedId) {
        int reasonCode = -1;
        try {
            RingLogger.getConfigPortalLogger().debug("[removeFeaturedPastLiveFeed] --> liveTimeUUID --> " + liveTimeUUID + " feedId --> " + feedId);
            reasonCode = storageTransaction.removeFeaturedPastLiveFeed(liveTimeUUID, feedId);
            RingLogger.getConfigPortalLogger().debug("[removeFeaturedPastLiveFeed] reasonCode --> " + reasonCode);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [removeFeaturedPastLiveFeed] --> " + e.getMessage());
        }
        return reasonCode;
    }

    public List<ChannelDTO> getOfficiallyVerifiedChannels() {
        List<ChannelDTO> featuredChannelList = new ArrayList<>();
        PivotedListReturnDTO<ChannelDTO, UUID> pivotedListReturnDTO = new PivotedListReturnDTO<>();
        UUID pivotId = null;
        try {
            do {
                RingLogger.getConfigPortalLogger().debug("[getOfficiallyVerifiedChannels] pivotId --> " + pivotId + "  limit --> " + LIMIT);
                pivotedListReturnDTO = channelTransaction.getChannelDAO().getOfficiallyVerifiedChannels(pivotId, LIMIT);
                pivotId = pivotedListReturnDTO.getPivot();
                if (pivotedListReturnDTO.getList() != null && !pivotedListReturnDTO.getList().isEmpty()) {
                    featuredChannelList.addAll(pivotedListReturnDTO.getList());
                } else {
                    break;
                }
            } while (pivotedListReturnDTO.getList().size() == LIMIT);
            if (Constants.SERVER.equals("dev")) {
                RingLogger.getConfigPortalLogger().debug("[getOfficiallyVerifiedChannels] officially verifiedChannels -> " + new Gson().toJson(featuredChannelList));
            } else {
                RingLogger.getConfigPortalLogger().debug("[getOfficiallyVerifiedChannels] officially verifiedChannels.size -> " + featuredChannelList.size());
            }
        } catch (Throwable e) {
            RingLogger.getConfigPortalLogger().error("Throwable [getFeaturedChannels] --> ", e);
        }
        return featuredChannelList;
    }

    public int verifyChannelOfficially(UUID channelId) {
        int reasonCode = -1;
        try {
            RingLogger.getConfigPortalLogger().debug("[verifyChannelOfficially] --> channelId --> " + channelId);
            reasonCode = transaction.verifyChannelOfficially(channelId);
            RingLogger.getConfigPortalLogger().debug("[verifyChannelOfficially] reasonCode --> " + reasonCode);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [verifyChannelOfficially] --> " + e.getMessage());
        }
        return reasonCode;
    }

    public int unverifyChannelOfficially(UUID channelId) {
        int reasonCode = -1;
        try {
            RingLogger.getConfigPortalLogger().debug("[unverifyChannelOfficially] --> channelId --> " + channelId);
            reasonCode = transaction.unverifyChannelOfficially(channelId);
            RingLogger.getConfigPortalLogger().debug("[unverifyChannelOfficially] reasonCode --> " + reasonCode);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [unverifyChannelOfficially] --> " + e.getMessage());
        }
        return reasonCode;
    }

    public long getTotalFollowerCount(long userId) {
        long followerCount = 0;
        try {
            followerCount = storageTransaction.getTotalFollowerCount(userId);
            RingLogger.getConfigPortalLogger().debug("[getTotalFollowerCount] followerCount --> " + followerCount);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [getTotalFollowerCount] --> " + e.getMessage());
        }
        return followerCount;
    }

    public long getTotalFollowingCount(long userId) {
        long followingCount = 0;
        try {
            followingCount = storageTransaction.getTotalFollowingCount(userId);
            RingLogger.getConfigPortalLogger().debug("[getTotalFollowingCount] followingCount --> " + followingCount);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [getTotalFollowingCount] --> " + e.getMessage());
        }
        return followingCount;
    }

    public int resetSecondaryContactVerificationHistory(long userTableId, int contactType) {
        int result = -1;
        try {
            RingLogger.getConfigPortalLogger().debug("[resetSecondaryContactVerificationHistory] --> userId : " + userTableId + " contactType : " + contactType);
            result = transaction.resetSecondaryContactVerificationHistory(userTableId, contactType);
            RingLogger.getConfigPortalLogger().debug("[resetSecondaryContactVerificationHistory] result --> " + result);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [resetSecondaryContactVerificationHistory] --> " + e.getMessage());
        }
        return result;
    }

    public int resetDeviceIdVerificationHistory(String deviceId) {
        int result = -1;
        try {
            RingLogger.getConfigPortalLogger().debug("[resetDeviceIdVerificationHistory] --> deviceId : " + deviceId);
            result = transaction.resetDeviceIdVerificationHistory(deviceId);
            RingLogger.getConfigPortalLogger().debug("[resetDeviceIdVerificationHistory] result --> " + result);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [resetDeviceIdVerificationHistory] --> " + e.getMessage());
        }
        return result;
    }

    public List<SearchedContactDTO> getSearchResult(String searchKey, List<Integer> userTypes, int start, int limit) {
        List<SearchedContactDTO> searchedContactDTOs = new ArrayList<>();
        RingLogger.getConfigPortalLogger().info("[getSearchResult] searchKey -> " + searchKey + " userTypes -> " + new Gson().toJson(userTypes) + " start -> " + start + " limit -> " + limit);
        try {
            searchedContactDTOs = storageTransaction.getSearchResult(0, searchKey, userTypes, start, limit);
            if (Constants.SERVER.equals("dev")) {
                RingLogger.getConfigPortalLogger().info("[getSearchResult] searchedContactDTOs -> " + new Gson().toJson(searchedContactDTOs));
            } else {
                RingLogger.getConfigPortalLogger().info("[getSearchResult] searchedContactDTOs.size -> " + searchedContactDTOs.size());
            }
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [getSearchResult] --> " + e);
        }
        return searchedContactDTOs;

    }

    public int updateTotalFriendsLimit(int limit) {
        int reasonCode = -1;
        RingLogger.getConfigPortalLogger().info("[updateTotalFriendsLimit] limit -> " + limit);
        try {
            boolean res = transaction.updateTotalFriendsLimit(limit);
            reasonCode = res ? 0 : -1;
            RingLogger.getConfigPortalLogger().info("[updateTotalFriendsLimit] reasonCode -> " + reasonCode);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [updateTotalFriendsLimit] --> " + e);
        }
        return reasonCode;
    }

    public int updatePendingFriendsLimit(int limit) {
        int reasonCode = -1;
        RingLogger.getConfigPortalLogger().info("[updatePendingFriendsLimit] limit -> " + limit);
        try {
            boolean res = transaction.updatePendingFriendsLimit(limit);
            reasonCode = res ? 0 : -1;
            RingLogger.getConfigPortalLogger().info("[updatePendingFriendsLimit] reasonCode -> " + reasonCode);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [updatePendingFriendsLimit] --> " + e);
        }
        return reasonCode;
    }

    public int deactivateUserAccount(UserDeactivationDTO dto) {
        RingLogger.getConfigPortalLogger().debug("[deactivateUserAccount] dto --> " + new Gson().toJson(dto));
        int reasonCode = -1;
        try {
            reasonCode = storageTransaction.deactivateUserAccount(dto);
            RingLogger.getConfigPortalLogger().debug("[deactivateUserAccount] reasonCode --> " + reasonCode);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [deactivateUserAccount] --> " + e);
        }
        return reasonCode;
    }

    public ListReturnDTO<ChannelSubscriptionDTO> getChannelSubscriptionTypes() {
        ListReturnDTO<ChannelSubscriptionDTO> listReturnDTO = new ListReturnDTO<>();
        try {
            listReturnDTO = channelTransaction.getChannelSubscriptionTypes();
            if (listReturnDTO != null) {
                if (Constants.SERVER.equals("dev")) {
                    RingLogger.getConfigPortalLogger().debug("[getChannelSubscriptionTypes] listReturnDTO --> " + new Gson().toJson(listReturnDTO));
                } else {
                    RingLogger.getConfigPortalLogger().debug("[getChannelSubscriptionTypes] listReturnDTO.size --> " + listReturnDTO.getList().size());
                }
            } else {
                RingLogger.getConfigPortalLogger().debug("[getChannelSubscriptionTypes] listReturnDTO --> null");
            }
        } catch (Exception ex) {
            RingLogger.getConfigPortalLogger().debug("Exception in [getChannelSubscriptionTypes]  --> " + ex.getMessage());
        }
        return listReturnDTO;
    }

    public int addChannelSubscriptionType(ChannelSubscriptionDTO subDTO) {
        RingLogger.getConfigPortalLogger().debug("[addChannelSubscriptionType] subDTO --> " + new Gson().toJson(subDTO));
        int reasonCode = -1;
        try {
            reasonCode = transaction.addChannelSubscriptionType(subDTO);
            RingLogger.getConfigPortalLogger().debug("[addChannelSubscriptionType] reasonCode --> " + reasonCode);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [addChannelSubscriptionType] --> " + e);
        }
        return reasonCode;
    }

    public int deleteChannelSubscriptionType(int subTypeId) {
        RingLogger.getConfigPortalLogger().debug("[deleteChannelSubscriptionType] pageId --> " + subTypeId);
        int reasonCode = -1;
        try {
            reasonCode = transaction.deleteChannelSubscriptionType(subTypeId);
            RingLogger.getConfigPortalLogger().debug("[deleteChannelSubscriptionType] reasonCode --> " + reasonCode);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [deleteChannelSubscriptionType] --> " + e);
        }
        return reasonCode;
    }

    public List<UserBasicInfoDTO> getUserBasicInfo(List<Long> userIds, boolean doLog) {
        if (doLog) {
            RingLogger.getConfigPortalLogger().debug("[getUserBasicInfo] userIds.size --> " + userIds.size());
        }
        List<UserBasicInfoDTO> userBasicInfoDTOs = new ArrayList<>();
        try {
            userBasicInfoDTOs = storageTransaction.getUserBasicInfo(userIds);
            if (doLog) {
                RingLogger.getConfigPortalLogger().debug("[getUserBasicInfo] userBasicInfoDTOs.size --> " + userBasicInfoDTOs.size());
            }
        } catch (Exception ex) {
            RingLogger.getConfigPortalLogger().debug("Exception in [getUserBasicInfo]  --> " + ex.getMessage());
        }
        return userBasicInfoDTOs;
    }

    public ListReturnDTO<ChannelDTO> getChannelsByType(int channelType, int categoryId, int start, int limit) {
        RingLogger.getConfigPortalLogger().debug("[getChannelsByType] channelType --> " + channelType + " categoryId --> " + categoryId + " start--> " + start + " limit --> " + limit);
        ListReturnDTO<ChannelDTO> listReturnDTO = new ListReturnDTO<>();
        try {
            listReturnDTO = channelTransaction.getChannelsByType(com.ringid.admin.utils.Constants.LOGGED_IN_USER_ID, channelType, categoryId, start, limit);
            if (listReturnDTO != null && listReturnDTO.getList() != null && listReturnDTO.getList().size() > 0) {
                RingLogger.getConfigPortalLogger().debug("[getChannelsByType] listReturnDTO.getList().size --> " + listReturnDTO.getList().size());
            } else {
                RingLogger.getConfigPortalLogger().debug("[getChannelsByType] listReturnDTO.getList() --> null");
            }
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [getChannelsByType] --> " + e);
        }
        return listReturnDTO;
    }

    public int updatePageWeight(long pageId, int pageType, int weight) {
        RingLogger.getConfigPortalLogger().debug("[updatePageWeight] pageId --> " + pageId + " pageType --> " + pageType + " weight--> " + weight);
        int reasonCode = -1;
        try {
            reasonCode = transaction.updatePageWeight(pageId, pageType, weight);
            RingLogger.getConfigPortalLogger().debug("[updatePageWeight] reasonCode --> " + reasonCode);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [updatePageWeight] --> " + e);
        }
        return reasonCode;
    }

    public List<Pair<Long, Integer>> getPageAndWeights(int pageType, int start, int limit) {
        RingLogger.getConfigPortalLogger().debug("[getPageAndWeights] pageType --> " + pageType + " start --> " + start + " limit--> " + limit);
        List<Pair<Long, Integer>> pageAndWeights = new ArrayList<>();
        try {
            pageAndWeights = transaction.getPageAndWeights(pageType, start, limit);
            if (Constants.SERVER.equals("dev")) {
                RingLogger.getConfigPortalLogger().debug("[getPageAndWeights] pageAndWeights --> " + new Gson().toJson(pageAndWeights));
            } else {
                RingLogger.getConfigPortalLogger().debug("[getPageAndWeights] pageAndWeights.size --> " + pageAndWeights.size());
            }
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [getPageAndWeights] --> " + e);
        }
        return pageAndWeights;
    }

    public ChannelMediaDTO getNowPlayingMedia(long sessionUserId, UUID channelId, UUID webId) {
        RingLogger.getConfigPortalLogger().debug("[getNowPlayingMedia] sessionUserId --> " + sessionUserId + " channelId --> " + channelId + " webId--> " + webId);
        ChannelMediaDTO channelMediaDTO = new ChannelMediaDTO();
        try {
            channelMediaDTO = channelTransaction.getNowPlayingMedia(sessionUserId, channelId, webId);
            RingLogger.getConfigPortalLogger().debug("[getNowPlayingMedia] reasonCode --> " + new Gson().toJson(channelMediaDTO));
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [getNowPlayingMedia] --> " + e);
        }
        return channelMediaDTO;
    }

    public int increaseTVChannelUniqueViewerCount(long userId, UUID channelId, UUID webId) {
        RingLogger.getConfigPortalLogger().debug("[increaseTVChannelUniqueViewerCount] userId --> " + userId + " channelId --> " + channelId + " webId--> " + webId);
        int reasonCode = -1;
        try {
            reasonCode = channelTransaction.increaseTVChannelUniqueViewerCount(userId, channelId, webId);
            RingLogger.getConfigPortalLogger().debug("[increaseTVChannelUniqueViewerCount] reasonCode --> " + reasonCode);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [increaseTVChannelUniqueViewerCount] --> " + e);
        }
        return reasonCode;
    }

    public int createEvent(EventDTO eventDTO) {
        RingLogger.getConfigPortalLogger().debug("[createEvent] eventDTO -->" + new Gson().toJson(eventDTO));
        int reasonCode = -1;
        try {
            reasonCode = transaction.createEvent(eventDTO);
            RingLogger.getConfigPortalLogger().debug("[createEvent] reasonCode --> " + reasonCode);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [createEvent] --> " + e);
        }
        return reasonCode;
    }

    public int updateEvent(EventDTO eventDTO) {
        RingLogger.getConfigPortalLogger().debug("[updateEvent] eventDTO -->" + new Gson().toJson(eventDTO));
        int reasonCode = -1;
        try {
            reasonCode = transaction.updateEvent(eventDTO);
            RingLogger.getConfigPortalLogger().debug("[updateEvent] reasonCode --> " + reasonCode);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [updateEvent] --> " + e);
        }
        return reasonCode;
    }

    public int deleteEvent(long eventId) {
        RingLogger.getConfigPortalLogger().debug("[deleteEvent] eventId -->" + eventId);
        int reasonCode = -1;
        try {
            reasonCode = transaction.deleteEvent(eventId);
            RingLogger.getConfigPortalLogger().debug("[deleteEvent] reasonCode --> " + reasonCode);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [deleteEvent] --> " + e);
        }
        return reasonCode;
    }

    public List<EventDTO> getPastEvents(long curTime, int start, int limit) {
        RingLogger.getConfigPortalLogger().debug("[getPastEvents] curTime --> " + curTime + " start --> " + start + " limit--> " + limit);
        List<EventDTO> eventDTOs = new ArrayList<>();
        try {
            ListReturnDTO<EventDTO> listReturnDTO = storageTransaction.getPastEvents(curTime, start, limit);
            if (listReturnDTO != null && listReturnDTO.getList().size() > 0) {
                eventDTOs.addAll(listReturnDTO.getList());
                RingLogger.getConfigPortalLogger().debug("[getPastEvents] listReturnDTO --> " + new Gson().toJson(listReturnDTO));
            }
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [getPastEvents] --> " + e);
        }
        return eventDTOs;
    }

    public List<EventDTO> getOngoingEvents(long curTime, int start, int limit) {
        RingLogger.getConfigPortalLogger().debug("[getOngoingEvents] curTime --> " + curTime + " start --> " + start + " limit--> " + limit);
        List<EventDTO> eventDTOs = new ArrayList<>();
        try {
            ListReturnDTO<EventDTO> listReturnDTO = storageTransaction.getOngoingEvents(curTime, start, limit);
            if (listReturnDTO != null && listReturnDTO.getList().size() > 0) {
                eventDTOs.addAll(listReturnDTO.getList());
                RingLogger.getConfigPortalLogger().debug("[getOngoingEvents] listReturnDTO --> " + new Gson().toJson(listReturnDTO));
            }
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [getOngoingEvents] --> " + e);
        }
        return eventDTOs;
    }

    public List<EventDTO> getUpcomingEvents(long curTime, int start, int limit) {
        RingLogger.getConfigPortalLogger().debug("[getUpcomingEvents] curTime --> " + curTime + " start --> " + start + " limit--> " + limit);
        List<EventDTO> eventDTOs = new ArrayList<>();
        try {
            ListReturnDTO<EventDTO> listReturnDTO = storageTransaction.getUpcomingEvents(curTime, start, limit);
            if (listReturnDTO != null && listReturnDTO.getList().size() > 0) {
                eventDTOs.addAll(listReturnDTO.getList());
                RingLogger.getConfigPortalLogger().debug("[getUpcomingEvents] listReturnDTO --> " + new Gson().toJson(listReturnDTO));
            }
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [getUpcomingEvents] --> " + e);
        }
        return eventDTOs;
    }

    public int addLiveSubscriptionFee(StreamingTariffDTO dto) {
        int reasonCode = -1;
        RingLogger.getConfigPortalLogger().debug("[addLiveSubscriptionFee] streamingTariffDTO --> " + new Gson().toJson(dto));
        try {
            reasonCode = transaction.addLiveSubscriptionFee(dto);
            RingLogger.getConfigPortalLogger().debug("[addLiveSubscriptionFee] reasonCode --> " + reasonCode);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [addLiveSubscriptionFee] --> " + e);
        }
        return reasonCode;
    }

    public int deleteLiveSubscriptionFee(StreamingTariffDTO dto) {
        int reasonCode = -1;
        RingLogger.getConfigPortalLogger().debug("[deleteLiveSubscriptionFee] streamingTariffDTO --> " + new Gson().toJson(dto));
        try {
            reasonCode = transaction.deleteLiveSubscriptionFee(dto);
            RingLogger.getConfigPortalLogger().debug("[deleteLiveSubscriptionFee] reasonCode --> " + reasonCode);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [deleteLiveSubscriptionFee] --> " + e);
        }
        return reasonCode;
    }

    public List<StreamingTariffDTO> getAllSubscriptionFees() {
        List<StreamingTariffDTO> streamingTariffDTOs = new ArrayList<>();
        try {
            ListReturnDTO<StreamingTariffDTO> listReturnDTO = transaction.getAllSubscriptionFees();
            if (listReturnDTO != null && listReturnDTO.getList().size() > 0) {
                streamingTariffDTOs.addAll(listReturnDTO.getList());
                RingLogger.getConfigPortalLogger().debug("[getAllSubscriptionFees] streamingTariffDTOs --> " + listReturnDTO.getList().size());
            }
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [getAllSubscriptionFees] --> " + e);
        }
        return streamingTariffDTOs;
    }

    public int changeChannelOwnership(UUID channelId, long oldUserId, long newUserId) {
        int reasonCode = -1;
        RingLogger.getConfigPortalLogger().debug("[changeChannelOwnership] channelId --> " + channelId + " oldUserId --> " + oldUserId + " newUserId--> " + newUserId);
        try {
            reasonCode = transaction.changeChannelOwnership(channelId, oldUserId, newUserId);
            RingLogger.getConfigPortalLogger().debug("[changeChannelOwnership] reasonCode --> " + reasonCode);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [changeChannelOwnership] --> " + e);
        }
        return reasonCode;
    }

    public int getCurrentNameUpdateLimit() {
        int value = 0;
        try {
            value = transaction.getCurrentNameUpdateLimit();
            RingLogger.getConfigPortalLogger().debug("[getCurrentNameUpdateLimit] value --> " + value);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [getCurrentNameUpdateLimit] --> " + e);
        }
        return value;
    }

    public int updateNameUpdateLimit(int value) {
        int reasonCode = -1;
        RingLogger.getConfigPortalLogger().debug("[updateNameUpdateLimit] value --> " + value);
        try {
            reasonCode = transaction.updateNameUpdateLimit(value) == true ? 0 : -1;
            RingLogger.getConfigPortalLogger().debug("[updateNameUpdateLimit] reasonCode --> " + reasonCode);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [updateNameUpdateLimit] --> " + e);
        }
        return reasonCode;
    }

    public int addFeedInHighlights(int feedType, UUID feedId, int weight) {
        int reasonCode = -1;
        RingLogger.getConfigPortalLogger().debug("[addFeedInHighlights] feedType --> " + feedType + " feedId --> " + feedId + " weight --> " + weight);
        try {
            reasonCode = transaction.addFeedInHighlights(feedType, feedId, weight);
            RingLogger.getConfigPortalLogger().debug("[addFeedInHighlights] reasonCode --> " + reasonCode);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [addFeedInHighlights] --> " + e);
        }
        return reasonCode;
    }

    public int removeFeedFromHighlights(UUID feedId) {
        int reasonCode = -1;
        RingLogger.getConfigPortalLogger().debug("[removeFeedFromHighlights] feedId --> " + feedId);
        try {
            reasonCode = transaction.removeFeedFromHighlights(feedId);
            RingLogger.getConfigPortalLogger().debug("[removeFeedFromHighlights] reasonCode --> " + reasonCode);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [removeFeedFromHighlights] --> " + e);
        }
        return reasonCode;
    }

    public int updateHighlightedFeedWeight(UUID feedId, int weight) {
        int reasonCode = -1;
        RingLogger.getConfigPortalLogger().debug("[updateHighlightedFeedWeight] feedId --> " + feedId + " weight --> " + weight);
        try {
            reasonCode = transaction.updateHighlightedFeedWeight(feedId, weight);
            RingLogger.getConfigPortalLogger().debug("[updateHighlightedFeedWeight] reasonCode --> " + reasonCode);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [updateHighlightedFeedWeight] --> " + e);
        }
        return reasonCode;
    }

    public List<HighlightedFeedDTO> getHighlightedFeeds(int feedType) {
        PivotedListReturnDTO<HighlightedFeedDTO, UUID> pivotedListReturnDTO;
        UUID pivotId = null;
        List<HighlightedFeedDTO> highlightedFeedDTOs = new ArrayList<>();
        RingLogger.getConfigPortalLogger().debug("[getHighlightedFeeds] pivotId --> " + pivotId + " feedType --> " + feedType + " scroll --> 2" + " limit --> " + LIMIT);
        try {
            do {
                pivotedListReturnDTO = transaction.getHighlightedFeeds(feedType, pivotId, AppConstants.SCROLL_DOWN, LIMIT);
                if (pivotedListReturnDTO != null) {
                    highlightedFeedDTOs.addAll(pivotedListReturnDTO.getList());
                    pivotId = pivotedListReturnDTO.getPivot();
                    RingLogger.getConfigPortalLogger().debug("[getHighlightedFeeds] feedReturnDTO --> " + pivotedListReturnDTO.getList().size());
                } else {
                    RingLogger.getConfigPortalLogger().debug("[getHighlightedFeeds] feedReturnDTO --> NULL");
                    return highlightedFeedDTOs;
                }
            } while (pivotedListReturnDTO.getList().size() == LIMIT && highlightedFeedDTOs.size() < LIMIT_ONE_THOUSAND);

        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [getHighlightedFeeds] --> " + e);
        }
        return highlightedFeedDTOs;
    }

    public FeedReturnDTO getFeedList(long sessionUserId, long wallOwnerId, UUID pageCustomCategoryId, int action, UUID pivotFeedId, Integer highlightedFeedType, int scroll, int limit) {
        FeedReturnDTO feedReturnDTO = null;
        RingLogger.getConfigPortalLogger().debug("[getFeedList] sessionUserId --> " + sessionUserId + " wallOwnerId --> " + wallOwnerId + " pageCustomCategoryId --> " + pageCustomCategoryId + " action --> " + action + " pivotFeedId --> " + pivotFeedId + " highlightedFeedType " + highlightedFeedType + " scroll --> " + scroll + " limit --> " + limit);
        try {
            feedReturnDTO = storageTransaction.getFeedList(sessionUserId, wallOwnerId, pageCustomCategoryId, action, pivotFeedId, highlightedFeedType, scroll, limit);
            if (feedReturnDTO != null) {
                RingLogger.getConfigPortalLogger().debug("[getFeedList] feedReturnDTO --> " + feedReturnDTO.getFeedDTOList().size());
            } else {
                RingLogger.getConfigPortalLogger().debug("[getFeedList] feedReturnDTO --> NULL");
            }
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [getFeedList] --> " + e);
        }
        return feedReturnDTO;
    }

    public int enableOrDisableAutoFollow(long pageId, boolean isEnable) {
        int reasonCode = -1;
        RingLogger.getConfigPortalLogger().debug("[enableOrDisableAutoFollow] pageId --> " + pageId + " isEnable --> " + isEnable);
        try {
            reasonCode = transaction.enableOrDisableAutoFollow(pageId, isEnable);
            RingLogger.getConfigPortalLogger().debug("[enableOrDisableAutoFollow] reasonCode --> " + reasonCode);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [enableOrDisableAutoFollow] --> " + e);
        }
        return reasonCode;
    }

    public int updateChannelServerIpAndPort(UUID channelId, String oldServerIp, int oldPort, String newServerIp, int newPort) {
        int reasonCode = -1;
        RingLogger.getConfigPortalLogger().debug("[updateChannelServerIpAndPort] channelId --> " + channelId + " oldServerIp --> " + oldServerIp + " oldPort --> " + oldPort + " newServerIp --> " + newServerIp + " newPort --> " + newPort);
        try {
            reasonCode = transaction.updateChannelServerIpAndPort(channelId, oldServerIp, oldPort, newServerIp, newPort);
            RingLogger.getConfigPortalLogger().debug("[updateChannelServerIpAndPort] reasonCode --> " + reasonCode);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [updateChannelServerIpAndPort] --> " + e);
        }
        return reasonCode;
    }

    public int updateStreamingServerIpAndPort(UUID channelId, String currentStreamServerIp, int currentPort, String newStreamingServerIp, int newPort) {
        int reasonCode = -1;
        RingLogger.getConfigPortalLogger().debug("[updateStreamingServerIpAndPort] channelId --> " + channelId + " currentStreamServerIp --> " + currentStreamServerIp + " currentPort --> " + currentPort + " newStreamingServerIp --> " + newStreamingServerIp + " newPort --> " + newPort);
        try {
            reasonCode = transaction.updateStreamingServerIpAndPort(channelId, currentStreamServerIp, currentPort, newStreamingServerIp, newPort);
            RingLogger.getConfigPortalLogger().debug("[updateStreamingServerIpAndPort] reasonCode --> " + reasonCode);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [updateStreamingServerIpAndPort] --> " + e);
        }
        return reasonCode;
    }

    public List<Long> getUnverifiedLiveUsers(long pivotUserId, int limit) {
        List<Long> unverifiedLiveUsers = new ArrayList<>();
        RingLogger.getConfigPortalLogger().debug("[getUnverifiedLiveUsers] pivotUserId --> " + pivotUserId + " limit --> " + limit);
        try {
            unverifiedLiveUsers = transaction.getUnverifiedLiveUsers(pivotUserId, limit);
            RingLogger.getConfigPortalLogger().debug("[getUnverifiedLiveUsers] unverifiedLiveUsers :: " + unverifiedLiveUsers != null ? unverifiedLiveUsers.size() : "NULL");
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().debug("Exception in [getUnverifiedLiveUsers] --> " + e);
        }
        return unverifiedLiveUsers;
    }

    public int updateChannelBooster(UUID channelId, long channelBoster) {
        int reasonCode = -1;
        RingLogger.getConfigPortalLogger().debug("[updateChannelBooster] channelId --> " + channelId + " channelBoster --> " + channelBoster);
        try {
            reasonCode = transaction.updateChannelBooster(channelId, channelBoster);
            RingLogger.getConfigPortalLogger().debug("[updateChannelBooster] reasonCode --> " + reasonCode);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [updateChannelBooster] --> " + e);
        }
        return reasonCode;
    }

    public int addChannelCategory(ChannelCategoryDTO categoryDTO) {
        int reasonCode = -1;
        RingLogger.getConfigPortalLogger().debug("[addChannelCategory] channelCategoryDTO --> " + new Gson().toJson(categoryDTO));
        try {
            reasonCode = transaction.addChannelCategory(categoryDTO);
            RingLogger.getConfigPortalLogger().debug("[addChannelCategory] reasonCode --> " + reasonCode);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [addChannelCategory] --> " + e);
        }
        return reasonCode;
    }

    public int updateChannelCategory(ChannelCategoryDTO categoryDTO) {
        int reasonCode = -1;
        RingLogger.getConfigPortalLogger().debug("[updateChannelCategory] channelCategoryDTO --> " + new Gson().toJson(categoryDTO));
        try {
            reasonCode = transaction.updateChannelCategory(categoryDTO);
            RingLogger.getConfigPortalLogger().debug("[updateChannelCategory] reasonCode --> " + reasonCode);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [updateChannelCategory] --> " + e);
        }
        return reasonCode;
    }

    public int deleteChannelCategory(int categoryId) {
        int reasonCode = -1;
        RingLogger.getConfigPortalLogger().debug("[deleteChannelCategory] channelCategoryId --> " + categoryId);
        try {
            reasonCode = transaction.deleteChannelCategory(categoryId);
            RingLogger.getConfigPortalLogger().debug("[deleteChannelCategory] reasonCode --> " + reasonCode);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [deleteChannelCategory] --> " + e);
        }
        return reasonCode;
    }

    public List<ChannelCategoryDTO> getChannelCategoryList() {
        ListReturnDTO<ChannelCategoryDTO> channelCategoryDTOs = null;
        try {
            channelCategoryDTOs = transaction.getChannelCategoryList();
            RingLogger.getConfigPortalLogger().debug("[getChannelCategoryList] channelCategoryDTOs :: " + channelCategoryDTOs != null ? channelCategoryDTOs.getList().size() : "NULL");
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [getChannelCategoryList] --> " + e);
        }
        if (channelCategoryDTOs != null) {
            return channelCategoryDTOs.getList();
        }
        return new ArrayList<>();
    }

    public int deactivatePage(UserDeactivationDTO dto) {
        int reasonCode = -1;
        RingLogger.getConfigPortalLogger().debug("[deactivatePage] userDeactivationDTO --> " + new Gson().toJson(dto));
        try {
            reasonCode = storageTransaction.deactivatePage(dto);
            RingLogger.getConfigPortalLogger().debug("[deactivatePage] reasonCode --> " + reasonCode);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [deactivatePage] --> " + e);
        }
        return reasonCode;
    }

    public int activatePage(long userId) {
        int reasonCode = -1;
        RingLogger.getConfigPortalLogger().debug("[activatePage] userId --> " + userId);
        try {
            reasonCode = storageTransaction.activatePage(userId);
            RingLogger.getConfigPortalLogger().debug("[activatePage] reasonCode --> " + reasonCode);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [activatePage] --> " + e);
        }
        return reasonCode;
    }

    public int updateDonationRoomPageId(long pageId) {
        int reasonCode = -1;
        RingLogger.getConfigPortalLogger().debug("[updateDonationRoomPageId] pageId --> " + pageId);
        try {
            reasonCode = transaction.updateDonationRoomPageId(pageId);
            RingLogger.getConfigPortalLogger().debug("[updateDonationRoomPageId] reasonCode --> " + reasonCode);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [updateDonationRoomPageId] --> " + e);
        }
        return reasonCode;
    }

    public int updateTopLiveRoomPage(long pageId) {
        int reasonCode = -1;
        RingLogger.getConfigPortalLogger().debug("[updateTopLiveRoomPage] pageId --> " + pageId);
        try {
            reasonCode = transaction.updateTopLiveRoomPage(pageId);
            RingLogger.getConfigPortalLogger().debug("[updateTopLiveRoomPage] reasonCode --> " + reasonCode);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [updateTopLiveRoomPage] --> " + e);
        }
        return reasonCode;
    }

    public int createDonationPage(PageDTO pageDTO) {
        int reasonCode = -1;
        RingLogger.getConfigPortalLogger().debug("[createDonationPage] pageDTO --> " + new Gson().toJson(pageDTO));
        try {
            reasonCode = transaction.createDonationPage(pageDTO, AppConstants.APP_TYPES.FULL);
            RingLogger.getConfigPortalLogger().debug("[createDonationPage] reasonCode --> " + reasonCode);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [createDonationPage] --> " + e);
        }
        return reasonCode;
    }

    public int updateDonationPage(PageDTO pageDTO, long pageId) {
        int reasonCode = -1;
        ReturnDTO returnDTO;
        RingLogger.getConfigPortalLogger().debug("[updateDonationPage] pageDTO --> " + new Gson().toJson(pageDTO) + " pageId --> " + pageId);
        try {
            returnDTO = transaction.updateDonationPage(pageDTO, pageId);
            reasonCode = returnDTO.getReasonCode();
            RingLogger.getConfigPortalLogger().debug("[updateDonationPage] reasonCode --> " + reasonCode);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [updateDonationPage] --> " + e);
        }
        return reasonCode;
    }

    public int deactivateDonationPage(long pageId) {
        int reasonCode = -1;
        RingLogger.getConfigPortalLogger().debug("[deactivateDonationPage] pageId --> " + pageId);
        try {
            reasonCode = transaction.deactivateDonationPage(pageId);
            RingLogger.getConfigPortalLogger().debug("[deactivateDonationPage] reasonCode --> " + reasonCode);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [deactivateDonationPage] --> " + e);
        }
        return reasonCode;
    }

    public List<Long> getDonationPages() {
        List<Long> pageIds = null;
        try {
            pageIds = transaction.getDonationPages();
            RingLogger.getConfigPortalLogger().debug("[getDonationPages] pageDTOs :: " + pageIds != null ? pageIds.size() : "NULL");
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [getDonationPages] --> " + e);
        }
        return pageIds;
    }

    public int updateLiveScheduleSerial(long userId, int userType, long liveTime, int scheduleSerial) {
        int reasonCode = -1;
        RingLogger.getConfigPortalLogger().debug("[updateLiveScheduleSerial] userId --> " + userId + " userType --> " + userType + " liveTime --> " + liveTime + " scheduleSerial --> " + scheduleSerial);
        try {
            reasonCode = transaction.updateLiveScheduleSerial(userId, userType, liveTime, scheduleSerial);
            RingLogger.getConfigPortalLogger().debug("[updateLiveScheduleSerial] reasonCode --> " + reasonCode);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [updateLiveScheduleSerial] --> " + e);
        }
        return reasonCode;
    }

    public boolean updateToggleSettings(int settingsName, int enable, long userId) {
        boolean reasonCode = false;
        RingLogger.getConfigPortalLogger().debug("[updateToggleSettings] settingsName --> " + settingsName + " enable --> " + enable + " userId --> " + userId);
        try {
            reasonCode = storageTransaction.updateToggleSettings(settingsName, enable, userId, AppConstants.APP_TYPES.FULL);
            RingLogger.getConfigPortalLogger().debug("[updateToggleSettings] reasonCode --> " + reasonCode);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [updateToggleSettings] --> " + e);
        }
        return reasonCode;
    }

    public int setCelebrityMinGiftValue(long celebrityPageId, int giftValue) {
        int reasonCode = -1;
        RingLogger.getConfigPortalLogger().debug("[setCelebrityMinGiftValue] celebrityPageId --> " + celebrityPageId + " giftValue --> " + giftValue);
        try {
            reasonCode = transaction.setCelebrityMinGiftValue(celebrityPageId, giftValue);
            RingLogger.getConfigPortalLogger().debug("[setCelebrityMinGiftValue] reasonCode --> " + reasonCode);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [setCelebrityMinGiftValue] --> " + e);
        }
        return reasonCode;
    }

    public Integer getCelebrityMinGiftValue(long celebrityPageId) {
        Integer celebrityMinGiftValue = null;
        RingLogger.getConfigPortalLogger().debug("[getCelebrityMinGiftValue] celebrityPageId --> " + celebrityPageId);
        try {
            celebrityMinGiftValue = transaction.getCelebrityMinGiftValue(celebrityPageId);
            RingLogger.getConfigPortalLogger().debug("[getCelebrityMinGiftValue] celebrityMinGiftValue --> " + celebrityMinGiftValue);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [getCelebrityMinGiftValue] --> " + e);
        }
        return celebrityMinGiftValue;
    }

    public List<DeactiveReasonDTO> getDeactivateReasons() {
        List<DeactiveReasonDTO> deactiveReasonDTOs;
        try {
            deactiveReasonDTOs = transaction.getDeactivateReasons();
            RingLogger.getConfigPortalLogger().debug("[getDeactivateReasons] deactiveReasonDTOs :: " + deactiveReasonDTOs != null ? deactiveReasonDTOs.size() : "NULL");
        } catch (Exception e) {
            deactiveReasonDTOs = new ArrayList<>();
            RingLogger.getConfigPortalLogger().error("Exception in [getDeactivateReasons] --> " + e);
        }
        return deactiveReasonDTOs;
    }

    public int addDeactivateReasons(int id, String description) {
        int reasonCode = -1;
        RingLogger.getConfigPortalLogger().debug("[addDeactivateReasons] id --> " + id + " description --> " + description);
        try {
            reasonCode = transaction.addDeactivateReasons(id, description);
            RingLogger.getConfigPortalLogger().debug("[addDeactivateReasons] reasonCode --> " + reasonCode);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [addDeactivateReasons] --> " + e);
        }
        return reasonCode;
    }

    public int deleteLiveCallTariff(StreamingTariffDTO dto) {
        int reasonCode = -1;
        RingLogger.getConfigPortalLogger().debug("[deleteLiveCallTariff] dto --> " + new Gson().toJson(dto));
        try {
            reasonCode = transaction.deleteLiveCallTariff(dto);
            RingLogger.getConfigPortalLogger().debug("[deleteLiveCallTariff] reasonCode --> " + reasonCode);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [deleteLiveCallTariff] --> " + e);
        }
        return reasonCode;
    }

    public List<StreamingTariffDTO> getAllTariff() {
        ListReturnDTO<StreamingTariffDTO> tariffList = new ListReturnDTO<>();
        try {
            tariffList = transaction.getAllTariff();
            RingLogger.getConfigPortalLogger().debug("[getAllTariff] tariffList :: " + tariffList != null ? tariffList.getList().size() : "NULL");
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [getAllTariff] --> " + e);
        }
        return tariffList.getList();
    }

    public ArrayList<UserDeviceDTO> getDeviceInfo(long userId) {
        ArrayList<UserDeviceDTO> userDeviceList = new ArrayList<>();
        RingLogger.getConfigPortalLogger().debug("[getDeviceInfo] userId --> " + userId);
        try {
            userDeviceList = storageTransaction.getDeviceInfo(userId);
            RingLogger.getConfigPortalLogger().debug("[getDeviceInfo] userDeviceList :: " + (userDeviceList != null ? userDeviceList.size() : "NULL"));
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [getDeviceInfo] --> " + e);
        }
        return userDeviceList;
    }

    public int changeRingId(long oldRingId, long newRingId) {
        int reasonCode = -1;
        RingLogger.getConfigPortalLogger().debug("[changeRingId] oldRingId --> " + oldRingId + " newRingId --> " + newRingId);
        try {
            boolean response = transaction.changeRingId(oldRingId, newRingId);
            RingLogger.getConfigPortalLogger().debug("[changeRingId] response --> " + response);
            reasonCode = response ? 0 : -1;
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [changeRingId] --> " + e);
        }
        return reasonCode;
    }

    public int updateRingId(long ringId, long userId) {
        int reasonCode = -1;
        RingLogger.getConfigPortalLogger().debug("[updateRingId] ringId --> " + ringId + " userId --> " + userId);
        try {
            boolean response = transaction.updateRingId(ringId, userId);
            RingLogger.getConfigPortalLogger().debug("[updateRingId] response --> " + response);
            reasonCode = response ? 0 : -1;
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [updateRingId] --> " + e);
        }
        return reasonCode;
    }

    public ObjectReturnDTO getLatestOTP(long userId) {
        ObjectReturnDTO returnDTO = null;
        RingLogger.getConfigPortalLogger().debug("[getLatestOTP] userId --> " + userId);
        try {
            returnDTO = transaction.getLatestOTP(userId);
            RingLogger.getConfigPortalLogger().debug("[getLatestOTP] returnDTO --> " + new Gson().toJson(returnDTO));
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [getLatestOTP] --> " + e);
        }
        return returnDTO;
    }

    public int updatePageFollowRules(long pageId, int pageType, FollowRuleDTO dto) {
        int reasonCode = -1;
        RingLogger.getConfigPortalLogger().debug("[updatePageFollowRules] pageId --> " + pageId + " pageType --> " + pageType + " FollowRuleDTO --> " + new Gson().toJson(dto));
        try {
            reasonCode = transaction.updatePageFollowRules(pageId, pageType, dto);
            RingLogger.getConfigPortalLogger().debug("[updatePageFollowRules] reasonCode --> " + reasonCode);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [updatePageFollowRules] --> " + e);
        }
        return reasonCode;
    }

    public FollowRuleDTO getPageFollowRuleByPageId(long pageId) {
        FollowRuleDTO followRuleDTO = new FollowRuleDTO();
        RingLogger.getConfigPortalLogger().debug("[getPageFollowRuleByPageId] pageId --> " + pageId);
        try {
            followRuleDTO = transaction.getPageFollowRules(pageId);
            RingLogger.getConfigPortalLogger().debug("[getPageFollowRuleByPageId] followRuleDTO --> " + new Gson().toJson(followRuleDTO));
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [getPageFollowRuleByPageId] --> " + e);
        }
        return followRuleDTO;
    }

    public long getDefaultRestrictedTime() {
        long defaultRestrictionTime = 0;
        try {
            defaultRestrictionTime = transaction.getDefaultRestrictedTime();
            RingLogger.getConfigPortalLogger().debug("[getDefaultRestrictedTime] time --> " + defaultRestrictionTime);
        } catch (Exception e) {
        }
        return defaultRestrictionTime;
    }

    public int updateRestrictionTime(long time) {
        int reasoneCode = -1;
        boolean response;
        RingLogger.getConfigPortalLogger().debug("[updateResponseTime] time --> " + time);
        try {
            response = transaction.updateRestrictionTime(time);
            RingLogger.getConfigPortalLogger().debug("[updateResponseTime] response --> " + response);
            reasoneCode = response ? 0 : -1;
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [updateResponseTime] --> " + e);
        }
        return reasoneCode;
    }

    public int addCelebrityLiveSchedule(long userId, int userType, long donationPageId, long liveTime, UUID liveTimeUUID, int scheduleSerial) {
        int reasonCode = -1;
        RingLogger.getConfigPortalLogger().debug("[addCelebrityLiveSchedule] userId --> " + userId + " userType --> " + userType + " donationPageId --> " + donationPageId + " liveTime --> " + liveTime + " liveTimeUUID --> " + liveTimeUUID + " scheduleSerial --> " + scheduleSerial);
        try {
            reasonCode = storageTransaction.addCelebrityLiveSchedule(userId, userType, donationPageId, liveTime, liveTimeUUID, scheduleSerial);
            RingLogger.getConfigPortalLogger().debug("[addCelebrityLiveSchedule] reasonCode --> " + reasonCode);

        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [addCelebrityLiveSchedule] --> " + e);
        }
        return reasonCode;
    }

    public int editCelebrityLiveSchedule(long userId, int userType, long curLiveTime, long newLiveTime, UUID newLiveTimeUUID, long donationPageId) {
        int reasonCode = -1;
        RingLogger.getConfigPortalLogger().debug("[editCelebrityLiveSchedule] userId --> " + userId + " curLiveTime --> " + curLiveTime + " newLiveTime --> " + newLiveTime + " newLiveTimeUUID --> " + newLiveTimeUUID + " donationPageId --> " + donationPageId);
        try {
            reasonCode = storageTransaction.editLiveSchedule(userId, userType, curLiveTime, newLiveTime, newLiveTimeUUID, donationPageId);
            RingLogger.getConfigPortalLogger().debug("[editCelebrityLiveSchedule] reasonCode --> " + reasonCode);

        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [editCelebrityLiveSchedule] --> " + e);
        }
        return reasonCode;
    }

    public int deleteCelebrityLiveSchedule(long userId, int userType, long liveTime, boolean feedDelete) {
        int reasonCode = -1;
        ObjectReturnDTO<UUID> objectReturnDTO = new ObjectReturnDTO<>();
        RingLogger.getConfigPortalLogger().debug("[deleteCelebrityLiveSchedule] userId --> " + userId + " userType --> " + userType + " liveTime --> " + liveTime + " feedDelete --> " + feedDelete);
        try {
            objectReturnDTO = storageTransaction.deleteLiveSchedule(userId, userType, liveTime, feedDelete);
            RingLogger.getConfigPortalLogger().debug("[deleteCelebrityLiveSchedule] objectReturnDTO --> " + new Gson().toJson(objectReturnDTO));
            reasonCode = objectReturnDTO.getReasonCode();
            RingLogger.getConfigPortalLogger().debug("[deleteCelebrityLiveSchedule] reasonCode --> " + reasonCode);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [deleteCelebrityLiveSchedule] --> " + e);
        }
        return reasonCode;
    }

    public List<StreamingDTO> getAllLiveSchedule() {
        List<StreamingDTO> streamingDTOs = new ArrayList<>();
        PivotedListReturnDTO<StreamingDTO, UUID> listReturnDTO = null;
        UUID pivot = null;
        try {
            do {
                listReturnDTO = storageTransaction.getAllLiveSchedule(com.ringid.admin.utils.Constants.LOGGED_IN_USER_ID, AppConstants.UserTypeConstants.CELEBRITY, pivot, AppConstants.SCROLL_DOWN, LIMIT_ONE_THOUSAND);
                pivot = listReturnDTO.getPivot();
                streamingDTOs.addAll(listReturnDTO.getList());
            } while (listReturnDTO.getList().size() == LIMIT_ONE_THOUSAND);
            RingLogger.getConfigPortalLogger().debug("[getAllLiveSchedule] streamingDTOs --> " + streamingDTOs.size());

        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [getAllLiveSchedule] --> " + e);
        }
        return streamingDTOs;
    }

    public int addProfileOrCoverImage(CassImageDTO cassImageDTO) {
        int reasonCode = -1;
        RingLogger.getConfigPortalLogger().debug("[addProfileOrCoverImage] cassImageDTO --> " + cassImageDTO);
        try {
            reasonCode = storageTransaction.addProfileOrCoverImage(cassImageDTO);
            RingLogger.getConfigPortalLogger().debug("[addProfileOrCoverImage] reasonCode --> " + reasonCode);
            RingLogger.getConfigPortalLogger().debug("[addProfileOrCoverImage] cassImageDTO --> " + cassImageDTO);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [addProfileOrCoverImage] --> " + e);
        }
        return reasonCode;
    }

    public PageDTO getPageInfo(long pageId) {
        PageDTO pageDTO = new PageDTO();
        RingLogger.getConfigPortalLogger().debug("[getPageInfo] pageId --> " + pageId);
        try {
            pageDTO = storageTransaction.getPageInfo(pageId, com.ringid.admin.utils.Constants.LOGGED_IN_USER_ID);
            RingLogger.getConfigPortalLogger().debug("[getPageInfo] pageDTO --> " + new Gson().toJson(pageDTO));
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [getPageInfo] --> " + e);
        }
        return pageDTO;
    }

    public PageDTO getUserPageInfo(long pageId) {
        PageDTO pageDTO = new PageDTO();
        RingLogger.getConfigPortalLogger().debug("[getUserPageInfo] pageId --> " + pageId);
        try {
            pageDTO = storageTransaction.getUserPageInfo(pageId, com.ringid.admin.utils.Constants.LOGGED_IN_USER_ID);
            RingLogger.getConfigPortalLogger().debug("[getUserPageInfo] pageDTO --> " + new Gson().toJson(pageDTO));
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [getUserPageInfo] --> " + e);
        }
        return pageDTO;
    }

    public long getPageOwnerId(long pageId) {
        long pageOwnerId = 0;
//        RingLogger.getConfigPortalLogger().debug("[getPageOwnerId] pageId --> " + pageId);
        try {
            pageOwnerId = storageTransaction.getPageOwnerId(pageId);
//            RingLogger.getConfigPortalLogger().debug("[getPageOwnerId] pageOwnerId --> " + pageOwnerId);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [getPageOwnerId] --> " + e);
        }
        return pageOwnerId;
    }

    public int updateUserPage(PageDTO pageDTO, long pageId) {
        int reasonCode = -1;
        RingLogger.getConfigPortalLogger().debug("[updateUserPage] pageId -->" + pageId + " pageDTO --> " + new Gson().toJson(pageDTO));
        ReturnDTO returnDTO;
        try {
            long pageOwnerId = CassandraDAO.getInstance().getPageOwnerId(pageId);
            returnDTO = storageTransaction.updateUserPage(pageDTO, pageId, pageOwnerId);
            RingLogger.getConfigPortalLogger().debug("[updateUserPage] pageFeedBackDTO --> " + new Gson().toJson(returnDTO));
            if (returnDTO.getReasonCode() == AppConstants.NONE) {
                reasonCode = 0;
            }
            RingLogger.getConfigPortalLogger().debug("[updateUserPage] reasonCode --> " + reasonCode);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [updateUserPage] --> " + e);
        }
        return reasonCode;
    }

    public int updatePage(PageDTO pageDTO, long pageId) {
        int reasonCode = -1;
        RingLogger.getConfigPortalLogger().debug("[updatePage] pageId -->" + pageId + " pageDTO --> " + new Gson().toJson(pageDTO));
        try {
            ReturnDTO returnDTO = storageTransaction.updatePage(pageDTO, pageId, pageDTO.getOwnerId());
            RingLogger.getConfigPortalLogger().debug("[updatePage] pageFeedBackDTO --> " + new Gson().toJson(returnDTO));
            if (returnDTO.getReasonCode() == AppConstants.NONE) {
                reasonCode = 0;
            }
            RingLogger.getConfigPortalLogger().debug("[updatePage] reasonCode --> " + reasonCode);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [updatePage] --> " + e);
        }
        return reasonCode;
    }

    public int createUserPage(PageDTO pageDTO) {
        int reasonCode = -1;
        RingLogger.getConfigPortalLogger().debug("[createUserPage] pageDTO --> " + new Gson().toJson(pageDTO));
        try {
            reasonCode = storageTransaction.createUserPage(pageDTO, AppConstants.APP_TYPES.FULL);
            RingLogger.getConfigPortalLogger().debug("[createUserPage] reasonCode --> " + reasonCode);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [createUserPage] --> " + e);
        }
        return reasonCode;
    }

    public int createPage(PageDTO pageDTO) {
        int reasonCode = -1;
        RingLogger.getConfigPortalLogger().debug("[createPage] pageDTO --> " + new Gson().toJson(pageDTO));
        try {
            reasonCode = storageTransaction.createPage(pageDTO);
            RingLogger.getConfigPortalLogger().debug("[createPage] reasonCode --> " + reasonCode);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [createPage] --> " + e);
        }
        return reasonCode;
    }

    public List<StreamingDTO> getVerifiedStreams() {
        List<StreamingDTO> streamList = new ArrayList<>();
        Integer start;
        PivotedListReturnDTO<StreamingDTO, Integer> listReturnDTO;
        try {
            List<StreamRoomDTO> streamRoomList = getLiveRoomDetails();
            for (StreamRoomDTO dto : streamRoomList) {
                start = 0;
                do {

                    RingLogger.getConfigPortalLogger().debug("[getVerifiedStreams] roomId --> " + dto.getRoomId() + " roomName --> " + dto.getRoomName() + " start --> " + start + " limit --> " + LIMIT);

                    if (dto.getRoomId() != com.ringid.admin.utils.Constants.TOP_STREAMERS_ROOM_ID && dto.getRoomId() != com.ringid.admin.utils.Constants.CHARITY_ROOM_ID) {
                        listReturnDTO = transaction.getVerifiedStreams(dto.getRoomId(), start, LIMIT);

                        streamList.addAll(listReturnDTO.getList());
                        start += LIMIT;
                        RingLogger.getConfigPortalLogger().debug("[getVerifiedStreams] listReturnDTO.getList.size() --> " + listReturnDTO.getList().size());
                    }
                } while (streamList.size() == LIMIT);
            }
        } catch (Exception e) {

        }
        return streamList;
    }

    public List<StreamingDTO> getUnverifiedStreams() {
        List<StreamingDTO> streamList = new ArrayList<>();
        UUID pivotStreamId = null;
        PivotedListReturnDTO<StreamingDTO, UUID> listReturnDTO;
        try {
            List<StreamRoomDTO> streamRoomList = getLiveRoomDetails();
            for (StreamRoomDTO dto : streamRoomList) {
                do {
                    RingLogger.getConfigPortalLogger().debug("[getUnverifiedStreams] roomId --> " + dto.getRoomId() + " roomName --> " + dto.getRoomName() + " pivotStreamId --> " + pivotStreamId + " limit --> " + LIMIT);
                    if (dto.getRoomId() != com.ringid.admin.utils.Constants.TOP_STREAMERS_ROOM_ID && dto.getRoomId() != com.ringid.admin.utils.Constants.CHARITY_ROOM_ID) {
                        listReturnDTO = transaction.getUnverifiedStreams(dto.getRoomId(), pivotStreamId, Constants.SCROLL_DOWN, LIMIT);
                        streamList.addAll(listReturnDTO.getList());
                        pivotStreamId = listReturnDTO.getPivot();
                        RingLogger.getConfigPortalLogger().debug("[getUnverifiedStreams] listReturnDTO.getList.size() --> " + listReturnDTO.getList().size());
                    }
                } while (streamList.size() == LIMIT);
            }

        } catch (Exception e) {

        }
        return streamList;
    }

    public int updateLiveVerificationState(long userID, boolean state) {
        int reasonCode = -1;
        RingLogger.getConfigPortalLogger().debug("[updateLiveVerificationState] userId: " + userID + " state: " + state);
        try {
            reasonCode = transaction.updateLiveVerificationState(userID, state);
            RingLogger.getConfigPortalLogger().debug("[updateLiveVerificationState] reasonCode --> " + reasonCode);
        } catch (Exception e) {

        }
        return reasonCode;
    }

    public int makeDiscoverable(long pageId) {
        RingLogger.getConfigPortalLogger().debug("makeDiscoverable : pageId --> " + pageId + ", isDiscoverable --> true");
        return transaction.updateDiscoverableState(pageId, true);
    }

    public int removeFromDiscoverable(long pageId) {
        RingLogger.getConfigPortalLogger().debug("removeFromDiscoverable : pageId --> " + pageId + ", isDiscoverable --> false");
        return transaction.updateDiscoverableState(pageId, false);
    }

    public List<CassMultiMediaDTO> getRecentMediaList() {
        List<CassMultiMediaDTO> mediaList;
        try {
            mediaList = transaction.getUserRecentMedias();
            RingLogger.getConfigPortalLogger().info("## getRecentMediaList() --> " + (mediaList != null ? mediaList.size() : "temp is NULL"));
            if (mediaList != null && mediaList.size() > 0) {
                for (CassMultiMediaDTO multimedia : mediaList) {
                    UserBasicInfoDTO userBasicInfo = getUserBasicInfo(multimedia.getUserId());
                    if (userBasicInfo != null) {
                        multimedia.setFullName(userBasicInfo.getRingId() + "<br/>(" + userBasicInfo.getFirstName() + ")");
                    }
                }
            }
        } catch (Throwable e) {
            RingLogger.getConfigPortalLogger().error("Throwable [getRecentMediaList] --> ", e);
            mediaList = new ArrayList<>();
        }
        return mediaList;
    }

    public List<CassMultiMediaDTO> getTrendingMediaList() {
        List<CassMultiMediaDTO> trendingMediaList = new ArrayList<>();
        try {
            String pagingState = null;
            List<CassMultiMediaDTO> tempList;
            do {
                tempList = transaction.getTrendingMultiMedia(com.ringid.admin.utils.Constants.LOGGED_IN_USER_ID, pagingState, TRENDING_LIMIT);
                trendingMediaList.addAll(tempList);
            } while (tempList.size() == 100);
            if (trendingMediaList.size() > 0) {
                trendingMediaList
                        .forEach(multimedia -> {
                            UserBasicInfoDTO userBasicInfo = getUserBasicInfo(multimedia.getUserId());
                            if (userBasicInfo != null) {
                                multimedia.setFullName(userBasicInfo.getRingId() + "<br/>(" + userBasicInfo.getFirstName() + ")");
                            }
                        });
            }
        } catch (Throwable e) {
            RingLogger.getConfigPortalLogger().error("Throwable [getTrendingMediaList] --> " + e);
        }
        return trendingMediaList;
    }

    public boolean makeTrendingMedia(ArrayList<UUID> mediaIds) {
        return transaction.makeTrendingMedia(mediaIds);
    }

    public boolean removeTrendingMedia(List<CassMultiMediaDTO> mediaDTos) {
        return transaction.removeTrendingMedia(mediaDTos);
    }

    public List<Long> getPageIds() {
        List<Long> pageIds = new ArrayList<>();
        try {
            pageIds = transaction.getPageIds();
            RingLogger.getConfigPortalLogger().debug("[getPageIds] --> pageIds : " + pageIds.size());
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [getPageIds] -->  : " + e);
        }
        return pageIds;
    }

    public List<NPBasicFeedInfoDTO> getBreakingNewsListFromPages(List<Long> pageIds) {
        List<NPBasicFeedInfoDTO> breakingNewsFromPages = new ArrayList<>();
        try {
            pageIds
                    .forEach(pageId -> {
                        List<NPBasicFeedInfoDTO> temp = transaction.getBreakingNewsPortalFeeds(pageId, com.ringid.admin.utils.Constants.LOGGED_IN_USER_ID, COMMON_BREAKING_LIST_LIMIT);
                        breakingNewsFromPages.addAll(temp);
                    });
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in getBreakingNewsListFromPages: " + e);
        }
        RingLogger.getConfigPortalLogger().debug("breakingNewsFromPages size: " + breakingNewsFromPages.size());
        return breakingNewsFromPages;
    }

    public List<NPBasicFeedInfoDTO> getCommonBreakingNewsList() {
        UUID pivotID = null;
        List<NPBasicFeedInfoDTO> commonBreakingNews = new ArrayList<>();
        //transaction.getBreakingFeedCount(); ??
        long commonBreakingNewsCount = transaction.getBreakingFeedCount(AppConstants.GLOBAL_COUNTRY_CODE);
        RingLogger.getConfigPortalLogger().debug("Global Breaking News Count: " + commonBreakingNewsCount);
        while (commonBreakingNews.size() < commonBreakingNewsCount) {
            commonBreakingNews.addAll(transaction.getGlobalBreakingNewsPortalFeeds(AppConstants.GLOBAL_COUNTRY_CODE,
                    pivotID,
                    SCROLL_DOWN,
                    COMMON_BREAKING_LIST_LIMIT));
        }
        return commonBreakingNews;
    }

    public int makeCommonBreaking(Map<UUID, Integer> feedIdDuration) {
        return transaction.makeFeedBreakingNews(AppConstants.GLOBAL_COUNTRY_CODE, feedIdDuration);
    }

    public int removeFromCommonBreaking(List<UUID> feedId) {
        return transaction.removeFromBreakingNews(AppConstants.GLOBAL_COUNTRY_CODE, feedId);
    }

    public void destroy() {
        try {
            liveStorageTransaction.close();
            System.out.println("## LiveStorageTransaction closed");
        } catch (Exception e) {
            System.err.println("destroy LiveStorageTransaction [Exception] --> " + e);
        } catch (Throwable e) {
            System.err.println("destroy LiveStorageTransaction [Throwable] --> " + e);
        }
        try {
            channelTransaction.close();
            System.out.println("## ChannelTransaction closed");
        } catch (Exception e) {
            System.err.println("destroy ChannelTransaction [Exception] --> " + e);
        } catch (Throwable e) {
            System.err.println("destroy ChannelTransaction [Throwable] --> " + e);
        }
        try {
            transaction.close();
            System.out.println("## transaction closed");
        } catch (Exception e) {
            System.err.println("destroy AdminTransaction [Exception] --> " + e);
        } catch (Throwable e) {
            System.err.println("destroy AdminTransaction [Throwable] --> " + e);
        }
        try {
            LiveStreamSearch.getInstance().close();
            System.out.println("## LiveStreamSearch closed");
        } catch (Exception e) {
            System.err.println("destroy LiveStreamSearch [Exception] --> " + e);
        } catch (Throwable e) {
            System.err.println("destroy LiveStreamSearch [Throwable] --> " + e);
        }
        try {
            RingIDSolrSearch.getInstance().close();
            System.out.println("## RingIDSolrSearch closed");
        } catch (Exception e) {
            System.err.println("destroy RingIDSolrSearch [Exception] --> " + e);
        } catch (Throwable e) {
            System.err.println("destroy RingIDSolrSearch [Throwable] --> " + e);
        }
        try {
            StorageConnection.getInstance().getTransaction().close();
            System.out.println("## StorageConnection getTransaction closed");
        } catch (Exception e) {
            System.err.println("destroy StorageConnection getTransaction [Exception] --> " + e);
        } catch (Throwable e) {
            System.err.println("destroy StorageConnection getTransaction [Throwable] --> " + e);
        }
        try {
            StorageConnection.getInstance().close();
            System.out.println("## StorageConnection closed");
        } catch (Exception e) {
            System.err.println("destroy StorageConnection [Exception] --> " + e);
        } catch (Throwable e) {
            System.err.println("destroy StorageConnection [Throwable] --> " + e);
        }
        try {
            Thread.sleep(5000);
        } catch (Exception e) {
        }
    }

    public List<PageSearchDTO> searchPage(String name, int type, Boolean discoverable, int start, int limit) {
        List<PageSearchDTO> list = null;
        try {

//            URLClassLoader classLoader = (URLClassLoader) StorageTransaction.class.getClassLoader();
//            RingLogger.getConfigPortalLogger().debug(new Gson().toJson(classLoader.getURLs()));
//
//            classLoader = (URLClassLoader) ConcurrentUpdateSolrClient.class.getClassLoader();
            RingLogger.getConfigPortalLogger().debug("searchPage --> name : " + name + " type : " + type + " discoverable : " + discoverable + " start : " + start + " limit : " + limit);
            list = transaction.searchPage(name, type, discoverable, start, limit);
            RingLogger.getConfigPortalLogger().debug("searchPage --> " + (list != null ? list.size() : "list is NULL"));
            if (RingLogger.isDebug()) {
                RingLogger.getConfigPortalLogger().debug("searchPage --> " + (list != null ? new Gson().toJson(list) : "list is NULL"));
            }
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("searchPage [Exception] --> " + e);
        } catch (Throwable e) {
            RingLogger.getConfigPortalLogger().error("searchPage [Throwable] --> " + e);
            RingLogger.getConfigPortalLogger().error("--> " + Arrays.toString(e.getStackTrace()));
        }
        if (list == null) {
            list = new ArrayList<>();
        }
        return list;
    }

    public HashMap<Integer, List<ServiceDTO>> getPageMap(long ownerId) {
        HashMap<Integer, List<ServiceDTO>> pageMapList = new HashMap<>();
        try {
            RingLogger.getConfigPortalLogger().debug("[getPageMap] ownerId --> " + ownerId);
            pageMapList = storageTransaction.getPageMap(ownerId);
            if (pageMapList != null && pageMapList.size() > 0) {
                RingLogger.getConfigPortalLogger().debug("[getPageMap] pageMapList --> " + pageMapList.size());
            } else {
                RingLogger.getConfigPortalLogger().debug("[getPageMap] pageMapList --> NULL");
            }
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [getPageMap] --> " + e);
        }
        return pageMapList;
    }

    public List<PageCategoryDTO> getPageCategories() {
        List<PageCategoryDTO> list;
        try {
            list = transaction.getPageCategories();
            RingLogger.getConfigPortalLogger().debug("[getPageCategories] pageCategoryDTOs.size -> " + list.size());
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Throwable [getPageCategories] --> ", e);
            list = new ArrayList<>();
        }
        return list;
    }

    public int addPageCategory(PageCategoryDTO pageCategoryDTO) {
        int reasonCode = -1;
        RingLogger.getConfigPortalLogger().debug("[addPageCategory] pageCategoryDTO --> " + new Gson().toJson(pageCategoryDTO));
        try {
            reasonCode = transaction.addPageCategory(pageCategoryDTO);
            RingLogger.getConfigPortalLogger().debug("reasonCode[addPageCategory] --> " + reasonCode);
        } catch (Throwable e) {
            RingLogger.getConfigPortalLogger().error("Throwable [addPageCategory] --> ", e);
        }
        return reasonCode;
    }

    public int deletePageCategory(UUID id, int pageType) {
        int reasonCode = -1;
        RingLogger.getConfigPortalLogger().debug("[deletePageCategory] pageCategoryId --> " + id + " pageType --> " + pageType);
        try {
            reasonCode = transaction.deletePageCategory(id, pageType);
            RingLogger.getConfigPortalLogger().debug("reasonCode[deletePageCategory] --> " + reasonCode);
        } catch (Throwable e) {
            RingLogger.getConfigPortalLogger().error("Throwable [deletePageCategory] --> ", e);
        }
        return reasonCode;
    }

    public int editPageCategory(UUID pageId, String name, int pageType) {
        int reasonCode = -1;
        RingLogger.getConfigPortalLogger().debug("[editPageCategory] pageCategoryId --> " + pageId + " name --> " + name + " pageType --> " + pageType);
        try {
            reasonCode = transaction.editPageCategory(pageId, name, pageType);
            RingLogger.getConfigPortalLogger().debug("reasonCode[editPageCategory] --> " + reasonCode);
        } catch (Throwable e) {
            RingLogger.getConfigPortalLogger().error("Throwable [editPageCategory] --> ", e);
        }
        return reasonCode;
    }

    public List<PageCategoryDTO> getDefaultCategories() {
        List<PageCategoryDTO> list;
        try {
            list = transaction.getDefaultCategories();
            RingLogger.getConfigPortalLogger().debug("reasonCode[getDefaultCategories] pageCategoryDTOs.size --> " + list.size());
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Throwable [getDefaultCategories] --> ", e);
            list = new ArrayList<>();
        }
        return list;
    }

    public int addDefaultCategory(PageCategoryDTO pageCategoryDTO) {
        int reasonCode = -1;
        RingLogger.getConfigPortalLogger().debug("[addDefaultCategory] pageCategoryDTO --> " + new Gson().toJson(pageCategoryDTO));
        try {
            reasonCode = transaction.addDefaultCategory(pageCategoryDTO);
            RingLogger.getConfigPortalLogger().debug("reasonCode[addDefaultCategory] --> " + reasonCode);
        } catch (Throwable e) {
            RingLogger.getConfigPortalLogger().error("Throwable [addDefaultCategory] --> ", e);
        }
        return reasonCode;
    }

    public int deleteDefaultCategory(UUID defaultCategoryId, int pageType) {
        int reasonCode = -1;
        RingLogger.getConfigPortalLogger().debug("[deleteDefaultCategory] defaultCategoryId --> " + defaultCategoryId + " pageType --> " + pageType);
        try {
            reasonCode = transaction.deleteDefaultCategory(defaultCategoryId, pageType);
            RingLogger.getConfigPortalLogger().debug("reasonCode[deleteDefaultCategory] --> " + reasonCode);
        } catch (Throwable e) {
            RingLogger.getConfigPortalLogger().error("Throwable [deleteDefaultCategory] --> ", e);
        }
        return reasonCode;
    }

    public int editDefaultCategory(UUID pageCategoryId, String newCategoryName, int pageType) {
        RingLogger.getConfigPortalLogger().debug("[editDefaultCategory] pageCategoryId -> " + pageCategoryId + " newCategoryName -> " + newCategoryName + " pageType --> " + pageType);
        int reasonCode = -1;
        try {
            reasonCode = transaction.editDefaultCategory(pageCategoryId, newCategoryName, pageType);
            RingLogger.getConfigPortalLogger().debug("reasonCode[editDefaultCategory] --> " + reasonCode);
        } catch (Throwable e) {
            RingLogger.getConfigPortalLogger().error("Throwable [editDefaultCategory] --> ", e);
        }
        return reasonCode;
    }

    public UserBasicInfoDTO getUserBasicInfoByRingID(long ringID, boolean doLog) {
        UserBasicInfoDTO userBasicInfoDTO = null;
        try {
            if (doLog) {
                RingLogger.getConfigPortalLogger().debug("[getUserBasicInfoByRingID] ringID --> " + ringID);
            }
            userBasicInfoDTO = storageTransaction.getUserBasicInfoByRingId(ringID);
            if (doLog) {
                RingLogger.getConfigPortalLogger().debug("[getUserBasicInfoByRingID] userBasicInfoDTO --> " + (userBasicInfoDTO == null ? "userBasicInfoDTO is NULL" : userBasicInfoDTO));
            }
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Throwable [getUserBasicInfoByRingID] --> ", e);
        }
        return userBasicInfoDTO;
    }

    /*
     *Channel
     */
    public ChannelMediaDTO getChannelMediaDetails(UUID mediaId) {
        ChannelMediaDTO channelMediaDTO = null;
        try {
            RingLogger.getConfigPortalLogger().info("[getChannelMediaDetails] mediaId --> " + mediaId);
            channelMediaDTO = channelTransaction.getChannelMediaDetails(mediaId);
            RingLogger.getConfigPortalLogger().info("[getChannelMediaDetails] channelMediaDTO --> " + new Gson().toJson(channelMediaDTO));
        } catch (Throwable e) {
            RingLogger.getConfigPortalLogger().error("Throwable [getChannelMediaDetails] --> ", e);
        }
        return channelMediaDTO;
    }

    public List<ChannelMediaDTO> getChannelPlayList(UUID channelId) {
        List<ChannelMediaDTO> channelPendingMedias = new ArrayList<>();
        ChannelFeedBack channelFeedBack;
        try {
            RingLogger.getConfigPortalLogger().info("[getChannelPlayList] channelId --> " + channelId);
            channelFeedBack = channelTransaction.getChannelPlayList(channelId, null, LIMIT);
            if (channelFeedBack.getChannelMedia() != null && !channelFeedBack.getChannelMedia().isEmpty()) {
                channelPendingMedias.addAll(channelFeedBack.getChannelMedia());
            }
//            RingLogger.getConfigPortalLogger().info("[getChannelPlayList] channelFeedBack --> " + new Gson().toJson(channelFeedBack));
            RingLogger.getConfigPortalLogger().info("[getChannelPlayList] channelPendingMedias size --> " + channelPendingMedias.size());
        } catch (Throwable e) {
            RingLogger.getConfigPortalLogger().error("Throwable [getChannelPlayList] --> ", e);
        }
        return channelPendingMedias;
    }

    public List<ChannelMediaDTO> getUniquePublishedMedia(UUID channelId) {
        List<ChannelMediaDTO> channelPendingMedias = new ArrayList<>();
        ChannelFeedBack channelFeedBack;
        try {
            RingLogger.getConfigPortalLogger().info("[getUniquePublishedMedia] channelId --> " + channelId);
            channelFeedBack = channelTransaction.getUniquePublishedMedia(channelId, null, LIMIT);
            if (channelFeedBack.getChannelMedia() != null && !channelFeedBack.getChannelMedia().isEmpty()) {
                channelPendingMedias.addAll(channelFeedBack.getChannelMedia());
            }
//            RingLogger.getConfigPortalLogger().info("[getUniquePublishedMedia] channelFeedBack --> " + new Gson().toJson(channelFeedBack));
            RingLogger.getConfigPortalLogger().info("[getUniquePublishedMedia] channelPendingMedias size --> " + channelPendingMedias.size());
        } catch (Throwable e) {
            RingLogger.getConfigPortalLogger().error("Throwable [getUniquePublishedMedia] --> ", e);
        }
        return channelPendingMedias;
    }

    public List<ChannelMediaDTO> getChannelPendingMedia(UUID channelId) {
        List<ChannelMediaDTO> channelPendingMedias = new ArrayList<>();
        PivotedListReturnDTO<ChannelMediaDTO, UUID> pivotedListReturnDTO;
        try {
            RingLogger.getConfigPortalLogger().info("[getChannelPendingMedia] channelId --> " + channelId);
            pivotedListReturnDTO = channelTransaction.getPendingMedia(channelId, null, LIMIT);
            if (pivotedListReturnDTO.getList() != null && !pivotedListReturnDTO.getList().isEmpty()) {
                channelPendingMedias.addAll(pivotedListReturnDTO.getList());
            }
//            RingLogger.getConfigPortalLogger().info("[getChannelPendingMedia] channelFeedBack --> " + new Gson().toJson(channelFeedBack));
            RingLogger.getConfigPortalLogger().info("[getChannelPendingMedia] channelPendingMedias size --> " + channelPendingMedias.size());
        } catch (Throwable e) {
            RingLogger.getConfigPortalLogger().error("Throwable [getChannelPendingMedia] --> ", e);
        }
        return channelPendingMedias;
    }

    public List<ChannelDTO> getUnverifiedChannels() {
        List<ChannelDTO> unverifiedChannelList = new ArrayList<>();
        PivotedListReturnDTO<ChannelDTO, UUID> pivotedListReturnDTO = new PivotedListReturnDTO<>();
        UUID pivotId = null;
        try {
            do {
                RingLogger.getConfigPortalLogger().debug("[getUnverifiedChannels] pivotId --> " + pivotId + "  limit --> " + LIMIT);
                pivotedListReturnDTO = channelTransaction.getChannelDAO().getUnverifiedChannels(pivotId, LIMIT);
                pivotId = pivotedListReturnDTO.getPivot();
                if (pivotedListReturnDTO.getList() != null && !pivotedListReturnDTO.getList().isEmpty()) {
                    unverifiedChannelList.addAll(pivotedListReturnDTO.getList());
                } else {
                    break;
                }
            } while (pivotedListReturnDTO.getList().size() == LIMIT);
            if (Constants.SERVER.equals("dev")) {
                RingLogger.getConfigPortalLogger().debug("[getUnverifiedChannels] unverifiedChannelList --> " + new Gson().toJson(unverifiedChannelList));
            } else {
                RingLogger.getConfigPortalLogger().debug("[getUnverifiedChannels] unverifiedChannelList.size --> " + unverifiedChannelList.size());
            }
        } catch (Throwable e) {
            RingLogger.getConfigPortalLogger().error("Throwable [getUnverifiedChannels] --> ", e);
        }
        return unverifiedChannelList;
    }

    public List<ChannelDTO> getVerifiedChannels() {
        List<ChannelDTO> verifiedChannelList = new ArrayList<>();
        PivotedListReturnDTO<ChannelDTO, UUID> pivotedListReturnDTO = new PivotedListReturnDTO<>();
        UUID pivotId = null;
        try {
            do {
                RingLogger.getConfigPortalLogger().debug("[getVerifiedChannels] pivotId --> " + pivotId + "  limit --> " + LIMIT);
                pivotedListReturnDTO = channelTransaction.getChannelDAO().getVerifiedChannels(pivotId, LIMIT);
                pivotId = pivotedListReturnDTO.getPivot();
                if (pivotedListReturnDTO.getList() != null && !pivotedListReturnDTO.getList().isEmpty()) {
                    verifiedChannelList.addAll(pivotedListReturnDTO.getList());
                } else {
                    break;
                }
            } while (pivotedListReturnDTO.getList().size() == LIMIT);
            if (Constants.SERVER.equals("dev")) {
                RingLogger.getConfigPortalLogger().debug("[getVerifiedChannels] verifiedChannelList --> " + new Gson().toJson(verifiedChannelList));
            } else {
                RingLogger.getConfigPortalLogger().debug("[getVerifiedChannels] verifiedChannelList.size --> " + verifiedChannelList.size());
            }
        } catch (Throwable e) {
            RingLogger.getConfigPortalLogger().error("Exception in [getVerifiedChannels] --> " + e);
        }
        return verifiedChannelList;
    }

    public List<ChannelDTO> getFeaturedChannels() {
        List<ChannelDTO> featuredChannelList = new ArrayList<>();
        ChannelFeedBack channelFeedBack;
        String pagingPivotStr = null;
        UUID pivotId = null;
        try {
            do {
                RingLogger.getConfigPortalLogger().debug("[getFeaturedChannels] pivotId --> " + pivotId + "  limit --> " + LIMIT + " sessionUserId --> " + com.ringid.admin.utils.Constants.LOGGED_IN_USER_ID);
                channelFeedBack = channelTransaction.getChannelDAO().getFeaturedChannels(com.ringid.admin.utils.Constants.LOGGED_IN_USER_ID, pivotId, LIMIT);
                if (channelFeedBack == null) {
                    RingLogger.getConfigPortalLogger().debug("[getFeaturedChannels] channelFeedBack is NULL");
                    return featuredChannelList;
                }
                if (channelFeedBack.getChannelList() == null || channelFeedBack.getChannelList().isEmpty()) {
                    RingLogger.getConfigPortalLogger().debug("[getFeaturedChannels] channelFeedBack.getChannelList() is null");
                    return featuredChannelList;
                }
                pagingPivotStr = channelFeedBack.getPagingState();
                if (pagingPivotStr != null && !pagingPivotStr.isEmpty()) {
                    pivotId = UUID.fromString(pagingPivotStr);
                }
                if (channelFeedBack.getChannelList() != null && !channelFeedBack.getChannelList().isEmpty()) {
                    featuredChannelList.addAll(channelFeedBack.getChannelList());
                } else {
                    break;
                }
            } while (channelFeedBack.getChannelList().size() == LIMIT);
            RingLogger.getConfigPortalLogger().debug("[getFeaturedChannels] featuredChannelList size--> " + featuredChannelList.size());
        } catch (Throwable e) {
            RingLogger.getConfigPortalLogger().error("Exception in [getFeaturedChannels] --> ", e);
        }
        return featuredChannelList;
    }

    public int verifyChannel(UUID channelId) {
        int reasonCode = -1;
        try {
            RingLogger.getConfigPortalLogger().debug("[verifyChannel] channelId --> " + channelId);
            reasonCode = channelTransaction.getChannelDAO().verifyChannel(channelId);
            RingLogger.getConfigPortalLogger().debug("## [verifyChannel] reasonCode --> " + reasonCode);
        } catch (Throwable e) {
            RingLogger.getConfigPortalLogger().error("Throwable [verifyChannel] --> ", e);
        }
        return reasonCode;
    }

    public int unverifyChannel(UUID channelId) {
        int reasonCode = -1;
        try {
            RingLogger.getConfigPortalLogger().debug("[unverifyChannel] channelId --> " + channelId);
            reasonCode = channelTransaction.getChannelDAO().unverifyChannel(channelId);
            RingLogger.getConfigPortalLogger().debug("## [unverifyChannel] reasonCode --> " + reasonCode);
        } catch (Throwable e) {
            RingLogger.getConfigPortalLogger().error("Throwable [unverifyChannel] --> ", e);
        }
        return reasonCode;
    }

    public int deleteChannel(UUID channelId) {
        int reasonCode = -1;
        try {
            RingLogger.getConfigPortalLogger().debug("[deleteChannel] channelId --> " + channelId);
            reasonCode = channelTransaction.getChannelDAO().deleteChannel(null, channelId);
            RingLogger.getConfigPortalLogger().debug("## [deleteChannel] reasonCode --> " + reasonCode);
        } catch (Throwable e) {
            RingLogger.getConfigPortalLogger().error("Throwable [deleteChannel] --> ", e);
        }
        return reasonCode;
    }

    public int makeChannelFeatured(UUID channelId) {
        int reasonCode = -1;
        try {
            RingLogger.getConfigPortalLogger().debug("[makeChannelFeatured] channelId --> " + channelId);
            reasonCode = transaction.makeChannelFeatured(channelId);
            RingLogger.getConfigPortalLogger().debug("## [makeChannelFeatured] reasonCode --> " + reasonCode);
        } catch (Throwable e) {
            RingLogger.getConfigPortalLogger().error("Throwable [makeChannelFeatured] --> ", e);
        }
        return reasonCode;
    }

    public HashMap<Long, UserBasicInfoDTO> getUserMap(List<Long> userIDs) {
        HashMap<Long, UserBasicInfoDTO> usersMapByUserIDs;
        try {
            usersMapByUserIDs = storageTransaction.getUsersMapByUserIDs(userIDs);
        } catch (Exception e) {
            usersMapByUserIDs = new HashMap<>();
        }
        return usersMapByUserIDs;
    }

    public long getRingID(Long userID) {
        long ringID = 0;
        try {
            UserShortInfoDTO shortInfo = storageTransaction.getUserByID(userID);
            if (shortInfo != null) {
                ringID = shortInfo.getRingID();
            }
        } catch (Exception e) {
            ringID = 0;
        }
        return ringID;
    }

    public long getUserTableId(Long ringId) {
        long userTableId = 0;
        try {
            RingLogger.getConfigPortalLogger().debug("[getUserTableId] --> ringId : " + ringId);
            UserShortInfoDTO shortInfo = storageTransaction.getUserByRingID(ringId);
            if (shortInfo != null) {
                userTableId = shortInfo.getUserTableID();
            }
            RingLogger.getConfigPortalLogger().debug("[getUserTableId] --> userTableId : " + userTableId);
        } catch (Exception e) {
            userTableId = 0;
        }
        return userTableId;
    }

    public List<UUID> getServerChannels(String serverIp) {
        int limit = 100;
        UUID pivotChannelId = null;
        boolean hasMoreData = true;
        List<UUID> allChannelIdList = new ArrayList<>();
        while (hasMoreData) {
            RingLogger.getConfigPortalLogger().debug("serverIp --> " + serverIp + ", pivotChannelId --> " + pivotChannelId + ", limit --> " + limit);
            ChannelFeedBack channelFeedBack = channelTransaction.getServerChannels(serverIp, pivotChannelId, limit);
            List<ChannelDTO> channelList = channelFeedBack.getChannelList();
            if (!channelFeedBack.isSuccess() || channelFeedBack.getReasonCode() > 0) {
                hasMoreData = false;
            }
            if (channelList != null && !channelList.isEmpty()) {
                channelList
                        .forEach(channelDTO -> {
                            allChannelIdList.add(channelDTO.getChannelId());
                        });
                if (channelList.size() < limit) {
                    hasMoreData = false;
                }
            } else {
                hasMoreData = false;
            }
        }
        RingLogger.getConfigPortalLogger().debug("[getServerChannels] allChannelIdList --> " + allChannelIdList.size());

        return allChannelIdList;
    }

    public HashSet<String> getAllUniquePublishedMedia(UUID channelId) {
        int limit = 1000;
        Long pivotStartTime = null;
        boolean hasMoreData = true;
        HashSet<String> allPublishedMediaIdList = new HashSet<>();
        while (hasMoreData) {
            RingLogger.getConfigPortalLogger().debug("channelId --> " + channelId + ", pivotStartTime --> " + pivotStartTime + ", limit --> " + limit);
            ChannelFeedBack channelFeedBack = channelTransaction.getUniquePublishedMedia(channelId, pivotStartTime, limit);
            pivotStartTime = 0L;
            List<ChannelMediaDTO> channelMediaList = channelFeedBack.getChannelMedia();

            if (channelMediaList != null && !channelMediaList.isEmpty()) {
                for (ChannelMediaDTO channelMediaDTO : channelMediaList) {
                    if (pivotStartTime < channelMediaDTO.getStartTime()) {
                        pivotStartTime = channelMediaDTO.getStartTime();
                    }
                    allPublishedMediaIdList.add(channelMediaDTO.getMediaStreamUrl());
                }
                if (channelMediaList.size() < limit) {
                    hasMoreData = false;
                }
            } else {
                hasMoreData = false;
            }
        }
        RingLogger.getConfigPortalLogger().debug("allPublishedMediaIdList --> " + allPublishedMediaIdList.size());
        return allPublishedMediaIdList;
    }

    /*
     * Live room
     */
    public List<StreamRoomDTO> getLiveRoomDetails() {
        List<StreamRoomDTO> streamRoomList = new ArrayList<>();
        try {
            PivotedListReturnDTO<StreamRoomDTO, Long> listReturnDTO = transaction.getLiveRoomPages(0);
            streamRoomList = listReturnDTO.getList();

            StreamRoomDTO celebrityRoomDetails = storageTransaction.getCelebrityRoomDetails();
            streamRoomList.add(celebrityRoomDetails);

            RingLogger.getConfigPortalLogger().debug("[getLiveRoomDetails] streamRoomList --> " + streamRoomList.size());

        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Throwable [getLiveRoomDetails] --> ", e);
        }
        return streamRoomList;
    }

    public int addLiveRoom(StreamRoomDTO streamRoomDTO, boolean isCelebrityRoomPage) {
        int reasonCode = -1;
        try {
            RingLogger.getConfigPortalLogger().debug("[addLiveRoom] streamRoomDTO --> " + new Gson().toJson(streamRoomDTO) + " isCelebrityRoomPage --> " + isCelebrityRoomPage);
            reasonCode = transaction.createLiveRoomPage(streamRoomDTO, isCelebrityRoomPage);
            RingLogger.getConfigPortalLogger().debug("[addLiveRoom] reasonCode --> " + reasonCode);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Throwable [addLiveRoom] --> ", e);
        }
        return reasonCode;
    }

    public int updateLiveRoomInfo(StreamRoomDTO streamRoomDTO) {
        int reasonCode = -1;
        ReturnDTO returnDTO;
        try {
            RingLogger.getConfigPortalLogger().debug("[updateLiveRoomInfo] streamRoomDTO --> " + new Gson().toJson(streamRoomDTO));
            returnDTO = transaction.updateLiveRoomPage(streamRoomDTO);
            reasonCode = returnDTO.getReasonCode();
            RingLogger.getConfigPortalLogger().debug("[updateLiveRoomInfo] reasonCode --> " + reasonCode);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Throwable [updateLiveRoomInfo] --> ", e);
        }
        return reasonCode;
    }

    public int deleteLiveRoom(long roomId) {
        int reasonCode = -1;
        try {
            RingLogger.getConfigPortalLogger().debug("[deleteLiveRoomInfo] roomID --> " + roomId);
            reasonCode = transaction.deleteLiveRoomPage(roomId);
            RingLogger.getConfigPortalLogger().debug("[deleteLiveRoomInfo] reasonCode --> " + reasonCode);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Throwable [deleteLiveRoomInfo] --> ", e);
        }

        return reasonCode;
    }

    public int resetMNVCRetryLimit(long userTableId) {
        int result = -1;
        try {
            RingLogger.getConfigPortalLogger().debug("[resetMNVCRetryLimit] --> userId : " + userTableId);
            result = transaction.resetMNVCRetryLimit(userTableId);
            RingLogger.getConfigPortalLogger().debug("[resetMNVCRetryLimit] result --> " + result);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [resetMNVCRetryLimit] --> " + e.getMessage());
        }
        return result;
    }

    public int resetRVCRetryLimit(long userTableId) {
        int result = -1;
        try {
            RingLogger.getConfigPortalLogger().debug("[resetRVCRetryLimit] --> userId : " + userTableId);
            result = transaction.resetRVCRetryLimit(userTableId);
            RingLogger.getConfigPortalLogger().debug("[resetRVCRetryLimit] result --> " + result);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [resetRVCRetryLimit] --> " + e.getMessage());
        }
        return result;
    }

    public int resetOTPHistory(long userTableId) {
        int result = -1;
        try {
            RingLogger.getConfigPortalLogger().debug("[resetOTPHistory] --> userId : " + userTableId);
            result = transaction.resetOTPHistory(userTableId);
            RingLogger.getConfigPortalLogger().debug("[resetOTPHistory] result --> " + result);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [resetOTPHistory] --> " + e.getMessage());
        }
        return result;
    }

    public int resetOTPHistoryByDevice(long userTableId) {
        int result = -1;
        try {
            RingLogger.getConfigPortalLogger().debug("[resetOTPHistoryByDevice] --> userId : " + userTableId);
            ArrayList<UserDeviceDTO> deviceInfo = getDeviceInfo(userTableId);
            for (UserDeviceDTO device : deviceInfo) {
                int resultTemp = transaction.resetOTPHistoryByDevice(device.getDeviceID());
                if (result < 1) {
                    result = resultTemp;
                }
                resultTemp = resetDeviceIdVerificationHistory(device.getDeviceID());
                if (result < 1) {
                    result = resultTemp;
                }
            }
            RingLogger.getConfigPortalLogger().debug("[resetOTPHistoryByDevice] result --> " + result);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [resetOTPHistoryByDevice] --> " + e.getMessage());
        }
        return result;
    }

    public int resetOTPHistoryByDeviceId(String deviceId) {
        int reasonCode = -1;
        try {
            RingLogger.getConfigPortalLogger().debug("[resetOTPHistoryByDeviceId] --> deviceId : " + deviceId);
            reasonCode = transaction.resetOTPHistoryByDevice(deviceId);
            RingLogger.getConfigPortalLogger().debug("[resetOTPHistoryByDeviceId] result --> " + reasonCode);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [resetOTPHistoryByDeviceId] --> " + e.getMessage());
        }
        return reasonCode;
    }

    public int resetOTPHistoryByNumber(String number) {
        int reasonCode = -1;
        try {
            RingLogger.getConfigPortalLogger().debug("[resetOTPHistoryByNumber] --> number : " + number);
            reasonCode = transaction.resetOTPHistoryByNumber(number);
            RingLogger.getConfigPortalLogger().debug("[resetOTPHistoryByNumber] result --> " + reasonCode);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [resetOTPHistoryByNumber] --> " + e.getMessage());
        }
        return reasonCode;
    }

    public int makeUserPageCelebrity(long pageId, FollowRuleDTO dto) {
        int reasonCode = -1;
        RingLogger.getConfigPortalLogger().debug("[makeUserPageCelebrity] pageId --> " + pageId + " followRuleDTO --> " + new Gson().toJson(dto));
        try {
            reasonCode = transaction.makeUserPageCelebrity(pageId, dto);
            RingLogger.getConfigPortalLogger().debug("[makeUserPageCelebrity] result --> " + reasonCode);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [makeUserPageCelebrity] --> " + e.getMessage());
        }
        return reasonCode;
    }

    public int revokeCelebrityUserPageStatus(long pageID, boolean disableAutoFollow) {
        int result = -1;
        RingLogger.getConfigPortalLogger().debug("[revokeCelebrityUserPageStatus] pageID --> " + pageID + " disableAutoFollow --> " + disableAutoFollow);
        try {
            result = transaction.revokeCelebrityUserPageStatus(pageID, disableAutoFollow);
            RingLogger.getConfigPortalLogger().debug("[revokeCelebrityUserPageStatus] result --> " + result);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [revokeCelebrityUserPageStatus] --> " + e.getMessage());
        }
        return result;
    }

    public List<PageDTO> getPageDetailsForAdmin(List<Long> pageIds, boolean doLog) {
        RingLogger.getConfigPortalLogger().debug("[getPageDetailsForAdmin] pageIds -- > " + new Gson().toJson(pageIds));
        List<PageDTO> pageDTOs = new ArrayList<>();
        try {
            pageDTOs = transaction.getPageDetailsForAdmin(pageIds);
            if (pageDTOs != null && pageDTOs.size() > 0) {
                if (doLog) {
                    RingLogger.getConfigPortalLogger().debug("[getPageDetailsForAdmin] pageDTOs --> " + new Gson().toJson(pageDTOs));
                } else {
                    RingLogger.getConfigPortalLogger().debug("[getPageDetailsForAdmin] pageDTOs --> " + pageDTOs.size());
                }
            } else {
                RingLogger.getConfigPortalLogger().debug("[getPageDetailsForAdmin] pageDTOs --> NULL");
            }
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().debug("Exception in [getPageDetailsForAdmin] --> " + e.getMessage());
        }
        return pageDTOs;
    }

    public List<UserDTO> getCelebrityList(long pivot, int limit) {
        List<PageDTO> celebrityPageDTOs;
        List<UserDTO> celebrityUserDTOs = new ArrayList<>();
        RingLogger.getConfigPortalLogger().debug("[getCelebrityList] pivot --> " + pivot + " limit --> " + limit);
        try {
            List<Long> clebrityPageIds = storageTransaction.getCelebrityUserPageIds(pivot, limit);
            if (clebrityPageIds != null && clebrityPageIds.size() > 0) {
                RingLogger.getConfigPortalLogger().debug("[getCelebrityList] clebrityPageIds -- > " + clebrityPageIds.size());
                celebrityPageDTOs = getPageDetailsForAdmin(clebrityPageIds, false);
                if (celebrityPageDTOs != null && celebrityPageDTOs.size() > 0) {
                    celebrityPageDTOs
                            .forEach(pageDTO -> {
                                UserDTO userDTO = new UserDTO();
                                userDTO.setCelebrityDTO(pageDTO);
                                celebrityUserDTOs.add(userDTO);
                            });
                    RingLogger.getConfigPortalLogger().debug("[getCelebrityList] celebrityUserDTOs --> " + celebrityUserDTOs.size());
                }
            } else {
                RingLogger.getConfigPortalLogger().debug("[getCelebrityList] clebrityPageIds --> NULL");

            }
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().debug("Exception in [getCelebrityList] --> " + e.getMessage());
        }
        return celebrityUserDTOs;
    }

    public UserDTO getCelebrityByPageId(long id) {
        UserDTO userDTO = new UserDTO();
        try {
            RingLogger.getConfigPortalLogger().debug("[getCelebrityByPageId] id --> " + id);
            PageDTO userPageInfo = storageTransaction.getUserPageInfo(id, 0);
            userPageInfo.setCelebrityID(userPageInfo.getPageId());
            userDTO.setCelebrityDTO(userPageInfo);
            RingLogger.getConfigPortalLogger().debug("[getCelebrityByPageId] userDTO --> " + userDTO == null ? " userDTO is null" : new Gson().toJson(userDTO));
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().debug("Exception in [getCelebrityByPageId] --> " + e.getMessage());
        }
        return userDTO;
    }

    public int unverifyMyNumber(String dialingCode, String mobilePhone, long userId) {
        int result = -1;
        RingLogger.getConfigPortalLogger().debug("[unverifyMyNumber] dialingCode --> " + dialingCode + " mobilePhone --> " + mobilePhone + " userId --> " + userId);
        try {
            result = storageTransaction.unverifyMyNumber(dialingCode, mobilePhone, userId);
            RingLogger.getConfigPortalLogger().debug("[unverifyMyNumber] result --> " + result);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [unverifyMyNumber] --> " + e.getMessage());
        }
        return result;
    }

    public int verifyMyNumber(String dialingCode, String mobilePhone, long userId) {
        int result = -1;
        RingLogger.getConfigPortalLogger().debug("[verifyMyNumber] dialingCode --> " + dialingCode + " mobilePhone --> " + mobilePhone + " userId --> " + userId);
        try {
            boolean sucs = storageTransaction.verifyMyPickedNumber(dialingCode, mobilePhone, userId);
            result = sucs == true ? 0 : -1;
            RingLogger.getConfigPortalLogger().debug("[verifyMyNumber] result --> " + result);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [verifyMyNumber] --> " + e.getMessage());
        }
        return result;
    }

    public UserSettingsDTO getMySettings(long userId) {
        UserSettingsDTO userSettingsDTO = new UserSettingsDTO();
        RingLogger.getConfigPortalLogger().debug("[getMySettings] userId --> " + userId);
        try {
            userSettingsDTO = storageTransaction.getMySettings(userId, AppConstants.APP_TYPES.FULL);
            RingLogger.getConfigPortalLogger().debug("[getMySettings] result --> " + new Gson().toJson(userSettingsDTO));
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [getMySettings] --> " + e.getMessage());
        }
        return userSettingsDTO;
    }

    public int makePageFeatured(long pageId, int pageType, FollowRuleDTO followRuleDTO) {
        int reasonCode = -1;
        RingLogger.getConfigPortalLogger().debug("[makePageFeatured] pageId --> " + pageId + " pageType --> " + pageType + " followRuleDTO --> " + new Gson().toJson(followRuleDTO));
        try {
            reasonCode = transaction.makePageFeatured(pageId, pageType, followRuleDTO);
            RingLogger.getConfigPortalLogger().debug("[makePageFeatured] reasonCode --> " + reasonCode);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Throwable [makePageFeatured] --> ", e);
        }
        return reasonCode;
    }

    public int makePageUnfeatured(List<Long> pageIds, int pageType, boolean disableAutoFollow) {
        int reasonCode = -1;
        try {
            for (long pageId : pageIds) {
                RingLogger.getConfigPortalLogger().debug("[makePageUnfeatured] pageId --> " + pageId + " pageType --> " + pageType + " disableAutoFollow --> " + disableAutoFollow);
                reasonCode = transaction.makePageUnfeatured(pageId, pageType, disableAutoFollow);
                RingLogger.getConfigPortalLogger().debug("[makePageUnfeatured] reasonCode --> " + reasonCode);
            }
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Throwable [makePageUnfeatured] --> ", e);
        }
        return reasonCode;
    }

    public PivotedListReturnDTO<Long, UUID> getFeaturedPageIds(int featuredPageType, UUID pivotId, int limit, int scroll) {
        PivotedListReturnDTO<Long, UUID> pivotedListReturnDTO = new PivotedListReturnDTO<>();
        try {
            RingLogger.getConfigPortalLogger().debug("[getFeaturedPages] featuredPageType --> " + featuredPageType + " pivotId --> " + pivotId + " limit --> " + limit + " scroll --> " + scroll);
            pivotedListReturnDTO = transaction.getFeaturedPages(featuredPageType, pivotId, limit, scroll);
            RingLogger.getConfigPortalLogger().debug("[getFeaturedPages] pivotedListReturnDTO --> " + (pivotedListReturnDTO != null ? pivotedListReturnDTO.getList().size() : "pivotedListReturnDTO is NULL"));
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [getFeaturedPages] --> " + e);
        }
        return pivotedListReturnDTO;
    }

    public boolean updateLiveVerifiedSettings(boolean isVerified) {
        boolean result = false;
        try {
            RingLogger.getConfigPortalLogger().debug("[updateLiveVerifiedSettings] isVerifies --> " + isVerified);
            result = storageTransaction.updateLiveVerifiedSettings(isVerified);
            RingLogger.getConfigPortalLogger().debug("[updateLiveVerifiedSettings] result --> " + result);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Throwable [updateLiveVerifiedSettings] --> ", e);
        }
        return result;
    }

    public int makeUserFeatured(long userId, int weight) {
        int result = -1;
        try {
            RingLogger.getConfigPortalLogger().debug("[makeUserFeatured] userId --> " + userId + " weight --> " + weight);
            result = transaction.makeUserFeatured(userId, weight);
            RingLogger.getConfigPortalLogger().debug("[makeUserFeatured] result --> " + result);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Throwable [makeUserFeatured] --> ", e);
        }
        return result;
    }

    public List<CelebrityCategoryDTO> getCelebrityCategoryList() {
        List<CelebrityCategoryDTO> celebrityCategoryDTOs;
        try {
            celebrityCategoryDTOs = storageTransaction.getCelebrityCategories();
            RingLogger.getConfigPortalLogger().debug("[getCelebrityCategoryList] -- > " + celebrityCategoryDTOs.size());
        } catch (Exception e) {
            celebrityCategoryDTOs = new ArrayList<>();
            RingLogger.getConfigPortalLogger().debug("Exception in [getCelebrityCategoryList] --> " + e.getMessage());
        }
        return celebrityCategoryDTOs;
    }

    public boolean addCelebrityCategory(long celebrityPageId, HashMap<String, Integer> categoryMap) {
        boolean sucs;
        try {
            RingLogger.getConfigPortalLogger().debug("[addCelebrityCategory] celebrityPageId --> " + celebrityPageId + " categoryMap --> " + categoryMap);
            sucs = storageTransaction.updateCelebrityCategory(celebrityPageId, categoryMap);
            RingLogger.getConfigPortalLogger().debug("[addCelebrityCategory] -- > [sucs] -> " + sucs);
        } catch (Exception e) {
            sucs = false;
            RingLogger.getConfigPortalLogger().debug("Exception in [getCelebrityCategoryList] --> " + e.getMessage());
        }
        return sucs;
    }

    public boolean updateCelebrityCategory(long celebrityPageId, HashMap<String, Integer> categoryMap) {
        boolean sucs;
        try {
            RingLogger.getConfigPortalLogger().debug("[updateCelebrityCategory] celebrityPageId --> " + celebrityPageId + " categoryMap --> " + categoryMap);
            sucs = storageTransaction.updateCelebrityCategory(celebrityPageId, categoryMap);
            RingLogger.getConfigPortalLogger().debug("[updateCelebrityCategory] -- > [sucs] -> " + sucs);
        } catch (Exception e) {
            sucs = false;
            RingLogger.getConfigPortalLogger().debug("Exception in [updateCelebrityCategory] --> " + e.getMessage());
        }
        return sucs;
    }

    public List<String> getCategoryListOfAParticularCelebrity(long celebrityPageId) {
        ArrayList<String> celebrityCetegoryList;
        try {
            RingLogger.getConfigPortalLogger().debug("[getCategoryListOfAParticularCelebrity] celebrityPageId -- > " + celebrityPageId);
            celebrityCetegoryList = storageTransaction.getCelebrityCetegoryList(celebrityPageId);
            if (celebrityCetegoryList != null) {
                RingLogger.getConfigPortalLogger().debug("[getCategoryListOfAParticularCelebrity] celebrityCetegoryList -- > " + celebrityCetegoryList.size());
            } else {
                RingLogger.getConfigPortalLogger().debug("[getCategoryListOfAParticularCelebrity] celebrityCetegoryList -- > NULL");
            }
        } catch (Exception e) {
            celebrityCetegoryList = new ArrayList<>();
            RingLogger.getConfigPortalLogger().debug("Exception in [getCategoryListOfAParticularCelebrity] --> " + e.getMessage());
        }
        return celebrityCetegoryList;
    }

    public int updateFeaturedUsersWeight(long userId, int weight) {
        int result = -1;
        try {
            RingLogger.getConfigPortalLogger().debug("[updateFeaturedUsersWeight] userId --> " + userId + " weight --> " + weight);
            result = transaction.updateFeaturedUsersWeight(userId, weight);
            RingLogger.getConfigPortalLogger().debug("[updateFeaturedUsersWeight] result --> " + result);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Throwable [updateFeaturedUsersWeight] --> ", e);
        }
        return result;
    }

    public List<LiveUserDetailsDTO> getLiveFeaturedUsers(long pivot, int limit) {
        List<LiveUserDetailsDTO> liveFeaturedUserList = new ArrayList<>();
        RingLogger.getConfigPortalLogger().debug("[getLiveFeaturedUsers] pivot --> " + pivot + " limit --> " + limit);
        try {
            liveFeaturedUserList = transaction.getLiveFeaturedUsers(pivot, limit);
            RingLogger.getConfigPortalLogger().debug("[getLiveFeaturedUsers] liveFeaturedUserList -- > " + liveFeaturedUserList.size());
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().debug("Exception in [getLiveFeaturedUsers] --> " + e.getMessage());
        }

        return liveFeaturedUserList;
    }

    public int updateFeaturedStatus(long userId, int weight, boolean userVerificationStatus) {
        int reasonCode = -1;
        try {
            RingLogger.getConfigPortalLogger().debug("[updateFeaturedStatus] userId --> " + userId + " weight --> " + weight + " userVerificationStatus --> " + userVerificationStatus);
            reasonCode = transaction.updateFeaturedStatus(userId, weight, userVerificationStatus);
            RingLogger.getConfigPortalLogger().debug("[updateFeaturedStatus] reasonCode -- > " + reasonCode);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().debug("Exception in [getLiveFeaturedUsers] --> " + e.getMessage());
        }

        return reasonCode;
    }

    public UserBasicInfoDTO getUserBasicInfo(long wallOwnerId) {
        UserBasicInfoDTO userBasicInfoDTO = null;
        try {
            RingLogger.getConfigPortalLogger().debug("[getUserBasicInfo] wallOwnerId --> " + wallOwnerId);
            userBasicInfoDTO = storageTransaction.getUserBasicInfo(wallOwnerId);
            RingLogger.getConfigPortalLogger().debug("getUserBasicInfo --> " + (userBasicInfoDTO == null ? "userBasicInfoDTO is NULL" : new Gson().toJson(userBasicInfoDTO)));
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Throwable [getUserBasicInfo] --> ", e);
        }
        return userBasicInfoDTO;
    }

    public UserProfileDetailsDTO getMyProfile(long wallOwnerId) {
        UserProfileDetailsDTO userProfileDetailsDTO;
        try {
            RingLogger.getConfigPortalLogger().debug("[getMyProfile] wallOwnerId --> " + wallOwnerId);
            userProfileDetailsDTO = storageTransaction.getMyProfile(wallOwnerId);
            RingLogger.getConfigPortalLogger().debug("getMyProfile --> " + (userProfileDetailsDTO == null ? "userProfileDetailsDTO is NULL" : new Gson().toJson(userProfileDetailsDTO)));
        } catch (Exception e) {
            userProfileDetailsDTO = new UserProfileDetailsDTO();
            RingLogger.getConfigPortalLogger().error("Throwable [getMyProfile] --> ", e);
        }
        return userProfileDetailsDTO;
    }

    public UserDTO getUserDetails(long wallOwnerId, boolean doLog) {
        UserDTO userDTO;
        try {
            if (doLog) {
                RingLogger.getConfigPortalLogger().debug("[getUserDetails] wallOwnerId --> " + wallOwnerId);
            }
            userDTO = storageTransaction.getUserDetails(wallOwnerId, 0L);
            if (doLog) {
                RingLogger.getConfigPortalLogger().debug("getUserDetails --> " + (userDTO == null ? "userDTO is NULL" : new Gson().toJson(userDTO)));
            }
        } catch (Exception e) {
            userDTO = new UserDTO();
            RingLogger.getConfigPortalLogger().error("Throwable [getUserDetails] --> ", e);
        }
        return userDTO;
    }

    public CassAlbumDTO getAlbumDetails(UUID albumId, long activeUserId, int mediaType, long sessionUserID) {
        CassAlbumDTO albumDetails;
        try {
            RingLogger.getConfigPortalLogger().debug("albumId --> " + albumId + ", ownerId --> " + sessionUserID + ", mediaType --> " + mediaType + ", sessionUserId --> " + sessionUserID);
            albumDetails = storageTransaction.getAlbumDetails(albumId, sessionUserID, mediaType, sessionUserID);
            RingLogger.getConfigPortalLogger().debug("## albumDetails --> " + new Gson().toJson(albumDetails));
        } catch (Exception e) {
            albumDetails = new CassAlbumDTO();
            RingLogger.getConfigPortalLogger().error("## Exception [getAlbumDetails] --> ", e);
        }
        return albumDetails;
    }

    public int addStatus(FeedDTO feedDTO) {
        int reasonCode;
        try {
            RingLogger.getConfigPortalLogger().debug("## feedDTO --> " + new Gson().toJson(feedDTO));
            reasonCode = storageTransaction.addStatus(feedDTO);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("## Exception [addStatus] --> ", e);
            reasonCode = ReasonCode.EXCEPTION_OCCURED;
        }
        return reasonCode;
    }

    public int editStatus(FeedDTO feedDTO, long sessionUserId) {
        int reasonCode;
        try {
            RingLogger.getConfigPortalLogger().debug("[editStatus] sessionUserId --> " + sessionUserId + " feedDTO --> " + new Gson().toJson(feedDTO));
            reasonCode = storageTransaction.editStatus(feedDTO, sessionUserId);
            RingLogger.getConfigPortalLogger().debug("[editStatus] reasonCode --> " + reasonCode);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("## Exception [editStatus] --> ", e);
            reasonCode = ReasonCode.EXCEPTION_OCCURED;
        }
        return reasonCode;
    }

    public int verifyNumber(UUID channelId) {
        int reasonCode = -1;
        try {
            RingLogger.getConfigPortalLogger().debug("[verifyChannel] channelId --> " + channelId);
            reasonCode = channelTransaction.getChannelDAO().verifyChannel(channelId);
            RingLogger.getConfigPortalLogger().debug("## [verifyChannel] reasonCode --> " + reasonCode);
        } catch (Throwable e) {
            RingLogger.getConfigPortalLogger().error("Throwable [verifyChannel] --> ", e);
        }
        return reasonCode;
    }

    public int addTariff(StreamingTariffDTO dto) {
        int reasonCode = -1;
        RingLogger.getConfigPortalLogger().debug("[addTarrif] streamingTariffDTO --> " + new Gson().toJson(dto));
        try {
            reasonCode = transaction.addCallTariff(dto);
            RingLogger.getConfigPortalLogger().debug("[addTarrif] reasonCode --> " + reasonCode);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Throwable [addTarrif] --> ", e);
        }
        return reasonCode;
    }
}
