/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin;

import com.ringid.admin.actions.fileUpload.FileUploadForm;
import com.ringid.admin.authServerCommunicator.AuthServerCommunicator;
import com.ringid.admin.dto.CheckLogDTO;
import com.ringid.admin.utils.Constants;
import java.io.IOException;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;

import org.apache.struts.action.ActionMapping;
import org.apache.struts.upload.FormFile;

/**
 *
 * @author Kazi Nasir Uddin Oly
 * @Date Jun 7, 2016
 */
public class BaseAction extends Action {

    protected final String SUCCESS = "success";
    protected final String FAILURE = "failure";
    protected final String UNAUTHORIZED = "unauthorized";

    protected String checkAuthentication(ActionMapping mapping, ActionForm form, HttpServletRequest request) {
        ActionErrors errors = form.validate(mapping, request);
        if (errors != null && !errors.isEmpty()) {
            if (errors.get("auth").hasNext()) {
                return UNAUTHORIZED;
            } else {
                saveErrors(request.getSession(), errors);
                return FAILURE;
            }
        }
        return SUCCESS;
    }

    protected void managePages(HttpServletRequest request, BaseForm form, long dataListSize) {
        long pageNo = 1;
        if (form.getPageNo() > 0) {
            pageNo = form.getPageNo();
        }

        long totalPages = 1;
        if (dataListSize != -1) {
            if (dataListSize > 0) {
                totalPages = dataListSize / form.getRecordPerPage();
                if (dataListSize % form.getRecordPerPage() != 0) {
                    totalPages++;
                }
            }
            if (totalPages < pageNo) {
                pageNo = totalPages;
            }
        } else {
            switch (form.getAction()) {
                case Constants.RECORD_PER_PAGE_CHANGE:
                    totalPages = 1;
                    pageNo = 1;
                    break;
                case Constants.SEARCH:
                    pageNo = 1;
                    break;
                default:
                    if (request.getSession().getAttribute(Constants.TOTAL_PAGES) != null && Integer.parseInt(request.getSession().getAttribute(Constants.TOTAL_PAGES).toString()) > 0) {
                        totalPages = Integer.parseInt(request.getSession().getAttribute(Constants.TOTAL_PAGES).toString());
                    }
                    if (totalPages < pageNo) {
                        totalPages = pageNo;
                    }
                    break;
            }
        }
        form.setPageNo((int) pageNo);
        request.setAttribute(Constants.TOTAL_RECORDS, dataListSize);
        request.setAttribute(Constants.TOTAL_PAGES, totalPages);
        request.setAttribute(Constants.CURRENT_PAGE_NO, pageNo);
        request.setAttribute(Constants.STARTING_RECORD_NO, (pageNo - 1) * form.getRecordPerPage());
        request.setAttribute(Constants.ENDING_RECORD_NO, pageNo * form.getRecordPerPage());
        request.getSession(true).setAttribute(Constants.RECORD_PER_PAGE, form.getRecordPerPage());
    }

    protected String[] getDataFromFile(ActionForm form) throws IOException {
        FileUploadForm myForm = (FileUploadForm) form;
        FormFile myFile = myForm.getTheFile();
        byte[] fileData = myFile.getFileData();
        String data = new String(fileData);
        return data.split("\r\n|\r|\n");
    }

    protected boolean isNull(String value) {
        return (value == null || value.length() == 0);
    }

    protected void notifyAuthServers(final int serverType, final String serverIP) throws IOException {
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    AuthServerCommunicator authServerCommunicator = new AuthServerCommunicator();
                    authServerCommunicator.startReceiver();
                    authServerCommunicator.notifyAuthServers(serverType, serverIP);
                } catch (Exception e) {
                }
            }
        });
        t.start();
    }

    protected void notifyAuthServers(final List<CheckLogDTO> checkLogDTOs) throws IOException {
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    AuthServerCommunicator authServerCommunicator = new AuthServerCommunicator();
                    authServerCommunicator.startReceiver();
                    authServerCommunicator.notifyAuthServers(checkLogDTOs);
                } catch (Exception e) {
                }
            }
        });
        t.start();
    }

    protected void notifyAuthServers(final int serverType, final int serverId) throws IOException {
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    AuthServerCommunicator authServerCommunicator = new AuthServerCommunicator();
                    authServerCommunicator.startReceiver();
                    authServerCommunicator.notifyAuthServers(serverType, serverId);
                } catch (Exception e) {
                }
            }
        });
        t.start();
    }

    protected String getSuccessMessage(String message) {
        return "<span style='color: green'>" + message + "</span>";
    }

    protected String getErrorMessage(String message) {
        return "<span style='color: red'>" + message + "</span>";
    }
}
