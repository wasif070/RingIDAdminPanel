/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.breakingNews;

import com.ringid.admin.dto.BreakingNewsDTO;
import com.ringid.admin.service.BreakingNewsService;
import com.ringid.admin.repository.BreakingNewsRepository;
import com.google.gson.Gson;
import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.dto.adminAuth.LoginDTO;
import com.ringid.admin.utils.Constants;
import com.ringid.admin.utils.ReasonCode;
import java.util.ArrayList;
import java.util.UUID;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.ringid.newsportals.NPBasicFeedInfoDTO;

/**
 *
 * @author Kazi Nasir Uddin Oly
 * @Date Dec 8, 2016
 */
public class BreakingNewsListAction extends BaseAction {

    private ArrayList<NPBasicFeedInfoDTO> list = new ArrayList<>();

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = FAILURE;
        String activistName;
        try {
            LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
            activistName = loginDTO.getUserName();
        } catch (Exception e) {
            target = UNAUTHORIZED;
            return mapping.findForward(target);
        }
        try {
            BreakingNewsForm breakingNewsForm = (BreakingNewsForm) form;
            BreakingNewsDTO dto = new BreakingNewsDTO();
            String breakingNewsType = null;
            if (request.getParameter("ForceReload") != null && "1".equals(request.getParameter("ForceReload"))) {
                BreakingNewsRepository.getInstance().forceLoadData();
            }
            // decalare common breaking or remove breaking
            if (breakingNewsForm.getSelectedIDs() != null && breakingNewsForm.getSelectedIDs().length > 0) {
                ArrayList<UUID> newsIds = new ArrayList<>();
                for (String s : breakingNewsForm.getSelectedIDs()) {
                    newsIds.add(UUID.fromString(s));
                }
                RingLogger.getActivityLogger().debug("Action : DeclareOrRemoveBreakingAction[" + breakingNewsForm.getSubmitType() + "] activistName : " + activistName + " newsIds --> " + new Gson().toJson(newsIds));
                int reasonCode = -1;
                if (breakingNewsForm.getSubmitType().equals("Make Breaking")) {
                    reasonCode = BreakingNewsService.getInstance().makeCommonBreakingFeed(newsIds);
                } else {
                    reasonCode = BreakingNewsService.getInstance().removeFromBreakingFeed(newsIds);
                }
                if (reasonCode == ReasonCode.NONE) {
                    request.setAttribute("message", "<span style='color: green'>Operation Successfull</span>");
                } else {
                    request.setAttribute("message", "<span style='color: red'>Operation failed</span>");
                }
            }
            // list operation
            if (breakingNewsForm.getBreakingNewsType() == null) {
                breakingNewsForm.setBreakingNewsType("page");
            }
            breakingNewsType = breakingNewsForm.getBreakingNewsType();
            BreakingNewsService scheduler = BreakingNewsService.getInstance();
            if (breakingNewsForm.getSearchText() != null && breakingNewsForm.getSearchText().length() > 0) {
                dto.setSearchText(breakingNewsForm.getSearchText().trim().toLowerCase());
            }
            if (breakingNewsForm.getColumn() <= 0) {
                breakingNewsForm.setColumn(Constants.COLUMN_ONE);
                breakingNewsForm.setSort(Constants.DESC_SORT);
            }
            dto.setColumn(breakingNewsForm.getColumn());
            dto.setSortType(breakingNewsForm.getSort());
            switch (breakingNewsType) {
                case "page":
                    list = scheduler.getBreakingNewsListFromPages(dto);
                    RingLogger.getConfigPortalLogger().debug("list size ==> " + (list == null ? "Page BreakingList is NULL" : list.size()));
                    break;
                case "common":
                    list = scheduler.getCommonBreakingNewsList(dto);
                    RingLogger.getConfigPortalLogger().debug("list size ==> " + (list == null ? "Common BreakingList is NULL" : list.size()));
                    break;
            }
            request.getSession(true).setAttribute(Constants.DATA_ROWS, list);
            breakingNewsForm.setTotalDataSize(list.size());
            managePages(request, breakingNewsForm, breakingNewsForm.getTotalDataSize());
            target = SUCCESS;
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in BreakingNewsListAction --> " + e);
            target = FAILURE;
        }
        return mapping.findForward(target);
    }
}
