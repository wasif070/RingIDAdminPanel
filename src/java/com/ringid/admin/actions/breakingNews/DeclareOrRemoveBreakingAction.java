/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.breakingNews;

import com.ringid.admin.service.BreakingNewsService;
import com.google.gson.Gson;
import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.dto.adminAuth.LoginDTO;
import com.ringid.admin.utils.Constants;
import com.ringid.admin.utils.ReasonCode;
import java.util.ArrayList;
import java.util.UUID;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

/**
 *
 * @author Kazi Nasir Uddin Oly
 * @Date Dec 8, 2016
 */
public class DeclareOrRemoveBreakingAction extends BaseAction {

    ActionErrors err = new ActionErrors();

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = FAILURE;
        String activistName;
        try {
            LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
            activistName = loginDTO.getUserName();
        } catch (Exception e) {
            target = UNAUTHORIZED;
            return mapping.findForward(target);
        }
        try {
            BreakingNewsForm breakingNewsForm = (BreakingNewsForm) form;
            if (breakingNewsForm.getSelectedIDs() != null && breakingNewsForm.getSelectedIDs().length > 0) {
                ArrayList<UUID> newsIds = new ArrayList<>();
                for (String s : breakingNewsForm.getSelectedIDs()) {
                    newsIds.add(UUID.fromString(s));
                }
                RingLogger.getActivityLogger().debug("Action : DeclareOrRemoveBreakingAction[" + breakingNewsForm.getSubmitType() + "] activistName : " + activistName + " newsIds --> " + new Gson().toJson(newsIds));
                int reasonCode = -1;
                if (breakingNewsForm.getSubmitType().equals("Make Breaking")) {
                    reasonCode = BreakingNewsService.getInstance().makeCommonBreakingFeed(newsIds);
                } else {
                    reasonCode = BreakingNewsService.getInstance().removeFromBreakingFeed(newsIds);
                }
                if (reasonCode == ReasonCode.NONE) {
                    err.add("errormsg", new ActionMessage("errors.newsupdate.successful"));
                } else {
                    err.add("errormsg", new ActionMessage("update failed , reasonCode: " + reasonCode, false));
                }
                saveErrors(request.getSession(), err);
            } else {
                err.add("errormsg", new ActionMessage("errors.select.one"));
                saveErrors(request.getSession(), err);
            }
            request.getSession(true).setAttribute(Constants.RECORD_PER_PAGE, breakingNewsForm.getRecordPerPage());
            request.setAttribute(Constants.CURRENT_PAGE_NO, breakingNewsForm.getPageNo());
        } catch (NullPointerException e) {
            target = FAILURE;
        }
        return mapping.findForward(target);
    }

}
