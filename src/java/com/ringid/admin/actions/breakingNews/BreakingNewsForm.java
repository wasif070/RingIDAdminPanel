/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.breakingNews;

import com.ringid.admin.BaseForm;

/**
 *
 * @author Kazi Nasir Uddin Oly
 * @Date Dec 8, 2016
 */
public class BreakingNewsForm extends BaseForm {

    private int id;
    private String feedId;
    private String pageName;
    private String pageType;
    private String newsTitle;
    private String breaking;
    private long totalDataSize;
    private String breakingNewsType;

    public String getBreakingNewsType() {
        return breakingNewsType;
    }

    public void setBreakingNewsType(String breakingNewsType) {
        this.breakingNewsType = breakingNewsType;
    }

    public long getTotalDataSize() {
        return totalDataSize;
    }

    public void setTotalDataSize(long totalDataSize) {
        this.totalDataSize = totalDataSize;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFeedId() {
        return feedId;
    }

    public void setFeedId(String feedId) {
        this.feedId = feedId;
    }

    public String getPageName() {
        return pageName;
    }

    public void setPageName(String pageName) {
        this.pageName = pageName;
    }

    public String getPageType() {
        return pageType;
    }

    public void setPageType(String pageType) {
        this.pageType = pageType;
    }

    public String getNewsTitle() {
        return newsTitle;
    }

    public void setNewsTitle(String newsTitle) {
        this.newsTitle = newsTitle;
    }

    public String isBreaking() {
        return breaking;
    }

    public void setBreaking(String isBreaking) {
        this.breaking = isBreaking;
    }

}
