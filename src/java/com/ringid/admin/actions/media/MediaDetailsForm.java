/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ringid.admin.actions.media;

import com.ringid.admin.BaseForm;
import org.ringid.newsfeeds.CassMultiMediaDTO;

/**
 *
 * @author Kazi Nasir Uddin Oly 
 * @Date Dec 11, 2016
 */
public class MediaDetailsForm extends BaseForm {
   private CassMultiMediaDTO cassMultiMediaDTO;

    public CassMultiMediaDTO getCassMultiMediaDTO() {
        return cassMultiMediaDTO;
    }

    public void setCassMultiMediaDTO(CassMultiMediaDTO cassMultiMediaDTO) {
        this.cassMultiMediaDTO = cassMultiMediaDTO;
    }
   
   
    
    
}
