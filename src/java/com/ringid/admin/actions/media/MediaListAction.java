/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.media;

import com.ringid.admin.dto.MediaDTO;
import com.ringid.admin.service.MediaService;
import com.ringid.admin.repository.MediaLoader;
import com.google.gson.Gson;
import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.dto.adminAuth.LoginDTO;
import com.ringid.admin.utils.Constants;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.ringid.newsfeeds.CassMultiMediaDTO;

/**
 *
 * @author Kazi Nasir Uddin Oly
 * @Date Dec 10, 2016
 */
public class MediaListAction extends BaseAction {

    private List<CassMultiMediaDTO> mediaList = null;

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = FAILURE;
        String activistName;
        try {
            LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
            activistName = loginDTO.getUserName();
        } catch (Exception e) {
            target = UNAUTHORIZED;
            return mapping.findForward(target);
        }
        try {
            MediaForm mediaForm = (MediaForm) form;
            MediaDTO dto = new MediaDTO();

            if (request.getParameter("ForceReload") != null && "1".equals(request.getParameter("ForceReload"))) {
                MediaLoader.getInstance().forceReload();
            }
            // make media trending or remove
            if (mediaForm.getSelectedIDs() != null && mediaForm.getSelectedIDs().length > 0) {
                ArrayList<UUID> mediaIds = initiateMediaIdList(request.getParameter("id"), mediaForm.getSelectedIDs());
                String submitType = mediaForm.getSubmitType();
                RingLogger.getActivityLogger().debug("Action : MakeTrendingAction[" + submitType + "] activistName : " + activistName + " mediaIds --> " + new Gson().toJson(mediaIds));
                boolean trendingResult = MakeOrRemoveTrending(submitType, mediaIds);
                setResultMessage(trendingResult, request);
            }
            // list part
            if (mediaForm.getMediaType() == null) {
                mediaForm.setMediaType("recent");
            }
            if (mediaForm.getSearchText() != null && mediaForm.getSearchText().length() > 0) {
                dto.setSearchText(mediaForm.getSearchText().trim().toLowerCase());
            }
            if (mediaForm.getColumn() <= 0) {
                mediaForm.setColumn(Constants.COLUMN_ONE);
                mediaForm.setSort(Constants.DESC_SORT);
            }
            dto.setColumn(mediaForm.getColumn());
            dto.setSortType(mediaForm.getSort());
            switch (mediaForm.getMediaType()) {
                case "recent":
                    mediaList = MediaService.getInstance().getRecentMedias(dto);
                    break;
                case "trending":
                    mediaList = MediaService.getInstance().getTrendingMedias(dto);
                    break;
            }
            mediaForm.setTotalDataSize(mediaList.size());
            request.getSession(true).setAttribute(Constants.DATA_ROWS, mediaList);
            managePages(request, mediaForm, mediaForm.getTotalDataSize());
        } catch (NullPointerException e) {
            target = FAILURE;
        }
        return mapping.findForward(target);
    }

    private ArrayList<UUID> initiateMediaIdList(String idFromParams, String[] idsFromMediaForm) {
        ArrayList<UUID> mediaIds = new ArrayList<>();
        if (idFromParams != null) {
            mediaIds.add(UUID.fromString(idFromParams));
        } else if (idsFromMediaForm != null && idsFromMediaForm.length > 0) {
            for (String s : idsFromMediaForm) {
                mediaIds.add(UUID.fromString(s));
            }
        }
        return mediaIds;
    }

    private boolean MakeOrRemoveTrending(String submitType, ArrayList<UUID> mediaIds) throws Exception {
        boolean trendingResult = false;
        switch (submitType) {
            case "Make Trending":
                trendingResult = MediaService.getInstance().makeTrending(mediaIds);
                break;
            case "Remove From Trending":
                List<CassMultiMediaDTO> mediaDTOs = MediaService.getInstance().getTrendingMultiMediaDTOsFromId(mediaIds);
                trendingResult = MediaService.getInstance().removeTrending(mediaDTOs);
                break;
        }
        return trendingResult;
    }

    private void setResultMessage(boolean trendingResult, HttpServletRequest request) {
        if (trendingResult) {
            request.setAttribute("successMessage", "success");
        } else {
            request.setAttribute("errorMessage", "error");
        }
    }
}
