/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.media;

import com.ringid.admin.BaseForm;

/**
 *
 * @author Kazi Nasir Uddin Oly
 * @Date Dec 10, 2016
 */
public class MediaForm extends BaseForm {

    private int totalDataSize;
    private String mediaType;
    private Long ringId;

    public Long getRingId() {
        return ringId;
    }

    public void setRingId(Long ringId) {
        this.ringId = ringId;
    }

    public String getMediaType() {
        return mediaType;
    }

    public void setMediaType(String mediaType) {
        this.mediaType = mediaType;
    }

    public int getTotalDataSize() {
        return totalDataSize;
    }

    public void setTotalDataSize(int totalDataSize) {
        this.totalDataSize = totalDataSize;
    }

}
