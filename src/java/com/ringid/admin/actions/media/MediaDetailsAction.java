/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.media;

import com.ringid.admin.service.MediaService;
import com.ringid.admin.BaseAction;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.ringid.newsfeeds.CassMultiMediaDTO;

/**
 *
 * @author Kazi Nasir Uddin Oly
 * @Date Dec 11, 2016
 */
public class MediaDetailsAction extends BaseAction {


    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = FAILURE;
        try {
            int index = Integer.parseInt(request.getParameter("index"));
            String mediaType = request.getParameter("mediaType");
            CassMultiMediaDTO mediaDetails = null;
            if(mediaType.equals("recent")){
                mediaDetails = MediaService.getInstance().getMediaDetails(index);
            } else if(mediaType.equals("trending")){
                mediaDetails = MediaService.getInstance().getTrendingMediaDetails(index);
            }
            MediaDetailsForm mediaDetailsForm = (MediaDetailsForm) form;
            mediaDetailsForm.setCassMultiMediaDTO(mediaDetails);
            request.setAttribute("mediaType", mediaType);
            request.setAttribute("type", mediaDetails.getMultiMediaType());
            target = SUCCESS;
        } catch (NullPointerException e) {
            target = FAILURE;
        }
        return mapping.findForward(target);
    }
}
