/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.media;

import com.ringid.admin.service.MediaService;
import com.google.gson.Gson;
import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.dto.adminAuth.LoginDTO;
import com.ringid.admin.utils.Constants;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.ringid.newsfeeds.CassMultiMediaDTO;

/**
 *
 * @author Kazi Nasir Uddin Oly
 * @Date Dec 11, 2016
 */
public class MakeTrendingAction extends BaseAction {

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = FAILURE;
        String activistName;
        try {
            LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
            activistName = loginDTO.getUserName();
        } catch (Exception e) {
            target = UNAUTHORIZED;
            return mapping.findForward(target);
        }
        try {
            MediaForm mediaForm = (MediaForm) form;
            ArrayList<UUID> mediaIds = initiateMediaIdList(request.getParameter("id"), mediaForm.getSelectedIDs());
            String submitType = mediaForm.getSubmitType();
            RingLogger.getActivityLogger().debug("Action : MakeTrendingAction[" + submitType + "] activistName : " + activistName + " mediaIds --> " + new Gson().toJson(mediaIds));
            boolean trendingResult = MakeOrRemoveTrending(submitType, mediaIds);
            setResultMessage(trendingResult, request);
            switch (submitType) {
                case "Make Trending":
                    request.setAttribute("mediaType", "recent");
                    break;
                case "Remove From Trending":
                    request.setAttribute("mediaType", "trending");
                    break;
            }

            request.getSession(true).setAttribute(Constants.RECORD_PER_PAGE, mediaForm.getRecordPerPage());
            request.setAttribute(Constants.CURRENT_PAGE_NO, mediaForm.getPageNo());
        } catch (NullPointerException e) {
            target = FAILURE;
        }
        return mapping.findForward(target);
    }

    private ArrayList<UUID> initiateMediaIdList(String idFromParams, String[] idsFromMediaForm) {
        ArrayList<UUID> mediaIds = new ArrayList<>();
        if (idFromParams != null) {
            mediaIds.add(UUID.fromString(idFromParams));
        } else if (idsFromMediaForm != null && idsFromMediaForm.length > 0) {
            for (String s : idsFromMediaForm) {
                mediaIds.add(UUID.fromString(s));
            }
        }
        return mediaIds;
    }

    private boolean MakeOrRemoveTrending(String submitType, ArrayList<UUID> mediaIds) throws Exception {
        boolean trendingResult = false;
        switch (submitType) {
            case "Make Trending":
                trendingResult = MediaService.getInstance().makeTrending(mediaIds);
                break;
            case "Remove From Trending":
                List<CassMultiMediaDTO> mediaDTOs = MediaService.getInstance().getTrendingMultiMediaDTOsFromId(mediaIds);
                trendingResult = MediaService.getInstance().removeTrending(mediaDTOs);
                break;
        }
        return trendingResult;
    }

    private void setResultMessage(boolean trendingResult, HttpServletRequest request) {
        if (trendingResult) {
            request.setAttribute("successMessage", "success");
        } else {
            request.setAttribute("errorMessage", "error");
        }
    }
}
