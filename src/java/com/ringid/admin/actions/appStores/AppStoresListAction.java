/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.appStores;

import com.ringid.admin.dto.AppStoresDTO;
import com.ringid.admin.service.AppStoresService;
import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.utils.Constants;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author mamun
 * @Date Jan 28, 2017
 */
public class AppStoresListAction extends BaseAction {

    ArrayList<AppStoresDTO> list = new ArrayList<>();

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = FAILURE;
        try {
            AppStoresForm appStoresForm = (AppStoresForm) form;
            AppStoresDTO appStoresDTO = new AppStoresDTO();
            AppStoresService scheduler = AppStoresService.getInstance();

            if (appStoresForm.getSearchText() != null) {
                appStoresDTO.setQueryString(appStoresForm.getSearchText().trim().toLowerCase());
            }

            list = scheduler.getAppStoresList(appStoresForm.getColumn(), appStoresForm.getSort(), appStoresDTO);
            RingLogger.getConfigPortalLogger().debug("AppStoresListAction : [ TotalRecords : " + list.size() + " ]");

            request.getSession(true).setAttribute(Constants.DATA_ROWS, list);
            managePages(request, appStoresForm, list.size());
        } catch (NullPointerException e) {
            RingLogger.getConfigPortalLogger().error(e);
            target = FAILURE;
        }
        return mapping.findForward(target);
    }
}
