/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.appStores;

import com.ringid.admin.dto.AppStoresDTO;
import com.ringid.admin.service.AppStoresService;
import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author mamun
 * @Date Jan 28, 2017
 */
public class AppStoresEditAction extends BaseAction {

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        String target = checkAuthentication(mapping, form, request);
        try {
            if (target.equals(SUCCESS)) {
                int id = 0;
                AppStoresForm appStoresForm = (AppStoresForm) form;
                if (request.getParameter("id") != null) {
                    id = Integer.parseInt(request.getParameter("id"));
                }
                RingLogger.getConfigPortalLogger().debug("AppStoresEditAction : [Id : " + id + " ]");

                AppStoresService scheduler = AppStoresService.getInstance();
                AppStoresDTO appStoresDTO = scheduler.getAppStoresInfoById(id);

                if (appStoresDTO != null) {
                    RingLogger.getConfigPortalLogger().debug("AppStoresEditAction : " + appStoresDTO.toString());
                    appStoresForm.setId(appStoresDTO.getId());
                    appStoresForm.setDeviceType(String.valueOf(appStoresDTO.getDevice()));
                    appStoresForm.setVersion(appStoresDTO.getVersion());
                    target = SUCCESS;
                } else {
                    RingLogger.getConfigPortalLogger().debug("AppStoresEditAction : appStoresDTO is null");
                }
            }
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error(e);
            target = FAILURE;
        }
        return mapping.findForward(target);
    }
}
