/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.appStores;

import com.ringid.admin.dto.AppStoresDTO;
import com.ringid.admin.service.AppStoresService;
import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.dto.adminAuth.LoginDTO;
import com.ringid.admin.utils.Constants;
import com.ringid.admin.utils.MyAppError;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author mamun
 * @Date Jan 28, 2017
 */
public class AppStoresDeleteAction extends BaseAction {

    ArrayList<AppStoresDTO> list = new ArrayList<>();

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        String target = checkAuthentication(mapping, form, request);
        String activistName;
        try {
            LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
            activistName = loginDTO.getUserName();
        } catch (Exception e) {
            target = UNAUTHORIZED;
            return mapping.findForward(target);
        }
        try {
            if (target.equals(SUCCESS)) {
                AppStoresForm appStoresForm = (AppStoresForm) form;

                int id = 0;
                if (request.getParameter("id") != null) {
                    id = Integer.parseInt(request.getParameter("id"));
                }
                RingLogger.getActivityLogger().debug("[AppStoresDeleteAction] activistName : " + activistName + " Id --> " + id);
                RingLogger.getConfigPortalLogger().debug("AppStoresDeleteAction : [Id : " + id + " ]");
                AppStoresService scheduler = AppStoresService.getInstance();
                MyAppError error = scheduler.deleteAppStores(id);
                RingLogger.getConfigPortalLogger().debug("AppStoresDeleteAction : [error : " + error.getERROR_TYPE() + " ]");
                if (error.getERROR_TYPE() > 0) {
                    target = FAILURE;
                } else {

                    //AppStoresLoader.getInstance().forceReload();
                    list = scheduler.getAppStoresList();
                    request.getSession(true).setAttribute(Constants.DATA_ROWS, list);
                    managePages(request, appStoresForm, list.size());
                }
            }
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in AppStoresDeleteAction....", e);
            target = FAILURE;
        }
        return mapping.findForward(target);
    }
}
