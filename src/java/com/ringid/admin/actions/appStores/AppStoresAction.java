/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.appStores;

import com.ringid.admin.dto.AppStoresDTO;
import com.ringid.admin.service.AppStoresService;
import com.google.gson.Gson;
import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.dto.adminAuth.LoginDTO;
import com.ringid.admin.utils.Constants;
import com.ringid.admin.utils.MyAppError;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author mamun
 * @Date Jan 28, 2017
 */
public class AppStoresAction extends BaseAction {

    ArrayList<AppStoresDTO> list = new ArrayList<>();

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = checkAuthentication(mapping, form, request);
        String activistName;
        try {
            LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
            activistName = loginDTO.getUserName();
        } catch (Exception e) {
            target = UNAUTHORIZED;
            return mapping.findForward(target);
        }
        try {
            if (target.equals(SUCCESS)) {
                AppStoresForm appStoresForm = (AppStoresForm) form;
                AppStoresService scheduler = AppStoresService.getInstance();
                AppStoresDTO appStoresDTO = scheduler.getAppStoresDTO(appStoresForm);
                RingLogger.getConfigPortalLogger().debug(appStoresForm.getDeviceType());
                RingLogger.getConfigPortalLogger().debug(appStoresForm.getVersion());
                RingLogger.getActivityLogger().debug("Action : AppStoresAction[add] activistName : " + activistName + " appStoresDTO --> " + new Gson().toJson(appStoresDTO));
                MyAppError error = scheduler.addAppStoresInfo(appStoresDTO);
                if (error.getERROR_TYPE() > 0) {
                    if (error.getERROR_TYPE() == MyAppError.OTHERERROR) {
                        RingLogger.getConfigPortalLogger().error("Other error in addAppStores: " + error.getERROR_TYPE());
                        appStoresForm.setMessage(error.getErrorMessage());
                        request.getSession(true).setAttribute("error", "2");
                    }
                    target = FAILURE;
                } else {

                    //AppStoresLoader.getInstance().forceReload();
                    list = scheduler.getAppStoresList();
                    request.getSession(true).setAttribute(Constants.DATA_ROWS, list);
                    managePages(request, appStoresForm, list.size());
                }
            }
        } catch (Exception e) {
            target = FAILURE;
            RingLogger.getConfigPortalLogger().error("Exception in AppStoresAction....", e);
        }
        return mapping.findForward(target);
    }
}
