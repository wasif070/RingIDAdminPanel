/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.appStores;

import com.ringid.admin.dto.AppStoresDTO;
import com.ringid.admin.service.AppStoresService;
import com.google.gson.Gson;
import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.dto.adminAuth.LoginDTO;
import com.ringid.admin.utils.Constants;
import com.ringid.admin.utils.MyAppError;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author mamun
 * @Date Jan 28, 2017
 */
public class AppStoresUpdateAction extends BaseAction {

    ArrayList<AppStoresDTO> list = new ArrayList<>();

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        String target = checkAuthentication(mapping, form, request);
        String activistName;
        try {
            LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
            activistName = loginDTO.getUserName();
        } catch (Exception e) {
            target = UNAUTHORIZED;
            return mapping.findForward(target);
        }
        try {
            if (target.equals(SUCCESS)) {
                AppStoresForm appStoresForm = (AppStoresForm) form;
                AppStoresService scheduler = AppStoresService.getInstance();
                AppStoresDTO appStoresDTO = scheduler.getAppStoresDTO(appStoresForm);
                MyAppError error = new MyAppError();
                RingLogger.getActivityLogger().debug("[AppStoresUpdateAction] activistName : " + activistName + " appStoresDTO --> " + new Gson().toJson(appStoresDTO));
                RingLogger.getConfigPortalLogger().debug("AppStoresUpdateAction : [Id : " + appStoresForm.getId() + " ]");

                if (appStoresForm.getId() > 0) {
                    appStoresDTO.setId(appStoresForm.getId());
                    error = scheduler.updateAppStores(appStoresDTO);
                }
                RingLogger.getConfigPortalLogger().debug("AppStoresUpdateAction : [ error: " + error.getERROR_TYPE() + " ]");

                if (error.getERROR_TYPE() > 0) {
                    target = FAILURE;
                    appStoresForm.setMessage(error.getErrorMessage());
                    request.getSession(true).setAttribute("error", "2");
                } else {
                    list = scheduler.getAppStoresList();
                    request.getSession(true).setAttribute(Constants.DATA_ROWS, list);
                    managePages(request, appStoresForm, list.size());
                }
            }
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception AppStoresUpdateAction...", e);
        }
        return mapping.findForward(target);
    }
}
