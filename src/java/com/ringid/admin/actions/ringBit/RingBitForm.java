/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.ringBit;

import com.ringid.admin.BaseForm;
import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.upload.FormFile;

/**
 *
 * @author Rabby
 */
public class RingBitForm extends BaseForm {

    private long userId;
    private String ringId;
    private String userName;
    private double coinAmount;
    private long addTime;
    private int status;
    private String settingsValueImage;
    private String settingsValueText;
    private int dataType;
    private long ringBitImageId;
    private long ringBitTextId;
    private FormFile theFile;

    public FormFile getTheFile() {
        return theFile;
    }

    public void setTheFile(FormFile theFile) {
        this.theFile = theFile;
    }

    public long getRingBitImageId() {
        return ringBitImageId;
    }

    public void setRingBitImageId(long ringBitImageId) {
        this.ringBitImageId = ringBitImageId;
    }

    public long getRingBitTextId() {
        return ringBitTextId;
    }

    public void setRingBitTextId(long ringBitTextId) {
        this.ringBitTextId = ringBitTextId;
    }

    public String getSettingsValueImage() {
        return settingsValueImage;
    }

    public void setSettingsValueImage(String settingsValueImage) {
        this.settingsValueImage = settingsValueImage;
    }

    public String getSettingsValueText() {
        return settingsValueText;
    }

    public void setSettingsValueText(String settingsValueText) {
        this.settingsValueText = settingsValueText;
    }

    public int getDataType() {
        return dataType;
    }

    public void setDataType(int dataType) {
        this.dataType = dataType;
    }

    public String getRingId() {
        return ringId;
    }

    public void setRingId(String ringId) {
        this.ringId = ringId;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public double getCoinAmount() {
        return coinAmount;
    }

    public void setCoinAmount(double coinAmount) {
        this.coinAmount = coinAmount;
    }

    public long getAddTime() {
        return addTime;
    }

    public void setAddTime(long addTime) {
        this.addTime = addTime;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        ActionErrors errors = new ActionErrors();
        return errors;
    }
}
