/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.ringBit;

import com.ringid.admin.dto.RingBitDTO;
import com.ringid.admin.repository.RingBitLoader;
import com.ringid.admin.service.RingBitService;
import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.Constants;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author Rabby
 */
public class RingBitListAction extends BaseAction {

    List<RingBitDTO> list = new ArrayList<>();

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = SUCCESS;
        try {
            RingBitForm formBeam = (RingBitForm) form;
            if (request.getParameter("ForceReload") != null) {
                RingBitLoader.getInstance().forceLoad();
            }
            list = RingBitService.getInstance().getRingBitList(formBeam.getSearchText());
            request.getSession(true).setAttribute(Constants.DATA_ROWS, list);
            managePages(request, formBeam, list.size());
        } catch (Exception e) {
            target = FAILURE;
        }
        return mapping.findForward(target);
    }
}
