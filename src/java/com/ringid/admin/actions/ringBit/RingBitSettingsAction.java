/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.ringBit;

import com.ringid.admin.dto.RingBitDTO;
import com.ringid.admin.service.RingBitService;
import com.ringid.admin.dao.RingBitDAO;
import com.google.gson.Gson;
import com.ringid.admin.BaseAction;
import com.ringid.admin.actions.liveRoom.UploadBannerImage;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.utils.Constants;
import com.ringid.admin.utils.MyAppError;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.upload.FormFile;
import ringid.LoginTaskScheduler;

/**
 *
 * @author mamun
 */
public class RingBitSettingsAction extends BaseAction {

    static Logger loggger = RingLogger.getConfigPortalLogger();

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = FAILURE;
        MyAppError error = new MyAppError();
        try {
            RingBitForm ringBitForm = (RingBitForm) form;
            RingBitDTO ringBitDTO = new RingBitDTO();
            RingBitService scheduler = RingBitService.getInstance();
            FormFile myFile;
            String fileName, bannerUrl;
            byte[] fileData;
            auth.com.utils.MyAppError myAppError;
            switch (ringBitForm.getAction()) {
                case Constants.ADD:
                    ringBitDTO = RingBitDAO.getInstance().getRingBitSettingImageAndTextInfo();
                    myFile = ringBitForm.getTheFile();
                    fileName = myFile.getFileName();
                    fileData = myFile.getFileData();
                    if (ringBitDTO.getRingBitImageId() > 0) {
                        if (fileName.length() > 0 || fileData.length > 0) {
                            myAppError = LoginTaskScheduler.getInstance().login(Constants.USERID, Constants.PASSWORD);
                            RingLogger.getConfigPortalLogger().debug("##  login --> " + new Gson().toJson(myAppError));
                            bannerUrl = new UploadBannerImage().upLoadToCloud(Constants.USERID, fileName, fileData);
                            RingLogger.getConfigPortalLogger().debug("##  bannerUrl --> " + bannerUrl);
                            myAppError = LoginTaskScheduler.getInstance().logout(Constants.USERID);
                            RingLogger.getConfigPortalLogger().debug("##  myAppError --> " + new Gson().toJson(myAppError));
                            ringBitForm.setSettingsValueImage(bannerUrl);
                        }
                        ringBitDTO = scheduler.getRingBitDTO(ringBitForm);
                        error = scheduler.updateRingBitSettingInfo(ringBitDTO);
                        if (error.getERROR_TYPE() == MyAppError.NOERROR) {
                            target = SUCCESS;
                        }
                    } else {
                        if (fileName.length() < 1 || fileData.length < 1) {
                            throw new Exception("please upload appropriate data!");
                        }
                        myAppError = LoginTaskScheduler.getInstance().login(Constants.USERID, Constants.PASSWORD);
                        RingLogger.getConfigPortalLogger().debug("##  login --> " + new Gson().toJson(myAppError));
                        bannerUrl = new UploadBannerImage().upLoadToCloud(Constants.USERID, fileName, fileData);
                        RingLogger.getConfigPortalLogger().debug("##  bannerUrl --> " + bannerUrl);
                        myAppError = LoginTaskScheduler.getInstance().logout(Constants.USERID);
                        RingLogger.getConfigPortalLogger().debug("##  myAppError --> " + new Gson().toJson(myAppError));
                        if (bannerUrl != null) {
                            ringBitForm.setSettingsValueImage(bannerUrl);
                            ringBitDTO = scheduler.getRingBitDTO(ringBitForm);
                            error = scheduler.addRingBitSettingInfo(ringBitDTO);
                            if (error.getERROR_TYPE() == MyAppError.NOERROR) {
                                target = SUCCESS;
                            }
                        }
                    }
                    break;
                case Constants.EDIT:
                    ringBitDTO = RingBitDAO.getInstance().getRingBitSettingImageAndTextInfo();
                    if (ringBitDTO != null) {
                        ringBitForm.setSettingsValueImage(ringBitDTO.getSettingValueImage());
                        ringBitForm.setSettingsValueText(ringBitDTO.getSettingValueText());
                        ringBitForm.setRingBitImageId(ringBitDTO.getRingBitImageId());
                        ringBitForm.setRingBitTextId(ringBitDTO.getRingBitTextId());
                        target = SUCCESS;
                    }
                    break;
                case Constants.UPDATE:
                    myFile = ringBitForm.getTheFile();
                    fileName = myFile.getFileName();
                    fileData = myFile.getFileData();
                    if (fileName.length() > 0 || fileData.length > 0) {
                        myAppError = LoginTaskScheduler.getInstance().login(Constants.USERID, Constants.PASSWORD);
                        RingLogger.getConfigPortalLogger().debug("##  login --> " + new Gson().toJson(myAppError));

                        bannerUrl = new UploadBannerImage().upLoadToCloud(Constants.USERID, fileName, fileData);
                        RingLogger.getConfigPortalLogger().debug("##  bannerUrl --> " + bannerUrl);
                        myAppError = LoginTaskScheduler.getInstance().logout(Constants.USERID);
                        RingLogger.getConfigPortalLogger().debug("##  myAppError --> " + new Gson().toJson(myAppError));

                        ringBitForm.setSettingsValueImage(bannerUrl);
                    }
                    ringBitDTO = scheduler.getRingBitDTO(ringBitForm);
                    error = scheduler.updateRingBitSettingInfo(ringBitDTO);
                    if (error.getERROR_TYPE() == MyAppError.NOERROR) {
                        target = SUCCESS;
                    }
                    break;
                default:
                    target = SUCCESS;
                    break;
            }
            if (SUCCESS.equals(target) && ringBitForm.getAction() != Constants.EDIT) {
                ringBitDTO = RingBitDAO.getInstance().getRingBitSettingImageAndTextInfo();
                List<RingBitDTO> list = new ArrayList<>();
                list.add(ringBitDTO);
                request.getSession(true).setAttribute(Constants.DATA_ROWS, list);
                managePages(request, ringBitForm, list.size());
            }
        } catch (NullPointerException e) {
            target = FAILURE;
        }
        return mapping.findForward(target);
    }
}
