package com.ringid.admin.actions.login;

import com.ringid.admin.dto.adminAuth.LoginDTO;
import com.ringid.admin.dao.adminAuth.LoginDAO;
import com.ringid.admin.utils.MyAppError;

public class LoginTaskSchedular {

    private LoginTaskSchedular() {

    }

    private static class LoginTaskSchedularHolder {

        private static final LoginTaskSchedular INSTANCE = new LoginTaskSchedular();
    }

    public static LoginTaskSchedular getInstance() {
        return LoginTaskSchedularHolder.INSTANCE;
    }

    public MyAppError checkErrorInfo(LoginDTO loginDTO) {
        MyAppError error;
        LoginDAO loginDAO = new LoginDAO();
        error = loginDAO.checkLoginInfo(loginDTO);
        return error;
    }
}
