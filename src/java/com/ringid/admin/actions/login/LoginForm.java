package com.ringid.admin.actions.login;

import com.ringid.admin.utils.Constants;
import javax.servlet.http.HttpServletRequest;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

public class LoginForm extends org.apache.struts.action.ActionForm {

    private long userId;
    private String userName;
    private String fullName;
    private String email;
    private String password;
    private int userType;
    private String message;

    public LoginForm() {

    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getUserType() {
        return userType;
    }

    public void setUserType(int userType) {
        this.userType = userType;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        ActionErrors errors = new ActionErrors();
        boolean userNameRequired = false;
        if (getUserName() == null || getUserName().length() < 1) {
            userNameRequired = true;
        }
        if (getPassword() == null || getPassword().length() < 1) {
            if (userNameRequired) {
                errors.add(Constants.MESSAGE, new ActionMessage("errors.usernamepass.required"));
            } else {
                errors.add(Constants.MESSAGE, new ActionMessage("errors.password.required"));
            }
            return errors;
        }
        if (userNameRequired) {
            errors.add(Constants.MESSAGE, new ActionMessage("errors.userName.required"));
        }
        return errors;
    }

}
