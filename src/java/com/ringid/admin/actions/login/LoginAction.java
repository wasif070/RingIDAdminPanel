package com.ringid.admin.actions.login;

import com.ringid.admin.dto.adminAuth.LoginDTO;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.projectMenu.MenuDTO;
import com.ringid.admin.projectMenu.MenuGenerator;
import com.ringid.admin.actions.userFeatureManagement.UserFeatureTaskScheduler;
import com.ringid.admin.utils.Constants;
import com.ringid.admin.utils.MyAppError;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class LoginAction extends org.apache.struts.action.Action {

    private static final String SUCCESS = "success";
    private static final String FAILURE = "failure";
//    public static boolean startApplication = false;
    static Logger logger = RingLogger.getConfigPortalLogger();

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm from, HttpServletRequest request, HttpServletResponse response) throws Exception {

        String target = FAILURE;
        LoginForm loginBean = null;
        try {
            loginBean = (LoginForm) from;

            LoginDTO loginDTO = new LoginDTO();
            loginDTO.setUserName(loginBean.getUserName());
            loginDTO.setPassword(loginBean.getPassword());
            loginDTO.setLoginTime(System.currentTimeMillis());

            MyAppError error = LoginTaskSchedular.getInstance().checkErrorInfo(loginDTO);

            switch (error.getERROR_TYPE()) {
                case MyAppError.NOERROR: {
                    loginDTO.setFeauterMapList(UserFeatureTaskScheduler.getInstance().getFeatureMappingByRoleId(loginDTO.getPermissionLevel()));
                    request.getSession(true).setAttribute(Constants.LOGIN_DTO, loginDTO);
                    request.getSession(true).setAttribute(Constants.LOGIN_ID, loginDTO.getUserId());
                    List<MenuDTO> menuList = new MenuGenerator().generateMenu(loginDTO);
                    request.getSession(true).setAttribute("menuList", menuList);
                    request.getSession(true).setAttribute("subMenuMap", new MenuGenerator().getSubMenuMap(menuList));
                    target = SUCCESS;
                    break;
                }
                case MyAppError.DBERROR: {
                    throw new Exception("Database connection failed " + error.getErrorMessage());
                }
                case MyAppError.VALIDATIONERROR: {
                    throw new Exception("invalid credentials");
                }
            }
        } catch (Exception e) {
            String errorText = e.toString();
            if (errorText.contains(".Exception:")) {
                errorText = errorText.substring(errorText.indexOf(".Exception:") + ".Exception:".length() + 1);
            }
            if (loginBean != null) {
                loginBean.setMessage(errorText);
            }
            request.getSession(true).setAttribute(Constants.LOGIN_DTO, null);
            request.getSession(true).setAttribute(Constants.LOGIN_ID, null);
            logger.debug("Exception [LoginAction] --> " + errorText);
        }
        return mapping.findForward(target);
    }
}
