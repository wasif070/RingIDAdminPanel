/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.floationMenuSlideItem;

import com.ringid.admin.dto.FloatingMenuSlideItemDTO;
import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.service.FloationMenuService;
import com.ringid.admin.utils.Constants;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author Rabby
 */
public class FloatingMenuSlideItemListAction extends BaseAction {

    private static final Logger logger = RingLogger.getConfigPortalLogger();
    List<FloatingMenuSlideItemDTO> list;

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = SUCCESS;
        try {
            FloatingMenuSlideItemForm floatingMenuSlideItemForm = (FloatingMenuSlideItemForm) form;
            list = FloationMenuService.getInstance().getData(floatingMenuSlideItemForm.getFeatureType(), floatingMenuSlideItemForm.getStatus(), floatingMenuSlideItemForm.getSearchText());
            request.getSession(true).setAttribute(Constants.DATA_ROWS, list);
            managePages(request, floatingMenuSlideItemForm, list.size());
        } catch (Exception e) {
            logger.error(e);
            target = FAILURE;
        }
        return mapping.findForward(target);
    }
}
