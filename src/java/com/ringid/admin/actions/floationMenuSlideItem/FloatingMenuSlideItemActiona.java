/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.floationMenuSlideItem;

import com.ringid.admin.BaseAction;
import com.ringid.admin.service.FloationMenuService;
import com.ringid.admin.utils.MyAppError;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author Rabby
 */
public class FloatingMenuSlideItemActiona extends BaseAction {

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = SUCCESS;
        try {
            FloatingMenuSlideItemForm floatingMenuSlideItemForm = (FloatingMenuSlideItemForm) form;
            MyAppError error = new MyAppError();
            switch (floatingMenuSlideItemForm.getSubmitType()) {
                case "ADD":
                    error = FloationMenuService.getInstance().addFloatingMenuSlideItem(floatingMenuSlideItemForm.getFeatureType(), floatingMenuSlideItemForm.getSelectedIDs());
                    break;
                case "REMOVE":
                    error = FloationMenuService.getInstance().deleteFloatingMenuSlideItem(floatingMenuSlideItemForm.getFeatureType(), floatingMenuSlideItemForm.getSelectedIDs());
                    break;
            }
            if (error.getERROR_TYPE() == MyAppError.NOERROR) {
                request.setAttribute("message", "<span style='color:green'>Operation successfull</span>");
            } else {
                request.setAttribute("message", "<span style='color:red'>Operation failed[" + error.getErrorMessage() + "]</span>");
                target = FAILURE;
            }
        } catch (Exception e) {
            target = FAILURE;
        }
        return mapping.findForward(target);
    }
}
