/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.deactivateReasons;

import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.dto.adminAuth.LoginDTO;
import com.ringid.admin.service.AdminService;
import com.ringid.admin.utils.Constants;
import com.ringid.admin.utils.Utils;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.ringid.utilities.AppConstants;

/**
 *
 * @author ipvision
 */
public class DeactivateReasonsAction extends BaseAction {

    Logger logger = RingLogger.getConfigPortalLogger();

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = checkAuthentication(mapping, form, request);
        String activistName;
        try {
            LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
            activistName = loginDTO.getUserName();
        } catch (Exception e) {
            target = UNAUTHORIZED;
            return mapping.findForward(target);
        }
        try {
            if (target.equals(SUCCESS)) {
                DeactivateReasonsForm deactivateReasonsForm = (DeactivateReasonsForm) form;
                int reasonCode = -1;
                int id = -1;
                id = AdminService.getInstance().addDeactivateReasonsInfo(deactivateReasonsForm.getDescription());
                if (id > 0) {
                    RingLogger.getActivityLogger().debug("Action : DeactivateReasonsAction[add] activistName : " + activistName + " id --> " + id + " description --> " + deactivateReasonsForm.getDescription());
                    reasonCode = AdminService.getInstance().addDeactivateReasonsInfo(id, deactivateReasonsForm.getDescription());

                    if (reasonCode == AppConstants.NONE) {
                        request.setAttribute("message", "<span style='color: green'> Add DeactivateReasons Successfull </span>");
                        target = SUCCESS;
                    } else {
                        request.setAttribute("message", "<span style='color: red'>" + Utils.getMessageForReasonCode(reasonCode) + "</span>");
                        target = FAILURE;
                    }
                } else {
                    request.setAttribute("message", "<span style='color: red'> Exception in [DeactivateReasonsDAO] --> addDeactivateReasonsInfo </span>");
                    target = FAILURE;
                }
            }
        } catch (Exception e) {
            target = FAILURE;
        }
        return mapping.findForward(target);
    }
}
