/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.deactivateReasons;

import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.service.AdminService;
import com.ringid.admin.utils.Constants;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.ringid.users.DeactiveReasonDTO;

/**
 *
 * @author ipvision
 */
public class DeactivateReasonsListAction extends BaseAction {

    static Logger loggger = RingLogger.getConfigPortalLogger();

    List<DeactiveReasonDTO> list = new ArrayList<>();

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = FAILURE;
        try {
            DeactivateReasonsForm deactivateReasonsForm = (DeactivateReasonsForm) form;

            list = AdminService.getInstance().getDeactivateReasonsList(deactivateReasonsForm.getSearchText());
            request.getSession(true).setAttribute(Constants.DATA_ROWS, list);
            managePages(request, deactivateReasonsForm, list.size());
        } catch (NullPointerException e) {
            target = FAILURE;
        }
        return mapping.findForward(target);
    }
}
