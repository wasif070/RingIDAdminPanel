/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.userPages;

import com.google.gson.Gson;
import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.dto.adminAuth.LoginDTO;
import com.ringid.admin.actions.newsPortal.UploadImage;
import com.ringid.admin.service.PageService;
import com.ringid.admin.utils.Constants;
import com.ringid.admin.utils.Utils;
import java.util.UUID;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.ringid.newsfeeds.ImageDTO;
import org.ringid.pages.PageDTO;
import org.ringid.utilities.AppConstants;
import ringid.LoginTaskScheduler;
import ringid.sessionless.SessionlessTaskseheduler;

/**
 *
 * @author mamun
 */
public class UserPageAction extends BaseAction {

    Logger logger = RingLogger.getConfigPortalLogger();

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = checkAuthentication(mapping, form, request);
        String activistName;
        try {
            LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
            activistName = loginDTO.getUserName();
        } catch (Exception e) {
            target = UNAUTHORIZED;
            return mapping.findForward(target);
        }
        try {
            if (target.equals(SUCCESS)) {
                UserPageForm userPageForm = (UserPageForm) form;

                String profileImageUrl = "";
                UUID profileImageId = null;
                String coverImageUrl = "";
                UUID coverImageId = null;
                PageDTO pageDTO = null;
                int reasonCode = 0;

                switch (userPageForm.getAction()) {
                    case Constants.ADD:
                        LoginTaskScheduler.getInstance().initSessionLessCommunication(Constants.USERID);
                        long pageId = SessionlessTaskseheduler.getInstance().getIdFromUserRingIdGenerator(Constants.USERID);
                        long pageRingId = SessionlessTaskseheduler.getInstance().getIdFromRingIdGenerator(Constants.USERID);
                        LoginTaskScheduler.getInstance().destroySessionLessCommunication(Constants.USERID);

                        LoginTaskScheduler.getInstance().login(Constants.USERID, Constants.PASSWORD);
                        if (userPageForm.getTheFileProfileImage().getFileName() != null && userPageForm.getTheFileProfileImage().getFileName().length() > 0) {
                            String fileName = userPageForm.getTheFileProfileImage().getFileName();
                            byte[] fileData = userPageForm.getTheFileProfileImage().getFileData();
                            ImageDTO imageDTO = new UploadImage().upLoadToCloud(Constants.USERID, fileName, fileData, Constants.PROFILE_IMAGE_UPLOAD_URL);
                            if (imageDTO != null) {
                                profileImageUrl = imageDTO.getImageURL();
                                userPageForm.setProfileImageUrl(profileImageUrl);
                                imageDTO.setImageType(AppConstants.IMAGE_TYPE_PROFILE_IMAGE);
                                imageDTO.setImagePrivacy(AppConstants.PROFILE_IMAGE_PRIVACY);
                                imageDTO.setUserTableID(pageId);
                                int rc = PageService.getInstance().addProfileOrCoverImage(imageDTO);
                                profileImageId = imageDTO.getImageID();
                                System.out.println(profileImageUrl);
                            }
                        }
                        if (userPageForm.getTheFileCoverImage().getFileName() != null && userPageForm.getTheFileCoverImage().getFileName().length() > 0) {
                            String fileName = userPageForm.getTheFileCoverImage().getFileName();
                            byte[] fileData = userPageForm.getTheFileCoverImage().getFileData();
                            ImageDTO imageDTO = new UploadImage().upLoadToCloud(Constants.USERID, fileName, fileData, Constants.COVER_IMAGE_UPLOAD_URL);
                            if (imageDTO != null) {
                                coverImageUrl = imageDTO.getImageURL();
                                userPageForm.setCoverImageUrl(coverImageUrl);
                                imageDTO.setImageType(AppConstants.IMAGE_TYPE_COVER_IMAGE);
                                imageDTO.setImagePrivacy(AppConstants.COVER_IMAGE_PRIVACY);
                                imageDTO.setUserTableID(pageId);
                                int rc = PageService.getInstance().addProfileOrCoverImage(imageDTO);
                                coverImageId = imageDTO.getImageID();
                                System.out.println(coverImageUrl);
                            }
                        }
                        LoginTaskScheduler.getInstance().logout(Constants.USERID);

                        pageDTO = new PageDTO();
                        pageDTO.setPageName(userPageForm.getPageName());
                        pageDTO.setPageType(userPageForm.getPageType());
                        pageDTO.setPageId(pageId);
                        pageDTO.setPageRingId(pageRingId);
                        pageDTO.setCountry(userPageForm.getCountry());
                        pageDTO.setProfileImageUrl(profileImageUrl);
                        pageDTO.setProfileImageId(profileImageId);
                        pageDTO.setCoverImageUrl(coverImageUrl);
                        pageDTO.setCoverImageId(coverImageId);
                        pageDTO.setOwnerId(userPageForm.getOwnerId());
                        pageDTO.setPageOwner(userPageForm.getOwnerId());
                        if (userPageForm.getPageCategoryId() != null) {
                            pageDTO.setPageCategoryId(UUID.fromString(userPageForm.getPageCategoryId()));
                        }

                        RingLogger.getActivityLogger().debug("[add page UserPageAction] activistName : " + activistName + " pageDTO --> " + new Gson().toJson(pageDTO));
                        reasonCode = PageService.getInstance().createPage(pageDTO);
                        if (reasonCode == AppConstants.NONE) {
                            target = SUCCESS;
                            request.setAttribute("message", "<span style='color: green'>Successfully added</span>");
                        } else {
                            target = FAILURE;
                            request.setAttribute("message", "<span style='color: red'>Failed to added(rc: " + reasonCode + ")</span>");
                        }
                        break;
                    case Constants.EDIT:
                        pageDTO = PageService.getInstance().getPageInfo(userPageForm.getPageId());
                        if (pageDTO != null) {
                            userPageForm.setPageRingId(pageDTO.getPageRingId());
                            userPageForm.setPageType(pageDTO.getPageType());
                            userPageForm.setPageName(pageDTO.getPageName());
                            userPageForm.setCountry(pageDTO.getCountry());
                            userPageForm.setProfileImageUrl(pageDTO.getProfileImageUrl());
                            userPageForm.setCoverImageUrl(pageDTO.getCoverImageUrl());
                            userPageForm.setOwnerId(pageDTO.getPageOwner());
                            if (pageDTO.getPageCategoryId() != null) {
                                userPageForm.setPageCategoryId(pageDTO.getPageCategoryId().toString());
                            }
                        } else {
                            target = FAILURE;
                        }
                        break;
                    case Constants.UPDATE:

                        profileImageUrl = userPageForm.getProfileImageUrl();
                        profileImageId = null;
                        coverImageUrl = userPageForm.getCoverImageUrl();
                        coverImageId = null;

                        LoginTaskScheduler.getInstance().login(Constants.USERID, Constants.PASSWORD);
                        if (userPageForm.getTheFileProfileImage().getFileName() != null && userPageForm.getTheFileProfileImage().getFileName().length() > 0) {
                            String fileName = userPageForm.getTheFileProfileImage().getFileName();
                            byte[] fileData = userPageForm.getTheFileProfileImage().getFileData();
                            ImageDTO imageDTO = new UploadImage().upLoadToCloud(Constants.USERID, fileName, fileData, Constants.PROFILE_IMAGE_UPLOAD_URL);
                            profileImageUrl = imageDTO.getImageURL();
                            userPageForm.setProfileImageUrl(profileImageUrl);
                            imageDTO.setImageType(AppConstants.IMAGE_TYPE_PROFILE_IMAGE);
                            imageDTO.setImagePrivacy(AppConstants.PROFILE_IMAGE_PRIVACY);
                            imageDTO.setUserTableID(userPageForm.getPageId());
                            int rc = PageService.getInstance().addProfileOrCoverImage(imageDTO);
                            profileImageId = imageDTO.getImageID();
                            System.out.println(profileImageUrl);
                        }
                        if (userPageForm.getTheFileCoverImage().getFileName() != null && userPageForm.getTheFileCoverImage().getFileName().length() > 0) {
                            String fileName = userPageForm.getTheFileCoverImage().getFileName();
                            byte[] fileData = userPageForm.getTheFileCoverImage().getFileData();
                            ImageDTO imageDTO = new UploadImage().upLoadToCloud(Constants.USERID, fileName, fileData, Constants.COVER_IMAGE_UPLOAD_URL);
                            coverImageUrl = imageDTO.getImageURL();
                            userPageForm.setCoverImageUrl(coverImageUrl);
                            imageDTO.setImageType(AppConstants.IMAGE_TYPE_COVER_IMAGE);
                            imageDTO.setImagePrivacy(AppConstants.COVER_IMAGE_PRIVACY);
                            imageDTO.setUserTableID(userPageForm.getPageId());
                            int rc = PageService.getInstance().addProfileOrCoverImage(imageDTO);
                            coverImageId = imageDTO.getImageID();
                            System.out.println(coverImageUrl);
                        }
                        LoginTaskScheduler.getInstance().logout(Constants.USERID);

                        pageDTO = new PageDTO();
                        pageDTO.setPageName(userPageForm.getPageName());
                        pageDTO.setPageType(userPageForm.getPageType());
                        pageDTO.setPageId(userPageForm.getPageId());
                        pageDTO.setPageRingId(userPageForm.getPageRingId());
                        pageDTO.setCountry(userPageForm.getCountry());
                        pageDTO.setProfileImageUrl(profileImageUrl);
                        pageDTO.setProfileImageId(profileImageId);
                        pageDTO.setCoverImageUrl(coverImageUrl);
                        pageDTO.setCoverImageId(coverImageId);
                        pageDTO.setOwnerId(userPageForm.getOwnerId());
                        pageDTO.setPageOwner(userPageForm.getOwnerId());
                        if (userPageForm.getPageCategoryId() != null) {
                            pageDTO.setPageCategoryId(UUID.fromString(userPageForm.getPageCategoryId()));
                        }

                        RingLogger.getActivityLogger().debug("[NewsPortalUpdateAction] activistName : " + activistName + " pageDTO --> " + new Gson().toJson(pageDTO));
                        reasonCode = PageService.getInstance().updatePage(pageDTO);
                        if (reasonCode == AppConstants.NONE) {
                            target = SUCCESS;
                            request.setAttribute("message", "<span style='color: green'>Successfully Updated</span>");
                        } else {
                            target = FAILURE;
                            request.setAttribute("message", "<span style='color: red'>" + Utils.getMessageForReasonCode(reasonCode) + "</span>");
                        }
                        break;
                    default:
                        break;
                }
            }
        } catch (Exception e) {
            target = FAILURE;
            logger.error(e);
            request.setAttribute("message", "<span style='color: red'>Expception Occured</span>");
        }
        return mapping.findForward(target);
    }
}
