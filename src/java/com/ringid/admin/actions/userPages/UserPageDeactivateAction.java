/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.userPages;

import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.dto.adminAuth.LoginDTO;
import com.ringid.admin.service.PageService;
import com.ringid.admin.utils.Constants;
import com.ringid.admin.utils.Utils;
import java.util.Arrays;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.ringid.utilities.AppConstants;

/**
 *
 * @author mamun
 */
public class UserPageDeactivateAction extends BaseAction {

    Logger logger = RingLogger.getConfigPortalLogger();

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = checkAuthentication(mapping, form, request);
        String activistName;
        try {
            LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
            activistName = loginDTO.getUserName();
        } catch (Exception e) {
            target = UNAUTHORIZED;
            return mapping.findForward(target);
        }
        try {
            if (target.equals(SUCCESS)) {
                UserPageForm userPageForm = (UserPageForm) form;
                RingLogger.getActivityLogger().debug("[UserPageDeactivateAction] activistName : " + activistName + " pageIds --> " + Arrays.toString(userPageForm.getPageIds()));
                int reasonCode = PageService.getInstance().deactiveUserPages(userPageForm.getDeactivateReasons());
                if (reasonCode == AppConstants.NONE) {
                    target = SUCCESS;
                    request.setAttribute("message", "<span style='color: green'>Successfully Updated</span>");
                } else {
                    target = FAILURE;
                    request.setAttribute("message", "<span style='color: red'>" + Utils.getMessageForReasonCode(reasonCode) + "</span>");
                }
            }
        } catch (Exception e) {
            logger.error(e);
            target = FAILURE;
        }
        return mapping.findForward(target);
    }
}
