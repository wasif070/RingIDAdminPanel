/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.userPages;

import com.ringid.admin.BaseForm;
import org.apache.struts.upload.FormFile;

/**
 *
 * @author mamun
 */
public class UserPageForm extends BaseForm {

    private long id;
    private String name;
    private int discoverable;
    private int trending;
    private int type;
    private long[] pageIds;
    private String[] deactivateReasons;
    private String pageName;
    private int pageType;
    private long pageId;
    private long pageRingId;
    private String country;
    private String profileImageUrl;
    private String coverImageUrl;
    private String slogan;
    private long ownerId;
    private FormFile theFileProfileImage;
    private FormFile theFileCoverImage;
    private boolean searchByRingId;
    private String pageCategoryId;

    public String getPageCategoryId() {
        return pageCategoryId;
    }

    public void setPageCategoryId(String pageCategoryId) {
        this.pageCategoryId = pageCategoryId;
    }

    public String[] getDeactivateReasons() {
        return deactivateReasons;
    }

    public void setDeactivateReasons(String[] deactivateReasons) {
        this.deactivateReasons = deactivateReasons;
    }

    public boolean isSearchByRingId() {
        return searchByRingId;
    }

    public void setSearchByRingId(boolean searchByRingId) {
        this.searchByRingId = searchByRingId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getDiscoverable() {
        return discoverable;
    }

    public void setDiscoverable(int discoverable) {
        this.discoverable = discoverable;
    }

    public int getTrending() {
        return trending;
    }

    public void setTrending(int trending) {
        this.trending = trending;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public long[] getPageIds() {
        return pageIds;
    }

    public void setPageIds(long[] pageIds) {
        this.pageIds = pageIds;
    }

    public String getPageName() {
        return pageName;
    }

    public void setPageName(String pageName) {
        this.pageName = pageName;
    }

    public int getPageType() {
        return pageType;
    }

    public void setPageType(int pageType) {
        this.pageType = pageType;
    }

    public long getPageId() {
        return pageId;
    }

    public void setPageId(long pageId) {
        this.pageId = pageId;
    }

    public long getPageRingId() {
        return pageRingId;
    }

    public void setPageRingId(long pageRingId) {
        this.pageRingId = pageRingId;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getProfileImageUrl() {
        return profileImageUrl;
    }

    public void setProfileImageUrl(String profileImageUrl) {
        this.profileImageUrl = profileImageUrl;
    }

    public String getCoverImageUrl() {
        return coverImageUrl;
    }

    public void setCoverImageUrl(String coverImageUrl) {
        this.coverImageUrl = coverImageUrl;
    }

    public String getSlogan() {
        return slogan;
    }

    public void setSlogan(String slogan) {
        this.slogan = slogan;
    }

    public long getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(long ownerId) {
        this.ownerId = ownerId;
    }

    public FormFile getTheFileProfileImage() {
        return theFileProfileImage;
    }

    public void setTheFileProfileImage(FormFile theFileProfileImage) {
        this.theFileProfileImage = theFileProfileImage;
    }

    public FormFile getTheFileCoverImage() {
        return theFileCoverImage;
    }

    public void setTheFileCoverImage(FormFile theFileCoverImage) {
        this.theFileCoverImage = theFileCoverImage;
    }

}
