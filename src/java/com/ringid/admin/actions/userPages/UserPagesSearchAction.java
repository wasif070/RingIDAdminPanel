/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.userPages;

import com.ringid.admin.dto.UserPageDTO;
import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.dto.adminAuth.LoginDTO;
import com.ringid.admin.service.AdminService;
import com.ringid.admin.service.PageService;
import com.ringid.admin.utils.Constants;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.ringid.users.DeactiveReasonDTO;

/**
 *
 * @author mamun
 */
public class UserPagesSearchAction extends BaseAction {

    private static final Logger logger = RingLogger.getConfigPortalLogger();

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = SUCCESS;
        try {
            LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        } catch (Exception e) {
            target = UNAUTHORIZED;
            return mapping.findForward(target);
        }
        try {
            UserPageForm userPageForm = (UserPageForm) form;
            UserPageDTO userPageDTO = new UserPageDTO();
            if (userPageForm.getSearchText() != null && userPageForm.getSearchText().length() > 0) {
                userPageDTO.setSearchText(userPageForm.getSearchText().trim().toLowerCase());
            }
            if (userPageForm.getColumn() <= 0) {
                userPageForm.setColumn(Constants.COLUMN_ONE);
                userPageForm.setSort(Constants.DESC_SORT);
            }
            userPageDTO.setColumn(userPageForm.getColumn());
            userPageDTO.setSortType(userPageForm.getSort());
            userPageDTO.setPageId(userPageForm.getId());
            userPageDTO.setPageType(userPageForm.getPageType());
            List<UserPageDTO> userPageList = PageService.getInstance().getUserPageList(userPageDTO);
            List<DeactiveReasonDTO> deactiveReasonDTOs = AdminService.getInstance().getDeactivateReasonsList(null);
            request.getSession(true).setAttribute(Constants.DATA_ROWS, userPageList);
            request.getSession(true).setAttribute("deactiveReasonList", deactiveReasonDTOs);
            managePages(request, userPageForm, userPageList.size());
        } catch (Exception e) {
            logger.error("exception --> " + e);
            target = FAILURE;
        }

        return mapping.findForward(target);
    }
}
