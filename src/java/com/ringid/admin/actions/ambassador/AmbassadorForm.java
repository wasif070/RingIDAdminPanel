/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.ambassador;

import com.ringid.admin.BaseForm;
import javax.servlet.http.HttpServletRequest;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author mamun
 */
public class AmbassadorForm extends BaseForm {

    private long productPageId;
    private long[] ambassadors;
    private long userId;
    private long ringId;
    private String userName;
    private int userType;
    private String userTypeStr;
    private boolean spamUser;

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public long getRingId() {
        return ringId;
    }

    public void setRingId(long ringId) {
        this.ringId = ringId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public int getUserType() {
        return userType;
    }

    public void setUserType(int userType) {
        this.userType = userType;
    }

    public String getUserTypeStr() {
        return userTypeStr;
    }

    public void setUserTypeStr(String userTypeStr) {
        this.userTypeStr = userTypeStr;
    }

    public boolean isSpamUser() {
        return spamUser;
    }

    public void setSpamUser(boolean spamUser) {
        this.spamUser = spamUser;
    }

    public long getProductPageId() {
        return productPageId;
    }

    public void setProductPageId(long productPageId) {
        this.productPageId = productPageId;
    }

    public long[] getAmbassadors() {
        return ambassadors;
    }

    public void setAmbassadors(long[] ambassadors) {
        this.ambassadors = ambassadors;
    }

    @Override
    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        ActionErrors errors = new ActionErrors();
        return errors;
    }
}
