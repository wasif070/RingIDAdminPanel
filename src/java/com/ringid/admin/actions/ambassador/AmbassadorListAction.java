/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.ambassador;

import com.google.gson.Gson;
import com.ringid.admin.service.AmbassadorService;
import com.ringid.admin.repository.AmbassadorLoader;
import com.ringid.admin.BaseAction;
import com.ringid.admin.dto.adminAuth.LoginDTO;
import com.ringid.admin.repository.ProductPageLoader;
import com.ringid.admin.utils.Constants;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.ringid.pages.PageDTO;

/**
 *
 * @author mamun
 */
public class AmbassadorListAction extends BaseAction {

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = checkAuthentication(mapping, form, request);
        AmbassadorForm ambassadorForm = (AmbassadorForm) form;
        try {
            if (target.equals(SUCCESS)) {
                AmbassadorService scheduler = AmbassadorService.getInstance();
                if (request.getParameter("ForceReload") != null && "1".equals(request.getParameter("ForceReload"))) {
                    ProductPageLoader.getInstance().forceReload();
                    AmbassadorLoader.getInstance().forceReload();
                }
                List<PageDTO> productPageDTOs = new ArrayList<>();
                List<PageDTO> ambassadorsPageDTOs = new ArrayList<>();
                productPageDTOs = ProductPageLoader.getInstance().getAllProductPageDTOs();
                request.getSession(true).setAttribute("productPages", productPageDTOs);
                ambassadorsPageDTOs = scheduler.getAmbassadorsOfProductPage(ambassadorForm.getProductPageId());
                request.getSession(true).setAttribute(Constants.DATA_ROWS, ambassadorsPageDTOs);
                managePages(request, ambassadorForm, ambassadorsPageDTOs.size());
            }
        } catch (Exception e) {
            target = FAILURE;
        }
        return mapping.findForward(target);
    }
}
