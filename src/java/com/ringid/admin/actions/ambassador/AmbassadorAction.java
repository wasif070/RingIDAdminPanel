/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.ambassador;

import com.ringid.admin.service.AmbassadorService;
import com.ringid.admin.repository.AmbassadorLoader;
import com.google.gson.Gson;
import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.dto.adminAuth.LoginDTO;
import com.ringid.admin.repository.ProductPageLoader;
import com.ringid.admin.utils.Constants;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.ringid.pages.PageDTO;

/**
 *
 * @author mamun
 */
public class AmbassadorAction extends BaseAction {

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = checkAuthentication(mapping, form, request);
        String activistName;
        AmbassadorForm ambassadorForm = (AmbassadorForm) form;
        try {
            LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
            activistName = loginDTO.getUserName();
        } catch (Exception e) {
            target = UNAUTHORIZED;
            return mapping.findForward(target);
        }
        try {
            if (target.equals(SUCCESS)) {
                AmbassadorService scheduler = AmbassadorService.getInstance();
                List<Long> ambassadorList = new ArrayList<>();
                List<PageDTO> productPageDTOs = new ArrayList<>();
                List<Long> failedAmbassadors = new ArrayList<>();
                switch (ambassadorForm.getAction()) {
                    case Constants.ADD: {
                        if (ambassadorForm.getSubmitType() == null || ambassadorForm.getSubmitType().length() < 1) {
                            ambassadorForm.setSubmitType("BOTH");
                        }
                        if (ambassadorForm.getSubmitType().equals("USER")) {
                            ambassadorList.add(ambassadorForm.getUserId());
                        } else if (ambassadorForm.getSubmitType().equals("PAGE")) {
                            if (ambassadorForm.getAmbassadors() != null && ambassadorForm.getAmbassadors().length > 0) {
                                ambassadorList = Arrays.stream(ambassadorForm.getAmbassadors()).boxed().collect(Collectors.toList());
                            }
                        } else {
                            if (ambassadorForm.getAmbassadors() != null && ambassadorForm.getAmbassadors().length > 0) {
                                ambassadorList = Arrays.stream(ambassadorForm.getAmbassadors()).boxed().collect(Collectors.toList());
                            }
                            ambassadorList.add(ambassadorForm.getUserId());
                        }
                        RingLogger.getActivityLogger().debug("Action : AmbassadorAction[addAmbassador] activistName : " + activistName + " productPageId -> " + ambassadorForm.getProductPageId() + " ambassadorList -> " + new Gson().toJson(ambassadorList));
                        failedAmbassadors = scheduler.addAmbassador(ambassadorForm.getProductPageId(), ambassadorList);
                        if (failedAmbassadors.size() > 0) {
                            target = FAILURE;
                            request.setAttribute("message", "<span>Failed</span>");
                        } else {
                            AmbassadorLoader.getInstance().forceReload(ambassadorForm.getProductPageId());
                            request.setAttribute("message", "<span>Successful</span>");
                        }
                        break;
                    }
                    case Constants.DELETE: {
                        if (ambassadorForm.getSelectedIDs() != null && ambassadorForm.getSelectedIDs().length > 0) {
                            for (String str : ambassadorForm.getSelectedIDs()) {
                                ambassadorList.add(Long.parseLong(str));
                            }
                        }
                        RingLogger.getActivityLogger().debug("Action : AmbassadorAction[removeAmbassador] activistName : " + activistName + " productPageId -> " + ambassadorForm.getProductPageId() + " ambassadorList" + new Gson().toJson(ambassadorList));
                        failedAmbassadors = scheduler.removeAmbassador(ambassadorForm.getProductPageId(), ambassadorList);
                        if (failedAmbassadors.size() > 0) {
                            target = FAILURE;
                            request.setAttribute("message", "<span>Failed</span>");
                        } else {
                            AmbassadorLoader.getInstance().forceReload(ambassadorForm.getProductPageId());
                            request.setAttribute("message", "<span>Successful</span>");
                        }
                        break;
                    }
                    default: {
                        break;
                    }
                }
                productPageDTOs = ProductPageLoader.getInstance().getAllProductPageDTOs();
                request.getSession(true).setAttribute("productPages", productPageDTOs);
            }
        } catch (Exception e) {
            target = FAILURE;
        }
        if (target.equals(SUCCESS)) {
            return new ActionForward(mapping.findForward(target).getPath() + "?productPageId=" + ambassadorForm.getProductPageId(), true);
        }
        return mapping.findForward(target);
    }
}
