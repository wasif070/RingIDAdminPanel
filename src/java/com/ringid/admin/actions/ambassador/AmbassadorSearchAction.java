/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.ambassador;

import com.ringid.admin.BaseAction;
import com.ringid.admin.repository.ProductPageLoader;
import com.ringid.admin.service.AmbassadorService;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.ringid.pages.PageDTO;
import org.ringid.pages.ServiceDTO;
import org.ringid.users.UserBasicInfoDTO;
import org.ringid.utilities.AppConstants;

/**
 *
 * @author mamun
 */
public class AmbassadorSearchAction extends BaseAction {

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = checkAuthentication(mapping, form, request);
        AmbassadorForm ambassadorForm = (AmbassadorForm) form;
        try {
            if (target.equals(SUCCESS)) {
                AmbassadorService scheduler = AmbassadorService.getInstance();
                UserBasicInfoDTO userBasicInfoDTO = null;
                List<PageDTO> productPageDTOs = new ArrayList<>();
                List<ServiceDTO> userPageList = new ArrayList<>();
                if (ambassadorForm.getSearchText() != null && ambassadorForm.getSearchText().length() > 0) {
                    userBasicInfoDTO = scheduler.getUserBasicInfo(ambassadorForm.getSearchText().trim());
                }
                if (userBasicInfoDTO != null) {
                    ambassadorForm.setUserId(userBasicInfoDTO.getUserId());
                    ambassadorForm.setRingId(userBasicInfoDTO.getRingId());
                    ambassadorForm.setUserName(userBasicInfoDTO.getFirstName());
                    ambassadorForm.setUserType(userBasicInfoDTO.getUserType());
                    ambassadorForm.setSpamUser(userBasicInfoDTO.isSpammedUser());
                    switch (userBasicInfoDTO.getUserType()) {
                        case AppConstants.UserTypeConstants.DEFAULT_USER:
                            ambassadorForm.setUserTypeStr("Default User");
                            break;
                        case AppConstants.UserTypeConstants.SPECIAL_CONTACT:
                            ambassadorForm.setUserTypeStr("SPECIAL CONTACT");
                            break;
                        case AppConstants.UserTypeConstants.USER_PAGE:
                            ambassadorForm.setUserTypeStr("User Page");
                            break;
                        case AppConstants.UserTypeConstants.ROOM_PAGE:
                            ambassadorForm.setUserTypeStr("ROOM PAGE");
                            break;
                        case AppConstants.UserTypeConstants.DONATION_PAGE:
                            ambassadorForm.setUserTypeStr("DONATION PAGE");
                            break;
                        case AppConstants.UserTypeConstants.SPORTS_PAGE:
                            ambassadorForm.setUserTypeStr("SPORTS PAGE");
                            break;
                        case AppConstants.UserTypeConstants.CELEBRITY:
                            ambassadorForm.setUserTypeStr("Celebrity");
                            break;
                        case AppConstants.UserTypeConstants.NEWSPORTAL:
                            ambassadorForm.setUserTypeStr("NEWSPORTAL");
                            break;
                        case AppConstants.UserTypeConstants.MEDIA_PAGE:
                            ambassadorForm.setUserTypeStr("MEDIA PAGE");
                            break;
                        case AppConstants.UserTypeConstants.BUSINESS_PAGE:
                            ambassadorForm.setUserTypeStr("BUSINESS PAGE");
                            break;
                        case AppConstants.UserTypeConstants.PRODUCT_PAGE:
                            ambassadorForm.setUserTypeStr("PRODUCT PAGE");
                            break;
                        case AppConstants.UserTypeConstants.GROUP:
                            ambassadorForm.setUserTypeStr("GROUP");
                            break;
                        default:
                            ambassadorForm.setUserTypeStr("" + userBasicInfoDTO.getUserType());
                            break;
                    }
                    userPageList = scheduler.getUserPageDTOs(userBasicInfoDTO.getUserId());
                    request.getSession(true).setAttribute("userPageList", userPageList);
                }
                productPageDTOs = ProductPageLoader.getInstance().getAllProductPageDTOs();
                request.getSession(true).setAttribute("productPages", productPageDTOs);
            }
        } catch (Exception e) {
            target = FAILURE;
        }
        return mapping.findForward(target);
    }
}
