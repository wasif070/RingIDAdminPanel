/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.ringId.resetPassword;

import auth.com.utils.MyAppError;
import com.google.gson.Gson;
import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.dto.adminAuth.LoginDTO;
import com.ringid.admin.service.AdminService;
import com.ringid.admin.service.UserService;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

/**
 *
 * @author Rabby
 */
public class ResetPasswordAction extends BaseAction {

    ActionErrors err = new ActionErrors();

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = SUCCESS;
        err.clear();
        String activistName;
        try {
            LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(com.ringid.admin.utils.Constants.LOGIN_DTO);
            activistName = loginDTO.getUserName();
        } catch (Exception e) {
            target = UNAUTHORIZED;
            return mapping.findForward(target);
        }
        try {
            ResetPasswordForm resetPasswordForm = (ResetPasswordForm) form;
            ResetPasswordDTO resetPasswordDTO = new ResetPasswordDTO();
            resetPasswordDTO.setUserId(resetPasswordForm.getUserId());
            resetPasswordDTO.setPassword(resetPasswordForm.getPassword());
            RingLogger.getActivityLogger().debug("[ResetPasswordAction] activistName : " + activistName + " resetPasswordDTO --> " + new Gson().toJson(resetPasswordDTO));
            RingLogger.getConfigPortalLogger().debug("userId : " + resetPasswordForm.getUserId() + " password : " + resetPasswordForm.getPassword());
            MyAppError error = UserService.getInstance().resetPassword(resetPasswordDTO);
            if (error.getERROR_TYPE() == MyAppError.NOERROR) {
                err.add("errormsg", new ActionMessage("Reset Password successful ", false));
                request.setAttribute("errormsg", "<span style='color:green'>Reset Password successful</span>");
            } else {
                err.add("errormsg", new ActionMessage("Reset Password failed ", false));
                request.setAttribute("errormsg", "<span style='color:red'>Reset Password failed</span>");
                target = FAILURE;
            }
            saveErrors(request, err);
        } catch (Exception e) {
            target = FAILURE;
        }

        return mapping.findForward(target);
    }
}
