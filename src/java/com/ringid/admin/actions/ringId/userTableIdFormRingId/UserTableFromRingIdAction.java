/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.ringId.userTableIdFormRingId;

import com.ringid.admin.BaseAction;
import com.ringid.admin.service.AdminService;
import com.ringid.admin.service.UserService;
import com.ringid.admin.utils.Constants;
import com.ringid.admin.utils.Utils;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author Rabby
 */
public class UserTableFromRingIdAction extends BaseAction {

    List<UserTableIdFromRingIdDTO> list = new ArrayList<>();

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = SUCCESS;
        try {
            UserTableIdFromRingIdForm userTableIdFromRingIdForm = (UserTableIdFromRingIdForm) form;
            String[] ringIds = userTableIdFromRingIdForm.getRingIds().split(",");
            list = UserService.getInstance().getUsetTableIdFromRingId(ringIds);
            request.getSession(true).setAttribute(Constants.DATA_ROWS, list);
            managePages(request, userTableIdFromRingIdForm, list.size());
        } catch (Exception e) {
            target = FAILURE;
        }
        return mapping.findForward(target);
    }
}
