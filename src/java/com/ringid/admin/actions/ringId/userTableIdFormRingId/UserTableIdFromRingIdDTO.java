/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.ringId.userTableIdFormRingId;

/**
 *
 * @author Rabby
 */
public class UserTableIdFromRingIdDTO {

    private long rId;
    private long utId;

    public long getRingId() {
        return rId;
    }

    public void setRingId(long rId) {
        this.rId = rId;
    }

    public long getUserTableId() {
        return utId;
    }

    public void setUserTableId(long utId) {
        this.utId = utId;
    }

}
