/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.ringId.autoFollowPages;

import com.ringid.admin.cassandra.CassandraDAO;
import com.ringid.admin.dao.PageDAO;
import com.ringid.admin.dto.UserPageDTO;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.ringid.users.UserBasicInfoDTO;
import org.ringid.utilities.AppConstants;

/**
 *
 * @author mamun
 */
public class AutoFollowPageLoader {

    private final static long LOADING_INTERVAL = 24 * 60 * 60 * 1000;
    private long LOADING_TIME = 0;
    private List<UserPageDTO> autoFollowPageIdAndStatus;
    private Map<Long, UserPageDTO> autoFollowPageMap;

    private AutoFollowPageLoader() {
        autoFollowPageIdAndStatus = new ArrayList<>();
        autoFollowPageMap = new HashMap<>();
    }

    private static class AutoFollowPageLoaderHolder {

        private static final AutoFollowPageLoader INSTANCE = new AutoFollowPageLoader();
    }

    public static AutoFollowPageLoader getInstance() {
        return AutoFollowPageLoaderHolder.INSTANCE;
    }

    private void forceLoadData() {
        try {
            autoFollowPageIdAndStatus = PageDAO.getInstance().getAutoFollowPageStatusInfo();
        } catch (Exception e) {

        }
    }

    private void checkForReload() {
        long currentTime = System.currentTimeMillis();
        if (currentTime - LOADING_TIME > LOADING_INTERVAL) {
            LOADING_TIME = currentTime;
            forceLoadData();
        }
    }

    public synchronized void forceReload() {
        LOADING_TIME = System.currentTimeMillis();
        forceLoadData();
    }

    public List<UserPageDTO> getAutoFollowPages(UserPageDTO searchDTO) {
        checkForReload();
        List<UserPageDTO> autoFollowPageStatusInfo = new ArrayList<>();
        try {
            for (UserPageDTO dto : autoFollowPageIdAndStatus) {
                UserPageDTO sdto = new UserPageDTO();
                if (autoFollowPageMap.containsKey(dto.getPageId())) {
                    sdto = autoFollowPageMap.get(dto.getPageId());
                    sdto.setPageAutoFollow(dto.isPageAutoFollow());
                } else {
                    UserBasicInfoDTO info = CassandraDAO.getInstance().getUserBasicInfo(dto.getPageId());
                    sdto.setPageId(dto.getPageId());
                    sdto.setPageAutoFollow(dto.isPageAutoFollow());
                    sdto.setPageName(info.getFirstName());
                    sdto.setPageRingId(info.getRingId());
                    sdto.setPageType(info.getUserType());
                    sdto.setProfileImageUrl(info.getProfileImageURL());
                    switch (info.getActiveStatus()) {
                        case AppConstants.UserActiveStatus.ACTIVE:
                            sdto.setPageActiveStatus("ACTIVE");
                            break;
                        case AppConstants.UserActiveStatus.DEACTIVE:
                            sdto.setPageActiveStatus("DEACTIVE");
                            break;
                        case AppConstants.UserActiveStatus.CLOSED:
                            sdto.setPageActiveStatus("CLOSED");
                            break;
                    }
                    autoFollowPageMap.put(sdto.getPageId(), sdto);
                }
                if (searchDTO.getSearchText() != null && searchDTO.getSearchText().length() > 0 && !searchDTO.getSearchText().contains(sdto.getPageName().toLowerCase())) {
                    continue;
                }
                autoFollowPageStatusInfo.add(sdto);
            }
        } catch (Exception ex) {

        }
        return autoFollowPageStatusInfo;
    }
}
