/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.ringId.autoFollowPages;

import com.ringid.admin.BaseAction;
import com.ringid.admin.actions.userPages.UserPageForm;
import com.ringid.admin.dto.UserPageDTO;
import com.ringid.admin.service.PageService;
import com.ringid.admin.utils.Constants;
import com.ringid.admin.utils.log.RingLogger;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author mamun
 */
public class AutoFollowPageListAction extends BaseAction {

    List<UserPageDTO> list = new ArrayList<>();

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = FAILURE;
        try {
            UserPageForm appStoresForm = (UserPageForm) form;

            UserPageDTO userPageDTO = new UserPageDTO();

            if (request.getParameter("ForceReload") != null && "1".equals(request.getParameter("ForceReload"))) {
                AutoFollowPageLoader.getInstance().forceReload();
            }
            if (appStoresForm.getSearchText() != null) {
                userPageDTO.setSearchText(appStoresForm.getSearchText().trim().toLowerCase());
            }

            if (appStoresForm.getColumn() <= 0) {
                appStoresForm.setColumn(Constants.COLUMN_ONE);
                appStoresForm.setSort(Constants.DESC_SORT);
            }
            userPageDTO.setColumn(appStoresForm.getColumn());
            userPageDTO.setSortType(appStoresForm.getSort());

            list = PageService.getInstance().getAutoFollowPages(userPageDTO);
            RingLogger.getConfigPortalLogger().debug("AutoFollowPageListAction : [ TotalRecords : " + list.size() + " ]");

            request.getSession(true).setAttribute(Constants.DATA_ROWS, list);
            managePages(request, appStoresForm, list.size());
        } catch (NullPointerException e) {
            RingLogger.getConfigPortalLogger().error(e);
            target = FAILURE;
        }
        return mapping.findForward(target);
    }
}
