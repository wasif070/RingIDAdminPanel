/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.ringId.celebrity;

import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.service.CelebrityService;
import com.ringid.admin.service.UserService;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.ringid.users.UserSettingsDTO;

/**
 *
 * @author ipvision
 */
public class CelebrityMinGiftEditAction extends BaseAction {

    private static final Logger logger = RingLogger.getConfigPortalLogger();

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = SUCCESS;
        long userId;
        try {
            CelebrityForm celebrityForm = (CelebrityForm) form;
            Integer celebrityMinGift = null;
            userId = Long.parseLong(celebrityForm.getPageId());
            celebrityMinGift = CelebrityService.getInstance().getCelebrityMinGiftValue(userId);
            UserSettingsDTO userSettingsDTO = UserService.getInstance().getMySettings(userId);
            if (userSettingsDTO != null) {
                celebrityForm.setAnonymousCall(userSettingsDTO.getAnonymousCall());
                celebrityForm.setAnonymousChat(userSettingsDTO.getAnonymousChat());
            }
            if (celebrityMinGift != null) {
                celebrityForm.setGift(celebrityMinGift.toString());
            }
        } catch (Exception e) {
            logger.error("exception in [CelebrityMinGiftEditAction] --> " + e);
            target = FAILURE;
        }
        return mapping.findForward(target);
    }
}
