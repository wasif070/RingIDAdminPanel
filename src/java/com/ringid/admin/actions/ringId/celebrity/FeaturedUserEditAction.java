/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.ringId.celebrity;

import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.utils.Constants;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.Logger;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author Rabby
 */
public class FeaturedUserEditAction extends BaseAction {

    private static final Logger logger = RingLogger.getConfigPortalLogger();
    ActionErrors err = new ActionErrors();
    private final String MULTIPLE = "multiple";

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = SUCCESS;
        err.clear();
        try {
            CelebrityForm celebrityForm = (CelebrityForm) form;
            if (request.getParameter("editType") != null && request.getParameter("editType").trim().equalsIgnoreCase("multiple")) {
                target = MULTIPLE;
                List<SpecialUsersDTO> specialUsersDTOs = new ArrayList<>();
                if (celebrityForm.getSelectedIDs() != null) {
                    String[] ringIdWeightAndNameList = celebrityForm.getSelectedIDs();
                    String[] ringIdWeightAndName;
                    celebrityForm.setType(1);
                    for (String str : ringIdWeightAndNameList) {
                        ringIdWeightAndName = str.split(",");
                        if (ringIdWeightAndName.length > 2) {
                            SpecialUsersDTO dto = new SpecialUsersDTO();
                            dto.setUserId(ringIdWeightAndName[0]);
                            dto.setWeight(Integer.parseInt(ringIdWeightAndName[1]));
                            dto.setName(ringIdWeightAndName[2]);
                            specialUsersDTOs.add(dto);
                        }
                    }
                }
                request.getSession().setAttribute(Constants.DATA_ROWS, specialUsersDTOs);
                return mapping.findForward(target);
            }
            long ringId = 0;
            int weight = 0;
            if (request.getParameter("id") != null && request.getParameter("id").length() > 0) {
                ringId = Long.parseLong(request.getParameter("id"));
            }
            if (request.getParameter("weight") != null && request.getParameter("weight").length() > 0) {
                weight = Integer.parseInt(request.getParameter("weight"));
            }
            celebrityForm.setType(1);
            celebrityForm.setUserId(String.valueOf(ringId));
            celebrityForm.setWeight(weight);
        } catch (Exception e) {
            logger.error(e);
            target = FAILURE;
        }
        return mapping.findForward(target);
    }
}
