/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.ringId.celebrity;

import com.google.gson.Gson;
import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.dto.adminAuth.LoginDTO;
import com.ringid.admin.service.CelebrityService;
import com.ringid.admin.utils.Constants;
import java.util.HashMap;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.Logger;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.ringid.celebrities.CelebrityCategoryDTO;
import org.ringid.utilities.ReasonCode;

/**
 *
 * @author Rabby
 */
public class MakeCelebrityCategoryAction extends BaseAction {

    private static final Logger logger = RingLogger.getConfigPortalLogger();
    ActionErrors err = new ActionErrors();

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = SUCCESS;
        err.clear();
        String activistName;
        try {
            LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
            activistName = loginDTO.getUserName();
        } catch (Exception e) {
            target = UNAUTHORIZED;
            return mapping.findForward(target);
        }
        try {
            CelebrityForm celebrityForm = (CelebrityForm) form;
            HashMap<String, Integer> categoryMap = null;
            List<CelebrityCategoryDTO> categoyList = CelebrityService.getInstance().getCelebrityCategoriesList();
            if (celebrityForm.getSelectedIDs() != null) {
                categoryMap = new HashMap<>();
                for (String id : celebrityForm.getSelectedIDs()) {
                    for (CelebrityCategoryDTO dto : categoyList) {
                        if (String.valueOf(dto.getCategoryID()).equals(id)) {
                            categoryMap.put(dto.getCategoryName(), dto.getCategoryID());
                            break;
                        }
                    }
                }
            }
            RingLogger.getActivityLogger().debug("Action : MakeCelebrityCategoryAction[updateCelebrityCategory] activistName : " + activistName + " userId --> " + celebrityForm.getUserId() + " categoryMap --> " + new Gson().toJson(categoryMap));
            int reasonCode = CelebrityService.getInstance().updateCelebrityCategory(Long.parseLong(celebrityForm.getUserId()), categoryMap);
            if (reasonCode == ReasonCode.NONE) {
                request.getSession(true).setAttribute("message", "<span style='color:green'>Operation successful.</span>");
            } else {
                request.getSession(true).setAttribute("message", "<span style='color:red'>Operation failed.</span>");
            }
        } catch (Exception e) {
            logger.error("exception --> " + e);
            target = FAILURE;
        }
        return mapping.findForward(target);
    }
}
