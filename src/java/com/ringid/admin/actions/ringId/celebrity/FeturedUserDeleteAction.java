/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.ringId.celebrity;

import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.dto.adminAuth.LoginDTO;
import com.ringid.admin.service.CelebrityService;
import com.ringid.admin.service.UserService;
import com.ringid.admin.utils.Constants;
import com.ringid.admin.utils.Utils;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author Rabby
 */
public class FeturedUserDeleteAction extends BaseAction {

    ActionErrors err = new ActionErrors();

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = SUCCESS;
        err.clear();
        String activistName;
        try {
            LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
            activistName = loginDTO.getUserName();
        } catch (Exception e) {
            target = UNAUTHORIZED;
            return mapping.findForward(target);
        }
        try {
            long ringId = 0;
            int weight = 0;
            int verificationState = 1;
            int type = 0;
            String[] idandweight;
            if (request.getParameter("type") != null && request.getParameter("type").length() > 0) {
                type = Integer.parseInt(request.getParameter("type"));
            }
            if (request.getParameter("id") != null && request.getParameter("id").length() > 0) {
                ringId = Long.parseLong(request.getParameter("id"));
            }
            if (request.getParameter("weight") != null && request.getParameter("weight").length() > 0) {
                weight = Integer.parseInt(request.getParameter("weight"));
            }

            if (request.getParameter("verificationState") != null && request.getParameter("verificationState").length() > 0) {
                verificationState = Integer.parseInt(request.getParameter("verificationState"));
            }
            CelebrityForm celebrityForm = (CelebrityForm) form;
            celebrityForm.setType(1);
            int responseCode = -1;
            switch (type) {
                case 1:
                    switch (verificationState) {
                        case 0:
                            if (celebrityForm.getSelectedIDs() != null && celebrityForm.getSelectedIDs().length > 0) {
                                for (String str : celebrityForm.getSelectedIDs()) {
                                    idandweight = str.split(",");
                                    if (idandweight[0] != null && idandweight[1] != null) {
                                        RingLogger.getActivityLogger().debug("Action : FeturedUserDeleteAction[updateFeaturedStatus] activistName : " + activistName + " --> delete feture id --> " + ringId + " weight -- > " + weight + " verificationState --> " + false);
                                        responseCode = UserService.getInstance().updateFeaturedStatus(Long.valueOf(idandweight[0]), Integer.valueOf(idandweight[1]), false);
                                    }
                                }
                            } else {
                                RingLogger.getActivityLogger().debug("Action : FeturedUserDeleteAction[updateFeaturedStatus] activistName : " + activistName + " --> delete feture id --> " + ringId + " weight -- > " + weight + " verificationState --> " + false);
                                responseCode = UserService.getInstance().updateFeaturedStatus(ringId, weight, false);
                            }
                            break;
                        case 1:
                            if (celebrityForm.getSelectedIDs() != null && celebrityForm.getSelectedIDs().length > 0) {
                                for (String str : celebrityForm.getSelectedIDs()) {
                                    idandweight = str.split(",");
                                    if (idandweight[0] != null && idandweight[1] != null) {
                                        RingLogger.getActivityLogger().debug("Action : FeturedUserDeleteAction[updateFeaturedStatus] activistName : " + activistName + " --> delete feture id --> " + ringId + " weight -- > " + weight + " verificationState --> " + true);
                                        responseCode = UserService.getInstance().updateFeaturedStatus(Long.valueOf(idandweight[0]), Integer.valueOf(idandweight[1]), true);
                                    }
                                }
                            } else {
                                RingLogger.getActivityLogger().debug("Action : FeturedUserDeleteAction[updateFeaturedStatus] activistName : " + activistName + " --> delete feture id --> " + ringId + " weight -- > " + weight + " verificationState --> " + true);
                                responseCode = UserService.getInstance().updateFeaturedStatus(ringId, weight, true);
                            }
                            break;
                    }
                    break;
                case 2:
                    RingLogger.getActivityLogger().debug("Action : FeturedUserDeleteAction[deleteCelebrityPage] activistName : " + activistName + " --> delete feture id --> " + ringId);
                    responseCode = CelebrityService.getInstance().deleteCelebrityPage(ringId, true);
                    break;
            }
            RingLogger.getActivityLogger().debug("## responseCode --> " + responseCode);
            if (responseCode == 0) {
                request.getSession(true).setAttribute("message", "<span style='color:green'>Delete successful.</span>");
            } else {
                request.getSession(true).setAttribute("message", "<span style='color:red'>Delete failed. " + Utils.getMessageForReasonCode(responseCode) + " </span>");
            }
        } catch (Exception e) {
            RingLogger.getActivityLogger().error(e);
            RingLogger.getConfigPortalLogger().error(e);
            target = FAILURE;
        }
        return mapping.findForward(target);
    }
}
