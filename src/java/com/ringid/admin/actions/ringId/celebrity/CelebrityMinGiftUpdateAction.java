/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.ringId.celebrity;

import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.dto.adminAuth.LoginDTO;
import com.ringid.admin.service.AdminService;
import com.ringid.admin.service.CelebrityService;
import com.ringid.admin.utils.Constants;
import com.ringid.admin.utils.Utils;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.ringid.utilities.AppConstants;

/**
 *
 * @author ipvision
 */
public class CelebrityMinGiftUpdateAction extends BaseAction {

    private static final Logger logger = RingLogger.getConfigPortalLogger();

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = SUCCESS;
        String msgString = "";
        String activistName;
        try {
            LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
            activistName = loginDTO.getUserName();
        } catch (Exception e) {
            target = UNAUTHORIZED;
            return mapping.findForward(target);
        }
        try {
            CelebrityForm celebrityForm = (CelebrityForm) form;
            int responseCode = -1;

            RingLogger.getActivityLogger().debug("Action : [CelebrityMinGiftUpdateAction] activistName : " + activistName + " celebrityPageId --> " + celebrityForm.getPageId() + " giftValue -- > " + celebrityForm.getGift());
            responseCode = CelebrityService.getInstance().setCelebrityMinGiftValue(Long.valueOf(celebrityForm.getPageId()), Integer.valueOf(celebrityForm.getGift()));
            RingLogger.getActivityLogger().debug("## responseCode --> " + responseCode);
            if (responseCode == 0) {
                msgString = "<span style='color:green'> Celebrity min gift value updated </span>";
                //request.getSession(true).setAttribute("message", "<span style='color:green'> Celebrity min gift value updated </span>");
            } else {
                msgString = "<span style='color:red'> " + Utils.getMessageForReasonCode(responseCode) + " </span>";
                //request.getSession(true).setAttribute("message", "<span style='color:red'> " + Utils.getMessageForReasonCode(responseCode) + " </span>");
            }
            boolean anonymousCall = AdminService.getInstance().anonymousCallChatOnOff(AppConstants.ANONYMOUS_CALL, celebrityForm.getAnonymousCall(), Long.valueOf(celebrityForm.getPageId()));
            boolean anonymousChat = AdminService.getInstance().anonymousCallChatOnOff(AppConstants.ANONYMOUS_CHAT, celebrityForm.getAnonymousChat(), Long.valueOf(celebrityForm.getPageId()));
            if (anonymousCall == true) {
                msgString += "<br/><span style='color:green'> Anonymous Call" + (celebrityForm.getAnonymousCall() == 0 ? " Off" : " On") + " Successful </span>";
            } else {
                msgString += "<br/><span style='color:green'> Anonymous Call" + (celebrityForm.getAnonymousCall() == 0 ? " Off" : " On") + " Failed </span>";
            }
            if (anonymousChat == true) {
                msgString += "<br/><span style='color:green'> Anonymous Chat" + (celebrityForm.getAnonymousChat() == 0 ? " Off" : " On") + " Successful </span>";
            } else {
                msgString += "<br/><span style='color:green'> Anonymous Chat" + (celebrityForm.getAnonymousChat() == 0 ? " Off" : " On") + " Failed </span>";
            }
            request.getSession(true).setAttribute("message", msgString);
        } catch (Exception e) {
            RingLogger.getActivityLogger().error(e);
            RingLogger.getConfigPortalLogger().error(e);
            target = FAILURE;
        }
        return mapping.findForward(target);
    }
}
