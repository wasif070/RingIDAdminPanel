/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.ringId.celebrity;

import com.google.gson.Gson;
import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.dto.adminAuth.LoginDTO;
import com.ringid.admin.service.CelebrityService;
import com.ringid.admin.service.PageService;
import com.ringid.admin.service.UserService;
import com.ringid.admin.utils.Constants;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.Logger;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.ringid.pages.ServiceDTO;
import org.ringid.utilities.AppConstants;

/**
 *
 * @author Rabby
 */
public class CelebrityAction extends BaseAction {

    private static final Logger logger = RingLogger.getConfigPortalLogger();
    ActionErrors err = new ActionErrors();

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = SUCCESS;
        err.clear();
        String activistName;
        try {
            LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
            activistName = loginDTO.getUserName();
        } catch (Exception e) {
            target = UNAUTHORIZED;
            return mapping.findForward(target);
        }
        try {
            CelebrityForm celebrityForm = (CelebrityForm) form;
            SpecialUsersDTO celebrityDTO = new SpecialUsersDTO();
            celebrityDTO.setUserId(celebrityForm.getUserId());
            celebrityDTO.setWeight(celebrityForm.getWeight());
            int responseCode;
            switch (celebrityForm.getType()) {
                case 1:
                    List<ServiceDTO> userPageList;
                    if (celebrityForm.getSubmitType() != null && celebrityForm.getSubmitType().equals("Make Celebrity")) {
                        RingLogger.getActivityLogger().debug("Action : CelebrityAction[makeUserPageCelebrity] activistName : " + activistName + " pageIds --> " + new Gson().toJson(celebrityForm.getSelectedIDs()));
                        responseCode = CelebrityService.getInstance().makeUserPagesCelebrity(celebrityForm.getSelectedIDs());
                        if (responseCode == AppConstants.NONE) {
                            err.add("errormsg", new ActionMessage("<span style='color:green'>Make Celebrity Page successful.</span>", false));
                        } else {
                            err.add("errormsg", new ActionMessage("<span style='color:red'>Make Celebrity Page failed.(resoneCode: ) " + responseCode + "</span>", false));
                        }
                    } else {
                        long ringId = 0;
                        if (request.getParameter("ringId") != null && request.getParameter("ringId").length() > 0) {
                            ringId = Long.parseLong(request.getParameter("ringId"));
                            request.setAttribute("ringId", Long.parseLong(request.getParameter("ringId")));
                        }
                        userPageList = PageService.getInstance().getUserPageList(ringId);
                        request.getSession(true).setAttribute(Constants.DATA_ROWS, userPageList);
                    }
                    break;
                case 2:
                    RingLogger.getActivityLogger().debug("Action : CelebrityAction[makeUserFeatured] activistName : " + activistName + " userId --> " + celebrityDTO.getUserId() + " weight --> " + celebrityDTO.getWeight());
                    responseCode = UserService.getInstance().makeUserFeatured(Long.parseLong(celebrityDTO.getUserId()), celebrityDTO.getWeight(), true);
                    if (responseCode == AppConstants.NONE) {
                        err.add("errormsg", new ActionMessage("<span style='color:green'>Make Featured successful.</span>", false));
                    } else {
                        err.add("errormsg", new ActionMessage("<span style='color:red'>Make Featured failed.(resoneCode: ) " + responseCode + "</span>", false));
                    }
                    break;
                default:
                    err.add("errormsg", new ActionMessage("<span style='color:green'>Please select user type.</span>", false));
                    break;
            }
            request.setAttribute("type", celebrityForm.getType());
            saveErrors(request, err);
        } catch (Exception e) {
            logger.error("exception --> " + e);
            target = FAILURE;
        }
        return mapping.findForward(target);
    }
}
