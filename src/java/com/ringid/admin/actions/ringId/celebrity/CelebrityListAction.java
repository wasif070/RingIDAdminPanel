/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.ringId.celebrity;

import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.service.CelebrityService;
import com.ringid.admin.service.LiveStreamingService;
import com.ringid.admin.utils.Constants;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.Logger;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.ringid.users.UserDTO;

/**
 *
 * @author ipvision
 */
public class CelebrityListAction extends BaseAction {

    private static final Logger logger = RingLogger.getConfigPortalLogger();
    ActionErrors err = new ActionErrors();
    List<UserDTO> userDTOs = new ArrayList<>();

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = SUCCESS;
        err.clear();
        try {
            CelebrityForm celebrityForm = (CelebrityForm) form;
            if (celebrityForm.getType() == 0) {
                celebrityForm.setType(1);
            }
            switch (celebrityForm.getType()) {
                case 1:
                    if (celebrityForm.getColumn() <= 0) {
                        celebrityForm.setColumn(Constants.COLUMN_THREE);
                        celebrityForm.setSort(Constants.DESC_SORT);
                    }
                    userDTOs = LiveStreamingService.getInstance().getFeaturedStreamOwnersList(celebrityForm.getPageType(), celebrityForm.getSearchText(), celebrityForm.getColumn(), celebrityForm.getSort());
                    break;
                case 2:

                    if (celebrityForm.getColumn() <= 0) {
                        celebrityForm.setColumn(Constants.COLUMN_ONE);
                        celebrityForm.setSort(Constants.ASC_SORT);
                    }
                    userDTOs = CelebrityService.getInstance().getCelebrityList(celebrityForm.getSearchText(), celebrityForm.getColumn(), celebrityForm.getSort());
                    break;
                default:
                    break;
            }

            request.getSession(true).setAttribute(Constants.DATA_ROWS, userDTOs);
            managePages(request, celebrityForm, userDTOs.size());
        } catch (Exception e) {
            logger.error("exception --> " + e);
            target = FAILURE;
        }

        return mapping.findForward(target);
    }
}
