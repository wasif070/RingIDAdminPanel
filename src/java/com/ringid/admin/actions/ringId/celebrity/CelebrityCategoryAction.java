/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.ringId.celebrity;

import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.service.CelebrityService;
import com.ringid.admin.utils.Constants;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.Logger;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.ringid.celebrities.CelebrityCategoryDTO;

/**
 *
 * @author Rabby
 */
public class CelebrityCategoryAction extends BaseAction {

    private static final Logger logger = RingLogger.getConfigPortalLogger();
    ActionErrors err = new ActionErrors();

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = SUCCESS;
        int id = 10000;
        try {
            id = Integer.valueOf(request.getParameter("id"));
            CelebrityForm celebrityForm = (CelebrityForm) form;
            celebrityForm.setUserId(String.valueOf(id));
            List<CelebrityCategoryDTO> categoryList = new ArrayList<>();
            categoryList = CelebrityService.getInstance().getCelebrityCategoriesList();
            List<String> categoryListOfCelebrity = CelebrityService.getInstance().getCategoryListOfAParticularCelebrity(id);
            //dumy data
//            CelebrityCategoryDTO dto = new CelebrityCategoryDTO();
//            categoryList.add(dto);
//            categoryList.add(dto);
//            categoryList.add(dto);
//            categoryList.add(dto);
//            dto.setCategoryName("ABC");
//            
            request.getSession(true).setAttribute(Constants.DATA_ROWS, categoryList);
            request.setAttribute("categoryListOfCelebrity", categoryListOfCelebrity);
        } catch (Exception e) {
            logger.error("exception --> " + e);
            target = FAILURE;
        }
        return mapping.findForward(target);
    }
}
