/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.ringId.celebrity;

import com.google.gson.Gson;
import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.dto.adminAuth.LoginDTO;
import com.ringid.admin.service.AdminService;
import com.ringid.admin.service.CelebrityService;
import com.ringid.admin.service.UserService;
import com.ringid.admin.utils.Constants;
import java.util.HashMap;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.ringid.celebrities.CelebrityCategoryDTO;
import org.ringid.users.UserSettingsDTO;
import org.ringid.utilities.AppConstants;
import org.ringid.utilities.ReasonCode;

/**
 *
 * @author ipvision
 */
public class CelebrityUpdateInfoAction extends BaseAction {

    private static final Logger logger = RingLogger.getConfigPortalLogger();

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = SUCCESS;
        String activistName;
        CelebrityForm celebrityForm = (CelebrityForm) form;
        try {
            LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
            activistName = loginDTO.getUserName();
        } catch (Exception e) {
            target = UNAUTHORIZED;
            return mapping.findForward(target);
        }
        try {
            long id = 0L;
            int reasonCode = -1;
            List<CelebrityCategoryDTO> categoryList;
            switch (celebrityForm.getAction()) {
                case Constants.EDIT:
                    if (request.getParameter("id") != null && request.getParameter("id").length() > 0) {
                        id = Long.parseLong(request.getParameter("id"));
                        celebrityForm.setUserId(String.valueOf(id));
                        categoryList = CelebrityService.getInstance().getCelebrityCategoriesList();
                        List<String> categoryListOfCelebrity = CelebrityService.getInstance().getCategoryListOfAParticularCelebrity(id);

                        Integer celebrityMinGift = CelebrityService.getInstance().getCelebrityMinGiftValue(id);
                        if (celebrityMinGift != null) {
                            celebrityForm.setGift(String.valueOf(celebrityMinGift));
                        }
                        UserSettingsDTO userSettingsDTO = UserService.getInstance().getMySettings(id);
                        if (userSettingsDTO != null) {
                            celebrityForm.setAnonymousCall(userSettingsDTO.getAnonymousCall());
                            celebrityForm.setAnonymousChat(userSettingsDTO.getAnonymousChat());
                        }
                        request.getSession(true).setAttribute(Constants.DATA_ROWS, categoryList);
                        request.setAttribute("categoryListOfCelebrity", categoryListOfCelebrity);
                    } else {
                        target = FAILURE;
                        request.getSession(true).setAttribute("message", "<span style='color:red'> id : null</span>");
                    }
                    break;
                case Constants.UPDATE:
                    switch (celebrityForm.getType()) {
                        case 1:
                            HashMap<String, Integer> categoryMap = new HashMap<>();
                            categoryList = CelebrityService.getInstance().getCelebrityCategoriesList();
                            if (celebrityForm.getSelectedIDs() != null) {
                                for (String catId : celebrityForm.getSelectedIDs()) {
                                    for (CelebrityCategoryDTO dto : categoryList) {
                                        if (String.valueOf(dto.getCategoryID()).equals(catId)) {
                                            categoryMap.put(dto.getCategoryName(), dto.getCategoryID());
                                            break;
                                        }
                                    }
                                }
                            }
                            RingLogger.getActivityLogger().debug("Action : CelebrityUpdateInfoAction[updateCelebrityCategory] activistName : " + activistName + " userId --> " + celebrityForm.getUserId() + " categoryMap --> " + new Gson().toJson(categoryMap));
                            reasonCode = CelebrityService.getInstance().updateCelebrityCategory(Long.parseLong(celebrityForm.getUserId()), categoryMap);
                            RingLogger.getActivityLogger().debug("## reasonCode --> " + reasonCode);
                            break;
                        case 2:
                            RingLogger.getActivityLogger().debug("Action : CelebrityUpdateInfoAction[setCelebrityMinGiftValue] activistName : " + activistName + " celebrityPageId --> " + celebrityForm.getUserId() + " giftValue -- > " + celebrityForm.getGift());
                            reasonCode = CelebrityService.getInstance().setCelebrityMinGiftValue(Long.valueOf(celebrityForm.getUserId()), Integer.valueOf(celebrityForm.getGift()));
                            RingLogger.getActivityLogger().debug("## reasonCode --> " + reasonCode);
                            break;
                        case 3:
                            RingLogger.getActivityLogger().debug("Action : CelebrityUpdateInfoAction[setCelebrityMinGiftValue] activistName : " + activistName + " settingsName --> " + AppConstants.ANONYMOUS_CALL + " enable -- > " + celebrityForm.getAnonymousCall() + " userId --> " + celebrityForm.getUserId());
                            boolean anonymousCall = AdminService.getInstance().anonymousCallChatOnOff(AppConstants.ANONYMOUS_CALL, celebrityForm.getAnonymousCall(), Long.valueOf(celebrityForm.getUserId()));
                            RingLogger.getActivityLogger().debug("## reasonCode --> " + anonymousCall);
                            RingLogger.getActivityLogger().debug("Action : CelebrityUpdateInfoAction[setCelebrityMinGiftValue] activistName : " + activistName + " settingsName --> " + AppConstants.ANONYMOUS_CHAT + " enable -- > " + celebrityForm.getAnonymousChat() + " userId --> " + celebrityForm.getUserId());
                            boolean anonymousChat = AdminService.getInstance().anonymousCallChatOnOff(AppConstants.ANONYMOUS_CHAT, celebrityForm.getAnonymousChat(), Long.valueOf(celebrityForm.getUserId()));
                            RingLogger.getActivityLogger().debug("## reasonCode --> " + anonymousChat);
                            if (anonymousCall == true) {
                                reasonCode = 0;
                            }
                            if (anonymousChat == true) {
                                reasonCode = 0;
                            }
                            break;
                    }
                    if (reasonCode == ReasonCode.NONE) {
                        target = SUCCESS;
                        request.getSession(true).setAttribute("message", "<span style='color:green'>Operation successful.</span>");
                    } else {
                        target = FAILURE;
                        request.setAttribute("message", "<span style='color:red'>Operation failed.(resoneCode: ) " + reasonCode + "</span>");
                    }
                    break;
            }
        } catch (Exception e) {
            logger.error("exception --> " + e);
            target = FAILURE;
        }
        if (SUCCESS.equals(target) && celebrityForm.getAction() == Constants.UPDATE) {
            return new ActionForward(mapping.findForward(target).getPath() + "?type=2", true);
        }
        return mapping.findForward(target);
    }
}
