/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.ringId.celebrity;

import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.dto.adminAuth.LoginDTO;
import com.ringid.admin.service.UserService;
import com.ringid.admin.utils.Constants;
import com.ringid.admin.utils.Utils;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.Logger;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author Rabby
 */
public class FeaturedUserUpdateAction extends BaseAction {

    private static final Logger logger = RingLogger.getConfigPortalLogger();
    ActionErrors err = new ActionErrors();

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = SUCCESS;
        err.clear();
        try {
            LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
            CelebrityForm celebrityForm = (CelebrityForm) form;
            RingLogger.getActivityLogger().debug("[FeaturedUserUpdateAction] activistName : " + loginDTO.getUserName() + " userId --> " + celebrityForm.getUserId() + " weight --> " + celebrityForm.getWeight());
            int responseCode = UserService.getInstance().updateFeaturedUsersWeight(Long.parseLong(celebrityForm.getUserId()), celebrityForm.getWeight());
            if (responseCode == 0) {
                request.getSession(true).setAttribute("message", "<span style='color:green'>Update successful.</span>");
            } else {
                request.getSession(true).setAttribute("message", "<span style='color:red'>Update failed. " + Utils.getMessageForReasonCode(responseCode) + "</span>");
            }
        } catch (Exception e) {
            logger.error(e);
            target = FAILURE;
        }
        return mapping.findForward(target);
    }
}
