/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.ringId.updateRestrictionTime;

import com.ringid.admin.BaseForm;
import com.ringid.admin.dto.adminAuth.LoginDTO;
import com.ringid.admin.utils.Constants;
import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

/**
 *
 * @author Rabby
 */
public class UpdateRestrictionTimeForm extends BaseForm {

    private String time;
    private long day;
    private long hour;
    private long minute;
    private long secound;
    private int nameUpdateLimit;
    private int type;
    private int totalFriendLimit;
    private int pendingFriendLimit;
    private int otpRetryLimit;
    private long otpResetTimeDay;
    private long otpResetTimeHour;
    private long otpResetTimeMin;
    private long otpResetTimeSec;

    public long getOtpResetTimeDay() {
        return otpResetTimeDay;
    }

    public void setOtpResetTimeDay(long otpResetTimeDay) {
        this.otpResetTimeDay = otpResetTimeDay;
    }

    public long getOtpResetTimeHour() {
        return otpResetTimeHour;
    }

    public void setOtpResetTimeHour(long otpResetTimeHour) {
        this.otpResetTimeHour = otpResetTimeHour;
    }

    public long getOtpResetTimeMin() {
        return otpResetTimeMin;
    }

    public void setOtpResetTimeMin(long otpResetTimeMin) {
        this.otpResetTimeMin = otpResetTimeMin;
    }

    public long getOtpResetTimeSec() {
        return otpResetTimeSec;
    }

    public void setOtpResetTimeSec(long otpResetTimeSec) {
        this.otpResetTimeSec = otpResetTimeSec;
    }

    public int getOtpRetryLimit() {
        return otpRetryLimit;
    }

    public void setOtpRetryLimit(int otpRetryLimit) {
        this.otpRetryLimit = otpRetryLimit;
    }

    public int getTotalFriendLimit() {
        return totalFriendLimit;
    }

    public void setTotalFriendLimit(int totalFriendLimit) {
        this.totalFriendLimit = totalFriendLimit;
    }

    public int getPendingFriendLimit() {
        return pendingFriendLimit;
    }

    public void setPendingFriendLimit(int pendingFriendLimit) {
        this.pendingFriendLimit = pendingFriendLimit;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getNameUpdateLimit() {
        return nameUpdateLimit;
    }

    public void setNameUpdateLimit(int nameUpdateLimit) {
        this.nameUpdateLimit = nameUpdateLimit;
    }

    public long getDay() {
        return day;
    }

    public void setDay(long day) {
        this.day = day;
    }

    public long getHour() {
        return hour;
    }

    public void setHour(long hour) {
        this.hour = hour;
    }

    public long getMinute() {
        return minute;
    }

    public void setMinute(long minute) {
        this.minute = minute;
    }

    public long getSecound() {
        return secound;
    }

    public void setSecound(long secound) {
        this.secound = secound;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        ActionErrors errors = new ActionErrors();
        LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        if (loginDTO == null) {
            errors.add("auth", new ActionMessage("UnAuthorized Access"));
        } else {
            if (getSecound() < 0 || getSecound() > 60) {
                errors.add("secound", new ActionMessage("errors.invalid.required"));
            }
            if (getMinute() < 0 || getMinute() > 60) {
                errors.add("minute", new ActionMessage("errors.invalid.required"));
            }
            if (getHour() < 0 || getHour() > 24) {
                errors.add("hour", new ActionMessage("errors.invalid.required"));
            }
            if (getDay() < 0) {
                errors.add("day", new ActionMessage("errors.invalid.required"));
            }
        }
        return errors;
    }
}
