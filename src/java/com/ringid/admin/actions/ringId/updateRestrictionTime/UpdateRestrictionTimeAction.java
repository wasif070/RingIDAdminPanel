/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.ringId.updateRestrictionTime;

import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.dto.adminAuth.LoginDTO;
import com.ringid.admin.service.AdminService;
import com.ringid.admin.service.UserService;
import com.ringid.admin.utils.Utils;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author Rabby
 */
public class UpdateRestrictionTimeAction extends BaseAction {

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = SUCCESS;
        String activistName;
        try {
            LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(com.ringid.admin.utils.Constants.LOGIN_DTO);
            activistName = loginDTO.getUserName();
        } catch (Exception e) {
            target = UNAUTHORIZED;
            return mapping.findForward(target);
        }
        try {
            UpdateRestrictionTimeForm updateRestrictionTimeForm = (UpdateRestrictionTimeForm) form;
            long time = 0;
            time += updateRestrictionTimeForm.getSecound() * 1000;
            time += updateRestrictionTimeForm.getMinute() * 60 * 1000;
            time += updateRestrictionTimeForm.getHour() * 60 * 60 * 1000;
            time += updateRestrictionTimeForm.getDay() * 24 * 60 * 60 * 1000;

            long otpTime = 0;
            otpTime += updateRestrictionTimeForm.getOtpResetTimeSec() * 1000;
            otpTime += updateRestrictionTimeForm.getOtpResetTimeMin() * 60 * 1000;
            otpTime += updateRestrictionTimeForm.getOtpResetTimeHour() * 60 * 60 * 1000;
            otpTime += updateRestrictionTimeForm.getOtpResetTimeDay() * 24 * 60 * 60 * 1000;
            int result = 0;
            switch (updateRestrictionTimeForm.getType()) {
                case 1: {
                    RingLogger.getActivityLogger().debug("[UpdateRestrictionTimeAction] activistName : " + activistName + " time: " + time + " type: " + updateRestrictionTimeForm.getType());
                    result = AdminService.getInstance().updateRestrictionTime(time);
                    if (result == 0) {
                        request.setAttribute("message", "<span style='color: green'>Restriction time updated successfully</span>");
                    } else {
                        request.setAttribute("message", "<span style='color: red'>" + Utils.getMessageForReasonCode(result) + "</span>");
                    }
                    break;
                }
                case 2: {
                    RingLogger.getActivityLogger().debug("[UpdateRestrictionTimeAction] activistName : " + activistName + " nameUpdateLimit: " + updateRestrictionTimeForm.getNameUpdateLimit() + " type: " + updateRestrictionTimeForm.getType());
                    result = AdminService.getInstance().updateNameUpdateLimit(updateRestrictionTimeForm.getNameUpdateLimit());
                    if (result == 0) {
                        request.setAttribute("message", "<span style='color: green'>NameUpdate Limit updated successfully.</span>");
                    } else {
                        request.setAttribute("message", "<span style='color: red'>" + Utils.getMessageForReasonCode(result) + "</span>");
                    }
                    break;
                }
                case 3: {
                    RingLogger.getActivityLogger().debug("[UpdateRestrictionTimeAction] activistName : " + activistName + " totalFriendLimit: " + updateRestrictionTimeForm.getTotalFriendLimit() + " type: " + updateRestrictionTimeForm.getType());
                    result = UserService.getInstance().updateTotalFriendsLimit(updateRestrictionTimeForm.getTotalFriendLimit());
                    if (result == 0) {
                        request.setAttribute("message", "<span style='color: green'>Total Friend Limit updated successfully.</span>");
                    } else {
                        request.setAttribute("message", "<span style='color: red'>" + Utils.getMessageForReasonCode(result) + "</span>");
                    }
                    break;
                }
                case 4: {
                    RingLogger.getActivityLogger().debug("[UpdateRestrictionTimeAction] activistName : " + activistName + " pendingFriendLimit: " + updateRestrictionTimeForm.getPendingFriendLimit() + " type: " + updateRestrictionTimeForm.getType());
                    result = UserService.getInstance().updatePendingFriendsLimit(updateRestrictionTimeForm.getPendingFriendLimit());
                    if (result == 0) {
                        request.setAttribute("message", "<span style='color: green'>Pending Friend Limit updated successfully.</span>");
                    } else {
                        request.setAttribute("message", "<span style='color: red'>" + Utils.getMessageForReasonCode(result) + "</span>");
                    }
                    break;
                }
                case 5: {
                    RingLogger.getActivityLogger().debug("UpdateRestrictionTimeAction[updateOTPRetryLimit] activistName : " + activistName + " otpRetryLimit: " + updateRestrictionTimeForm.getOtpRetryLimit() + " type: " + updateRestrictionTimeForm.getType());
                    result = AdminService.getInstance().updateOTPRetryLimit(updateRestrictionTimeForm.getOtpRetryLimit());
                    if (result == 0) {
                        request.setAttribute("message", "<span style='color: green'>OTP Retry Limit updated successfully.</span>");
                    } else {
                        request.setAttribute("message", "<span style='color: red'>" + Utils.getMessageForReasonCode(result) + "</span>");
                    }
                    break;
                }
                case 6: {
                    RingLogger.getActivityLogger().debug("UpdateRestrictionTimeAction[updateOTPRetryResetTime] activistName : " + activistName + " otpRetryLimitResetTime: " + otpTime + " type: " + updateRestrictionTimeForm.getType());
                    result = AdminService.getInstance().updateOTPRetryResetTime(otpTime);
                    if (result == 0) {
                        request.setAttribute("message", "<span style='color: green'>OTP Retry Limit Reset Time updated successfully.</span>");
                    } else {
                        request.setAttribute("message", "<span style='color: red'>" + Utils.getMessageForReasonCode(result) + "</span>");
                    }
                    break;
                }
                default:
                    long currentRestictionTime = AdminService.getInstance().getDefaultRestrictedTime();
                    RingLogger.getConfigPortalLogger().debug("currentRestrictionTime: " + currentRestictionTime);
                    updateRestrictionTimeForm.setDay(currentRestictionTime / (24 * 60 * 60 * 1000));
                    currentRestictionTime %= (24 * 60 * 60 * 1000);
                    updateRestrictionTimeForm.setHour(currentRestictionTime / (60 * 60 * 1000));
                    currentRestictionTime %= (60 * 60 * 1000);
                    updateRestrictionTimeForm.setMinute(currentRestictionTime / (60 * 1000));
                    currentRestictionTime %= (60 * 1000);
                    updateRestrictionTimeForm.setSecound(currentRestictionTime / (1000));
                    RingLogger.getConfigPortalLogger().debug("day: " + updateRestrictionTimeForm.getDay() + " hour: " + updateRestrictionTimeForm.getHour() + " minute: " + updateRestrictionTimeForm.getMinute() + " secound: " + updateRestrictionTimeForm.getSecound());
                    int currentNameUpdateTime = AdminService.getInstance().getCurrentNameUpdateLimit();
                    updateRestrictionTimeForm.setNameUpdateLimit(currentNameUpdateTime);
                    int totalFriedlimit = UserService.getInstance().getTotalFriendLimit();
                    updateRestrictionTimeForm.setTotalFriendLimit(totalFriedlimit);
                    int pendingFriendLimit = UserService.getInstance().getPendingFriendLimit();
                    updateRestrictionTimeForm.setPendingFriendLimit(pendingFriendLimit);
                    int otpRetrylimit = AdminService.getInstance().getOTPRetryLimit();
                    updateRestrictionTimeForm.setOtpRetryLimit(otpRetrylimit);
                    long otpRetryLimitResetTime = AdminService.getInstance().getOTPRetryLimitResetTime();
                    updateRestrictionTimeForm.setOtpResetTimeDay(otpRetryLimitResetTime / (24 * 60 * 60 * 1000));
                    otpRetryLimitResetTime %= (24 * 60 * 60 * 1000);
                    updateRestrictionTimeForm.setOtpResetTimeHour(otpRetryLimitResetTime / (60 * 60 * 1000));
                    otpRetryLimitResetTime %= (60 * 60 * 1000);
                    updateRestrictionTimeForm.setOtpResetTimeMin(otpRetryLimitResetTime / (60 * 1000));
                    otpRetryLimitResetTime %= (60 * 1000);
                    updateRestrictionTimeForm.setOtpResetTimeSec(otpRetryLimitResetTime / (1000));
                    break;
            }
            request.setAttribute("type", updateRestrictionTimeForm.getType());
        } catch (Exception e) {
            target = FAILURE;
        }
        return mapping.findForward(target);
    }
}
