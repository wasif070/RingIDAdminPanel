/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.ringId.virtualRingId;

import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.dto.adminAuth.LoginDTO;
import com.ringid.admin.service.AdminService;
import com.ringid.admin.utils.MyAppError;
import java.text.SimpleDateFormat;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author mamun
 */
public class VitualRingIdAction extends BaseAction {

    Logger logger = RingLogger.getConfigPortalLogger();

    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = SUCCESS;
        String activistName;
        try {
            LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(com.ringid.admin.utils.Constants.LOGIN_DTO);
            activistName = loginDTO.getUserName();
        } catch (Exception e) {
            target = UNAUTHORIZED;
            return mapping.findForward(target);
        }
        try {
            VirtualRingIdForm virtualRingIdForm = (VirtualRingIdForm) form;
            if (virtualRingIdForm.getSubmitType() != null && virtualRingIdForm.getSubmitType().length() > 0) {
                switch (virtualRingIdForm.getSubmitType()) {
                    case "delete":
                        MyAppError error = AdminService.getInstance().deleteAllVirtualRingIdBeforeDate(sdf.parse(virtualRingIdForm.getSendDate()).getTime());
                        if (error.getERROR_TYPE() == MyAppError.NOERROR) {
                            request.setAttribute("message", "<span style='color: green'>Successfully Delete</span>");
                        } else {
                            request.setAttribute("message", "<span style='color: red'>Failed to Delete</span>");
                        }
                        break;
                    default:
                        break;
                }
            }
            int count = AdminService.getInstance().getAllVirtualRingIdCountBeforeDate(sdf.parse(virtualRingIdForm.getSendDate()).getTime());
            virtualRingIdForm.setCount(count);
        } catch (Exception e) {
            target = FAILURE;
        }

        return mapping.findForward(target);
    }
}
