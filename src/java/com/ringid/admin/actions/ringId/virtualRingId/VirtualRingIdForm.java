/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.ringId.virtualRingId;

import com.ringid.admin.BaseForm;
import com.ringid.admin.dto.adminAuth.LoginDTO;
import com.ringid.admin.utils.Constants;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

/**
 *
 * @author mamun
 */
public class VirtualRingIdForm extends BaseForm {

    private String sendDate;
    private int count;
    private SimpleDateFormat sdf;

    public VirtualRingIdForm() {
        sdf = new SimpleDateFormat("yyyy-MM-dd");
    }

    public String getSendDate() {
        return sendDate;
    }

    public void setSendDate(String sendDate) {
        this.sendDate = sendDate;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    @Override
    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        ActionErrors errors = new ActionErrors();
        LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        if (loginDTO == null) {
            errors.add("auth", new ActionMessage("UnAuthorized Access"));
        } else {
            if (getSendDate() == null || getSendDate().length() <= 0) {
                setSendDate(sdf.format(new Date()));
            }
            if (getSubmitType() != null && getSubmitType().equals("delete")) {
                long currentTime = 0L, sendTime = 0L;
                try {
                    currentTime = sdf.parse(sdf.format(new Date())).getTime();
                    sendTime = sdf.parse(getSendDate()).getTime();
                } catch (ParseException ex) {

                }
                if (currentTime - sendTime < Constants.SEVEN_DAY) {
                    errors.add("sendDate", new ActionMessage("Sorry. Can not delete. The date must be 7 days ago from current date."));
                }
            }
        }
        return errors;
    }
}
