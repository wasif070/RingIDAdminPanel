/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.ringId.newsfeed;

import com.ringid.admin.BaseAction;
import com.ringid.admin.cassandra.CassandraDAO;
import com.ringid.admin.service.NewsFeedService;
import com.ringid.admin.service.UserService;
import com.ringid.admin.utils.Constants;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.ringid.newsfeeds.FeedDTO;
import org.ringid.users.UserBasicInfoDTO;

/**
 *
 * @author mamun
 */
public class NewsFeedListAction extends BaseAction {

    List<FeedDTO> feedDTOs = new ArrayList<>();

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = SUCCESS;
        try {
            NewsFeedForm newsFeedForm = (NewsFeedForm) form;
            int pageNo = 1;
            if (newsFeedForm.getPageNo() > 0) {
                pageNo = newsFeedForm.getPageNo();
            }
            int start = (pageNo - 1) * newsFeedForm.getRecordPerPage();
            long userId = newsFeedForm.getId();
            long tempId = CassandraDAO.getInstance().getUserTableId(userId);
            if (tempId > 0) {
                userId = tempId;
            }
            feedDTOs = NewsFeedService.getInstance().getFeedList(userId, start, newsFeedForm.getRecordPerPage(), newsFeedForm.getAction());
            UserBasicInfoDTO userBasicInfoDTO = UserService.getInstance().getUserBasicInfo(userId);
            request.getSession(true).setAttribute("userBasicInfoDTO", userBasicInfoDTO);
            HashMap<Long, UserBasicInfoDTO> userBasicInfoDTOHashMap = UserService.getInstance().getUserBasicInfoHashMap(feedDTOs);
            request.getSession(true).setAttribute("userBasicInfoDTOHashMap", userBasicInfoDTOHashMap);

            newsFeedForm.setTotalDataSize(feedDTOs.size());
            newsFeedForm.setUserId(feedDTOs.get(0).getWallOwnerId());
            request.getSession().setAttribute(Constants.DATA_ROWS, feedDTOs);
            managePages(request, newsFeedForm, feedDTOs.size());
        } catch (Exception e) {
            e.printStackTrace();
            target = FAILURE;
        }
        return mapping.findForward(target);
    }
}
