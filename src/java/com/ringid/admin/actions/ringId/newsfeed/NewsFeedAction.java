/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.ringId.newsfeed;

import com.ringid.admin.BaseAction;
import com.ringid.admin.service.NewsFeedService;
import com.ringid.admin.service.UserService;
import com.ringid.admin.utils.ReasonCode;
import com.ringid.admin.utils.Utils;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.ringid.users.UserBasicInfoDTO;
import org.ringid.utilities.AppConstants;

/**
 *
 * @author mamun
 */
public class NewsFeedAction extends BaseAction {

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = SUCCESS;
        int reasonCode = -1;
        NewsFeedForm newsFeedForm = (NewsFeedForm) form;
        try {
            UserBasicInfoDTO userBasicInfoDTO = UserService.getInstance().getUserBasicInfo(newsFeedForm.getUserId());

            if (userBasicInfoDTO != null && (userBasicInfoDTO.getUserType() == AppConstants.UserTypeConstants.CELEBRITY || userBasicInfoDTO.getUserType() == AppConstants.UserTypeConstants.SPORTS_PAGE)) {
                switch (userBasicInfoDTO.getUserType()) {
                    case AppConstants.UserTypeConstants.CELEBRITY:
                        newsFeedForm.setFeedType(AppConstants.HighlightedFeedType.CELEBRITY);
                        break;
                    case AppConstants.UserTypeConstants.SPORTS_PAGE:
                        newsFeedForm.setFeedType(AppConstants.HighlightedFeedType.SPORTS);
                        break;
                }
                reasonCode = NewsFeedService.getInstance().addFeedInHighlights(newsFeedForm.getFeedType(), newsFeedForm.getSelectedIDs());
                if (reasonCode == ReasonCode.NONE) {
                    request.setAttribute("message", "<span style='color:green;'> Operaion Successfull </span>");
                } else {
                    request.setAttribute("message", "<span style='color:red;'> Operaion Failed " + Utils.getMessageForReasonCode(reasonCode) + " </span>");
                }
            } else {
                request.setAttribute("message", "<span style='color:red;'> Operation Failed (User is not celebrity or sports page)  </span>");
            }

        } catch (Exception e) {
            target = FAILURE;
        }
//        if (SUCCESS.equals(target)) {
//            return new ActionForward(mapping.findForward(target).getPath() + "?searchByRingId=" + newsFeedForm.isSearchByRingId() + "&id=" + newsFeedForm.getId(), true);
//        }
        return mapping.findForward(target);
    }
}
