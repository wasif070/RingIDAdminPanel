/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.ringId.feedHighlights;

import com.ringid.admin.BaseAction;
import com.ringid.admin.service.NewsFeedService;
import com.ringid.admin.utils.ReasonCode;
import com.ringid.admin.utils.Utils;
import java.util.UUID;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author mamun
 */
public class FeedHighlightDeleteAction extends BaseAction {

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = SUCCESS;
        int reasonCode = -1;
        try {
            FeedHighlightForm feedHighlightForm = (FeedHighlightForm) form;

            reasonCode = NewsFeedService.getInstance().removeFeedFromHighlights(UUID.fromString(feedHighlightForm.getFeedId()));
            if (reasonCode == ReasonCode.NONE) {
                request.setAttribute("message", "<span style='color:green;'> Operaion Successfull </span>");
            } else {
                request.setAttribute("message", "<span style='color:red;'> Operaion Failed " + Utils.getMessageForReasonCode(reasonCode) + " </span>");
            }
        } catch (Exception e) {
            e.printStackTrace();
            target = FAILURE;
        }
        return mapping.findForward(target);
    }
}
