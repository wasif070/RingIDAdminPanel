/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.ringId.feedHighlights;

import com.ringid.admin.BaseAction;
import com.ringid.admin.service.NewsFeedService;
import com.ringid.admin.utils.Constants;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.ringid.newsfeeds.HighlightedFeedDTO;
import org.ringid.utilities.AppConstants;

/**
 *
 * @author mamun
 */
public class FeedHighlightListAction extends BaseAction {

    List<HighlightedFeedDTO> highlightedFeedDTOs = new ArrayList<>();

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = SUCCESS;
        try {
            FeedHighlightForm feedHighlightForm = (FeedHighlightForm) form;

            if (feedHighlightForm.getFeedType() <= 0) {
                feedHighlightForm.setFeedType(AppConstants.HighlightedFeedType.CELEBRITY);
            }
            highlightedFeedDTOs = NewsFeedService.getInstance().getHighlightedFeeds(feedHighlightForm.getFeedType());

            feedHighlightForm.setTotalDataSize(highlightedFeedDTOs.size());
            request.getSession().setAttribute(Constants.DATA_ROWS, highlightedFeedDTOs);
            managePages(request, feedHighlightForm, highlightedFeedDTOs.size());
        } catch (Exception e) {
            e.printStackTrace();
            target = FAILURE;
        }
        return mapping.findForward(target);
    }
}
