/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.ringId.celebritySchedule;

import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.service.AdminService;
import com.ringid.admin.service.LiveStreamingService;
import com.ringid.admin.utils.Constants;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.ringid.livestream.StreamingDTO;

/**
 *
 * @author ipvision
 */
public class CelebrityScheduleListAction extends BaseAction {

    private static final Logger logger = RingLogger.getConfigPortalLogger();
    List<StreamingDTO> list = new ArrayList<>();

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = SUCCESS;
        try {
            CelebrityScheduleForm celebrityForm = (CelebrityScheduleForm) form;
            if (request.getParameter("ForceReload") != null && "1".equals(request.getParameter("ForceReload"))) {
                CelebrityScheduleLoader.getInstance().forceReload();
            }
            if (celebrityForm.getPageId() > 0 || (celebrityForm.getPageName() != null && celebrityForm.getPageName().length() > 0)) {
                list = LiveStreamingService.getInstance().getCelebrityLiveSchedule(celebrityForm.getPageId(), celebrityForm.getPageName());
            } else {
                list = LiveStreamingService.getInstance().getAllCelebrityLiveSchedule();
            }
            request.getSession(true).setAttribute(Constants.DATA_ROWS, list);
            managePages(request, celebrityForm, list.size());
        } catch (Exception e) {
            logger.error("exception --> " + e);
            target = FAILURE;
        }

        return mapping.findForward(target);
    }
}
