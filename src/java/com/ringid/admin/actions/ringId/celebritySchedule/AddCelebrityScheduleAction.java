/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.ringId.celebritySchedule;

import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.dto.adminAuth.LoginDTO;
import com.ringid.admin.service.AdminService;
import com.ringid.admin.service.LiveStreamingService;
import com.ringid.admin.utils.Constants;
import com.ringid.admin.utils.TimeUUID;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.ringid.utilities.AppConstants;

/**
 *
 * @author ipvision
 */
public class AddCelebrityScheduleAction extends BaseAction {

    private static final Logger logger = RingLogger.getConfigPortalLogger();

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = SUCCESS;
        String activistName;
        try {
            LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
            activistName = loginDTO.getUserName();
        } catch (Exception e) {
            target = UNAUTHORIZED;
            return mapping.findForward(target);
        }
        try {
            CelebrityScheduleForm celebrityForm = (CelebrityScheduleForm) form;
            long liveTime = getLiveTime(celebrityForm.getLiveTime());
            RingLogger.getActivityLogger().debug("[AddCelebrityScheduleAction] activistName : " + activistName + "pageId --> " + celebrityForm.getPageId() + " usertype --> " + AppConstants.UserTypeConstants.CELEBRITY + " livetime --> " + liveTime);
            UUID liveTimeUUID = TimeUUID.createUUID(liveTime);
            int reasonCode = LiveStreamingService.getInstance().addCelebrityLiveSchedule(celebrityForm.getPageId(), AppConstants.UserTypeConstants.CELEBRITY, celebrityForm.getDonationPageId(), liveTime, liveTimeUUID, celebrityForm.getLiveScheduleSerial());
            if (reasonCode == AppConstants.NONE) {
                CelebrityScheduleLoader.getInstance().forceReload();
            } else {
                target = FAILURE;
            }
            request.setAttribute("successMessage", reasonCode);
        } catch (Exception e) {
            logger.error("exception --> " + e);
            target = FAILURE;
        }

        return mapping.findForward(target);
    }

    private long getLiveTime(String liveTime) throws ParseException {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        Date date = dateFormat.parse(liveTime);
        return date.getTime();
    }
}
