/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.ringId.celebritySchedule;

import com.ringid.admin.cassandra.CassandraDAO;
import com.ringid.admin.utils.log.RingLogger;
import java.util.ArrayList;
import java.util.List;
import org.apache.logging.log4j.Logger;
import org.ringid.livestream.StreamingDTO;

/**
 *
 * @author Rabby
 */
public class CelebrityScheduleLoader {

    private final static long LOADING_INTERVAL = 60 * 60 * 1000;
    private long loadingTime = 0;
    private List<StreamingDTO> liveScheduleList;
    private static CelebrityScheduleLoader loader = null;
    static Logger logger = RingLogger.getConfigPortalLogger();

    public static CelebrityScheduleLoader getInstance() {
        if (loader == null) {
            createLoader();
        }
        return loader;
    }

    private synchronized static void createLoader() {
        if (loader == null) {
            loader = new CelebrityScheduleLoader();
        }
    }

    private void forceLoadData() {
        liveScheduleList = new ArrayList<>();
        try {
            liveScheduleList = CassandraDAO.getInstance().getAllLiveSchedule();
            logger.debug("liveScheduleList " + "::" + liveScheduleList.size());

        } catch (Exception e) {
            logger.debug("CelebrityScheduleLoader Exception...", e);
        }
    }

    private void checkForReload() {
        long currentTime = System.currentTimeMillis();
        if (currentTime - loadingTime > LOADING_INTERVAL) {
            loadingTime = currentTime;
            forceLoadData();
        }
    }

    public synchronized void forceReload() {
        loadingTime = System.currentTimeMillis();
        forceLoadData();
    }

    public synchronized List<StreamingDTO> getCelebrityLiveScheduleList() {
        checkForReload();
        List<StreamingDTO> celebriyLiveScheduleList = liveScheduleList;
        logger.debug("celebriyLiveScheduleList Size: " + celebriyLiveScheduleList.size());
        return celebriyLiveScheduleList;
    }

    public synchronized List<StreamingDTO> getCelebrityLiveScheduleList(StreamingDTO sdto) {
        checkForReload();
        List<StreamingDTO> celebriyLiveScheduleList = new ArrayList<>();
        for (StreamingDTO dto : liveScheduleList) {
            if (sdto.getName() != null && sdto.getName().length() > 0) {
                long pageTableId = 0;
                try {
                    pageTableId = Long.parseLong(sdto.getName().trim());
                } catch (Exception e) {
                    pageTableId = 0;
                }
                if ((pageTableId != 0 && pageTableId == dto.getUserTableId()) || (dto.getName().toLowerCase().contains(sdto.getName().toLowerCase()))) {
                    celebriyLiveScheduleList.add(dto);
                }
            } else {
                celebriyLiveScheduleList.add(dto);
            }
        }
        logger.debug("celebriyLiveScheduleList Size: " + celebriyLiveScheduleList.size());
        return celebriyLiveScheduleList;
    }
}
