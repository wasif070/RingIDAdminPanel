/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.ringId.celebritySchedule;

import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.dto.adminAuth.LoginDTO;
import com.ringid.admin.service.AdminService;
import com.ringid.admin.service.LiveStreamingService;
import com.ringid.admin.utils.Constants;
import com.ringid.admin.utils.TimeUUID;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.ringid.utilities.AppConstants;

/**
 *
 * @author ipvision
 */
public class UpdateCelebrityScheduleAction extends BaseAction {

    private static final Logger logger = RingLogger.getConfigPortalLogger();

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = SUCCESS;
        String activistName;
        int reasonCode = -1;
        try {
            LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
            activistName = loginDTO.getUserName();
        } catch (Exception e) {
            target = UNAUTHORIZED;
            return mapping.findForward(target);
        }
        try {
            CelebrityScheduleForm celebrityScheduleForm = (CelebrityScheduleForm) form;
            logger.debug("liveTime -->" + celebrityScheduleForm.getLiveTime() + " newLiveTime --> " + celebrityScheduleForm.getNewLiveTime());
            long curLivetime = Long.parseLong(celebrityScheduleForm.getLiveTime());
            long newLiveTime = 0L;
            UUID newLiveTimeUUID = null;
//            UserBasicInfoDTO userBasicInfoDTO = AdminService.getInstance().getUserBasicInfo(celebrityScheduleForm.getPageId());
            if ((celebrityScheduleForm.getNewLiveTime() != null && celebrityScheduleForm.getNewLiveTime().length() > 0) || celebrityScheduleForm.getDonationPageId() != celebrityScheduleForm.getOldDonationPageId()) {
                newLiveTime = (celebrityScheduleForm.getNewLiveTime() != null && celebrityScheduleForm.getNewLiveTime().length() > 0) ? getTime(celebrityScheduleForm.getNewLiveTime()) : curLivetime;
                newLiveTimeUUID = TimeUUID.createUUID(newLiveTime);
                RingLogger.getActivityLogger().debug("[UpdateCelebrityScheduleAction] activistName : " + activistName + " pageId --> " + celebrityScheduleForm.getPageId() + " currentLiveTime --> " + curLivetime + " newLivetime --> " + newLiveTime + " donationPageId --> " + celebrityScheduleForm.getDonationPageId());
                reasonCode = LiveStreamingService.getInstance().editCelebrityLiveSchedule(celebrityScheduleForm.getPageId(), AppConstants.UserTypeConstants.CELEBRITY, curLivetime, newLiveTime, newLiveTimeUUID, celebrityScheduleForm.getDonationPageId());
                if (reasonCode == AppConstants.NONE) {
                    CelebrityScheduleLoader.getInstance().forceReload();
                } else {
                    target = FAILURE;
                }
            }
            if (celebrityScheduleForm.getLiveScheduleSerial() > 0) {
                int result = -1;
                result = LiveStreamingService.getInstance().updateLiveScheduleSerial(celebrityScheduleForm.getPageId(), AppConstants.UserTypeConstants.CELEBRITY, (reasonCode == AppConstants.NONE) ? newLiveTime : curLivetime, celebrityScheduleForm.getLiveScheduleSerial());
                if (result == AppConstants.NONE) {
                    CelebrityScheduleLoader.getInstance().forceReload();
                } else {
                    target = FAILURE;
                }
            }
            request.setAttribute("successMessage", reasonCode);
        } catch (Exception e) {
            logger.error("exception --> " + e);
            target = FAILURE;
        }

        return mapping.findForward(target);
    }

    private long getTime(String liveTime) throws ParseException {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        Date date = dateFormat.parse(liveTime);
        return date.getTime();
    }
}
