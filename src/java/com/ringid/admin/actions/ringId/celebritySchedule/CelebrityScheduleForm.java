/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.ringId.celebritySchedule;

import com.ringid.admin.BaseForm;

/**
 *
 * @author ipvision
 */
public class CelebrityScheduleForm extends BaseForm {

    private long pageId;
    private String newLiveTime;
    private String liveTime;
    private String pageName;
    private String currentLiveTime;
    private int liveScheduleSerial;
    private int userType;
    private long donationPageId;
    private long oldDonationPageId;

    public long getOldDonationPageId() {
        return oldDonationPageId;
    }

    public void setOldDonationPageId(long oldDonationPageId) {
        this.oldDonationPageId = oldDonationPageId;
    }

    public long getDonationPageId() {
        return donationPageId;
    }

    public void setDonationPageId(long donationPageId) {
        this.donationPageId = donationPageId;
    }

    public int getUserType() {
        return userType;
    }

    public void setUserType(int userType) {
        this.userType = userType;
    }

    public int getLiveScheduleSerial() {
        return liveScheduleSerial;
    }

    public void setLiveScheduleSerial(int liveScheduleSerial) {
        this.liveScheduleSerial = liveScheduleSerial;
    }

    public String getCurrentLiveTime() {
        return currentLiveTime;
    }

    public void setCurrentLiveTime(String currentLiveTime) {
        this.currentLiveTime = currentLiveTime;
    }

    public String getPageName() {
        return pageName;
    }

    public void setPageName(String pageName) {
        this.pageName = pageName;
    }

    public long getPageId() {
        return pageId;
    }

    public void setPageId(long pageId) {
        this.pageId = pageId;
    }

    public String getNewLiveTime() {
        return newLiveTime;
    }

    public void setNewLiveTime(String newLiveTime) {
        this.newLiveTime = newLiveTime;
    }

    public String getLiveTime() {
        return liveTime;
    }

    public void setLiveTime(String liveTime) {
        this.liveTime = liveTime;
    }

}
