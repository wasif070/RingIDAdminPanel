/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.ringId.celebritySchedule;

/**
 *
 * @author mamun
 */
public class LiveToChannelDTO {

    private long pageId;
    private String channelId;
    private String pageName;
    private String channelName;
    private long liveTime;
    private String liveTimeStr;
    private int liveNow;
    private String title;
    private String description;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getLiveNow() {
        return liveNow;
    }

    public void setLiveNow(int liveNow) {
        this.liveNow = liveNow;
    }

    public long getLiveTime() {
        return liveTime;
    }

    public void setLiveTime(long liveTime) {
        this.liveTime = liveTime;
    }

    public String getLiveTimeStr() {
        return liveTimeStr;
    }

    public void setLiveTimeStr(String liveTimeStr) {
        this.liveTimeStr = liveTimeStr;
    }

    public String getPageName() {
        return pageName;
    }

    public void setPageName(String pageName) {
        this.pageName = pageName;
    }

    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    public long getPageId() {
        return pageId;
    }

    public void setPageId(long pageId) {
        this.pageId = pageId;
    }

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

}
