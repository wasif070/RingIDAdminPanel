/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.ringId.celebritySchedule;

import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.dto.adminAuth.LoginDTO;
import com.ringid.admin.service.AdminService;
import com.ringid.admin.service.LiveStreamingService;
import com.ringid.admin.utils.Constants;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.ringid.utilities.AppConstants;

/**
 *
 * @author ipvision
 */
public class DeleteCelebrityScheduleAction extends BaseAction {

    private static final Logger logger = RingLogger.getConfigPortalLogger();

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = SUCCESS;
        String activistName;
        try {
            LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
            activistName = loginDTO.getUserName();
        } catch (Exception e) {
            target = UNAUTHORIZED;
            return mapping.findForward(target);
        }
        try {
            long pageId = 0;
            long liveTime = 0;
            if (request.getParameter("id") != null && request.getParameter("id").length() > 0) {
                pageId = Long.parseLong(request.getParameter("id"));
            }

            if (request.getParameter("liveTime") != null && request.getParameter("liveTime").length() > 0) {
                liveTime = Long.parseLong(request.getParameter("liveTime"));
            }
//            UserBasicInfoDTO userBasicInfoDTO = AdminService.getInstance().getUserBasicInfo(pageId);
            RingLogger.getActivityLogger().debug("[DeleteCelebrityScheduleAction] activistName : " + activistName + " pageId --> " + pageId + " livetime --> " + liveTime);
            int reasonCode = LiveStreamingService.getInstance().deleteCelebrityLiveSchedule(pageId, AppConstants.UserTypeConstants.CELEBRITY, liveTime);
            if (reasonCode == AppConstants.NONE) {
                CelebrityScheduleLoader.getInstance().forceReload();
            } else {
                target = FAILURE;
            }
            reasonCode = LiveStreamingService.getInstance().deleteLiveToChannelInfo(pageId);
            if (reasonCode == 0) {
                notifyAuthServers(com.ringid.admin.utils.Constants.RELOAD_LIVE_TO_CHANNEL, null);
            }
        } catch (Exception e) {
            logger.error("exception --> " + e);
            target = FAILURE;
        }

        return mapping.findForward(target);
    }
}
