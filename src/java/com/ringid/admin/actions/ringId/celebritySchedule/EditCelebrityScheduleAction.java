/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.ringId.celebritySchedule;

import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author ipvision
 */
public class EditCelebrityScheduleAction extends BaseAction {

    private static final Logger logger = RingLogger.getConfigPortalLogger();

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = SUCCESS;
        try {
            CelebrityScheduleForm celebrityScheduleForm = (CelebrityScheduleForm) form;
            long pageId = 0;
            long liveTime = 0;
            int userType = 0;
            if (request.getParameter("id") != null && request.getParameter("id").length() > 0) {
                pageId = Long.parseLong(request.getParameter("id"));
            }
            if (request.getParameter("liveTime") != null && request.getParameter("liveTime").length() > 0) {
                liveTime = Long.parseLong(request.getParameter("liveTime"));
            }
            if (request.getParameter("userType") != null && request.getParameter("userType").length() > 0) {
                userType = Integer.parseInt(request.getParameter("userType"));
            }
            celebrityScheduleForm.setPageId(pageId);
            celebrityScheduleForm.setLiveTime(String.valueOf(liveTime));
            celebrityScheduleForm.setCurrentLiveTime(getDateTime(liveTime));
            celebrityScheduleForm.setUserType(userType);
        } catch (Exception e) {
            logger.error("exception --> " + e);
            target = FAILURE;
        }

        return mapping.findForward(target);
    }

    private String getDateTime(long liveTime) {
        Date date = new Date(liveTime);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        return sdf.format(date);
    }
}
