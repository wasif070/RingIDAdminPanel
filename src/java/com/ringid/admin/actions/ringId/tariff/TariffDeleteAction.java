/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.ringId.tariff;

import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.dto.adminAuth.LoginDTO;
import com.ringid.admin.service.AdminService;
import com.ringid.admin.service.LiveStreamingService;
import com.ringid.admin.utils.Constants;
import com.ringid.admin.utils.ReasonCode;
import com.ringid.admin.utils.Utils;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author Rabby
 */
public class TariffDeleteAction extends BaseAction {

    private static final Logger logger = RingLogger.getConfigPortalLogger();

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = SUCCESS;
        String activistName;
        TariffForm tariffAddForm = (TariffForm) form;
        int reasonCode = -1;
        try {
            LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
            activistName = loginDTO.getUserName();
        } catch (Exception e) {
            target = UNAUTHORIZED;
            return mapping.findForward(target);
        }
        try {
            switch (tariffAddForm.getType()) {
                case 1:
                    RingLogger.getActivityLogger().debug("[TariffDeleteAction] activistName : " + activistName + " tariff: " + tariffAddForm.getTariff());
                    reasonCode = LiveStreamingService.getInstance().deleteLiveCallTariff(tariffAddForm.getTariff());
                    if (reasonCode == ReasonCode.NONE) {
                        request.setAttribute("message", "<span style='color: green'>Tariff deleted successfully</span>");
                    } else {
                        request.setAttribute("message", "<span style='color: red'> deleteTariff : " + Utils.getMessageForReasonCode(reasonCode) + "</span>");
                        target = FAILURE;
                    }
                    break;
                case 2:
                    RingLogger.getActivityLogger().debug("[deleteLiveSubscriptionFee TariffDeleteAction] activistName : " + activistName + " tariff: " + tariffAddForm.getTariff());
                    reasonCode = LiveStreamingService.getInstance().deleteLiveSubscriptionFee(tariffAddForm.getTariff());
                    if (reasonCode == ReasonCode.NONE) {
                        request.setAttribute("message", "<span style='color: green'>LiveSubscriptionFee deleted successfully</span>");
                    } else {
                        request.setAttribute("message", "<span style='color: red'> deleteLiveSubscriptionFee : " + Utils.getMessageForReasonCode(reasonCode) + "</span>");
                        target = FAILURE;
                    }
                    break;
            }
        } catch (Exception e) {
            logger.error("exception --> " + e);
            target = FAILURE;
        }
        if (SUCCESS.equals(target)) {
            return new ActionForward(mapping.findForward(target).getPath() + "?type=" + tariffAddForm.getType(), true);
        }
        return mapping.findForward(target);
    }
}
