/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.ringId.tariff;

import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.dto.adminAuth.LoginDTO;
import com.ringid.admin.service.AdminService;
import com.ringid.admin.service.LiveStreamingService;
import com.ringid.admin.utils.ReasonCode;
import com.ringid.admin.utils.Utils;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author Rabby
 */
public class TariffAddAction extends BaseAction {

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = SUCCESS;
        String activistName;
        TariffForm tariffAddForm = (TariffForm) form;
        int reasonCode = - 1;
        try {
            LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(com.ringid.admin.utils.Constants.LOGIN_DTO);
            activistName = loginDTO.getUserName();
        } catch (Exception e) {
            target = UNAUTHORIZED;
            return mapping.findForward(target);
        }
        try {
            switch (tariffAddForm.getType()) {
                case 1:
                    RingLogger.getActivityLogger().debug("[TariffAddAction] activistName : " + activistName + " tariff: " + tariffAddForm.getTariff());
                    reasonCode = LiveStreamingService.getInstance().addTariff(tariffAddForm.getTariff());
                    if (reasonCode == ReasonCode.NONE) {
                        request.setAttribute("message", "<span style='color: green'>Tariff added successfully</span>");
                    } else {
                        request.setAttribute("message", "<span style='color: red'> addTariff : " + Utils.getMessageForReasonCode(reasonCode) + "</span>");
                        target = FAILURE;
                    }
                    break;
                case 2:
                    RingLogger.getActivityLogger().debug("[addLiveSubscriptionFee TariffAddAction] activistName : " + activistName + " tariff: " + tariffAddForm.getTariff());
                    reasonCode = LiveStreamingService.getInstance().addLiveSubscriptionFee(tariffAddForm.getTariff());
                    if (reasonCode == ReasonCode.NONE) {
                        request.setAttribute("message", "<span style='color: green'>LiveSubscriptionFee added successfully</span>");
                    } else {
                        request.setAttribute("message", "<span style='color: red'> addLiveSubscriptionFee : " + Utils.getMessageForReasonCode(reasonCode) + "</span>");
                        target = FAILURE;
                    }
                    break;
            }
        } catch (Exception e) {
            target = FAILURE;
        }
        if (SUCCESS.equals(target)) {
            return new ActionForward(mapping.findForward(target).getPath() + "?type=" + tariffAddForm.getType(), true);
        }
        return mapping.findForward(target);
    }
}
