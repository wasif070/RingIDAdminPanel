/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.ringId.tariff;

import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.service.AdminService;
import com.ringid.admin.service.LiveStreamingService;
import com.ringid.admin.utils.Constants;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.ringid.livestream.StreamingTariffDTO;

/**
 *
 * @author Rabby
 */
public class TariffListAction extends BaseAction {

    private static final Logger logger = RingLogger.getConfigPortalLogger();
    List<StreamingTariffDTO> list = new ArrayList<>();

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = SUCCESS;
        try {
            TariffForm tariffForm = (TariffForm) form;
            switch (tariffForm.getType()) {
                case 1:
                    list = LiveStreamingService.getInstance().getAllTariff();
                    break;
                case 2:
                    list = LiveStreamingService.getInstance().getAllLiveSubscriptionFees();
                    break;
            }
            request.getSession(true).setAttribute(Constants.DATA_ROWS, list);
            managePages(request, tariffForm, list.size());
        } catch (Exception e) {
            logger.error("exception --> " + e);
            target = FAILURE;
        }
        return mapping.findForward(target);
    }
}
