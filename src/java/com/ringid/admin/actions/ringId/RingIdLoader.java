/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.ringId;

import com.ringid.admin.cassandra.CassandraDAO;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.utils.Constants;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.apache.logging.log4j.Logger;
import org.ringid.newsfeeds.FeedDTO;
import org.ringid.newsfeeds.FeedReturnDTO;
import org.ringid.utilities.AppConstants;

/**
 *
 * @author mamun
 */
public class RingIdLoader {

    private final static long LOADING_INTERVAL = 60 * 60 * 1000;
    private long loadingTime = 0;
    private static RingIdLoader instance = null;
    private final Logger LOGGER = RingLogger.getConfigPortalLogger();
    private UUID nextPivotId;
    private List<FeedDTO> feedDTOs;
    private int CASSLIMIT = 100;

    private RingIdLoader() {
        nextPivotId = null;
        feedDTOs = new ArrayList<>();
    }

    public static RingIdLoader getInstance() {
        if (instance == null) {
            createLoader();
        }
        return instance;
    }

    private synchronized static void createLoader() {
        if (instance == null) {
            instance = new RingIdLoader();
        }
    }

    private void checkForReload() {
        long currentTime = System.currentTimeMillis();
        if (currentTime - loadingTime > LOADING_INTERVAL) {
            loadingTime = currentTime;
            forceLoadData();
        }
    }

    public synchronized void forceReload() {
        loadingTime = System.currentTimeMillis();
        forceLoadData();
    }

    private void forceLoadData() {
        nextPivotId = null;
        feedDTOs.clear();
    }

    public List<FeedDTO> getfeedList(long userId, int start, int limit, int action) {
        checkForReload();
        if (action == Constants.SEARCH) {
            forceReload();
        }
        int toIndex = start + limit;
        LOGGER.info("[getfeedList] userId --> " + userId + " start --> " + start + " limit --> " + limit + " toIndex --> " + toIndex);
        FeedReturnDTO feedReturnDTO;
        try {
            if (toIndex > feedDTOs.size()) {
                int cassLimit = toIndex - feedDTOs.size();
                if (nextPivotId != null || feedDTOs.isEmpty()) {
                    feedReturnDTO = CassandraDAO.getInstance().getFeedList(userId, userId, null, AppConstants.FeedPullAction.OWN_WALL_FEED, nextPivotId, null, AppConstants.SCROLL_DOWN, cassLimit);
                    if (feedReturnDTO != null) {
                        nextPivotId = feedReturnDTO.getNextPivotId();
                        feedDTOs.addAll(feedReturnDTO.getFeedDTOList());
                    }
                }
            }
        } catch (Exception e) {
            LOGGER.error("Exception in [getfeedList] " + e);
        }
        toIndex = toIndex > feedDTOs.size() ? feedDTOs.size() : toIndex;
        LOGGER.info("[getfeedList] feedDTOs --> " + feedDTOs.size());
        return feedDTOs.subList(start, toIndex);
    }

    public List<FeedDTO> getfeedList(long userId) {
        LOGGER.info("[getfeedList] userId --> " + userId);
        List<FeedDTO> feedDTOList = new ArrayList<>();
        FeedReturnDTO feedReturnDTO;
        UUID nxtPivotId = null;
        try {
            do {
                feedReturnDTO = CassandraDAO.getInstance().getFeedList(userId, userId, null, AppConstants.FeedPullAction.OWN_WALL_FEED, nxtPivotId, null, AppConstants.SCROLL_DOWN, CASSLIMIT);
                if (feedReturnDTO != null && feedReturnDTO.getFeedDTOList().size() > 0) {
                    feedDTOList.addAll(feedReturnDTO.getFeedDTOList());
                } else {
                    break;
                }
                nxtPivotId = feedReturnDTO.getNextPivotId();
            } while (feedReturnDTO.getFeedDTOList().size() == CASSLIMIT && feedDTOList.size() < 1000);
        } catch (Exception e) {
            LOGGER.error("Exception in [getfeedList] " + e);
        }
        LOGGER.info("[getfeedList] feedDTOs --> " + feedDTOList.size());
        return feedDTOList;
    }
}
