/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.ringId.updateLiveVerificationSettings;

import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.dto.adminAuth.LoginDTO;
import com.ringid.admin.service.AdminService;
import com.ringid.admin.service.LiveStreamingService;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.Logger;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

/**
 *
 * @author Rabby
 */
public class UpdateLiveVerificationSettingsAction extends BaseAction {

    private static final Logger logger = RingLogger.getConfigPortalLogger();
    ActionErrors err = new ActionErrors();

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = SUCCESS;
        err.clear();
        String activistName;
        try {
            LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(com.ringid.admin.utils.Constants.LOGIN_DTO);
            activistName = loginDTO.getUserName();
        } catch (Exception e) {
            target = UNAUTHORIZED;
            return mapping.findForward(target);
        }
        try {
            UpdateLiveVerificationSettingsForm updateLiveVerificationSettingsForm = (UpdateLiveVerificationSettingsForm) form;
            RingLogger.getActivityLogger().debug("[UpdateLiveVerificationSettingsAction] activistName : " + activistName + " isVerified --> " + updateLiveVerificationSettingsForm.isIsVerified());
            boolean result = LiveStreamingService.getInstance().updateLiveVerifiedSettings(updateLiveVerificationSettingsForm.isIsVerified());
            if (result) {
                err.add("errormsg", new ActionMessage("<span style='color:green'>Operation successful.</span>", false));
            } else {
                err.add("errormsg", new ActionMessage("<span style='color:red'>Operation failed.</span>", false));
            }
            saveErrors(request, err);
        } catch (Exception e) {
            target = FAILURE;
            logger.error(e);
        }
        return mapping.findForward(target);
    }
}
