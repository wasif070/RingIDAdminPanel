/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.ringId.postStatus;

import com.google.gson.Gson;
import com.ringid.admin.BaseAction;
import com.ringid.admin.dto.adminAuth.LoginDTO;
import com.ringid.admin.service.LiveStreamingService;
import com.ringid.admin.service.NewsFeedService;
import com.ringid.admin.utils.HTTPStatusCodes;
import com.ringid.admin.utils.ReasonCode;
import com.ringid.admin.utils.log.RingLogger;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.UUID;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.json.JSONObject;
import org.ringid.newsfeeds.FeedDTO;
import ringid.LoginTaskScheduler;

/**
 *
 * @author mamun
 */
public class RecordedLiveFeedDeleteAction extends BaseAction {

    private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = SUCCESS;
        String message = "";
        String activistName;
        try {
            LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(com.ringid.admin.utils.Constants.LOGIN_DTO);
            activistName = loginDTO.getUserName();
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [RecordedLiveFeedDeleteAction] " + e);
            target = UNAUTHORIZED;
            return mapping.findForward(target);
        }
        try {
            PostStatusForm postStatusForm = (PostStatusForm) form;
            String validity;
            int reasonCode = - 1;
            if (postStatusForm.getDatetime() != null && postStatusForm.getDatetime().length() > 0) {
                long currentTime = System.currentTimeMillis();
                validity = LiveStreamingService.getInstance().getCloudValidity(dateFormat.parse(postStatusForm.getDatetime()).getTime(), currentTime);
                LoginTaskScheduler.getInstance().login(com.ringid.admin.utils.Constants.USERID, com.ringid.admin.utils.Constants.PASSWORD);
                String mediaUrl = new Upload().restoreMediaCloud(com.ringid.admin.utils.Constants.USERID, com.ringid.admin.utils.Constants.RESTORE_LINK_CLOUD_MEDIA, postStatusForm.getUrl());
                JSONObject jSONObject = new JSONObject(mediaUrl);
                if (jSONObject.getInt("code") == HTTPStatusCodes.ACCEPTED) {
                    mediaUrl = new Upload().deleteFromCloud(com.ringid.admin.utils.Constants.USERID, com.ringid.admin.utils.Constants.DELETE_LINK_CLOUD_MEDIA, validity);
                    LoginTaskScheduler.getInstance().logout(com.ringid.admin.utils.Constants.USERID);
                    jSONObject = new JSONObject(mediaUrl);
                    if (jSONObject.getInt("code") == HTTPStatusCodes.NO_CONTENT) {
                        long sessionUserId = postStatusForm.getPageId();
                        int sessionUserType = postStatusForm.getPageType();

                        FeedDTO feedDTO = new FeedDTO();
                        feedDTO.setFeedId(UUID.fromString(postStatusForm.getFeedId()));
                        feedDTO.setExpiredAfter(currentTime + dateFormat.parse(postStatusForm.getDatetime()).getTime());

                        RingLogger.getConfigPortalLogger().debug("[RecordedLiveFeedDeleteAction] sessionUserId --> " + sessionUserId + " sessionUserType -->" + sessionUserType + " feedDTO --> " + new Gson().toJson(feedDTO));
                        RingLogger.getActivityLogger().debug("[RecordedLiveFeedDeleteAction] activistName : " + activistName + " sessionUserId --> " + sessionUserId + " sessionUserType --> " + sessionUserType + " feedDTO --> " + new Gson().toJson(feedDTO));
                        reasonCode = NewsFeedService.getInstance().editStatus(feedDTO, sessionUserId);
                        if (reasonCode == ReasonCode.NONE) {
                            message = "Edit Status Successfull";
                            target = SUCCESS;
                        } else {
                            message = "Edit Status Failed!!! (rc: " + reasonCode + ")";
                            target = FAILURE;
                        }
                    } else {
                        LoginTaskScheduler.getInstance().logout(com.ringid.admin.utils.Constants.USERID);
                        message = "Status Post Failed!!! (rc: delete media failed)";
                        target = FAILURE;
                    }
                } else {
                    LoginTaskScheduler.getInstance().logout(com.ringid.admin.utils.Constants.USERID);
                    message = "Status Post Failed!!! (rc: restore media failed)";
                    target = FAILURE;
                }
            }
        } catch (ParseException e) {
            RingLogger.getConfigPortalLogger().error("Exception in [PostRecordedLiveFeedAction] " + e);
            target = FAILURE;
        }
        request.setAttribute("message", "<span style='color: green;'>" + message + "</span>");
        return mapping.findForward(target);
    }
}
