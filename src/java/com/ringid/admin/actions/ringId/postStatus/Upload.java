/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.ringId.postStatus;

import auth.com.dto.InfoDTo;
import com.google.gson.Gson;
import com.ringid.admin.utils.Constants;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.utils.HTTPRequestProcessor;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;
import javax.imageio.ImageIO;
import org.json.JSONException;
import org.json.JSONObject;
import ringid.LoginTaskScheduler;

/**
 *
 * @author mamun
 */
public class Upload {

    public String uploadToCloud(String USER_ID, String filename, byte[] fileData, String serverUrl, int mediaType, UUID streamId) throws IOException {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(USER_ID);
        RingLogger.getConfigPortalLogger().debug("userId --> " + USER_ID + " filename --> " + filename + " serverUrl --> " + serverUrl + " mediaType --> " + mediaType + " streamId --> " + streamId);
        String mediaUrl = null;
        String response;
        HashMap<String, Object> headerValues = new HashMap<>();
        headerValues.put("access-control-allow-origin", "*");
        HashMap<String, Object> bodyValues = null;
        ArrayList<String> names = new ArrayList<>();
        ArrayList<byte[]> dataValues = new ArrayList<>();
        int height = 0, width = 0;
        if (mediaType == Constants.MEDIA_TYPE.IMAGE) {
            try (InputStream in = new ByteArrayInputStream(fileData)) {
                BufferedImage buf = ImageIO.read(in);
                height = buf.getHeight();
                width = buf.getWidth();
                System.out.println(width + " " + height);
            }
        }
        bodyValues = getBodyValues(infoDTo, height, width, streamId, mediaType);
        names.add(filename);
        dataValues.add(fileData);
        response = HTTPRequestProcessor.uploader_method("POST", headerValues, bodyValues, null, names, dataValues, serverUrl, "");
        try {
            JSONObject jSONObject = new JSONObject(response);
            response = jSONObject.getString("mainstr");
            if (response != null && response.length() > 0) {
                byte result = (byte) response.charAt(0);
                System.out.println(response.substring(2));
                if ((int) result == 1) {
                    mediaUrl = response.substring(2);
                } else {
                    mediaUrl = "";
                }
            }
        } catch (JSONException e) {

        }
        return mediaUrl;
    }

    public String deleteFromCloud(String USER_ID, String serverUrl, String validity) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(USER_ID);
        RingLogger.getConfigPortalLogger().debug("userId --> " + USER_ID + " serverUrl --> " + serverUrl + " validity --> " + validity);
        HashMap<String, Object> headerValues = new HashMap<>();
        headerValues.put("X-STORAGE-TOKEN", infoDTo.getSessionId());
        headerValues.put("X-USER", infoDTo.getUserId());
        headerValues.put("X-AUTHSERVER", infoDTo.getAuthIP());
        headerValues.put("X-COMPORT", infoDTo.getAuthPORT());
        headerValues.put("X-AVAILABLE-BEFORE", 1);
        headerValues.put("X-IPV-TTL", validity);
        return HTTPRequestProcessor.uploader_method("DELETE", headerValues, null, null, null, null, serverUrl, "");
    }

    public String restoreMediaCloud(String USER_ID, String serverUrl, String mediaUrl) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(USER_ID);
        RingLogger.getConfigPortalLogger().debug("userId --> " + USER_ID + " mediaUrl --> " + mediaUrl + " serverUrl --> " + serverUrl);
        HashMap<String, Object> headerValues = new HashMap<>();
        headerValues.put("X-STORAGE-TOKEN", infoDTo.getSessionId());
        headerValues.put("X-USER", infoDTo.getUserId());
        headerValues.put("X-AUTHSERVER", infoDTo.getAuthIP());
        headerValues.put("X-COMPORT", infoDTo.getAuthPORT());
        headerValues.put("X-RESTORE-URL", mediaUrl);
        return HTTPRequestProcessor.uploader_method("POST", headerValues, null, null, null, null, serverUrl, "");
    }

    private HashMap<String, Object> getBodyValues(InfoDTo infoDTo, int height, int width, UUID streamId, int mediaType) {
        HashMap<String, Object> bodyValues = new HashMap<>();
        bodyValues.put("sId", infoDTo.getSessionId());
        bodyValues.put("uId", infoDTo.getUserId());
        bodyValues.put("authServer", infoDTo.getAuthIP());
        bodyValues.put("comPort", infoDTo.getAuthPORT());
        bodyValues.put("x-app-version", Constants.VERSION);
        if (mediaType == Constants.MEDIA_TYPE.IMAGE) {
            bodyValues.put("cimX", 0);
            bodyValues.put("cimY", 0);
            bodyValues.put("ih", height);
            bodyValues.put("iw", width);
        } else if (mediaType == Constants.MEDIA_TYPE.VIDEO) {
            bodyValues.put("streamId", streamId);
        }
        return bodyValues;
    }
}
