/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.ringId.postStatus;

import auth.com.utils.Constants;
import com.google.gson.Gson;
import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.helper.Helper;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.dto.adminAuth.LoginDTO;
import com.ringid.admin.actions.ringId.celebritySchedule.CelebrityScheduleLoader;
import com.ringid.admin.service.LiveStreamingService;
import com.ringid.admin.utils.TimeUUID;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import javax.imageio.ImageIO;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.upload.FormFile;
import org.ringid.feedbacks.NewsFeedFeedBack;
import org.ringid.newsfeeds.ImageDTO;
import org.ringid.newsfeeds.MediaAlbumDTO;
import org.ringid.newsfeeds.MediaContentDTO;
import org.ringid.newsfeeds.NewsFeed;
import org.ringid.utilities.AppConstants;
import ringid.LoginTaskScheduler;
import java.io.IOException;
import java.text.ParseException;
import java.util.UUID;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.ringid.newsfeeds.RecordLiveDTO;

/**
 *
 * @author ipvision
 */
public class PostStatusAction extends BaseAction {

    private static int feedType = AppConstants.TEXT;
    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = SUCCESS;
        String message = "";
        String activistName;
        try {
            LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(com.ringid.admin.utils.Constants.LOGIN_DTO);
            activistName = loginDTO.getUserName();
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [PostStatusAction] " + e);
            target = UNAUTHORIZED;
            return mapping.findForward(target);
        }
        try {
            PostStatusForm postStatusForm = (PostStatusForm) form;

            long sessionUserId = postStatusForm.getPageId();
            int sessionUserType = postStatusForm.getPageType();
            long celebrityLiveTime = 0;

            feedType = AppConstants.TEXT;
            String albumName = "";
            if (postStatusForm.getAlbumName() != null && postStatusForm.getAlbumName().length() > 0) {
                albumName = postStatusForm.getAlbumName();
            }
            ArrayList<FormFile> myFiles = getFileListAndSetFeedType(postStatusForm, albumName);

            NewsFeed newsFeed = new NewsFeed();
            switch (sessionUserType) {
                case AppConstants.UserTypeConstants.CELEBRITY:
                    Date date = dateFormat.parse(postStatusForm.getDatetime());
                    celebrityLiveTime = date.getTime();
                    RecordLiveDTO recordLiveDTO = new RecordLiveDTO();
                    recordLiveDTO.setStartTime(celebrityLiveTime);
                    recordLiveDTO.setStartTimeUUID(TimeUUID.createUUID(celebrityLiveTime));
                    newsFeed.setLive(recordLiveDTO);
                    newsFeed.setLiveType(AppConstants.FEED_LIVE_TYPE.SCHEDULE);
                    break;
                default:
                    break;
            }
            newsFeed.setFeedContentType(feedType);
            newsFeed.setStatus(postStatusForm.getStatustext());
            if (myFiles != null && feedType > 0) {
                switch (feedType) {
                    case AppConstants.SINGLE_IMAGE:
                    case AppConstants.SINGLE_IMAGE_WITH_ALBUM:
                    case AppConstants.MULTIPLE_IMAGE_WITH_ALBUM:
                        newsFeed.setAlbumDTO(prepareImageList(myFiles, albumName));
                        break;
                    case AppConstants.SINGLE_AUDIO:
                    case AppConstants.SINGLE_AUDIO_WITH_ALBUM:
                    case AppConstants.MULTIPLE_AUDIO_WITH_ALBUM:
                        newsFeed.setAlbumDTO(prepareAudioList(myFiles, feedType, albumName, postStatusForm.getMediaDuration()));
                        break;
                    case AppConstants.SINGLE_VIDEO:
                    case AppConstants.SINGLE_VIDEO_WITH_ALBUM:
                    case AppConstants.MULTIPLE_VIDEO_WITH_ALBUM:
                        newsFeed.setAlbumDTO(prepareVideoList(myFiles, feedType, albumName, postStatusForm.getMediaDuration(), sessionUserId));
                        break;
                }
            }
            RingLogger.getConfigPortalLogger().debug("[PostStatusAction] sessionUserId --> " + sessionUserId + " sessionUserType -->" + sessionUserType + " donationPageId --> " + postStatusForm.getDonationPageId() + " newsFeed --> " + new Gson().toJson(newsFeed));
            RingLogger.getActivityLogger().debug("[PostStatusAction] activistName : " + activistName + " sessionUserId --> " + sessionUserId + " sessionUserType --> " + sessionUserType + " donationPageId --> " + postStatusForm.getDonationPageId() + " newsFeed --> " + new Gson().toJson(newsFeed));
            NewsFeedFeedBack newsFeedFeedBack = Helper.getInstance().addStatus(sessionUserId, sessionUserType, newsFeed, postStatusForm.getDonationPageId());
            if (newsFeedFeedBack.isSuccess()) {
                message = "Status Post Successfull";
                if (sessionUserType == AppConstants.UserTypeConstants.CELEBRITY) {
                    LiveStreamingService.getInstance().updateLiveScheduleSerial(postStatusForm.getPageId(), AppConstants.UserTypeConstants.CELEBRITY, celebrityLiveTime, postStatusForm.getLiveScheduleSerial());
                    CelebrityScheduleLoader.getInstance().forceReload();
                }
                target = SUCCESS;
            } else {
                message = "Status Post Failed!!! (rc: " + newsFeedFeedBack.getReasonCode() + ")";
                target = FAILURE;
            }

        } catch (ParseException | IOException e) {
            RingLogger.getConfigPortalLogger().error("Exception in [PostStatusAction] " + e);
            target = FAILURE;
        }
        response.setContentType("application/json;charset=UTF-8");
        response.getWriter().write(message);
        return null;
    }

    private ArrayList<FormFile> getFileListAndSetFeedType(PostStatusForm postStatusForm, String albumName) {
        ArrayList<FormFile> myFiles = null;
        if (postStatusForm.getStatusimage() != null && postStatusForm.getStatusimage().size() > 0) {
            RingLogger.getConfigPortalLogger().debug("imagestatus");
            myFiles = postStatusForm.getStatusimage();
            if (myFiles.size() > 1) {
                feedType = AppConstants.MULTIPLE_IMAGE_WITH_ALBUM;
            } else if (albumName != null && albumName.length() > 0) {
                feedType = AppConstants.SINGLE_IMAGE_WITH_ALBUM;
            } else {
                feedType = AppConstants.SINGLE_IMAGE;
            }
        } else if (postStatusForm.getStatusaudio() != null && postStatusForm.getStatusaudio().size() > 0) {
            RingLogger.getConfigPortalLogger().debug("audiostatus");
            myFiles = postStatusForm.getStatusaudio();
            if (myFiles.size() > 1) {
                feedType = AppConstants.MULTIPLE_AUDIO_WITH_ALBUM;
            } else if (albumName != null && albumName.length() > 0) {
                feedType = AppConstants.SINGLE_AUDIO_WITH_ALBUM;
            } else {
                feedType = AppConstants.SINGLE_AUDIO;
            }
        } else if (postStatusForm.getStatusvideo() != null && postStatusForm.getStatusvideo().size() > 0) {
            RingLogger.getConfigPortalLogger().debug("videostatus");
            myFiles = postStatusForm.getStatusvideo();
            if (myFiles.size() > 1) {
                feedType = AppConstants.MULTIPLE_VIDEO_WITH_ALBUM;
            } else if (albumName != null && albumName.length() > 0) {
                feedType = AppConstants.SINGLE_VIDEO_WITH_ALBUM;
            } else {
                feedType = AppConstants.SINGLE_VIDEO;
            }
        }
        RingLogger.getConfigPortalLogger().debug("no status");
        return myFiles;
    }

    private MediaAlbumDTO prepareImageList(ArrayList<FormFile> myFiles, String albumName) throws IOException {
        MediaAlbumDTO mediaAlbumDTO = new MediaAlbumDTO();
        ArrayList<ImageDTO> imageList = new ArrayList<>();
        LoginTaskScheduler.getInstance().login(com.ringid.admin.utils.Constants.USERID, com.ringid.admin.utils.Constants.PASSWORD);
        for (FormFile formFile : myFiles) {
            String imageUrl = new Upload().uploadToCloud(com.ringid.admin.utils.Constants.USERID, formFile.getFileName(), formFile.getFileData(), com.ringid.admin.utils.Constants.UPLOAD_LINK_PAGE_IMAGE, com.ringid.admin.utils.Constants.MEDIA_TYPE.IMAGE, null);
            BufferedImage bufferedImage = ImageIO.read(new ByteArrayInputStream(formFile.getFileData()));
            ImageDTO imageDTO1 = new ImageDTO();
            imageDTO1.setImageType(AppConstants.IMAGE_TYPE_FEED_IMAGE);
            imageDTO1.setImageHeight(bufferedImage.getHeight());
            imageDTO1.setImageWidth(bufferedImage.getWidth());
            imageDTO1.setImageURL(imageUrl);
            imageDTO1.setImagePrivacy(Constants.CASS_PRIVACY_PUBLIC);
            imageList.add(imageDTO1);
        }
        LoginTaskScheduler.getInstance().logout(com.ringid.admin.utils.Constants.USERID);
        mediaAlbumDTO.setAlbumName(albumName);
        mediaAlbumDTO.setImageList(imageList);
        mediaAlbumDTO.setMediaType(Constants.MEDIA_TYPE_IMAGE);
        mediaAlbumDTO.setImageType(AppConstants.IMAGE_TYPE_FEED_IMAGE);
        return mediaAlbumDTO;
    }

    private MediaAlbumDTO prepareAudioList(ArrayList<FormFile> myFiles, int feedType, String albumName, int mediaDuration) throws IOException {
        MediaAlbumDTO mediaAlbumDTO = new MediaAlbumDTO();
        ArrayList<MediaContentDTO> mediaList = new ArrayList<>();
        LoginTaskScheduler.getInstance().login(com.ringid.admin.utils.Constants.USERID, com.ringid.admin.utils.Constants.PASSWORD);
        for (FormFile formFile : myFiles) {
            String audioUrl = new Upload().uploadToCloud(com.ringid.admin.utils.Constants.USERID, formFile.getFileName(), formFile.getFileData(), com.ringid.admin.utils.Constants.UPLOAD_LINK_PAGE_AUDIO, com.ringid.admin.utils.Constants.MEDIA_TYPE.AUDIO, null);
            MediaContentDTO mediaDTO = new MediaContentDTO();
            mediaDTO.setMediaStreamURL(audioUrl);
            mediaDTO.setMediaTitle(formFile.getFileName());
            mediaDTO.setPrivacy(Constants.CASS_PRIVACY_PUBLIC);
            mediaDTO.setMediaType(Constants.MEDIA_TYPE_AUDIO);
            mediaDTO.setMediaDuration(mediaDuration);
            mediaList.add(mediaDTO);
        }
        LoginTaskScheduler.getInstance().logout(com.ringid.admin.utils.Constants.USERID);
        mediaAlbumDTO.setMediaContentList(mediaList);
        mediaAlbumDTO.setAlbumName(albumName);
        mediaAlbumDTO.setMediaType(Constants.MEDIA_TYPE_AUDIO);
        return mediaAlbumDTO;
    }

    private MediaAlbumDTO prepareVideoList(ArrayList<FormFile> myFiles, int feedType, String albumName, int mediaDuration, long sessionUserId) throws IOException {
        MediaAlbumDTO mediaAlbumDTO = new MediaAlbumDTO();
        ArrayList<MediaContentDTO> mediaList = new ArrayList<>();
        UUID streamId = null;
        LoginTaskScheduler.getInstance().login(com.ringid.admin.utils.Constants.USERID, com.ringid.admin.utils.Constants.PASSWORD);
        for (FormFile formFile : myFiles) {
            UUID tempUUID = TimeUUID.timeBased();
            streamId = new UUID(tempUUID.getMostSignificantBits(), sessionUserId);
            String audioUrl = new Upload().uploadToCloud(com.ringid.admin.utils.Constants.USERID, formFile.getFileName(), formFile.getFileData(), com.ringid.admin.utils.Constants.UPLOAD_LINK_CLOUD_MEDIA, com.ringid.admin.utils.Constants.MEDIA_TYPE.VIDEO, streamId);
            MediaContentDTO mediaDTO = new MediaContentDTO();
            mediaDTO.setMediaStreamURL(audioUrl);
            mediaDTO.setMediaTitle(formFile.getFileName());
            mediaDTO.setPrivacy(Constants.CASS_PRIVACY_PUBLIC);
            mediaDTO.setMediaType(Constants.MEDIA_TYPE_VIDEO);
            mediaDTO.setMediaDuration(mediaDuration);
            mediaList.add(mediaDTO);
        }
        LoginTaskScheduler.getInstance().logout(com.ringid.admin.utils.Constants.USERID);
        mediaAlbumDTO.setMediaContentList(mediaList);
        mediaAlbumDTO.setAlbumName(albumName);
        mediaAlbumDTO.setMediaType(Constants.MEDIA_TYPE_VIDEO);
        return mediaAlbumDTO;
    }
}
