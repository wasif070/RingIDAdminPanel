/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.ringId.postStatus;

import com.ringid.admin.BaseForm;
import java.util.ArrayList;
import java.util.UUID;
import org.apache.struts.upload.FormFile;

/**
 *
 * @author ipvision
 */
public class PostStatusForm extends BaseForm {

    private Long pageId;
    private String datetime;
    private String statustext;
    private String albumName;
    private int mediaDuration;
    private ArrayList<FormFile> statusimage = new ArrayList<>();
    private ArrayList<FormFile> statusvideo = new ArrayList<>();
    private ArrayList<FormFile> statusaudio = new ArrayList<>();
    private int liveScheduleSerial;
    private long donationPageId;
    private int pageType;
    private String startTime;
    private String endTime;
    private String url;
    private int viewerCount;
    private String location;
    private String profileImage;
    private String title;
    private int top;
    private String duration;
    private String pageName;
    private String pageRingId;
    private int streamingType;
    private String feedId;

    public String getFeedId() {
        return feedId;
    }

    public void setFeedId(String feedId) {
        this.feedId = feedId;
    }

    public int getStreamingType() {
        return streamingType;
    }

    public void setStreamingType(int streamingType) {
        this.streamingType = streamingType;
    }

    public String getPageRingId() {
        return pageRingId;
    }

    public void setPageRingId(String pageRingId) {
        this.pageRingId = pageRingId;
    }

    public String getPageName() {
        return pageName;
    }

    public void setPageName(String pageName) {
        this.pageName = pageName;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public int getTop() {
        return top;
    }

    public void setTop(int top) {
        this.top = top;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getViewerCount() {
        return viewerCount;
    }

    public void setViewerCount(int viewerCount) {
        this.viewerCount = viewerCount;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getPageType() {
        return pageType;
    }

    public void setPageType(int pageType) {
        this.pageType = pageType;
    }

    public long getDonationPageId() {
        return donationPageId;
    }

    public void setDonationPageId(long donationPageId) {
        this.donationPageId = donationPageId;
    }

    public int getLiveScheduleSerial() {
        return liveScheduleSerial;
    }

    public void setLiveScheduleSerial(int liveScheduleSerial) {
        this.liveScheduleSerial = liveScheduleSerial;
    }

    public int getMediaDuration() {
        return mediaDuration;
    }

    public void setMediaDuration(int mediaDuration) {
        this.mediaDuration = mediaDuration;
    }

    public ArrayList<FormFile> getStatusvideo() {
        return statusvideo;
    }

    public void setStatusvideo(ArrayList<FormFile> statusvideo) {
        this.statusvideo = statusvideo;
    }

    public ArrayList<FormFile> getStatusaudio() {
        return statusaudio;
    }

    public void setStatusaudio(ArrayList<FormFile> statusaudio) {
        this.statusaudio = statusaudio;
    }

    public ArrayList<FormFile> getStatusimage() {
        return statusimage;
    }

    public void setStatusimage(ArrayList<FormFile> statusimage) {
        this.statusimage = statusimage;
    }

    public String getAlbumName() {
        return albumName;
    }

    public void setAlbumName(String albumName) {
        this.albumName = albumName;
    }

    public Long getPageId() {
        return pageId;
    }

    public void setPageId(Long pageId) {
        this.pageId = pageId;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    public String getStatustext() {
        return statustext;
    }

    public void setStatustext(String statustext) {
        this.statustext = statustext;
    }

}
