/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.ringId.postStatus;

import com.google.gson.Gson;
import com.ringid.admin.BaseAction;
import com.ringid.admin.cassandra.CassandraDAO;
import com.ringid.admin.utils.helper.Helper;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.dto.adminAuth.LoginDTO;
import com.ringid.admin.service.LiveStreamingService;
import com.ringid.admin.utils.Constants;
import com.ringid.admin.utils.MyAppError;
import com.ringid.admin.utils.TimeUUID;
import com.ringid.admin.utils.Utils;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;
import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.upload.FormFile;
import org.ringid.feedbacks.NewsFeedFeedBack;
import org.ringid.livestream.StreamingDTO;
import org.ringid.newsfeeds.NewsFeed;
import org.ringid.newsfeeds.RecordLiveDTO;
import org.ringid.utilities.AppConstants;
import ringid.LoginTaskScheduler;

/**
 *
 * @author mamun
 */
public class PostRecordedLiveFeedAction extends BaseAction {

    private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = SUCCESS;
        String message = "";
        String activistName;
        try {
            LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(com.ringid.admin.utils.Constants.LOGIN_DTO);
            activistName = loginDTO.getUserName();
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [PostRecordedLiveFeedAction] " + e);
            target = UNAUTHORIZED;
            return mapping.findForward(target);
        }
        try {
            PostStatusForm postStatusForm = (PostStatusForm) form;
            FormFile mediaFile = null;
            if (postStatusForm.getStatusvideo() != null && postStatusForm.getStatusvideo().size() > 0) {
                mediaFile = postStatusForm.getStatusvideo().get(0);
            }
            if (mediaFile != null && mediaFile.getFileData() != null && mediaFile.getFileData().length > 0) {
                UUID streamId = null;
                UUID tempUUID = TimeUUID.timeBased();
                streamId = new UUID(tempUUID.getMostSignificantBits(), postStatusForm.getPageId());
                LoginTaskScheduler.getInstance().login(com.ringid.admin.utils.Constants.USERID, com.ringid.admin.utils.Constants.PASSWORD);
                String mediaUrl = new Upload().uploadToCloud(com.ringid.admin.utils.Constants.USERID, mediaFile.getFileName(), mediaFile.getFileData(), com.ringid.admin.utils.Constants.UPLOAD_LINK_CLOUD_MEDIA, com.ringid.admin.utils.Constants.MEDIA_TYPE.VIDEO, streamId);
                LoginTaskScheduler.getInstance().logout(com.ringid.admin.utils.Constants.USERID);
                if (mediaUrl != null && !mediaUrl.isEmpty()) {
                    long sessionUserId = postStatusForm.getPageId();
                    int sessionUserType = postStatusForm.getPageType();
                    long startTime, endTime, durationLongValue = 0L;
                    if (postStatusForm.getDuration() != null && postStatusForm.getDuration().length() > 0) {
                        durationLongValue = (long) Double.parseDouble(postStatusForm.getDuration()) * 1000;
                    }
                    Date date = dateFormat.parse(postStatusForm.getStartTime());
                    startTime = date.getTime();
                    endTime = startTime + durationLongValue;

                    final BufferedImage bi = ImageIO.read(new URL(Constants.MEDIA_CLOUD_BASE_URL + postStatusForm.getProfileImage()));

                    NewsFeed newsFeed = new NewsFeed();
                    RecordLiveDTO recordLiveDTO = new RecordLiveDTO();

                    recordLiveDTO.setStreamId(streamId);
                    recordLiveDTO.setUrl(mediaUrl);
                    recordLiveDTO.setType(postStatusForm.getStreamingType());
                    recordLiveDTO.setDuration(durationLongValue);
                    recordLiveDTO.setViewerCount(postStatusForm.getViewerCount());
                    recordLiveDTO.setLocation(postStatusForm.getLocation());
                    recordLiveDTO.setImage(postStatusForm.getProfileImage());
                    recordLiveDTO.setImageHeight(bi.getHeight());
                    recordLiveDTO.setImageWidth(bi.getWidth());
                    recordLiveDTO.setTitle(postStatusForm.getTitle());
                    recordLiveDTO.setStartTime(startTime);
                    recordLiveDTO.setStartTimeUUID(TimeUUID.createUUID(startTime));
                    recordLiveDTO.setEndTime(endTime);

                    newsFeed.setLive(recordLiveDTO);
                    newsFeed.setLiveType(AppConstants.FEED_LIVE_TYPE.PAST_LIVE);
                    newsFeed.setFeedContentType(AppConstants.TEXT);
                    RingLogger.getConfigPortalLogger().debug("[PostRecordedLiveFeedAction] sessionUserId --> " + sessionUserId + " sessionUserType -->" + sessionUserType + " newsFeed --> " + new Gson().toJson(newsFeed));
                    RingLogger.getActivityLogger().debug("[PostRecordedLiveFeedAction] activistName : " + activistName + " sessionUserId --> " + sessionUserId + " sessionUserType --> " + sessionUserType + " newsFeed --> " + new Gson().toJson(newsFeed));
                    NewsFeedFeedBack newsFeedFeedBack = Helper.getInstance().addStatus(sessionUserId, sessionUserType, newsFeed, postStatusForm.getDonationPageId());
                    if (newsFeedFeedBack.isSuccess()) {
                        StreamingDTO stream = new StreamingDTO();
                        stream.setRecordLive(true);
                        stream.setUserTableId(postStatusForm.getPageId());
                        stream.setuId(postStatusForm.getPageRingId());
                        stream.setName(postStatusForm.getPageName());
                        stream.setOwnerId(CassandraDAO.getInstance().getPageOwnerId(postStatusForm.getPageId())); // set page's owner id
                        stream.setProfileImage(postStatusForm.getProfileImage());
                        stream.setStreamId(streamId);
                        stream.setCountry(postStatusForm.getLocation());
                        stream.setTitle(postStatusForm.getTitle());
                        stream.setStreamMediaType(postStatusForm.getStreamingType());
                        stream.setRegistrationType(Utils.getRegistrationTypeByStreamingMediaType(postStatusForm.getStreamingType()));
                        stream.setViewerCount(postStatusForm.getViewerCount());
                        stream.setStartTime(startTime);
                        stream.setEndTime(endTime);
                        stream.setDuration((int) durationLongValue);
                        stream.setUrl(mediaUrl);
                        MyAppError myAppError = LiveStreamingService.updateManualRecordedLive(streamId, stream);
                        if (myAppError.getERROR_TYPE() == MyAppError.NOERROR) {
                            notifyAuthServers(Constants.RELOAD_MANUAL_RECORDED_LIVES, null);
                        }
                        message = "Status Post Successfull";
                        target = SUCCESS;
                    } else {
                        message = "Status Post Failed!!! (rc: " + newsFeedFeedBack.getReasonCode() + ")";
                        target = FAILURE;
                    }
                } else {
                    message = "Status Post Failed!!! (rc: Video Upload failed)";
                    target = FAILURE;
                }
            } else {
                message = "Status Post Failed!!! (rc: Invalid Media File )";
                target = FAILURE;
            }
        } catch (ParseException | IOException e) {
            RingLogger.getConfigPortalLogger().error("Exception in [PostRecordedLiveFeedAction] " + e);
            target = FAILURE;
        }
        request.setAttribute("message", "<span style='color: green;'>" + message + "</span>");
        return mapping.findForward(target);
    }
}
