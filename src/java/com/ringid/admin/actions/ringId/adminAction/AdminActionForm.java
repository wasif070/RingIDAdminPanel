/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.ringId.adminAction;

import com.ringid.admin.BaseForm;
import javax.servlet.http.HttpServletRequest;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

/**
 *
 * @author mamun
 */
public class AdminActionForm extends BaseForm {

    private String userId;
    private int actionType;
    private int device;

    public int getDevice() {
        return device;
    }

    public void setDevice(int device) {
        this.device = device;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public int getActionType() {
        return actionType;
    }

    public void setActionType(int actionType) {
        this.actionType = actionType;
    }

    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        ActionErrors errors = new ActionErrors();
        if (getActionType() < 1) {
            errors.add("actionType", new ActionMessage("errors.roomname.required"));
        }
        if (getUserId() == null || getUserId().length() < 1) {
            errors.add("userId", new ActionMessage("errors.roomname.required"));
        }
        return errors;
    }
}
