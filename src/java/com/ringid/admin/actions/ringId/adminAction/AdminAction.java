/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.ringId.adminAction;

import com.ringid.admin.BaseAction;
import com.ringid.admin.dto.adminAuth.LoginDTO;
import com.ringid.admin.service.UserService;
import com.ringid.admin.utils.log.RingLogger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author user
 */
public class AdminAction extends BaseAction {

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = SUCCESS;
        String activistName;
        try {
            LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(com.ringid.admin.utils.Constants.LOGIN_DTO);
            activistName = loginDTO.getUserName();
        } catch (Exception e) {
            target = UNAUTHORIZED;
            return mapping.findForward(target);
        }
        try {
            AdminActionForm adminActionForm = (AdminActionForm) form;
            RingLogger.getActivityLogger().debug("[AdminAction] activistName : " + activistName + " actionType --> " + adminActionForm.getActionType());
            String result = UserService.getInstance().adminAction(adminActionForm.getUserId().trim(), adminActionForm.getActionType(), adminActionForm.getDevice(), adminActionForm.getMessage());
            request.setAttribute("message", result);
        } catch (Exception e) {
            target = FAILURE;
        }
        return mapping.findForward(target);
    }
}
