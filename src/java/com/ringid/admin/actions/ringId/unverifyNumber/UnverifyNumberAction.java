/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.ringId.unverifyNumber;

import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.dto.adminAuth.LoginDTO;
import com.ringid.admin.service.AdminService;
import com.ringid.admin.service.UserService;
import com.ringid.admin.utils.MyAppError;
import com.ringid.admin.utils.Utils;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.Logger;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

/**
 *
 * @author Rabby
 */
public class UnverifyNumberAction extends BaseAction {

    private static Logger logger = RingLogger.getConfigPortalLogger();
    ActionErrors err = new ActionErrors();

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = SUCCESS;
        err.clear();
        String activistName;
        int verified = 0;
        try {
            LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(com.ringid.admin.utils.Constants.LOGIN_DTO);
            activistName = loginDTO.getUserName();
        } catch (Exception e) {
            target = UNAUTHORIZED;
            return mapping.findForward(target);
        }
        try {
            UnverifyNumberForm unverifyNumberForm = (UnverifyNumberForm) form;
            UnverifyNumberDTO dto = new UnverifyNumberDTO();
            dto.setUserId(unverifyNumberForm.getUserId());
            dto.setDialingCode(unverifyNumberForm.getDialingCode());
            dto.setMobileNumber(unverifyNumberForm.getMobileNumber());
            RingLogger.getActivityLogger().debug("[UnverifyNumberAction] activistName : " + activistName + " userId: " + dto.getUserId() + " mobileNumber: " + dto.getMobileNumber() + " dialinigCode: " + dto.getDialingCode());
            int result = UserService.getInstance().unverifyMyNumber(dto);
            if (result == 0) {
                err.add("errormsg", new ActionMessage("Unverify Number successful ", false));
                MyAppError myAppError = UserService.getInstance().updatePhoneNumberInfo(dto.getMobileNumber(), dto.getDialingCode(), dto.getUserId(), verified);
                if (myAppError.getERROR_TYPE() > 0) {
                    logger.error("database error " + myAppError.getERROR_TYPE());
                    request.setAttribute("errormsg", "<span style='color:red'>Unverify phone number failed!</span>");
                    target = FAILURE;
                }
            } else {
                err.add("errormsg", new ActionMessage(Utils.getMessageForReasonCode(result), false));
                target = FAILURE;
            }
            saveErrors(request, err);
        } catch (Exception e) {
            target = FAILURE;
        }
        return mapping.findForward(target);
    }
}
