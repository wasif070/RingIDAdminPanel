/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.ringId.verifyNumber;

import com.ringid.admin.BaseForm;
import com.ringid.admin.dto.adminAuth.LoginDTO;
import com.ringid.admin.utils.Constants;
import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

/**
 *
 * @author Rabby
 */
public class VerifyNumberForm extends BaseForm {

    private String userId;
    private String mobileNumber;
    private String dialingCode;
    private String prefix;

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public String getDialingCode() {
        return dialingCode;
    }

    public void setDialingCode(String dialingCode) {
        this.dialingCode = dialingCode;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    @Override
    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        ActionErrors errors = new ActionErrors();
        LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        if (loginDTO == null) {
            errors.add("auth", new ActionMessage("UnAuthorized Access"));
        } else {
            if(getSubmitType() == null || !getSubmitType().equals("prefix")){
                if (getUserId() == null || getUserId().length() == 0) {
                    errors.add("userId", new ActionMessage("errors.data.required"));
                }
                if (getDialingCode() == null || getDialingCode().length() == 0) {
                    errors.add("dialingCode", new ActionMessage("errors.data.required"));
                }
                if (getMobileNumber() == null || getMobileNumber().equals("")) {
                    errors.add("mobileNumber", new ActionMessage("errors.data.required"));
                }
            }
        }
        return errors;
    }
}
