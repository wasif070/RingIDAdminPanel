/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.ringId.searchUser;

import java.util.Comparator;
import org.ringid.users.UserBasicInfoDTO;

/**
 *
 * @author ipvision
 */
public class UserDetailsDTO {

    private long ringId;
    private long utid;
    private String fullName;
    private String profileImageUrl;
    private String coverImageUrl;
    private String userTypeStr;
    private String gender;
    private String mobilePhone;
    private String dialingCode;
    private String secondaryDialingCode;
    private String email;
    private String birthday;
    private String currentCity;
    private String homeCity;
    private String secondaryEmail;
    private String aboutMe;
    private String secondaryMobileNumber;
    private String country;
    private int userType;
    private String uId;
    private long followerCount;
    private long followingCount;
    private long friendCount;
    private boolean isPageVerified;
    private boolean spamUser;
    private String registrationDateStr;
    private int activeStatus;
    private String activeStatusStr;
    private String lastLoginTime;
    private String lastLoginPlatform;

    public String getLastLoginTime() {
        return lastLoginTime;
    }

    public void setLastLoginTime(String lastLoginTime) {
        this.lastLoginTime = lastLoginTime;
    }

    public String getLastLoginPlatform() {
        return lastLoginPlatform;
    }

    public void setLastLoginPlatform(String lastLoginPlatform) {
        this.lastLoginPlatform = lastLoginPlatform;
    }

    public int getActiveStatus() {
        return activeStatus;
    }

    public void setActiveStatus(int activeStatus) {
        this.activeStatus = activeStatus;
    }

    public String getActiveStatusStr() {
        return activeStatusStr;
    }

    public void setActiveStatusStr(String activeStatusStr) {
        this.activeStatusStr = activeStatusStr;
    }

    public String getRegistrationDateStr() {
        return registrationDateStr;
    }

    public void setRegistrationDateStr(String registrationDateStr) {
        this.registrationDateStr = registrationDateStr;
    }

    public boolean isSpamUser() {
        return spamUser;
    }

    public void setSpamUser(boolean spamUser) {
        this.spamUser = spamUser;
    }

    public boolean getIsPageVerified() {
        return isPageVerified;
    }

    public void setIsPageVerified(boolean isPageVerified) {
        this.isPageVerified = isPageVerified;
    }

    public long getFriendCount() {
        return friendCount;
    }

    public void setFriendCount(long friendCount) {
        this.friendCount = friendCount;
    }

    public long getFollowerCount() {
        return followerCount;
    }

    public void setFollowerCount(long followerCount) {
        this.followerCount = followerCount;
    }

    public long getFollowingCount() {
        return followingCount;
    }

    public void setFollowingCount(long followingCount) {
        this.followingCount = followingCount;
    }

    public String getUserIdentity() {
        return uId;
    }

    public void setUserIdentity(String uId) {
        this.uId = uId;
    }

    public String getUserTypeStr() {
        return userTypeStr;
    }

    public void setUserTypeStr(String userTypeStr) {
        this.userTypeStr = userTypeStr;
    }

    public int getUserType() {
        return userType;
    }

    public void setUserType(int userType) {
        this.userType = userType;
    }

    public long getUtid() {
        return utid;
    }

    public void setUtid(long utid) {
        this.utid = utid;
    }

    public long getRingId() {
        return ringId;
    }

    public void setRingId(long ringId) {
        this.ringId = ringId;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getProfileImageUrl() {
        return profileImageUrl;
    }

    public void setProfileImageUrl(String profileImageUrl) {
        this.profileImageUrl = profileImageUrl;
    }

    public String getCoverImageUrl() {
        return coverImageUrl;
    }

    public void setCoverImageUrl(String coverImageUrl) {
        this.coverImageUrl = coverImageUrl;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getMobilePhone() {
        return mobilePhone;
    }

    public void setMobilePhone(String mobilePhone) {
        this.mobilePhone = mobilePhone;
    }

    public String getDialingCode() {
        return dialingCode;
    }

    public void setDialingCode(String dialingCode) {
        this.dialingCode = dialingCode;
    }

    public String getSecondaryDialingCode() {
        return secondaryDialingCode;
    }

    public void setSecondaryDialingCode(String secondaryDialingCode) {
        this.secondaryDialingCode = secondaryDialingCode;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getCurrentCity() {
        return currentCity;
    }

    public void setCurrentCity(String currentCity) {
        this.currentCity = currentCity;
    }

    public String getHomeCity() {
        return homeCity;
    }

    public void setHomeCity(String homeCity) {
        this.homeCity = homeCity;
    }

    public String getSecondaryEmail() {
        return secondaryEmail;
    }

    public void setSecondaryEmail(String secondaryEmail) {
        this.secondaryEmail = secondaryEmail;
    }

    public String getAboutMe() {
        return aboutMe;
    }

    public void setAboutMe(String aboutMe) {
        this.aboutMe = aboutMe;
    }

    public String getSecondaryMobileNumber() {
        return secondaryMobileNumber;
    }

    public void setSecondaryMobileNumber(String secondaryMobileNumber) {
        this.secondaryMobileNumber = secondaryMobileNumber;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public static class CompUserIdDSC implements Comparator<UserBasicInfoDTO> {

        @Override
        public int compare(UserBasicInfoDTO arg0, UserBasicInfoDTO arg1) {
            return Long.compare(arg1.getUserId(), arg0.getUserId());
        }
    }

    public static class CompUserIdASC implements Comparator<UserBasicInfoDTO> {

        @Override
        public int compare(UserBasicInfoDTO arg0, UserBasicInfoDTO arg1) {
            return Long.compare(arg0.getUserId(), arg1.getUserId());
        }
    }
}
