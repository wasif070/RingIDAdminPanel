/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.ringId.searchUser;

/**
 *
 * @author mamun
 */
public class CallAndChatStatusDTO {

    private int anonymousCallStatus;
    private int anonymousChatStatus;
    private int globalCallStatus;
    private int globalChatStatus;
    private int appType;

    public int getGlobalChatStatus() {
        return globalChatStatus;
    }

    public void setGlobalChatStatus(int globalChatStatus) {
        this.globalChatStatus = globalChatStatus;
    }

    public int getAppType() {
        return appType;
    }

    public void setAppType(int appType) {
        this.appType = appType;
    }

    public int getAnonymousCallStatus() {
        return anonymousCallStatus;
    }

    public void setAnonymousCallStatus(int anonymousCallStatus) {
        this.anonymousCallStatus = anonymousCallStatus;
    }

    public int getAnonymousChatStatus() {
        return anonymousChatStatus;
    }

    public void setAnonymousChatStatus(int anonymousChatStatus) {
        this.anonymousChatStatus = anonymousChatStatus;
    }

    public int getGlobalCallStatus() {
        return globalCallStatus;
    }

    public void setGlobalCallStatus(int globalCallStatus) {
        this.globalCallStatus = globalCallStatus;
    }

}
