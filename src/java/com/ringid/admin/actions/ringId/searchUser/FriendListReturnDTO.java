/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.ringId.searchUser;

import java.util.ArrayList;
import org.ringid.users.UserDTO;

/**
 *
 * @author Rabby
 */
public class FriendListReturnDTO {

    private long nextPivotId;
    private ArrayList<UserDTO> friendList;

    public long getNextPivotId() {
        return nextPivotId;
    }

    public void setNextPivotId(long nextPivotId) {
        this.nextPivotId = nextPivotId;
    }

    public ArrayList<UserDTO> getFriendList() {
        return friendList;
    }

    public void setFriendList(ArrayList<UserDTO> friendList) {
        this.friendList = friendList;
    }

}
