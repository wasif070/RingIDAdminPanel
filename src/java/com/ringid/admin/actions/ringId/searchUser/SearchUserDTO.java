/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.ringId.searchUser;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.ringid.contacts.SearchedContactDTO;
import org.ringid.feedbacks.WalletFeedBack;
import org.ringid.newsfeeds.FeedReturnDTO;
import org.ringid.pages.ServiceDTO;
import org.ringid.users.UserBasicInfoDTO;
import org.ringid.users.UserDeviceDTO;

/**
 *
 * @author Rabby
 */
public class SearchUserDTO {

    private UserDetailsDTO userDetailsDTO;
    private UserBasicInfoDTO pageOwnerInfo;
    private ArrayList<UserDeviceDTO> userDeviceList;
    private HashMap<Integer, List<ServiceDTO>> userPageMap;
    private List<SearchedContactDTO> searchResultPeople;
    private List<SearchedContactDTO> searchResultPage;
    private FeedReturnDTO feedReturnDTO;
    private FeedReturnDTO celebrityPastLiveFeed;
    private List<CallAndChatStatusDTO> callAndChatStatusDTOs;
    private WalletFeedBack walletFeedBack;

    public WalletFeedBack getWalletFeedBack() {
        return walletFeedBack;
    }

    public void setWalletFeedBack(WalletFeedBack walletFeedBack) {
        this.walletFeedBack = walletFeedBack;
    }

    public List<CallAndChatStatusDTO> getCallAndChatStatusDTOs() {
        return callAndChatStatusDTOs;
    }

    public void setCallAndChatStatusDTOs(List<CallAndChatStatusDTO> callAndChatStatusDTOs) {
        this.callAndChatStatusDTOs = callAndChatStatusDTOs;
    }

    public FeedReturnDTO getCelebrityPastLiveFeed() {
        return celebrityPastLiveFeed;
    }

    public void setCelebrityPastLiveFeed(FeedReturnDTO celebrityPastLiveFeed) {
        this.celebrityPastLiveFeed = celebrityPastLiveFeed;
    }

    public FeedReturnDTO getFeedReturnDTO() {
        return feedReturnDTO;
    }

    public void setFeedReturnDTO(FeedReturnDTO feedReturnDTO) {
        this.feedReturnDTO = feedReturnDTO;
    }

    public List<SearchedContactDTO> getSearchResultPeople() {
        return searchResultPeople;
    }

    public void setSearchResultPeople(List<SearchedContactDTO> searchResultPeople) {
        this.searchResultPeople = searchResultPeople;
    }

    public List<SearchedContactDTO> getSearchResultPage() {
        return searchResultPage;
    }

    public void setSearchResultPage(List<SearchedContactDTO> searchResultPage) {
        this.searchResultPage = searchResultPage;
    }

    public HashMap<Integer, List<ServiceDTO>> getUserPageMap() {
        return userPageMap;
    }

    public void setUserPageMap(HashMap<Integer, List<ServiceDTO>> userPageMap) {
        this.userPageMap = userPageMap;
    }

    public UserDetailsDTO getUserDetailsDTO() {
        return userDetailsDTO;
    }

    public void setUserDetailsDTO(UserDetailsDTO userDetailsDTO) {
        this.userDetailsDTO = userDetailsDTO;
    }

    public UserBasicInfoDTO getPageOwnerInfo() {
        return pageOwnerInfo;
    }

    public void setPageOwnerInfo(UserBasicInfoDTO pageOwnerInfo) {
        this.pageOwnerInfo = pageOwnerInfo;
    }

    public ArrayList<UserDeviceDTO> getUserDeviceList() {
        return userDeviceList;
    }

    public void setUserDeviceList(ArrayList<UserDeviceDTO> userDeviceList) {
        this.userDeviceList = userDeviceList;
    }

}
