/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.ringId.searchUser;

import com.ringid.admin.BaseForm;
import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author Rabby
 */
public class SearchUserForm extends BaseForm {

    private String id;
    private SearchUserDTO searchUserDTO;
    private boolean searchByRingId;
    private String userId;
    private String ringId;
    private String ownerId;
    private int userType;
    private String pageStatusUpdateType;
    private String[] countriesISO;
    private String country;
    private int viewType;
    private long pageId;

    public long getPageId() {
        return pageId;
    }

    public void setPageId(long pageId) {
        this.pageId = pageId;
    }

    public int getViewType() {
        return viewType;
    }

    public void setViewType(int viewType) {
        this.viewType = viewType;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String[] getCountriesISO() {
        return countriesISO;
    }

    public void setCountriesISO(String[] countriesISO) {
        this.countriesISO = countriesISO;
    }

    public String getPageStatusUpdateType() {
        return pageStatusUpdateType;
    }

    public void setPageStatusUpdateType(String pageStatusUpdateType) {
        this.pageStatusUpdateType = pageStatusUpdateType;
    }

    public int getUserType() {
        return userType;
    }

    public void setUserType(int userType) {
        this.userType = userType;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getRingId() {
        return ringId;
    }

    public void setRingId(String ringId) {
        this.ringId = ringId;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    public boolean isSearchByRingId() {
        return searchByRingId;
    }

    public void setSearchByRingId(boolean searchByRingId) {
        this.searchByRingId = searchByRingId;
    }

    public SearchUserDTO getSearchUserDTO() {
        return searchUserDTO;
    }

    public void setSearchUserDTO(SearchUserDTO searchUserDTO) {
        this.searchUserDTO = searchUserDTO;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        ActionErrors errors = new ActionErrors();

        return errors;
    }
}
