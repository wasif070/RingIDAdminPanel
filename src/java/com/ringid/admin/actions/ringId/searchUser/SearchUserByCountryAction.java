/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.ringId.searchUser;

import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.service.AdminService;
import com.ringid.admin.service.UserService;
import com.ringid.admin.utils.Constants;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.ringid.users.UserBasicInfoDTO;

/**
 *
 * @author mamun
 */
public class SearchUserByCountryAction extends BaseAction {

    Logger LOGGER = RingLogger.getConfigPortalLogger();
    List<UserBasicInfoDTO> userBasicInfoDTOs;

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = SUCCESS;
        try {
            SearchUserForm searchUserForm = (SearchUserForm) form;
            if (request.getParameter("ForceReload") != null && "1".equals(request.getParameter("ForceReload"))) {
                SearchUserLoader.getInstance().forceReload();
                userBasicInfoDTOs = new ArrayList<>();
            }
            if (userBasicInfoDTOs != null && userBasicInfoDTOs.size() > 0 && searchUserForm.getAction() != Constants.SEARCH && searchUserForm.getColumn() > 0) {
                managePages(request, searchUserForm, userBasicInfoDTOs.size());
                int fromIndex = (searchUserForm.getPageNo() - 1) * searchUserForm.getRecordPerPage();
                int toIndex = (searchUserForm.getPageNo()) * searchUserForm.getRecordPerPage();
                switch (searchUserForm.getColumn()) {
                    case Constants.COLUMN_ONE:
                        if (searchUserForm.getSort() == Constants.ASC_SORT) {
                            Collections.sort(userBasicInfoDTOs, new UserDetailsDTO.CompUserIdASC());
                        } else {
                            Collections.sort(userBasicInfoDTOs, new UserDetailsDTO.CompUserIdDSC());
                        }
                        break;
                    default:
                        if (searchUserForm.getSort() == Constants.ASC_SORT) {
                            Collections.sort(userBasicInfoDTOs, new UserDetailsDTO.CompUserIdASC());
                        } else {
                            Collections.sort(userBasicInfoDTOs, new UserDetailsDTO.CompUserIdDSC());
                        }
                        searchUserForm.setColumn(Constants.COLUMN_ONE);
                        break;
                }
                LOGGER.debug("[SearchUserByCountryAction] action --> " + searchUserForm.getAction() + " userBasicInfoDTOs.size --> " + userBasicInfoDTOs.size() + " column -- > " + searchUserForm.getColumn());
                request.getSession(true).setAttribute(Constants.DATA_ROWS, userBasicInfoDTOs.subList(fromIndex, toIndex > userBasicInfoDTOs.size() ? userBasicInfoDTOs.size() : toIndex));
                return mapping.findForward(target);
            } else if (userBasicInfoDTOs != null && userBasicInfoDTOs.size() > 0 && (searchUserForm.getAction() == Constants.PAGE_NAVIGATION || searchUserForm.getAction() == Constants.RECORD_PER_PAGE_CHANGE)) {
                managePages(request, searchUserForm, userBasicInfoDTOs.size());
                int fromIndex = (searchUserForm.getPageNo() - 1) * searchUserForm.getRecordPerPage();
                int toIndex = (searchUserForm.getPageNo()) * searchUserForm.getRecordPerPage();
                LOGGER.debug("[SearchUserByCountryAction] action --> " + searchUserForm.getAction() + " userBasicInfoDTOs.size --> " + userBasicInfoDTOs.size());
                request.getSession(true).setAttribute(Constants.DATA_ROWS, userBasicInfoDTOs.subList(fromIndex, toIndex > userBasicInfoDTOs.size() ? userBasicInfoDTOs.size() : toIndex));
                return mapping.findForward(target);
            }
            userBasicInfoDTOs = UserService.getInstance().getUserListByCountryCode(searchUserForm.getCountry());
            LOGGER.debug("[SearchUserByCountryAction] action --> " + searchUserForm.getAction() + " userBasicInfoDTOs.size --> " + userBasicInfoDTOs.size());
            searchUserForm.setPageNo(1);
            searchUserForm.setRecordPerPage(50);
            managePages(request, searchUserForm, userBasicInfoDTOs.size());
            int fromIndex = (searchUserForm.getPageNo() - 1) * searchUserForm.getRecordPerPage();
            int toIndex = (searchUserForm.getPageNo()) * searchUserForm.getRecordPerPage();
            Collections.sort(userBasicInfoDTOs, new UserDetailsDTO.CompUserIdASC());
            searchUserForm.setColumn(Constants.COLUMN_ONE);
            searchUserForm.setSort(Constants.ASC_SORT);
            request.getSession(true).setAttribute(Constants.DATA_ROWS, userBasicInfoDTOs.subList(fromIndex, toIndex > userBasicInfoDTOs.size() ? userBasicInfoDTOs.size() : toIndex));
        } catch (Exception e) {
            target = FAILURE;
        }
        return mapping.findForward(target);
    }
}
