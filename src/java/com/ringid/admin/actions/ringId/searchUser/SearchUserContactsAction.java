/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.ringId.searchUser;

import com.ringid.admin.BaseAction;
import com.ringid.admin.service.AdminService;
import com.ringid.admin.service.UserService;
import com.ringid.admin.utils.Constants;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author Rabby
 */
public class SearchUserContactsAction extends BaseAction {

    private final String SINGLE_CONTACT = "singleContact";

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = SUCCESS;
        try {
            SearchUserForm searchUserForm = (SearchUserForm) form;
            SearchUserDTO searchResult = UserService.getInstance().searchUserContacts(searchUserForm.getSearchText().trim(), 0, 10);
            searchUserForm.setSearchText(searchUserForm.getSearchText().trim());
            request.getSession(true).setAttribute(Constants.DATA_ROWS, searchResult);
        } catch (Exception e) {
            target = FAILURE;
        }
        return mapping.findForward(target);
    }
}
