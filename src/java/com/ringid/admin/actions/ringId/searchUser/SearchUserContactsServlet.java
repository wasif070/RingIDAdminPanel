/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.ringId.searchUser;

import com.google.gson.Gson;
import com.ipvision.ringid.storage.coin.dto.CoinLogDTO;
import com.ringid.admin.actions.ringId.postStatus.Upload;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.actions.ringId.resetPassword.ResetPasswordDTO;
import com.ringid.admin.cassandra.CassandraDAO;
import com.ringid.admin.dto.adminAuth.LoginDTO;
import com.ringid.admin.service.CoinService;
import com.ringid.admin.service.LiveStreamingService;
import com.ringid.admin.service.NewsFeedService;
import com.ringid.admin.service.UserService;
import com.ringid.admin.utils.Constants;
import com.ringid.admin.utils.HTTPStatusCodes;
import com.ringid.admin.utils.ReasonCode;
import com.ringid.admin.utils.TimeUUID;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.UUID;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import org.ringid.contacts.SearchedContactDTO;
import org.ringid.newsfeeds.FeedDTO;
import org.ringid.newsfeeds.FeedReturnDTO;
import org.ringid.utilities.AppConstants;
import ringid.LoginTaskScheduler;

/**
 *
 * @author Rabby
 */
public class SearchUserContactsServlet extends HttpServlet {

    Logger logger = RingLogger.getConfigPortalLogger();
    private static final String FAILED = "{\"sucs\": false, \"ms\": \"Operation failed\"}";
    private static final String SUCCESS = "{\"sucs\": true, \"ms\": \"Operation successfull\"}";
    private static final String EXC = "{\"sucs\": false, \"ms\": \"Exception..\"}";
    private static final String INVALID_DATA = "{\"sucs\": false, \"ms\": \"Invalid data\"}";
    private static final int REQ_TYPE_SEARCH_CONTACTS = 1;
    private static final int REQ_TYPE_NEWSFEED = 2;
    private static final int REQ_TYPE_ADD_PAST_LIVE_FEED = 3;
    private static final int REQ_TYPE_REMOVE_PAST_LIVE_FEED = 4;
    private static final int REQ_TYPE_FRIEND_LIST = 5;
    private static final int REQ_TYPE_CHANGE_PASSWORD = 6;
    private static final int REQ_TYPE_WALLET_INFO = 7;
    private static final int REQ_TYPE_COIN_TRANSACTION = 8;
    private static final int REQ_TYPE_EDIT_STATUS = 9;

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doProcess(request, response);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doProcess(req, resp);
    }
    // </editor-fold>

    private void doProcess(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("application/json;charset=UTF-8");
        String result = FAILED;
        String searchText = "";
        int requestType = 0;
        int start = 0;
        int limit = 0;
        int type = 0;
        try {
            if (containsParameter(request, "reqType")) {
                requestType = Integer.parseInt(request.getParameter("reqType"));
            }
            switch (requestType) {
                case REQ_TYPE_EDIT_STATUS: {
                    JSONObject message = new JSONObject();
                    long wallOwnerId = 0;
                    UUID feedId = null;
                    int reasonCode = -1;
                    String expiredAfter = "", cloudmedia = "";
                    if (containsParameter(request, "wallOwnerId")) {
                        wallOwnerId = Long.parseLong(request.getParameter("wallOwnerId"));
                    }
                    if (containsParameter(request, "feedId")) {
                        feedId = UUID.fromString(request.getParameter("feedId"));
                    }
                    if (containsParameter(request, "expiredAfter")) {
                        expiredAfter = request.getParameter("expiredAfter");
                    }
                    if (containsParameter(request, "mediaurl")) {
                        cloudmedia = request.getParameter("mediaurl");
                    }
                    if (expiredAfter.length() > 0) {
                        long currentTime = System.currentTimeMillis(), datetime = 0;
                        String activistName = "", validity = "";
                        try {
                            LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(com.ringid.admin.utils.Constants.LOGIN_DTO);
                            activistName = loginDTO.getUserName();
                        } catch (Exception e) {
                            RingLogger.getConfigPortalLogger().error("Exception in [RecordedLiveFeedDeleteAction] " + e);
                        }
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        if (!expiredAfter.equals("-1")) {
                            datetime = sdf.parse(expiredAfter).getTime();
                            validity = LiveStreamingService.getInstance().getCloudValidity(datetime, currentTime);
                        }
                        LoginTaskScheduler.getInstance().login(com.ringid.admin.utils.Constants.USERID, com.ringid.admin.utils.Constants.PASSWORD);
                        String mediaUrl = new Upload().restoreMediaCloud(com.ringid.admin.utils.Constants.USERID, com.ringid.admin.utils.Constants.RESTORE_LINK_CLOUD_MEDIA, cloudmedia);
                        System.out.println("[restoreMediaCloud] response --> " + mediaUrl);
                        JSONObject jSONObject = new JSONObject(mediaUrl);
                        if (jSONObject.getInt("code") == HTTPStatusCodes.ACCEPTED) {
                            if (validity.length() > 0) {
                                mediaUrl = new Upload().deleteFromCloud(com.ringid.admin.utils.Constants.USERID, com.ringid.admin.utils.Constants.DELETE_LINK_CLOUD_MEDIA + cloudmedia, validity);
                                System.out.println("[deleteFromCloud] response --> " + mediaUrl);
                            }
                            jSONObject = new JSONObject(mediaUrl);
                            if (jSONObject.getInt("code") == HTTPStatusCodes.NO_CONTENT || jSONObject.getInt("code") == HTTPStatusCodes.ACCEPTED) {
                                FeedDTO feedDTO = new FeedDTO();
                                feedDTO.setFeedId(feedId);
                                if (datetime > 0) {
                                    feedDTO.setExpiredAfter(datetime);
                                } else {
                                    feedDTO.setExpiredAfter(Long.MAX_VALUE);
                                }
                                RingLogger.getConfigPortalLogger().debug("[RecordedLiveFeedDeleteAction] sessionUserId --> " + wallOwnerId + " feedDTO --> " + new Gson().toJson(feedDTO));
                                RingLogger.getActivityLogger().debug("[RecordedLiveFeedDeleteAction] activistName : " + activistName + " sessionUserId --> " + wallOwnerId + " feedDTO --> " + new Gson().toJson(feedDTO));
                                reasonCode = NewsFeedService.getInstance().editStatus(feedDTO, wallOwnerId);
                                if (reasonCode == ReasonCode.NONE) {
                                    message.put("sucs", true);
                                    message.put("ms", "Edit Status Successfull");
                                } else {
                                    mediaUrl = new Upload().restoreMediaCloud(com.ringid.admin.utils.Constants.USERID, com.ringid.admin.utils.Constants.RESTORE_LINK_CLOUD_MEDIA, cloudmedia);
                                    System.out.println("[restoreMediaCloud] response --> " + mediaUrl);
                                    message.put("sucs", false);
                                    message.put("ms", "Edit Status Failed!!! (rc: " + reasonCode + ")");
                                }
                            } else {
                                message.put("sucs", false);
                                message.put("ms", "Status Post Failed!!! (rc: delete media failed)");
                            }
                            LoginTaskScheduler.getInstance().logout(com.ringid.admin.utils.Constants.USERID);
                        } else {
                            LoginTaskScheduler.getInstance().logout(com.ringid.admin.utils.Constants.USERID);
                            message.put("sucs", false);
                            message.put("ms", "Status Post Failed!!! (rc: restore media failed)");
                        }
                    }
                    result = message.toString();
                    break;
                }
                case REQ_TYPE_COIN_TRANSACTION: {
                    long userId = 0;
                    UUID pivotId = null;
                    boolean isGift = false;
                    if (containsParameter(request, "utId")) {
                        userId = Long.parseLong(request.getParameter("utId"));
                    }
                    if (containsParameter(request, "pvtId")) {
                        pivotId = UUID.fromString(request.getParameter("pvtId"));
                    }
                    if (containsParameter(request, "lmt")) {
                        limit = Integer.parseInt(request.getParameter("lmt"));
                    }
                    if (containsParameter(request, "gft")) {
                        isGift = Boolean.parseBoolean(request.getParameter("gft"));
                    }
                    List<CoinLogDTO> coinLogDTOs = CoinService.getInstance().getCoinTransaction(userId, pivotId, isGift, limit);
                    if (coinLogDTOs == null) {
                        result = new Gson().toJson(new CoinLogDTO());
                    } else {
                        result = new Gson().toJson(coinLogDTOs);
                    }
                    break;
                }
                case REQ_TYPE_WALLET_INFO: {
                    long userId = 0;
                    if (containsParameter(request, "utId")) {
                        userId = Long.parseLong(request.getParameter("utId"));
                    }
                    String res = CoinService.getInstance().getWalletInfo(userId);
                    if (res != null) {
                        result = res;
                    }
                    break;
                }
                case REQ_TYPE_CHANGE_PASSWORD: {
                    String ringId = null;
                    String password = null;
                    if (containsParameter(request, "ringId")) {
                        ringId = request.getParameter("ringId");
                    }
                    if (containsParameter(request, "newPassword")) {
                        password = request.getParameter("newPassword");
                    }
                    ResetPasswordDTO resetPasswordDTO = new ResetPasswordDTO();
                    resetPasswordDTO.setUserId(ringId);
                    resetPasswordDTO.setPassword(password);
                    auth.com.utils.MyAppError error = UserService.getInstance().resetPassword(resetPasswordDTO);

                    SearchUserFeedbackDTO feedBackDTO = new SearchUserFeedbackDTO();
                    if (error.getERROR_TYPE() == auth.com.utils.MyAppError.NOERROR) {
                        feedBackDTO.setSucs(true);
                        feedBackDTO.setMessage("Operation Successfull");
                    } else {
                        feedBackDTO.setSucs(false);
                        feedBackDTO.setMessage(error.getErrorMessage());
                    }
                    result = new Gson().toJson(feedBackDTO);
                    break;
                }
                case REQ_TYPE_FRIEND_LIST: {
                    long userId = 0;
                    long pivotId = 0;
                    if (containsParameter(request, "utId")) {
                        userId = Long.parseLong(request.getParameter("utId"));
                    }
                    if (containsParameter(request, "pvtId")) {
                        pivotId = Long.parseLong(request.getParameter("pvtId"));
                    }
                    if (containsParameter(request, "lmt")) {
                        limit = Integer.parseInt(request.getParameter("lmt"));
                    }
                    FriendListReturnDTO dto = UserService.getInstance().getFriendList(userId, pivotId, limit);
                    if (dto == null) {
                        result = new Gson().toJson(new FriendListReturnDTO());
                    } else {
                        result = new Gson().toJson(dto);
                    }
                    break;
                }
                case REQ_TYPE_ADD_PAST_LIVE_FEED: {
                    UUID liveTimeUUID = TimeUUID.createUUID(System.currentTimeMillis());
                    UUID feedId = null;
                    int action = 0;
                    int makeFeatured = 0;
                    if (containsParameter(request, "mkFetr")) {
                        makeFeatured = Integer.parseInt(request.getParameter("mkFetr"));
                    }
                    if (containsParameter(request, "actn")) {
                        action = Integer.parseInt(request.getParameter("actn"));
                    }
                    if (containsParameter(request, "liveTimeUUID")) {
                        liveTimeUUID = UUID.fromString(request.getParameter("liveTimeUUID"));
                    }
                    if (containsParameter(request, "feedId")) {
                        feedId = UUID.fromString(request.getParameter("feedId"));
                    }
                    int reasonCode = LiveStreamingService.getInstance().addPastLiveFeed(liveTimeUUID, feedId, action, makeFeatured);
                    if (reasonCode == 0) {
                        result = SUCCESS;
                    } else {
                        result = FAILED;
                    }
                    break;
                }
                case REQ_TYPE_REMOVE_PAST_LIVE_FEED: {
                    UUID liveTimeUUID = TimeUUID.createUUID(System.currentTimeMillis());
                    UUID feedId = null;
                    int action = 0;
                    int removeFeatured = 0;
                    if (containsParameter(request, "mkFetr")) {
                        removeFeatured = Integer.parseInt(request.getParameter("mkFetr"));
                    }
                    if (containsParameter(request, "actn")) {
                        action = Integer.parseInt(request.getParameter("actn"));
                    }
                    if (containsParameter(request, "liveTimeUUID")) {
                        liveTimeUUID = UUID.fromString(request.getParameter("liveTimeUUID"));
                    }
                    if (containsParameter(request, "feedId")) {
                        feedId = UUID.fromString(request.getParameter("feedId"));
                    }
                    int reasonCode = LiveStreamingService.getInstance().removePastLiveFeed(liveTimeUUID, feedId, action, removeFeatured);
                    if (reasonCode == 0) {
                        result = SUCCESS;
                    } else {
                        result = FAILED;
                    }
                    break;
                }
                case REQ_TYPE_NEWSFEED: {
                    long userId = 0;
                    UUID pivotFeedId = null;
                    int action = AppConstants.FeedPullAction.OWN_WALL_FEED;
                    if (containsParameter(request, "actn")) {
                        action = Integer.parseInt(request.getParameter("actn"));
                    }
                    if (action != AppConstants.FeedPullAction.OWN_WALL_FEED) {
                        userId = Constants.LOGGED_IN_USER_ID;
                    }
                    if (containsParameter(request, "utId")) {
                        userId = Long.parseLong(request.getParameter("utId"));
                    }
                    if (containsParameter(request, "pvtUUID")) {
                        pivotFeedId = UUID.fromString(request.getParameter("pvtUUID"));
                    }
                    if (containsParameter(request, "lmt")) {
                        limit = Integer.parseInt(request.getParameter("lmt"));
                    }
                    long wallOwnerId = 0;
                    if (containsParameter(request, "searchText")) {
                        wallOwnerId = CassandraDAO.getInstance().getUserTableId(Long.parseLong(request.getParameter("searchText").trim().replace(" ", "")));
                        if (wallOwnerId <= 0) {
                            wallOwnerId = Long.parseLong(request.getParameter("searchText").trim().replace(" ", ""));
                        }
                    }
                    if (wallOwnerId <= 0) {
                        wallOwnerId = userId;
                    } else if (action == AppConstants.FeedPullAction.TOP_ROOM_LIVE_FEED) {
                        action = AppConstants.FeedPullAction.TOP_ROOM_LIVE_FEED_BY_USER;
                    } else if (action == AppConstants.FeedPullAction.PAST_LIVE_FEED) {
                        action = AppConstants.FeedPullAction.CELEBRITY_PAST_LIVE_FEED;
                    }

                    if (userId > 0) {
                        FeedReturnDTO feedReturnDTO = NewsFeedService.getInstance().getNewsFeedList(userId, wallOwnerId, pivotFeedId, action, limit);
                        if (feedReturnDTO != null) {
                            result = new Gson().toJson(feedReturnDTO);
                        } else {
                            result = new Gson().toJson(new FeedReturnDTO());
                        }
                    }
                    break;
                }
                case REQ_TYPE_SEARCH_CONTACTS:
                default: {
                    if (containsParameter(request, "searchText")) {
                        searchText = request.getParameter("searchText");
                    }
                    if (containsParameter(request, "st")) {
                        start = Integer.parseInt(request.getParameter("st"));
                    }
                    if (containsParameter(request, "lmt")) {
                        limit = Integer.parseInt(request.getParameter("lmt"));
                    }
                    if (containsParameter(request, "type")) {
                        type = Integer.parseInt(request.getParameter("type"));
                    }
                    List<SearchedContactDTO> searchResult = UserService.getInstance().getSearchResult(type, searchText.trim(), start, limit);
                    result = new Gson().toJson(searchResult);
                    break;
                }
            }
        } catch (Exception ex) {
            logger.error("Error in SearchUserContactsServlet: " + ex.getMessage());
            response.getWriter().write(EXC);
            return;
        }
        response.getWriter().write(result);
    }

    private boolean containsParameter(HttpServletRequest request, String parameter) {
        return request.getParameter(parameter) != null && request.getParameter(parameter).trim().length() > 0;
    }
}
