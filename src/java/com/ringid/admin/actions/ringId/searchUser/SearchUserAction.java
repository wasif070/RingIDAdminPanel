/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.ringId.searchUser;

import com.ringid.admin.BaseAction;
import com.ringid.admin.service.UserService;
import com.ringid.admin.utils.ReasonCode;
import com.ringid.admin.utils.Utils;
import com.ringid.admin.utils.log.RingLogger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author Rabby
 */
public class SearchUserAction extends BaseAction {

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = SUCCESS;
        String searchText = null, submitType = null;
        try {
            SearchUserForm searchUserForm = (SearchUserForm) form;
            searchText = searchUserForm.getSearchText();
            submitType = searchUserForm.getSubmitType();
            if (searchText != null && searchText.length() > 0) {
                //Deactivate
                if (submitType != null && submitType.length() > 0) {
                    switch (submitType) {
                        case "Deactivate":
                            int reasonCode = UserService.getInstance().deactivateUserAccount(searchText);
                            if (reasonCode == ReasonCode.NONE) {
                                request.setAttribute("message", "<span style='color: green'> Operation Successfull</span>");
                            } else {
                                request.setAttribute("message", "<span style='color: red'> Operation Failed " + Utils.getMessageForReasonCode(reasonCode) + " </span>");
                            }
                            break;
                    }
                }
                //Search
                SearchUserDTO searchUserDTO = UserService.getInstance().searchUserInfo(searchUserForm.getSearchText().trim());
                if (searchUserDTO.getUserDetailsDTO() == null) {
                    request.setAttribute("message", "<span style='color: red'>User Details not found for id : " + searchUserForm.getSearchText().trim() + "</span>");
                } else {
                    searchUserForm.setSearchUserDTO(searchUserDTO);
                    searchUserForm.setUserType(searchUserDTO.getUserDetailsDTO().getUserType());
                }
            } else {
                RingLogger.getConfigPortalLogger().info("submitType -> " + submitType + " -> searchText -> " + searchText);
            }
        } catch (Exception e) {
            target = FAILURE;
            RingLogger.getConfigPortalLogger().info("Exception : <searchText> : " + searchText + " <submitType> : " + submitType, e);
            request.setAttribute("message", "<span style='color: red'> Internal Error. " + e + " </span>");

        }
        return mapping.findForward(target);
    }
}
