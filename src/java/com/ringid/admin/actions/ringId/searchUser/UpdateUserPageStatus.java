/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.ringId.searchUser;

import com.ringid.admin.BaseAction;
import com.ringid.admin.service.CelebrityService;
import com.ringid.admin.service.PageService;
import com.ringid.admin.service.UserService;
import com.ringid.admin.utils.ReasonCode;
import com.ringid.admin.utils.Utils;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.ringid.contacts.FollowRuleDTO;

/**
 *
 * @author mamun
 */
public class UpdateUserPageStatus extends BaseAction {

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = SUCCESS;
        SearchUserForm searchUserForm = (SearchUserForm) form;
        try {
            if (searchUserForm.getUserId() != null && searchUserForm.getUserId().length() > 0) {
                FollowRuleDTO dto = new FollowRuleDTO();
                Set<String> countryIso = null;
                if (searchUserForm.getCountriesISO() != null) {
                    countryIso = new HashSet<>(Arrays.asList(searchUserForm.getCountriesISO()));
                }
                switch (searchUserForm.getPageStatusUpdateType()) {
                    case "makeDefault": {
                        int reasonCode = PageService.getInstance().makePageDefault(Long.parseLong(searchUserForm.getUserId().trim()), searchUserForm.getPageId());
                        if (reasonCode == ReasonCode.NONE) {
                            request.setAttribute("message", "<span style='color: green'> Operation Successfull</span>");
                        } else {
                            request.setAttribute("message", "<span style='color: red'> Operation Failed " + Utils.getMessageForReasonCode(reasonCode) + " </span>");
                        }
                        break;
                    }
                    case "Deactivate": {
                        int reasonCode = UserService.getInstance().deactivateUserAccount(searchUserForm.getUserId());
                        if (reasonCode == ReasonCode.NONE) {
                            request.setAttribute("message", "<span style='color: green'> Operation Successfull</span>");
                        } else {
                            request.setAttribute("message", "<span style='color: red'> Operation Failed " + Utils.getMessageForReasonCode(reasonCode) + " </span>");
                        }
                        break;
                    }
                    case "makeCelebrity": {
                        int reasonCode = CelebrityService.getInstance().makePageCelebrity(Long.parseLong(searchUserForm.getUserId()));
                        if (reasonCode == ReasonCode.NONE) {
                            request.setAttribute("message", "<span style='color: green'> Operation Successfull</span>");
                        } else {
                            request.setAttribute("message", "<span style='color: red'> Operation Failed " + Utils.getMessageForReasonCode(reasonCode) + " </span>");
                        }
                        break;
                    }
                    case "removeCelebrity": {
                        int reasonCode = CelebrityService.getInstance().deleteCelebrityPage(Long.parseLong(searchUserForm.getUserId()), true);
                        if (reasonCode == ReasonCode.NONE) {
                            request.setAttribute("message", "<span style='color: green'> Operation Successfull</span>");
                        } else {
                            request.setAttribute("message", "<span style='color: red'> Operation Failed " + Utils.getMessageForReasonCode(reasonCode) + " </span>");
                        }
                        break;
                    }
                    case "verifyPage": {
                        int reasonCode = PageService.getInstance().verifyPage(Long.parseLong(searchUserForm.getUserId()), searchUserForm.getUserType());
                        if (reasonCode == ReasonCode.NONE) {
                            request.setAttribute("message", "<span style='color: green'> Operation Successfull</span>");
                        } else {
                            request.setAttribute("message", "<span style='color: red'> Operation Failed " + Utils.getMessageForReasonCode(reasonCode) + " </span>");
                        }
                        break;
                    }

                    case "unverifyPage": {
                        int reasonCode = PageService.getInstance().unverifyPage(Long.parseLong(searchUserForm.getUserId()), searchUserForm.getUserType());
                        if (reasonCode == ReasonCode.NONE) {
                            request.setAttribute("message", "<span style='color: green'> Operation Successfull</span>");
                        } else {
                            request.setAttribute("message", "<span style='color: red'> Operation Failed " + Utils.getMessageForReasonCode(reasonCode) + " </span>");
                        }
                        break;
                    }
                }
            }
        } catch (Exception e) {
            target = FAILURE;
        }
        return new ActionForward(mapping.findForward(target).getPath() + "?searchText=" + searchUserForm.getUserId(), true);
    }
}
