/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.ringId.searchUser;

import com.ringid.admin.actions.servers.authServers.AuthServerDTO;
import com.ringid.admin.actions.servers.authServers.AuthServerTaskScheduler;
import com.ringid.admin.cassandra.CassandraDAO;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.utils.Constants;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.ringid.users.UserBasicInfoDTO;
import ringid.sessionless.SessionlessTaskseheduler;

/**
 *
 * @author mamun
 */
public class SearchUserLoader {

    private final static long LOADING_INTERVAL = 24 * 60 * 60 * 1000;
    private long loadingTime = 0;
    private static SearchUserLoader instance = null;
    private final Logger LOGGER = RingLogger.getConfigPortalLogger();
    private Map<String, List<UserBasicInfoDTO>> countryCodeAndUserListMap;

    private SearchUserLoader() {
        countryCodeAndUserListMap = new HashMap<>();
    }

    public static SearchUserLoader getInstance() {
        if (instance == null) {
            createLoader();
        }
        return instance;
    }

    private synchronized static void createLoader() {
        if (instance == null) {
            instance = new SearchUserLoader();
        }
    }

    private void checkForReload() {
        long currentTime = System.currentTimeMillis();
        if (currentTime - loadingTime > LOADING_INTERVAL) {
            loadingTime = currentTime;
            forceLoadData();
        }
    }

    public synchronized void forceReload() {
        loadingTime = System.currentTimeMillis();
        forceLoadData();
    }

    private void forceLoadData() {
        countryCodeAndUserListMap.clear();
        try {
            Map<Long, Long> allUserIds = new HashMap<>();
            ArrayList<AuthServerDTO> authServerList = AuthServerTaskScheduler.getInstance().getAuthServerList(new AuthServerDTO());
            for (AuthServerDTO dto : authServerList) {
                ArrayList<JSONObject> response = SessionlessTaskseheduler.getInstance().getServerUserIds(Constants.USERID, dto.getServerIP(), dto.getStartPort());
                for (JSONObject jSONObject : response) {
                    if (jSONObject.getBoolean("sucs") && jSONObject.has("userIds")) {
                        JSONArray userIds = jSONObject.getJSONArray("userIds");
                        for (int i = 0, len = userIds.length(); i < len; i++) {
                            allUserIds.put(userIds.getLong(i), userIds.getLong(i));
                        }
                    }
                }
            }
            LOGGER.debug("SearchUserLoader [forceLoadData] allUserIds.size --> " + allUserIds.size());
            List<Long> userIdList = new ArrayList<>();
            Iterator it = allUserIds.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry pair = (Map.Entry) it.next();
                userIdList.add((long) pair.getValue());
            }
            List<UserBasicInfoDTO> userBasicInfoDTOs = CassandraDAO.getInstance().getUserBasicInfo(userIdList, false);
            LOGGER.debug("SearchUserLoader [forceLoadData] userBasicInfoDTOs.size --> " + userBasicInfoDTOs.size());
            for (UserBasicInfoDTO userBasicInfoDTO : userBasicInfoDTOs) {
                if (countryCodeAndUserListMap.containsKey(userBasicInfoDTO.getDialingCode())) {
                    countryCodeAndUserListMap.get(userBasicInfoDTO.getDialingCode()).add(userBasicInfoDTO);
                } else {
                    List<UserBasicInfoDTO> list = new ArrayList<>();
                    list.add(userBasicInfoDTO);
                    countryCodeAndUserListMap.put(userBasicInfoDTO.getDialingCode(), list);
                }
            }
        } catch (Exception e) {
            LOGGER.debug("SearchUserLoader [forceLoadData] Exception...", e);
        }
        LOGGER.debug("SearchUserLoader [forceLoadData] countryCodeAndUserListMap.size --> " + countryCodeAndUserListMap.size());
    }

    public List<UserBasicInfoDTO> getUserListByCountryCode(String countryCode) {
        checkForReload();
        List<UserBasicInfoDTO> userBasicInfoDTOs = new ArrayList<>();
        if (countryCodeAndUserListMap.containsKey(countryCode)) {
            userBasicInfoDTOs = countryCodeAndUserListMap.get(countryCode);
        }
        LOGGER.debug("[getUserListByCountryCode] countryCode --> " + countryCode + " userBasicInfoDTOs.size :: " + userBasicInfoDTOs.size());
        return userBasicInfoDTOs;
    }
}
