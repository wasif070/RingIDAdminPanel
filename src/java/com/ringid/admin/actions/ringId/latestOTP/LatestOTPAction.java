/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.ringId.latestOTP;

import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.dto.adminAuth.LoginDTO;
import com.ringid.admin.service.AdminService;
import com.ringid.admin.utils.Utils;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.ringid.utilities.ObjectReturnDTO;

/**
 *
 * @author ipvision
 */
public class LatestOTPAction extends BaseAction {

    Logger logger = RingLogger.getConfigPortalLogger();

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = SUCCESS;
        String activistName;
        try {
            LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(com.ringid.admin.utils.Constants.LOGIN_DTO);
            activistName = loginDTO.getUserName();
        } catch (Exception e) {
            target = UNAUTHORIZED;
            return mapping.findForward(target);
        }
        try {
            LatestOTPForm latestOTPForm = (LatestOTPForm) form;
            RingLogger.getActivityLogger().debug("[ChangeRingIdAction] activistName : " + activistName + "userId --> " + latestOTPForm.getRingId());

            ObjectReturnDTO returnDTO = null;
            if (latestOTPForm.getRingId() != null && latestOTPForm.getRingId().length() > 0) {
                returnDTO = AdminService.getInstance().getLatestOTP(latestOTPForm.getRingId());
                if (returnDTO.getReasonCode() == 0) {
                    latestOTPForm.setLatestOTP(returnDTO.getValue().toString());
                } else {
                    request.setAttribute("msgstr", "<span> " + Utils.getMessageForReasonCode(returnDTO.getReasonCode()) + " </span>");
                }
            } else {
                request.setAttribute("msgstr", "<span> ringId required </span>");
            }
        } catch (Exception e) {
            target = FAILURE;
        }

        return mapping.findForward(target);
    }
}
