/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.ringId.resetRetyLimit;

import com.ringid.admin.BaseForm;
import com.ringid.admin.dto.adminAuth.LoginDTO;
import com.ringid.admin.utils.Constants;
import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

/**
 *
 * @author Rabby
 */
public class ResetRetryLimitForm extends BaseForm {

    private String userId;
    private int resetType;
    private String dialingCode;
    private String mobileNo;
    private String deviceId;

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getDialingCode() {
        return dialingCode;
    }

    public void setDialingCode(String dialingCode) {
        this.dialingCode = dialingCode;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public int getResetType() {
        return resetType;
    }

    public void setResetType(int resetType) {
        this.resetType = resetType;
    }

    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        ActionErrors errors = new ActionErrors();
        LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        if (loginDTO == null) {
            errors.add("auth", new ActionMessage("UnAuthorized Access"));
        }
//        else {
//            switch (getResetType()) {
//                case 3:
//                case 6:
//                    if (getMobileNo() == null || getMobileNo().length() == 0) {
//                        errors.add("mobileNo", new ActionMessage("errors.data.required"));
//                    }
//                    if (getDialingCode() == null || getDialingCode().length() == 0) {
//                        errors.add("dialingCode", new ActionMessage("errors.data.required"));
//                    }
//                    break;
//                case 5:
//                    if (getDeviceId() == null || getDeviceId().length() == 0) {
//                        errors.add("deviceId", new ActionMessage("errors.data.required"));
//                    }
//                    break;
//                default:
//                    if (getUserId() == null || getUserId().length() == 0) {
//                        errors.add("userId", new ActionMessage("errors.data.required"));
//                    }
//                    break;
//            }
//        }
        return errors;
    }
}
