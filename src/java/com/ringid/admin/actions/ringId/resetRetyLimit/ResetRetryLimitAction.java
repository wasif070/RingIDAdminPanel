/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.ringId.resetRetyLimit;

import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.dto.adminAuth.LoginDTO;
import com.ringid.admin.service.AdminService;
import com.ringid.admin.service.UserService;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.ringid.utilities.AppConstants;

/**
 *
 * @author Rabby
 */
public class ResetRetryLimitAction extends BaseAction {

    ActionErrors err = new ActionErrors();

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = SUCCESS;
        err.clear();
        String activistName;
        try {
            LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(com.ringid.admin.utils.Constants.LOGIN_DTO);
            activistName = loginDTO.getUserName();
        } catch (Exception e) {
            target = UNAUTHORIZED;
            return mapping.findForward(target);
        }
        try {
            ResetRetryLimitForm resetRetryLimitForm = (ResetRetryLimitForm) form;
            int result = 1;
            String msg = "";
            long userTableId = 0L;
            if (resetRetryLimitForm.getUserId() != null && resetRetryLimitForm.getUserId().length() > 0) {
                userTableId = Long.valueOf(resetRetryLimitForm.getUserId());
                long tempId = UserService.getInstance().getUserIdFromRingId(resetRetryLimitForm.getUserId());
                if (tempId > 0) {
                    userTableId = tempId;
                }
            }
            RingLogger.getActivityLogger().debug("[ResetRetryLimitAction] activistName : " + activistName + " resetType --> " + resetRetryLimitForm.getResetType() + " ringId --> " + resetRetryLimitForm.getUserId());
            switch (resetRetryLimitForm.getResetType()) {
                case 0:
                    if (userTableId > 0) {
                        result = AdminService.getInstance().resetMNVCRetryLimit(userTableId);
                        msg += "[resetMNVCRetryLimit] rc --> " + result;

                        result = AdminService.getInstance().resetRVCRetryLimit(userTableId);
                        msg += " [resetRVCRetryLimit] rc --> " + result;

                        result = AdminService.getInstance().resetOTPHistory(userTableId);
                        msg += " [resetOTPHistory] rc --> " + result;

                        result = AdminService.getInstance().resetOTPHistoryByDevice(userTableId);
                        msg += " [resetOTPHistoryByDevice] rc --> " + result;

                        result = AdminService.getInstance().resetSecondaryContactVerificationHistory(userTableId, AppConstants.RING_ID);
                        msg += " [resetSecondaryContactVerificationHistory] rc --> " + result;
                    } else {
                        msg += " userId null";
                    }
                    if (resetRetryLimitForm.getDialingCode() != null && resetRetryLimitForm.getDialingCode().length() > 0 && resetRetryLimitForm.getMobileNo() != null && resetRetryLimitForm.getMobileNo().length() > 0) {
                        result = AdminService.getInstance().resetOTPHistoryByNumber(resetRetryLimitForm.getDialingCode() + resetRetryLimitForm.getMobileNo());
                        msg += " [resetOTPHistoryByNumber] rc --> " + result;
                    } else {
                        msg += " dialingCode or mobileNo null";
                    }
                    break;
                case 1:
                    if (userTableId > 0) {
                        result = AdminService.getInstance().resetMNVCRetryLimit(userTableId);
                        msg += "[resetMNVCRetryLimit] rc --> " + result;
                    } else {
                        msg += " userId null";
                    }
                    break;
                case 2:
                    if (userTableId > 0) {
                        result = AdminService.getInstance().resetRVCRetryLimit(userTableId);
                        msg += "[resetRVCRetryLimit] rc --> " + result;
                    } else {
                        msg += " userId null";
                    }
                    break;
                case 3:
                    if (resetRetryLimitForm.getDialingCode() != null && resetRetryLimitForm.getDialingCode().length() > 0 && resetRetryLimitForm.getMobileNo() != null && resetRetryLimitForm.getMobileNo().length() > 0) {
                        result = UserService.getInstance().resetSignupRetryLimit(resetRetryLimitForm.getDialingCode(), resetRetryLimitForm.getMobileNo());
                        msg += "[resetSignupRetryLimit] rc --> " + result;
                    } else {
                        msg += " dialingCode or mobileNo null";
                    }
                    break;
                case 4:
                    if (userTableId > 0) {
                        result = AdminService.getInstance().resetOTPHistory(userTableId);
                        msg += "[resetOTPHistory] rc --> " + result;
                    } else {
                        msg += " userId null";
                    }
                    break;
                case 5:
                    if (resetRetryLimitForm.getDeviceId() != null && resetRetryLimitForm.getDeviceId().length() > 0) {
                        result = AdminService.getInstance().resetOTPHistoryByDeviceId(resetRetryLimitForm.getDeviceId());
                        msg += "[resetOTPHistoryByDeviceId] rc --> " + result;
                    } else {
                        msg += " deviceId null";
                    }
                    break;
                case 6:
                    if (resetRetryLimitForm.getDialingCode() != null && resetRetryLimitForm.getDialingCode().length() > 0 && resetRetryLimitForm.getMobileNo() != null && resetRetryLimitForm.getMobileNo().length() > 0) {
                        result = AdminService.getInstance().resetOTPHistoryByNumber(resetRetryLimitForm.getDialingCode() + resetRetryLimitForm.getMobileNo());
                        msg += "[resetOTPHistoryByNumber] rc --> " + result;
                    } else {
                        msg += " dialingCode or mobileNo null";
                    }
                    break;

                case 7:
                    if (userTableId > 0) {
                        result = AdminService.getInstance().resetSecondaryContactVerificationHistory(userTableId, AppConstants.RING_ID);
                        msg += "[resetSecondaryContactVerificationHistory] rc --> " + result;
                    } else {
                        msg += " userId null";
                    }
                    break;
                case 8:
                    if (resetRetryLimitForm.getDeviceId() != null && resetRetryLimitForm.getDeviceId().length() > 0) {
                        result = AdminService.getInstance().resetDeviceIdVerificationHistory(resetRetryLimitForm.getDeviceId());
                        msg += "[resetDeviceIdVerificationHistory] rc --> " + result;
                    } else {
                        msg += " deviceId null";
                    }
                    break;
            }

            err.add("errormsg", new ActionMessage(msg, false));
            saveErrors(request.getSession(), err);

        } catch (Exception e) {
            target = FAILURE;
        }

        return mapping.findForward(target);
    }
}
