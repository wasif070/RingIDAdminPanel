/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.ringId.autoFollowSetting;

import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.dto.adminAuth.LoginDTO;
import com.ringid.admin.service.PageService;
import com.ringid.admin.utils.Constants;
import com.ringid.admin.utils.MyAppError;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.ringid.utilities.AppConstants;

/**
 *
 * @author mamun
 */
public class AutoFollowSettingAction extends BaseAction {

    private static final Logger logger = RingLogger.getConfigPortalLogger();
    private final String CELEBRITY = "celebrity";
    private final String NEWSPORTAL = "newsportal";
    private final String AUTO_FOLLOW = "autoFollowPages";

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = SUCCESS;
        String discoverable = "";
        String type = "";
        String activistName;
        try {
            LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
            activistName = loginDTO.getUserName();
        } catch (Exception e) {
            target = UNAUTHORIZED;
            return mapping.findForward(target);
        }
        try {
            AutoFollowSettingForm autoFollowSettingForm = (AutoFollowSettingForm) form;
            if (request.getParameter("goback") != null && request.getParameter("goback").equals("newsportal")) {
                target = NEWSPORTAL;
            }
            if (request.getParameter("goback") != null && request.getParameter("goback").equals("celebrity")) {
                target = CELEBRITY;
            }
            if (request.getParameter("goback") != null && request.getParameter("goback").equals("autoFollowPages")) {
                target = AUTO_FOLLOW;
            }
            if (request.getParameter("discoverable") != null) {
                discoverable = request.getParameter("discoverable");
            }
            if (request.getParameter("type") != null) {
                type = request.getParameter("type");
            }
            int responseCode = -1;
            RingLogger.getActivityLogger().debug("Action : CelebrityAction[makeUserPageCelebrity] activistName : " + activistName + " pageId --> " + autoFollowSettingForm.getPageId() + " isEnable --> " + autoFollowSettingForm.getAutoFollowStatus());
            responseCode = PageService.getInstance().enableOrDisablePageAutoFollow(autoFollowSettingForm.getPageId(), autoFollowSettingForm.getAutoFollowStatus());
            if (responseCode == AppConstants.NONE) {
                request.setAttribute("message", "<span style='color:green'> Operation Successfull.</span>");
            } else {
                request.setAttribute("message", "<span style='color:red'> Operation Failed.(resoneCode: ) " + responseCode + "</span>");
            }
        } catch (Exception e) {
            logger.error("exception --> " + e);
            target = FAILURE;
        }
        if (NEWSPORTAL.equals(target)) {
            return new ActionForward(mapping.findForward(target).getPath() + "?ForceReload=1&discoverable=" + discoverable + "&type=" + type, true);
        } else if (CELEBRITY.equals(target)) {
            return new ActionForward(mapping.findForward(target).getPath() + "?type=" + type, true);
        } else if (AUTO_FOLLOW.equals(target)) {
            return new ActionForward(mapping.findForward(target).getPath() + "?type=" + type, true);
        }
        return mapping.findForward(target);
    }
}
