/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.ringId.autoFollowSetting;

import com.ringid.admin.BaseForm;

/**
 *
 * @author mamun
 */
public class AutoFollowSettingForm extends BaseForm {

    private String pageId;
    private int autoFollowStatus;
    private boolean searchByRingId;

    public boolean isSearchByRingId() {
        return searchByRingId;
    }

    public void setSearchByRingId(boolean searchByRingId) {
        this.searchByRingId = searchByRingId;
    }

    public String getPageId() {
        return pageId;
    }

    public void setPageId(String pageId) {
        this.pageId = pageId;
    }

    public int getAutoFollowStatus() {
        return autoFollowStatus;
    }

    public void setAutoFollowStatus(int autoFollowStatus) {
        this.autoFollowStatus = autoFollowStatus;
    }

}
