/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.ringId.VerifiedUnverifiedLivestreamUser;

import com.google.gson.Gson;
import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.dto.adminAuth.LoginDTO;
import com.ringid.admin.service.LiveStreamingService;
import com.ringid.admin.service.UserService;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.ringid.admin.utils.Constants;
import com.ringid.admin.utils.Utils;
import org.ringid.livestream.StreamingDTO;
import org.ringid.utilities.AppConstants;

/**
 *
 * @author Rabby
 */
public class LivestreamUserListAcion extends BaseAction {

    private static final Logger logger = RingLogger.getConfigPortalLogger();
    List<StreamingDTO> userDTOs = new ArrayList<>();
    private String[] userIds;

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {

        String target = SUCCESS;
        String activistName;
        try {
            LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
            activistName = loginDTO.getUserName();
        } catch (Exception e) {
            target = UNAUTHORIZED;
            return mapping.findForward(target);
        }
        try {
            LivestreamUserForm livestreamUserForm = (LivestreamUserForm) form;
            // update operation
            userIds = livestreamUserForm.getSelectedIDs();
            String optionalMsg = "";
            if (livestreamUserForm.getSubmitType() != null && livestreamUserForm.getSubmitType().length() > 0 && userIds != null && userIds.length > 0) {
                int reasonCode = -1;
                RingLogger.getActivityLogger().debug("Action : LivestreamUserAction[" + livestreamUserForm.getSubmitType() + "] activistName : " + activistName + " userIds --> " + new Gson().toJson(userIds));
                switch (livestreamUserForm.getSubmitType()) {
                    case "Unverify":
                        reasonCode = LiveStreamingService.getInstance().updateLiveVerificationState(userIds, false);
                        livestreamUserForm.setType(2);
                        break;
                    case "Verify":
                        reasonCode = LiveStreamingService.getInstance().updateLiveVerificationState(userIds, true);
                        livestreamUserForm.setType(1);
                        break;
                    case "Make Featured":
                        int rc;
                        int sucs = 0;
                        int failed = 0;
                        reasonCode = 0;
                        for (String id : userIds) {
                            rc = UserService.getInstance().makeUserFeatured(Long.valueOf(id), Constants.FEATURED_USER_DEFAULT_WEIGHT, false);
                            if (rc > 0) {
                                failed++;
                                reasonCode = rc;
                            } else {
                                sucs++;
                            }
                        }
                        optionalMsg = "Successful operation : " + sucs + ", failed operation count : " + failed;
                        livestreamUserForm.setType(2);
                        break;
                }
                if (reasonCode == AppConstants.NONE) {
                    request.setAttribute("message", "<span style='color:green'>Operation successful. " + (!optionalMsg.isEmpty() ? optionalMsg : "") + "</span>");
                } else {
                    request.setAttribute("message", "<span style='color:red'>Operation failed. " + (!optionalMsg.isEmpty() ? optionalMsg : "") + Utils.getMessageForReasonCode(reasonCode) + "</span>");
                }
            }
            // list part
            if (livestreamUserForm.getType() == 0) {
                livestreamUserForm.setType(1);
            }
            switch (livestreamUserForm.getType()) {
                case 1:
                    userDTOs = LiveStreamingService.getInstance().getUnverifiedLiveUser(livestreamUserForm.getSearchText());
                    break;
                case 2:
                    userDTOs = LiveStreamingService.getInstance().getVerifiedLiveUser(livestreamUserForm.getSearchText());
                    break;
                default:
                    break;
            }
            request.getSession(true).setAttribute(Constants.DATA_ROWS, userDTOs);
            managePages(request, livestreamUserForm, userDTOs.size());

        } catch (Exception e) {
            logger.error(e);
            target = FAILURE;
        }
        return mapping.findForward(target);
    }
}
