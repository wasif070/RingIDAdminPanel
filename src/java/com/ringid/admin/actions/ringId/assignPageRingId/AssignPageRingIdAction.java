/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.ringId.assignPageRingId;

import com.ringid.admin.BaseAction;
import com.ringid.admin.dto.adminAuth.LoginDTO;
import com.ringid.admin.service.PageService;
import com.ringid.admin.utils.Utils;
import com.ringid.admin.utils.log.RingLogger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.ringid.utilities.ReasonCode;

/**
 *
 * @author mamun
 */
public class AssignPageRingIdAction extends BaseAction {

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = SUCCESS;
        String activistName;
        try {
            LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(com.ringid.admin.utils.Constants.LOGIN_DTO);
            activistName = loginDTO.getUserName();
        } catch (Exception e) {
            target = UNAUTHORIZED;
            return mapping.findForward(target);
        }
        try {
            int reasonCode = -1;
            AssignPageRingIdForm assignPageRingIdForm = (AssignPageRingIdForm) form;
            if (assignPageRingIdForm.getUserId() != null && assignPageRingIdForm.getUserId().length() > 0 && assignPageRingIdForm.getRingId() != null && assignPageRingIdForm.getRingId().length() > 0) {
                RingLogger.getActivityLogger().debug("[AssignPageRingIdAction] activistName : " + activistName + " userId --> " + assignPageRingIdForm.getUserId() + " ringId --> " + assignPageRingIdForm.getRingId());
                reasonCode = PageService.getInstance().assignPageRingId(Long.parseLong(assignPageRingIdForm.getUserId()), Long.parseLong(assignPageRingIdForm.getRingId()));
                if (reasonCode == ReasonCode.NONE) {
                    request.setAttribute("message", "<span style='color : green;'>Assign Page RingId Successful</span>");
                } else {
                    request.setAttribute("message", "<span style='color : red;'>Assign Page RingId " + Utils.getMessageForReasonCode(reasonCode) + " </span>");
                    target = FAILURE;
                }
            } else {
                request.setAttribute("message", "<span style='color : red;'>Invalid UserId or RingId</span>");
                target = FAILURE;
            }
        } catch (Exception e) {
            target = FAILURE;
        }
        return mapping.findForward(target);
    }
}
