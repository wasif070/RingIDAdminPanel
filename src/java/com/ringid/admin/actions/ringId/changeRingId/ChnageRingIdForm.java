/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.ringId.changeRingId;

import com.ringid.admin.BaseForm;
import com.ringid.admin.dto.adminAuth.LoginDTO;
import com.ringid.admin.utils.Constants;
import javax.servlet.http.HttpServletRequest;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

/**
 *
 * @author ipvision
 */
public class ChnageRingIdForm extends BaseForm {

    private String oldRingId;
    private String newRingId;

    public String getOldRingId() {
        return oldRingId;
    }

    public void setOldRingId(String oldRingId) {
        this.oldRingId = oldRingId;
    }

    public String getNewRingId() {
        return newRingId;
    }

    public void setNewRingId(String newRingId) {
        this.newRingId = newRingId;
    }

    @Override
    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        ActionErrors errors = new ActionErrors();
        LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        if (loginDTO == null) {
            errors.add("auth", new ActionMessage("UnAuthorized Access"));
        } else {
            if (getOldRingId() == null || getOldRingId().length() < 1) {
                errors.add("oldRingId", new ActionMessage("Required"));
            }
            if (getNewRingId() == null || getNewRingId().length() < 1) {
                errors.add("newRingId", new ActionMessage("Required"));
            }
        }
        return errors;
    }

}
