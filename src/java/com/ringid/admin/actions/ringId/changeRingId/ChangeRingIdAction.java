/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.ringId.changeRingId;

import com.google.gson.Gson;
import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.dto.adminAuth.LoginDTO;
import com.ringid.admin.service.UserService;
import com.ringid.admin.utils.Constants;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.ringid.users.UserBasicInfoDTO;
import ringid.command.CommandTaskScheduler;

/**
 *
 * @author ipvision
 */
public class ChangeRingIdAction extends BaseAction {

    Logger logger = RingLogger.getConfigPortalLogger();

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = SUCCESS;
        String activistName;
        try {
            LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(com.ringid.admin.utils.Constants.LOGIN_DTO);
            activistName = loginDTO.getUserName();
        } catch (Exception e) {
            target = UNAUTHORIZED;
            return mapping.findForward(target);
        }
        try {
            ChnageRingIdForm changeRingIdForm = (ChnageRingIdForm) form;
            UserBasicInfoDTO userBasicInfoDTO = UserService.getInstance().getUserBasicInfoByRingID(Long.parseLong(changeRingIdForm.getNewRingId()));
            if (userBasicInfoDTO != null) {
                target = FAILURE;
                request.setAttribute("msgstr", "<span style='color:red'> Operation Failed. New RingId already exists </span>");
                return mapping.findForward(target);
            }
            RingLogger.getActivityLogger().debug("[ChangeRingIdAction] activistName : " + activistName + "oldRingId --> " + changeRingIdForm.getOldRingId() + " newRingId --> " + changeRingIdForm.getNewRingId());
            auth.com.utils.MyAppError error = CommandTaskScheduler.getInstance().changeRingId(Constants.USERID, changeRingIdForm.getOldRingId(), changeRingIdForm.getNewRingId());
            logger.debug("[ChangeRingIdAction] response from AuthServer -> " + new Gson().toJson(error));
            if (error.getERROR_TYPE() == auth.com.utils.MyAppError.NOERROR) {
                request.setAttribute("msgstr", "<span>RingId Changed Successfully</span>");
            } else {
                request.setAttribute("message", "<span style='color: red'>Failed to change RingId</span>");
            }

        } catch (Exception e) {
            target = FAILURE;
        }

        return mapping.findForward(target);
    }

}
