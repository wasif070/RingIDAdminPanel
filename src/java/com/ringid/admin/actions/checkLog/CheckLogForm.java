/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.checkLog;

import com.ringid.admin.BaseForm;
import com.ringid.admin.dto.adminAuth.LoginDTO;
import com.ringid.admin.projectMenu.MenuNames;
import com.ringid.admin.utils.Constants;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

/**
 *
 * @author ipvision
 */
public class CheckLogForm extends BaseForm {

    private Integer serverType;
    private String serverIP;
    private String status;
    private Long requestTime;
    private String responseMSG;
    private Long responseTime;
    private String requestDate;
    private String responseDate;
    private Long pckId;
    public static final String SUCCESS = "SUCCESS";
    public static final String PENDING = "PENDING";
    public static final String FAILURE = "FAILED";
    public static final String TIMEOUT = "TIME-OUT";

    public static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm a");

    public Integer getServerType() {
        return serverType;
    }

    public void setServerType(Integer serverType) {
        this.serverType = serverType;
    }

    public String getServerIP() {
        return serverIP;
    }

    public void setServerIP(String serverIP) {
        this.serverIP = serverIP;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getRequestTime() {
        return requestTime;
    }

    public void setRequestTime(Long requestTime) {
        this.requestTime = requestTime;
    }

    public String getResponseMSG() {
        return responseMSG;
    }

    public void setResponseMSG(String responseMSG) {
        this.responseMSG = responseMSG;
    }

    public Long getResponseTime() {
        return responseTime;
    }

    public void setResponseTime(Long responseTime) {
        this.responseTime = responseTime;
    }

    public String getRequestDate() {
        return requestDate;
    }

    public void setRequestDate(Long requestDate) {
        this.requestDate = "";
        if (requestDate > 0) {
            Date date = new Date(requestDate);
            this.requestDate = sdf.format(date);
        }
    }

    public String getResponseDate() {
        return responseDate;
    }

    public void setResponseDate(Long responseDate) {
        this.responseDate = "";
        if (responseDate > 0) {
            Date date = new Date(responseDate);
            this.responseDate = sdf.format(date);
        }
    }

    public Long getPckId() {
        return pckId;
    }

    public void setPckId(Long pckId) {
        this.pckId = pckId;
    }

    @Override
    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        ActionErrors errors = new ActionErrors();
        LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        if (loginDTO == null) {
            errors.add("auth", new ActionMessage("UnAuthorized Access"));
        } else {
            if (Constants.ADD == getAction() || Constants.UPDATE == getAction()) {
            }
        }
        return errors;
    }
}
