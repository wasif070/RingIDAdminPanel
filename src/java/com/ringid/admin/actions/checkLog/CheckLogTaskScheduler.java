/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.checkLog;

import com.ringid.admin.dto.CheckLogDTO;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ipvision
 */
public class CheckLogTaskScheduler {

    private CheckLogTaskScheduler() {

    }

    private static class CheckLogTaskSchedulerHolder {

        private static final CheckLogTaskScheduler INSTANCE = new CheckLogTaskScheduler();
    }

    public static CheckLogTaskScheduler getInstance() {
        return CheckLogTaskSchedulerHolder.INSTANCE;
    }

    public ArrayList<CheckLogDTO> getCheckLogList(CheckLogDTO dto) {
        return CheckLogLoader.getInstance().getCheckLogList(dto);
    }

    public ArrayList<CheckLogDTO> getCheckSearchLogList(CheckLogDTO checkLogDTO) {
        return CheckLogLoader.getInstance().getCheckLogSearchList(checkLogDTO);
    }

    public void removeAll(String[] pckIds) {
        CheckLogLoader.getInstance().removeAll(pckIds);
    }

    List<CheckLogDTO> getCheckLogList(String[] pckIds) {
        return CheckLogLoader.getInstance().getCheckLogList(pckIds);
    }
}
