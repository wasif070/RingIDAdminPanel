/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.checkLog;

import com.ringid.admin.dto.CheckLogDTO;
import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.utils.Constants;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author ipvision
 */
public class CheckLogResendOrDeleteAction extends BaseAction {

    Logger logger = RingLogger.getConfigPortalLogger();

    ArrayList<CheckLogDTO> list = new ArrayList<>();

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = checkAuthentication(mapping, form, request);
        try {
            if (target.equals(SUCCESS)) {
                CheckLogForm checkLogForm = (CheckLogForm) form;
                CheckLogTaskScheduler scheduler = CheckLogTaskScheduler.getInstance();
                CheckLogDTO checkLogDTO = new CheckLogDTO();
                int action = Integer.valueOf(request.getParameter("action"));
                if (checkLogForm.getSelectedIDs() != null && action == Constants.LOG_DELETE) {
                    scheduler.removeAll(checkLogForm.getSelectedIDs());
                } else if (checkLogForm.getSelectedIDs() != null && action == Constants.LOG_RESEND) {
                    List<CheckLogDTO> checkLogDTOs = scheduler.getCheckLogList(checkLogForm.getSelectedIDs());
                    notifyAuthServers(checkLogDTOs);
                }
                list = scheduler.getCheckLogList(checkLogDTO);
                request.getSession(true).setAttribute(Constants.DATA_ROWS, list);
                managePages(request, checkLogForm, list.size());
            }
        } catch (Exception e) {
            target = FAILURE;
        }
        return mapping.findForward(target);
    }
}
