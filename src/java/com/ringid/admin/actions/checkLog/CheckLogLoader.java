/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.checkLog;

import com.ringid.admin.dto.CheckLogDTO;
import com.ringid.admin.authServerCommunicator.AuthServerCommunicator;
import com.ringid.admin.utils.log.RingLogger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author ipvision
 */
public class CheckLogLoader {

    private final static long LOADING_INTERVAL = 60 * 60 * 1000;
    private static CheckLogLoader checkLogLoader = null;
    private HashMap<String, CheckLogDTO> checkLogDTOHashMap;
    static Logger logger = RingLogger.getConfigPortalLogger();

    private CheckLogLoader() {
        checkLogDTOHashMap = new HashMap<>();
    }

    public static CheckLogLoader getInstance() {
        if (checkLogLoader == null) {
            createLoader();
        }
        return checkLogLoader;
    }

    private synchronized static void createLoader() {
        if (checkLogLoader == null) {
            checkLogLoader = new CheckLogLoader();
        }
    }

    private ArrayList<CheckLogDTO> checkForRemoveAndGetData() {
        ArrayList<CheckLogDTO> checkLogDTOs = new ArrayList<>();
        Long currentTime = System.currentTimeMillis();
        Set set = checkLogDTOHashMap.entrySet();
        Iterator i = set.iterator();
        while (i.hasNext()) {
            Map.Entry map = (Map.Entry) i.next();
            CheckLogDTO dto = (CheckLogDTO) map.getValue();
            if (currentTime - dto.getRequestTime() > LOADING_INTERVAL) {
                i.remove();
                continue;
            }
            checkLogDTOs.add(dto);
        }
        return checkLogDTOs;
    }

    public synchronized ArrayList<CheckLogDTO> getCheckLogList(CheckLogDTO sdto) {
        checkLogDTOHashMap = AuthServerCommunicator.getCheckLogList();
        ArrayList<CheckLogDTO> checkLogDTOs = checkForRemoveAndGetData();
        Collections.sort(checkLogDTOs, new CheckLogDTO.CompRequestTimeDSC());
        logger.debug("CheckLog List Size: " + checkLogDTOs.size());
        return checkLogDTOs;
    }

    public synchronized ArrayList<CheckLogDTO> getCheckLogSearchList(CheckLogDTO checkLogDTO) {
        ArrayList<CheckLogDTO> checkLogDTOs = checkForRemoveAndGetData();
        ArrayList<CheckLogDTO> list = new ArrayList<>();
        for (CheckLogDTO dto : checkLogDTOs) {
            if (dto.getStatus().equals(checkLogDTO.getStatus())) {
                list.add(dto);
            }
        }
        logger.debug("CheckLog Search List Size: " + list.size());
        return list;
    }

    public synchronized void removeAll(String[] pckIds) {
//        Long pckId;
        for (String pckId : pckIds) {
//            pckId = Long.valueOf(str);
            checkLogDTOHashMap.remove(pckId);
        }
    }

    List<CheckLogDTO> getCheckLogList(String[] pckIds) {
        List<CheckLogDTO> checkLogDTOs = new ArrayList<>();
        for (String pckId : pckIds) {
            CheckLogDTO checkLogDTO = checkLogDTOHashMap.get(pckId);
            checkLogDTOs.add(checkLogDTO);
        }
        logger.debug("CheckLog List Size: " + checkLogDTOs.size());
        return checkLogDTOs;
    }
}
