/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.channel;

import com.ringid.admin.dto.ChannelVerificationDTO;
import com.google.gson.Gson;
import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.dto.adminAuth.LoginDTO;
import com.ringid.admin.repository.ChannelRepository;
import com.ringid.admin.service.ChannelService;
import com.ringid.admin.utils.Constants;
import com.ringid.admin.utils.ReasonCode;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.ringid.channel.ChannelMediaDTO;

/**
 *
 * @author Rabby
 */
public class ChannelMediaListAction extends BaseAction {

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = SUCCESS;
        String activistName;
        try {
            LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
            activistName = loginDTO.getUserName();
        } catch (Exception e) {
            target = UNAUTHORIZED;
            return mapping.findForward(target);
        }
        try {
            ChannelForm channelForm = (ChannelForm) form;

            // update operation
            if (channelForm.getSubmitType() != null && channelForm.getSubmitType().length() > 0) {
                ArrayList<UUID> Ids = new ArrayList<>();
                Ids.add(UUID.fromString(channelForm.getChannelId()));
                RingLogger.getActivityLogger().debug("Action : ActivateOrDeactivateChannelAction[" + channelForm.getSubmitType() + "] activistName : " + activistName + " Ids --> " + new Gson().toJson(Ids));
                int reasonCode = -1;
                switch (channelForm.getSubmitType()) {
                    case "Verify Channel":
                        reasonCode = ChannelService.getInstance().verifyChannel(Ids);
                        if (reasonCode == ReasonCode.NONE) {
                            channelForm.setChannelType(2);
                        }
                        break;
                    case "Unverify Channel":
                        reasonCode = ChannelService.getInstance().unverifyChannel(Ids);
                        if (reasonCode == ReasonCode.NONE) {
                            channelForm.setChannelType(1);
                        }
                        break;
                    case "Delete Channel":
                        reasonCode = ChannelService.getInstance().deleteChannel(Ids);
                        break;
                    case "Make Featured":
                        reasonCode = ChannelService.getInstance().makeChannelFeatured(Ids);
                        if (reasonCode == ReasonCode.NONE) {
                            channelForm.setChannelType(3);
                        }
                        break;
                }
                if (reasonCode == ReasonCode.NONE) {
                    request.setAttribute("errormsg", "<span style='color: green'> Successfully update</span>");
                    ChannelRepository.getInstance().forceReload();
                } else {
                    request.setAttribute("errormsg", "<span style='color: red'> Failed to update (rc: " + reasonCode + ")</span>");
                }
            }

            //list part
            List<ChannelMediaDTO> channelMediaList = ChannelService.getInstance().getChannelMediaList(UUID.fromString(channelForm.getChannelId()));
            channelForm.setMedialList(channelMediaList);

            ChannelVerificationDTO channelVerificationDTO = ChannelService.getInstance().getChannelVerificationDTO(UUID.fromString(channelForm.getChannelId().trim()), channelForm.getChannelType());
            request.getSession(true).setAttribute(Constants.DATA_ROWS, channelMediaList);
            channelForm.setChannelTitle(channelVerificationDTO.getTitle());
            managePages(request, channelForm, channelMediaList.size());
        } catch (Exception e) {
            target = FAILURE;
        }
        return mapping.findForward(target);
    }
}
