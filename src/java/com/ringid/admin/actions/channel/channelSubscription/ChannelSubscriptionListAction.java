/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.channel.channelSubscription;

import com.ringid.admin.repository.ChannelSubscriptionLoader;
import com.ringid.admin.BaseAction;
import com.ringid.admin.service.ChannelService;
import com.ringid.admin.utils.Constants;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.ringid.channel.ChannelSubscriptionDTO;

/**
 *
 * @author mamun
 */
public class ChannelSubscriptionListAction extends BaseAction {

    List<ChannelSubscriptionDTO> channelSubscriptionDTOs;

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = SUCCESS;
        try {
            ChannelSubscriptionForm channelSubscriptionForm = (ChannelSubscriptionForm) form;
            if (request.getParameter("ForceReload") != null && "1".equals(request.getParameter("ForceReload"))) {
                ChannelSubscriptionLoader.getInstance().forceReload();
            }
            channelSubscriptionDTOs = ChannelService.getInstance().getChannelSubscriptionTypeList();
            request.getSession(true).setAttribute(Constants.DATA_ROWS, channelSubscriptionDTOs);
            managePages(request, channelSubscriptionForm, channelSubscriptionDTOs.size());
        } catch (Exception e) {
            target = FAILURE;
        }
        return mapping.findForward(target);
    }
}
