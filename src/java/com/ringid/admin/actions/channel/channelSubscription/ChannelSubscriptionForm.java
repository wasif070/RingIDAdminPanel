/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.channel.channelSubscription;

import com.ringid.admin.BaseForm;
import com.ringid.admin.dto.adminAuth.LoginDTO;
import com.ringid.admin.utils.Constants;
import javax.servlet.http.HttpServletRequest;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

/**
 *
 * @author mamun
 */
public class ChannelSubscriptionForm extends BaseForm {

    private int subTypeId;
    private String subTypeName;
    private long subDuration;
    private float subFee;
    private long subTime;
    private boolean isDefault;

    public int getSubTypeId() {
        return subTypeId;
    }

    public void setSubTypeId(int subTypeId) {
        this.subTypeId = subTypeId;
    }

    public String getSubTypeName() {
        return subTypeName;
    }

    public void setSubTypeName(String subTypeName) {
        this.subTypeName = subTypeName;
    }

    public long getSubDuration() {
        return subDuration;
    }

    public void setSubDuration(long subDuration) {
        this.subDuration = subDuration;
    }

    public float getSubFee() {
        return subFee;
    }

    public void setSubFee(float subFee) {
        this.subFee = subFee;
    }

    public long getSubTime() {
        return subTime;
    }

    public void setSubTime(long subTime) {
        this.subTime = subTime;
    }

    public boolean getIsDefault() {
        return isDefault;
    }

    public void setIsDefault(boolean isDefault) {
        this.isDefault = isDefault;
    }

    @Override
    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        ActionErrors errors = new ActionErrors();
        LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        if (loginDTO == null) {
            errors.add("auth", new ActionMessage("UnAuthorized Access"));
        }
        return errors;
    }
}
