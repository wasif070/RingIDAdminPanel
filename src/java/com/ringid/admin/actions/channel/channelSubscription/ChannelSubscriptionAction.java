/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.channel.channelSubscription;

import com.ringid.admin.repository.ChannelSubscriptionLoader;
import com.google.gson.Gson;
import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.dto.adminAuth.LoginDTO;
import com.ringid.admin.service.AdminService;
import com.ringid.admin.service.ChannelService;
import com.ringid.admin.utils.Constants;
import com.ringid.admin.utils.ReasonCode;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.ringid.channel.ChannelSubscriptionDTO;

/**
 *
 * @author mamun
 */
public class ChannelSubscriptionAction extends BaseAction {

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = checkAuthentication(mapping, form, request);
        String activistName;
        int reasonCode = -1;
        try {
            LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
            activistName = loginDTO.getUserName();
        } catch (Exception e) {
            target = UNAUTHORIZED;
            return mapping.findForward(target);
        }
        try {
            ChannelSubscriptionForm channelSubscriptionForm = (ChannelSubscriptionForm) form;
            int subTypeId = 0;
            if (target.equals(SUCCESS)) {
                switch (channelSubscriptionForm.getAction()) {
                    case Constants.ADD: {
                        subTypeId = AdminService.getInstance().addDeactivateReasonsInfo("");
                        channelSubscriptionForm.setSubTypeId(subTypeId);
                        ChannelSubscriptionDTO channelSubscriptionDTO = ChannelService.getInstance().getChannelSubscriptionDTO(channelSubscriptionForm);
                        RingLogger.getActivityLogger().debug("[add ChannelSubscriptionAction] activistName : " + activistName + " channelSubscriptionDTO --> " + new Gson().toJson(channelSubscriptionDTO));
                        reasonCode = ChannelService.getInstance().addChannelSubscriptionType(channelSubscriptionDTO);
                        RingLogger.getActivityLogger().debug("[add ChannelSubscriptionAction] activistName : " + activistName + " reasonCode --> " + reasonCode);
                        if (reasonCode == ReasonCode.NONE) {
                            request.setAttribute("message", "<span style='color:green'>Operation successfull</span>");
                            ChannelSubscriptionLoader.getInstance().forceReload();
                        } else {
                            request.setAttribute("message", "<span style='color:red'>Operation failed[ rc : " + reasonCode + " ]</span>");
                            target = FAILURE;
                        }
                        break;
                    }
                    case Constants.DELETE: {
                        RingLogger.getActivityLogger().debug("[delete ChannelSubscriptionAction] activistName : " + activistName + " id --> " + channelSubscriptionForm.getSubTypeId());
                        reasonCode = ChannelService.getInstance().deleteChannelSubscriptionType(channelSubscriptionForm.getSubTypeId());
                        RingLogger.getActivityLogger().debug("[delete ChannelSubscriptionAction] activistName : " + activistName + " reasonCode --> " + reasonCode);
                        if (reasonCode == ReasonCode.NONE) {
                            request.setAttribute("message", "<span style='color:green'>Operation successfull</span>");
                            ChannelSubscriptionLoader.getInstance().forceReload();
                        } else {
                            request.setAttribute("message", "<span style='color:red'>Operation failed[ rc : " + reasonCode + " ]</span>");
                            target = FAILURE;
                        }
                        break;
                    }
                }
            } else {
                request.setAttribute("message", "<span style='color:red'>Invalid Operation</span>");
            }
        } catch (Exception e) {
            target = FAILURE;
            RingLogger.getConfigPortalLogger().error("Exception in [ChannelSubscriptionAction] -> " + e);
        }
        return mapping.findForward(target);
    }
}
