/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.channel;

import com.google.gson.Gson;
import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.dto.adminAuth.LoginDTO;
import com.ringid.admin.repository.ChannelRepository;
import com.ringid.admin.service.ChannelService;
import com.ringid.admin.utils.Constants;
import com.ringid.admin.utils.ReasonCode;
import java.util.ArrayList;
import java.util.UUID;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

/**
 *
 * @author rabby
 */
public class ActivateOrDeactivateChannelAction extends BaseAction {

    ActionErrors err = new ActionErrors();

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = FAILURE;
        String activistName;
        ChannelForm channelForm = (ChannelForm) form;
        try {
            LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
            activistName = loginDTO.getUserName();
        } catch (Exception e) {
            target = UNAUTHORIZED;
            return mapping.findForward(target);
        }
        try {
            if (channelForm.getSelectedIDs() != null && channelForm.getSelectedIDs().length > 0) {
                ArrayList<UUID> Ids = new ArrayList<>();
                for (String s : channelForm.getSelectedIDs()) {
                    Ids.add(UUID.fromString(s));
                }
                RingLogger.getActivityLogger().debug("Action : ActivateOrDeactivateChannelAction[" + channelForm.getSubmitType() + "] activistName : " + activistName + " Ids --> " + new Gson().toJson(Ids));
                int reasonCode = -1;
                switch (channelForm.getSubmitType()) {
                    case "Primary Verify":
                        reasonCode = ChannelService.getInstance().verifyChannel(Ids);
                        break;
                    case "Primary Unverify":
                        reasonCode = ChannelService.getInstance().unverifyChannel(Ids);
                        break;
                    case "Officially Verify":
                        reasonCode = ChannelService.getInstance().verifyChannelOfficially(Ids);
                        break;
                    case "Officially Unverify":
                        reasonCode = ChannelService.getInstance().unverifyChannelOfficially(Ids);
                        break;
                    case "Delete Channel":
                        reasonCode = ChannelService.getInstance().deleteChannel(Ids);
                        break;
                    case "Make Featured":
                        reasonCode = ChannelService.getInstance().makeChannelFeatured(Ids);
                        break;
                }
                if (reasonCode == ReasonCode.NONE) {
                    target = SUCCESS;
                    ChannelRepository.getInstance().forceReload();
                    notifyAuthServers(Constants.RELOAD_CHANNEL_CATEGORY, "");
                } else {
                    err.add("errormsg", new ActionMessage("update failed , reasonCode: " + reasonCode, false));
                }
                saveErrors(request.getSession(), err);
            } else {
                err.add("errormsg", new ActionMessage("errors.select.one"));
                saveErrors(request.getSession(), err);
            }
            request.getSession(true).setAttribute(Constants.RECORD_PER_PAGE, channelForm.getRecordPerPage());
            request.setAttribute(Constants.CURRENT_PAGE_NO, channelForm.getPageNo());
        } catch (NullPointerException e) {
            target = FAILURE;
        }
        if (SUCCESS.equals(target)) {
            return new ActionForward(mapping.findForward(target).getPath() + "?channelType=" + channelForm.getChannelType(), true);
        }
        return mapping.findForward(target);
    }

}
