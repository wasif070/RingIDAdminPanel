/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.channel;

import auth.com.utils.MyAppError;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.dto.adminAuth.LoginDTO;
import com.ringid.admin.repository.ChannelRepository;
import com.ringid.admin.service.ChannelService;
import com.ringid.admin.utils.Constants;
import com.ringid.admin.utils.ReasonCode;
import java.util.UUID;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.ringid.utilities.AppConstants;
import ringid.LoginTaskScheduler;
import ringid.sessionless.SessionlessTaskseheduler;

/**
 *
 * @author mamun
 */
public class UpdateIpAndPortAction extends BaseAction {

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = FAILURE;
        String activistName;
        ChannelForm channelForm = (ChannelForm) form;
        try {
            LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
            activistName = loginDTO.getUserName();
        } catch (Exception e) {
            target = UNAUTHORIZED;
            return mapping.findForward(target);
        }
        try {
            int reasonCode = -1, newStreamServerPort = 0, newPort = 0;
            String newStreamServerIp = "", newServerIp = "", responseFromAuth;
            MyAppError myAppError;
            JsonObject jsonObject;
            if (channelForm.getSubmitType() != null && channelForm.getSubmitType().length() > 0) {
                switch (channelForm.getSubmitType()) {
                    case "register":
                        RingLogger.getActivityLogger().debug("Action : UpdateIpAndPortAction[notifyChannelServerActivateDeactivate] activistName : " + activistName + " channeUUID --> " + channelForm.getChannelId() + " activateType --> " + 1 + " ChnlIP --> " + channelForm.getNewChannelIP() + " ChnlPort --> " + channelForm.getNewChannelPort() + " isTvChannel --> " + (channelForm.getChannelType() == 3));
                        RingLogger.getConfigPortalLogger().debug("Action : UpdateIpAndPortAction[notifyChannelServerActivateDeactivate] activistName : " + activistName + " channeUUID --> " + channelForm.getChannelId() + " activateType --> " + 1 + " ChnlIP --> " + channelForm.getNewChannelIP() + " ChnlPort --> " + channelForm.getNewChannelPort() + " isTvChannel --> " + (channelForm.getChannelType() == 3));
                        LoginTaskScheduler.getInstance().initSessionLessCommunication(Constants.USERID);
                        responseFromAuth = SessionlessTaskseheduler.getInstance().notifyChannelServerActivateDeactivate(Constants.USERID, UUID.fromString(channelForm.getChannelId()), 1, channelForm.getChannelIP(), channelForm.getChannelPort(), channelForm.getChannelType() == AppConstants.ChannelTypes.TV_CHANNEL);
                        myAppError = LoginTaskScheduler.getInstance().destroySessionLessCommunication(Constants.USERID);
                        System.out.println("destroySessionLessCommunication --> " + myAppError);
                        if (responseFromAuth != null) {
                            RingLogger.getConfigPortalLogger().debug("UpdateIpAndPortAction[notifyChannelServerActivateDeactivate] " + responseFromAuth);
                            jsonObject = (JsonObject) new JsonParser().parse(responseFromAuth);
                            if (jsonObject.has("sucs") && jsonObject.get("sucs").getAsBoolean()) {
                                target = FAILURE;
                                request.setAttribute("errormsg", jsonObject.get("mg"));
                                ChannelRepository.getInstance().forceReload();
                            } else {
                                target = FAILURE;
                                request.setAttribute("errormsg", responseFromAuth);
                            }
                        } else {
                            target = FAILURE;
                            request.setAttribute("errormsg", "<span style='color:red;'>Update failed , response: null </span>");
                        }
                        break;
                    case "unregister":
                        RingLogger.getActivityLogger().debug("Action : UpdateIpAndPortAction[notifyChannelServerActivateDeactivate] activistName : " + activistName + " channeUUID --> " + channelForm.getChannelId() + " activateType --> " + 0 + " ChnlIP --> " + channelForm.getChannelIP() + " ChnlPort --> " + channelForm.getChannelPort() + " isTvChannel --> " + (channelForm.getChannelType() == 3));
                        RingLogger.getConfigPortalLogger().debug("Action : UpdateIpAndPortAction[notifyChannelServerActivateDeactivate] activistName : " + activistName + " channeUUID --> " + channelForm.getChannelId() + " activateType --> " + 0 + " ChnlIP --> " + channelForm.getChannelIP() + " ChnlPort --> " + channelForm.getChannelPort() + " isTvChannel --> " + (channelForm.getChannelType() == 3));
                        LoginTaskScheduler.getInstance().initSessionLessCommunication(Constants.USERID);
                        responseFromAuth = SessionlessTaskseheduler.getInstance().notifyChannelServerActivateDeactivate(Constants.USERID, UUID.fromString(channelForm.getChannelId()), 0, channelForm.getChannelIP(), channelForm.getChannelPort(), channelForm.getChannelType() == AppConstants.ChannelTypes.TV_CHANNEL);
                        myAppError = LoginTaskScheduler.getInstance().destroySessionLessCommunication(Constants.USERID);
                        System.out.println("destroySessionLessCommunication --> " + myAppError);
                        if (responseFromAuth != null) {
                            RingLogger.getConfigPortalLogger().debug("UpdateIpAndPortAction[notifyChannelServerActivateDeactivate] " + responseFromAuth);
                            jsonObject = (JsonObject) new JsonParser().parse(responseFromAuth);
                            if (jsonObject.has("sucs") && jsonObject.get("sucs").getAsBoolean()) {
                                channelForm.setActiveRegisterBtn(true);
                                target = FAILURE;
                                request.setAttribute("errormsg", jsonObject.get("mg"));
                                ChannelRepository.getInstance().forceReload();
                            } else {
                                target = FAILURE;
                                request.setAttribute("errormsg", responseFromAuth);
                            }
                        } else {
                            target = FAILURE;
                            request.setAttribute("errormsg", "<span style='color:red;'>Update failed , response: null </span>");
                        }
                        break;
                    default:
                        if (channelForm.getChannelIPandPort() != null && channelForm.getChannelIPandPort().length() > 0) {
                            RingLogger.getActivityLogger().debug("Action : UpdateIpAndPortAction[updateChannelServerIpAndPort] activistName : " + activistName + " channeUUID --> " + channelForm.getChannelId() + " oldChnlIP --> " + channelForm.getChannelIP() + " oldChnlPort --> " + channelForm.getChannelPort() + " newChnlIPandPort --> " + channelForm.getChannelIPandPort());
                            if (channelForm.getChannelIPandPort().split(",").length > 0) {
                                newServerIp = channelForm.getChannelIPandPort().split(",")[0];
                                channelForm.setNewChannelIP(newServerIp);
                            }
                            if (channelForm.getChannelIPandPort().split(",").length > 1) {
                                newPort = Integer.parseInt(channelForm.getChannelIPandPort().split(",")[1]);
                                channelForm.setNewChannelPort(newPort);
                            }
                            if (newServerIp.length() > 0 && !channelForm.getChannelIP().equalsIgnoreCase(newServerIp) && newPort > 0) {
                                reasonCode = ChannelService.getInstance().updateChannelServerIpAndPort(UUID.fromString(channelForm.getChannelId()), channelForm.getChannelIP(), channelForm.getChannelPort(), newServerIp, newPort);
                                if (reasonCode == ReasonCode.NONE) {
                                    channelForm.setChannelIP(newServerIp);
                                    channelForm.setChannelPort(newPort);
                                }
                            }
                        }
                        if (channelForm.getStreamingServerIPandPort() != null && channelForm.getStreamingServerIPandPort().length() > 0) {
                            RingLogger.getActivityLogger().debug("Action : UpdateIpAndPortAction[updateStreamingServerIpAndPort] activistName : " + activistName + " channeUUID --> " + channelForm.getChannelId() + " oldStrmIP --> " + channelForm.getStreamingServerIP() + " oldStrmPort --> " + channelForm.getStreamingServerPort() + " newStrmIPandPort --> " + channelForm.getStreamingServerIPandPort());
                            if (channelForm.getStreamingServerIPandPort().split(",").length > 0) {
                                newStreamServerIp = channelForm.getStreamingServerIPandPort().split(",")[0];
                            }
                            if (channelForm.getStreamingServerIPandPort().split(",").length > 1) {
                                newStreamServerPort = Integer.parseInt(channelForm.getStreamingServerIPandPort().split(",")[1]);
                            }
                            if (newStreamServerIp.length() > 0 && !channelForm.getStreamingServerIP().equalsIgnoreCase(newStreamServerIp) && newStreamServerPort > 0) {
                                reasonCode = ChannelService.getInstance().updateStreamingServerIpAndPort(UUID.fromString(channelForm.getChannelId()), channelForm.getStreamingServerIP(), channelForm.getStreamingServerPort(), newStreamServerIp, newStreamServerPort);
                                if (reasonCode == ReasonCode.NONE) {
                                    channelForm.setStreamingServerIP(newStreamServerIp);
                                    channelForm.setStreamingServerPort(newStreamServerPort);
                                }
                            }
                        }
                        switch (reasonCode) {
                            case ReasonCode.NONE:
                                target = FAILURE;
                                request.setAttribute("errormsg", "<span style='color:green;'>Updated successfully!</span>");
                                ChannelRepository.getInstance().forceReload();
                                break;
                            case -1:
                                target = FAILURE;
                                request.setAttribute("errormsg", "<span style='color:green;'>Nothing to update</span>");
                                break;
                            default:
                                request.setAttribute("errormsg", "<span style='color:red;'>Update failed , reasonCode: " + reasonCode + "</span>");
                                break;
                        }
                        channelForm.setActiveRegisterBtn(false);
                        break;
                }
            } else {
                target = FAILURE;
            }
            request.setAttribute("activeTab", 2);
        } catch (NullPointerException e) {
            target = FAILURE;
            e.printStackTrace();
        }
        if (SUCCESS.equals(target)) {
            return new ActionForward(mapping.findForward(target).getPath() + "?channelType=" + channelForm.getChannelType(), true);
        }
        return mapping.findForward(target);
    }
}
