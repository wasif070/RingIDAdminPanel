/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.channel;

import com.ringid.admin.BaseAction;
import com.ringid.admin.cassandra.CassandraDAO;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.dto.adminAuth.LoginDTO;
import com.ringid.admin.repository.ChannelRepository;
import com.ringid.admin.service.ChannelService;
import com.ringid.admin.utils.Constants;
import com.ringid.admin.utils.ReasonCode;
import java.util.UUID;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author mamun
 */
public class ChangeChannelOwnerAction extends BaseAction {

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = FAILURE;
        String activistName;
        ChannelForm channelForm = (ChannelForm) form;
        try {
            LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
            activistName = loginDTO.getUserName();
        } catch (Exception e) {
            target = UNAUTHORIZED;
            return mapping.findForward(target);
        }
        try {
            int reasonCode = -1;
            long newUserId;
            newUserId = CassandraDAO.getInstance().getUserTableId(channelForm.getNewUserId());
            if (newUserId > 0) {
                RingLogger.getActivityLogger().debug("Action : ChangeChannelOwnerAction[changeChannelOwnership] activistName : " + activistName + " channeUUID --> " + channelForm.getChannelId() + " oldUserId --> " + channelForm.getOldUserId() + " newUserId --> " + newUserId);
                reasonCode = ChannelService.getInstance().changeChannelOwnership(UUID.fromString(channelForm.getChannelId()), channelForm.getOldUserId(), newUserId);
            }
            if (reasonCode == ReasonCode.NONE) {
                target = SUCCESS;
                request.setAttribute("errormsg", "<span style='color:green;'>Updated successfully!</span>");
                ChannelRepository.getInstance().forceReload();
            } else {
                request.setAttribute("errormsg", "<span style='color:red;'>Update failed , reasonCode: " + reasonCode + "</span>");
            }
            request.setAttribute("activeTab", 1);
        } catch (NullPointerException e) {
            target = FAILURE;
            e.printStackTrace();
        }
        if (SUCCESS.equals(target)) {
            return new ActionForward(mapping.findForward(target).getPath() + "?channelType=" + channelForm.getChannelType(), true);
        }
        return mapping.findForward(target);
    }
}
