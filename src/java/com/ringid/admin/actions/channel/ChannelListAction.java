/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.channel;

import com.ringid.admin.dto.ChannelVerificationDTO;
import com.google.gson.Gson;
import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.dto.adminAuth.LoginDTO;
import com.ringid.admin.repository.ChannelRepository;
import com.ringid.admin.service.ChannelService;
import com.ringid.admin.utils.Constants;
import com.ringid.admin.utils.ReasonCode;
import java.util.ArrayList;
import java.util.UUID;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author Rabby
 */
public class ChannelListAction extends BaseAction {

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = SUCCESS;
        String activistName;
        try {
            LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
            activistName = loginDTO.getUserName();
        } catch (Exception e) {
            target = UNAUTHORIZED;
            return mapping.findForward(target);
        }
        try {
            ChannelForm channelForm = (ChannelForm) form;
            if (request.getParameter("ForceReload") != null && "1".equals(request.getParameter("ForceReload"))) {
                ChannelRepository.getInstance().forceReload();
            }
            // update operation
            if (channelForm.getSelectedIDs() != null && channelForm.getSelectedIDs().length > 0) {
                ArrayList<UUID> Ids = new ArrayList<>();
                for (String s : channelForm.getSelectedIDs()) {
                    Ids.add(UUID.fromString(s));
                }
                RingLogger.getActivityLogger().debug("Action : ActivateOrDeactivateChannelAction[" + channelForm.getSubmitType() + "] activistName : " + activistName + " Ids --> " + new Gson().toJson(Ids));
                int reasonCode = -1;
                switch (channelForm.getSubmitType()) {
                    case "Primary Verify":
                        reasonCode = ChannelService.getInstance().verifyChannel(Ids);
                        break;
                    case "Primary Unverify":
                        reasonCode = ChannelService.getInstance().unverifyChannel(Ids);
                        break;
                    case "Officially Verify":
                        reasonCode = ChannelService.getInstance().verifyChannelOfficially(Ids);
                        break;
                    case "Officially Unverify":
                        reasonCode = ChannelService.getInstance().unverifyChannelOfficially(Ids);
                        break;
                    case "Delete Channel":
                        reasonCode = ChannelService.getInstance().deleteChannel(Ids);
                        break;
                    case "Make Featured":
                        reasonCode = ChannelService.getInstance().makeChannelFeatured(Ids);
                        break;
                }
                if (reasonCode == ReasonCode.NONE) {
                    request.setAttribute("errormsg", "<span style='color: green'> Successfully update</span>");
                    target = SUCCESS;
                    ChannelRepository.getInstance().forceReload();
                } else {
                    request.setAttribute("errormsg", "<span style='color: red'> Failed to update (rc: " + reasonCode + ")</span>");
                }
            }
            // list operation
            int channelType;
            if (channelForm.getChannelType() == 0) {
                channelType = 1;
            } else {
                channelType = channelForm.getChannelType();
            }
            ChannelVerificationDTO channelVerificationDTO = new ChannelVerificationDTO();
            channelVerificationDTO.setChannelType(channelType);
            if (channelForm.getSearchText() != null && channelForm.getSearchText().length() > 0) {
                channelVerificationDTO.setSearchText(channelForm.getSearchText());
            }
            if (channelForm.getColumn() <= 0) {
                channelForm.setColumn(Constants.COLUMN_THREE);
                channelForm.setSort(Constants.DESC_SORT);
            }
            channelVerificationDTO.setColumn(channelForm.getColumn());
            channelVerificationDTO.setSortType(channelForm.getSort());
            ArrayList<ChannelVerificationDTO> channelList = ChannelService.getInstance().getChannelVerificationList(channelVerificationDTO);

            request.getSession(true).setAttribute(Constants.DATA_ROWS, channelList);
            request.setAttribute("channelType", channelType);
            managePages(request, channelForm, channelList.size());
        } catch (Exception e) {
            target = FAILURE;
        }
        return mapping.findForward(target);
    }
}
