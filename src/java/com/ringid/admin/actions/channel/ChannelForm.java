/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.channel;

import com.ringid.admin.BaseForm;
import java.util.List;
import org.ringid.channel.ChannelMediaDTO;

/**
 *
 * @author Rabby
 */
public class ChannelForm extends BaseForm {

    private String channelId;
    private String name;
    private String channelTitle;
    private List<ChannelMediaDTO> medialList;
    private long channelBoster;
    private String channelIP;
    private int channelPort;
    private String streamingServerIP;
    private int streamingServerPort;
    private String newChannelIP;
    private int newChannelPort;
    private String newStreamingServerIP;
    private int newStreamingServerPort;
    private String channelIPandPort;
    private String streamingServerIPandPort;
    private long oldUserId;
    private long newUserId;
    private long userId;
    private int channelType;
    private boolean channelSharable;
    private boolean oldValueOfChannelSharable;
    private long oldValueOfChannelBoster;
    private boolean activeRegisterBtn;
    private int channelTypeStream;

    public int getChannelTypeStream() {
        return channelTypeStream;
    }

    public void setChannelTypeStream(int channelTypeStream) {
        this.channelTypeStream = channelTypeStream;
    }

    public boolean isActiveRegisterBtn() {
        return activeRegisterBtn;
    }

    public void setActiveRegisterBtn(boolean activeRegisterBtn) {
        this.activeRegisterBtn = activeRegisterBtn;
    }

    public long getOldValueOfChannelBoster() {
        return oldValueOfChannelBoster;
    }

    public void setOldValueOfChannelBoster(long oldValueOfChannelBoster) {
        this.oldValueOfChannelBoster = oldValueOfChannelBoster;
    }

    public boolean isOldValueOfChannelSharable() {
        return oldValueOfChannelSharable;
    }

    public void setOldValueOfChannelSharable(boolean oldValueOfChannelSharable) {
        this.oldValueOfChannelSharable = oldValueOfChannelSharable;
    }

    public boolean isChannelSharable() {
        return channelSharable;
    }

    public void setChannelSharable(boolean channelSharable) {
        this.channelSharable = channelSharable;
    }

    public int getChannelType() {
        return channelType;
    }

    public void setChannelType(int channelType) {
        this.channelType = channelType;
    }

    public String getChannelTitle() {
        return channelTitle;
    }

    public void setChannelTitle(String channelTitle) {
        this.channelTitle = channelTitle;
    }

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public long getOldUserId() {
        return oldUserId;
    }

    public void setOldUserId(long oldUserId) {
        this.oldUserId = oldUserId;
    }

    public long getNewUserId() {
        return newUserId;
    }

    public void setNewUserId(long newUserId) {
        this.newUserId = newUserId;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getChannelIPandPort() {
        return channelIPandPort;
    }

    public void setChannelIPandPort(String channelIPandPort) {
        this.channelIPandPort = channelIPandPort;
    }

    public String getStreamingServerIPandPort() {
        return streamingServerIPandPort;
    }

    public void setStreamingServerIPandPort(String streamingServerIPandPort) {
        this.streamingServerIPandPort = streamingServerIPandPort;
    }

    public String getNewChannelIP() {
        return newChannelIP;
    }

    public void setNewChannelIP(String newChannelIP) {
        this.newChannelIP = newChannelIP;
    }

    public int getNewChannelPort() {
        return newChannelPort;
    }

    public void setNewChannelPort(int newChannelPort) {
        this.newChannelPort = newChannelPort;
    }

    public String getNewStreamingServerIP() {
        return newStreamingServerIP;
    }

    public void setNewStreamingServerIP(String newStreamingServerIP) {
        this.newStreamingServerIP = newStreamingServerIP;
    }

    public int getNewStreamingServerPort() {
        return newStreamingServerPort;
    }

    public void setNewStreamingServerPort(int newStreamingServerPort) {
        this.newStreamingServerPort = newStreamingServerPort;
    }

    public String getChannelIP() {
        return channelIP;
    }

    public void setChannelIP(String channelIP) {
        this.channelIP = channelIP;
    }

    public int getChannelPort() {
        return channelPort;
    }

    public void setChannelPort(int channelPort) {
        this.channelPort = channelPort;
    }

    public String getStreamingServerIP() {
        return streamingServerIP;
    }

    public void setStreamingServerIP(String streamingServerIP) {
        this.streamingServerIP = streamingServerIP;
    }

    public int getStreamingServerPort() {
        return streamingServerPort;
    }

    public void setStreamingServerPort(int streamingServerPort) {
        this.streamingServerPort = streamingServerPort;
    }

    public long getChannelBoster() {
        return channelBoster;
    }

    public void setChannelBoster(long channelBoster) {
        this.channelBoster = channelBoster;
    }

    public List<ChannelMediaDTO> getMedialList() {
        return medialList;
    }

    public void setMedialList(List<ChannelMediaDTO> medialList) {
        this.medialList = medialList;
    }

    public String getName() {
        return name;
    }

    public void setName(String string) {
        name = string;
    }
}
