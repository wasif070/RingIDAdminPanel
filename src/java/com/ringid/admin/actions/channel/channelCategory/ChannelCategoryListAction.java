/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.channel.channelCategory;

import com.ringid.admin.repository.ChannelCategoryLoader;
import com.ringid.admin.BaseAction;
import com.ringid.admin.dto.ChannelVerificationDTO;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.service.ChannelService;
import com.ringid.admin.utils.Constants;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.ringid.channel.ChannelCategoryDTO;

/**
 *
 * @author mamun
 */
public class ChannelCategoryListAction extends BaseAction {

    private static final Logger logger = RingLogger.getConfigPortalLogger();
    List<ChannelCategoryDTO> channelCategoryDTOs = new ArrayList<>();

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = SUCCESS;
        try {
            ChannelCategoryForm channelCategoryForm = (ChannelCategoryForm) form;
            ChannelVerificationDTO dto = new ChannelVerificationDTO();

            if (request.getParameter("ForceReload") != null && "1".equals(request.getParameter("ForceReload"))) {
                ChannelCategoryLoader.getInstance().forceReload();
            }

            if (channelCategoryForm.getSearchText() != null && channelCategoryForm.getSearchText().length() > 0) {
                dto.setSearchText(channelCategoryForm.getSearchText().trim().toLowerCase());
            }
            if (channelCategoryForm.getColumn() <= 0) {
                channelCategoryForm.setColumn(Constants.COLUMN_THREE);
                channelCategoryForm.setSort(Constants.DESC_SORT);
            }
            dto.setColumn(channelCategoryForm.getColumn());
            dto.setSortType(channelCategoryForm.getSort());
            channelCategoryDTOs = ChannelService.getInstance().getChannelCategoryList(dto);

            request.getSession(true).setAttribute(Constants.DATA_ROWS, channelCategoryDTOs);
            managePages(request, channelCategoryForm, channelCategoryDTOs.size());
        } catch (Exception e) {
            target = FAILURE;
        }
        return mapping.findForward(target);
    }
}
