/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.channel.channelCategory;

import com.ringid.admin.BaseForm;
import com.ringid.admin.utils.Constants;
import javax.servlet.http.HttpServletRequest;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.upload.FormFile;

/**
 *
 * @author mamun
 */
public class ChannelCategoryForm extends BaseForm {

    private int categoryId;
    private String category;
    private String description;
    private String banner;
    private int verticalWeight;
    private int horizontalWeight;
    private long channelCount;
    private FormFile theFile;

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getBanner() {
        return banner;
    }

    public void setBanner(String banner) {
        this.banner = banner;
    }

    public int getVerticalWeight() {
        return verticalWeight;
    }

    public void setVerticalWeight(int verticalWeight) {
        this.verticalWeight = verticalWeight;
    }

    public int getHorizontalWeight() {
        return horizontalWeight;
    }

    public void setHorizontalWeight(int horizontalWeight) {
        this.horizontalWeight = horizontalWeight;
    }

    public long getChannelCount() {
        return channelCount;
    }

    public void setChannelCount(long channelCount) {
        this.channelCount = channelCount;
    }

    public FormFile getTheFile() {
        return theFile;
    }

    public void setTheFile(FormFile theFile) {
        this.theFile = theFile;
    }

    @Override
    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        ActionErrors errors = new ActionErrors();
        if (Constants.ADD == getAction() || Constants.UPDATE == getAction()) {
            if (getCategory() == null || getCategory().length() < 1) {
                errors.add("category", new ActionMessage("errors.roomname.required"));
            }
            if (Constants.ADD == getAction()) {
                if (getTheFile() == null || getTheFile().getFileSize() < 1) {
                    errors.add("theFile", new ActionMessage("errors.file.required"));
                }
            }
        }
        return errors;
    }

}
