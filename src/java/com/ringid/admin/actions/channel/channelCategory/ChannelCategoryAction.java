/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.channel.channelCategory;

import com.ringid.admin.repository.ChannelCategoryLoader;
import auth.com.utils.MyAppError;
import com.google.gson.Gson;
import com.ringid.admin.BaseAction;
import com.ringid.admin.actions.liveRoom.UploadBannerImage;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.dto.adminAuth.LoginDTO;
import com.ringid.admin.service.AdminService;
import com.ringid.admin.service.ChannelService;
import com.ringid.admin.utils.Constants;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.upload.FormFile;
import org.ringid.channel.ChannelCategoryDTO;
import org.ringid.utilities.AppConstants;
import ringid.LoginTaskScheduler;

/**
 *
 * @author mamun
 */
public class ChannelCategoryAction extends BaseAction {

    private static final Logger logger = RingLogger.getConfigPortalLogger();

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = checkAuthentication(mapping, form, request);
        String activistName;
        int reasonCode = -1;
        try {
            LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
            activistName = loginDTO.getUserName();
        } catch (Exception e) {
            target = UNAUTHORIZED;
            return mapping.findForward(target);
        }
        try {
            if (target.equals(SUCCESS)) {
                int id = 0;
                ChannelCategoryForm channelCategoryForm = (ChannelCategoryForm) form;
                ChannelCategoryDTO dto = null;
                FormFile myFile = channelCategoryForm.getTheFile();
                String fileName;
                String bannerUrl = channelCategoryForm.getBanner();
                byte[] fileData;
                MyAppError myAppError = new MyAppError();

                switch (channelCategoryForm.getAction()) {
                    case Constants.ADD:

                        fileName = myFile.getFileName();
                        fileData = myFile.getFileData();
                        if (fileName.length() < 1 || fileData.length < 1) {
                            throw new Exception("please upload appropriate data!");
                        }

                        myAppError = LoginTaskScheduler.getInstance().login(Constants.USERID, Constants.PASSWORD);
                        RingLogger.getConfigPortalLogger().debug("##  login --> " + new Gson().toJson(myAppError));

                        bannerUrl = new UploadBannerImage().upLoadToCloud(Constants.USERID, fileName, fileData);
                        RingLogger.getConfigPortalLogger().debug("##  bannerUrl --> " + bannerUrl);
                        myAppError = LoginTaskScheduler.getInstance().logout(Constants.USERID);
                        RingLogger.getConfigPortalLogger().debug("##  myAppError --> " + new Gson().toJson(myAppError));

                        id = AdminService.getInstance().addDeactivateReasonsInfo(channelCategoryForm.getDescription());

                        if (id > 0 && bannerUrl != null && bannerUrl.length() > 0) {
                            channelCategoryForm.setCategoryId(id);
                            channelCategoryForm.setBanner(bannerUrl);

                            dto = ChannelService.getInstance().getChannelCategory(channelCategoryForm);

                            RingLogger.getActivityLogger().debug("[add ChannelCategoryAction] activistName : " + activistName + " channelCategoryDTO --> " + new Gson().toJson(dto));

                            reasonCode = ChannelService.getInstance().addChannelCategory(dto);
                            if (reasonCode == AppConstants.NONE) {
                                target = SUCCESS;
                                ChannelCategoryLoader.getInstance().forceReload();
                                request.setAttribute("message", "<span style='color: green'>Successfully added</span>");
                                notifyAuthServers(Constants.RELOAD_CHANNEL_CATEGORY, "");
                            } else {
                                target = FAILURE;
                                request.setAttribute("message", "<span style='color: red'>Failed to added(rc: " + reasonCode + ")</span>");
                            }
                        } else {
                            target = FAILURE;
                        }
                        break;

                    case Constants.EDIT:
                        dto = ChannelService.getInstance().getChannelCategoryDTOById(channelCategoryForm.getCategoryId());
                        if (dto != null) {
                            channelCategoryForm.setCategory(dto.getCategory());
                            channelCategoryForm.setBanner(dto.getBanner());
                            channelCategoryForm.setChannelCount(dto.getChannelCount());
                            channelCategoryForm.setDescription(dto.getDescription());
                            channelCategoryForm.setVerticalWeight(dto.getVerticalWeight());
                            channelCategoryForm.setHorizontalWeight(dto.getHorizontalWeight());
                        } else {
                            target = FAILURE;
                        }
                        break;

                    case Constants.UPDATE:
                        if (myFile.getFileName() != null && myFile.getFileName().length() > 0) {
                            fileName = myFile.getFileName();
                            fileData = myFile.getFileData();
                            if (fileName.length() < 1 || fileData.length < 1) {
                                throw new Exception("please upload appropriate data!");
                            }

                            myAppError = LoginTaskScheduler.getInstance().login(Constants.USERID, Constants.PASSWORD);
                            RingLogger.getConfigPortalLogger().debug("##  login --> " + new Gson().toJson(myAppError));

                            bannerUrl = new UploadBannerImage().upLoadToCloud(Constants.USERID, fileName, fileData);
                            RingLogger.getConfigPortalLogger().debug("##  bannerUrl --> " + bannerUrl);
                            myAppError = LoginTaskScheduler.getInstance().logout(Constants.USERID);
                            RingLogger.getConfigPortalLogger().debug("##  myAppError --> " + new Gson().toJson(myAppError));
                        }
                        channelCategoryForm.setBanner(bannerUrl);

                        dto = ChannelService.getInstance().getChannelCategory(channelCategoryForm);
                        RingLogger.getActivityLogger().debug("[update ChannelCategoryAction] activistName : " + activistName + " channelCategoryDTO --> " + new Gson().toJson(dto));

                        reasonCode = ChannelService.getInstance().updateChannelCategory(dto);
                        if (reasonCode == AppConstants.NONE) {
                            target = SUCCESS;
                            ChannelCategoryLoader.getInstance().forceReload();
                            request.setAttribute("message", "<span style='color: green'>Successfully updated</span>");
                            notifyAuthServers(Constants.RELOAD_CHANNEL_CATEGORY, "");
                        } else {
                            target = FAILURE;
                            request.setAttribute("message", "<span style='color: red'>Failed to update(rc: " + reasonCode + ")</span>");
                        }
                        break;

                    case Constants.DELETE:
                        RingLogger.getActivityLogger().debug("[delete ChannelCategoryAction] activistName : " + activistName + " channelCategoryId --> " + channelCategoryForm.getCategoryId());
                        reasonCode = ChannelService.getInstance().deleteChannelCategory(channelCategoryForm.getCategoryId());
                        if (reasonCode == AppConstants.NONE) {
                            target = SUCCESS;
                            ChannelCategoryLoader.getInstance().forceReload();
                            request.setAttribute("message", "<span style='color: green'>Successfully deleted </span>");
                            notifyAuthServers(Constants.RELOAD_CHANNEL_CATEGORY, "");
                        } else {
                            target = FAILURE;
                            request.setAttribute("message", "<span style='color: red'>Failed to delete (rc: " + reasonCode + ")</span>");
                        }
                        break;
                }
            }
        } catch (Exception e) {
            target = FAILURE;
            logger.error(e);
            request.setAttribute("message", "<span style='color: red'>Failed (reason: Expception Occured)</span>");
        }
        return mapping.findForward(target);
    }
}
