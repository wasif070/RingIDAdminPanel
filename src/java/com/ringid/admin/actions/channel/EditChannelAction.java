/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.channel;

import com.ringid.admin.dto.ChannelVerificationDTO;
import com.ringid.admin.BaseAction;
import com.ringid.admin.dto.adminAuth.LoginDTO;
import com.ringid.admin.service.ChannelService;
import com.ringid.admin.utils.Constants;
import java.util.UUID;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author mamun
 */
public class EditChannelAction extends BaseAction {

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = FAILURE;
        try {
            LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        } catch (Exception e) {
            target = UNAUTHORIZED;
            return mapping.findForward(target);
        }
        try {
            ChannelForm channelForm = (ChannelForm) form;
            ChannelVerificationDTO dto;
            int channeltype = channelForm.getChannelType();
            if (channeltype <= 0) {
                channeltype = 1;
            }
            if (channelForm.getChannelId() != null && channelForm.getChannelId().length() > 0) {

                dto = ChannelService.getInstance().getChannelVerificationDTO(UUID.fromString(channelForm.getChannelId()), channeltype);

                channelForm.setChannelSharable(dto.isChannelSharable());
                channelForm.setChannelBoster(dto.getChannelBooster());
                channelForm.setOldUserId(dto.getUserId());
                channelForm.setName(dto.getTitle());
                channelForm.setChannelIP(dto.getChannelIP());
                channelForm.setChannelPort(dto.getChannelPort());
                channelForm.setStreamingServerIP(dto.getStreamingServerIP());
                channelForm.setStreamingServerPort(dto.getStreamingServerPort());
                channelForm.setChannelType(channeltype);
                channelForm.setUserId(Long.valueOf(dto.getUserIdentity()));
                channelForm.setOldValueOfChannelSharable(channelForm.isChannelSharable());
                channelForm.setOldValueOfChannelBoster(channelForm.getChannelBoster());
                target = SUCCESS;
            } else {
                target = FAILURE;
            }
        } catch (NullPointerException e) {
            target = FAILURE;
            e.printStackTrace();
        }
        return mapping.findForward(target);
    }
}
