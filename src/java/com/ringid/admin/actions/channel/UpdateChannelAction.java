/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.channel;

import com.ringid.admin.BaseAction;
import com.ringid.admin.cassandra.CassandraDAO;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.dto.adminAuth.LoginDTO;
import com.ringid.admin.repository.ChannelRepository;
import com.ringid.admin.service.ChannelService;
import com.ringid.admin.utils.Constants;
import com.ringid.admin.utils.ReasonCode;
import java.util.UUID;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author mamun
 */
public class UpdateChannelAction extends BaseAction {

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = FAILURE;
        String activistName;
        ChannelForm channelForm = (ChannelForm) form;
        try {
            LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
            activistName = loginDTO.getUserName();
        } catch (Exception e) {
            target = UNAUTHORIZED;
            return mapping.findForward(target);
        }
        try {
            int reasonCode = -1, newStreamServerPort, newPort;
            long newUserId = 0L;
            String newStreamServerIp, newServerIp;
            if (channelForm.getSubmitType() != null && channelForm.getSubmitType().length() > 0) {
                switch (Integer.parseInt(channelForm.getSubmitType())) {
                    case 1:
                        if (channelForm.getOldValueOfChannelBoster() != channelForm.getChannelBoster()) {
                            RingLogger.getActivityLogger().debug("Action : UpdateChannelAction[updateChannelBooster] activistName : " + activistName + " channeUUID --> " + channelForm.getChannelId() + " channelBooster --> " + channelForm.getChannelBoster());
                            reasonCode = ChannelService.getInstance().updateChannelBooster(UUID.fromString(channelForm.getChannelId()), channelForm.getChannelBoster());
                        }
                        if (channelForm.isOldValueOfChannelSharable() != channelForm.isChannelSharable()) {
                            RingLogger.getActivityLogger().debug("Action : UpdateChannelAction[updateIsChannelSharable] activistName : " + activistName + " channeUUID --> " + channelForm.getChannelId() + " isChannelSharable --> " + channelForm.isChannelSharable());
                            reasonCode = ChannelService.getInstance().updateIsChannelSharable(UUID.fromString(channelForm.getChannelId()), channelForm.isChannelSharable());
                        }
                        break;
                    case 2:
                        newUserId = CassandraDAO.getInstance().getUserTableId(channelForm.getNewUserId());
                        RingLogger.getActivityLogger().debug("Action : UpdateChannelAction[changeChannelOwnership] activistName : " + activistName + " channeUUID --> " + channelForm.getChannelId() + " oldUserId --> " + channelForm.getOldUserId() + " newUserId --> " + newUserId);

                        reasonCode = ChannelService.getInstance().changeChannelOwnership(UUID.fromString(channelForm.getChannelId()), channelForm.getOldUserId(), newUserId);
                        break;
                    case 3:
                        if (channelForm.getChannelIPandPort() != null && channelForm.getChannelIPandPort().length() > 0) {
                            RingLogger.getActivityLogger().debug("Action : UpdateChannelAction[updateChannelServerIpAndPort] activistName : " + activistName + " channeUUID --> " + channelForm.getChannelId() + " oldChnlIP --> " + channelForm.getChannelIP() + " oldChnlPort --> " + channelForm.getChannelPort() + " newChnlIPandPort --> " + channelForm.getChannelIPandPort());
                            newServerIp = channelForm.getChannelIPandPort().split(",")[0];
                            newPort = Integer.parseInt(channelForm.getChannelIPandPort().split(",")[1]);

                            reasonCode = ChannelService.getInstance().updateChannelServerIpAndPort(UUID.fromString(channelForm.getChannelId()), channelForm.getChannelIP(), channelForm.getChannelPort(), newServerIp, newPort);
                        }

                        if (channelForm.getStreamingServerIPandPort() != null && channelForm.getStreamingServerIPandPort().length() > 0) {
                            RingLogger.getActivityLogger().debug("Action : UpdateChannelAction[updateStreamingServerIpAndPort] activistName : " + activistName + " channeUUID --> " + channelForm.getChannelId() + " oldStrmIP --> " + channelForm.getStreamingServerIP() + " oldStrmPort --> " + channelForm.getStreamingServerPort() + " newStrmIPandPort --> " + channelForm.getStreamingServerIPandPort());
                            newStreamServerIp = channelForm.getStreamingServerIPandPort().split(",")[0];
                            newStreamServerPort = Integer.parseInt(channelForm.getStreamingServerIPandPort().split(",")[1]);

                            reasonCode = ChannelService.getInstance().updateStreamingServerIpAndPort(UUID.fromString(channelForm.getChannelId()), channelForm.getStreamingServerIP(), channelForm.getStreamingServerPort(), newStreamServerIp, newStreamServerPort);
                        }
                        break;
                }
            } else {
                reasonCode = -1;
            }

            if (reasonCode == ReasonCode.NONE) {
                target = SUCCESS;
                request.setAttribute("errormsg", "<span style='color:green;'>Updated successfully!</span>");
                ChannelRepository.getInstance().forceReload();
            } else {
                request.setAttribute("errormsg", "<span style='color:red;'>Update failed , reasonCode: " + reasonCode + "</span>");
            }

            request.getSession(true).setAttribute(Constants.RECORD_PER_PAGE, channelForm.getRecordPerPage());
            request.setAttribute(Constants.CURRENT_PAGE_NO, channelForm.getPageNo());
        } catch (NullPointerException e) {
            target = FAILURE;
            e.printStackTrace();
        }
        if (SUCCESS.equals(target)) {
            return new ActionForward(mapping.findForward(target).getPath() + "?channelType=" + channelForm.getChannelType(), true);
        }
        return mapping.findForward(target);
    }
}
