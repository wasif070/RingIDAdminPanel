/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.donationPage;

import com.ringid.admin.dto.DonationPageDTO;
import com.ringid.admin.repository.DonationPageLoader;
import com.google.gson.Gson;
import com.ringid.admin.BaseAction;
import com.ringid.admin.cassandra.CassandraDAO;
import com.ringid.admin.actions.liveRoom.UploadBannerImage;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.dto.adminAuth.LoginDTO;
import com.ringid.admin.actions.newsPortal.UploadImage;
import com.ringid.admin.service.PageService;
import com.ringid.admin.utils.Constants;
import com.ringid.admin.utils.MyAppError;
import java.util.UUID;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.ringid.newsfeeds.ImageDTO;
import org.ringid.pages.PageDTO;
import org.ringid.utilities.AppConstants;
import ringid.LoginTaskScheduler;
import ringid.sessionless.SessionlessTaskseheduler;

/**
 *
 * @author mamun
 */
public class DonationPageAction extends BaseAction {

    private static final Logger logger = RingLogger.getConfigPortalLogger();

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = checkAuthentication(mapping, form, request);
        String activistName;
        int reasonCode = -1;
        try {
            LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
            activistName = loginDTO.getUserName();
        } catch (Exception e) {
            target = UNAUTHORIZED;
            return mapping.findForward(target);
        }
        try {
            if (target.equals(SUCCESS)) {
                DonationPageForm donationPageForm = (DonationPageForm) form;
                long pageOwnerTableId = CassandraDAO.getInstance().getUserTableId(donationPageForm.getPageOwnerId());
                PageDTO pageDTO = new PageDTO();

                String bannerUrl = "";
                if (donationPageForm.getBannerImageUrl() != null && donationPageForm.getBannerImageUrl().length() > 0) {
                    bannerUrl = donationPageForm.getBannerImageUrl();
                }
                String profileImageUrl = "";
                UUID profileImageId = null;
                String coverImageUrl = "";
                UUID coverImageId = null;
                long pageId;
                long pageRingId;
                MyAppError myAppError;
                auth.com.utils.MyAppError error;

                switch (donationPageForm.getAction()) {
                    case Constants.ADD:

                        LoginTaskScheduler.getInstance().initSessionLessCommunication(Constants.USERID);
                        pageId = SessionlessTaskseheduler.getInstance().getIdFromUserRingIdGenerator(Constants.USERID);
                        pageRingId = SessionlessTaskseheduler.getInstance().getIdFromRingIdGenerator(Constants.USERID);
                        LoginTaskScheduler.getInstance().destroySessionLessCommunication(Constants.USERID);

                        LoginTaskScheduler.getInstance().login(Constants.USERID, Constants.PASSWORD);
                        if (donationPageForm.getTheFileProfileImage().getFileName() != null && donationPageForm.getTheFileProfileImage().getFileName().length() > 0) {
                            String fileName = donationPageForm.getTheFileProfileImage().getFileName();
                            byte[] fileData = donationPageForm.getTheFileProfileImage().getFileData();
                            ImageDTO imageDTO = new UploadImage().upLoadToCloud(Constants.USERID, fileName, fileData, Constants.PROFILE_IMAGE_UPLOAD_URL);
                            if (imageDTO != null) {
                                profileImageUrl = imageDTO.getImageURL();
                                donationPageForm.setProfileImageUrl(profileImageUrl);
                                imageDTO.setImageType(AppConstants.IMAGE_TYPE_PROFILE_IMAGE);
                                imageDTO.setImagePrivacy(AppConstants.PROFILE_IMAGE_PRIVACY);
                                imageDTO.setUserTableID(pageId);
                                int rc = PageService.getInstance().addProfileOrCoverImage(imageDTO);
                                profileImageId = imageDTO.getImageID();
                                System.out.println(profileImageUrl);
                            }
                        }
                        if (donationPageForm.getTheFileCoverImage().getFileName() != null && donationPageForm.getTheFileCoverImage().getFileName().length() > 0) {
                            String fileName = donationPageForm.getTheFileCoverImage().getFileName();
                            byte[] fileData = donationPageForm.getTheFileCoverImage().getFileData();
                            ImageDTO imageDTO = new UploadImage().upLoadToCloud(Constants.USERID, fileName, fileData, Constants.COVER_IMAGE_UPLOAD_URL);
                            if (imageDTO != null) {
                                coverImageUrl = imageDTO.getImageURL();
                                donationPageForm.setCoverImageUrl(coverImageUrl);
                                imageDTO.setImageType(AppConstants.IMAGE_TYPE_COVER_IMAGE);
                                imageDTO.setImagePrivacy(AppConstants.COVER_IMAGE_PRIVACY);
                                imageDTO.setUserTableID(pageId);
                                int rc = PageService.getInstance().addProfileOrCoverImage(imageDTO);
                                coverImageId = imageDTO.getImageID();
                                System.out.println(coverImageUrl);
                            }
                        }
                        LoginTaskScheduler.getInstance().logout(Constants.USERID);
                        pageDTO.setPageName(donationPageForm.getPageName());
                        pageDTO.setPageType(AppConstants.UserTypeConstants.DONATION_PAGE);
                        pageDTO.setPageId(pageId);
                        pageDTO.setPageRingId(pageRingId);
                        pageDTO.setCountry(donationPageForm.getCountry());
                        pageDTO.setProfileImageUrl(profileImageUrl);
                        pageDTO.setProfileImageId(profileImageId);
                        pageDTO.setCoverImageUrl(coverImageUrl);
                        pageDTO.setCoverImageId(coverImageId);
                        pageDTO.setPageOwner(pageOwnerTableId);

                        RingLogger.getActivityLogger().debug("[add DonationPageAction] activistName : " + activistName + " pageDTO --> " + new Gson().toJson(pageDTO));

                        reasonCode = PageService.getInstance().createDonationPage(pageDTO);
                        if (reasonCode == AppConstants.NONE) {
                            if (donationPageForm.getTheFileBannerImage().getFileName().length() > 0 && donationPageForm.getTheFileBannerImage().getFileData().length > 0) {
                                RingLogger.getConfigPortalLogger().debug("## SessionLessCommunication destroyed.");
                                error = LoginTaskScheduler.getInstance().login(Constants.USERID, Constants.PASSWORD);
                                RingLogger.getConfigPortalLogger().debug("##  login --> " + new Gson().toJson(error));

                                bannerUrl = new UploadBannerImage().upLoadToCloud(Constants.USERID, donationPageForm.getTheFileBannerImage().getFileName(), donationPageForm.getTheFileBannerImage().getFileData());
                                RingLogger.getConfigPortalLogger().debug("##  bannerUrl --> " + bannerUrl);
                                error = LoginTaskScheduler.getInstance().logout(Constants.USERID);
                                RingLogger.getConfigPortalLogger().debug("##  myAppError --> " + new Gson().toJson(error));
                            }
                            myAppError = PageService.getInstance().addDonationPageBannerImageAndVisibility(pageId, bannerUrl, donationPageForm.getVisible());
                            if (myAppError.getERROR_TYPE() == MyAppError.NOERROR) {
                                notifyAuthServers(Constants.RELOAD_HOME_DONATION_PAGE, null);
                                target = SUCCESS;
                                DonationPageLoader.getInstance().forceReload();
                                request.getSession(true).setAttribute("newmessage", "<span style='color: green'>Successfully added</span>");
                            }
                        } else {
                            target = FAILURE;
                            request.getSession(true).setAttribute("newmessage", "<span style='color: red'>Failed to added(rc: " + reasonCode + ")</span>");
                        }
                        break;
                    case Constants.EDIT:
                        pageDTO = PageService.getInstance().getDonationPageById(donationPageForm.getPageId());
                        DonationPageDTO donationPageDTO = PageService.getInstance().getDonationPage();
                        if (pageDTO != null) {
                            donationPageForm.setPageRingId(pageDTO.getPageRingId());
                            donationPageForm.setPageType(pageDTO.getPageType());
                            donationPageForm.setPageName(pageDTO.getPageName());
                            donationPageForm.setPageOwnerId(pageDTO.getPageOwner());
                            donationPageForm.setCountry(pageDTO.getCountry());
                            donationPageForm.setProfileImageUrl(pageDTO.getProfileImageUrl());
                            donationPageForm.setCoverImageUrl(pageDTO.getCoverImageUrl());
                            if (donationPageDTO.getPageId() == donationPageForm.getPageId()) {
                                donationPageForm.setBannerImageUrl(donationPageDTO.getBannerImageUrl());
                                donationPageForm.setVisible(donationPageDTO.getVisible());
                            }
                        } else {
                            target = FAILURE;
                        }
                        break;

                    case Constants.UPDATE:

                        LoginTaskScheduler.getInstance().login(Constants.USERID, Constants.PASSWORD);
                        if (donationPageForm.getTheFileProfileImage().getFileName() != null && donationPageForm.getTheFileProfileImage().getFileName().length() > 0) {
                            String fileName = donationPageForm.getTheFileProfileImage().getFileName();
                            byte[] fileData = donationPageForm.getTheFileProfileImage().getFileData();
                            ImageDTO imageDTO = new UploadImage().upLoadToCloud(Constants.USERID, fileName, fileData, Constants.PROFILE_IMAGE_UPLOAD_URL);
                            if (imageDTO != null) {
                                profileImageUrl = imageDTO.getImageURL();
                                donationPageForm.setProfileImageUrl(profileImageUrl);
                                imageDTO.setImageType(AppConstants.IMAGE_TYPE_PROFILE_IMAGE);
                                imageDTO.setImagePrivacy(AppConstants.PROFILE_IMAGE_PRIVACY);
                                imageDTO.setUserTableID(donationPageForm.getPageId());
                                int rc = PageService.getInstance().addProfileOrCoverImage(imageDTO);
                                profileImageId = imageDTO.getImageID();
                                System.out.println(profileImageUrl);
                                pageDTO.setProfileImageUrl(profileImageUrl);
                                pageDTO.setProfileImageId(profileImageId);
                            }
                        }
                        if (donationPageForm.getTheFileCoverImage().getFileName() != null && donationPageForm.getTheFileCoverImage().getFileName().length() > 0) {
                            String fileName = donationPageForm.getTheFileCoverImage().getFileName();
                            byte[] fileData = donationPageForm.getTheFileCoverImage().getFileData();
                            ImageDTO imageDTO = new UploadImage().upLoadToCloud(Constants.USERID, fileName, fileData, Constants.COVER_IMAGE_UPLOAD_URL);
                            if (imageDTO != null) {
                                coverImageUrl = imageDTO.getImageURL();
                                donationPageForm.setCoverImageUrl(coverImageUrl);
                                imageDTO.setImageType(AppConstants.IMAGE_TYPE_COVER_IMAGE);
                                imageDTO.setImagePrivacy(AppConstants.COVER_IMAGE_PRIVACY);
                                imageDTO.setUserTableID(donationPageForm.getPageId());
                                int rc = PageService.getInstance().addProfileOrCoverImage(imageDTO);
                                coverImageId = imageDTO.getImageID();
                                System.out.println(coverImageUrl);
                                pageDTO.setCoverImageUrl(coverImageUrl);
                                pageDTO.setCoverImageId(coverImageId);
                            }
                        }
                        LoginTaskScheduler.getInstance().logout(Constants.USERID);
                        pageDTO.setPageName(donationPageForm.getPageName());
                        pageDTO.setPageType(AppConstants.UserTypeConstants.DONATION_PAGE);
                        pageDTO.setPageId(donationPageForm.getPageId());
                        pageDTO.setPageRingId(donationPageForm.getPageRingId());
                        pageDTO.setCountry(donationPageForm.getCountry());
                        pageDTO.setPageOwner(donationPageForm.getPageOwnerId());

                        RingLogger.getActivityLogger().debug("[update DonationPageAction] activistName : " + activistName + " pageDTO --> " + new Gson().toJson(pageDTO));

                        reasonCode = PageService.getInstance().updateDonationPage(pageDTO, donationPageForm.getPageId());
                        if (reasonCode == AppConstants.NONE) {
                            if (donationPageForm.getTheFileBannerImage().getFileName().length() > 0 && donationPageForm.getTheFileBannerImage().getFileData().length > 0) {
                                RingLogger.getConfigPortalLogger().debug("## SessionLessCommunication destroyed.");
                                error = LoginTaskScheduler.getInstance().login(Constants.USERID, Constants.PASSWORD);
                                RingLogger.getConfigPortalLogger().debug("##  login --> " + new Gson().toJson(error));

                                bannerUrl = new UploadBannerImage().upLoadToCloud(Constants.USERID, donationPageForm.getTheFileBannerImage().getFileName(), donationPageForm.getTheFileBannerImage().getFileData());
                                RingLogger.getConfigPortalLogger().debug("##  bannerUrl --> " + bannerUrl);
                                error = LoginTaskScheduler.getInstance().logout(Constants.USERID);
                                RingLogger.getConfigPortalLogger().debug("##  myAppError --> " + new Gson().toJson(error));
                            }
                            myAppError = PageService.getInstance().addDonationPageBannerImageAndVisibility(donationPageForm.getPageId(), bannerUrl, donationPageForm.getVisible());
                            if (myAppError.getERROR_TYPE() == MyAppError.NOERROR) {
                                notifyAuthServers(Constants.RELOAD_HOME_DONATION_PAGE, null);
                                target = SUCCESS;
                                DonationPageLoader.getInstance().forceReload();
                                request.getSession(true).setAttribute("newmessage", "<span style='color: green'>Successfully updated</span>");
                            }
                        } else {
                            target = FAILURE;
                            request.getSession(true).setAttribute("newmessage", "<span style='color: red'>Failed to update(rc: " + reasonCode + ")</span>");
                        }
                        break;

                    case Constants.DELETE:
                        RingLogger.getActivityLogger().debug("[delete DonationPageAction] activistName : " + activistName + " pageDTO --> " + new Gson().toJson(pageDTO));

                        reasonCode = PageService.getInstance().deactivateDonationPage(donationPageForm.getPageId());
                        if (reasonCode == AppConstants.NONE) {
                            target = SUCCESS;
                            request.getSession(true).setAttribute("newmessage", "<span style='color: green'>Successfully deleted </span>");
                        } else {
                            target = FAILURE;
                            request.getSession(true).setAttribute("newmessage", "<span style='color: red'>Failed to delete (rc: " + reasonCode + ")</span>");
                        }
                        break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            target = FAILURE;
            logger.error(e);
            request.getSession(true).setAttribute("newmessage", "<span style='color: red'>Failed to added(reason: Expception Occured)</span>");
        }
        return mapping.findForward(target);
    }
}
