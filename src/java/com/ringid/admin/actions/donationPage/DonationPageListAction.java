/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.donationPage;

import com.ringid.admin.dto.DonationPageDTO;
import com.ringid.admin.repository.DonationPageLoader;
import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.service.PageService;
import com.ringid.admin.utils.Constants;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.ringid.pages.PageDTO;

/**
 *
 * @author mamun
 */
public class DonationPageListAction extends BaseAction {

    private static final Logger logger = RingLogger.getConfigPortalLogger();

    List<PageDTO> pageDTOs = new ArrayList<>();

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = SUCCESS;
        DonationPageForm pageForm = (DonationPageForm) form;
        try {
            if (request.getParameter("ForceReload") != null && "1".equals(request.getParameter("ForceReload"))) {
                DonationPageLoader.getInstance().forceReload();
            }
            DonationPageDTO dto = new DonationPageDTO();
            if (pageForm.getSearchText() != null && pageForm.getSearchText().length() > 0) {
                dto.setSearchText(pageForm.getSearchText().trim().toLowerCase());
            }
            if (pageForm.getColumn() <= 0) {
                pageForm.setColumn(Constants.COLUMN_ONE);
                pageForm.setSort(Constants.DESC_SORT);
            }
            dto.setColumn(pageForm.getColumn());
            dto.setSortType(pageForm.getSort());
            pageDTOs = PageService.getInstance().getDonationPages(dto);
            DonationPageDTO donationPageDTO = PageService.getInstance().getDonationPage();

            pageForm.setPageId(donationPageDTO.getPageId());
            pageForm.setBannerImageUrl(donationPageDTO.getBannerImageUrl());
            pageForm.setVisible(donationPageDTO.getVisible());

            request.getSession(true).setAttribute(Constants.DATA_ROWS, pageDTOs);
            request.setAttribute("donationPageId", pageForm.getPageId());
            managePages(request, pageForm, pageDTOs.size());
        } catch (Exception e) {
            logger.error(e);
            target = FAILURE;
        }
        return mapping.findForward(target);
    }
}
