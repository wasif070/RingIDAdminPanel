/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.donationPage;

import com.ringid.admin.BaseForm;
import com.ringid.admin.utils.Constants;
import javax.servlet.http.HttpServletRequest;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.upload.FormFile;

/**
 *
 * @author mamun
 */
public class DonationPageForm extends BaseForm {

    private String pageName;
    private int pageType;
    private long pageId;
    private long pageRingId;
    private String country;
    private String bannerImageUrl;
    private String profileImageUrl;
    private String coverImageUrl;
    private long pageOwnerId;
    private FormFile theFileBannerImage;
    private FormFile theFileProfileImage;
    private FormFile theFileCoverImage;
    private int visible;

    public int getVisible() {
        return visible;
    }

    public void setVisible(int visible) {
        this.visible = visible;
    }

    public String getBannerImageUrl() {
        return bannerImageUrl;
    }

    public void setBannerImageUrl(String bannerImageUrl) {
        this.bannerImageUrl = bannerImageUrl;
    }

    public FormFile getTheFileBannerImage() {
        return theFileBannerImage;
    }

    public void setTheFileBannerImage(FormFile theFileBannerImage) {
        this.theFileBannerImage = theFileBannerImage;
    }

    public String getPageName() {
        return pageName;
    }

    public void setPageName(String pageName) {
        this.pageName = pageName;
    }

    public int getPageType() {
        return pageType;
    }

    public void setPageType(int pageType) {
        this.pageType = pageType;
    }

    public long getPageId() {
        return pageId;
    }

    public void setPageId(long pageId) {
        this.pageId = pageId;
    }

    public long getPageRingId() {
        return pageRingId;
    }

    public void setPageRingId(long pageRingId) {
        this.pageRingId = pageRingId;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getProfileImageUrl() {
        return profileImageUrl;
    }

    public void setProfileImageUrl(String profileImageUrl) {
        this.profileImageUrl = profileImageUrl;
    }

    public String getCoverImageUrl() {
        return coverImageUrl;
    }

    public void setCoverImageUrl(String coverImageUrl) {
        this.coverImageUrl = coverImageUrl;
    }

    public long getPageOwnerId() {
        return pageOwnerId;
    }

    public void setPageOwnerId(long pageOwnerId) {
        this.pageOwnerId = pageOwnerId;
    }

    public FormFile getTheFileProfileImage() {
        return theFileProfileImage;
    }

    public void setTheFileProfileImage(FormFile theFileProfileImage) {
        this.theFileProfileImage = theFileProfileImage;
    }

    public FormFile getTheFileCoverImage() {
        return theFileCoverImage;
    }

    public void setTheFileCoverImage(FormFile theFileCoverImage) {
        this.theFileCoverImage = theFileCoverImage;
    }

    @Override
    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        ActionErrors errors = new ActionErrors();
        if (Constants.ADD == getAction() || Constants.UPDATE == getAction()) {
            if (getPageName() == null || getPageName().length() < 1) {
                errors.add("pageName", new ActionMessage("errors.roomname.required"));
            }
            if (getCountry() == null || getCountry().length() < 1) {
                errors.add("country", new ActionMessage("errors.roomname.required"));
            }
            if (Constants.ADD == getAction()) {
                if (getPageOwnerId() <= 0) {
                    errors.add("pageOwnerId", new ActionMessage("errors.roomname.required"));
                }
            }
        }
        return errors;
    }
}
