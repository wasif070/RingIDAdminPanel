/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.ringFeature;

import com.ringid.admin.dto.RingFeatureDTO;
import com.ringid.admin.service.RingFeatureService;
import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.MyAppError;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.ringid.utilities.AppConstants;

/**
 *
 * @author Rabby
 */
public class RingFeatureMappingAction extends BaseAction {

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = SUCCESS;
        try {
            RingFeatureForm ringFeatureForm = (RingFeatureForm) form;
            RingFeatureDTO ringFeatureDTO = new RingFeatureDTO();
            ringFeatureDTO.setId(ringFeatureForm.getId());
            ringFeatureDTO.setAppType(ringFeatureForm.getAppType());
            int successCount = 0;
            int failureCount = 0;
            int versions[] = {-1, -1, -1, -1, -1};
            setMinVersions(versions, ringFeatureForm);
            MyAppError error;
            for (int i = 0; i < versions.length; i++) {
                int version = versions[i];
                if (version >= 0) {
                    ringFeatureDTO.setPlatform(i + 1);
                    ringFeatureDTO.setMinVersion(version);
                    error = RingFeatureService.getInstance().addOrUpdateFeatureMapping(ringFeatureDTO);
                    if (error.getERROR_TYPE() == AppConstants.NONE) {
                        successCount++;
                    } else {
                        failureCount++;
                    }
                }
            }
            request.getSession(true).setAttribute("message", "<span style='color: green'>Operation successfull(successCount: " + successCount + ", failureCount: " + (failureCount) + ").</span>");
        } catch (Exception e) {
            target = FAILURE;
        }
        return mapping.findForward(target);
    }

    private void setMinVersions(int[] versions, RingFeatureForm form) {
        if (form.isPcVersionChk()) {
            versions[0] = form.getPcVersion();
        }
        if (form.isAndroidVersionChk()) {
            versions[1] = form.getAndroidVersion();
        }
        if (form.isIosVersionChk()) {
            versions[2] = form.getIosVersion();
        }
        if (form.isWindowsVersionChk()) {
            versions[3] = form.getWindowsVersion();
        }
        if (form.isWebVersionChk()) {
            versions[4] = form.getWebVersion();
        }
    }
}
