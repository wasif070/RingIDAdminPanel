/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.ringFeature;

import com.ringid.admin.BaseForm;
import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author Rabby
 */
public class RingFeatureForm extends BaseForm {

    private int id;
    private String featureName;
    private int appType;
    private String appTypeName;
    private int platForm;
    private String platfromName;
    private int minVersion;
    private int status;
    private int[] minVersions;
    private boolean[] disableCheckboxs;
    private int pcVersion;
    private int androidVersion;
    private int iosVersion;
    private int windowsVersion;
    private int webVersion;
    private boolean pcVersionChk;
    private boolean androidVersionChk;
    private boolean iosVersionChk;
    private boolean windowsVersionChk;
    private boolean webVersionChk;

    public RingFeatureForm() {
        pcVersion = -1;
        androidVersion = -1;
        iosVersion = -1;
        windowsVersion = -1;
        webVersion = -1;
        pcVersionChk = true;
        androidVersionChk = true;
        iosVersionChk = true;
        windowsVersionChk = true;
        webVersionChk = true;
    }

    public boolean isPcVersionChk() {
        return pcVersionChk;
    }

    public void setPcVersionChk(boolean pcVersionChk) {
        this.pcVersionChk = pcVersionChk;
    }

    public boolean isAndroidVersionChk() {
        return androidVersionChk;
    }

    public void setAndroidVersionChk(boolean androidVersionChk) {
        this.androidVersionChk = androidVersionChk;
    }

    public boolean isIosVersionChk() {
        return iosVersionChk;
    }

    public void setIosVersionChk(boolean iosVersionChk) {
        this.iosVersionChk = iosVersionChk;
    }

    public boolean isWindowsVersionChk() {
        return windowsVersionChk;
    }

    public void setWindowsVersionChk(boolean windowsVersionChk) {
        this.windowsVersionChk = windowsVersionChk;
    }

    public boolean isWebVersionChk() {
        return webVersionChk;
    }

    public void setWebVersionChk(boolean webVersionChk) {
        this.webVersionChk = webVersionChk;
    }

    public boolean[] getDisableCheckboxs() {
        return disableCheckboxs;
    }

    public void setDisableCheckboxs(boolean[] disableCheckboxs) {
        this.disableCheckboxs = disableCheckboxs;
    }

    public int getPcVersion() {
        return pcVersion;
    }

    public void setPcVersion(int pcVersion) {
        this.pcVersion = pcVersion;
    }

    public int getAndroidVersion() {
        return androidVersion;
    }

    public void setAndroidVersion(int androidVersion) {
        this.androidVersion = androidVersion;
    }

    public int getIosVersion() {
        return iosVersion;
    }

    public void setIosVersion(int iosVersion) {
        this.iosVersion = iosVersion;
    }

    public int getWindowsVersion() {
        return windowsVersion;
    }

    public void setWindowsVersion(int windowsVersion) {
        this.windowsVersion = windowsVersion;
    }

    public int getWebVersion() {
        return webVersion;
    }

    public void setWebVersion(int webVersion) {
        this.webVersion = webVersion;
    }

    public String getAppTypeName() {
        return appTypeName;
    }

    public void setAppTypeName(String appTypeName) {
        this.appTypeName = appTypeName;
    }

    public String getPlatfromName() {
        return platfromName;
    }

    public void setPlatfromName(String platfromName) {
        this.platfromName = platfromName;
    }

    public int[] getMinVersions() {
        return minVersions;
    }

    public void setMinVersions(int[] minVersions) {
        this.minVersions = minVersions;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFeatureName() {
        return featureName;
    }

    public void setFeatureName(String featureName) {
        this.featureName = featureName;
    }

    public int getAppType() {
        return appType;
    }

    public void setAppType(int appType) {
        this.appType = appType;
    }

    public int getPlatForm() {
        return platForm;
    }

    public void setPlatForm(int platForm) {
        this.platForm = platForm;
    }

    public int getMinVersion() {
        return minVersion;
    }

    public void setMinVersion(int minVersion) {
        this.minVersion = minVersion;
    }

    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        ActionErrors errors = new ActionErrors();
        return errors;
    }
}
