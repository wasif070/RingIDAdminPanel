/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.ringFeature;

import com.ringid.admin.dto.RingFeatureDTO;
import com.ringid.admin.service.RingFeatureService;
import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.utils.Constants;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author Rabby
 */
public class RingFeatureListAction extends BaseAction {

    private static Logger logger = RingLogger.getConfigPortalLogger();
    private List<RingFeatureDTO> list;

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = SUCCESS;
        try {
            RingFeatureForm featureForm = (RingFeatureForm) form;
            RingFeatureDTO dto = new RingFeatureDTO();
            if (featureForm.getColumn() <= 0) {
                featureForm.setColumn(Constants.COLUMN_ONE);
                featureForm.setSort(Constants.ASC_SORT);
            }
            dto.setSearchText(featureForm.getSearchText());
            dto.setColumn(featureForm.getColumn());
            dto.setSortType(featureForm.getSort());
            list = RingFeatureService.getInstance().getFeatureList(dto);
            managePages(request, featureForm, list.size());
            request.getSession(true).setAttribute(Constants.DATA_ROWS, list);
        } catch (Exception e) {
            logger.error(e);
            target = FAILURE;
        }
        return mapping.findForward(target);
    }
}
