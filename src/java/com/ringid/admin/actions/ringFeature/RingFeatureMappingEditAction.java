/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.ringFeature;

import com.ringid.admin.dto.RingFeatureDTO;
import com.ringid.admin.service.RingFeatureService;
import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author Rabby
 */
public class RingFeatureMappingEditAction extends BaseAction {

    private static Logger logger = RingLogger.getConfigPortalLogger();

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = SUCCESS;
        try {
            RingFeatureForm ringFeatureForm = (RingFeatureForm) form;
            List<RingFeatureDTO> list = RingFeatureService.getInstance().getFeatureMappingByFeatureId(ringFeatureForm.getId(), ringFeatureForm.getAppType());
            int[] versions = {-1, -1, -1, -1, -1};
            if (list != null && list.size() > 0) {
                ringFeatureForm.setId(list.get(0).getId());
                ringFeatureForm.setFeatureName(list.get(0).getFeatureName());
                ringFeatureForm.setAppType(list.get(0).getAppType());
                ringFeatureForm.setAppTypeName(list.get(0).getAppTypeName());
                for (RingFeatureDTO dto : list) {
                    versions[dto.getPlatform() - 1] = dto.getMinVersion();
                }
                request.setAttribute("versions", versions);
            }
        } catch (Exception e) {
            logger.error(e);
            target = FAILURE;
        }
        return mapping.findForward(target);
    }
}
