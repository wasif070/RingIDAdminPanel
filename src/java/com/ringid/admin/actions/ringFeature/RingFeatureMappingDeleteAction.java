/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.ringFeature;

import com.ringid.admin.service.RingFeatureService;
import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.MyAppError;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.ringid.utilities.AppConstants;

/**
 *
 * @author Rabby
 */
public class RingFeatureMappingDeleteAction extends BaseAction {

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = SUCCESS;
        try {
            RingFeatureForm ringFeatureForm = (RingFeatureForm) form;
            MyAppError error = RingFeatureService.getInstance().deleteFeatureMappingById(ringFeatureForm.getId(), ringFeatureForm.getAppType());
            if (error.getERROR_TYPE() == AppConstants.NONE) {
                request.getSession(true).setAttribute("message", "<span style='color: green'>Operation Successfull.</span>");
            } else {
                request.getSession(true).setAttribute("message", "<span style='color: red'>Operation Failed.</span>");
            }
        } catch (Exception e) {
            target = FAILURE;
        }
        return mapping.findForward(target);
    }
}
