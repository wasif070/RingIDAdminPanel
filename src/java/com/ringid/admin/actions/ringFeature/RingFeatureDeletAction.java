/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.ringFeature;

import com.ringid.admin.service.RingFeatureService;
import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.utils.MyAppError;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.ringid.utilities.AppConstants;

/**
 *
 * @author Rabby
 */
public class RingFeatureDeletAction extends BaseAction {

    private static Logger logger = RingLogger.getConfigPortalLogger();

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = SUCCESS;
        try {
            RingFeatureForm featureForm = (RingFeatureForm) form;
            MyAppError error = RingFeatureService.getInstance().deleteFeature(featureForm.getId());
            if (error.getERROR_TYPE() == AppConstants.NONE) {
                request.setAttribute("message", "<span style='color: green'>Operation successfull.</span>");
            } else {
                request.setAttribute("message", "<span style='color: red'>Operation failed.</span>");
            }
        } catch (Exception e) {
            logger.error(e);
            target = FAILURE;
        }
        return mapping.findForward(target);
    }
}
