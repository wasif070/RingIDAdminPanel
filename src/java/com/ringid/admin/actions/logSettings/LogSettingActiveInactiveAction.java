/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.logSettings;

import com.ringid.admin.dto.LogSettingDTO;
import com.ringid.admin.service.LogSettingService;
import com.ringid.admin.repository.LogSettingLoader;
import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.dto.adminAuth.LoginDTO;
import com.ringid.admin.utils.Constants;
import com.ringid.admin.utils.MyAppError;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author Rabby
 */
public class LogSettingActiveInactiveAction extends BaseAction {

    Logger logger = RingLogger.getConfigPortalLogger();
    private final int ACTIVE = 1;
    private final int IN_ACTIVE = 0;

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = checkAuthentication(mapping, form, request);
        String activistName;
        try {
            LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
            activistName = loginDTO.getUserName();
        } catch (Exception e) {
            target = UNAUTHORIZED;
            return mapping.findForward(target);
        }
        try {
            if (target.equals(SUCCESS)) {
                LogSettingForm logSettingForm = (LogSettingForm) form;
                if (logSettingForm.getSelectedIDs() != null && logSettingForm.getSelectedIDs().length > 0) {

                    int[] selectedIDs = new int[logSettingForm.getSelectedIDs().length];
                    for (int i = 0; i < logSettingForm.getSelectedIDs().length; i++) {
                        selectedIDs[i] = Integer.parseInt(logSettingForm.getSelectedIDs()[i]);
                    }
                    RingLogger.getActivityLogger().debug("[LogSettingActiveInactiveAction] activistName : " + activistName);
                    LogSettingService scheduler = LogSettingService.getInstance();
                    List<LogSettingDTO> logSettingDTOList = scheduler.getLogSettingInfoListById(selectedIDs);
                    String operation = request.getParameter("oprKey");
                    switch (operation) {
                        case "active":
                            setState(logSettingDTOList, ACTIVE);
                            break;
                        case "inactive":
                            setState(logSettingDTOList, IN_ACTIVE);
                            break;
                    }
                    MyAppError error = scheduler.updateLogSettingList(logSettingDTOList);
                    if (error.getERROR_TYPE() == MyAppError.NOERROR) {
                        LogSettingLoader.getInstance().forceReload();
                    }
                }
            }
        } catch (Exception e) {
            logger.error("LogSettingActiveInactiveAction Exception: " + e);
            target = FAILURE;
        }
        return mapping.findForward(target);
    }

    private void setState(List<LogSettingDTO> logSettingDTOList, int state) {
        for (int i = 0; i < logSettingDTOList.size(); i++) {
            logSettingDTOList.get(i).setValue(state);
        }
    }
}
