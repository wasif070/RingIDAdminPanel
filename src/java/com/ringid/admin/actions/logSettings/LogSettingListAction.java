/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.logSettings;

import com.ringid.admin.dto.LogSettingDTO;
import com.ringid.admin.service.LogSettingService;
import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.Constants;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author Rabby
 */
public class LogSettingListAction extends BaseAction {

    List<LogSettingDTO> list = new ArrayList<>();

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = SUCCESS;
        try {
            LogSettingForm logSettingForm = (LogSettingForm) form;
            LogSettingDTO logSettingDTO = new LogSettingDTO();
            LogSettingService scheduler = LogSettingService.getInstance();
            if (logSettingForm.getSearchText() != null && logSettingForm.getSearchText().length() > 0) {
                logSettingDTO.setSearchText(logSettingForm.getSearchText().trim().toLowerCase());
            }
            if (logSettingForm.getColumn() <= 0) {
                logSettingForm.setColumn(Constants.COLUMN_ONE);
                logSettingForm.setSort(Constants.DESC_SORT);
            }
            logSettingDTO.setColumn(logSettingForm.getColumn());
            logSettingDTO.setSortType(logSettingForm.getSort());
            list = scheduler.getLogSettingList(logSettingDTO);
            request.getSession(true).setAttribute(Constants.DATA_ROWS, list);
            managePages(request, logSettingForm, list.size());
        } catch (NullPointerException e) {

        }
        return mapping.findForward(target);
    }
}
