/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.logSettings;

import com.ringid.admin.dto.LogSettingDTO;
import com.ringid.admin.service.LogSettingService;
import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author Rabby
 */
public class LogSettingEditAction extends BaseAction {

    Logger logger = RingLogger.getConfigPortalLogger();

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = checkAuthentication(mapping, form, request);
        try {
            if (target.equals(SUCCESS)) {
                int id = 0;
                LogSettingForm logSettingForm = (LogSettingForm) form;
                if (request.getParameter("id") != null) {
                    id = Integer.parseInt(request.getParameter("id"));
                }
                LogSettingService scheduler = LogSettingService.getInstance();
                LogSettingDTO logSettingDTO = scheduler.getLogSettingInfoById(id);
                if (logSettingDTO != null) {
                    logSettingForm.setId(logSettingDTO.getId());
                    logSettingForm.setName(logSettingDTO.getName());
                    logSettingForm.setValue(logSettingDTO.getValue());
                    target = SUCCESS;
                } else {
                    target = FAILURE;
                }
            }
        } catch (Exception e) {
            logger.error("LogSettingEditAction Exception: " + e);
            target = FAILURE;
        }
        return mapping.findForward(target);
    }
}
