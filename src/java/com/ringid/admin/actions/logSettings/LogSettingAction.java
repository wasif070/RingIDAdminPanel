/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.logSettings;

import com.ringid.admin.dto.LogSettingDTO;
import com.ringid.admin.service.LogSettingService;
import com.ringid.admin.repository.LogSettingLoader;
import com.google.gson.Gson;
import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.dto.adminAuth.LoginDTO;
import com.ringid.admin.utils.Constants;
import com.ringid.admin.utils.MyAppError;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author Rabby
 */
public class LogSettingAction extends BaseAction {

    Logger logger = RingLogger.getConfigPortalLogger();

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = checkAuthentication(mapping, form, request);
        request.getSession(true).setAttribute("logSettingErrorMessege", "");
        String activistName;
        try {
            LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
            activistName = loginDTO.getUserName();
        } catch (Exception e) {
            target = UNAUTHORIZED;
            return mapping.findForward(target);
        }
        try {
            if (target.equals(SUCCESS)) {
                LogSettingForm logSettingForm = (LogSettingForm) form;
                LogSettingService scheduler = LogSettingService.getInstance();
                LogSettingDTO logSettingDTO = scheduler.getLogSettingDTO(logSettingForm);
                RingLogger.getActivityLogger().debug("Action : LogSettingAction[add] activistName : " + activistName + " logSettingDTO --> " + new Gson().toJson(logSettingDTO));
                MyAppError error = scheduler.addLogSettingInfo(logSettingDTO);
                if (error.getERROR_TYPE() > 0) {
                    if (error.getERROR_TYPE() == MyAppError.OTHERERROR) {
                        logger.error("LogSettingAction Other error " + error.getERROR_TYPE());
                        logSettingForm.setMessage(error.getErrorMessage());
                        request.getSession(true).setAttribute("logSettingErrorMessege", "<span style='color:red'>Insertion failed!</span>");
                    }
                    target = FAILURE;
                } else {
                    LogSettingLoader.getInstance().forceReload();
                }
            }
        } catch (Exception e) {
            request.getSession(true).setAttribute("logSettingErrorMessege", "<span style='color:red'>Insertion failed!</span>");
            target = FAILURE;
        }
        return mapping.findForward(target);
    }
}
