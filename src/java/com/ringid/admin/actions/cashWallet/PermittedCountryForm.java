/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.cashWallet;

import com.ringid.admin.BaseForm;
import com.ringid.admin.dto.adminAuth.LoginDTO;
import com.ringid.admin.utils.Constants;
import javax.servlet.http.HttpServletRequest;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

/**
 *
 * @author mamun
 */
public class PermittedCountryForm extends BaseForm {

    private int countryId;
    private String[] supportedPlatform;
    private String countryName;
    private String countryShortCode;
    private String currency;
    private String mobilePhoneDialingCode;
    private int signupCash;
    private int signupCoin;
    private int signupRingbit;
    private int referralCash;
    private int referralCoin;
    private int referralRingbit;
    private int cashoutlimit;
    private int status;
    
    public PermittedCountryForm(){
        status = -1;
    }

    public int getCountryId() {
        return countryId;
    }

    public void setCountryId(int countryId) {
        this.countryId = countryId;
    }

    public String[] getSupportedPlatform() {
        return supportedPlatform;
    }

    public void setSupportedPlatform(String[] supportedPlatform) {
        this.supportedPlatform = supportedPlatform;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getCountryShortCode() {
        return countryShortCode;
    }

    public void setCountryShortCode(String countryShortCode) {
        this.countryShortCode = countryShortCode;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getMobilePhoneDialingCode() {
        return mobilePhoneDialingCode;
    }

    public void setMobilePhoneDialingCode(String mobilePhoneDialingCode) {
        this.mobilePhoneDialingCode = mobilePhoneDialingCode;
    }

    public int getSignupCash() {
        return signupCash;
    }

    public void setSignupCash(int signupCash) {
        this.signupCash = signupCash;
    }

    public int getSignupCoin() {
        return signupCoin;
    }

    public void setSignupCoin(int signupCoin) {
        this.signupCoin = signupCoin;
    }

    public int getSignupRingbit() {
        return signupRingbit;
    }

    public void setSignupRingbit(int signupRingbit) {
        this.signupRingbit = signupRingbit;
    }

    public int getReferralCash() {
        return referralCash;
    }

    public void setReferralCash(int referralCash) {
        this.referralCash = referralCash;
    }

    public int getReferralCoin() {
        return referralCoin;
    }

    public void setReferralCoin(int referralCoin) {
        this.referralCoin = referralCoin;
    }

    public int getReferralRingbit() {
        return referralRingbit;
    }

    public void setReferralRingbit(int referralRingbit) {
        this.referralRingbit = referralRingbit;
    }

    public int getCashoutlimit() {
        return cashoutlimit;
    }

    public void setCashoutlimit(int cashoutlimit) {
        this.cashoutlimit = cashoutlimit;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        ActionErrors errors = new ActionErrors();
        LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        if (loginDTO == null) {
            errors.add("auth", new ActionMessage("UnAuthorized Access"));
        } else {
            if (Constants.ADD == getAction() || Constants.UPDATE == getAction()) {
                if (getCountryId() <= 0) {
                    errors.add("countryId", new ActionMessage("errors.data.required"));
                }
                if (getSupportedPlatform() == null || getSupportedPlatform().length < 0) {
                    errors.add("supportedPlatform", new ActionMessage("errors.data.required"));
                }
                if (getCurrency() == null || getCurrency().length() < 0) {
                    errors.add("currency", new ActionMessage("errors.data.required"));
                }
            }
        }
        return errors;
    }
}
