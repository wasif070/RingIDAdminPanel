/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.cashWallet;

import com.ringid.admin.dto.PermittedCountryDTO;
import com.ringid.admin.BaseAction;
import com.ringid.admin.dao.CoinDAO;
import com.ringid.admin.repository.PermittedCountryLoader;
import com.ringid.admin.service.CoinService;
import com.ringid.admin.utils.Constants;
import com.ringid.country.CountryLoader;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author mamun
 */
public class PermittedCountryListAction extends BaseAction {

    ArrayList<PermittedCountryDTO> list = new ArrayList<>();

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = FAILURE;
        try {
            long dataListSize = 0;
            PermittedCountryForm permittedCountryForm = (PermittedCountryForm) form;

            if (request.getParameter("ForceReload") != null && "1".equals(request.getParameter("ForceReload"))) {
                PermittedCountryLoader.getInstance().forceReload();
            }

            dataListSize = CoinDAO.getInstance().getTotalCountOfWalletPermittedCountry();

            if (permittedCountryForm.getCountryId() > 0) {
                permittedCountryForm.setMobilePhoneDialingCode(CountryLoader.getInstance().getCountryDTO(permittedCountryForm.getCountryId()).getDialingCode());
            }
            String str = "";
            if (permittedCountryForm.getSearchText() != null && permittedCountryForm.getSearchText().length() > 0) {
                str = permittedCountryForm.getSearchText().trim().toLowerCase();
            }
            list = CoinService.getInstance().getPermittedCountryList(permittedCountryForm.getMobilePhoneDialingCode(), permittedCountryForm.getStatus(), str);
            request.getSession(true).setAttribute(Constants.DATA_ROWS, list);
            managePages(request, permittedCountryForm, dataListSize);
        } catch (NullPointerException e) {
            target = FAILURE;
        }
        return mapping.findForward(target);
    }
}
