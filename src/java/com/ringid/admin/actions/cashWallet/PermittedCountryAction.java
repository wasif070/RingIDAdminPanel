/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.cashWallet;

import com.ringid.admin.dto.PermittedCountryDTO;
import com.ringid.admin.repository.PermittedCountryLoader;
import com.google.gson.Gson;
import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.dto.adminAuth.LoginDTO;
import com.ringid.admin.service.CoinService;
import com.ringid.admin.utils.Constants;
import com.ringid.admin.utils.MyAppError;
import com.ringid.country.CountryDTO;
import com.ringid.country.CountryLoader;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author mamun
 */
public class PermittedCountryAction extends BaseAction {

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        String target = checkAuthentication(mapping, form, request);
        String activistName;
        try {
            LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
            activistName = loginDTO.getUserName();
        } catch (Exception e) {
            target = UNAUTHORIZED;
            return mapping.findForward(target);
        }
        try {
            if (target.equals(SUCCESS)) {
                PermittedCountryForm permittedCountryForm = (PermittedCountryForm) form;
                MyAppError myAppError = new MyAppError();
                PermittedCountryDTO dto = new PermittedCountryDTO();
                String supportedplatform;
                CountryDTO countryDTO;

                switch (permittedCountryForm.getAction()) {
                    case Constants.ADD:
                    case Constants.UPDATE:
                        countryDTO = CountryLoader.getInstance().getCountryDTO(permittedCountryForm.getCountryId());
                        if (countryDTO != null) {
                            dto.setCountryName(countryDTO.getName());
                            dto.setCountryShortCode(countryDTO.getCode());
                            dto.setCurrency(permittedCountryForm.getCurrency());
                            dto.setMobilePhoneDialingCode(countryDTO.getDialingCode());
                            dto.setSignupCash(permittedCountryForm.getSignupCash());
                            dto.setSignupCoin(permittedCountryForm.getSignupCoin());
                            dto.setSignupRingbit(permittedCountryForm.getSignupRingbit());
                            dto.setReferralCash(permittedCountryForm.getReferralCash());
                            dto.setReferralCoin(permittedCountryForm.getReferralCoin());
                            dto.setReferralRingbit(permittedCountryForm.getReferralRingbit());
                            dto.setCashoutlimit(permittedCountryForm.getCashoutlimit());
                            dto.setStatus(permittedCountryForm.getStatus());
                            supportedplatform = "";
                            for (String str : permittedCountryForm.getSupportedPlatform()) {
                                if (supportedplatform.length() > 0) {
                                    supportedplatform += ",";
                                }
                                supportedplatform += str;
                            }
                            dto.setSupportedPlatform(supportedplatform);

                            RingLogger.getActivityLogger().debug("Action : PermittedCountryAction[add] activistName : " + activistName + " permittedCountryDTO --> " + new Gson().toJson(dto));

                            myAppError = CoinService.getInstance().updateRewardPermittedCountryInfo(dto);
                        } else {
                            myAppError = null;
                            target = FAILURE;
                        }
                        break;
                    case Constants.EDIT:
                        permittedCountryForm.setAction(Constants.UPDATE);
                        dto = CoinService.getInstance().getPermittedCountryInfoByCountryShortCode(permittedCountryForm.getCountryShortCode());

                        if (dto != null) {
                            permittedCountryForm.setCountryName(dto.getCountryName());
                            permittedCountryForm.setCountryShortCode(dto.getCountryShortCode());
                            permittedCountryForm.setMobilePhoneDialingCode(dto.getMobilePhoneDialingCode());
                            permittedCountryForm.setCurrency(dto.getCurrency());
                            permittedCountryForm.setSignupCash(dto.getSignupCash());
                            permittedCountryForm.setSignupCoin(dto.getSignupCoin());
                            permittedCountryForm.setSignupRingbit(dto.getSignupRingbit());
                            permittedCountryForm.setReferralCash(dto.getReferralCash());
                            permittedCountryForm.setReferralCoin(dto.getReferralCoin());
                            permittedCountryForm.setReferralRingbit(dto.getReferralRingbit());
                            permittedCountryForm.setCashoutlimit(dto.getCashoutlimit());
                            permittedCountryForm.setStatus(dto.getStatus());
                            String[] platform = dto.getSupportedPlatform().split(",");
                            permittedCountryForm.setSupportedPlatform(platform);
                            permittedCountryForm.setCountryId(CountryLoader.getInstance().getCountryId(permittedCountryForm.getMobilePhoneDialingCode()));
                        }
                        break;
                    case Constants.DELETE:
                        RingLogger.getActivityLogger().debug("Action : PermittedCountryAction[delete] activistName : " + activistName + " countryShortCode --> " + permittedCountryForm.getCountryShortCode());
                        myAppError = CoinService.getInstance().deleteRewardPermittedCountryInfo(permittedCountryForm.getCountryShortCode());
                        break;
                }
                if (myAppError == null || myAppError.getERROR_TYPE() > 0) {
                    request.getSession().setAttribute("msg", "<span style='color:red;'> Operation Failed </span>");
                } else {
                    request.getSession().setAttribute("msg", "<span style='color:green;'> Operation Successfull </span>");
                    PermittedCountryLoader.getInstance().forceReload();
                }
            }

        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception [BalanceListAction] --> " + e);
            target = FAILURE;
        }
        return mapping.findForward(target);
    }
}
