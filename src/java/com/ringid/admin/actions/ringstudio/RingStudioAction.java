/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.ringstudio;

import com.ringid.admin.dto.RingStudioDTO;
import com.ringid.admin.service.RingStudioService;
import com.google.gson.Gson;
import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.dto.adminAuth.LoginDTO;
import com.ringid.admin.utils.Constants;
import com.ringid.admin.utils.MyAppError;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author ipvision
 */
public class RingStudioAction extends BaseAction {

    protected final String EDIT_RING_STUDIO = "edit-ringstudio";
    static Logger loggger = RingLogger.getConfigPortalLogger();

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = checkAuthentication(mapping, form, request);
        String activistName;
        try {
            LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
            activistName = loginDTO.getUserName();
        } catch (Exception e) {
            target = UNAUTHORIZED;
            return mapping.findForward(target);
        }
        try {
            if (target.equals(SUCCESS)) {
                RingStudioForm ringStudioForm = (RingStudioForm) form;
                RingStudioDTO ringStudioDTO = new RingStudioDTO();
                RingStudioService scheduler = RingStudioService.getInstance();
                MyAppError myAppError;
                switch (ringStudioForm.getAction()) {
                    case Constants.ADD:
                        ringStudioDTO.setDevice(ringStudioForm.getDevice());
                        ringStudioDTO.setTitle(ringStudioForm.getTitle());
                        ringStudioDTO.setAppType(ringStudioForm.getAppType());
                        ringStudioDTO.setUrl(ringStudioForm.getUrl());
                        ringStudioDTO.setImageName(ringStudioForm.getImageName());
                        ringStudioDTO.setDisplayTitle(ringStudioForm.getDisplayTitle());
                        ringStudioDTO.setDisplayFloatingMenu(ringStudioForm.getDisplayFloatingMenu());
                        ringStudioDTO.setCountryCode(ringStudioForm.getCountryCode());
                        ringStudioDTO.setSupportedCountries(ringStudioForm.getSupportedCountries());

                        RingLogger.getActivityLogger().debug("[" + ringStudioForm.getAction() + " RingStudioAction] activistName : " + activistName + " ringStudioDTO --> " + new Gson().toJson(ringStudioDTO));
                        myAppError = scheduler.addRingStudioInfo(ringStudioDTO);
                        if (myAppError.getERROR_TYPE() > MyAppError.NOERROR) {
                            loggger.error("Error in addRingStudio: " + myAppError.getERROR_TYPE() + " ErrorMessage : " + myAppError.getErrorMessage());
                            ringStudioForm.setMessage(myAppError.getErrorMessage());
                            request.getSession(true).setAttribute("error", "2");
                            target = FAILURE;
                        }
                        break;
                    case Constants.EDIT:
                        ringStudioDTO = scheduler.getRingStudioInfoById(ringStudioForm.getId());
                        ringStudioForm.setAction(Constants.UPDATE);
                        ringStudioForm.setDevice(ringStudioDTO.getDevice());
                        ringStudioForm.setTitle(ringStudioDTO.getTitle());
                        ringStudioForm.setAppType(ringStudioDTO.getAppType());
                        ringStudioForm.setUrl(ringStudioDTO.getUrl());
                        ringStudioForm.setImageName(ringStudioDTO.getImageName());
                        ringStudioForm.setDisplayTitle(ringStudioDTO.getDisplayTitle());
                        ringStudioForm.setDisplayFloatingMenu(ringStudioDTO.getDisplayFloatingMenu());
                        ringStudioForm.setCountryCode(ringStudioDTO.getCountryCode());
                        ringStudioForm.setSupportedCountries(ringStudioDTO.getSupportedCountries());

                        target = EDIT_RING_STUDIO;
                        break;
                    case Constants.UPDATE:
                        ringStudioDTO.setId(ringStudioForm.getId());
                        ringStudioDTO.setDevice(ringStudioForm.getDevice());
                        ringStudioDTO.setTitle(ringStudioForm.getTitle());
                        ringStudioDTO.setAppType(ringStudioForm.getAppType());
                        ringStudioDTO.setUrl(ringStudioForm.getUrl());
                        ringStudioDTO.setImageName(ringStudioForm.getImageName());
                        ringStudioDTO.setDisplayTitle(ringStudioForm.getDisplayTitle());
                        ringStudioDTO.setDisplayFloatingMenu(ringStudioForm.getDisplayFloatingMenu());
                        ringStudioDTO.setCountryCode(ringStudioForm.getCountryCode());
                        ringStudioDTO.setSupportedCountries(ringStudioForm.getSupportedCountries());

                        RingLogger.getActivityLogger().debug("[" + ringStudioForm.getAction() + " RingStudioAction] activistName : " + activistName + " ringStudioDTO --> " + new Gson().toJson(ringStudioDTO));
                        myAppError = scheduler.updateRingStudio(ringStudioDTO);
                        if (myAppError.getERROR_TYPE() > MyAppError.NOERROR) {
                            loggger.error("Error in updateRingStudio: " + myAppError.getERROR_TYPE() + " ErrorMessage : " + myAppError.getErrorMessage());
                            ringStudioForm.setMessage(myAppError.getErrorMessage());
                            request.getSession(true).setAttribute("error", "2");
                            target = FAILURE;
                        }
                        break;
                    case Constants.DELETE:
                        RingLogger.getActivityLogger().debug("[" + ringStudioForm.getAction() + " RingStudioAction] activistName : " + activistName + " Id --> " + ringStudioForm.getId());
                        myAppError = scheduler.deleteRingStudio(ringStudioForm.getId());
                        if (myAppError.getERROR_TYPE() > MyAppError.NOERROR) {
                            loggger.error("Error in deleteRingStudio: " + myAppError.getERROR_TYPE() + " ErrorMessage : " + myAppError.getErrorMessage());
                            ringStudioForm.setMessage(myAppError.getErrorMessage());
                            request.getSession(true).setAttribute("error", "2");
                            target = FAILURE;
                        }
                        break;
                    default:
                        target = SUCCESS;
                        break;

                }
            }
        } catch (NullPointerException e) {
            loggger.error(e);
            target = FAILURE;
        }
        return mapping.findForward(target);
    }
}
