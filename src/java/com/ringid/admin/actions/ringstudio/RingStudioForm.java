package com.ringid.admin.actions.ringstudio;

import com.ringid.admin.BaseForm;
import com.ringid.admin.dto.adminAuth.LoginDTO;
import com.ringid.admin.projectMenu.MenuNames;
import com.ringid.admin.utils.Constants;
import javax.servlet.http.HttpServletRequest;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

public class RingStudioForm extends BaseForm {

    private int id;
    private int device;
    private int appType;
    private String title;
    private String url;
    private String imageName;
    private Integer displayTitle;
    private Integer displayFloatingMenu;
    private String countryCode;
    private String[] supportedCountries;

    public String[] getSupportedCountries() {
        return supportedCountries;
    }

    public void setSupportedCountries(String[] supportedCountries) {
        this.supportedCountries = supportedCountries;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getDevice() {
        return device;
    }

    public void setDevice(int device) {
        this.device = device;
    }

    public int getAppType() {
        return appType;
    }

    public void setAppType(int appType) {
        this.appType = appType;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public Integer getDisplayTitle() {
        return displayTitle;
    }

    public void setDisplayTitle(Integer displayTitle) {
        this.displayTitle = displayTitle;
    }

    public Integer getDisplayFloatingMenu() {
        return displayFloatingMenu;
    }

    public void setDisplayFloatingMenu(Integer displayFloatingMenu) {
        this.displayFloatingMenu = displayFloatingMenu;
    }

    @Override
    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        ActionErrors errors = new ActionErrors();
        LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        if (loginDTO == null) {
            errors.add("auth", new ActionMessage("UnAuthorized Access"));
        } else if (Constants.ADD == getAction() || Constants.UPDATE == getAction()) {
//                    if (getVersion() == null || getVersion().length() < 1) {
//                        errors.add("version", new ActionMessage("errors.version.required"));
//                    }
        }
        return errors;
    }

}
