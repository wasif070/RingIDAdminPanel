package com.ringid.admin.actions.ringstudio;

import com.ringid.admin.dto.RingStudioDTO;
import com.ringid.admin.service.RingStudioService;
import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.utils.Constants;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class RingStudioListAction extends BaseAction {

    static Logger loggger = RingLogger.getConfigPortalLogger();

    ArrayList<RingStudioDTO> list = new ArrayList<>();

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = FAILURE;
        try {
            RingStudioForm ringStudioForm = (RingStudioForm) form;
            RingStudioDTO ringStudioDTO = new RingStudioDTO();
            RingStudioService scheduler = RingStudioService.getInstance();

            if (ringStudioForm.getSearchText() != null && ringStudioForm.getSearchText().length() > 0) {
                ringStudioDTO.setSearchText(ringStudioForm.getSearchText().trim().toLowerCase());
            }

            list = scheduler.getRingStudioList(ringStudioDTO);
            loggger.debug("RingStudioListAction : [ TotalRecords : " + list.size() + " ]");

            request.getSession(true).setAttribute(Constants.DATA_ROWS, list);
            managePages(request, ringStudioForm, list.size());
        } catch (NullPointerException e) {
            loggger.error(e);
            target = FAILURE;
        }
        return mapping.findForward(target);
    }
}
