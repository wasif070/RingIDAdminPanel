/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.coinExchange;

import com.ringid.admin.dto.PaymentMethodDTO;
import com.ringid.admin.repository.PaymentMethodLoader;
import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.service.CoinService;
import com.ringid.admin.utils.Constants;
import com.ringid.admin.utils.MyAppError;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author mamun
 */
public class PaymentMethodAction extends BaseAction {

    static Logger loggger = RingLogger.getConfigPortalLogger();

    List<PaymentMethodDTO> list = new ArrayList<>();

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = FAILURE;
        MyAppError error = new MyAppError();
        try {
            PaymentMethodForm paymentMethodForm = (PaymentMethodForm) form;
            PaymentMethodDTO paymentMethodDTO = new PaymentMethodDTO();

            if (request.getParameter("ForceReload") != null && "1".equals(request.getParameter("ForceReload"))) {
                PaymentMethodLoader.getInstance().forceReload();
            }

            switch (paymentMethodForm.getAction()) {
                case Constants.ADD:
                    paymentMethodDTO = CoinService.getInstance().getPaymentMethodDTO(paymentMethodForm);
                    error = CoinService.getInstance().addPaymentMethod(paymentMethodDTO);
                    if (error.getERROR_TYPE() == MyAppError.NOERROR) {
                        target = SUCCESS;
                        PaymentMethodLoader.getInstance().forceReload();
                    }
                    break;
                case Constants.EDIT:
                    paymentMethodDTO = CoinService.getInstance().getPaymentMethodDTOById(paymentMethodForm.getId());
                    if (paymentMethodDTO != null) {
                        paymentMethodForm.setName(paymentMethodDTO.getName());
                        paymentMethodForm.setIsOpen(paymentMethodDTO.isIsOpen());
                        target = SUCCESS;
                    }
                    break;
                case Constants.UPDATE:
                    paymentMethodDTO = CoinService.getInstance().getPaymentMethodDTO(paymentMethodForm);
                    error = CoinService.getInstance().updatePaymentMethod(paymentMethodDTO);
                    if (error.getERROR_TYPE() == MyAppError.NOERROR) {
                        target = SUCCESS;
                        PaymentMethodLoader.getInstance().forceReload();
                    }
                    break;
                case Constants.DELETE:
                    error = CoinService.getInstance().deletePaymentMethod(paymentMethodForm.getId());
                    if (error.getERROR_TYPE() == MyAppError.NOERROR) {
                        target = SUCCESS;
                        PaymentMethodLoader.getInstance().forceReload();
                    }
                    break;
                default:
                    target = SUCCESS;
                    break;
            }
            if (SUCCESS.equals(target) && paymentMethodForm.getAction() != Constants.EDIT) {
                if (paymentMethodDTO == null) {
                    paymentMethodDTO = new PaymentMethodDTO();
                }
                if (paymentMethodForm.getSearchText() != null) {
                    paymentMethodDTO.setSearchText(paymentMethodForm.getSearchText().trim().toLowerCase());
                }
                if (paymentMethodForm.getColumn() <= 0) {
                    paymentMethodForm.setColumn(Constants.COLUMN_ONE);
                    paymentMethodForm.setSort(Constants.DESC_SORT);
                }
                paymentMethodDTO.setColumn(paymentMethodForm.getColumn());
                paymentMethodDTO.setSortType(paymentMethodForm.getSort());

                list = CoinService.getInstance().getPaymentMethodList(paymentMethodDTO);
                request.getSession(true).setAttribute(Constants.DATA_ROWS, list);
                managePages(request, paymentMethodForm, list.size());
            }
        } catch (NullPointerException e) {
            target = FAILURE;
        }
        return mapping.findForward(target);
    }
}
