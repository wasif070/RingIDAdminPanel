/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.newsPortal;

import com.ringid.admin.dto.newsportal.NewsPortalDTO;
import com.ringid.admin.cassandra.CassandraDAO;
import com.ringid.admin.utils.log.RingLogger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import org.apache.logging.log4j.Logger;
import org.ringid.pages.PageDTO;
import org.ringid.utilities.AppConstants;
import org.ringid.utilities.PivotedListReturnDTO;

/**
 *
 * @author ipvision
 */
public class FeaturedPageLoader {

    private final static long LOADING_INTERVAL = 60 * 60 * 1000;
    private long loadingTime = 0;
    private Map<Integer, List<NewsPortalDTO>> featuredNewsPortalMap;
    private final int LIMIT_ONE_THOUSAND = 1000;
    private static final Logger logger = RingLogger.getConfigPortalLogger();

    private FeaturedPageLoader() {
        featuredNewsPortalMap = new HashMap<>();
    }

    private static class FeaturedPageLoaderHolder {

        private static final FeaturedPageLoader INSTANCE = new FeaturedPageLoader();
    }

    public static FeaturedPageLoader getInstance() {
        return FeaturedPageLoaderHolder.INSTANCE;
    }

    private void loadData(int pageType) {
        List<NewsPortalDTO> newsPortalDTOs = new ArrayList<>();
        List<PageDTO> featuredPageDTOs = new ArrayList<>();
        List<Long> pageIds = new ArrayList<>();
        UUID pivotId = null;
        PivotedListReturnDTO<Long, UUID> pivotedListReturnDTO = new PivotedListReturnDTO<>();
        try {
            if (!featuredNewsPortalMap.containsKey(pageType)) {
                do {
                    pivotedListReturnDTO = CassandraDAO.getInstance().getFeaturedPageIds(pageType, pivotId, LIMIT_ONE_THOUSAND, AppConstants.SCROLL_DOWN);
                    if (pivotedListReturnDTO != null && pivotedListReturnDTO.getList().size() > 0) {
                        pageIds.addAll(pivotedListReturnDTO.getList());
                        pivotId = pivotedListReturnDTO.getPivot();
                    }
                } while (pivotedListReturnDTO != null && pivotedListReturnDTO.getList().size() == LIMIT_ONE_THOUSAND);

                featuredPageDTOs = CassandraDAO.getInstance().getPageDetailsForAdmin(pageIds, false);

                for (PageDTO pageDTO : featuredPageDTOs) {
                    NewsPortalDTO dto = new NewsPortalDTO();
                    dto.setPageId(pageDTO.getPageId());
                    dto.setPageName(pageDTO.getPageName());
                    dto.setCountryName(pageDTO.getCountry());
                    dto.setPageCatName(pageDTO.getPageCategoryName());
                    dto.setPageRingId(pageDTO.getPageRingId());
                    dto.setPageActiveStatus(pageDTO.getPageActiveStatus());
                    dto.setPageOwnerId(pageDTO.getPageOwner());
                    if (pageDTO.isIsAutoFollowDisabled() != null) {
                        dto.setIsAutoFollowDisabled(pageDTO.isIsAutoFollowDisabled());
                    } else {
                        dto.setIsAutoFollowDisabled(true);
                    }
                    dto.setWeight(pageDTO.getWeight());

                    newsPortalDTOs.add(dto);
                }

                logger.debug("FeaturedPageLoader pageType --> " + pageType + " newsPortalDTOList size --> " + newsPortalDTOs.size());
                featuredNewsPortalMap.put(pageType, newsPortalDTOs);
            }
        } catch (Exception ex) {
            logger.debug("Exception in [FeaturedPageLoader] loadData --> ", ex);
        }
    }

    public void forceReload(int pageType) {
        featuredNewsPortalMap.remove(pageType);
    }

    public void forceReload() {
        loadingTime = System.currentTimeMillis();
        featuredNewsPortalMap.clear();
    }

    private void checkForReload() {
        long currentTime = System.currentTimeMillis();
        if (currentTime - loadingTime > LOADING_INTERVAL) {
            forceReload();
        }
    }

    public List<NewsPortalDTO> getFeaturedPageList(NewsPortalDTO sdto) {
        checkForReload();
        List<NewsPortalDTO> newsPortalDTOs = new ArrayList<>();
        try {
            if (!featuredNewsPortalMap.containsKey(sdto.getPageType())) {
                loadData(sdto.getPageType());
            }
            for (NewsPortalDTO newsPortalDTO : featuredNewsPortalMap.get(sdto.getPageType())) {
                if (sdto.getSearchText() != null && sdto.getSearchText().length() > 0) {
                    if (!(newsPortalDTO.getPageName().toLowerCase().contains(sdto.getSearchText().trim().toLowerCase())
                            || String.valueOf(newsPortalDTO.getPageId()).toLowerCase().contains(sdto.getSearchText().trim().toLowerCase())
                            || String.valueOf(newsPortalDTO.getPageOwnerId()).toLowerCase().contains(sdto.getSearchText().trim().toLowerCase())
                            || newsPortalDTO.getCountryName().toLowerCase().contains(sdto.getSearchText().trim().toLowerCase()))) {
                        continue;
                    }
                }
                newsPortalDTOs.add(newsPortalDTO);
            }
        } catch (Exception e) {
            logger.debug("Exception in [getFeaturedPageList] ", e);
        }
        logger.debug("[getFeaturedPageList] pageType --> " + sdto.getPageType() + " newsPortalDTOs size --> " + newsPortalDTOs.size());
        return newsPortalDTOs;
    }
}
