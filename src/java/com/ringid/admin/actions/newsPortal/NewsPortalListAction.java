/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.newsPortal;

import com.ringid.admin.dto.newsportal.NewsPortalDTO;
import com.ringid.admin.repository.NewsPortalLoader;
import com.ringid.admin.BaseAction;
import com.ringid.admin.service.PageService;
import com.ringid.admin.utils.Constants;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author Kazi Nasir Uddin Oly
 * @Date Oct 2, 2016
 */
public class NewsPortalListAction extends BaseAction {

    NewsPortalFeedBack newsPortalFeedBack = new NewsPortalFeedBack();

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = FAILURE;
        try {
            NewsPortalForm newsPortalForm = (NewsPortalForm) form;
            if (request.getParameter("ForceReload") != null && "1".equals(request.getParameter("ForceReload"))) {
                FeaturedPageLoader.getInstance().forceReload();
                NewsPortalLoader.getInstance().forceReload();
            }
            NewsPortalDTO newsPortalDTO = PageService.getInstance().getNewsPortalDTO(newsPortalForm);

            int limit = newsPortalForm.getRecordPerPage(), start = 0, pageNo = 1;
            if (newsPortalForm.getPageNo() > 0) {
                pageNo = newsPortalForm.getPageNo();
            }
            start = (pageNo - 1) * limit;
            if (newsPortalForm.getColumn() <= 0) {
                newsPortalForm.setColumn(Constants.COLUMN_ONE);
                newsPortalForm.setSort(Constants.DESC_SORT);
            }
            newsPortalDTO.setSearchText(newsPortalForm.getSearchText());
            newsPortalDTO.setColumn(newsPortalForm.getColumn());
            newsPortalDTO.setSortType(newsPortalForm.getSort());
            if (newsPortalForm.getDiscoverable() == 3) {
                newsPortalFeedBack = PageService.getInstance().getFeaturedPages(newsPortalDTO, start, limit);
            } else {
                newsPortalFeedBack = PageService.getInstance().getPageList(newsPortalDTO, start, limit);
            }
            request.getSession(true).setAttribute(Constants.DATA_ROWS, newsPortalFeedBack.getNewsPortalList());
            managePages(request, newsPortalForm, newsPortalFeedBack.getTotalResult());
        } catch (NullPointerException e) {
            target = FAILURE;
        }
        return mapping.findForward(target);
    }

}
