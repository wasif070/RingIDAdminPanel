/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.newsPortal;

import auth.com.dto.InfoDTo;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.utils.HTTPRequestProcessor;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import javax.imageio.ImageIO;
import org.apache.logging.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import org.ringid.newsfeeds.ImageDTO;
import ringid.LoginTaskScheduler;

/**
 *
 * @author Rabby
 */
public class UploadImage {

    private static final Logger logger = RingLogger.getConfigPortalLogger();

    public ImageDTO upLoadToCloud(String USER_ID, String filename, byte[] fileData, String serverUrl) throws JSONException {
        ImageDTO imageDTO = null;
        Boolean returnVal = true;
        String imageUrl = null;
        int height = 100;
        int width = 100;
        try {
            InputStream in = new ByteArrayInputStream(fileData);
            BufferedImage buf = ImageIO.read(in);
            height = buf.getHeight();
            width = buf.getWidth();
            logger.debug("WIDTH: " + width + " HEIGHT: " + height);
        } catch (Exception e) {
        }

        HashMap<String, Object> headerValues = getheaderValues();
        HashMap<String, Object> bodyValues = getBodyValues(USER_ID, height, width);
        ArrayList<String> names = new ArrayList<>();
        names.add(filename);
        ArrayList<byte[]> dataValues = new ArrayList<>();
        dataValues.add(fileData);
        String response = HTTPRequestProcessor.uploader_method("POST", headerValues, bodyValues, null, names, dataValues, serverUrl, "");
        JSONObject jSONObject = new JSONObject(response);
        response = jSONObject.getString("mainstr");
        logger.debug("[UploadImageToCloud] response --> " + response);
        if (response != null && response.length() > 0) {
            JSONObject obj = new JSONObject(response);
            if (obj.getBoolean("sucs")) {
                imageDTO = new ImageDTO();
                logger.error("Successfull to Upload : pathToFileImage -> [" + serverUrl + "] : response --> " + response);
                imageUrl = obj.getString("iurl");
                imageDTO.setImageURL(obj.getString("iurl"));
                imageDTO.setImageHeight(obj.getInt("ih"));
                imageDTO.setImageWidth(obj.getInt("iw"));
            } else {
                returnVal = false;
                logger.error("Failed to Upload : pathToFileImage -> [" + serverUrl + "] : response --> " + response);
                imageUrl = null;
            }
        }
        return imageDTO;
    }

    private HashMap<String, Object> getheaderValues() {
        HashMap<String, Object> values = new HashMap<>();
        values.put("access-control-allow-origin", "*");
        return values;
    }

    private static HashMap<String, Object> getBodyValues(String userId, int height, int width) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        HashMap<String, Object> bodyValues = new HashMap<>();
        bodyValues.put("cimX", 0);
        bodyValues.put("cimY", 0);
        bodyValues.put("ih", height);
        bodyValues.put("iw", width);
        bodyValues.put("sId", infoDTo.getSessionId());
        bodyValues.put("uId", infoDTo.getUserId());
        bodyValues.put("authServer", infoDTo.getAuthIP());
        bodyValues.put("comPort", infoDTo.getAuthPORT());
        bodyValues.put("x-app-version", 149);
        return bodyValues;
    }
}
