/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.newsPortal;

import com.ringid.admin.dto.newsportal.NewsPortalDTO;
import java.util.List;

/**
 *
 * @author Kazi Nasir Uddin Oly
 * @Date Oct 4, 2016
 */
public class NewsPortalFeedBack {

    private int totalResult;
    private List<NewsPortalDTO> newsPortalList;

    public int getTotalResult() {
        return totalResult;
    }

    public void setTotalResult(int totalResult) {
        this.totalResult = totalResult;
    }

    public List<NewsPortalDTO> getNewsPortalList() {
        return newsPortalList;
    }

    public void setNewsPortalList(List<NewsPortalDTO> newsPortalList) {
        this.newsPortalList = newsPortalList;
    }

}
