/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ringid.admin.actions.newsPortal.type;


import com.ringid.admin.dto.newsportal.NewsPortalTypeDTO;
import com.ringid.admin.repository.NewsPortalTypeLoader;
import com.ringid.admin.utils.log.RingLogger;
import javax.servlet.http.HttpServlet;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.Logger;
/**
 *
 * @author Kazi Nasir Uddin Oly 
 * @Date Jun 20, 2016
 */
@WebServlet(name = "DownloadNewsPortalTypes", urlPatterns = {"/Download/DownloadNewsPortalTypes"})
public class DownloadNewsPortalTypes extends HttpServlet{
    static Logger logger = RingLogger.getConfigPortalLogger();
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/csv");
        response.setHeader("Content-Disposition", "attachment; filename=\"NewsPortalTypes.csv\"");
        try{
            OutputStream outputStream = response.getOutputStream();
            ArrayList<NewsPortalTypeDTO> list = NewsPortalTypeLoader.getInstance().getNewsPortalTypeListForDownload();
            outputStream.write("Name".getBytes());
            outputStream.write(",".getBytes());
            outputStream.write("Status".getBytes());
            outputStream.write("\n".getBytes());
            for(NewsPortalTypeDTO d: list){
                outputStream.write(d.getName().getBytes());
                outputStream.write(",".getBytes());
                outputStream.write(String.valueOf(d.getStatus()).getBytes());
                outputStream.write("\n".getBytes());
            }
            outputStream.flush();
            outputStream.close();
        } catch(Exception e){
            logger.error("DownloadNewsPortalTypes Exception: " + e.toString());
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }

    @Override
    public String getServletInfo() {
        return "Short Description";
    }
}
