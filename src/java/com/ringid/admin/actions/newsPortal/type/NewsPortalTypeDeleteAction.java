/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.newsPortal.type;

import com.ringid.admin.repository.NewsPortalTypeLoader;
import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.dto.adminAuth.LoginDTO;
import com.ringid.admin.service.PageService;
import com.ringid.admin.utils.Constants;
import com.ringid.admin.utils.MyAppError;
import java.util.UUID;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author Kazi Nasir Uddin Oly
 * @Date Jun 20, 2016
 */
public class NewsPortalTypeDeleteAction extends BaseAction {

    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        String target = checkAuthentication(mapping, form, request);
        String activistName;
        try {
            LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
            activistName = loginDTO.getUserName();
        } catch (Exception e) {
            target = UNAUTHORIZED;
            return mapping.findForward(target);
        }
        try {
            if (target.equals(SUCCESS)) {
                UUID id = null;
                int pageType = 0;
                if (request.getParameter("id") != null) {
                    id = UUID.fromString(request.getParameter("id"));
                }
                if (request.getParameter("pageType") != null) {
                    pageType = Integer.parseInt(request.getParameter("pageType"));
                }
                if (id != null) {
                    RingLogger.getActivityLogger().debug("[NewsPortalTypeDeleteAction] activistName : " + activistName + " Id --> " + id);
                    MyAppError error = PageService.getInstance().deleteDefaultPageCategory(id, pageType);
                    if (error.getERROR_TYPE() > 0) {
                        target = FAILURE;
                    } else {
                        NewsPortalTypeLoader.getInstance().forceLoadData();
                        notifyAuthServers(Constants.RELOAD_PAGE_TYPES, null);
                    }
                } else {
                    target = FAILURE;
                }
            }
        } catch (Exception e) {
            target = FAILURE;
        }
        return mapping.findForward(target);
    }
}
