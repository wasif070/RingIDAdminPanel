/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.newsPortal.type;

import com.ringid.admin.dto.newsportal.NewsPortalTypeDTO;
import com.ringid.admin.repository.NewsPortalTypeLoader;
import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.Constants;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author Kazi Nasir Uddin Oly
 * @Date Jun 20, 2016
 */
public class NewsPortalTypeListAction extends BaseAction {

    ArrayList<NewsPortalTypeDTO> list = new ArrayList<>();

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = FAILURE;
        try {
            NewsPortalTypeForm newsPortalTypeForm = (NewsPortalTypeForm) form;
            NewsPortalTypeDTO newsPortalTypeDTO = new NewsPortalTypeDTO();
            NewsPortalTypeLoader scheduler = NewsPortalTypeLoader.getInstance();

            if (request.getParameter("ForceReload") != null && "1".equals(request.getParameter("ForceReload"))) {
                NewsPortalTypeLoader.getInstance().forceLoadData();
            }
            if (newsPortalTypeForm.getSearchText() != null && newsPortalTypeForm.getSearchText().length() > 0) {
                newsPortalTypeDTO.setSearchText(newsPortalTypeForm.getSearchText().trim().toLowerCase());
            }
            if (newsPortalTypeForm.getType() > 0) {
                newsPortalTypeDTO.setIsSearchByType(true);
                newsPortalTypeDTO.setType(newsPortalTypeForm.getType());
            }
            if (newsPortalTypeForm.getColumn() <= 0) {
                newsPortalTypeForm.setColumn(Constants.COLUMN_ONE);
                newsPortalTypeForm.setSort(Constants.DESC_SORT);
            }
            newsPortalTypeDTO.setColumn(newsPortalTypeForm.getColumn());
            newsPortalTypeDTO.setSortType(newsPortalTypeForm.getSort());
            list = scheduler.getNewsPortalTypeList(newsPortalTypeDTO);
            request.getSession(true).setAttribute(Constants.DATA_ROWS, list);
            managePages(request, newsPortalTypeForm, list.size());
            request.getSession(true).setAttribute("search", newsPortalTypeDTO.getName());
        } catch (NullPointerException e) {
            target = FAILURE;
        }
        return mapping.findForward(target);
    }
}
