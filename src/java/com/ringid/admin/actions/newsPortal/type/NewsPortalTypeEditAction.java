/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.newsPortal.type;

import com.ringid.admin.dto.newsportal.NewsPortalTypeDTO;
import com.ringid.admin.repository.NewsPortalTypeLoader;
import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import java.util.UUID;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author Kazi Nasir Uddin Oly
 * @Date Jun 20, 2016
 */
public class NewsPortalTypeEditAction extends BaseAction {

    Logger logger = RingLogger.getConfigPortalLogger();

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        String target = checkAuthentication(mapping, form, request);
        try {
            if (target.equals(SUCCESS)) {
//                int id = 0;
                UUID pageId = null;
                NewsPortalTypeForm newsPortalTypeForm = (NewsPortalTypeForm) form;
                if (request.getParameter("id") != null) {
                    pageId = UUID.fromString((String) request.getParameter("id"));
                }
                NewsPortalTypeLoader scheduler = NewsPortalTypeLoader.getInstance();
                NewsPortalTypeDTO newsPortalTypeDTO = scheduler.getNewsPortalTypeInfoById(pageId);
                if (newsPortalTypeDTO != null) {
                    newsPortalTypeForm.setPageId(newsPortalTypeDTO.getId().toString());
                    newsPortalTypeForm.setName(newsPortalTypeDTO.getName());
                    newsPortalTypeForm.setStatus(String.valueOf(newsPortalTypeDTO.getStatus()));
                    newsPortalTypeForm.setType(newsPortalTypeDTO.getType());
                    target = SUCCESS;
                }
            }
        } catch (Exception e) {
            logger.error("NewsPortalTypeEditAction Exception: " + e);
            target = FAILURE;
        }
        return mapping.findForward(target);
    }
}
