/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.newsPortal.type;

import com.ringid.admin.dto.newsportal.NewsPortalTypeDTO;
import com.ringid.admin.repository.NewsPortalTypeLoader;
import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.service.PageService;
import com.ringid.admin.utils.Constants;
import com.ringid.admin.utils.MyAppError;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author Kazi Nasir Uddin Oly
 * @Date Jun 20, 2016
 */
public class NewsPortalTypeUploadAction extends BaseAction {

    static Logger logger = RingLogger.getConfigPortalLogger();
    String line = "";
    String cvsSplitBy = ",";

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = checkAuthentication(mapping, form, request);

        if (target.equals(SUCCESS)) {
            String dataArray[] = getDataFromFile(form);
            try {
                ArrayList<NewsPortalTypeDTO> newsPortalTypeList = new ArrayList<>();

                for (String s : dataArray) {
                    String[] newsPortalType = s.split(cvsSplitBy);
                    String name = newsPortalType[0];
                    String status = newsPortalType[1];
                    if (name.equalsIgnoreCase("Name")) {
                        continue;
                    }

                    NewsPortalTypeDTO newsPortalTypeDTO = new NewsPortalTypeDTO();
                    newsPortalTypeDTO.setName(name);
                    newsPortalTypeDTO.setStatus(Integer.valueOf(status));
                    newsPortalTypeList.add(newsPortalTypeDTO);
                }

                MyAppError error = PageService.getInstance().addDefaultPageCategories(newsPortalTypeList);
                if (error.getERROR_TYPE() > 0) {
                    target = FAILURE;
                } else {
                    NewsPortalTypeLoader.getInstance().forceLoadData();
                    notifyAuthServers(Constants.RELOAD_PAGE_TYPES, null);
                }
            } catch (Exception e) {
                logger.error("NewsPortalTypeUploadAction Exception: " + e);
                target = FAILURE;
            }
        }
        return mapping.findForward(target);
    }
}
