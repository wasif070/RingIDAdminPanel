/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.newsPortal.type;

import com.ringid.admin.dto.newsportal.NewsPortalTypeDTO;
import com.ringid.admin.repository.NewsPortalTypeLoader;
import com.google.gson.Gson;
import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.dto.adminAuth.LoginDTO;
import com.ringid.admin.service.PageService;
import com.ringid.admin.utils.Constants;
import com.ringid.admin.utils.MyAppError;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author Kazi Nasir Uddin Oly
 * @Date Jun 20, 2016
 */
public class NewsPortalTypeAction extends BaseAction {

    Logger logger = RingLogger.getConfigPortalLogger();

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = checkAuthentication(mapping, form, request);
        String activistName;
        try {
            LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
            activistName = loginDTO.getUserName();
        } catch (Exception e) {
            target = UNAUTHORIZED;
            return mapping.findForward(target);
        }
        try {
            if (target.equals(SUCCESS)) {
                NewsPortalTypeForm newsPortalTypeForm = (NewsPortalTypeForm) form;
                NewsPortalTypeDTO newsPortalTypeDTO = NewsPortalTypeLoader.getInstance().getNewsPortalTypeDTO(newsPortalTypeForm);
                RingLogger.getActivityLogger().debug("Action : NewsPortalTypeAction[add] activistName : " + activistName + " newsPortalTypeDTO --> " + new Gson().toJson(newsPortalTypeDTO));
                MyAppError error = PageService.getInstance().addDefaultPageCategory(newsPortalTypeDTO);
                if (error.getERROR_TYPE() > 0) {
                    if (error.getERROR_TYPE() == MyAppError.OTHERERROR) {
                        logger.error("NewsPortalTypeAction Other error " + error.getERROR_TYPE());
                        newsPortalTypeForm.setMessage(error.getErrorMessage());
                        request.getSession(true).setAttribute("error", "2");
                    }
                    target = FAILURE;
                } else {
                    NewsPortalTypeLoader.getInstance().forceLoadData();
                    notifyAuthServers(Constants.RELOAD_PAGE_TYPES, null);
                }
            }
        } catch (Exception e) {
            target = FAILURE;
        }
        return mapping.findForward(target);
    }
}
