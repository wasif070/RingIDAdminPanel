/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.newsPortal.category;

import com.ringid.admin.dto.newsportal.NewsPortalCategoryDTO;
import com.ringid.admin.repository.NewsPortalCategoryLoader;
import com.google.gson.Gson;
import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.dto.adminAuth.LoginDTO;
import com.ringid.admin.service.PageService;
import com.ringid.admin.utils.Constants;
import com.ringid.admin.utils.MyAppError;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author Kazi Nasir Uddin Oly
 * @Date Jun 20, 2016
 */
public class NewsPortalCategoryUpdateAction extends BaseAction {

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        String target = checkAuthentication(mapping, form, request);
        String activistName;
        try {
            LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
            activistName = loginDTO.getUserName();
        } catch (Exception e) {
            target = UNAUTHORIZED;
            return mapping.findForward(target);
        }
        try {
            if (target.equals(SUCCESS)) {
                NewsPortalCategoryForm newsPortalCategoryForm = (NewsPortalCategoryForm) form;
                NewsPortalCategoryDTO newsPortalCategoryDTO = NewsPortalCategoryLoader.getInstance().getNewsPortalCategoryDTO(newsPortalCategoryForm);
                RingLogger.getActivityLogger().debug("[NewsPortalCategoryUpdateAction] activistName : " + activistName + " newsPortalCategoryDTO --> " + new Gson().toJson(newsPortalCategoryDTO));
                MyAppError error = PageService.getInstance().updatePageCategory(newsPortalCategoryDTO);
                if (error.getERROR_TYPE() > 0) {
                    target = FAILURE;
                    newsPortalCategoryForm.setMessage(error.getErrorMessage());
                    request.getSession(true).setAttribute("error", "2");
                } else {
                    NewsPortalCategoryLoader.getInstance().forceReload();
                    notifyAuthServers(Constants.RELOAD_PAGE_CATEGORIES, null);
                }
            }
        } catch (Exception e) {
        }
        return mapping.findForward(target);
    }
}
