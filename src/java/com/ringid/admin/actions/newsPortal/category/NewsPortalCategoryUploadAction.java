/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.newsPortal.category;

import com.ringid.admin.dto.newsportal.NewsPortalCategoryDTO;
import com.ringid.admin.repository.NewsPortalCategoryLoader;
import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.service.PageService;
import com.ringid.admin.utils.Constants;
import com.ringid.admin.utils.MyAppError;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author Kazi Nasir Uddin Oly
 * @Date Jun 20, 2016
 */
public class NewsPortalCategoryUploadAction extends BaseAction {

    static Logger logger = RingLogger.getConfigPortalLogger();
    String line = "";
    String cvsSplitBy = ",";

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = checkAuthentication(mapping, form, request);

        if (target.equals(SUCCESS)) {
            String dataArray[] = getDataFromFile(form);
            try {
                ArrayList<NewsPortalCategoryDTO> newsPortalCategoryList = new ArrayList<>();

                for (String s : dataArray) {
                    String[] newsPortalCategory = s.split(cvsSplitBy);
                    String name = newsPortalCategory[0];
                    String status = newsPortalCategory[1];
                    if (name.equalsIgnoreCase("Name")) {
                        continue;
                    }

                    NewsPortalCategoryDTO newsPortalCategoryDTO = new NewsPortalCategoryDTO();
                    newsPortalCategoryDTO.setName(name);
                    newsPortalCategoryDTO.setStatus(Integer.valueOf(status));
                    newsPortalCategoryList.add(newsPortalCategoryDTO);
                }

                MyAppError error = PageService.getInstance().addPageCategories(newsPortalCategoryList);
                if (error.getERROR_TYPE() > 0) {
                    target = FAILURE;
                } else {
                    NewsPortalCategoryLoader.getInstance().forceReload();
                    notifyAuthServers(Constants.RELOAD_PAGE_CATEGORIES, null);
                }
            } catch (Exception e) {
                logger.error("NewsPortalCategoryUploadAction" + e);
                target = FAILURE;
            }
        }
        return mapping.findForward(target);
    }
}
