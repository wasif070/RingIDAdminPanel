/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.newsPortal.category;

import com.ringid.admin.dto.newsportal.NewsPortalCategoryDTO;
import com.ringid.admin.repository.NewsPortalCategoryLoader;
import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.Constants;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author Kazi Nasir Uddin Oly
 * @Date Jun 20, 2016
 */
public class NewsPortalCategoryListAction extends BaseAction {

    ArrayList<NewsPortalCategoryDTO> list = new ArrayList<>();

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = FAILURE;
        try {
            NewsPortalCategoryForm newsPortalCategoryForm = (NewsPortalCategoryForm) form;
            NewsPortalCategoryDTO newsPortalCategoryDTO = new NewsPortalCategoryDTO();

            if (request.getParameter("ForceReload") != null && "1".equals(request.getParameter("ForceReload"))) {
                NewsPortalCategoryLoader.getInstance().forceReload();
            }
            if (newsPortalCategoryForm.getSearchText() != null && newsPortalCategoryForm.getSearchText().length() > 0) {
                newsPortalCategoryDTO.setSearchText(newsPortalCategoryForm.getSearchText().trim().toLowerCase());
            }
            if (newsPortalCategoryForm.getType() > 0) {
                newsPortalCategoryDTO.setIsSearchByType(true);
                newsPortalCategoryDTO.setType(newsPortalCategoryForm.getType());
            }
            if (newsPortalCategoryForm.getColumn() <= 0) {
                newsPortalCategoryForm.setColumn(Constants.COLUMN_ONE);
                newsPortalCategoryForm.setSort(Constants.DESC_SORT);
            }
            newsPortalCategoryDTO.setColumn(newsPortalCategoryForm.getColumn());
            newsPortalCategoryDTO.setSortType(newsPortalCategoryForm.getSort());
            list = NewsPortalCategoryLoader.getInstance().getNewsPortalCategoryList(newsPortalCategoryDTO);
            request.getSession(true).setAttribute(Constants.DATA_ROWS, list);
            managePages(request, newsPortalCategoryForm, list.size());
            request.getSession(true).setAttribute("search", newsPortalCategoryDTO.getName());
        } catch (NullPointerException e) {
            target = FAILURE;
        }
        return mapping.findForward(target);
    }
}
