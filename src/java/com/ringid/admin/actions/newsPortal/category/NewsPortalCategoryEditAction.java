/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.newsPortal.category;

import com.ringid.admin.dto.newsportal.NewsPortalCategoryDTO;
import com.ringid.admin.repository.NewsPortalCategoryLoader;
import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import java.util.UUID;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author Kazi Nasir Uddin Oly
 * @Date Jun 20, 2016
 */
public class NewsPortalCategoryEditAction extends BaseAction {

    Logger logger = RingLogger.getConfigPortalLogger();

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        String target = checkAuthentication(mapping, form, request);
        try {
            if (target.equals(SUCCESS)) {
                int id = 0;
                UUID uuid = null;
                NewsPortalCategoryForm newsPortalCategoryForm = (NewsPortalCategoryForm) form;
                if (request.getParameter("id") != null) {
                    uuid = UUID.fromString((String) request.getParameter("id"));
                }
                NewsPortalCategoryDTO newsPortalCategoryDTO = NewsPortalCategoryLoader.getInstance().getNewsPortalCategoryDTO(uuid);
                if (newsPortalCategoryDTO != null) {
                    newsPortalCategoryForm.setPageId(newsPortalCategoryDTO.getId().toString());
                    newsPortalCategoryForm.setName(newsPortalCategoryDTO.getName());
                    newsPortalCategoryForm.setStatus(String.valueOf(newsPortalCategoryDTO.getStatus()));
                    newsPortalCategoryForm.setType(newsPortalCategoryDTO.getType());
                    target = SUCCESS;
                }
            }
        } catch (Exception e) {
            logger.error("NewsPortalCategoryEditAction Exception: " + e);
            target = FAILURE;
        }
        return mapping.findForward(target);
    }
}
