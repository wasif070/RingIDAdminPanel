/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.newsPortal;

import com.google.gson.Gson;
import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.dto.adminAuth.LoginDTO;
import com.ringid.admin.service.PageService;
import com.ringid.admin.utils.Constants;
import com.ringid.admin.utils.MyAppError;
import com.ringid.admin.utils.Utils;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

/**
 *
 * @author Kazi Nasir Uddin Oly
 * @Date Oct 2, 2016
 */
public class MakeDiscoverableAction extends BaseAction {

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = FAILURE;
        MyAppError myAppError = null;
        ActionErrors err = new ActionErrors();

        String activistName;
        try {
            LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
            activistName = loginDTO.getUserName();
        } catch (Exception e) {
            target = UNAUTHORIZED;
            return mapping.findForward(target);
        }

        try {
            NewsPortalForm newsPortalForm = (NewsPortalForm) form;
            if (newsPortalForm.getSelectedIDs() != null && newsPortalForm.getSelectedIDs().length > 0) {
                String submitType = newsPortalForm.getSubmitType();
                RingLogger.getActivityLogger().debug("Action : MakeDiscoverableAction[" + submitType + "] activistName : " + activistName + " pageIds --> " + new Gson().toJson(newsPortalForm.getSelectedIDs()) + " pageType --> " + newsPortalForm.getPageType());
                switch (submitType) {
                    case "Make Discoverable":
                        myAppError = PageService.getInstance().changePagesDiscoverableState(newsPortalForm.getSelectedIDs(), true);
                        break;
                    case "Make Not Discoverable":
                        myAppError = PageService.getInstance().changePagesDiscoverableState(newsPortalForm.getSelectedIDs(), false);
                        break;
                    case "Make Trending":
                        myAppError = PageService.getInstance().makePagesTrending(newsPortalForm.getSelectedIDs());
                        break;
                    case "Make Not Trending":
                        myAppError = PageService.getInstance().makePagesNotTrending(newsPortalForm.getSelectedIDs());
                        break;
                    case "Make Featured":
                        myAppError = PageService.getInstance().makePageFeatured(newsPortalForm.getSelectedIDs(), newsPortalForm.getPageType());
                        break;
                    case "Make Unfeatured":
                        myAppError = PageService.getInstance().makePageUnfeatured(newsPortalForm.getSelectedIDs(), newsPortalForm.getPageType(), true);
                        break;
                }
                FeaturedPageLoader.getInstance().forceReload();
                if (myAppError.getERROR_TYPE() == MyAppError.NOERROR) {
                    target = SUCCESS;
                    err.add("errormsg", new ActionMessage("<span style='color:green'>" + myAppError.getErrorMessage() + "</span>", false));
                    saveErrors(request.getSession(), err);
//                    NewsPortalLoader.getInstance().forceLoadData();
                } else {
                    err.add("errormsg", new ActionMessage("<span style='color:red'>" + Utils.getMessageForReasonCode(myAppError.getERROR_TYPE()) + "</span>", false));
                    saveErrors(request.getSession(), err);
                }
            } else {
                err.add("errormsg", new ActionMessage("errors.select.one"));
                saveErrors(request.getSession(), err);
            }

        } catch (NullPointerException e) {
            target = FAILURE;
        }
        return mapping.findForward(target);
    }
}
