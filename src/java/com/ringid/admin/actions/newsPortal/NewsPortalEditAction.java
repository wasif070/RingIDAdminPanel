/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.newsPortal;

import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.service.PageService;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.ringid.pages.PageDTO;

/**
 *
 * @author Rabby
 */
public class NewsPortalEditAction extends BaseAction {

    Logger logger = RingLogger.getConfigPortalLogger();

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = checkAuthentication(mapping, form, request);
        try {
            if (target.equals(SUCCESS)) {
                NewsPortalForm newsPortalForm = (NewsPortalForm) form;
                PageDTO pageDTO = PageService.getInstance().getPageInfo(newsPortalForm.getPageId());
                if (pageDTO != null) {
                    newsPortalForm.setPageRingId(pageDTO.getPageRingId());
                    newsPortalForm.setPageType(pageDTO.getPageType());
                    newsPortalForm.setPageName(pageDTO.getPageName());
                    newsPortalForm.setCountry(pageDTO.getCountry());
                    newsPortalForm.setProfileImageUrl(pageDTO.getProfileImageUrl());
                    newsPortalForm.setCoverImageUrl(pageDTO.getCoverImageUrl());
                    newsPortalForm.setOwnerId(pageDTO.getPageOwner());
                } else {
                    target = FAILURE;
                }
            }
        } catch (Exception e) {
            target = FAILURE;
            logger.error(e);
        }

        return mapping.findForward(target);
    }
}
