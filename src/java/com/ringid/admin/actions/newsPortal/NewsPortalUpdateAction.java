/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.newsPortal;

import com.google.gson.Gson;
import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.dto.adminAuth.LoginDTO;
import com.ringid.admin.service.PageService;
import com.ringid.admin.utils.Constants;
import com.ringid.admin.utils.Utils;
import java.util.UUID;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.ringid.newsfeeds.ImageDTO;
import org.ringid.pages.PageDTO;
import org.ringid.utilities.AppConstants;
import ringid.LoginTaskScheduler;

/**
 *
 * @author Rabby
 */
public class NewsPortalUpdateAction extends BaseAction {

    Logger logger = RingLogger.getConfigPortalLogger();

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = checkAuthentication(mapping, form, request);
        String activistName;
        try {
            LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
            activistName = loginDTO.getUserName();
        } catch (Exception e) {
            target = UNAUTHORIZED;
            return mapping.findForward(target);
        }
        try {
            if (target.equals(SUCCESS)) {
                NewsPortalForm newsPortalForm = (NewsPortalForm) form;

                String profileImageUrl = newsPortalForm.getProfileImageUrl();
                UUID profileImageId = null;
                String coverImageUrl = newsPortalForm.getCoverImageUrl();
                UUID coverImageId = null;

                LoginTaskScheduler.getInstance().login(Constants.USERID, Constants.PASSWORD);
                if (newsPortalForm.getTheFileProfileImage().getFileName() != null && newsPortalForm.getTheFileProfileImage().getFileName().length() > 0) {
                    String fileName = newsPortalForm.getTheFileProfileImage().getFileName();
                    byte[] fileData = newsPortalForm.getTheFileProfileImage().getFileData();
                    ImageDTO imageDTO = new UploadImage().upLoadToCloud(Constants.USERID, fileName, fileData, Constants.PROFILE_IMAGE_UPLOAD_URL);
                    profileImageUrl = imageDTO.getImageURL();
                    newsPortalForm.setProfileImageUrl(profileImageUrl);
                    imageDTO.setImageType(AppConstants.IMAGE_TYPE_PROFILE_IMAGE);
                    imageDTO.setImagePrivacy(AppConstants.PROFILE_IMAGE_PRIVACY);
                    imageDTO.setUserTableID(newsPortalForm.getPageId());
                    int rc = PageService.getInstance().addProfileOrCoverImage(imageDTO);
                    profileImageId = imageDTO.getImageID();
                    System.out.println(profileImageUrl);
                }
                if (newsPortalForm.getTheFileCoverImage().getFileName() != null && newsPortalForm.getTheFileCoverImage().getFileName().length() > 0) {
                    String fileName = newsPortalForm.getTheFileCoverImage().getFileName();
                    byte[] fileData = newsPortalForm.getTheFileCoverImage().getFileData();
                    ImageDTO imageDTO = new UploadImage().upLoadToCloud(Constants.USERID, fileName, fileData, Constants.COVER_IMAGE_UPLOAD_URL);
                    coverImageUrl = imageDTO.getImageURL();
                    newsPortalForm.setCoverImageUrl(coverImageUrl);
                    imageDTO.setImageType(AppConstants.IMAGE_TYPE_COVER_IMAGE);
                    imageDTO.setImagePrivacy(AppConstants.COVER_IMAGE_PRIVACY);
                    imageDTO.setUserTableID(newsPortalForm.getPageId());
                    int rc = PageService.getInstance().addProfileOrCoverImage(imageDTO);
                    coverImageId = imageDTO.getImageID();
                    System.out.println(coverImageUrl);
                }
                LoginTaskScheduler.getInstance().logout(Constants.USERID);

                PageDTO pageDTO = new PageDTO();
                pageDTO.setPageName(newsPortalForm.getPageName());
                pageDTO.setPageType(newsPortalForm.getPageType());
                pageDTO.setPageId(newsPortalForm.getPageId());
                pageDTO.setPageRingId(newsPortalForm.getPageRingId());
                pageDTO.setCountry(newsPortalForm.getCountry());
                pageDTO.setProfileImageUrl(profileImageUrl);
                pageDTO.setProfileImageId(profileImageId);
                pageDTO.setCoverImageUrl(coverImageUrl);
                pageDTO.setCoverImageId(coverImageId);
                pageDTO.setOwnerId(newsPortalForm.getOwnerId());
                pageDTO.setPageOwner(newsPortalForm.getOwnerId());

                RingLogger.getActivityLogger().debug("[NewsPortalUpdateAction] activistName : " + activistName + " pageDTO --> " + new Gson().toJson(pageDTO));
                int reasonCode = PageService.getInstance().updatePage(pageDTO);
                if (reasonCode == AppConstants.NONE) {
                    target = SUCCESS;
                    request.getSession(true).setAttribute("newmessage", "<span style='color: green'>Successfully Updated</span>");
                } else {
                    target = FAILURE;
                    request.getSession(true).setAttribute("newmessage", "<span style='color: red'>Failed to update " + Utils.getMessageForReasonCode(reasonCode) + " </span>");
                }
            }
        } catch (Exception e) {
            logger.error(e);
            target = FAILURE;
        }
        return mapping.findForward(target);
    }
}
