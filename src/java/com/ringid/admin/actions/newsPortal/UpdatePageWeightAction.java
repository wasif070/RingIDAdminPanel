/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.newsPortal;

import com.ringid.admin.repository.NewsPortalLoader;
import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.dto.adminAuth.LoginDTO;
import com.ringid.admin.service.PageService;
import com.ringid.admin.service.UserService;
import com.ringid.admin.utils.Constants;
import com.ringid.admin.utils.Utils;
import java.util.UUID;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.ringid.newsfeeds.ImageDTO;
import org.ringid.pages.PageDTO;
import org.ringid.utilities.AppConstants;
import ringid.LoginTaskScheduler;

/**
 *
 * @author mamun
 */
public class UpdatePageWeightAction extends BaseAction {

    Logger logger = RingLogger.getConfigPortalLogger();

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = checkAuthentication(mapping, form, request);
        String activistName;
        NewsPortalForm newsPortalForm = (NewsPortalForm) form;
        try {
            LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
            activistName = loginDTO.getUserName();
        } catch (Exception e) {
            target = UNAUTHORIZED;
            return mapping.findForward(target);
        }
        try {
            if (target.equals(SUCCESS)) {
                switch (newsPortalForm.getAction()) {
                    case Constants.EDIT:
                        newsPortalForm.setAction(Constants.UPDATE);
                        if (newsPortalForm.getPageId() > 0) {
                            PageDTO pageDTO = PageService.getInstance().getPageInfo(newsPortalForm.getPageId());
                            if (pageDTO != null) {
                                newsPortalForm.setProfileImageUrl(pageDTO.getProfileImageUrl());
                                newsPortalForm.setCoverImageUrl(pageDTO.getCoverImageUrl());
                            }
                        }
                        break;
                    case Constants.UPDATE: {
                        RingLogger.getActivityLogger().debug("[UpdatePageWeightAction] activistName : " + activistName + " pageId --> " + newsPortalForm.getPageId() + " pageType --> " + newsPortalForm.getPageType() + " weight --> " + newsPortalForm.getWeight());
                        int reasonCode = PageService.getInstance().updatePageWeight(newsPortalForm.getPageId(), newsPortalForm.getPageType(), newsPortalForm.getWeight());
                        if (reasonCode == AppConstants.NONE) {
                            target = SUCCESS;
                            NewsPortalLoader.getInstance().forceReload(newsPortalForm.getPageType());
                            FeaturedPageLoader.getInstance().forceReload(newsPortalForm.getPageType());
                            request.setAttribute("message", "<span style='color: green'>Successfully Updated</span>");
                        } else {
                            target = FAILURE;
                            request.setAttribute("message", "<span style='color: red'>Failed to update " + Utils.getMessageForReasonCode(reasonCode) + " </span>");
                        }

                        //update profile and cover image for product page
                        if (newsPortalForm.getPageType() == AppConstants.UserTypeConstants.PRODUCT_PAGE) {
                            String profileImageUrl = newsPortalForm.getProfileImageUrl();
                            UUID profileImageId = null;
                            String coverImageUrl = newsPortalForm.getCoverImageUrl();
                            UUID coverImageId = null;
                            long ownerId = UserService.getInstance().getPageOwnerId(newsPortalForm.getPageId());
                            LoginTaskScheduler.getInstance().login(Constants.USERID, Constants.PASSWORD);
                            if (newsPortalForm.getTheFileProfileImage().getFileName() != null && newsPortalForm.getTheFileProfileImage().getFileName().length() > 0) {
                                String fileName = newsPortalForm.getTheFileProfileImage().getFileName();
                                byte[] fileData = newsPortalForm.getTheFileProfileImage().getFileData();
                                ImageDTO imageDTO = new UploadImage().upLoadToCloud(Constants.USERID, fileName, fileData, Constants.PROFILE_IMAGE_UPLOAD_URL);
                                profileImageUrl = imageDTO.getImageURL();
                                newsPortalForm.setProfileImageUrl(profileImageUrl);
                                imageDTO.setImageType(AppConstants.IMAGE_TYPE_PROFILE_IMAGE);
                                imageDTO.setImagePrivacy(AppConstants.PROFILE_IMAGE_PRIVACY);
                                imageDTO.setUserTableID(newsPortalForm.getPageId());
                                int rc = PageService.getInstance().addProfileOrCoverImage(imageDTO);
                                profileImageId = imageDTO.getImageID();
                                reasonCode = PageService.getInstance().updatePageProfileImage(ownerId, newsPortalForm.getPageId(), profileImageId, profileImageUrl);
                                System.out.println(profileImageUrl + " reasonCode -> " + reasonCode);
                            }
                            if (newsPortalForm.getTheFileCoverImage().getFileName() != null && newsPortalForm.getTheFileCoverImage().getFileName().length() > 0) {
                                String fileName = newsPortalForm.getTheFileCoverImage().getFileName();
                                byte[] fileData = newsPortalForm.getTheFileCoverImage().getFileData();
                                ImageDTO imageDTO = new UploadImage().upLoadToCloud(Constants.USERID, fileName, fileData, Constants.COVER_IMAGE_UPLOAD_URL);
                                coverImageUrl = imageDTO.getImageURL();
                                newsPortalForm.setCoverImageUrl(coverImageUrl);
                                imageDTO.setImageType(AppConstants.IMAGE_TYPE_COVER_IMAGE);
                                imageDTO.setImagePrivacy(AppConstants.COVER_IMAGE_PRIVACY);
                                imageDTO.setUserTableID(newsPortalForm.getPageId());
                                int rc = PageService.getInstance().addProfileOrCoverImage(imageDTO);
                                coverImageId = imageDTO.getImageID();
                                reasonCode = PageService.getInstance().updatePageCoverImage(ownerId, newsPortalForm.getPageId(), coverImageId, coverImageUrl, 0, 0);
                                System.out.println(coverImageUrl + " reasonCode -> " + reasonCode);
                            }
                            LoginTaskScheduler.getInstance().logout(Constants.USERID);
                        }
                        break;
                    }
                }
            }
        } catch (Exception e) {
            logger.error(e);
            target = FAILURE;
        }
        if (SUCCESS.equals(target)) {
            return new ActionForward(mapping.findForward(target).getPath() + "?pageType=" + newsPortalForm.getPageType() + "&discoverable=" + newsPortalForm.getDiscoverable());
        }
        return mapping.findForward(target);
    }
}
