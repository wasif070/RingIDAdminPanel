/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.appUpdateUrl;

import com.google.gson.Gson;
import com.ringid.admin.BaseAction;
import com.ringid.admin.dto.AppUpdateUrlDTO;
import com.ringid.admin.dto.adminAuth.LoginDTO;
import com.ringid.admin.service.AdminService;
import com.ringid.admin.utils.Constants;
import com.ringid.admin.utils.MyAppError;
import com.ringid.admin.utils.Utils;
import com.ringid.admin.utils.log.RingLogger;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author mamun
 */
public class AppUpdateUrlAction extends BaseAction {

    static Logger loggger = RingLogger.getConfigPortalLogger();

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = FAILURE;
        String activistName;
        try {
            LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
            activistName = loginDTO.getUserName();
        } catch (Exception e) {
            target = UNAUTHORIZED;
            return mapping.findForward(target);
        }
        try {
            List<AppUpdateUrlDTO> list = new ArrayList<>();
            AppUpdateUrlForm appUpdateUrlForm = (AppUpdateUrlForm) form;
            AppUpdateUrlDTO dto = new AppUpdateUrlDTO();
            MyAppError error = null;
            switch (appUpdateUrlForm.getAction()) {
                case Constants.ADD: {
                    dto = AdminService.getInstance().getAppUpdateUrlDTO(appUpdateUrlForm);
                    RingLogger.getActivityLogger().debug("Action : AppUpdateUrlAction[add] activistName : " + activistName + " appUpdateUrlDTO --> " + new Gson().toJson(dto));
                    error = AdminService.getInstance().addAppUpdateUrlInfo(dto);
                    if (error.getERROR_TYPE() > 0) {
                        target = FAILURE;
                        request.setAttribute("message", error.getErrorMessage());
                    } else {
                        target = SUCCESS;
                        notifyAuthServers(Constants.RELOAD_APP_UPDATE_URL, "");
                        Utils.forceAppUpdateUrlsAPIToReload();
                    }
                    break;
                }
                case Constants.EDIT: {
                    if (appUpdateUrlForm.getId() > 0) {
                        target = SUCCESS;
                    } else {
                        target = FAILURE;
                    }
                    break;
                }
                case Constants.UPDATE: {
                    RingLogger.getActivityLogger().debug("Action : AppUpdateUrlAction[update] activistName : " + activistName + " id --> " + appUpdateUrlForm.getId() + " update_url --> " + appUpdateUrlForm.getUpdateUrl());
                    error = AdminService.getInstance().updateAppUpdateUrlInfo(appUpdateUrlForm.getId(), appUpdateUrlForm.getUpdateUrl());
                    if (error.getERROR_TYPE() > 0) {
                        target = FAILURE;
                        request.setAttribute("message", error.getErrorMessage());
                    } else {
                        target = SUCCESS;
                        notifyAuthServers(Constants.RELOAD_APP_UPDATE_URL, "");
                        Utils.forceAppUpdateUrlsAPIToReload();
                    }
                    break;
                }
                case Constants.DELETE: {
                    RingLogger.getActivityLogger().debug("Action : AppUpdateUrlAction[delete] activistName : " + activistName + " id --> " + appUpdateUrlForm.getId());
                    error = AdminService.getInstance().deleteAppUpdateUrlInfo(appUpdateUrlForm.getId());
                    if (error.getERROR_TYPE() > 0) {
                        target = FAILURE;
                        request.setAttribute("message", error.getErrorMessage());
                    } else {
                        target = SUCCESS;
                        notifyAuthServers(Constants.RELOAD_APP_UPDATE_URL, "");
                        Utils.forceAppUpdateUrlsAPIToReload();
                    }
                    break;
                }
                default: {
                    target = SUCCESS;
                    break;
                }
            }
            if (SUCCESS.equals(target) && appUpdateUrlForm.getAction() != Constants.EDIT) {
                list = AdminService.getInstance().getAppUpdateUrlListInfo();
                request.getSession(true).setAttribute(Constants.DATA_ROWS, list);
                managePages(request, appUpdateUrlForm, list.size());
            }
        } catch (NullPointerException e) {
            target = FAILURE;
        }
        return mapping.findForward(target);
    }
}
