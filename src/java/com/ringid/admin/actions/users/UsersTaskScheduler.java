/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.users;

import com.ringid.admin.utils.MyAppError;
import java.util.ArrayList;

/**
 *
 * @author Rabby
 */
public class UsersTaskScheduler {

    private UsersTaskScheduler() {

    }

    private static class UsersTaskSchedulerHolder {

        private static final UsersTaskScheduler INSTANCE = new UsersTaskScheduler();
    }

    public static UsersTaskScheduler getInstance() {
        return UsersTaskSchedulerHolder.INSTANCE;
    }

    public ArrayList<UsersDTO> getUserList(UsersDTO dto) {
        return UsersDAO.getInstance().getUsersList(dto);
    }

    public UsersDTO getUsersDTO(UsersForm userForm) {
        UsersDTO userDTO = new UsersDTO();

        if (!userForm.isNull(userForm.getName())) {
            userDTO.setName(userForm.getName());
        }

        if (!userForm.isNull(userForm.getPassword())) {
            userDTO.setPassword(userForm.getPassword());
        }

        if (!userForm.isNull(userForm.getFullname())) {
            userDTO.setFullname(userForm.getFullname());
        }

        userDTO.setPermissionLevel(userForm.getPermissionLevel());

        return userDTO;
    }

    public MyAppError addUserInfo(UsersDTO p_dto) {
        UsersDAO userDAO = new UsersDAO();
        return userDAO.addUserInfo(p_dto);
    }

    public MyAppError deleteUser(int id) {
        UsersDAO userDAO = new UsersDAO();
        return userDAO.deleteUser(id);
    }

    public UsersDTO getUserInfoById(int id) {
        return UsersDAO.getInstance().getUserDTO(id);
    }

    public MyAppError updateUser(UsersDTO p_dto) {
        UsersDAO newsPortalCategoryDAO = new UsersDAO();
        return newsPortalCategoryDAO.updateUser(p_dto);
    }
}
