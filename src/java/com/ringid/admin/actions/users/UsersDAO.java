/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.users;

import com.ringid.admin.BaseDAO;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.actions.userFeatureManagement.userRole.UserRoleLoader;
import com.ringid.admin.utils.Constants;
import com.ringid.admin.utils.MyAppError;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Rabby
 */
public class UsersDAO extends BaseDAO {

    private HashMap<Integer, UsersDTO> usersHashMapList;
    private static UsersDAO usersDAO = null;

    public UsersDAO() {

    }

    public static UsersDAO getInstance() {
        if (usersDAO == null) {
            createDAO();
        }
        return usersDAO;
    }

    private synchronized static void createDAO() {
        if (usersDAO == null) {
            usersDAO = new UsersDAO();
        }
    }

    private void loadData() {
        usersHashMapList = new HashMap<>();
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            stmt = db.connection.createStatement();
            String sql = "SELECT *"
                    + "FROM admin_users;";
            rs = stmt.executeQuery(sql);
            while (rs.next()) {
                UsersDTO dto = new UsersDTO();
                dto.setId(rs.getInt("id"));
                dto.setName(rs.getString("user_name"));
                dto.setPassword(rs.getString("user_password"));
                dto.setFullname(rs.getString("user_full_name"));
                dto.setPermissionLevel(rs.getInt("permission_level"));
                dto.setPermissionLevelName(UserRoleLoader.getInstance().getUserRoleById(dto.getPermissionLevel()).getName());
                usersHashMapList.put(dto.getId(), dto);
            }
            RingLogger.getConfigPortalLogger().debug(sql + "::" + usersHashMapList.size());

        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().debug("User List Exception...", e);
        } finally {
            close();
        }
    }

    private ArrayList<UsersDTO> getUsersData(UsersDTO sdto) {
        loadData();
        ArrayList<UsersDTO> data = new ArrayList<>();
        Set set = usersHashMapList.entrySet();
        Iterator i = set.iterator();
        while (i.hasNext()) {
            Map.Entry me = (Map.Entry) i.next();
            UsersDTO dto = (UsersDTO) me.getValue();
            if (dto.getPermissionLevel() < Constants.ADMIN) {
                continue;
            }
            if (sdto.getSearchText() != null && sdto.getSearchText().length() > 0) {
                if (!(dto.getName().toLowerCase().contains(sdto.getSearchText().trim().toLowerCase())
                        || dto.getFullname().toLowerCase().contains(sdto.getSearchText().trim().toLowerCase())
                        || dto.getPermissionLevelName().toLowerCase().contains(sdto.getSearchText().trim().toLowerCase()))) {
                    continue;
                }
            }
            data.add(dto);
        }
        return data;
    }

    public synchronized ArrayList<UsersDTO> getUsersList(UsersDTO sdto) {
        ArrayList<UsersDTO> usersList = getUsersData(sdto);
        RingLogger.getConfigPortalLogger().debug("Data" + usersList.size());
        return usersList;
    }

    public synchronized UsersDTO getUserDTO(int id) {
        loadData();
        if (usersHashMapList.containsKey(id)) {
            return usersHashMapList.get(id);
        }
        return null;
    }

    public MyAppError addUserInfo(UsersDTO dto) {
        MyAppError error = new MyAppError();
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            stmt = db.connection.createStatement();
            String query = "SELECT user_name from admin_users where user_name='" + dto.getName() + "';";
            rs = stmt.executeQuery(query);
            if (rs.next()) {
                RingLogger.getConfigPortalLogger().error("Duplicate content : " + dto.getName());
                error.setERROR_TYPE(MyAppError.VALIDATIONERROR);
                error.setErrorMessage("Duplicate Content : " + dto.getName());
                return error;
            }

            query = "INSERT INTO admin_users SET user_name = ? ,user_password = ?, user_full_name = ?, permission_level=?;";
            ps = db.connection.prepareStatement(query);
            ps.setString(1, dto.getName());
            ps.setString(2, dto.getPassword());
            ps.setString(3, dto.getFullname());
            ps.setInt(4, dto.getPermissionLevel());
            ps.execute();
            ps.close();
        } catch (SQLException e) {
            error.setERROR_TYPE(MyAppError.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            RingLogger.getConfigPortalLogger().debug("addUserInfo --> " + e);
        } catch (Exception e) {
            error.setERROR_TYPE(MyAppError.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            RingLogger.getConfigPortalLogger().debug("addUserInfo[1] --> " + e);
        } finally {
            close();
        }
        return error;
    }

    public MyAppError deleteUser(int id) {
        MyAppError error = new MyAppError();
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            stmt = db.connection.createStatement();
            String sql = "DELETE FROM admin_users WHERE id = " + id + ";";
            int a = stmt.executeUpdate(sql);
            RingLogger.getConfigPortalLogger().debug("delete success");
        } catch (SQLException e) {
            error.setERROR_TYPE(MyAppError.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            RingLogger.getConfigPortalLogger().debug("deleteUser --> " + e);
        } catch (Exception e) {
            error.setERROR_TYPE(MyAppError.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            RingLogger.getConfigPortalLogger().debug("deleteUser[1] --> " + e);
        } finally {
            close();
        }
        return error;
    }

    public MyAppError updateUser(UsersDTO dto) {
        MyAppError error = new MyAppError();
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            stmt = db.connection.createStatement();
            String query = "UPDATE admin_users SET user_name = ?, user_password = ?, user_full_name = ?, permission_level=? WHERE id = ?;";
            ps = db.connection.prepareStatement(query);
            ps.setString(1, dto.getName());
            ps.setString(2, dto.getPassword());
            ps.setString(3, dto.getFullname());
            ps.setInt(4, dto.getPermissionLevel());
            ps.setInt(5, dto.getId());
            ps.execute();
        } catch (SQLException e) {
            error.setERROR_TYPE(MyAppError.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            RingLogger.getConfigPortalLogger().debug("updateUser[1] --> " + e);
        } catch (Exception e) {
            error.setERROR_TYPE(MyAppError.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            RingLogger.getConfigPortalLogger().debug("updateUser --> " + e);
        } finally {
            close();
        }
        return error;
    }
}
