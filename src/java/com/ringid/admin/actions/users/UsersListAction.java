/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.users;

import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.Constants;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author Rabby
 */
public class UsersListAction extends BaseAction {

    ArrayList<UsersDTO> list = new ArrayList<>();

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = SUCCESS;
        try {
            UsersForm usersForm = (UsersForm) form;
            UsersDTO usersDTO = new UsersDTO();
            UsersTaskScheduler scheduler = UsersTaskScheduler.getInstance();
            usersDTO.setSearchText(usersForm.getSearchText());
            list = scheduler.getUserList(usersDTO);

            request.getSession(true).setAttribute(Constants.DATA_ROWS, list);
            managePages(request, usersForm, list.size());
        } catch (NullPointerException e) {

        }

        return mapping.findForward(target);
    }
}
