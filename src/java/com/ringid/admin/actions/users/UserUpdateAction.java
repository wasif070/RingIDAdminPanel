/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.users;

import com.google.gson.Gson;
import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.dto.adminAuth.LoginDTO;
import com.ringid.admin.utils.Constants;
import com.ringid.admin.utils.MyAppError;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author Rabby
 */
public class UserUpdateAction extends BaseAction {

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = checkAuthentication(mapping, form, request);
        request.getSession(true).setAttribute("userErrorMessege", "");
        String activistName;
        try {
            LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
            activistName = loginDTO.getUserName();
        } catch (Exception e) {
            target = UNAUTHORIZED;
            return mapping.findForward(target);
        }
        try {
            if (target.equals(SUCCESS)) {
                UsersForm userForm = (UsersForm) form;
                UsersTaskScheduler scheduler = UsersTaskScheduler.getInstance();
                UsersDTO userDTO = scheduler.getUsersDTO(userForm);
                userDTO.setId(userForm.getId());
                RingLogger.getActivityLogger().debug("[UserUpdateAction] activistName : " + activistName + " userDTO --> " + new Gson().toJson(userDTO));
                MyAppError error = scheduler.updateUser(userDTO);
                if (error.getERROR_TYPE() > 0) {
                    if (error.getERROR_TYPE() == MyAppError.VALIDATIONERROR) {
                        RingLogger.getConfigPortalLogger().error("Duplicate error " + error.getERROR_TYPE());
                        userForm.setMessage(error.getErrorMessage());
                        request.getSession(true).setAttribute("userErrorMessege", "<span style='color:red'>Duplicate content!</span>");
                    } else {
                        RingLogger.getConfigPortalLogger().error("database error " + error.getERROR_TYPE());
                        userForm.setMessage(error.getErrorMessage());
                        request.getSession(true).setAttribute("userErrorMessege", "<span style='color:red'>Update failed!</span>");
                    }
                    target = FAILURE;
                }
            }
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error(e);
            target = FAILURE;

        }
        return mapping.findForward(target);
    }
}
