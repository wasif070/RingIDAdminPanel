/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.users;

import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author Rabby
 */
public class UserEditAction extends BaseAction {

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = checkAuthentication(mapping, form, request);
        try {
            if (target.equals(SUCCESS)) {
                int id = 0;
                UsersForm userForm = (UsersForm) form;
                if (request.getParameter("id") != null) {
                    id = Integer.parseInt(request.getParameter("id"));
                }
                UsersTaskScheduler scheduler = UsersTaskScheduler.getInstance();
                UsersDTO userDTO = scheduler.getUserInfoById(id);
                if (userDTO != null) {
                    userForm.setId(userDTO.getId());
                    userForm.setName(userDTO.getName());
                    userForm.setPassword(userDTO.getPassword());
                    userForm.setFullname(userDTO.getFullname());
                    userForm.setPermissionLevel(userDTO.getPermissionLevel());
                    target = SUCCESS;
                }
            }
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error(e);
            target = FAILURE;
        }
        return mapping.findForward(target);
    }
}
