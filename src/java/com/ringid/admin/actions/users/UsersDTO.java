/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.users;

import com.ringid.admin.BaseDTO;

/**
 *
 * @author Rabby
 */
public class UsersDTO extends BaseDTO {

    private int id;
    private String name;
    private String password;
    private String fullname;
    private int permissionLevel;
    private String permissionLevelName;

    public String getPermissionLevelName() {
        return permissionLevelName;
    }

    public void setPermissionLevelName(String permissionLevelName) {
        this.permissionLevelName = permissionLevelName;
    }

    private boolean isSearchByName = false;

    public boolean isIsSearchByName() {
        return isSearchByName;
    }

    public void setIsSearchByName(boolean isSearchByName) {
        this.isSearchByName = isSearchByName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPermissionLevel() {
        return permissionLevel;
    }

    public void setPermissionLevel(int permissionLevel) {
        this.permissionLevel = permissionLevel;
    }

    public String getName() {
        return name;
    }

    public void setName(String string) {
        name = string;
    }
}
