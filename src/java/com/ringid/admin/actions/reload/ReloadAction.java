/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.reload;

import com.ringid.admin.BaseAction;
import com.ringid.admin.repository.BreakingNewsRepository;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author Kazi Nasir Uddin Oly
 * @Date Jan 2, 2017
 */
public class ReloadAction extends BaseAction {

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = FAILURE;
        try {
            ReloadForm reloadForm = (ReloadForm) form;
            switch(reloadForm.getReloadType()){
                case "all":
                    reloadAll();
                    break;
                case "breakingNews":
                    reloadBreakingNews();
                    break;           
                default:
                    break;
            }
            request.getSession(true).setAttribute("successMessage", true);
            target = SUCCESS;
        } catch (NullPointerException e) {
            target = FAILURE;
            request.getSession(true).setAttribute("successMessage", false);
        }
        return mapping.findForward(target);
    }
    
    private void reloadAll(){
        reloadBreakingNews();
    }
    
    private void reloadBreakingNews(){
        BreakingNewsRepository.getInstance().forceLoadData();
    }
    
}
