/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ringid.admin.actions.reload;

import com.ringid.admin.BaseForm;

/**
 *
 * @author Kazi Nasir Uddin Oly 
 * @Date Jan 2, 2017
 */
public class ReloadForm extends BaseForm{
    private String reloadType;

    public String getReloadType() {
        return reloadType;
    }

    public void setReloadType(String reloadType) {
        this.reloadType = reloadType;
    }
    
}
