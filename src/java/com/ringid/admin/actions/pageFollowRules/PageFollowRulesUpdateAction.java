/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.pageFollowRules;

import com.google.gson.Gson;
import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.dto.adminAuth.LoginDTO;
import com.ringid.admin.service.PageService;
import com.ringid.admin.utils.Constants;
import com.ringid.admin.utils.Utils;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.ringid.contacts.FollowRuleDTO;
import org.ringid.utilities.AppConstants;

/**
 *
 * @author Rabby
 */
public class PageFollowRulesUpdateAction extends BaseAction {

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        String target = checkAuthentication(mapping, form, request);
        String activistName;
        try {
            LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
            activistName = loginDTO.getUserName();
        } catch (Exception e) {
            target = UNAUTHORIZED;
            return mapping.findForward(target);
        }
        try {
            if (target.equals(SUCCESS)) {
                PageFollowRulesForm pageFollowRulesForm = (PageFollowRulesForm) form;

                Set<String> countryIso = new HashSet<>(Arrays.asList(pageFollowRulesForm.getCountriesISO()));
                Set<String> cityIso = new HashSet<>(Arrays.asList(pageFollowRulesForm.getCitiesIso().split(",")));
                Set<String> languageIso = new HashSet<>(Arrays.asList(pageFollowRulesForm.getLanguages().split(",")));

                FollowRuleDTO dto = new FollowRuleDTO();
                dto.setCitiesIso(cityIso);
                dto.setCountriesISO(countryIso);
                dto.setLanguages(languageIso);
                dto.setLivesInCityISO(pageFollowRulesForm.getLivesInCityISO());
                dto.setLivesInCountryISO(pageFollowRulesForm.getLivesInCountryISO());
                dto.setUpdateTime(System.currentTimeMillis());

                RingLogger.getActivityLogger().debug("[PageFollowRulesUpdateAction] activistName : " + activistName + " authServerDTO --> " + new Gson().toJson(dto));
                int reasonCode = -1;
                if (pageFollowRulesForm.getPageType() == AppConstants.UserTypeConstants.USER_PAGE && !pageFollowRulesForm.isPageFollow()) {
                    reasonCode = PageService.getInstance().addAutoFollowForUserPage(pageFollowRulesForm.getPageId(), dto);
                } else {
                    reasonCode = PageService.getInstance().updatePageFollowRules(pageFollowRulesForm.getPageId(), pageFollowRulesForm.getPageType(), dto);
                }
                if (reasonCode > 0) {
                    target = FAILURE;
                    request.setAttribute("message", "<span style='color: red'>" + Utils.getMessageForReasonCode(reasonCode) + "</span>");
                } else {
                    request.setAttribute("message", "<span style='color:green'>Successfully updated</span>");
                    pageFollowRulesForm.setPageFollow(true);
                }
            }
        } catch (Exception e) {
            target = FAILURE;
        }
        return mapping.findForward(target);
    }
}
