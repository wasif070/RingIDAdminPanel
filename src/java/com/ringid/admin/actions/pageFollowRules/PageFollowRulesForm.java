/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.pageFollowRules;

import com.ringid.admin.BaseForm;
import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author Rabby
 */
public class PageFollowRulesForm extends BaseForm {

    private long pageId;
    private int pageType;
    private String citiesIso;
    private String[] countriesISO;
    private String languages;
    private String livesInCityISO;
    private String livesInCountryISO;
    private long updateTime;
    private String featuredTime;
    private boolean searchByRingId;
    private boolean pageFollow;
    private String pageName;

    public String getPageName() {
        return pageName;
    }

    public void setPageName(String pageName) {
        this.pageName = pageName;
    }

    public boolean isPageFollow() {
        return pageFollow;
    }

    public void setPageFollow(boolean pageFollow) {
        this.pageFollow = pageFollow;
    }

    public String getCitiesIso() {
        return citiesIso;
    }

    public void setCitiesIso(String citiesIso) {
        this.citiesIso = citiesIso;
    }

    public String getLanguages() {
        return languages;
    }

    public void setLanguages(String languages) {
        this.languages = languages;
    }

    public boolean isSearchByRingId() {
        return searchByRingId;
    }

    public void setSearchByRingId(boolean searchByRingId) {
        this.searchByRingId = searchByRingId;
    }

    public String[] getCountriesISO() {
        return countriesISO;
    }

    public void setCountriesISO(String[] countriesISO) {
        this.countriesISO = countriesISO;
    }

    public String getFeaturedTime() {
        return featuredTime;
    }

    public void setFeaturedTime(String featuredTime) {
        this.featuredTime = featuredTime;
    }

    public long getPageId() {
        return pageId;
    }

    public void setPageId(long pageId) {
        this.pageId = pageId;
    }

    public int getPageType() {
        return pageType;
    }

    public void setPageType(int pageType) {
        this.pageType = pageType;
    }

    public String getLivesInCityISO() {
        return livesInCityISO;
    }

    public void setLivesInCityISO(String livesInCityISO) {
        this.livesInCityISO = livesInCityISO;
    }

    public String getLivesInCountryISO() {
        return livesInCountryISO;
    }

    public void setLivesInCountryISO(String livesInCountryISO) {
        this.livesInCountryISO = livesInCountryISO;
    }

    public long getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(long updateTime) {
        this.updateTime = updateTime;
    }

    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        ActionErrors errors = new ActionErrors();

        return errors;
    }
}
