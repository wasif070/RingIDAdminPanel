/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.pageFollowRules;

import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.service.PageService;
import com.ringid.admin.service.UserService;
import java.util.Iterator;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.ringid.contacts.FollowRuleDTO;
import org.ringid.users.UserBasicInfoDTO;
import org.ringid.utilities.AppConstants;

/**
 *
 * @author Rabby
 */
public class PageFollowRulesEditAction extends BaseAction {

    static Logger logger = RingLogger.getConfigPortalLogger();

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        String target = checkAuthentication(mapping, form, request);
        try {
            if (target.equals(SUCCESS)) {
                PageFollowRulesForm pageFollowRulesForm = (PageFollowRulesForm) form;

                FollowRuleDTO dto = PageService.getInstance().getPageFollowRulesByPageId(pageFollowRulesForm.getPageId());
                UserBasicInfoDTO userBasicInfoDTO = UserService.getInstance().getUserBasicInfo(pageFollowRulesForm.getPageId());
                if (userBasicInfoDTO != null) {
                    pageFollowRulesForm.setPageType(userBasicInfoDTO.getUserType());
                    pageFollowRulesForm.setPageName(userBasicInfoDTO.getFirstName());
                }

                if (dto != null) {
                    StringBuilder builder = new StringBuilder();

                    if (userBasicInfoDTO != null && userBasicInfoDTO.getUserType() == AppConstants.UserTypeConstants.USER_PAGE) {
                        pageFollowRulesForm.setPageFollow(true);
                    }

                    Iterator<String> itr = dto.getCitiesIso().iterator();
                    if (itr.hasNext()) {
                        builder.append(itr.next());
                    }
                    while (itr.hasNext()) {
                        builder.append(", ").append(itr.next());
                    }
                    pageFollowRulesForm.setCitiesIso(builder.toString());
                    pageFollowRulesForm.setCountriesISO(dto.getCountriesISO().toArray(new String[dto.getCountriesISO().size()]));

                    builder = new StringBuilder();
                    itr = dto.getLanguages().iterator();
                    if (itr.hasNext()) {
                        builder.append(itr.next());
                    }
                    while (itr.hasNext()) {
                        builder.append(", ").append(itr.next());
                    }
                    pageFollowRulesForm.setLanguages(builder.toString());
                    pageFollowRulesForm.setLivesInCityISO(dto.getLivesInCityISO());
                    pageFollowRulesForm.setLivesInCountryISO(dto.getLivesInCountryISO());
                    pageFollowRulesForm.setUpdateTime(dto.getUpdateTime());
                    target = SUCCESS;
                } else {
//                    request.setAttribute("message", "<span style='color: red'> Page follow rules not found</span>");
                    pageFollowRulesForm.setCitiesIso("");
                    pageFollowRulesForm.setCountriesISO(new String[]{""});
                    pageFollowRulesForm.setLanguages("");
                    pageFollowRulesForm.setLivesInCityISO("");
                    pageFollowRulesForm.setLivesInCountryISO("");
                    pageFollowRulesForm.setUpdateTime(0L);
                    if (userBasicInfoDTO != null && userBasicInfoDTO.getUserType() == AppConstants.UserTypeConstants.USER_PAGE) {
                        pageFollowRulesForm.setPageFollow(false);
                    }
                }
            }
        } catch (Exception e) {
            logger.error("PageFollowRulesEditAction Exception: " + e);
            target = FAILURE;
        }
        return mapping.findForward(target);
    }
}
