/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.userFeatureManagement.userRole.action;

import com.google.gson.Gson;
import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.dto.adminAuth.LoginDTO;
import com.ringid.admin.actions.userFeatureManagement.UserFeatureTaskScheduler;
import com.ringid.admin.dto.adminAuth.UserRoleDTO;
import com.ringid.admin.actions.userFeatureManagement.userRole.UserRoleLoader;
import com.ringid.admin.actions.userFeatureManagement.userRole.form.UserRoleForm;
import com.ringid.admin.utils.Constants;
import com.ringid.admin.utils.MyAppError;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author Rabby
 */
public class UserRoleUpdateAction extends BaseAction {

    static Logger logger = RingLogger.getConfigPortalLogger();

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = checkAuthentication(mapping, form, request);
        request.getSession(true).setAttribute("errorMessege", "");
        String activistName;
        try {
            LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
            activistName = loginDTO.getUserName();
        } catch (Exception e) {
            target = UNAUTHORIZED;
            return mapping.findForward(target);
        }
        try {
            if (target.equals(SUCCESS)) {
                UserRoleForm userRoleForm = (UserRoleForm) form;
                UserFeatureTaskScheduler scheduler = UserFeatureTaskScheduler.getInstance();
                UserRoleDTO userRoleDTO = new UserRoleDTO();
                userRoleDTO.setId(userRoleForm.getId());
                userRoleDTO.setName(userRoleForm.getName());

                RingLogger.getActivityLogger().debug("[UserRoleUpdateAction] activistName : " + activistName + " userRoleDTO --> " + new Gson().toJson(userRoleDTO));
                MyAppError error = scheduler.updateUserRole(userRoleDTO);
                if (error.getERROR_TYPE() > 0) {
                    logger.error("SettingUpdateAction Other error " + error.getERROR_TYPE());
                    userRoleForm.setMessage(error.getErrorMessage());
                    request.getSession(true).setAttribute("errorMessege", "<span style='color:red'>Update failed!</span>");
                    target = FAILURE;
                } else {
                    UserRoleLoader.getInstance().forceReload();
                }
            }
        } catch (Exception e) {
            logger.error("SettingUpdateAction Exception: " + e);
            target = FAILURE;
            request.getSession(true).setAttribute("errorMessege", "<span style='color:red'>Update failed!</span>");
        }
        return mapping.findForward(target);
    }
}
