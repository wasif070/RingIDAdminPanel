/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.userFeatureManagement.userRole.action;

import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.actions.userFeatureManagement.UserFeatureTaskScheduler;
import com.ringid.admin.dto.adminAuth.UserRoleDTO;
import com.ringid.admin.actions.userFeatureManagement.userRole.form.UserRoleForm;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author Rabby
 */
public class UserRoleEditAction extends BaseAction {

    private static final Logger logger = RingLogger.getConfigPortalLogger();

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = checkAuthentication(mapping, form, request);
        try {
            if (target.equals(SUCCESS)) {
                int id = 0;
                UserRoleForm userRoleForm = (UserRoleForm) form;
                if (request.getParameter("id") != null) {
                    id = Integer.parseInt(request.getParameter("id"));
                }
                UserFeatureTaskScheduler scheduler = UserFeatureTaskScheduler.getInstance();
                UserRoleDTO userRoleDTO = scheduler.getUserRoleById(id);
                if (userRoleDTO != null) {
                    userRoleForm.setId(userRoleDTO.getId());
                    userRoleForm.setName(userRoleDTO.getName());
                    target = SUCCESS;
                }
            }
        } catch (Exception e) {
            logger.error("UserRoleEditAction Exception: " + e);
            target = FAILURE;
        }
        return mapping.findForward(target);
    }
}
