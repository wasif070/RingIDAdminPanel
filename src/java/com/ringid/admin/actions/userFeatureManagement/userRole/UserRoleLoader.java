/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.userFeatureManagement.userRole;

import com.ringid.admin.dto.adminAuth.UserRoleDTO;
import com.ringid.admin.BaseDAO;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.utils.Constants;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Rabby
 */
public class UserRoleLoader extends BaseDAO {

    private final static long LOADING_INTERVAL = 60 * 60 * 1000;
    private long loadingTime = 0;
    private HashMap<Integer, UserRoleDTO> userRoleHashMapList;
    private static UserRoleLoader userRoleLoader = null;
    static Logger logger = RingLogger.getConfigPortalLogger();

    public static UserRoleLoader getInstance() {
        if (userRoleLoader == null) {
            createLoader();
        }
        return userRoleLoader;
    }

    private synchronized static void createLoader() {
        if (userRoleLoader == null) {
            userRoleLoader = new UserRoleLoader();
        }
    }

    private void forceLoadData() {
        userRoleHashMapList = new HashMap<>();
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            stmt = db.connection.createStatement();
            String sql = "SELECT id, name FROM users_role;";
            rs = stmt.executeQuery(sql);
            while (rs.next()) {
                UserRoleDTO dto = new UserRoleDTO();
                dto.setId(rs.getInt("id"));
                dto.setName(rs.getString("name"));
                userRoleHashMapList.put(dto.getId(), dto);
            }
            logger.debug(sql + "::" + userRoleHashMapList.size());
        } catch (Exception e) {
            logger.debug("UserRole List Exception...", e);
        } finally {
            close();
        }
    }

    public synchronized void forceReload() {
        loadingTime = System.currentTimeMillis();
        forceLoadData();
    }

    private void checkForReload() {
        long currentTime = System.currentTimeMillis();
        if (currentTime - loadingTime > LOADING_INTERVAL) {
            loadingTime = currentTime;
            forceLoadData();
        }
    }

    public synchronized ArrayList<UserRoleDTO> getUserRoleList(UserRoleDTO sdto) {
        checkForReload();
        ArrayList<UserRoleDTO> data = new ArrayList<>();
        Set set = userRoleHashMapList.entrySet();
        Iterator i = set.iterator();
        while (i.hasNext()) {
            Map.Entry me = (Map.Entry) i.next();
            UserRoleDTO dto = (UserRoleDTO) me.getValue();
            if (dto.getId() == Constants.SUPER_ADMIN) {
                continue;
            }
            if (sdto.getSearchText() != null && sdto.getSearchText().length() > 0) {
                if (!(dto.getName().toLowerCase().contains(sdto.getSearchText().trim().toLowerCase()))) {
                    continue;
                }
            }
            data.add(dto);
        }
        logger.debug("getSmsServerList Size: " + data.size());
        return data;
    }

    public synchronized UserRoleDTO getUserRoleById(int id) {
        checkForReload();
        if (userRoleHashMapList.containsKey(id)) {
            return userRoleHashMapList.get(id);
        }
        return null;
    }

    public int geFirstRoleId() {
        checkForReload();
        for (Map.Entry<Integer, UserRoleDTO> entry : userRoleHashMapList.entrySet()) {
            if (entry.getKey() != Constants.SUPER_ADMIN) {
                return entry.getKey();
            }
        }
        return 0;
    }
}
