/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.userFeatureManagement.userRole.action;

import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.actions.userFeatureManagement.UserFeatureTaskScheduler;
import com.ringid.admin.dto.adminAuth.UserRoleDTO;
import com.ringid.admin.actions.userFeatureManagement.userRole.UserRoleLoader;
import com.ringid.admin.actions.userFeatureManagement.userRole.form.UserRoleForm;
import com.ringid.admin.utils.Constants;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author Rabby
 */
public class UserRoleListAction extends BaseAction {

    Logger logger = RingLogger.getConfigPortalLogger();
    ArrayList<UserRoleDTO> list = new ArrayList<>();

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = SUCCESS;
        try {
            UserRoleForm userRoleForm = (UserRoleForm) form;
            UserRoleDTO userRoleDTO = new UserRoleDTO();
            UserFeatureTaskScheduler scheduler = UserFeatureTaskScheduler.getInstance();

            if (request.getParameter("ForceReload") != null && "1".equals(request.getParameter("ForceReload"))) {
                UserRoleLoader.getInstance().forceReload();
            }
            userRoleDTO.setSearchText(userRoleForm.getSearchText());
            list = scheduler.getUserRoleList(userRoleDTO);
            request.getSession(true).setAttribute(Constants.DATA_ROWS, list);
            managePages(request, userRoleForm, list.size());
            request.getSession(true).setAttribute("search", userRoleDTO.getName());
        } catch (NullPointerException e) {
            logger.error("UserRoleListAction Execption : " + e);
            target = FAILURE;
        }
        return mapping.findForward(target);
    }
}
