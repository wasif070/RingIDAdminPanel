/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.userFeatureManagement.featureRoleManage.action;

import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.actions.userFeatureManagement.UserFeatureTaskScheduler;
import com.ringid.admin.dto.adminAuth.FeatureDTO;
import com.ringid.admin.actions.userFeatureManagement.featureRoleManage.FeatureRoleUtils;
import com.ringid.admin.actions.userFeatureManagement.featureRoleManage.form.FeatureRoleForm;
import com.ringid.admin.utils.MyAppError;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Rabby
 */
public class FeatureRoleAction extends BaseAction {

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {

        String target = checkAuthentication(mapping, form, request);
        try {
            FeatureRoleForm featureRoleForm = (FeatureRoleForm) form;

            JSONArray jSONArray = null;
            if (request.getParameter("featureIds") != null) {
                String featureids = request.getParameter("featureIds");
                jSONArray = new JSONArray(featureids);
                RingLogger.getConfigPortalLogger().debug("Feature List to update jsonArray : " + jSONArray);
            }
            if (featureRoleForm.getRoleId() == 0) {
                featureRoleForm.setRoleId(UserFeatureTaskScheduler.getInstance().getFirstRoleId());
            }

            if (jSONArray != null && request.getParameter("operation") != null) {
                if (request.getParameter("operation").equals("update_feature")) {
                    updateFeaturePermissionByRole(jSONArray, featureRoleForm.getRoleId());
                }
            }

            ArrayList<FeatureDTO> rootFeatureList = UserFeatureTaskScheduler.getInstance().getRootFeatureList();
            ArrayList<FeatureDTO> allFeatureList = UserFeatureTaskScheduler.getInstance().getFeatureListByRoleId(featureRoleForm.getRoleId());
            String treeData = getTreedata(rootFeatureList, allFeatureList);

            request.setAttribute("treeData", treeData);
            request.setAttribute("roleId", featureRoleForm.getRoleId());
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error(e);
            target = FAILURE;
        }
        return mapping.findForward(target);
    }

    private String getTreedata(ArrayList<FeatureDTO> rootfeatureList, ArrayList<FeatureDTO> allFeatureList) {
        JSONArray jSONArray = new JSONArray();
        try {
            for (FeatureDTO dto : rootfeatureList) {
                boolean visited[] = new boolean[100];
                JSONObject jSONObject = FeatureRoleUtils.getInstance().DFSUtils(dto, visited, allFeatureList);
                jSONArray.put(jSONObject);
            }
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error(e);
        }
        return jSONArray.toString();
    }

    private void updateFeaturePermissionByRole(JSONArray jSONArray, int roleId) {
        MyAppError error;
        HashMap<Integer, FeatureDTO> featureListHashMap = UserFeatureTaskScheduler.getInstance().getFeatureListHashMap();
        ArrayList<FeatureDTO> listToBeUpdate = getFeatureListToBeUpdate(jSONArray, featureListHashMap);
        error = UserFeatureTaskScheduler.getInstance().updateFeaturePermissionForRole(listToBeUpdate, roleId);
        if (error.getERROR_TYPE() > MyAppError.NOERROR) {
            RingLogger.getConfigPortalLogger().debug("error in update FeatureRoleAction --> " + error.getErrorMessage());
        }
    }

    private ArrayList<FeatureDTO> getFeatureListToBeUpdate(JSONArray jArray, HashMap<Integer, FeatureDTO> featureListHashMap) {
        ArrayList<FeatureDTO> list = new ArrayList<>();

        FeatureRoleUtils.getInstance().resetFeatureListPermissionLevel(featureListHashMap);
        FeatureRoleUtils.getInstance().setFeatureListPermissionLevel(jArray, featureListHashMap);
        for (Map.Entry<Integer, FeatureDTO> entry : featureListHashMap.entrySet()) {
            FeatureDTO dto = entry.getValue();
            list.add(dto);
        }
        return list;
    }

}
