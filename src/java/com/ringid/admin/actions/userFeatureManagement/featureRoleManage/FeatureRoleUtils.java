/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.userFeatureManagement.featureRoleManage;

import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.dto.adminAuth.FeatureDTO;
import com.ringid.admin.utils.Constants;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Rabby
 */
public class FeatureRoleUtils {

    private static final AtomicInteger uniqueId = new AtomicInteger();

    private FeatureRoleUtils() {

    }

    private static class FeatureRoleUtilsHolder {

        private static final FeatureRoleUtils INSTANCE = new FeatureRoleUtils();
    }

    public static FeatureRoleUtils getInstance() {
        return FeatureRoleUtilsHolder.INSTANCE;
    }

    public JSONObject DFSUtils(FeatureDTO sdto, boolean visited[], ArrayList<FeatureDTO> allFeatureList) throws JSONException {
        visited[sdto.getId()] = true;
        JSONObject result = new JSONObject();
        JSONArray jArray = new JSONArray();
        result.put("text", sdto.getFeatureName());
        result.put("id", sdto.getId());
        result.put("state", getState(sdto));
        for (FeatureDTO dto : allFeatureList) {
            if (dto.getParentId() == sdto.getId()) {
                if (!visited[dto.getId()]) {
                    JSONObject jsonObject = DFSUtils(dto, visited, allFeatureList);
                    jArray.put(jsonObject);
                }
            }
        }
        getOperationType(sdto, jArray);
        if (jArray.length() > 0) {
            result.put("children", jArray);
        }
        return result;
    }

    private JSONObject getState(FeatureDTO featureDTO) throws JSONException {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("selected", featureDTO.getOperationType() == featureDTO.getPermissionLevel());
        return jSONObject;
    }

    private void getOperationType(FeatureDTO sdto, JSONArray jarray) throws JSONException {
        if (sdto.getOperationType() > 1) {
            if ((sdto.getOperationType() & 1) == 1) {
                JSONObject jSONObject1 = new JSONObject();
                jSONObject1.put("text", Constants.OPERATION_VIEW);
                jSONObject1.put("id", getUniqueId());
                JSONObject jo2 = new JSONObject();
                jo2.put("selected", (sdto.getPermissionLevel() & 1) == 1);
                jSONObject1.put("state", jo2);
                jarray.put(jSONObject1);
            }
            if (((sdto.getOperationType() >> 1) & 1) == 1) {
                JSONObject jSONObject1 = new JSONObject();
                jSONObject1.put("text", Constants.OPERATION_ADD);
                jSONObject1.put("id", getUniqueId());
                JSONObject jo2 = new JSONObject();
                jo2.put("selected", ((sdto.getPermissionLevel() >> 1) & 1) == 1);
                jSONObject1.put("state", jo2);
                jarray.put(jSONObject1);
            }
            if (((sdto.getOperationType() >> 2) & 1) == 1) {
                JSONObject jSONObject1 = new JSONObject();
                jSONObject1.put("text", Constants.OPERATION_MODIFY);
                jSONObject1.put("id", getUniqueId());
                JSONObject jo2 = new JSONObject();
                jo2.put("selected", ((sdto.getPermissionLevel() >> 2) & 1) == 1);
                jSONObject1.put("state", jo2);
                jarray.put(jSONObject1);
            }
            if (((sdto.getOperationType() >> 3) & 1) == 1) {
                JSONObject jSONObject1 = new JSONObject();
                jSONObject1.put("text", Constants.OPERATION_DELETE);
                jSONObject1.put("id", getUniqueId());
                JSONObject jo2 = new JSONObject();
                jo2.put("selected", ((sdto.getPermissionLevel() >> 3) & 1) == 1);
                jSONObject1.put("state", jo2);
                jarray.put(jSONObject1);
            }
        }
    }

    public void setFeatureListPermissionLevel(JSONArray jArray, HashMap<Integer, FeatureDTO> featureListHashMap) {
        boolean visit[] = new boolean[2000];
        try {
            for (int i = 0; i < jArray.length(); i++) {
                JSONObject j = jArray.getJSONObject(i);
                int id = j.getInt("id");
                String name = j.getString("text");
                int parentId = j.getInt("parent");
//                int permissionLevel = j.getBoolean("permissionLevel") ? 1 : 0;

                if (id < 10000) {
                    featureListHashMap.get(id).setPermissionLevel(featureListHashMap.get(id).getOperationType());
                    visit[id] = true;
                } else {
                    int pl = featureListHashMap.get(parentId).getPermissionLevel();
                    int operationType = featureListHashMap.get(parentId).getOperationType();
                    if (pl < operationType) {
                        switch (name) {
                            case Constants.OPERATION_VIEW:
                                pl += 1;
                                break;
                            case Constants.OPERATION_ADD:
                                pl += 2;
                                break;
                            case Constants.OPERATION_MODIFY:
                                pl += 4;
                                break;
                            case Constants.OPERATION_DELETE:
                                pl += 8;
                                break;
                            default:
                                break;
                        }
                        featureListHashMap.get(parentId).setPermissionLevel(pl);
                        visit[parentId] = true;
                    }
                }
            }
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error(e);
        }
    }

    public void resetFeatureListPermissionLevel(HashMap<Integer, FeatureDTO> featureListHashMap) {
        for (Map.Entry<Integer, FeatureDTO> entry : featureListHashMap.entrySet()) {
            entry.getValue().setPermissionLevel(0);
        }
    }

    private int getUniqueId() {
        return uniqueId.incrementAndGet() + 10000;
    }

}
