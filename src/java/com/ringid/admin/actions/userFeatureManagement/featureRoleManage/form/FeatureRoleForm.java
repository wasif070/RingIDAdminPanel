/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.userFeatureManagement.featureRoleManage.form;

import com.ringid.admin.BaseForm;
import com.ringid.admin.dto.adminAuth.FeatureDTO;
import java.util.List;
import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

/**
 *
 * @author Rabby
 */
public class FeatureRoleForm extends BaseForm {

    private int roleId;
    private String roleName;
    List<FeatureDTO> featureList;

    public int getRoleId() {
        return roleId;
    }

    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public List<FeatureDTO> getFeatureList() {
        return featureList;
    }

    public void setFeatureList(List<FeatureDTO> featureList) {
        this.featureList = featureList;
    }

    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        ActionErrors errors = new ActionErrors();
        return errors;
    }
}
