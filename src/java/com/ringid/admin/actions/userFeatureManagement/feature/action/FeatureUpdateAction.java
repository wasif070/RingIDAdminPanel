/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.userFeatureManagement.feature.action;

import com.google.gson.Gson;
import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.dto.adminAuth.LoginDTO;
import com.ringid.admin.actions.userFeatureManagement.UserFeatureTaskScheduler;
import com.ringid.admin.dto.adminAuth.FeatureDTO;
import com.ringid.admin.actions.userFeatureManagement.feature.FeatureLoader;
import com.ringid.admin.actions.userFeatureManagement.feature.form.FeatureForm;
import com.ringid.admin.utils.Constants;
import com.ringid.admin.utils.MyAppError;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author Rabby
 */
public class FeatureUpdateAction extends BaseAction {

    Logger logger = RingLogger.getConfigPortalLogger();

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = checkAuthentication(mapping, form, request);
        request.getSession(true).setAttribute("errorMessage", "");
        String activistName;
        try {
            LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
            activistName = loginDTO.getUserName();
        } catch (Exception e) {
            target = UNAUTHORIZED;
            return mapping.findForward(target);
        }
        try {
            if (target.equals(SUCCESS)) {
                FeatureForm featureForm = (FeatureForm) form;
                FeatureDTO featureDTO = new FeatureDTO();
                featureDTO.setId(featureForm.getId());
                featureDTO.setFeatureName(featureForm.getFeatureName());
                featureDTO.setFeatureCode(featureForm.getFeatureCode());
                featureDTO.setParentId(featureForm.getParentId());
                featureDTO.setProjectName(featureForm.getProjectName());
                featureDTO.setOperationType(UserFeatureTaskScheduler.getInstance().getOperationType(featureForm));

                RingLogger.getActivityLogger().debug("[FeatureUpdateAction] activistName : " + activistName + " featureDTO --> " + new Gson().toJson(featureDTO));
                MyAppError error = UserFeatureTaskScheduler.getInstance().updateFeature(featureDTO);
                if (error.getERROR_TYPE() > MyAppError.NOERROR) {
                    logger.error("FeatureUpdatAction Error : " + error.getERROR_TYPE());
                    featureForm.setMessage(error.getErrorMessage());
                    request.getSession(true).setAttribute("errorMessage", "<span style='color:red'>Update failed!</span>");
                    target = FAILURE;
                } else {
                    FeatureLoader.getInstance().forceReload();
                }
            }
        } catch (Exception e) {
            logger.error("FeatureUpdateAction Exception --> " + e);
            target = FAILURE;
            request.getSession(true).setAttribute("errorMessage", "<span style='color:red'>Update failed!</span>");
        }
        return mapping.findForward(target);
    }
}
