/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.userFeatureManagement.feature.action;

import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.actions.userFeatureManagement.UserFeatureTaskScheduler;
import com.ringid.admin.dto.adminAuth.FeatureDTO;
import com.ringid.admin.actions.userFeatureManagement.feature.FeatureLoader;
import com.ringid.admin.actions.userFeatureManagement.feature.form.FeatureForm;
import com.ringid.admin.utils.Constants;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author Rabby
 */
public class FeatureListAction extends BaseAction {

    Logger logger = RingLogger.getConfigPortalLogger();
    ArrayList<FeatureDTO> list = new ArrayList<>();

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = SUCCESS;
        try {
            FeatureForm featureForm = (FeatureForm) form;
            FeatureDTO featureDTO = new FeatureDTO();
            UserFeatureTaskScheduler scheduler = UserFeatureTaskScheduler.getInstance();

            if (request.getParameter("ForceReload") != null && "1".equals(request.getParameter("ForceReload"))) {
                FeatureLoader.getInstance().forceReload();
            }
            featureDTO.setSearchText(featureForm.getSearchText());
            list = scheduler.getFeatureList(featureDTO);
            request.getSession(true).setAttribute(Constants.DATA_ROWS, list);
            managePages(request, featureForm, list.size());
            request.getSession(true).setAttribute("search", featureDTO.getFeatureName());
        } catch (Exception e) {
            logger.error("FeatureListAction Exception : " + e);
            target = FAILURE;
        }
        return mapping.findForward(target);
    }
}
