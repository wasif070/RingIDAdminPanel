/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.userFeatureManagement.feature.action;

import com.google.gson.Gson;
import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.dto.adminAuth.LoginDTO;
import com.ringid.admin.actions.userFeatureManagement.UserFeatureTaskScheduler;
import com.ringid.admin.dto.adminAuth.FeatureDTO;
import com.ringid.admin.actions.userFeatureManagement.feature.FeatureLoader;
import com.ringid.admin.actions.userFeatureManagement.feature.form.FeatureForm;
import com.ringid.admin.utils.Constants;
import com.ringid.admin.utils.MyAppError;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author Rabby
 */
public class FeatureAction extends BaseAction {

    Logger logger = RingLogger.getConfigPortalLogger();

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = checkAuthentication(mapping, form, request);
        request.getSession(true).setAttribute("errorMessage", "");
        String activistName;
        try {
            LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
            activistName = loginDTO.getUserName();
        } catch (Exception e) {
            target = UNAUTHORIZED;
            return mapping.findForward(target);
        }
        try {
            FeatureForm featureForm = (FeatureForm) form;
            FeatureDTO dto = new FeatureDTO();
            dto.setFeatureName(featureForm.getFeatureName());
            dto.setFeatureCode(featureForm.getFeatureCode());
            dto.setParentId(featureForm.getParentId());
            dto.setProjectName(featureForm.getProjectName());
            dto.setOperationType(UserFeatureTaskScheduler.getInstance().getOperationType(featureForm));
            RingLogger.getActivityLogger().debug("[add FeatureAction] activistName : " + activistName + " featureDTO --> " + new Gson().toJson(dto));
            MyAppError error = UserFeatureTaskScheduler.getInstance().addFeature(dto);
            if (error.getERROR_TYPE() > MyAppError.NOERROR) {
                if (error.getERROR_TYPE() == MyAppError.OTHERERROR) {
                    logger.error("FeatureActio other error --> " + error.getERROR_TYPE());
                    featureForm.setMessage(error.getErrorMessage());
                    request.getSession(true).setAttribute("errorMessage", "<span style='color:red'>Insertion failed!</span>");
                }
                target = FAILURE;
            } else {
                FeatureLoader.getInstance().forceReload();
            }
        } catch (Exception e) {
            request.getSession(true).setAttribute("errormessage", "<span style='color:red'>Insertion failed!</span>");
            target = FAILURE;
        }
        return mapping.findForward(target);
    }
}
