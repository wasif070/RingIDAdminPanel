/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.userFeatureManagement.feature.action;

import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.actions.userFeatureManagement.UserFeatureTaskScheduler;
import com.ringid.admin.dto.adminAuth.FeatureDTO;
import com.ringid.admin.actions.userFeatureManagement.feature.form.FeatureForm;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author Rabby
 */
public class FeatureEditAction extends BaseAction {

    private static final Logger logger = RingLogger.getConfigPortalLogger();

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = checkAuthentication(mapping, form, request);
        try {
            if (target.equals(SUCCESS)) {
                FeatureForm featureForm = (FeatureForm) form;
                int id = 0;
                if (request.getParameter("id") != null) {
                    id = Integer.parseInt(request.getParameter("id"));
                }
                UserFeatureTaskScheduler scheduler = UserFeatureTaskScheduler.getInstance();
                FeatureDTO featureDTO = scheduler.getFeatureById(id);
                if (featureDTO != null) {
                    featureForm.setFeatureName(featureDTO.getFeatureName());
                    featureForm.setFeatureCode(featureDTO.getFeatureCode());
                    featureForm.setParentId(featureDTO.getParentId());
                    featureForm.setProjectName(featureDTO.getProjectName());
                    featureForm.setView(true);
                    if (((featureDTO.getOperationType() >> 1) & 1) == 1) {
                        featureForm.setAdd(true);
                    }
                    if (((featureDTO.getOperationType() >> 2) & 1) == 1) {
                        featureForm.setModify(true);
                    }
                    if (((featureDTO.getOperationType() >> 3) & 1) == 1) {
                        featureForm.setDelete(true);
                    }
                    target = SUCCESS;
                }
            }
        } catch (Exception e) {
            logger.error("FeatureEditAction Exception : " + e);
            target = FAILURE;
        }
        return mapping.findForward(target);
    }
}
