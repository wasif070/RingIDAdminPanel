/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.userFeatureManagement.feature;

import com.ringid.admin.dto.adminAuth.FeatureDTO;
import com.ringid.admin.BaseDAO;
import com.ringid.admin.utils.log.RingLogger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Rabby
 */
public class FeatureLoader extends BaseDAO {

    private final static long LOADING_INTERVAL = 60 * 60 * 1000;
    private long loadingTime = 0;
    private HashMap<Integer, FeatureDTO> featureHashMapList;
    static Logger logger = RingLogger.getConfigPortalLogger();

    private FeatureLoader() {

    }

    private static class FeatureLoaderHolder {

        private static final FeatureLoader INSTANCE = new FeatureLoader();
    }

    public static FeatureLoader getInstance() {
        return FeatureLoaderHolder.INSTANCE;
    }

    private void forceLoadData() {
        featureHashMapList = new HashMap<>();
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            stmt = db.connection.createStatement();
            String sql = "SELECT id, feature_name, feature_code, parent_id, project_name, operation_type FROM features_info WHERE project_name = 'configportal' ORDER BY id;";
            rs = stmt.executeQuery(sql);
            while (rs.next()) {
                FeatureDTO featureDTO = new FeatureDTO();
                featureDTO.setId(rs.getInt("id"));
                featureDTO.setFeatureName(rs.getString("feature_name"));
                featureDTO.setFeatureCode(rs.getString("feature_code"));
                featureDTO.setParentId(rs.getInt("parent_id"));
                featureDTO.setProjectName(rs.getString("project_name"));
                featureDTO.setOperationType(rs.getInt("operation_type"));
                featureHashMapList.put(featureDTO.getId(), featureDTO);
            }
            logger.debug("featureHashMapList --> " + featureHashMapList.size());
        } catch (Exception e) {
            logger.debug("FeatureLoader List exception --> ", e);
        } finally {
            close();
        }
    }

    public void forceReload() {
        loadingTime = System.currentTimeMillis();
        forceLoadData();
    }

    private void checkForReload() {
        long currentTime = System.currentTimeMillis();
        if (currentTime - loadingTime > LOADING_INTERVAL) {
            loadingTime = currentTime;
            forceLoadData();
        }
    }

    public ArrayList<FeatureDTO> getFeatureList(FeatureDTO sdto) {
        checkForReload();
        ArrayList<FeatureDTO> data = new ArrayList<>();
        for (Map.Entry<Integer, FeatureDTO> entry : featureHashMapList.entrySet()) {
            FeatureDTO dto = entry.getValue();
            dto.setParentName(getFeatureById(dto.getParentId()) != null ? getFeatureById(dto.getParentId()).getFeatureName() : "No parent");
            if (sdto.getSearchText() != null && sdto.getSearchText().length() > 0) {
                if (!(dto.getFeatureName().toLowerCase().contains(sdto.getSearchText().trim().toLowerCase())
                        || dto.getParentName().toLowerCase().contains(sdto.getSearchText().trim().toLowerCase())
                        || (dto.getFeatureCode() != null && dto.getFeatureCode().toLowerCase().contains(sdto.getSearchText().trim().toLowerCase())))) {
                    continue;
                }
            }
            data.add(dto);
        }
        return data;
    }

    public ArrayList<FeatureDTO> getFeatureListByParentId(int parentId) {
        checkForReload();
        ArrayList<FeatureDTO> data = new ArrayList<>();
        for (Map.Entry<Integer, FeatureDTO> entry : featureHashMapList.entrySet()) {
            FeatureDTO dto = entry.getValue();
            dto.setParentName(getFeatureById(dto.getParentId()) != null ? getFeatureById(dto.getParentId()).getFeatureName() : "No parent");
            if (dto.getParentId() != parentId) {
                continue;
            }
            data.add(dto);
        }
        return data;
    }

    public ArrayList<FeatureDTO> getRootFeatureList() {
        checkForReload();
        ArrayList<FeatureDTO> data = new ArrayList<>();
        for (Map.Entry<Integer, FeatureDTO> entry : featureHashMapList.entrySet()) {
            FeatureDTO dto = entry.getValue();
            dto.setParentName(getFeatureById(dto.getParentId()) != null ? getFeatureById(dto.getParentId()).getFeatureName() : "No parent");
            if (dto.getParentId() != 0) {
                continue;
            }
            data.add(dto);
        }
        return data;
    }

    public FeatureDTO getFeatureById(int featureId) {
        checkForReload();
        if (featureHashMapList.containsKey(featureId)) {
            return featureHashMapList.get(featureId);
        }
        return null;
    }

    public HashMap<Integer, FeatureDTO> getFeatureListHashMap() {
        checkForReload();
        return featureHashMapList;
    }

}
