/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.userFeatureManagement;

import com.ringid.admin.dto.adminAuth.UserFeatureMappingDTO;
import com.ringid.admin.dao.adminAuth.FeatureDAO;
import com.ringid.admin.dto.adminAuth.FeatureDTO;
import com.ringid.admin.actions.userFeatureManagement.feature.FeatureLoader;
import com.ringid.admin.actions.userFeatureManagement.feature.form.FeatureForm;
import com.ringid.admin.dao.adminAuth.FeatureRoleDAO;
import com.ringid.admin.dao.adminAuth.UserRoleDAO;
import com.ringid.admin.dto.adminAuth.UserRoleDTO;
import com.ringid.admin.actions.userFeatureManagement.userRole.UserRoleLoader;
import com.ringid.admin.utils.Constants;
import com.ringid.admin.utils.MyAppError;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author Rabby
 */
public class UserFeatureTaskScheduler {

    private UserFeatureTaskScheduler() {

    }

    private static class UserFeatureTaskSchedulerHolder {

        private static final UserFeatureTaskScheduler INSTANCE = new UserFeatureTaskScheduler();
    }

    public static UserFeatureTaskScheduler getInstance() {
        return UserFeatureTaskSchedulerHolder.INSTANCE;
    }

    /*
     * userRole
     */
    public ArrayList<UserRoleDTO> getUserRoleList(UserRoleDTO dto) {
        return UserRoleLoader.getInstance().getUserRoleList(dto);
    }

    public UserRoleDTO getUserRoleById(int id) {
        UserRoleDAO userRoleDAO = new UserRoleDAO();
        return userRoleDAO.getUserRoleById(id);
    }

    public MyAppError addUserRole(UserRoleDTO sdto) {
        UserRoleDAO userRoleDAO = new UserRoleDAO();
        return userRoleDAO.addUserRole(sdto);
    }

    public MyAppError updateUserRole(UserRoleDTO sdto) {
        UserRoleDAO userRoleDAO = new UserRoleDAO();
        return userRoleDAO.updateUserRole(sdto);
    }

    public MyAppError deleteUserRole(int id) {
        UserRoleDAO userRoleDAO = new UserRoleDAO();
        return userRoleDAO.deleteUserRole(id);
    }

    public int getFirstRoleId() {
        return UserRoleLoader.getInstance().geFirstRoleId();
    }

    /*
     * feature
     */
    public ArrayList<FeatureDTO> getFeatureList(FeatureDTO dto) {
        return FeatureLoader.getInstance().getFeatureList(dto);
    }

    public ArrayList<FeatureDTO> getFeatureListByParentId(int parentId) {
        return FeatureLoader.getInstance().getFeatureListByParentId(parentId);
    }

    public ArrayList<FeatureDTO> getRootFeatureList() {
        return FeatureLoader.getInstance().getRootFeatureList();
    }

    public HashMap<Integer, FeatureDTO> getFeatureListHashMap() {
        return FeatureLoader.getInstance().getFeatureListHashMap();
    }

    public FeatureDTO getFeatureById(int id) {
        FeatureDAO featureDAO = new FeatureDAO();
        return featureDAO.getFeatureById(id);
    }

    public MyAppError addFeature(FeatureDTO dto) {
        FeatureDAO featureDAO = new FeatureDAO();
        return featureDAO.addFeature(dto);
    }

    public MyAppError updateFeature(FeatureDTO dto) {
        FeatureDAO featureDAO = new FeatureDAO();
        return featureDAO.updateFeature(dto);
    }

    public MyAppError deleteFeature(int featureId) {
        FeatureDAO featureDAO = new FeatureDAO();
        return featureDAO.deleteFeature(featureId);
    }

    public int getOperationType(FeatureForm featureForm) {
        int operationType = 0;
        if (featureForm.isView()) {
            operationType += 1;
        }
        if (featureForm.isAdd()) {
            operationType += 2;
        }
        if (featureForm.isModify()) {
            operationType += 4;
        }
        if (featureForm.isDelete()) {
            operationType += 8;
        }
        return operationType;
    }

    /*
     * Feature_role_mapping
     */
    public ArrayList<FeatureDTO> getFeatureListByRoleId(int roleId) {
        ArrayList<FeatureDTO> list = getFeatureList(new FeatureDTO());
        for (FeatureDTO dto : list) {
            int permissionLevel = new FeatureRoleDAO().getPermissionLevel(dto.getId(), roleId);
            dto.setPermissionLevel(permissionLevel);
            dto.setHasPermission(permissionLevel > 0);
        }
        return list;
    }

    public MyAppError updateFeaturePermissionForRole(ArrayList<FeatureDTO> featureList, int roleId) {
        FeatureRoleDAO featureRoleDAO = new FeatureRoleDAO();
        return featureRoleDAO.updateFeaturePermissionForRole(featureList, roleId);
    }

    public boolean hasPermissionForFeature(String featureName, int role_id, String operationType) {
        if (role_id == 1) {
            return true;
        }
        FeatureDAO featureDAO = new FeatureDAO();
        FeatureRoleDAO featureRoleDAO = new FeatureRoleDAO();
        FeatureDTO featureDTO = featureDAO.getFeatureByName(featureName);
        if (featureDTO != null) {
            int permissionLevel = featureRoleDAO.getPermissionLevel(featureDTO.getId(), role_id);
            if (operationType != null && operationType.length() > 0) {
                switch (operationType) {
                    case Constants.OPERATION_ADD:
                        if (((permissionLevel >> 1) & 1) == 1) {
                            return true;
                        }
                        return false;
                    case Constants.OPERATION_MODIFY:
                        if (((permissionLevel >> 2) & 1) == 1) {
                            return true;
                        }
                        return false;
                    case Constants.OPERATION_DELETE:
                        if (((permissionLevel >> 3) & 1) == 1) {
                            return true;
                        }
                        return false;
                    default:
                        return false;
                }
            }
            boolean hasAnyChildPermission = featureRoleDAO.hasPermissionOfAnyChild(featureDTO.getId(), role_id);
            return (permissionLevel > 0 || hasAnyChildPermission);
        }
        return false;
    }

    public HashMap<String, UserFeatureMappingDTO> getFeatureMappingByRoleId(int roleId) {
        FeatureRoleDAO featureRoleDAO = new FeatureRoleDAO();
        return featureRoleDAO.getUserFeatureMappingByRoleId(roleId);
    }
}
