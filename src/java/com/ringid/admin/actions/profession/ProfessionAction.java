/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.profession;

import com.ringid.admin.dto.ProfessionDTO;
import com.ringid.admin.service.ProfessionService;
import com.ringid.admin.repository.ProfessionLoader;
import com.google.gson.Gson;
import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.dto.adminAuth.LoginDTO;
import com.ringid.admin.utils.Constants;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author mamun
 */
public class ProfessionAction extends BaseAction {

    static Logger loggger = RingLogger.getConfigPortalLogger();

    List<ProfessionDTO> professionDTOs = new ArrayList<>();

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = FAILURE;
        List<String> profession = new ArrayList<>();
        String activistName;
        try {
            LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
            activistName = loginDTO.getUserName();
        } catch (Exception e) {
            target = UNAUTHORIZED;
            return mapping.findForward(target);
        }
        try {
            ProfessionForm professionForm = (ProfessionForm) form;
            ProfessionDTO dto = new ProfessionDTO();
            int reasonCode = -1;
            ProfessionService scheduler = ProfessionService.getInstance();
            switch (professionForm.getAction()) {
                case Constants.ADD: {
                    if (professionForm.getProfession() != null && professionForm.getProfession().length > 0) {
                        RingLogger.getActivityLogger().debug("Action : ProfessionAction[add] activistName : " + activistName + " profession --> " + Arrays.toString(professionForm.getProfession()));
                        for (String str : professionForm.getProfession()) {
                            profession.add(str);
                        }
                        profession = scheduler.addProfessionData(profession);
                        if (profession.isEmpty()) {
                            target = SUCCESS;
                            ProfessionLoader.getInstance().forceReload();
                        } else {
                            request.setAttribute("message", "<span style='color: red'>Failed to add profession : " + new Gson().toJson(profession) + " </span>");
                        }
                    } else {
                        request.setAttribute("message", "<span style='color: red'>profession : null </span>");
                    }
                    break;
                }
                case Constants.DELETE: {
                    if (professionForm.getProfession() != null && professionForm.getProfession().length > 0) {
                        RingLogger.getActivityLogger().debug("Action : ProfessionAction[delete] activistName : " + activistName + " professionId --> " + Arrays.toString(professionForm.getProfession()));
                        for (String str : professionForm.getProfession()) {
                            UUID professionId = UUID.fromString(str);
                            reasonCode = scheduler.deleteProfession(professionId);
                        }
                        if (reasonCode == 0) {
                            target = SUCCESS;
                            ProfessionLoader.getInstance().forceReload();
                        }
                    } else {
                        request.setAttribute("message", "<span style='color: red'>profession : null </span>");
                    }
                    break;
                }
                default: {
                    target = SUCCESS;
                    break;
                }
            }
            if (SUCCESS.equals(target)) {
                if (request.getParameter("ForceReload") != null && "1".equals(request.getParameter("ForceReload"))) {
                    ProfessionLoader.getInstance().forceReload();
                }
                if (professionForm.getSearchText() != null && professionForm.getSearchText().length() > 0) {
                    dto.setSearchText(professionForm.getSearchText().trim().toLowerCase());
                }
                professionDTOs = scheduler.getProfessionList(dto);
                request.getSession(true).setAttribute(Constants.DATA_ROWS, professionDTOs);
                managePages(request, professionForm, professionDTOs.size());
            }
        } catch (NullPointerException e) {
            target = FAILURE;
        }
        return mapping.findForward(target);
    }
}
