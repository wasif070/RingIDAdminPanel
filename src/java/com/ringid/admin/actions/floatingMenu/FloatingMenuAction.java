/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.floatingMenu;

import com.ringid.admin.dto.FloatingMenuDTO;
import com.ringid.admin.BaseAction;
import com.ringid.admin.service.FloationMenuService;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.utils.Constants;
import com.ringid.admin.utils.MyAppError;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author mamun
 */
public class FloatingMenuAction extends BaseAction {

    private static final Logger logger = RingLogger.getConfigPortalLogger();

    List<FloatingMenuDTO> floatingMenuDTOs = new ArrayList<>();

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = SUCCESS;
        FloatingMenuForm floatingMenuForm = (FloatingMenuForm) form;
        FloatingMenuDTO floatingMenuDTO = new FloatingMenuDTO();
        MyAppError myAppError = new MyAppError();
        try {

            switch (floatingMenuForm.getAction()) {
                case Constants.ADD:
                    floatingMenuDTO.setTab(floatingMenuForm.getTab());
                    floatingMenuDTO.setType(floatingMenuForm.getType());
                    floatingMenuDTO.setName(floatingMenuForm.getName());
                    floatingMenuDTO.setWeight(floatingMenuForm.getWeight());
                    floatingMenuDTO.setSlideTime(floatingMenuForm.getSlideTime());
                    floatingMenuDTO.setRedirectId(floatingMenuForm.getRedirectId());
                    floatingMenuDTO.setBtnType(floatingMenuForm.getBtnType());
                    floatingMenuDTO.setBtnBgColor(floatingMenuForm.getBtnBgColor());
                    floatingMenuDTO.setBtnTxtColor(floatingMenuForm.getBtnTxtColor());

                    myAppError = FloationMenuService.getInstance().addFloatingMenuInfo(floatingMenuDTO);
                    break;
                case Constants.EDIT:
                    floatingMenuDTO = FloationMenuService.getInstance().getFloatingMenuByType(floatingMenuForm.getTab(), floatingMenuForm.getType(), floatingMenuForm.getName());
                    floatingMenuForm.setTab(floatingMenuDTO.getTab());
                    floatingMenuForm.setType(floatingMenuDTO.getType());
                    floatingMenuForm.setOldName(floatingMenuDTO.getName());
                    floatingMenuForm.setName(floatingMenuDTO.getName());
                    floatingMenuForm.setWeight(floatingMenuDTO.getWeight());
                    floatingMenuForm.setSlideTime(floatingMenuDTO.getSlideTime());
                    floatingMenuForm.setRedirectId(floatingMenuDTO.getRedirectId());
                    floatingMenuForm.setBtnType(floatingMenuDTO.getBtnType());
                    floatingMenuForm.setBtnBgColor(floatingMenuDTO.getBtnBgColor());
                    floatingMenuForm.setBtnTxtColor(floatingMenuDTO.getBtnTxtColor());
                    break;
                case Constants.UPDATE:
                    floatingMenuDTO.setTab(floatingMenuForm.getTab());
                    floatingMenuDTO.setType(floatingMenuForm.getType());
                    floatingMenuDTO.setName(floatingMenuForm.getName());
                    floatingMenuDTO.setWeight(floatingMenuForm.getWeight());
                    floatingMenuDTO.setSlideTime(floatingMenuForm.getSlideTime());
                    floatingMenuDTO.setRedirectId(floatingMenuForm.getRedirectId());
                    floatingMenuDTO.setBtnType(floatingMenuForm.getBtnType());
                    floatingMenuDTO.setBtnBgColor(floatingMenuForm.getBtnBgColor());
                    floatingMenuDTO.setBtnTxtColor(floatingMenuForm.getBtnTxtColor());

                    myAppError = FloationMenuService.getInstance().updateFloatingMenuInfo(floatingMenuDTO, floatingMenuForm.getOldName());
                    break;
//                case Constants.DELETE:
//                    myAppError = CommonTaskScheduler.getInstance().deleteFloatingMenu(floatingMenuForm.getType());
//                    break;//                case Constants.DELETE:
//                    myAppError = FloationMenuService.getInstance().deleteFloatingMenu(floatingMenuForm.getType());
//                    break;
            }
            if (floatingMenuForm.getAction() != Constants.EDIT && myAppError.getERROR_TYPE() == MyAppError.NOERROR) {
                request.setAttribute("message", "<span style='color:green;'> Operation Successfull </span>");
                notifyAuthServers(Constants.RELOAD_FLOATING_MENU, "");
            } else {
                request.setAttribute("message", "<span style='color:green;'> Operation Failed </span>");
            }
        } catch (Exception e) {
            logger.error(e);
            target = FAILURE;
        }
        return mapping.findForward(target);
    }
}
