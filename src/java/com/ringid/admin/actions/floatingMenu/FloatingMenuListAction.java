/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.floatingMenu;

import com.ringid.admin.dto.FloatingMenuDTO;
import com.ringid.admin.BaseAction;
import com.ringid.admin.service.FloationMenuService;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.utils.Constants;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author mamun
 */
public class FloatingMenuListAction extends BaseAction {

    private static final Logger logger = RingLogger.getConfigPortalLogger();

    List<FloatingMenuDTO> floatingMenuDTOs = new ArrayList<>();

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = SUCCESS;
        FloatingMenuForm floatingMenuForm = (FloatingMenuForm) form;
        try {
            FloatingMenuDTO dto = new FloatingMenuDTO();
            dto.setSearchText(floatingMenuForm.getSearchText());
            if (floatingMenuForm.getColumn() <= 0) {
                floatingMenuForm.setColumn(Constants.COLUMN_ONE);
                floatingMenuForm.setSort(Constants.DESC_SORT);
            }
            dto.setColumn(floatingMenuForm.getColumn());
            dto.setSortType(floatingMenuForm.getSort());
            floatingMenuDTOs = FloationMenuService.getInstance().getFloatingMenuList(dto);
            request.getSession(true).setAttribute(Constants.DATA_ROWS, floatingMenuDTOs);
            managePages(request, floatingMenuForm, floatingMenuDTOs.size());
        } catch (Exception e) {
            logger.error(e);
            target = FAILURE;
        }
        return mapping.findForward(target);
    }
}
