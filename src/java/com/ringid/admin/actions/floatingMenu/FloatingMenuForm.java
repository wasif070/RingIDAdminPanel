/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.floatingMenu;

import com.ringid.admin.BaseForm;
import javax.servlet.http.HttpServletRequest;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author mamun
 */
public class FloatingMenuForm extends BaseForm {

    private int tab;
    private int type;
    private String name;
    private int weight;
    private double slideTime;
    private long redirectId;
    private int btnType;
    private String btnBgColor;
    private String btnTxtColor;
    private String oldName;

    public String getOldName() {
        return oldName;
    }

    public void setOldName(String oldName) {
        this.oldName = oldName;
    }

    public int getTab() {
        return tab;
    }

    public void setTab(int tab) {
        this.tab = tab;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public double getSlideTime() {
        return slideTime;
    }

    public void setSlideTime(double slideTime) {
        this.slideTime = slideTime;
    }

    public long getRedirectId() {
        return redirectId;
    }

    public void setRedirectId(long redirectId) {
        this.redirectId = redirectId;
    }

    public int getBtnType() {
        return btnType;
    }

    public void setBtnType(int btnType) {
        this.btnType = btnType;
    }

    public String getBtnBgColor() {
        return btnBgColor;
    }

    public void setBtnBgColor(String btnBgColor) {
        this.btnBgColor = btnBgColor;
    }

    public String getBtnTxtColor() {
        return btnTxtColor;
    }

    public void setBtnTxtColor(String btnTxtColor) {
        this.btnTxtColor = btnTxtColor;
    }

    @Override
    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        ActionErrors errors = new ActionErrors();

        return errors;
    }
}
