/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.servers.imageServers;

import com.ringid.admin.utils.MyAppError;
import java.util.ArrayList;

/**
 *
 * @author Kazi Nasir Uddin Oly
 * @Date Jun 8, 2016
 */
public class ImageServerTaskScheduler {

    private ImageServerTaskScheduler() {

    }

    private static class ImageServerTaskSchedulerHolder {

        private static final ImageServerTaskScheduler INSTANCE = new ImageServerTaskScheduler();
    }

    public static ImageServerTaskScheduler getInstance() {
        return ImageServerTaskSchedulerHolder.INSTANCE;
    }

    public ArrayList<ImageServerDTO> getImageServerList(ImageServerDTO dto) {
        return ImageServerLoader.getInstance().getImageServerList(dto);
    }

    public ArrayList<ImageServerDTO> getImageServerList(int columnValue, int sortType, ImageServerDTO dto) {
        return ImageServerLoader.getInstance().getImageServerList(columnValue, sortType, dto);
    }

    public int getMaxId() {
        ImageServerDAO authServerDAO = new ImageServerDAO();
        return authServerDAO.getMaxId();
    }

    public MyAppError addImageServerInfo(ImageServerDTO p_dto) {
        ImageServerDAO imageServerDAO = new ImageServerDAO();
        return imageServerDAO.addImageServerInfo(p_dto);
    }

    public MyAppError addImageServerInfo(ArrayList<ImageServerDTO> maps) {
        ImageServerDAO imageServerDAO = new ImageServerDAO();
        return imageServerDAO.addImageServerInfo(maps);
    }

    public ImageServerDTO getImageServerInfoById(int id) {
        return ImageServerLoader.getInstance().getImageServerDTO(id);
    }

    public MyAppError deleteImageServer(int id) {
        ImageServerDAO imageServerDAO = new ImageServerDAO();
        return imageServerDAO.deleteImageServer(id);
    }

    public MyAppError updateImageServer(ImageServerDTO p_dto) {
        ImageServerDAO imageServerDAO = new ImageServerDAO();
        return imageServerDAO.updateImageServer(p_dto);
    }

    public ArrayList<ImageServerDTO> getImageServerListForDownload() {
        return ImageServerLoader.getInstance().getImageServerListForDownload();
    }

    public ImageServerDTO getImageServerDTO(ImageServerForm imageServerForm) {
        ImageServerDTO imageServerDTO = new ImageServerDTO();
        imageServerDTO.setServerIP(imageServerForm.getServerIP());
        imageServerDTO.setServerPort(Integer.valueOf(imageServerForm.getServerPort()));

        if (!imageServerForm.isNull(imageServerForm.getCurrentRegistration())) {
            imageServerDTO.setCurrentRegistration(Integer.valueOf(imageServerForm.getCurrentRegistration()));
        }

        if (imageServerForm.isNull(imageServerForm.getMaxRegistration())) {
            imageServerDTO.setMaxRegistration(10000);
        } else {
            imageServerDTO.setMaxRegistration(Integer.valueOf(imageServerForm.getMaxRegistration()));
        }

        if (imageServerForm.isNull(imageServerForm.getServerStatus())) {
            imageServerDTO.setServerStatus(1);
        } else {
            imageServerDTO.setServerStatus(Integer.valueOf(imageServerForm.getServerStatus()));
        }
        return imageServerDTO;
    }
}
