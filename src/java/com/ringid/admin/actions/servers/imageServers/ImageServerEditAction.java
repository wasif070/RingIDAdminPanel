/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.servers.imageServers;

import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author Kazi Nasir Uddin Oly
 * @Date Jun 8, 2016
 */
public class ImageServerEditAction extends BaseAction {

    Logger logger = RingLogger.getConfigPortalLogger();

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        String target = checkAuthentication(mapping, form, request);
        try {
            if (target.equals(SUCCESS)) {
                int id = 0;
                ImageServerForm imageServerForm = (ImageServerForm) form;
                if (request.getParameter("id") != null) {
                    id = Integer.parseInt(request.getParameter("id"));
                }
                ImageServerTaskScheduler scheduler = ImageServerTaskScheduler.getInstance();
                ImageServerDTO imageServerDTO = scheduler.getImageServerInfoById(id);
                if (imageServerDTO != null) {
                    imageServerForm.setNewId(imageServerDTO.getId());
                    imageServerForm.setId(imageServerDTO.getId());
                    imageServerForm.setServerIP(imageServerDTO.getServerIP());
                    imageServerForm.setServerPort(String.valueOf(imageServerDTO.getServerPort()));
                    imageServerForm.setCurrentRegistration(String.valueOf(imageServerDTO.getCurrentRegistration()));
                    imageServerForm.setMaxRegistration(String.valueOf(imageServerDTO.getMaxRegistration()));
                    imageServerForm.setServerStatus(String.valueOf(imageServerDTO.getServerStatus()));
                    target = SUCCESS;
                }
            }
        } catch (Exception e) {
            logger.error("ImageServerEditAction Exception: " + e);
            target = FAILURE;
        }
        return mapping.findForward(target);
    }
}
