/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.servers.imageServers;

import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.Constants;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author Kazi Nasir Uddin Oly
 * @Date Jun 8, 2016
 */
public class ImageServerListAction extends BaseAction {

    ArrayList<ImageServerDTO> list = new ArrayList<>();

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = FAILURE;
        try {
            ImageServerForm imageServerForm = (ImageServerForm) form;
            ImageServerDTO imageServerDTO = new ImageServerDTO();
            ImageServerTaskScheduler scheduler = ImageServerTaskScheduler.getInstance();

            if (request.getParameter("ForceReload") != null && "1".equals(request.getParameter("ForceReload"))) {
                ImageServerLoader.getInstance().forceReload();
            }
            if (imageServerForm.getSearchText() != null && imageServerForm.getSearchText().length() > 0) {
                imageServerDTO.setSearchText(imageServerForm.getSearchText().trim().toLowerCase());
            }
            if (imageServerForm.getColumn() <= 0) {
                imageServerForm.setColumn(Constants.COLUMN_ONE);
                imageServerForm.setSort(Constants.DESC_SORT);
            }
            imageServerDTO.setColumn(imageServerForm.getColumn());
            imageServerDTO.setSortType(imageServerForm.getSort());
            list = scheduler.getImageServerList(imageServerDTO);
            request.getSession(true).setAttribute(Constants.DATA_ROWS, list);
            managePages(request, imageServerForm, list.size());
            request.getSession(true).setAttribute("search", imageServerDTO.getServerIP());
        } catch (NullPointerException e) {
            target = FAILURE;
        }
        return mapping.findForward(target);
    }

}
