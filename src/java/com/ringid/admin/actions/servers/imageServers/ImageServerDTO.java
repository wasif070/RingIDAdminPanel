/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.servers.imageServers;

import com.ringid.admin.BaseDTO;
import com.ringid.admin.utils.Constants;
import java.util.Comparator;

/**
 *
 * @author Kazi Nasir Uddin Oly
 * @Date Jun 7, 2016
 */
public class ImageServerDTO extends BaseDTO {

    private int id;
    private int newId;
    private String serverIP;
    private int serverPort;
    private int currentRegistration;
    private int maxRegistration;
    private int serverStatus;
    private boolean isSearchByIP = false;

    public int getNewId() {
        return newId;
    }

    public void setNewId(int newId) {
        this.newId = newId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getServerIP() {
        return serverIP;
    }

    public void setServerIP(String serverIP) {
        this.serverIP = serverIP;
    }

    public int getServerPort() {
        return serverPort;
    }

    public void setServerPort(int serverPort) {
        this.serverPort = serverPort;
    }

    public int getCurrentRegistration() {
        return currentRegistration;
    }

    public void setCurrentRegistration(int currentRegistration) {
        this.currentRegistration = currentRegistration;
    }

    public int getMaxRegistration() {
        return maxRegistration;
    }

    public void setMaxRegistration(int maxRegistration) {
        this.maxRegistration = maxRegistration;
    }

    public int getServerStatus() {
        return serverStatus;
    }

    public void setServerStatus(int serverStatus) {
        this.serverStatus = serverStatus;
    }

    public boolean isIsSearchByIP() {
        return isSearchByIP;
    }

    public void setIsSearchByIP(boolean isSearchByIP) {
        this.isSearchByIP = isSearchByIP;
    }

    public static class CompASC implements Comparator<ImageServerDTO> {

        @Override
        public int compare(ImageServerDTO o1, ImageServerDTO o2) {
            switch (column) {
                case Constants.COLUMN_ONE:
                    return o1.getId() - o2.getId();
                case Constants.COLUMN_TWO:
                    return o1.getServerIP().compareTo(o2.getServerIP());
                default:
                    return o1.getId() - o2.getId();
            }
        }
    }

    public static class CompDSC implements Comparator<ImageServerDTO> {

        @Override
        public int compare(ImageServerDTO o1, ImageServerDTO o2) {
            switch (column) {
                case Constants.COLUMN_ONE:
                    return o2.getId() - o1.getId();
                case Constants.COLUMN_TWO:
                    return o2.getServerIP().compareTo(o1.getServerIP());
                default:
                    return o2.getId() - o1.getId();
            }
        }
    }
}
