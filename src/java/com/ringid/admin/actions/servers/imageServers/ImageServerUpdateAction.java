/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.servers.imageServers;

import com.google.gson.Gson;
import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.dto.adminAuth.LoginDTO;
import com.ringid.admin.utils.Constants;
import com.ringid.admin.utils.MyAppError;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author Kazi Nasir Uddin Oly
 * @Date Jun 8, 2016
 */
public class ImageServerUpdateAction extends BaseAction {

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        String target = checkAuthentication(mapping, form, request);
        String activistName;
        try {
            LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
            activistName = loginDTO.getUserName();
        } catch (Exception e) {
            target = UNAUTHORIZED;
            return mapping.findForward(target);
        }
        try {
            if (target.equals(SUCCESS)) {
                ImageServerForm imageServerForm = (ImageServerForm) form;
                ImageServerTaskScheduler scheduler = ImageServerTaskScheduler.getInstance();
                ImageServerDTO imageServerDTO = scheduler.getImageServerDTO(imageServerForm);
                imageServerDTO.setId(imageServerForm.getId());
                imageServerDTO.setNewId(imageServerForm.getNewId());
                if (imageServerDTO.getNewId() <= 0) {
                    imageServerDTO.setNewId(imageServerDTO.getId());
                }
                RingLogger.getActivityLogger().debug("[ImageServerUpdateAction] activistName : " + activistName + " imageServerDTO --> " + new Gson().toJson(imageServerDTO));
                MyAppError error = scheduler.updateImageServer(imageServerDTO);
                if (error.getERROR_TYPE() > 0) {
                    target = FAILURE;
                    imageServerForm.setMessage(error.getErrorMessage());
                    request.getSession(true).setAttribute("error", "2");
                } else {
                    ImageServerLoader.getInstance().forceReload();
                }
            }
        } catch (Exception e) {
        }
        return mapping.findForward(target);
    }
}
