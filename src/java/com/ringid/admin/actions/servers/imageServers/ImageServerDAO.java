/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.servers.imageServers;

import com.ringid.admin.BaseDAO;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.utils.MyAppError;
import java.sql.SQLException;
import java.util.ArrayList;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Kazi Nasir Uddin Oly
 * @Date Jun 7, 2016
 */
public class ImageServerDAO extends BaseDAO {

    static Logger logger = RingLogger.getConfigPortalLogger();

    public ImageServerDAO() {

    }

    public int getMaxId() {
        int id = 0;
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            stmt = db.connection.createStatement();
            String query = "SELECT max(id) AS maxId FROM imageservers;";
            rs = stmt.executeQuery(query);
            if (rs.next()) {
                id = rs.getInt("maxId");
            }
        } catch (Exception e) {
            logger.error("Exception in AuthSeverDAO[getMaxId] -> " + e);
        } finally {
            close();
        }
        return id;
    }

    public MyAppError addImageServerInfo(ImageServerDTO dto) {
        MyAppError error = new MyAppError();
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            stmt = db.connection.createStatement();
            String query = "INSERT INTO imageservers(id, serverIP, serverPort, currentRegitration, maxRegitration, serverStatus) VALUES (?, ?, ?, ?, ?, ?);";
            ps = db.connection.prepareStatement(query);
            int index = 1;
            ps.setInt(index++, dto.getId());
            ps.setString(index++, dto.getServerIP());
            ps.setInt(index++, dto.getServerPort());
            ps.setInt(index++, dto.getCurrentRegistration());
            ps.setInt(index++, dto.getMaxRegistration());
            ps.setInt(index++, dto.getServerStatus());
            ps.execute();
            ps.close();
        } catch (SQLException e) {
            error.setERROR_TYPE(error.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            logger.debug("Single addImageServerInfo SQL Exception --> " + e);
        } catch (Exception e) {
            error.setERROR_TYPE(error.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            logger.debug("Single addImageServerInfo Exception --> " + e);
        } finally {
            close();
        }
        return error;
    }

    public MyAppError addImageServerInfo(ArrayList<ImageServerDTO> maps) {
        MyAppError error = new MyAppError();
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            String query = "INSERT INTO imageservers (id, serverIP, serverPort, currentRegitration, maxRegitration, serverStatus)                                         "
                    + "         SELECT * FROM (SELECT ? AS c0, SELECT ? AS c1, ? AS c2, ? AS c3, ? AS c4, ? AS c5) AS tmp                                                                       "
                    + "         WHERE NOT EXISTS (                                                                                          "
                    + "         SELECT serverIP, serverPort FROM imageservers WHERE serverIP = ? AND serverPort = ? "
                    + "         );";
            ps = db.connection.prepareStatement(query);
            int i = 1;
            for (ImageServerDTO entry : maps) {
                int index = 1;
                ps.setInt(index++, entry.getId());
                ps.setString(index++, entry.getServerIP());
                ps.setInt(index++, entry.getServerPort());
                ps.setInt(index++, entry.getCurrentRegistration());
                ps.setInt(index++, entry.getMaxRegistration());
                ps.setInt(index++, entry.getServerStatus());
                ps.setString(index++, entry.getServerIP());
                ps.setInt(index++, entry.getServerPort());
                ps.addBatch();
                i++;
                if ((i + 1) % 1000 == 0) {
                    ps.executeBatch();
                }
            }
            int a[] = ps.executeBatch();
        } catch (SQLException e) {
            error.setERROR_TYPE(error.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            logger.debug("addImageServerInfo Map SQL Exception --> " + e);
        } catch (Exception e) {
            error.setERROR_TYPE(error.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            logger.debug("addImageServerInfo Map Exception --> " + e);
        } finally {
            close();
        }
        return error;
    }

    public MyAppError updateImageServer(ImageServerDTO dto) {
        MyAppError error = new MyAppError();
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            String query = "UPDATE imageservers SET id = ?, serverIP = ?, serverPort = ?, currentRegitration = ?, maxRegitration = ?, serverStatus = ? WHERE id = ?;";
            ps = db.connection.prepareStatement(query);
            int index = 1;
            ps.setInt(index++, dto.getNewId());
            ps.setString(index++, dto.getServerIP());
            ps.setInt(index++, dto.getServerPort());
            ps.setInt(index++, dto.getCurrentRegistration());
            ps.setInt(index++, dto.getMaxRegistration());
            ps.setInt(index++, dto.getServerStatus());
            ps.setInt(index++, dto.getId());
            ps.execute();
        } catch (SQLException e) {
            error.setERROR_TYPE(error.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            logger.debug("updateImageServer SQL Exception --> " + e);
        } catch (Exception e) {
            error.setERROR_TYPE(error.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            logger.debug("updateImageServer Exception --> " + e);
        } finally {
            close();
        }
        return error;
    }

    public MyAppError deleteImageServer(int id) {
        MyAppError error = new MyAppError();
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            stmt = db.connection.createStatement();
            String sql = "DELETE FROM imageservers WHERE id = " + id + ";";
            int a = stmt.executeUpdate(sql);
        } catch (SQLException e) {
            error.setERROR_TYPE(error.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            logger.debug("deleteImageServer SQL Exception --> " + e);
        } catch (Exception e) {
            error.setERROR_TYPE(error.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            logger.debug("deleteImageServer Exception  --> " + e);
        } finally {
            close();
        }
        return error;
    }

}
