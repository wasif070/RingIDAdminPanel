/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ringid.admin.actions.servers.imageServers;


import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.utils.MyAppError;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;


/**
 *
 * @author Kazi Nasir Uddin Oly 
 * @Date Jun 8, 2016
 */
public class ImageServerUploadAction extends BaseAction{
    static Logger logger = RingLogger.getConfigPortalLogger();
    String line = "";
    String cvsSplitBy = ",";

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = checkAuthentication(mapping, form, request);
        if(target.equals(SUCCESS)){
            String[] dataArray = getDataFromFile(form);
            try{
                ArrayList<ImageServerDTO> imageServerList = new ArrayList<>();

                for(String s : dataArray) {
                    String[] imageServer = s.split(cvsSplitBy);
                    String serverIP = imageServer[0];
                    String registerPort = imageServer[1];
                    String currentRegistration = imageServer[2];
                    String maxRegistration = imageServer[3];
                    String serverStatus = imageServer[4];
                    if(serverIP.equalsIgnoreCase("Server IP")){
                        continue;
                    }

                    ImageServerDTO imageServerDTO = new ImageServerDTO();
                    imageServerDTO.setServerIP(serverIP);
                    imageServerDTO.setServerPort(Integer.valueOf(registerPort));
                    imageServerDTO.setCurrentRegistration(Integer.valueOf(currentRegistration));
                    imageServerDTO.setMaxRegistration(Integer.valueOf(maxRegistration));
                    imageServerDTO.setServerStatus(Integer.valueOf(serverStatus));

                    imageServerList.add(imageServerDTO); 
                }

                MyAppError error = ImageServerTaskScheduler.getInstance().addImageServerInfo(imageServerList);
                if(error.getERROR_TYPE() > 0){
                    target = FAILURE;
                } else{
                    ImageServerLoader.getInstance().forceReload();
                }
            } catch(Exception e){
                logger.error("ImageServerUploadAction Exception: " + e); 
                target = FAILURE;
            }
        }
        return mapping.findForward(target);
    }

}
