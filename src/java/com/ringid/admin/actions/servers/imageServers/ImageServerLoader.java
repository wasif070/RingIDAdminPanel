/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.servers.imageServers;

import com.ringid.admin.BaseDAO;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.utils.Constants;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Kazi Nasir Uddin Oly
 * @Date Jun 8, 2016
 */
public class ImageServerLoader extends BaseDAO {

    private final static long LOADING_INTERVAL = 60 * 60 * 1000;
    private long loadingTime = 0;
    private HashMap<Integer, ImageServerDTO> imageServerHashMapList;
    private static ImageServerLoader imageServerLoader = null;
    static Logger logger = RingLogger.getConfigPortalLogger();

    public ImageServerLoader() {

    }

    public static ImageServerLoader getInstance() {
        if (imageServerLoader == null) {
            createLoader();
        }
        return imageServerLoader;
    }

    private synchronized static void createLoader() {
        if (imageServerLoader == null) {
            imageServerLoader = new ImageServerLoader();
        }
    }

    private void forceLoadData() {
        imageServerHashMapList = new HashMap<>();
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            stmt = db.connection.createStatement();
            String sql = "SELECT id, serverIP, serverPort, currentRegitration, maxRegitration, serverStatus FROM imageservers;";
            rs = stmt.executeQuery(sql);
            while (rs.next()) {
                ImageServerDTO dto = new ImageServerDTO();
                dto.setId(rs.getInt("id"));
                dto.setServerIP(rs.getString("serverIP"));
                dto.setServerPort(rs.getInt("serverPort"));
                dto.setCurrentRegistration(rs.getInt("currentRegitration"));
                dto.setMaxRegistration(rs.getInt("maxRegitration"));
                dto.setServerStatus(rs.getInt("serverStatus"));
                imageServerHashMapList.put(dto.getId(), dto);
            }
            logger.debug(sql + "::" + imageServerHashMapList.size());

        } catch (Exception e) {
            logger.debug("Image Server List Exception...", e);
        } finally {
            close();
        }
    }

    private void checkForReload() {
        long currentTime = System.currentTimeMillis();
        if (currentTime - loadingTime > LOADING_INTERVAL) {
            loadingTime = currentTime;
            forceLoadData();
        }
    }

    public synchronized void forceReload() {
        loadingTime = System.currentTimeMillis();
        forceLoadData();
    }

    private boolean contains(ImageServerDTO dto, String searchText) {
        return (String.valueOf(dto.getId()).contains(searchText))
                || (dto.getServerIP() != null && dto.getServerIP().contains(searchText))
                || (String.valueOf(dto.getServerPort()).contains(searchText))
                || (String.valueOf(dto.getCurrentRegistration()).contains(searchText))
                || (String.valueOf(dto.getMaxRegistration()).contains(searchText));
    }

    private ArrayList<ImageServerDTO> getImageServerData(ImageServerDTO sdto) {
        checkForReload();
        ArrayList<ImageServerDTO> data = new ArrayList<>();
        Set set = imageServerHashMapList.entrySet();
        Iterator i = set.iterator();
        while (i.hasNext()) {
            Map.Entry me = (Map.Entry) i.next();
            ImageServerDTO dto = (ImageServerDTO) me.getValue();
            if (sdto.getSearchText() != null && sdto.getSearchText().length() > 0 && !contains(dto, sdto.getSearchText())) {
                continue;

            }
            data.add(dto);
        }
        if (sdto.getSortType() == Constants.ASC_SORT) {
            Collections.sort(data, new ImageServerDTO.CompASC());
        } else {
            Collections.sort(data, new ImageServerDTO.CompDSC());
        }
        return data;
    }

    public synchronized ArrayList<ImageServerDTO> getImageServerList(ImageServerDTO sdto) {
        ArrayList<ImageServerDTO> imageServerList = getImageServerData(sdto);
        logger.debug("getImageServerList Size: " + imageServerList.size());
        return imageServerList;
    }

    public synchronized ArrayList<ImageServerDTO> getImageServerList(int columnValue, int sortType, ImageServerDTO sdto) {
        ArrayList<ImageServerDTO> imageServerList = getImageServerData(sdto);
        if (sdto.getSortType() == Constants.ASC_SORT) {
            Collections.sort(imageServerList, new ImageServerDTO.CompASC());
        } else {
            Collections.sort(imageServerList, new ImageServerDTO.CompDSC());
        }
        logger.debug("getImageServerList Size: " + imageServerList.size());
        return imageServerList;
    }

    public synchronized ArrayList<ImageServerDTO> getImageServerListForDownload() {
        checkForReload();
        ArrayList<ImageServerDTO> data = new ArrayList<>(imageServerHashMapList.values());
        return data;
    }

    public synchronized ImageServerDTO getImageServerDTO(int id) {
        checkForReload();
        if (imageServerHashMapList.containsKey(id)) {
            return imageServerHashMapList.get(id);
        }
        return null;
    }

}
