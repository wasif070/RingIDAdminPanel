package com.ringid.admin.actions.servers.streamingservers;

import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.utils.Constants;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class StreamingServerListAction extends BaseAction {

    static Logger loggger = RingLogger.getConfigPortalLogger();

    List<StreamingServerDTO> list = new ArrayList<>();

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = FAILURE;
        try {
            StreamingServerForm streamingServerForm = (StreamingServerForm) form;
            StreamingServerDTO streamingServerDTO = new StreamingServerDTO();
            StreamingServerTaskScheduler scheduler = StreamingServerTaskScheduler.getInstance();
            if (streamingServerForm.getSearchText() != null && streamingServerForm.getSearchText().length() > 0) {
                streamingServerDTO.setSearchText(streamingServerForm.getSearchText().trim().toLowerCase());
            }
            if (streamingServerForm.getColumn() <= 0) {
                streamingServerForm.setColumn(Constants.COLUMN_ONE);
                streamingServerForm.setSort(Constants.DESC_SORT);
            }
            streamingServerDTO.setColumn(streamingServerForm.getColumn());
            streamingServerDTO.setSortType(streamingServerForm.getSort());
            list = scheduler.getStreamingServersList(streamingServerDTO);
            loggger.debug("StreamingServerListAction : [ TotalRecords : " + list.size() + " ]");
            request.getSession(true).setAttribute(Constants.DATA_ROWS, list);
            managePages(request, streamingServerForm, list.size());
        } catch (NullPointerException e) {
            loggger.error(e);
            target = FAILURE;
        }
        return mapping.findForward(target);
    }
}
