package com.ringid.admin.actions.servers.streamingservers;

import com.ringid.admin.BaseDAO;
import com.ringid.admin.utils.log.RingLogger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import org.apache.logging.log4j.Logger;

public class StreamingServerDAO extends BaseDAO {

    static Logger logger = RingLogger.getConfigPortalLogger();

    public StreamingServerDAO() {

    }

    public Map<Integer, StreamingServerDTO> getStreamingServerData() {
        Map<Integer, StreamingServerDTO> streamingServerData = new HashMap<>();
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            stmt = db.connection.createStatement();
            String sql = "SELECT id, serverIP, registerPort, currentRegistration, maxRegistration, type, serverStatus FROM streamingservers;";
            rs = stmt.executeQuery(sql);
            while (rs.next()) {
                StreamingServerDTO dto = new StreamingServerDTO();
                dto.setId(rs.getInt("id"));
                dto.setServerIP(rs.getString("serverIP"));
                dto.setRegisterPort(rs.getInt("registerPort"));
                dto.setCurrentRegistration(rs.getInt("currentRegistration"));
                dto.setMaxRegistration(rs.getInt("maxRegistration"));
                dto.setType(rs.getInt("type"));
                dto.setServerStatus(rs.getInt("serverStatus"));
                streamingServerData.put(dto.getId(), dto);
            }
            logger.debug(sql + " [StreamingServerList] --> ::" + streamingServerData.size());
        } catch (Exception e) {
            logger.debug("StreamingServerList Exception...", e);
        } finally {
            close();
        }
        return streamingServerData;
    }

    public ArrayList<StreamingServerDTO> getStreamingServerServerList(StreamingServerDTO sdto) {

        ArrayList<StreamingServerDTO> streamingServerList = new ArrayList<>();

        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            stmt = db.connection.createStatement();
            String sql = "SELECT id, serverIP, registerPort, currentRegistration, maxRegistration, serverStatus FROM streamingservers;";
            rs = stmt.executeQuery(sql);
            while (rs.next()) {
                StreamingServerDTO dto = new StreamingServerDTO();
                dto.setId(rs.getInt("id"));
                dto.setServerIP(rs.getString("serverIP"));
                dto.setRegisterPort(rs.getInt("registerPort"));
                dto.setCurrentRegistration(rs.getInt("currentRegistration"));
                dto.setMaxRegistration(rs.getInt("maxRegistration"));
                dto.setServerStatus(rs.getInt("serverStatus"));
                if (sdto.getSearchText() != null && sdto.getSearchText().length() > 0) {
                    if (!(dto.getServerIP().toLowerCase().contains(sdto.getSearchText().trim().toLowerCase()))) {
                        continue;
                    }
                }
                streamingServerList.add(dto);
            }
            logger.debug(sql + " [StreamingServerList] --> ::" + streamingServerList.size());

        } catch (Exception e) {
            logger.debug("StreamingServerList Exception...", e);
        } finally {
            close();
        }
        return streamingServerList;
    }

    public ArrayList<StreamingServerDTO> getActiveChannelStreamingServerServerList() {

        ArrayList<StreamingServerDTO> streamingServerList = new ArrayList<>();

        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            stmt = db.connection.createStatement();
            String sql = "SELECT id, serverIP, registerPort, currentRegistration, maxRegistration, serverStatus FROM streamingservers WHERE serverStatus = 1 and type = 3;";
            rs = stmt.executeQuery(sql);
            while (rs.next()) {
                StreamingServerDTO dto = new StreamingServerDTO();
                dto.setId(rs.getInt("id"));
                dto.setServerIP(rs.getString("serverIP"));
                dto.setRegisterPort(rs.getInt("registerPort"));
                dto.setCurrentRegistration(rs.getInt("currentRegistration"));
                dto.setMaxRegistration(rs.getInt("maxRegistration"));
                dto.setServerStatus(rs.getInt("serverStatus"));

                streamingServerList.add(dto);
            }
            logger.debug(sql + " [getActiveChannelStreamingServerServerList] --> ::" + streamingServerList.size());

        } catch (Exception e) {
            logger.debug("getActiveChannelStreamingServerServerList Exception...", e);
        } finally {
            close();
        }
        return streamingServerList;
    }

    public StreamingServerDTO getStreamingServerDTO(int id) {

        StreamingServerDTO streamingServerDTO = new StreamingServerDTO();

        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            //stmt = db.connection.createStatement();
            String sql = "SELECT id, serverIP, registerPort, currentRegistration, maxRegistration, serverStatus FROM streamingservers where id = ? ;";

            ps = db.connection.prepareStatement(sql);
            ps.setInt(1, id);
            rs = ps.executeQuery();

            while (rs.next()) {
                streamingServerDTO.setId(rs.getInt("id"));
                streamingServerDTO.setServerIP(rs.getString("serverIP"));
                streamingServerDTO.setRegisterPort(rs.getInt("registerPort"));
                streamingServerDTO.setCurrentRegistration(rs.getInt("currentRegistration"));
                streamingServerDTO.setMaxRegistration(rs.getInt("maxRegistration"));
                streamingServerDTO.setServerStatus(rs.getInt("serverStatus"));
            }
            logger.debug(sql + "::" + streamingServerDTO.getId());

        } catch (Exception e) {
            logger.debug("StreamingServerDTO Exception...", e);
        } finally {
            close();
        }

        return streamingServerDTO;
    }
}
