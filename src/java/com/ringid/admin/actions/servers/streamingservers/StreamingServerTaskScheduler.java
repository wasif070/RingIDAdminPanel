package com.ringid.admin.actions.servers.streamingservers;

import java.util.ArrayList;
import java.util.List;

public class StreamingServerTaskScheduler {

    private StreamingServerTaskScheduler() {

    }

    private static class StreamingServerTaskSchedulerHolder {

        private static final StreamingServerTaskScheduler INSTANCE = new StreamingServerTaskScheduler();
    }

    public static StreamingServerTaskScheduler getInstance() {
        return StreamingServerTaskSchedulerHolder.INSTANCE;
    }

    public List<StreamingServerDTO> getStreamingServersList(StreamingServerDTO sdto) {
        List<StreamingServerDTO> streamingServerList = StreamingServerLoader.getInstance().getStreamingServerList(sdto);
        return streamingServerList;
    }

    public ArrayList<StreamingServerDTO> getActiveChannelStreamingServerServerList() {
        StreamingServerDAO streamingServerDAO = new StreamingServerDAO();
        ArrayList<StreamingServerDTO> streamingServerList = streamingServerDAO.getActiveChannelStreamingServerServerList();
        return streamingServerList;
    }

//    public ArrayList<StreamingServerDTO> getStreamingServersList(int columnValue, int sortType, StreamingServerDTO dto) {
//        StreamingServerDAO streamingServerDAO = new StreamingServerDAO();
//        ArrayList<StreamingServerDTO> appStoresList = streamingServerDAO.getStreamingServerServerList();
//        Collections.sort(appStoresList, new StreamingServerDTO.CompIdDSC());
//        return appStoresList;
//    }
    public StreamingServerDTO getStreamingServersInfoById(int id) {
        StreamingServerDTO streamingServerDTO = StreamingServerLoader.getInstance().getStreamingServerDTO(id);
        return streamingServerDTO;
    }
}
