package com.ringid.admin.actions.servers.streamingservers;

import com.google.gson.Gson;
import com.ringid.admin.BaseDTO;
import com.ringid.admin.utils.Constants;
import java.util.Comparator;

public class StreamingServerDTO extends BaseDTO {

    private int id;
    private String serverIP;
    private int registerPort;
    private int currentRegistration;
    private int maxRegistration;
    private int serverStatus;
    private int type;

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getServerIP() {
        return serverIP;
    }

    public void setServerIP(String serverIP) {
        this.serverIP = serverIP;
    }

    public int getRegisterPort() {
        return registerPort;
    }

    public void setRegisterPort(int registerPort) {
        this.registerPort = registerPort;
    }

    public int getCurrentRegistration() {
        return currentRegistration;
    }

    public void setCurrentRegistration(int currentRegistration) {
        this.currentRegistration = currentRegistration;
    }

    public int getMaxRegistration() {
        return maxRegistration;
    }

    public void setMaxRegistration(int maxRegistration) {
        this.maxRegistration = maxRegistration;
    }

    public int getServerStatus() {
        return serverStatus;
    }

    public void setServerStatus(int serverStatus) {
        this.serverStatus = serverStatus;
    }

    public static class CompASC implements Comparator<StreamingServerDTO> {

        @Override
        public int compare(StreamingServerDTO o1, StreamingServerDTO o2) {
            switch (column) {
                case Constants.COLUMN_ONE:
                    return o1.getServerIP().compareTo(o2.getServerIP());
                default:
                    return o1.getServerIP().compareTo(o2.getServerIP());
            }
        }
    }

    public static class CompDSC implements Comparator<StreamingServerDTO> {

        @Override
        public int compare(StreamingServerDTO o1, StreamingServerDTO o2) {
            switch (column) {
                case Constants.COLUMN_ONE:
                    return o2.getServerIP().compareTo(o1.getServerIP());
                default:
                    return o2.getServerIP().compareTo(o1.getServerIP());
            }
        }
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }

}
