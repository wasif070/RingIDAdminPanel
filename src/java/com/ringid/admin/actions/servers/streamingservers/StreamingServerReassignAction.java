package com.ringid.admin.actions.servers.streamingservers;

import com.google.gson.Gson;
import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.utils.Constants;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import ringid.LoginTaskScheduler;
import ringid.sessionless.SessionlessTaskseheduler;

public class StreamingServerReassignAction extends BaseAction {

    static Logger logger = RingLogger.getConfigPortalLogger();

    List<StreamingServerDTO> list = new ArrayList<>();

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        String target = checkAuthentication(mapping, form, request);

        try {
            if (target.equals(SUCCESS)) {
                StreamingServerForm streamingServerForm = (StreamingServerForm) form;
                int id = 0;
                if (request.getParameter("id") != null) {
                    id = Integer.parseInt(request.getParameter("id"));
                }
                logger.debug("StreamingServerReassignAction : [Id : " + id + " ]");
                StreamingServerTaskScheduler scheduler = StreamingServerTaskScheduler.getInstance();
                StreamingServerDTO streamingServer = scheduler.getStreamingServersInfoById(id);
                LoginTaskScheduler.getInstance().initSessionLessCommunication(Constants.USERID);
                auth.com.utils.MyAppError error = SessionlessTaskseheduler.getInstance().serverReasign(Constants.USERID, streamingServer.getServerIP(), streamingServer.getRegisterPort(), Constants.REASIGN_STREAM_SERVER);
                LoginTaskScheduler.getInstance().destroySessionLessCommunication(Constants.USERID);
                logger.debug("StreamingServerReassignAction : [error : " + error.getERROR_TYPE() + " ]");
                if (error.getERROR_TYPE() > 0) {
                    target = FAILURE;
                } else {
                    list = scheduler.getStreamingServersList(new StreamingServerDTO());
                    request.getSession(true).setAttribute(Constants.DATA_ROWS, list);
                    managePages(request, streamingServerForm, list.size());
                }
            }
        } catch (Exception e) {
            logger.error("Exception in StreamingServerReassignAction....", e);
            target = FAILURE;
        }
        return mapping.findForward(target);
    }
}
