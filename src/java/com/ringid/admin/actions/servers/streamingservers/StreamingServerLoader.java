/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.servers.streamingservers;

import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.utils.Constants;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author mamun
 */
public class StreamingServerLoader {

    private final static long LOADING_INTERVAL = 60 * 60 * 1000;
    private long loadingTime = 0;
    private Map<Integer, StreamingServerDTO> streamingServerHashMap;
    static Logger logger = RingLogger.getConfigPortalLogger();

    private StreamingServerLoader() {

    }

    private static class StreamingServerLoaderHolder {

        private static final StreamingServerLoader INSTANCE = new StreamingServerLoader();
    }

    public static StreamingServerLoader getInstance() {
        return StreamingServerLoaderHolder.INSTANCE;
    }

    private void forceLoadData() {
        streamingServerHashMap = new HashMap<>();
        streamingServerHashMap = new StreamingServerDAO().getStreamingServerData();
    }

    public void forceReload() {
        loadingTime = System.currentTimeMillis();
        forceLoadData();
    }

    private void checkForReload() {
        long currentTime = System.currentTimeMillis();
        if (currentTime - loadingTime > LOADING_INTERVAL) {
            loadingTime = currentTime;
            forceLoadData();
        }
    }

    private boolean contains(StreamingServerDTO dto, String searchText) {
        return (dto.getServerIP() != null && dto.getServerIP().contains(searchText))
                || (String.valueOf(dto.getRegisterPort()).contains(searchText))
                || (String.valueOf(dto.getCurrentRegistration()).contains(searchText))
                || (String.valueOf(dto.getMaxRegistration()).contains(searchText));
    }

    public List<StreamingServerDTO> getStreamingServerList(StreamingServerDTO sdto) {
        checkForReload();
        List<StreamingServerDTO> data = new ArrayList<>();
        Set set = streamingServerHashMap.entrySet();
        Iterator i = set.iterator();
        while (i.hasNext()) {
            Map.Entry me = (Map.Entry) i.next();
            StreamingServerDTO dto = (StreamingServerDTO) me.getValue();
            if (sdto.getSearchText() != null && sdto.getSearchText().length() > 0 && !contains(dto, sdto.getSearchText())) {
                continue;
            }
            data.add(dto);
        }
        if (sdto.getSortType() == Constants.DESC_SORT) {
            Collections.sort(data, new StreamingServerDTO.CompDSC());
        } else {
            Collections.sort(data, new StreamingServerDTO.CompASC());
        }
        logger.debug("getSmsServerList Size: " + data.size());
        return data;
    }

    public StreamingServerDTO getStreamingServerDTO(int id) {
        checkForReload();
        if (streamingServerHashMap.containsKey(id)) {
            return streamingServerHashMap.get(id);
        }
        return null;
    }
}
