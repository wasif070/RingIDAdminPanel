/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.servers.coinServers;

import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author Rabby
 */
public class CoinServerEditAction extends BaseAction {

    private static Logger logger = RingLogger.getConfigPortalLogger();

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        String target = checkAuthentication(mapping, form, request);
        try {
            if (target.equals(SUCCESS)) {
                int id = 0;
                CoinServerForm coinServerForm = (CoinServerForm) form;
                if (request.getParameter("id") != null) {
                    id = Integer.parseInt(request.getParameter("id"));
                }
                CoinServerTaskScheduler scheduler = CoinServerTaskScheduler.getInstance();
                CoinServerDTO coinServerDTO = scheduler.getCoinServerInfoById(id);
                if (coinServerDTO != null) {
                    coinServerForm.setNewId(coinServerDTO.getId());
                    coinServerForm.setId(coinServerDTO.getId());
                    coinServerForm.setServerUrl(coinServerDTO.getServerUrl());
                    coinServerForm.setServerIP(coinServerDTO.getServerIP());
                    coinServerForm.setServerPort(coinServerDTO.getServerPort());
                    coinServerForm.setServerStatus(coinServerDTO.getServerStatus());
                    target = SUCCESS;
                }
            }
        } catch (Exception e) {
            logger.error(e);
            target = FAILURE;
        }
        return mapping.findForward(target);
    }
}
