/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.servers.coinServers;

import com.ringid.admin.BaseDAO;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.utils.Constants;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Rabby
 */
public class CoinServerLoader extends BaseDAO {

    private final static long LOADING_INTERVAL = 60 * 60 * 1000;
    private long loadingTime = 0;
    private HashMap<Integer, CoinServerDTO> coinServerHashMapList;
    private static CoinServerLoader coinServerLoader = null;
    static Logger logger = RingLogger.getConfigPortalLogger();

    public CoinServerLoader() {

    }

    public static CoinServerLoader getInstance() {
        if (coinServerLoader == null) {
            createLoader();
        }
        return coinServerLoader;
    }

    private synchronized static void createLoader() {
        if (coinServerLoader == null) {
            coinServerLoader = new CoinServerLoader();
        }
    }

    private void forceLoadData() {
        coinServerHashMapList = new HashMap<>();
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            stmt = db.connection.createStatement();
            String sql = "SELECT id, server_url, serverIP, serverPort, server_status FROM coinservers;";
            rs = stmt.executeQuery(sql);
            while (rs.next()) {
                CoinServerDTO dto = new CoinServerDTO();
                dto.setId(rs.getInt("id"));
                dto.setServerUrl(rs.getString("server_url"));
                dto.setServerIP(rs.getString("serverIP"));
                dto.setServerPort(rs.getInt("serverPort"));
                dto.setServerStatus(rs.getInt("server_status"));
                coinServerHashMapList.put(dto.getId(), dto);
            }
            logger.debug(sql + "::" + coinServerHashMapList.size());

        } catch (Exception e) {
            logger.debug("Coin Server List Exception...", e);
        } finally {
            close();
        }
    }

    private void checkForReload() {
        long currentTime = System.currentTimeMillis();
        if (currentTime - loadingTime > LOADING_INTERVAL) {
            loadingTime = currentTime;
            forceLoadData();
        }
    }

    public synchronized void forceReload() {
        loadingTime = System.currentTimeMillis();
        forceLoadData();
    }

    private boolean contains(CoinServerDTO dto, String searchText) {
        return (String.valueOf(dto.getId()).contains(searchText))
                || (dto.getServerUrl() != null && dto.getServerUrl().toLowerCase().contains(searchText))
                || (dto.getServerIP() != null && dto.getServerIP().contains(searchText));
    }

    private ArrayList<CoinServerDTO> getCoinServerData(CoinServerDTO sdto) {
        checkForReload();
        ArrayList<CoinServerDTO> data = new ArrayList<>();
        Set set = coinServerHashMapList.entrySet();
        Iterator i = set.iterator();
        while (i.hasNext()) {
            Map.Entry me = (Map.Entry) i.next();
            CoinServerDTO dto = (CoinServerDTO) me.getValue();
            if (sdto.getSearchText() != null && sdto.getSearchText().length() > 0 && !contains(dto, sdto.getSearchText())) {
                continue;
            }
            data.add(dto);
        }
        if (sdto.getSortType() == Constants.DESC_SORT) {
            Collections.sort(data, new CoinServerDTO.CompDSC());
        } else {
            Collections.sort(data, new CoinServerDTO.CompASC());
        }
        return data;
    }

    public synchronized ArrayList<CoinServerDTO> getCoinServerList(CoinServerDTO sdto) {
        ArrayList<CoinServerDTO> chatServerList = getCoinServerData(sdto);
        logger.debug("getChatServerList Size: " + chatServerList.size());
        return chatServerList;
    }

    public synchronized ArrayList<CoinServerDTO> getCoinServerList(int columnValue, int sortType, CoinServerDTO sdto) {
        ArrayList<CoinServerDTO> chatServerList = getCoinServerData(sdto);
        if (sdto.getSortType() == Constants.DESC_SORT) {
            Collections.sort(chatServerList, new CoinServerDTO.CompDSC());
        } else {
            Collections.sort(chatServerList, new CoinServerDTO.CompASC());
        }
        logger.debug("getChatServerList Size: " + chatServerList.size());
        return chatServerList;
    }

    public synchronized ArrayList<CoinServerDTO> getCoinServerListForDownload() {
        checkForReload();
        ArrayList<CoinServerDTO> data = new ArrayList<>(coinServerHashMapList.values());
        return data;
    }

    public synchronized CoinServerDTO getCoinServerDTO(int id) {
        checkForReload();
        if (coinServerHashMapList.containsKey(id)) {
            return coinServerHashMapList.get(id);
        }
        return null;
    }
}
