/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.servers.coinServers;

import com.google.gson.Gson;
import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.dto.adminAuth.LoginDTO;
import com.ringid.admin.utils.Constants;
import com.ringid.admin.utils.MyAppError;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author Rabby
 */
public class CoinServerUpdateAction extends BaseAction {

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        String target = checkAuthentication(mapping, form, request);
        String activistName;
        try {
            LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
            activistName = loginDTO.getUserName();
        } catch (Exception e) {
            target = UNAUTHORIZED;
            return mapping.findForward(target);
        }
        try {
            if (target.equals(SUCCESS)) {
                CoinServerForm coinServerForm = (CoinServerForm) form;
                CoinServerTaskScheduler scheduler = CoinServerTaskScheduler.getInstance();
                CoinServerDTO coinServerDTO = scheduler.getCoinServerDTO(coinServerForm);
                coinServerDTO.setId(coinServerForm.getId());
                coinServerDTO.setNewId(coinServerForm.getNewId());
                if (coinServerDTO.getNewId() <= 0) {
                    coinServerDTO.setNewId(coinServerDTO.getId());
                }
                RingLogger.getActivityLogger().debug("[CoinServerUpdateAction] activistName : " + activistName + " coinServerDTO --> " + new Gson().toJson(coinServerDTO));
                MyAppError error = scheduler.updateCoinServer(coinServerDTO);
                if (error.getERROR_TYPE() > 0) {
                    target = FAILURE;
                    coinServerForm.setMessage(error.getErrorMessage());
                    request.getSession(true).setAttribute("error", "2");
                } else {
                    CoinServerLoader.getInstance().forceReload();
                }
            }
        } catch (Exception e) {
        }
        return mapping.findForward(target);
    }
}
