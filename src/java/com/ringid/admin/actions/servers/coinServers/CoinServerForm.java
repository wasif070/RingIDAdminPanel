/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.servers.coinServers;

import com.ringid.admin.BaseForm;
import com.ringid.admin.dto.adminAuth.LoginDTO;
import com.ringid.admin.projectMenu.MenuNames;
import com.ringid.admin.utils.Constants;
import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

/**
 *
 * @author Rabby
 */
public class CoinServerForm extends BaseForm {

    private int id;
    private int newId;
    private String serverUrl;
    private String serverIP;
    private int serverPort;
    private int serverStatus;

    public int getNewId() {
        return newId;
    }

    public void setNewId(int newId) {
        this.newId = newId;
    }

    public String getServerIP() {
        return serverIP;
    }

    public void setServerIP(String serverIP) {
        this.serverIP = serverIP;
    }

    public int getServerPort() {
        return serverPort;
    }

    public void setServerPort(int serverPort) {
        this.serverPort = serverPort;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getServerUrl() {
        return serverUrl;
    }

    public void setServerUrl(String serverUrl) {
        this.serverUrl = serverUrl;
    }

    public int getServerStatus() {
        return serverStatus;
    }

    public void setServerStatus(int serverStatus) {
        this.serverStatus = serverStatus;
    }

    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        ActionErrors errors = new ActionErrors();
        LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        String FEATURE_NAME = MenuNames.COIN_SERVER;
        if (loginDTO == null) {
            errors.add("auth", new ActionMessage("UnAuthorized Access"));
        } else if (Constants.ADD == getAction() || Constants.UPDATE == getAction()) {
            if (!(loginDTO.getPermissionLevel() == Constants.SUPER_ADMIN) && !(loginDTO.getFeauterMapList().get(FEATURE_NAME).getHasAddPermission()
                    || loginDTO.getFeauterMapList().get(FEATURE_NAME).getHasModifyPermission())) {
                errors.add("auth", new ActionMessage("UnAuthorized Access"));
            } else if (getServerUrl() == null || getServerUrl().length() == 0) {
                errors.add("serverUrl", new ActionMessage("errors.serverIP.required"));
            }
        }
        return errors;
    }
}
