/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.servers.coinServers;

import com.ringid.admin.BaseDTO;
import com.ringid.admin.utils.Constants;
import java.util.Comparator;

/**
 *
 * @author Rabby
 */
public class CoinServerDTO extends BaseDTO {

    private boolean isSearchByUrl = false;
    private int id;
    private int newId;
    private String serverUrl;
    private String serverIP;
    private int serverPort;
    private int serverStatus;

    public int getNewId() {
        return newId;
    }

    public void setNewId(int newId) {
        this.newId = newId;
    }

    public String getServerIP() {
        return serverIP;
    }

    public void setServerIP(String serverIP) {
        this.serverIP = serverIP;
    }

    public int getServerPort() {
        return serverPort;
    }

    public void setServerPort(int serverPort) {
        this.serverPort = serverPort;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getServerUrl() {
        return serverUrl;
    }

    public void setServerUrl(String serverUrl) {
        this.serverUrl = serverUrl;
    }

    public int getServerStatus() {
        return serverStatus;
    }

    public void setServerStatus(int serverStatus) {
        this.serverStatus = serverStatus;
    }

    public boolean isIsSearchByUrl() {
        return isSearchByUrl;
    }

    public void setIsSearchByUrl(boolean isSearchByUrl) {
        this.isSearchByUrl = isSearchByUrl;
    }

    public static class CompASC implements Comparator<CoinServerDTO> {

        @Override
        public int compare(CoinServerDTO o1, CoinServerDTO o2) {
            switch (column) {
                case Constants.COLUMN_ONE:
                    return o1.getId() - o2.getId();
                case Constants.COLUMN_TWO:
                    return o1.getServerIP().compareTo(o2.getServerIP());
                default:
                    return o1.getId() - o2.getId();
            }
        }
    }

    public static class CompDSC implements Comparator<CoinServerDTO> {

        @Override
        public int compare(CoinServerDTO o1, CoinServerDTO o2) {
            switch (column) {
                case Constants.COLUMN_ONE:
                    return o2.getId() - o1.getId();
                case Constants.COLUMN_TWO:
                    return o2.getServerIP().compareTo(o1.getServerIP());
                default:
                    return o2.getId() - o1.getId();
            }
        }
    }
}
