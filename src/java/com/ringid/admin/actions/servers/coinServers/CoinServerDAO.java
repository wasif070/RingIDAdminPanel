/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.servers.coinServers;

import com.ringid.admin.BaseDAO;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.utils.MyAppError;
import java.sql.SQLException;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Rabby
 */
public class CoinServerDAO extends BaseDAO {

    static Logger logger = RingLogger.getConfigPortalLogger();

    public CoinServerDAO() {

    }

    public int getMaxId() {
        int id = 0;
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            stmt = db.connection.createStatement();
            String query = "SELECT max(id) AS maxId FROM coinservers;";
            rs = stmt.executeQuery(query);
            if (rs.next()) {
                id = rs.getInt("maxId");
            }
        } catch (Exception e) {
            logger.error("Exception in AuthSeverDAO[getMaxId] -> " + e);
        } finally {
            close();
        }
        return id;
    }

    public MyAppError addCoinServerInfo(CoinServerDTO dto) {
        MyAppError error = new MyAppError();
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            stmt = db.connection.createStatement();
            String query = "INSERT INTO coinservers(id, server_url, serverIP, serverPort, server_status) VALUES (?, ?, ?, ?, ?);";
            ps = db.connection.prepareStatement(query);
            int index = 1;
            ps.setInt(index++, dto.getId());
            ps.setString(index++, dto.getServerUrl());
            ps.setString(index++, dto.getServerIP());
            ps.setInt(index++, dto.getServerPort());
            ps.setInt(index++, dto.getServerStatus());
            ps.execute();
            ps.close();
        } catch (SQLException e) {
            error.setERROR_TYPE(MyAppError.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            logger.debug("Single addCoinServerInfo SQL Exception --> " + e);
        } catch (Exception e) {
            error.setERROR_TYPE(MyAppError.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            logger.debug("Single addCoinServerInfo Exception --> " + e);
        } finally {
            close();
        }
        return error;
    }

    public MyAppError updateCoinServer(CoinServerDTO dto) {
        MyAppError error = new MyAppError();
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            String query = "UPDATE coinservers SET id = ?, server_url = ?, serverIP = ?, serverPort = ?, server_status = ? WHERE id = ?;";
            ps = db.connection.prepareStatement(query);
            int index = 1;
            ps.setInt(index++, dto.getNewId());
            ps.setString(index++, dto.getServerUrl());
            ps.setString(index++, dto.getServerIP());
            ps.setInt(index++, dto.getServerPort());
            ps.setInt(index++, dto.getServerStatus());
            ps.setInt(index++, dto.getId());
            ps.execute();
        } catch (SQLException e) {
            error.setERROR_TYPE(MyAppError.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            logger.debug("updateCoinServer SQL Exception --> " + e);
        } catch (Exception e) {
            error.setERROR_TYPE(MyAppError.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            logger.debug("updateCoinServer Exception --> " + e);
        } finally {
            close();
        }
        return error;
    }

    public MyAppError deleteCoinServer(int id) {
        MyAppError error = new MyAppError();
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            stmt = db.connection.createStatement();
            String sql = "DELETE FROM coinservers WHERE id = " + id + ";";
            int a = stmt.executeUpdate(sql);
            logger.debug(sql + " : delete success");
        } catch (SQLException e) {
            error.setERROR_TYPE(MyAppError.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            logger.debug("deleteCoinServer SQL Exception --> " + e);
        } catch (Exception e) {
            error.setERROR_TYPE(MyAppError.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            logger.debug("deleteCoinServer Exception --> " + e);
        } finally {
            close();
        }
        return error;
    }
}
