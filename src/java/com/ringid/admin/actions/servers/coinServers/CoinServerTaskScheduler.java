/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.servers.coinServers;

import com.ringid.admin.utils.MyAppError;
import java.util.ArrayList;

/**
 *
 * @author Rabby
 */
public class CoinServerTaskScheduler {

    private CoinServerTaskScheduler() {

    }

    private static class CoinServerTaskSchedulerHolder {

        private static final CoinServerTaskScheduler INSTANCE = new CoinServerTaskScheduler();
    }

    public static CoinServerTaskScheduler getInstance() {
        return CoinServerTaskSchedulerHolder.INSTANCE;
    }

    public ArrayList<CoinServerDTO> getCoinServerList(CoinServerDTO dto) {
        return CoinServerLoader.getInstance().getCoinServerList(dto);
    }

    public ArrayList<CoinServerDTO> getCoinServerList(int columnValue, int sortType, CoinServerDTO dto) {
        return CoinServerLoader.getInstance().getCoinServerList(columnValue, sortType, dto);
    }

    public int getMaxId() {
        CoinServerDAO authServerDAO = new CoinServerDAO();
        return authServerDAO.getMaxId();
    }

    public MyAppError addCoinServerInfo(CoinServerDTO p_dto) {
        CoinServerDAO coinServerDAO = new CoinServerDAO();
        return coinServerDAO.addCoinServerInfo(p_dto);
    }

    public CoinServerDTO getCoinServerInfoById(int id) {
        return CoinServerLoader.getInstance().getCoinServerDTO(id);
    }

    public MyAppError deleteCoinServer(int id) {
        CoinServerDAO coinServerDAO = new CoinServerDAO();
        return coinServerDAO.deleteCoinServer(id);
    }

    public MyAppError updateCoinServer(CoinServerDTO p_dto) {
        CoinServerDAO coinServerDAO = new CoinServerDAO();
        return coinServerDAO.updateCoinServer(p_dto);
    }

    public ArrayList<CoinServerDTO> getCoinServerListForDownload() {
        return CoinServerLoader.getInstance().getCoinServerListForDownload();
    }

    public CoinServerDTO getCoinServerDTO(CoinServerForm coinServerForm) {
        CoinServerDTO coinServerDTO = new CoinServerDTO();
        coinServerDTO.setServerUrl(coinServerForm.getServerUrl());
        coinServerDTO.setServerIP(coinServerForm.getServerIP());
        coinServerDTO.setServerPort(coinServerForm.getServerPort());
        coinServerDTO.setServerStatus(coinServerForm.getServerStatus());
        return coinServerDTO;
    }
}
