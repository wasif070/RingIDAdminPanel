/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.servers.coinServers;

import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.Constants;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author Rabby
 */
public class CoinServerListAction extends BaseAction {

    ArrayList<CoinServerDTO> list = new ArrayList<>();

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = FAILURE;
        try {
            CoinServerForm coinServerForm = (CoinServerForm) form;
            CoinServerDTO coinServerDTO = new CoinServerDTO();
            CoinServerTaskScheduler scheduler = CoinServerTaskScheduler.getInstance();

            if (request.getParameter("ForceReload") != null && "1".equals(request.getParameter("ForceReload"))) {
                CoinServerLoader.getInstance().forceReload();
            }
            if (coinServerForm.getSearchText() != null && coinServerForm.getSearchText().length() > 0) {
                coinServerDTO.setSearchText(coinServerForm.getSearchText().trim().toLowerCase());
            }
            if (coinServerForm.getColumn() <= 0) {
                coinServerForm.setColumn(Constants.COLUMN_ONE);
                coinServerForm.setSort(Constants.DESC_SORT);
            }
            coinServerDTO.setColumn(coinServerForm.getColumn());
            coinServerDTO.setSortType(coinServerForm.getSort());
            list = scheduler.getCoinServerList(coinServerDTO);
            request.getSession(true).setAttribute(Constants.DATA_ROWS, list);
            managePages(request, coinServerForm, list.size());
            request.getSession(true).setAttribute("search", coinServerDTO.getServerUrl());
        } catch (NullPointerException e) {
            target = FAILURE;
        }
        return mapping.findForward(target);
    }
}
