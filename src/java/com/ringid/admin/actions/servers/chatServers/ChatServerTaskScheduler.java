/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.servers.chatServers;

import com.ringid.admin.utils.MyAppError;
import java.util.ArrayList;

/**
 *
 * @author Kazi Nasir Uddin Oly
 * @Date Jun 8, 2016
 */
public class ChatServerTaskScheduler {

    private ChatServerTaskScheduler() {

    }

    private static class ChatServerTaskSchedulerHolder {

        private static final ChatServerTaskScheduler INSTANCE = new ChatServerTaskScheduler();
    }

    public static ChatServerTaskScheduler getInstance() {
        return ChatServerTaskSchedulerHolder.INSTANCE;
    }

    public ArrayList<ChatServerDTO> getChatServerList(ChatServerDTO dto) {
        return ChatServerLoader.getInstance().getChatServerList(dto);
    }

    public ArrayList<ChatServerDTO> getChatServerList(int columnValue, int sortType, ChatServerDTO dto) {
        return ChatServerLoader.getInstance().getChatServerList(columnValue, sortType, dto);
    }

    public int getMaxId() {
        ChatServerDAO authServerDAO = new ChatServerDAO();
        return authServerDAO.getMaxId();
    }

    public MyAppError addChatServerInfo(ChatServerDTO p_dto) {
        ChatServerDAO chatServerDAO = new ChatServerDAO();
        return chatServerDAO.addChatServerInfo(p_dto);
    }

    public MyAppError addChatServerInfo(ArrayList<ChatServerDTO> maps) {
        ChatServerDAO chatServerDAO = new ChatServerDAO();
        return chatServerDAO.addChatServerInfo(maps);
    }

    public ChatServerDTO getChatServerInfoById(int id) {
        return ChatServerLoader.getInstance().getChatServerDTO(id);
    }

    public MyAppError deleteChatServer(int id) {
        ChatServerDAO chatServerDAO = new ChatServerDAO();
        return chatServerDAO.deleteChatServer(id);
    }

    public MyAppError updateChatServer(ChatServerDTO p_dto) {
        ChatServerDAO chatServerDAO = new ChatServerDAO();
        return chatServerDAO.updateChatServer(p_dto);
    }

    public ArrayList<ChatServerDTO> getChatServerListForDownload() {
        return ChatServerLoader.getInstance().getChatServerListForDownload();
    }

    public ChatServerDTO getChatServerDTO(ChatServerForm chatServerForm) {
        ChatServerDTO chatServerDTO = new ChatServerDTO();
        chatServerDTO.setServerIP(chatServerForm.getServerIP());
        chatServerDTO.setRegisterPort(Integer.valueOf(chatServerForm.getRegisterPort()));

        if (!chatServerForm.isNull(chatServerForm.getCurrentRegistration())) {
            chatServerDTO.setCurrentRegistration(Integer.valueOf(chatServerForm.getCurrentRegistration()));
        }

        if (chatServerForm.isNull(chatServerForm.getMaxRegistration())) {
            chatServerDTO.setMaxRegistration(10000);
        } else {
            chatServerDTO.setMaxRegistration(Integer.valueOf(chatServerForm.getMaxRegistration()));
        }

        if (!chatServerForm.isNull(chatServerForm.getServerStatus())) {
            chatServerDTO.setServerStatus(Integer.valueOf(chatServerForm.getServerStatus()));
        }

        return chatServerDTO;
    }
}
