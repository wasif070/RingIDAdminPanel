/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.servers.chatServers;

import com.ringid.admin.BaseDAO;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.utils.Constants;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Kazi Nasir Uddin Oly
 * @Date Jun 8, 2016
 */
public class ChatServerLoader extends BaseDAO {

    private final static long LOADING_INTERVAL = 60 * 60 * 1000;
    private long loadingTime = 0;
    private HashMap<Integer, ChatServerDTO> chatServerHashMapList;
    private static ChatServerLoader chatServerLoader = null;
    static Logger logger = RingLogger.getConfigPortalLogger();

    public ChatServerLoader() {

    }

    public static ChatServerLoader getInstance() {
        if (chatServerLoader == null) {
            createLoader();
        }
        return chatServerLoader;
    }

    private synchronized static void createLoader() {
        if (chatServerLoader == null) {
            chatServerLoader = new ChatServerLoader();
        }
    }

    private void forceLoadData() {
        chatServerHashMapList = new HashMap<>();
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            stmt = db.connection.createStatement();
            String sql = "SELECT id, serverIP, registerPort, currentRegitration, maxRegitration, serverStatus FROM chatservers;";
            rs = stmt.executeQuery(sql);
            while (rs.next()) {
                ChatServerDTO dto = new ChatServerDTO();
                dto.setId(rs.getInt("id"));
                dto.setServerIP(rs.getString("serverIP"));
                dto.setRegisterPort(rs.getInt("registerPort"));
                dto.setCurrentRegistration(rs.getInt("currentRegitration"));
                dto.setMaxRegistration(rs.getInt("maxRegitration"));
                dto.setServerStatus(rs.getInt("serverStatus"));
                chatServerHashMapList.put(dto.getId(), dto);
            }
            logger.debug(sql + "::" + chatServerHashMapList.size());

        } catch (Exception e) {
            logger.debug("Chat Server List Exception...", e);
        } finally {
            close();
        }
    }

    private void checkForReload() {
        long currentTime = System.currentTimeMillis();
        if (currentTime - loadingTime > LOADING_INTERVAL) {
            loadingTime = currentTime;
            forceLoadData();
        }
    }

    public synchronized void forceReload() {
        loadingTime = System.currentTimeMillis();
        forceLoadData();
    }

    private boolean contains(ChatServerDTO dto, String searchText) {
        return (String.valueOf(dto.getId()).contains(searchText))
                || (dto.getServerIP() != null && dto.getServerIP().contains(searchText))
                || (String.valueOf(dto.getRegisterPort()).contains(searchText))
                || (String.valueOf(dto.getCurrentRegistration()).contains(searchText))
                || (String.valueOf(dto.getMaxRegistration()).contains(searchText));
    }

    private ArrayList<ChatServerDTO> getChatServerData(ChatServerDTO sdto) {
        checkForReload();
        ArrayList<ChatServerDTO> data = new ArrayList<>();
        Set set = chatServerHashMapList.entrySet();
        Iterator i = set.iterator();
        while (i.hasNext()) {
            Map.Entry me = (Map.Entry) i.next();
            ChatServerDTO dto = (ChatServerDTO) me.getValue();
            if (sdto.getSearchText() != null && sdto.getSearchText().length() > 0 && !contains(dto, sdto.getSearchText())) {
                continue;
            }
            data.add(dto);
        }
        if (sdto.getSortType() == Constants.DESC_SORT) {
            Collections.sort(data, new ChatServerDTO.CompDSC());
        } else {
            Collections.sort(data, new ChatServerDTO.CompASC());
        }
        return data;
    }

    public synchronized ArrayList<ChatServerDTO> getChatServerList(ChatServerDTO sdto) {
        ArrayList<ChatServerDTO> chatServerList = getChatServerData(sdto);
        logger.debug("getChatServerList Size: " + chatServerList.size());
        return chatServerList;
    }

    public synchronized ArrayList<ChatServerDTO> getChatServerList(int columnValue, int sortType, ChatServerDTO sdto) {
        ArrayList<ChatServerDTO> chatServerList = getChatServerData(sdto);
        if (sortType == Constants.DESC_SORT) {
            Collections.sort(chatServerList, new ChatServerDTO.CompDSC());
        } else {
            Collections.sort(chatServerList, new ChatServerDTO.CompASC());
        }
        logger.debug("getChatServerList Size: " + chatServerList.size());
        return chatServerList;
    }

    public synchronized ArrayList<ChatServerDTO> getChatServerListForDownload() {
        checkForReload();
        ArrayList<ChatServerDTO> data = new ArrayList<>(chatServerHashMapList.values());
        return data;
    }

    public synchronized ChatServerDTO getChatServerDTO(int id) {
        checkForReload();
        if (chatServerHashMapList.containsKey(id)) {
            return chatServerHashMapList.get(id);
        }
        return null;
    }

}
