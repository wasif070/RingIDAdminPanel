/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.servers.chatServers;

import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.Constants;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author Kazi Nasir Uddin Oly
 * @Date Jun 8, 2016
 */
public class ChatServerListAction extends BaseAction {

    ArrayList<ChatServerDTO> list = new ArrayList<>();

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = FAILURE;
        try {
            ChatServerForm chatServerForm = (ChatServerForm) form;
            ChatServerDTO chatServerDTO = new ChatServerDTO();
            ChatServerTaskScheduler scheduler = ChatServerTaskScheduler.getInstance();

            if (request.getParameter("ForceReload") != null && "1".equals(request.getParameter("ForceReload"))) {
                ChatServerLoader.getInstance().forceReload();
            }
            if (chatServerForm.getSearchText() != null && chatServerForm.getSearchText().length() > 0) {
                chatServerDTO.setSearchText(chatServerForm.getSearchText().trim().toLowerCase());
            }
            if (chatServerForm.getColumn() <= 0) {
                chatServerForm.setColumn(Constants.COLUMN_ONE);
                chatServerForm.setSort(Constants.DESC_SORT);
            }
            chatServerDTO.setColumn(chatServerForm.getColumn());
            chatServerDTO.setSortType(chatServerForm.getSort());
            list = scheduler.getChatServerList(chatServerDTO);
            request.getSession(true).setAttribute(Constants.DATA_ROWS, list);
            managePages(request, chatServerForm, list.size());
            request.getSession(true).setAttribute("search", chatServerDTO.getServerIP());
        } catch (NullPointerException e) {
            target = FAILURE;
        }
        return mapping.findForward(target);
    }

}
