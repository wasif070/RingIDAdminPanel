/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.servers.chatServers;

import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author Kazi Nasir Uddin Oly
 * @Date Jun 8, 2016
 */
public class ChatServerEditAction extends BaseAction {

    private static Logger logger = RingLogger.getConfigPortalLogger();

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        String target = checkAuthentication(mapping, form, request);
        try {
            if (target.equals(SUCCESS)) {
                int id = 0;
                ChatServerForm chatServerForm = (ChatServerForm) form;
                if (request.getParameter("id") != null) {
                    id = Integer.parseInt(request.getParameter("id"));
                }
                ChatServerTaskScheduler scheduler = ChatServerTaskScheduler.getInstance();
                ChatServerDTO chatServerDTO = scheduler.getChatServerInfoById(id);
                if (chatServerDTO != null) {
                    chatServerForm.setNewId(chatServerDTO.getId());
                    chatServerForm.setId(chatServerDTO.getId());
                    chatServerForm.setServerIP(chatServerDTO.getServerIP());
                    chatServerForm.setRegisterPort(String.valueOf(chatServerDTO.getRegisterPort()));
                    chatServerForm.setCurrentRegistration(String.valueOf(chatServerDTO.getCurrentRegistration()));
                    chatServerForm.setMaxRegistration(String.valueOf(chatServerDTO.getMaxRegistration()));
                    chatServerForm.setServerStatus(String.valueOf(chatServerDTO.getServerStatus()));
                    target = SUCCESS;
                }
            }
        } catch (Exception e) {
            logger.error(e);
            target = FAILURE;
        }
        return mapping.findForward(target);
    }
}
