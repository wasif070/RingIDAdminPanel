/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ringid.admin.actions.servers.chatServers;

import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.utils.Constants;
import com.ringid.admin.utils.MyAppError;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author Kazi Nasir Uddin Oly 
 * @Date Jun 8, 2016
 */
public class ChatServerUploadAction extends BaseAction{
    String line = "";
    String cvsSplitBy = ",";
    static Logger logger = RingLogger.getConfigPortalLogger();

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = checkAuthentication(mapping, form, request);
        if(target.equals(SUCCESS)){
            String[] dataArray = getDataFromFile(form);
            try{
                ArrayList<ChatServerDTO> chatServerList = new ArrayList<>();

                for(String s : dataArray) {
                    String[] chatServer = s.split(cvsSplitBy);
                    String serverIP = chatServer[0];
                    String registerPort = chatServer[1];
                    String currentRegistration = chatServer[2];
                    String maxRegistration = chatServer[3];
                    String serverStatus = chatServer[4];
                    if(serverIP.equalsIgnoreCase("Server IP")){
                        continue;
                    }

                    ChatServerDTO chatServerDTO = new ChatServerDTO();
                    chatServerDTO.setServerIP(serverIP);
                    chatServerDTO.setRegisterPort(Integer.valueOf(registerPort));
                    chatServerDTO.setCurrentRegistration(Integer.valueOf(currentRegistration));
                    chatServerDTO.setMaxRegistration(Integer.valueOf(maxRegistration));
                    chatServerDTO.setServerStatus(Integer.valueOf(serverStatus));

                    chatServerList.add(chatServerDTO); 
                }

                MyAppError error = ChatServerTaskScheduler.getInstance().addChatServerInfo(chatServerList);
                if(error.getERROR_TYPE() > 0){
                    target = FAILURE;
                } else{
                    ChatServerLoader.getInstance().forceReload();
                    notifyAuthServers(Constants.RELOAD_CHAT_SERVERS, null);
                }
            } catch(Exception e){
                logger.error("ChatServerUploadAction Exception: " + e); 
                target = FAILURE;
            }
        }
        return mapping.findForward(target);
    }

}
