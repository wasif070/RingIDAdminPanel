/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.servers.chatServers;

import com.ringid.admin.BaseForm;
import com.ringid.admin.dto.adminAuth.LoginDTO;
import com.ringid.admin.projectMenu.MenuNames;
import com.ringid.admin.utils.Constants;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

/**
 *
 * @author Kazi Nasir Uddin Oly
 * @Date Jun 8, 2016
 */
public class ChatServerForm extends BaseForm {

    private int id;
    private int newId;
    private String serverIP;
    private String registerPort;
    private String currentRegistration;
    private String maxRegistration;
    private String serverStatus;
    private ArrayList<ChatServerDTO> chatServerDTO = new ArrayList<ChatServerDTO>();

    public int getNewId() {
        return newId;
    }

    public void setNewId(int newId) {
        this.newId = newId;
    }

    public ArrayList<ChatServerDTO> getChatServerDTO() {
        return chatServerDTO;
    }

    public void setChatServerDTO(ArrayList<ChatServerDTO> chatServerDTO) {
        this.chatServerDTO = chatServerDTO;
    }

    public ChatServerForm() {
        super();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getServerIP() {
        return serverIP;
    }

    public void setServerIP(String serverIP) {
        this.serverIP = serverIP;
    }

    public String getRegisterPort() {
        return registerPort;
    }

    public void setRegisterPort(String registerPort) {
        this.registerPort = registerPort;
    }

    public String getCurrentRegistration() {
        return currentRegistration;
    }

    public void setCurrentRegistration(String currentRegistration) {
        this.currentRegistration = currentRegistration;
    }

    public String getMaxRegistration() {
        return maxRegistration;
    }

    public void setMaxRegistration(String maxRegistration) {
        this.maxRegistration = maxRegistration;
    }

    public String getServerStatus() {
        return serverStatus;
    }

    public void setServerStatus(String serverStatus) {
        this.serverStatus = serverStatus;
    }

    @Override
    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        ActionErrors errors = new ActionErrors();
        LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        String FEATURE_NAME = MenuNames.CHAT_SERVER;
        if (loginDTO == null) {
            errors.add("auth", new ActionMessage("UnAuthorized Access"));
        } else if (Constants.ADD == getAction() || Constants.UPDATE == getAction()) {
            if (!(loginDTO.getPermissionLevel() == Constants.SUPER_ADMIN) && !(loginDTO.getFeauterMapList().get(FEATURE_NAME).getHasAddPermission()
                    || loginDTO.getFeauterMapList().get(FEATURE_NAME).getHasModifyPermission())) {
                errors.add("auth", new ActionMessage("UnAuthorized Access"));
            } else {
                if (getServerIP() == null || getServerIP().length() == 0) {
                    errors.add("serverIP", new ActionMessage("errors.serverIP.required"));
                }
                if (getRegisterPort() == null || getRegisterPort().length() == 0) {
                    errors.add("registerPort", new ActionMessage("errors.registerPort.required"));
                }
            }
        }
        return errors;
    }
}
