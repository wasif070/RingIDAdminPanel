/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.servers.relayServers;

import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.utils.Constants;
import com.ringid.admin.utils.MyAppError;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;


/**
 *
 * @author Kazi Nasir Uddin Oly 
 * @Date Jun 4, 2016
 */
public class RelayServerUploadAction extends BaseAction {
    static Logger logger = RingLogger.getConfigPortalLogger();
    String line = "";
    String cvsSplitBy = ",";

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = checkAuthentication(mapping, form, request);
        
        if(target.equals(SUCCESS)){
            String dataArray[] = getDataFromFile(form);
            try{
                ArrayList<RelayServerDTO> relayServerList = new ArrayList<>();

                for(String s : dataArray) {
                    String[] relayServer = s.split(cvsSplitBy);
                    String serverIP = relayServer[0];
                    String relayPort = relayServer[1];
                    String logLevel = relayServer[2];
                    String active = relayServer[3];
                    if(serverIP.equalsIgnoreCase("Server IP")){
                        continue;
                    }

                    RelayServerDTO relayServerDTO = new RelayServerDTO();
                    relayServerDTO.setServerIP(serverIP);
                    relayServerDTO.setRelayPort(Integer.valueOf(relayPort));
                    relayServerDTO.setLogLevel(logLevel);
                    relayServerDTO.setActive(Integer.valueOf(active));

                    relayServerList.add(relayServerDTO); 
                }

                MyAppError error = RelayServerTaskScheduler.getInstance().addRelayServerInfo(relayServerList);
                if(error.getERROR_TYPE() > 0){
                    target = FAILURE;
                } else{
                    RelayServerLoader.getInstance().forceReload();
//                    notifyAuthServers(Constants.RELOAD_RELAY_SERVERS, null);
                }
            } catch(Exception e){
                logger.error("RelayServerUploadAction Exception: " + e);
                target = FAILURE;
            }
        }
        return mapping.findForward(target);
    }
    
  
}
