/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.servers.relayServers;

import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author Kazi Nasir Uddin Oly
 * @Date Jun 6, 2016
 */
public class RelayServerEditAction extends BaseAction {

    Logger logger = RingLogger.getConfigPortalLogger();

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        String target = checkAuthentication(mapping, form, request);
        try {
            if (target.equals(SUCCESS)) {
                int id = 0;
                RelayServerForm relayServerForm = (RelayServerForm) form;
                if (request.getParameter("id") != null) {
                    id = Integer.parseInt(request.getParameter("id"));
                }
                RelayServerTaskScheduler scheduler = RelayServerTaskScheduler.getInstance();
                RelayServerDTO relayServerDTO = scheduler.getRelayServerInfoById(id);
                if (relayServerDTO != null) {
                    relayServerForm.setNewId(relayServerDTO.getId());
                    relayServerForm.setId(relayServerDTO.getId());
                    relayServerForm.setServerIP(relayServerDTO.getServerIP());
                    relayServerForm.setRelayPort(String.valueOf(relayServerDTO.getRelayPort()));
                    relayServerForm.setLogLevel(relayServerDTO.getLogLevel());
                    relayServerForm.setActive(String.valueOf(relayServerDTO.getActive()));
                    target = SUCCESS;
                }
            }
        } catch (Exception e) {
            logger.error("RelayServerEditAction Exception: " + e);
            target = FAILURE;
        }
        return mapping.findForward(target);
    }
}
