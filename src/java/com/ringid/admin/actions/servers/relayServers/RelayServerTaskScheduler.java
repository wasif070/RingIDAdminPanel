/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.servers.relayServers;

import com.ringid.admin.utils.MyAppError;
import java.util.ArrayList;

/**
 *
 * @author Kazi Nasir Uddin Oly
 * @Date Jun 5, 2016
 */
public class RelayServerTaskScheduler {

    private RelayServerTaskScheduler() {

    }

    private static class RelayServerTaskSchedulerHolder {

        private static final RelayServerTaskScheduler INSTANCE = new RelayServerTaskScheduler();
    }

    public static RelayServerTaskScheduler getInstance() {
        return RelayServerTaskSchedulerHolder.INSTANCE;
    }

    public ArrayList<RelayServerDTO> getRelayServerList(RelayServerDTO dto) {
        return RelayServerLoader.getInstance().getRelayServerList(dto);
    }

    public ArrayList<RelayServerDTO> getRelayServerList(int columnValue, int sortType, RelayServerDTO dto) {
        return RelayServerLoader.getInstance().getRelayServerList(columnValue, sortType, dto);
    }

    public int getMaxId() {
        RelayServerDAO authServerDAO = new RelayServerDAO();
        return authServerDAO.getMaxId();
    }

    public MyAppError addRelayServerInfo(RelayServerDTO p_dto) {
        RelayServerDAO relayServerDAO = new RelayServerDAO();
        return relayServerDAO.addRelayServerInfo(p_dto);
    }

    public MyAppError addRelayServerInfo(ArrayList<RelayServerDTO> maps) {
        RelayServerDAO relayServerDAO = new RelayServerDAO();
        return relayServerDAO.addRelayServerInfo(maps);
    }

    public RelayServerDTO getRelayServerInfoById(int id) {
        return RelayServerLoader.getInstance().getRelayServerDTO(id);
    }

    public MyAppError deleteRelayServer(int id) {
        RelayServerDAO relayServerDAO = new RelayServerDAO();
        return relayServerDAO.deleteRelayServer(id);
    }

    public MyAppError updateRelayServer(RelayServerDTO p_dto) {
        RelayServerDAO relayServerDAO = new RelayServerDAO();
        return relayServerDAO.updateRelayServer(p_dto);
    }

    public ArrayList<RelayServerDTO> getRelayServerListForDownload() {
        return RelayServerLoader.getInstance().getRelayServerForDownload();
    }

    public RelayServerDTO getRelayServerDTO(RelayServerForm relayServerForm) {
        RelayServerDTO relayServerDTO = new RelayServerDTO();
        relayServerDTO.setServerIP(relayServerForm.getServerIP());

        if (relayServerForm.isNull(relayServerForm.getRelayPort())) {
            relayServerDTO.setRelayPort(9102);
        } else {
            relayServerDTO.setRelayPort(Integer.valueOf(relayServerForm.getRelayPort()));
        }

        if (relayServerForm.isNull(relayServerForm.getLogLevel())) {
            relayServerDTO.setLogLevel("ALL");
        } else {
            relayServerDTO.setLogLevel(relayServerForm.getLogLevel());
        }

        if (relayServerForm.isNull(relayServerForm.getActive())) {
            relayServerDTO.setActive(1);
        } else {
            relayServerDTO.setActive(Integer.valueOf(relayServerForm.getActive()));
        }

        return relayServerDTO;
    }

}
