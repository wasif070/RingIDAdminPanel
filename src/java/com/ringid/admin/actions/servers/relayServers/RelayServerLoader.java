/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.servers.relayServers;

import com.ringid.admin.BaseDAO;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.utils.Constants;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Kazi Nasir Uddin Oly
 * @Date Jun 5, 2016
 */
public class RelayServerLoader extends BaseDAO {

    private final static long LOADING_INTERVAL = 60 * 60 * 1000;
    private long loadingTime = 0;
    private HashMap<Integer, RelayServerDTO> relayServerHashMapList;
    private static RelayServerLoader relayServerLoader = null;
    static Logger logger = RingLogger.getConfigPortalLogger();

    private RelayServerLoader() {

    }

    public static RelayServerLoader getInstance() {
        if (relayServerLoader == null) {
            createLoader();
        }
        return relayServerLoader;
    }

    private synchronized static void createLoader() {
        if (relayServerLoader == null) {
            relayServerLoader = new RelayServerLoader();
        }
    }

    private void forceLoadData() {
        relayServerHashMapList = new HashMap<>();
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            stmt = db.connection.createStatement();
            String sql = "SELECT id, serverIP, relayPort, logLevel, active FROM relay_servers;";
            rs = stmt.executeQuery(sql);
            while (rs.next()) {
                RelayServerDTO dto = new RelayServerDTO();
                dto.setId(rs.getInt("id"));
                dto.setServerIP(rs.getString("serverIP"));
                dto.setRelayPort(rs.getInt("relayPort"));
                dto.setLogLevel(rs.getString("logLevel"));
                dto.setActive(rs.getInt("active"));
                relayServerHashMapList.put(dto.getId(), dto);
            }
            logger.debug(sql + "::" + relayServerHashMapList.size());
        } catch (Exception e) {
            logger.debug("SmsServer List Exception...", e);
        }
    }

    private void checkForReload() {
        long currentTime = System.currentTimeMillis();
        if (currentTime - loadingTime > LOADING_INTERVAL) {
            loadingTime = currentTime;
            forceLoadData();
        }
    }

    private boolean contains(RelayServerDTO dto, String searchText) {
        return (String.valueOf(dto.getId()).contains(searchText))
                || (dto.getServerIP() != null && dto.getServerIP().contains(searchText))
                || (String.valueOf(dto.getRelayPort()).contains(searchText));
    }

    private ArrayList<RelayServerDTO> getRelayServerData(RelayServerDTO sdto) {
        checkForReload();
        ArrayList<RelayServerDTO> data = new ArrayList<>();
        Set set = relayServerHashMapList.entrySet();
        Iterator i = set.iterator();
        while (i.hasNext()) {
            Map.Entry me = (Map.Entry) i.next();
            RelayServerDTO dto = (RelayServerDTO) me.getValue();
            if (sdto.getSearchText() != null && sdto.getSearchText().length() > 0 && !contains(dto, sdto.getSearchText())) {
                continue;
            }
            data.add(dto);
        }
        if (sdto.getSortType() == Constants.DESC_SORT) {
            Collections.sort(data, new RelayServerDTO.CompDSC());
        } else {
            Collections.sort(data, new RelayServerDTO.CompASC());
        }
        return data;
    }

    public synchronized ArrayList<RelayServerDTO> getRelayServerList(RelayServerDTO sdto) {
        ArrayList<RelayServerDTO> relayServerList = getRelayServerData(sdto);
        logger.debug("getRelayServerList Size: " + relayServerList.size());
        return relayServerList;
    }

    public synchronized ArrayList<RelayServerDTO> getRelayServerList(int columnValue, int sortType, RelayServerDTO sdto) {
        ArrayList<RelayServerDTO> relayServerList = getRelayServerData(sdto);
        if (sortType == Constants.DESC_SORT) {
            Collections.sort(relayServerList, new RelayServerDTO.CompDSC());
        } else {
            Collections.sort(relayServerList, new RelayServerDTO.CompASC());
        }
        logger.debug("getRelayServerList Size: " + relayServerList.size());

        return relayServerList;
    }

    public synchronized void forceReload() {
        loadingTime = System.currentTimeMillis();
        forceLoadData();
    }

    public synchronized RelayServerDTO getRelayServerDTO(int id) {
        checkForReload();
        if (relayServerHashMapList.containsKey(id)) {
            return relayServerHashMapList.get(id);
        }
        return null;
    }

    public synchronized ArrayList<RelayServerDTO> getRelayServerForDownload() {
        checkForReload();
        ArrayList<RelayServerDTO> data = new ArrayList<>(relayServerHashMapList.values());
        return data;
    }

}
