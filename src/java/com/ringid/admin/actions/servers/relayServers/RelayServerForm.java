/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.servers.relayServers;

import com.ringid.admin.BaseForm;
import com.ringid.admin.dto.adminAuth.LoginDTO;
import com.ringid.admin.projectMenu.MenuNames;
import com.ringid.admin.utils.Constants;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

/**
 *
 * @author Kazi Nasir Uddin Oly
 * @Date Jun 5, 2016
 */
public class RelayServerForm extends BaseForm {

    private int id;
    private int newId;
    private String serverIP;
    private String relayPort;
    private String logLevel;
    private String active;
    private ArrayList<RelayServerDTO> relayServerList = new ArrayList<RelayServerDTO>();

    public RelayServerForm() {
        super();
    }

    public int getNewId() {
        return newId;
    }

    public void setNewId(int newId) {
        this.newId = newId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getServerIP() {
        return serverIP;
    }

    public void setServerIP(String serverIP) {
        this.serverIP = serverIP;
    }

    public String getRelayPort() {
        return relayPort;
    }

    public void setRelayPort(String relayPort) {
        this.relayPort = relayPort;
    }

    public String getLogLevel() {
        return logLevel;
    }

    public void setLogLevel(String logLevel) {
        this.logLevel = logLevel;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public ArrayList<RelayServerDTO> getRelayServerList() {
        return relayServerList;
    }

    public void setRelayServerList(ArrayList<RelayServerDTO> relayServerList) {
        this.relayServerList = relayServerList;
    }

    @Override
    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        ActionErrors errors = new ActionErrors();
//        LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
//        String FEATURE_NAME = MenuNames.RELAY_SERVER;
//        if (loginDTO == null) {
//            errors.add("auth", new ActionMessage("UnAuthorized Access"));
//        } else if (Constants.ADD == getAction() || Constants.UPDATE == getAction()) {
//            if (!(loginDTO.getPermissionLevel() == Constants.SUPER_ADMIN) && !(loginDTO.getFeauterMapList().get(FEATURE_NAME).getHasAddPermission()
//                    || loginDTO.getFeauterMapList().get(FEATURE_NAME).getHasModifyPermission())) {
//                errors.add("auth", new ActionMessage("UnAuthorized Access"));
//            } else if (getServerIP() == null || getServerIP().length() == 0) {
//                errors.add("serverIP", new ActionMessage("errors.serverIP.required"));
//            }
//        }
        return errors;
    }

}
