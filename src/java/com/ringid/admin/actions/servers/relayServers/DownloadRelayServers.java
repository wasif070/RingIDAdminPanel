/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ringid.admin.actions.servers.relayServers;

import com.ringid.admin.utils.log.RingLogger;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Kazi Nasir Uddin Oly 
 * @Date Jun 7, 2016
 */
@WebServlet(name = "DownloadRelayServers", urlPatterns = {"/Download/DownloadRelayServers"})
public class DownloadRelayServers extends HttpServlet{
    static Logger logger = RingLogger.getConfigPortalLogger();
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/csv");
        response.setHeader("Content-Disposition", "attachment; filename=\"RelayServers.csv\"");
        try{
            OutputStream outputStream = response.getOutputStream();
            ArrayList<RelayServerDTO> list = RelayServerTaskScheduler.getInstance().getRelayServerListForDownload();
            outputStream.write("Server IP".getBytes());
            outputStream.write(",".getBytes());
            outputStream.write("Relay Port".getBytes());
            outputStream.write(",".getBytes());
            outputStream.write("Log Level".getBytes());
            outputStream.write(",".getBytes());
            outputStream.write("Active".getBytes());
            outputStream.write("\n".getBytes());
            for(RelayServerDTO d: list){
                outputStream.write(d.getServerIP().getBytes());
                outputStream.write(",".getBytes());
                outputStream.write(String.valueOf(d.getRelayPort()).getBytes());
                outputStream.write(",".getBytes());
                outputStream.write(d.getLogLevel().getBytes());
                outputStream.write(",".getBytes());
                outputStream.write(String.valueOf(d.getActive()).getBytes());
                outputStream.write("\n".getBytes());
            }
            outputStream.flush();
            outputStream.close();
        } catch(Exception e){
            logger.error("DownloadRelayServers Exception: " + e.toString());
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }

    @Override
    public String getServletInfo() {
        return "Short Description";
    }
}
