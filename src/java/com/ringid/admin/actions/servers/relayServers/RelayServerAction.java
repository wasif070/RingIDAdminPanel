/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.servers.relayServers;

import com.google.gson.Gson;
import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.dto.adminAuth.LoginDTO;
import com.ringid.admin.utils.Constants;
import com.ringid.admin.utils.MyAppError;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author Kazi Nasir Uddin Oly
 * @Date Jun 6, 2016
 */
public class RelayServerAction extends BaseAction {

    Logger logger = RingLogger.getConfigPortalLogger();

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = checkAuthentication(mapping, form, request);
        String activistName;
        try {
            LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
            activistName = loginDTO.getUserName();
        } catch (Exception e) {
            target = UNAUTHORIZED;
            return mapping.findForward(target);
        }
        try {
            if (target.equals(SUCCESS)) {
                RelayServerForm relayServerForm = (RelayServerForm) form;
                RelayServerTaskScheduler scheduler = RelayServerTaskScheduler.getInstance();
                RelayServerDTO relayServerDTO = scheduler.getRelayServerDTO(relayServerForm);
                if (relayServerForm.getNewId() <= 0) {
                    int maxId = scheduler.getMaxId();
                    relayServerDTO.setId(maxId + 1);
                }
                RingLogger.getActivityLogger().debug("Action : RelayServerAction[add] activistName : " + activistName + " relayServerDTO --> " + new Gson().toJson(relayServerDTO));
                MyAppError error = scheduler.addRelayServerInfo(relayServerDTO);
                if (error.getERROR_TYPE() > 0) {
                    if (error.getERROR_TYPE() == MyAppError.OTHERERROR) {
                        logger.error("RelayServerAction Other error " + error.getERROR_TYPE());
                        relayServerForm.setMessage(error.getErrorMessage());
                        request.getSession(true).setAttribute("error", "2");
                    }
                    target = FAILURE;
                } else {
                    RelayServerLoader.getInstance().forceReload();
//                    notifyAuthServers(Constants.RELOAD_RELAY_SERVERS, null);
                }
            }
        } catch (Exception e) {
            target = FAILURE;
        }
        return mapping.findForward(target);
    }

}
