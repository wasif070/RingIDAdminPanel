/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.servers.relayServers;

import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.Constants;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author Kazi Nasir Uddin Oly
 * @Date Jun 6, 2016
 */
public class RelayServerListAction extends BaseAction {

    ArrayList<RelayServerDTO> list = new ArrayList<>();

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = FAILURE;
        try {
            RelayServerForm relayServerForm = (RelayServerForm) form;
            RelayServerDTO relayServerDTO = new RelayServerDTO();
            RelayServerTaskScheduler scheduler = RelayServerTaskScheduler.getInstance();

            if (request.getParameter("ForceReload") != null && "1".equals(request.getParameter("ForceReload"))) {
                RelayServerLoader.getInstance().forceReload();
            }
            if (relayServerForm.getSearchText() != null && relayServerForm.getSearchText().length() > 0) {
                relayServerDTO.setSearchText(relayServerForm.getSearchText().trim().toLowerCase());
            }
            if (relayServerForm.getColumn() <= 0) {
                relayServerForm.setColumn(Constants.COLUMN_ONE);
                relayServerForm.setSort(Constants.DESC_SORT);
            }
            relayServerDTO.setColumn(relayServerForm.getColumn());
            relayServerDTO.setSortType(relayServerForm.getSort());
            list = scheduler.getRelayServerList(relayServerDTO);
            request.getSession(true).setAttribute(Constants.DATA_ROWS, list);
            managePages(request, relayServerForm, list.size());
            request.getSession(true).setAttribute("search", relayServerDTO.getServerIP());
        } catch (NullPointerException e) {
            target = FAILURE;
        }
        return mapping.findForward(target);
    }

}
