/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.servers.authServers;

import com.ringid.admin.BaseDAO;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.utils.MyAppError;
import java.sql.SQLException;
import java.util.ArrayList;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Kazi Nasir Uddin Oly
 * @Date Jun 11, 2016
 */
public class AuthServerDAO extends BaseDAO {

    static Logger logger = RingLogger.getConfigPortalLogger();

    public AuthServerDAO() {

    }

    public int getMaxId() {
        int id = 0;
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            stmt = db.connection.createStatement();
            String query = "SELECT max(id) AS maxId FROM authservers;";
            rs = stmt.executeQuery(query);
            if (rs.next()) {
                id = rs.getInt("maxId");
            }
        } catch (Exception e) {
            logger.error("Exception in AuthSeverDAO[getMaxId] -> " + e);
        } finally {
            close();
        }
        return id;
    }

    public MyAppError addAuthServerInfo(AuthServerDTO dto) {
        MyAppError error = new MyAppError();
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            stmt = db.connection.createStatement();
            String query = "INSERT INTO authservers(id, startRingID, endRingID, serverID,"
                    + "serverIP, startPort, endPort, relayPort, threadPoolSize,"
                    + "logLevel, currentRegitration, liveSessions, maxRegitration,"
                    + "lastPacketID, playingRole, serverStatus)"
                    + " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
            ps = db.connection.prepareStatement(query);
            int i = 1;
            ps.setInt(i++, dto.getId());
            ps.setString(i++, dto.getStartRingID());
            ps.setString(i++, dto.getEndRingID());
            ps.setInt(i++, dto.getServerID());
            ps.setString(i++, dto.getServerIP());
            ps.setInt(i++, dto.getStartPort());
            ps.setInt(i++, dto.getEndPort());
            ps.setInt(i++, dto.getRelayPort());
            ps.setInt(i++, dto.getThreadPoolSize());
            ps.setString(i++, dto.getLogLevel());
            ps.setInt(i++, dto.getCurrentRegistration());
            ps.setInt(i++, dto.getLiveSessions());
            ps.setInt(i++, dto.getMaxRegistration());
            ps.setInt(i++, dto.getLastPacketID());
            ps.setInt(i++, dto.getPlayingRole());
            ps.setInt(i++, dto.getServerStatus());
            ps.execute();
            ps.close();
        } catch (SQLException e) {
            error.setERROR_TYPE(MyAppError.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            logger.debug("Single addAuthServerInfo SQL Exception --> " + e);
        } catch (Exception e) {
            error.setERROR_TYPE(MyAppError.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            logger.debug("Single addAuthServerInfo Exception --> " + e);
        } finally {
            close();
        }
        return error;
    }

    public MyAppError addAuthServerInfo(ArrayList<AuthServerDTO> maps) {
        MyAppError error = new MyAppError();
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            String query = "INSERT INTO authservers (id, startRingID, endRingID, serverID,"
                    + "     serverIP, startPort, endPort, relayPort, threadPoolSize,"
                    + "     logLevel, currentRegitration, liveSessions, maxRegitration,"
                    + "     lastPacketID, playingRole, serverStatus)                                         "
                    + "         SELECT * FROM (SELECT ? AS c0, SELECT ? AS c1, ? AS c2, ? AS c3, ? AS c4, ? AS c5, ? AS c6, ? AS c7, ? AS c8,"
                    + "          ? AS c9, ? AS c10, ? AS c11, ? AS c12, ? AS c13, ? AS c14, ? AS c15 ) AS tmp                                                                       "
                    + "         WHERE NOT EXISTS (                                                                                          "
                    + "         SELECT serverIP FROM authservers WHERE serverIP = ?"
                    + "         );";
            ps = db.connection.prepareStatement(query);
            int i = 1;
            for (AuthServerDTO entry : maps) {
                int index = 1;
                ps.setInt(index++, entry.getId());
                ps.setString(index++, entry.getStartRingID());
                ps.setString(index++, entry.getEndRingID());
                ps.setInt(index++, entry.getServerID());
                ps.setString(index++, entry.getServerIP());
                ps.setInt(index++, entry.getStartPort());
                ps.setInt(index++, entry.getEndPort());
                ps.setInt(index++, entry.getRelayPort());
                ps.setInt(index++, entry.getThreadPoolSize());
                ps.setString(index++, entry.getLogLevel());
                ps.setInt(index++, entry.getCurrentRegistration());
                ps.setInt(index++, entry.getLiveSessions());
                ps.setInt(index++, entry.getMaxRegistration());
                ps.setInt(index++, entry.getLastPacketID());
                ps.setInt(index++, entry.getPlayingRole());
                ps.setInt(index++, entry.getServerStatus());
                ps.setString(index++, entry.getServerIP());
                ps.addBatch();
                i++;
                if ((i + 1) % 1000 == 0) {
                    ps.executeBatch();
                }
            }
            int a[] = ps.executeBatch();
        } catch (SQLException e) {
            error.setERROR_TYPE(MyAppError.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            logger.debug("addAuthServerInfo Map SQL Exception--> " + e);
        } catch (Exception e) {
            error.setERROR_TYPE(MyAppError.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            logger.debug("addAuthServerInfo Map Exception --> " + e);
        } finally {
            close();
        }
        return error;
    }

    public MyAppError updateAuthServer(AuthServerDTO dto) {
        MyAppError error = new MyAppError();
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            String query = "UPDATE authservers SET id = ?,  startRingID = ?, endRingID = ?, serverID = ?,"
                    + "     serverIP = ?, startPort = ?, endPort = ?, relayPort = ?, threadPoolSize = ?,"
                    + "     logLevel = ?, currentRegitration = ?, liveSessions = ?, maxRegitration = ?,"
                    + "     lastPacketID = ?, playingRole = ?, serverStatus = ? "
                    + "     WHERE id = ?;";
            ps = db.connection.prepareStatement(query);
            int index = 1;
            ps.setInt(index++, dto.getNewId());
            ps.setString(index++, dto.getStartRingID());
            ps.setString(index++, dto.getEndRingID());
            ps.setInt(index++, dto.getServerID());
            ps.setString(index++, dto.getServerIP());
            ps.setInt(index++, dto.getStartPort());
            ps.setInt(index++, dto.getEndPort());
            ps.setInt(index++, dto.getRelayPort());
            ps.setInt(index++, dto.getThreadPoolSize());
            ps.setString(index++, dto.getLogLevel());
            ps.setInt(index++, dto.getCurrentRegistration());
            ps.setInt(index++, dto.getLiveSessions());
            ps.setInt(index++, dto.getMaxRegistration());
            ps.setInt(index++, dto.getLastPacketID());
            ps.setInt(index++, dto.getPlayingRole());
            ps.setInt(index++, dto.getServerStatus());
            ps.setInt(index++, dto.getId());
            ps.execute();
        } catch (SQLException e) {
            error.setERROR_TYPE(error.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            logger.debug("updateAuthServer SQL Exception --> " + e);
        } catch (Exception e) {
            error.setERROR_TYPE(error.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            logger.debug("updateAuthServer Exception --> " + e);
        } finally {
            close();
        }
        return error;
    }

    public MyAppError deleteAuthServer(int id) {
        MyAppError error = new MyAppError();
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            stmt = db.connection.createStatement();
            String sql = "DELETE FROM authservers WHERE id = " + id + ";";
            int a = stmt.executeUpdate(sql);
            logger.debug("delete success");
        } catch (SQLException e) {
            error.setERROR_TYPE(MyAppError.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            logger.debug("deleteAuthServer SQL Exception --> " + e);
        } catch (Exception e) {
            error.setERROR_TYPE(MyAppError.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            logger.debug("deleteAuthServer Exception --> " + e);
        } finally {
            close();
        }
        return error;
    }
}
