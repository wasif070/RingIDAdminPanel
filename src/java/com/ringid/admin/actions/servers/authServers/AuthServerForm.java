/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.servers.authServers;

import com.ringid.admin.BaseForm;
import com.ringid.admin.dto.adminAuth.LoginDTO;
import com.ringid.admin.projectMenu.MenuNames;
import com.ringid.admin.utils.Constants;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

/**
 *
 * @author Kazi Nasir Uddin Oly
 * @Date Jun 11, 2016
 */
public class AuthServerForm extends BaseForm {

    private int id;
    private int newId;
    private String startRingID;
    private String endRingID;
    private String serverID;
    private String serverIP;
    private String startPort;
    private String endPort;
    private String relayPort;
    private String threadPoolSize;
    private String logLevel;
    private String currentRegistration;
    private String liveSessions;
    private String maxRegistration;
    private String lastPacketID;
    private String playingRole;
    private String serverStatus;

    private ArrayList<AuthServerDTO> authServerDTOs = new ArrayList<>();

    public AuthServerForm() {
        super();
    }

    public int getNewId() {
        return newId;
    }

    public void setNewId(int newId) {
        this.newId = newId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStartRingID() {
        return startRingID;
    }

    public void setStartRingID(String startRingID) {
        this.startRingID = startRingID;
    }

    public String getEndRingID() {
        return endRingID;
    }

    public void setEndRingID(String endRingID) {
        this.endRingID = endRingID;
    }

    public String getServerID() {
        return serverID;
    }

    public void setServerID(String serverID) {
        this.serverID = serverID;
    }

    public String getServerIP() {
        return serverIP;
    }

    public void setServerIP(String serverIP) {
        this.serverIP = serverIP;
    }

    public String getStartPort() {
        return startPort;
    }

    public void setStartPort(String startPort) {
        this.startPort = startPort;
    }

    public String getEndPort() {
        return endPort;
    }

    public void setEndPort(String endPort) {
        this.endPort = endPort;
    }

    public String getRelayPort() {
        return relayPort;
    }

    public void setRelayPort(String relayPort) {
        this.relayPort = relayPort;
    }

    public String getThreadPoolSize() {
        return threadPoolSize;
    }

    public void setThreadPoolSize(String threadPoolSize) {
        this.threadPoolSize = threadPoolSize;
    }

    public String getLogLevel() {
        return logLevel;
    }

    public void setLogLevel(String logLevel) {
        this.logLevel = logLevel;
    }

    public String getCurrentRegistration() {
        return currentRegistration;
    }

    public void setCurrentRegistration(String currentRegistration) {
        this.currentRegistration = currentRegistration;
    }

    public String getLiveSessions() {
        return liveSessions;
    }

    public void setLiveSessions(String liveSessions) {
        this.liveSessions = liveSessions;
    }

    public String getMaxRegistration() {
        return maxRegistration;
    }

    public void setMaxRegistration(String maxRegistration) {
        this.maxRegistration = maxRegistration;
    }

    public String getLastPacketID() {
        return lastPacketID;
    }

    public void setLastPacketID(String lastPacketID) {
        this.lastPacketID = lastPacketID;
    }

    public String getPlayingRole() {
        return playingRole;
    }

    public void setPlayingRole(String playingRole) {
        this.playingRole = playingRole;
    }

    public String getServerStatus() {
        return serverStatus;
    }

    public void setServerStatus(String serverStatus) {
        this.serverStatus = serverStatus;
    }

    public ArrayList<AuthServerDTO> getAuthServerDTOs() {
        return authServerDTOs;
    }

    public void setAuthServerDTOs(ArrayList<AuthServerDTO> authServerDTOs) {
        this.authServerDTOs = authServerDTOs;
    }

    @Override
    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        ActionErrors errors = new ActionErrors();
        LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        String FEATURE_NAME = MenuNames.AUTH_SERVER;
        if (loginDTO == null) {
            errors.add("auth", new ActionMessage("UnAuthorized Access"));
        } else if (Constants.ADD == getAction() || Constants.UPDATE == getAction()) {
            if (!(loginDTO.getPermissionLevel() == Constants.SUPER_ADMIN) && !(loginDTO.getFeauterMapList().get(FEATURE_NAME).getHasAddPermission()
                    || loginDTO.getFeauterMapList().get(FEATURE_NAME).getHasModifyPermission())) {
                errors.add("auth", new ActionMessage("UnAuthorized Access"));
            } else if (getServerIP() == null || getServerIP().length() < 1) {
                errors.add("serverIP", new ActionMessage("errors.serverIP.required"));
            }
        }
        return errors;
    }

}
