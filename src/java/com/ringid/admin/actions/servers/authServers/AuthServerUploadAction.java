/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.servers.authServers;

import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.utils.Constants;
import com.ringid.admin.utils.MyAppError;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author Kazi Nasir Uddin Oly
 * @Date Jun 11, 2016
 */
public class AuthServerUploadAction extends BaseAction {

    static Logger logger = RingLogger.getConfigPortalLogger();

    String line = "";
    String cvsSplitBy = ",";

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = checkAuthentication(mapping, form, request);
        if (target.equals(SUCCESS)) {
            String[] dataArray = getDataFromFile(form);
            try {
                ArrayList<AuthServerDTO> authServerList = new ArrayList<>();

                for (String s : dataArray) {
                    String[] authServer = s.split(cvsSplitBy);
                    String startRingID = authServer[0];
                    String endRingID = authServer[1];
                    String serverID = authServer[2];
                    String serverIP = authServer[3];
                    String startPort = authServer[4];
                    String endPort = authServer[5];
                    String relayPort = authServer[6];
                    String threadPoolSize = authServer[7];
                    String logLevel = authServer[8];
                    String currentRegistration = authServer[9];
                    String liveSessions = authServer[10];
                    String maxRegistration = authServer[11];
                    String lastPacketID = authServer[12];
                    String playingRoleString = authServer[13];
                    String serverStatus = authServer[14];
                    if (startRingID.equalsIgnoreCase("Start Ring ID")) {
                        continue;
                    }

                    AuthServerDTO authServerDTO = new AuthServerDTO();
                    authServerDTO.setStartRingID(startRingID);
                    authServerDTO.setEndRingID(endRingID);
                    authServerDTO.setServerID(Integer.valueOf(serverID));
                    authServerDTO.setServerIP(serverIP);
                    authServerDTO.setStartPort(Integer.valueOf(startPort));
                    authServerDTO.setEndPort(Integer.valueOf(endPort));
                    authServerDTO.setRelayPort(Integer.valueOf(relayPort));
                    authServerDTO.setThreadPoolSize(Integer.valueOf(threadPoolSize));
                    authServerDTO.setLogLevel(logLevel);
                    authServerDTO.setCurrentRegistration(Integer.valueOf(currentRegistration));
                    authServerDTO.setLiveSessions(Integer.valueOf(liveSessions));
                    authServerDTO.setMaxRegistration(Integer.valueOf(maxRegistration));
                    authServerDTO.setLastPacketID(Integer.valueOf(lastPacketID));
                    authServerDTO.setPlayingRole(Integer.valueOf(playingRoleString));
                    authServerDTO.setServerStatus(Integer.valueOf(serverStatus));

                    authServerList.add(authServerDTO);
                }

                MyAppError error = AuthServerTaskScheduler.getInstance().addAuthServerInfo(authServerList);
                if (error.getERROR_TYPE() > 0) {
                    target = FAILURE;
                } else {
                    AuthServerLoader.getInstance().forceReload();
                    notifyAuthServers(Constants.RELOAD_AUTH_SERVERS, null);
                }
            } catch (Exception e) {
                logger.error("AuthServerUploadAction: " + e.getMessage());
                target = FAILURE;
            }
        }
        return mapping.findForward(target);
    }
}
