/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.servers.authServers;

import com.ringid.admin.BaseDAO;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.utils.Constants;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import org.apache.logging.log4j.Logger;
import ringid.command.CommandTaskScheduler;

/**
 *
 * @author Kazi Nasir Uddin Oly
 * @Date Jun 11, 2016
 */
public class AuthServerLoader extends BaseDAO {

    private final static long LOADING_INTERVAL = 60 * 60 * 1000;
    private long loadingTime = 0;
    private HashMap<Integer, AuthServerDTO> authServerHashMapList;
    private Logger logger = RingLogger.getConfigPortalLogger();
    private final HashMap<Integer, String> maxGeneratedRingId;
    private static boolean loadMaxGeneratedRingid;

    private AuthServerLoader() {
        loadMaxGeneratedRingid = true;
        maxGeneratedRingId = new HashMap<>();
    }

    private static class AuthServerLoaderHolder {

        private static final AuthServerLoader INSTANCE = new AuthServerLoader();
    }

    public static AuthServerLoader getInstance() {
        return AuthServerLoaderHolder.INSTANCE;
    }

    private void forceLoadData() {
        authServerHashMapList = new HashMap<>();
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            stmt = db.connection.createStatement();
            String sql = "SELECT id, startRingID, endRingID, serverID,"
                    + "serverIP, startPort, endPort, relayPort, threadPoolSize,"
                    + "logLevel, currentRegitration, liveSessions, maxRegitration,"
                    + "lastPacketID, playingRole, serverStatus FROM authservers;";
            rs = stmt.executeQuery(sql);
            while (rs.next()) {
                AuthServerDTO dto = new AuthServerDTO();
                dto.setId(rs.getInt("id"));
                dto.setStartRingID(rs.getString("startRingID"));
                dto.setEndRingID(rs.getString("endRingID"));
                dto.setServerID(rs.getInt("serverID"));
                dto.setServerIP(rs.getString("serverIP"));
                dto.setStartPort(rs.getInt("startPort"));
                dto.setEndPort(rs.getInt("endPort"));
                dto.setRelayPort(rs.getInt("relayPort"));
                dto.setThreadPoolSize(rs.getInt("threadPoolSize"));
                dto.setLogLevel(rs.getString("logLevel"));
                dto.setCurrentRegistration(rs.getInt("currentRegitration"));
                dto.setLiveSessions(rs.getInt("liveSessions"));
                dto.setMaxRegistration(rs.getInt("maxRegitration"));
                dto.setLastPacketID(rs.getInt("lastPacketID"));
                dto.setPlayingRole(rs.getInt("playingRole"));
                dto.setServerStatus(rs.getInt("serverStatus"));
                if (maxGeneratedRingId.containsKey(dto.getServerID())) {
                    dto.setMaxGeneratedRingId(maxGeneratedRingId.get(dto.getServerID()));
                }
                authServerHashMapList.put(dto.getId(), dto);
            }
            loadMaxGeneratedRingId();
            logger.debug(sql + "::" + authServerHashMapList.size());

        } catch (Exception e) {
            logger.debug("Auth Server List Exception...", e);
        } finally {
            close();
        }
    }

    private void checkForReload() {
        long currentTime = System.currentTimeMillis();
        if (currentTime - loadingTime > LOADING_INTERVAL) {
            loadingTime = currentTime;
            loadMaxGeneratedRingid = true;
            forceLoadData();
        }
    }

    public synchronized void forceReload() {
        loadingTime = System.currentTimeMillis();
        forceLoadData();
    }

    private void loadMaxGeneratedRingId() {
        Runnable runnable = () -> {
            try {
                if (loadMaxGeneratedRingid) {
                    for (Map.Entry<Integer, AuthServerDTO> entry : authServerHashMapList.entrySet()) {
                        AuthServerDTO dto = entry.getValue();
                        dto.setMaxGeneratedRingId(CommandTaskScheduler.getInstance().getMaxGeneratedRingId(Constants.USERID, dto.getServerIP(), dto.getStartPort()));
                        maxGeneratedRingId.put(dto.getServerID(), dto.getMaxGeneratedRingId());
                    }
                    loadMaxGeneratedRingid = false;
                }
            } catch (Exception e) {
                logger.error("Exception in loadMaxGeneratedRingId -> " + e);
            }
        };
        new Thread(runnable).start();
    }

    public void loadMaxGeneratedRingId(int serverId) {
        if (authServerHashMapList.containsKey(serverId)) {
            try {
                AuthServerDTO dto = authServerHashMapList.get(serverId);
                dto.setMaxGeneratedRingId(CommandTaskScheduler.getInstance().getMaxGeneratedRingId(Constants.USERID, dto.getServerIP(), dto.getStartPort()));
                maxGeneratedRingId.put(dto.getServerID(), dto.getMaxGeneratedRingId());
            } catch (Exception e) {
            }
        }
    }

    private boolean contains(AuthServerDTO dto, String searchText) {
        return (String.valueOf(dto.getId()).contains(searchText))
                || (String.valueOf(dto.getStartRingID()).contains(searchText))
                || (String.valueOf(dto.getEndRingID()).contains(searchText))
                || (dto.getMaxGeneratedRingId() != null && dto.getMaxGeneratedRingId().trim().contains(searchText))
                || (dto.getServerIP().contains(searchText));
    }

    private ArrayList<AuthServerDTO> getAuthServerData(AuthServerDTO sdto) {
        checkForReload();
        ArrayList<AuthServerDTO> data = new ArrayList<>();
        Set set = authServerHashMapList.entrySet();
        Iterator i = set.iterator();
        while (i.hasNext()) {
            Map.Entry me = (Map.Entry) i.next();
            AuthServerDTO dto = (AuthServerDTO) me.getValue();
            if (sdto.getSearchText() != null && sdto.getSearchText().length() > 0 && !contains(dto, sdto.getSearchText())) {
                continue;
            }
            data.add(dto);
        }
        if (sdto.getSortType() == Constants.DESC_SORT) {
            Collections.sort(data, new AuthServerDTO.CompDSC());
        } else {
            Collections.sort(data, new AuthServerDTO.CompASC());
        }
        return data;
    }

    public synchronized ArrayList<AuthServerDTO> getAuthServerList(AuthServerDTO sdto) {
        ArrayList<AuthServerDTO> authServerList = getAuthServerData(sdto);
        logger.debug("AuthServerList Size: " + authServerList.size());
        return authServerList;
    }

    public synchronized ArrayList<AuthServerDTO> getAuthServerList(int columnValue, int sortType, AuthServerDTO sdto) {
        ArrayList<AuthServerDTO> authServerList = getAuthServerData(sdto);
        switch (columnValue) {
            case Constants.COLUMN_ONE:
                Collections.sort(authServerList, (sortType == Constants.ASC_SORT ? new AuthServerDTO.CompASC() : new AuthServerDTO.CompASC()));
                break;
            default:
                Collections.sort(authServerList, new AuthServerDTO.CompDSC());
        }
        logger.debug("AuthServerList Size: " + authServerList.size());
        return authServerList;
    }

    public synchronized ArrayList<AuthServerDTO> getAuthServerListForDownload() {
        checkForReload();
        ArrayList<AuthServerDTO> data = new ArrayList<>(authServerHashMapList.values());
        return data;
    }

    public synchronized AuthServerDTO getAuthServerDTO(int id) {
        checkForReload();
        if (authServerHashMapList.containsKey(id)) {
            return authServerHashMapList.get(id);
        }
        return null;
    }

    public AuthServerDTO getAuthServerDTOByServerID(int serverId) {
        checkForReload();
        AuthServerDTO dto = new AuthServerDTO();
        for (Map.Entry<Integer, AuthServerDTO> entry : authServerHashMapList.entrySet()) {
            if (entry.getValue().getServerID() == serverId) {
                dto = entry.getValue();
                break;
            }
        }
        return dto;
    }
}
