/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.servers.authServers;

import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.Constants;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author Kazi Nasir Uddin Oly
 * @Date Jun 11, 2016
 */
public class AuthServerListAction extends BaseAction {

    ArrayList<AuthServerDTO> list = new ArrayList<>();

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = SUCCESS;
        try {
            AuthServerForm authServerForm = (AuthServerForm) form;
            AuthServerDTO authServerDTO = new AuthServerDTO();
            AuthServerTaskScheduler scheduler = AuthServerTaskScheduler.getInstance();

            if (request.getParameter("ForceReload") != null && "1".equals(request.getParameter("ForceReload"))) {
                AuthServerLoader.getInstance().forceReload();
            }
            if (request.getParameter("ReloadMaxRingID") != null && Integer.valueOf(request.getParameter("ReloadMaxRingID")) > 0) {
                scheduler.loadMaxGeneratedRingId(Integer.valueOf(request.getParameter("ReloadMaxRingID")));
            }
            if (authServerForm.getSearchText() != null && authServerForm.getSearchText().length() > 0) {
                authServerDTO.setSearchText(authServerForm.getSearchText().trim().toLowerCase());
            }
            if (authServerForm.getColumn() <= 0) {
                authServerForm.setColumn(Constants.COLUMN_ONE);
                authServerForm.setSort(Constants.DESC_SORT);
            }
            authServerDTO.setColumn(authServerForm.getColumn());
            authServerDTO.setSortType(authServerForm.getSort());
            list = scheduler.getAuthServerList(authServerDTO);
            request.getSession(true).setAttribute(Constants.DATA_ROWS, list);
            managePages(request, authServerForm, list.size());
        } catch (NullPointerException e) {
            target = FAILURE;
        }
        return mapping.findForward(target);
    }
}
