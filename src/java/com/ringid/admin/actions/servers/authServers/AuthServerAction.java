/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.servers.authServers;

import com.google.gson.Gson;
import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.dto.adminAuth.LoginDTO;
import com.ringid.admin.utils.Constants;
import com.ringid.admin.utils.MyAppError;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author Kazi Nasir Uddin Oly
 * @Date Jun 11, 2016
 */
public class AuthServerAction extends BaseAction {

    Logger logger = RingLogger.getConfigPortalLogger();

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = checkAuthentication(mapping, form, request);
        String activistName;
        try {
            LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
            activistName = loginDTO.getUserName();
        } catch (Exception e) {
            target = UNAUTHORIZED;
            return mapping.findForward(target);
        }
        try {
            if (target.equals(SUCCESS)) {
                AuthServerForm authServerForm = (AuthServerForm) form;
                AuthServerTaskScheduler scheduler = AuthServerTaskScheduler.getInstance();
                AuthServerDTO authServerDTO = scheduler.getAuthServerDTO(authServerForm);
                if (authServerForm.getNewId() <= 0) {
                    int maxId = scheduler.getMaxId();
                    authServerDTO.setId(maxId + 1);
                }
                RingLogger.getActivityLogger().debug("Action : AuthServerAction[add] activistName : " + activistName + " authServerDTO --> " + new Gson().toJson(authServerDTO));
                MyAppError error = scheduler.addAuthServerInfo(authServerDTO);
                if (error.getERROR_TYPE() > 0) {
                    if (error.getERROR_TYPE() == MyAppError.OTHERERROR) {
                        logger.error("Other error in addAuthServerInfo " + error.getERROR_TYPE());
                        authServerForm.setMessage(error.getErrorMessage());
                        request.getSession(true).setAttribute("error", "2");
                    }
                    target = FAILURE;
                } else {
                    AuthServerLoader.getInstance().forceReload();
                    notifyAuthServers(Constants.RELOAD_AUTH_SERVERS, null);
                }
            }
        } catch (Exception e) {
            target = FAILURE;
        }
        return mapping.findForward(target);
    }
}
