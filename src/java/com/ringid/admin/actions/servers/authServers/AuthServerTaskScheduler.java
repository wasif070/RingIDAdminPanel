/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.servers.authServers;

import com.ringid.admin.utils.MyAppError;
import java.util.ArrayList;

/**
 *
 * @author Kazi Nasir Uddin Oly
 * @Date Jun 11, 2016
 */
public class AuthServerTaskScheduler {

    private AuthServerTaskScheduler() {

    }

    private static class AuthServerTaskSchedulerHolder {

        private static final AuthServerTaskScheduler SCHEDULER = new AuthServerTaskScheduler();
    }

    public static AuthServerTaskScheduler getInstance() {
        return AuthServerTaskSchedulerHolder.SCHEDULER;
    }

    public void loadMaxGeneratedRingId(int serverId) {
        AuthServerLoader.getInstance().loadMaxGeneratedRingId(serverId);
    }

    public ArrayList<AuthServerDTO> getAuthServerList(AuthServerDTO dto) {
        return AuthServerLoader.getInstance().getAuthServerList(dto);
    }

    public ArrayList<AuthServerDTO> getAuthServerList(int columnValue, int sortType, AuthServerDTO dto) {
        return AuthServerLoader.getInstance().getAuthServerList(columnValue, sortType, dto);
    }

    public int getMaxId() {
        AuthServerDAO authServerDAO = new AuthServerDAO();
        return authServerDAO.getMaxId();
    }

    public MyAppError addAuthServerInfo(AuthServerDTO p_dto) {
        AuthServerDAO authServerDAO = new AuthServerDAO();
        return authServerDAO.addAuthServerInfo(p_dto);
    }

    public MyAppError addAuthServerInfo(ArrayList<AuthServerDTO> maps) {
        AuthServerDAO authServerDAO = new AuthServerDAO();
        return authServerDAO.addAuthServerInfo(maps);
    }

    public AuthServerDTO getAuthServerInfoById(int id) {
        return AuthServerLoader.getInstance().getAuthServerDTO(id);
    }

    public MyAppError deleteAuthServer(int id) {
        AuthServerDAO authServerDAO = new AuthServerDAO();
        return authServerDAO.deleteAuthServer(id);
    }

    public MyAppError updateAuthServer(AuthServerDTO p_dto) {
        AuthServerDAO authServerDAO = new AuthServerDAO();
        return authServerDAO.updateAuthServer(p_dto);
    }

    public ArrayList<AuthServerDTO> getAuthServerListForDownload() {
        return AuthServerLoader.getInstance().getAuthServerListForDownload();
    }

    public AuthServerDTO getAuthServerDTO(AuthServerForm authServerForm) {
        AuthServerDTO authServerDTO = new AuthServerDTO();

        authServerDTO.setId(authServerForm.getId());

        if (!authServerForm.isNull(authServerForm.getStartRingID())) {
            authServerDTO.setStartRingID(authServerForm.getStartRingID());
        }

        if (!authServerForm.isNull(authServerForm.getEndRingID())) {
            authServerDTO.setEndRingID(authServerForm.getEndRingID());
        }

        if (!authServerForm.isNull(authServerForm.getServerID())) {
            authServerDTO.setServerID(Integer.valueOf(authServerForm.getServerID()));
        }

        authServerDTO.setServerIP(authServerForm.getServerIP());

        if (authServerForm.isNull(authServerForm.getStartPort())) {
            authServerDTO.setStartPort(30000);
        } else {
            authServerDTO.setStartPort(Integer.valueOf(authServerForm.getStartPort()));
        }

        if (authServerForm.isNull(authServerForm.getEndPort())) {
            authServerDTO.setEndPort(30100);
        } else {
            authServerDTO.setEndPort(Integer.valueOf(authServerForm.getEndPort()));
        }

        if (!authServerForm.isNull(authServerForm.getRelayPort())) {
            authServerDTO.setRelayPort(Integer.valueOf(authServerForm.getRelayPort()));
        }

        if (authServerForm.isNull(authServerForm.getThreadPoolSize())) {
            authServerDTO.setThreadPoolSize(100);
        } else {
            authServerDTO.setThreadPoolSize(Integer.valueOf(authServerForm.getThreadPoolSize()));
        }

        if (authServerForm.isNull(authServerForm.getLogLevel())) {
            authServerDTO.setLogLevel("ALL");
        } else {
            authServerDTO.setLogLevel(authServerForm.getLogLevel());
        }

        if (!authServerForm.isNull(authServerForm.getCurrentRegistration())) {
            authServerDTO.setCurrentRegistration(Integer.valueOf(authServerForm.getCurrentRegistration()));
        }

        if (!authServerForm.isNull(authServerForm.getLiveSessions())) {
            authServerDTO.setLiveSessions(Integer.valueOf(authServerForm.getLiveSessions()));
        }

        if (authServerForm.isNull(authServerForm.getMaxRegistration())) {
            authServerDTO.setMaxRegistration(10000);
        } else {
            authServerDTO.setMaxRegistration(Integer.valueOf(authServerForm.getMaxRegistration()));
        }

        if (!authServerForm.isNull(authServerForm.getLastPacketID())) {
            authServerDTO.setLastPacketID(Integer.valueOf(authServerForm.getLastPacketID()));
        }

        if (authServerForm.isNull(authServerForm.getPlayingRole())) {
            authServerDTO.setPlayingRole(1);
        } else {
            authServerDTO.setPlayingRole(Integer.valueOf(authServerForm.getPlayingRole()));
        }

        if (authServerForm.isNull(authServerForm.getServerStatus())) {
            authServerDTO.setServerStatus(1);
        } else {
            authServerDTO.setServerStatus(Integer.valueOf(authServerForm.getServerStatus()));
        }

        return authServerDTO;
    }
}
