/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ringid.admin.actions.servers.authServers;

import com.ringid.admin.utils.log.RingLogger;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Kazi Nasir Uddin Oly 
 * @Date Jun 11, 2016
 */

@WebServlet(name = "DownloadAuthServers", urlPatterns = {"/Download/DownloadAuthServers"})
public class DownloadAuthServers extends HttpServlet{
    static Logger logger = RingLogger.getConfigPortalLogger();

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/csv");
        response.setHeader("Content-Disposition", "attachment; filename=\"AuthServers.csv\"");
        try{
            OutputStream outputStream = response.getOutputStream();
            ArrayList<AuthServerDTO> list = AuthServerTaskScheduler.getInstance().getAuthServerListForDownload();
            outputStream.write("Start Ring ID".getBytes());
            outputStream.write(",".getBytes());
            outputStream.write("End Ring ID".getBytes());
            outputStream.write(",".getBytes());
            outputStream.write("Server ID".getBytes());
            outputStream.write(",".getBytes());
            outputStream.write("Server IP".getBytes());
            outputStream.write(",".getBytes());
            outputStream.write("Start Port".getBytes());
            outputStream.write(",".getBytes());
            outputStream.write("End Port".getBytes());
            outputStream.write(",".getBytes());
            outputStream.write("Relay Port".getBytes());
            outputStream.write(",".getBytes());
            outputStream.write("Thread Pool Size".getBytes());
            outputStream.write(",".getBytes());
            outputStream.write("Log Level".getBytes());
            outputStream.write(",".getBytes());
            outputStream.write("Current Registration".getBytes());
            outputStream.write(",".getBytes());
            outputStream.write("Live Sessions".getBytes());
            outputStream.write(",".getBytes());
            outputStream.write("Maximum Registration".getBytes());
            outputStream.write(",".getBytes());
            outputStream.write("Last Packet ID".getBytes());
            outputStream.write(",".getBytes());
            outputStream.write("Playing Role".getBytes());
            outputStream.write(",".getBytes());
            outputStream.write("Server Status".getBytes());
            outputStream.write("\n".getBytes());
            for(AuthServerDTO d: list){
                outputStream.write(d.getStartRingID().getBytes());
                outputStream.write(",".getBytes());
                outputStream.write(d.getEndRingID().getBytes());
                outputStream.write(",".getBytes());
                outputStream.write(String.valueOf(d.getServerID()).getBytes());
                outputStream.write(",".getBytes());
                outputStream.write(d.getServerIP().getBytes());
                outputStream.write(",".getBytes());
                outputStream.write(String.valueOf(d.getStartPort()).getBytes());
                outputStream.write(",".getBytes());
                outputStream.write(String.valueOf(d.getEndPort()).getBytes());
                outputStream.write(",".getBytes());
                outputStream.write(String.valueOf(d.getRelayPort()).getBytes());
                outputStream.write(",".getBytes());
                outputStream.write(String.valueOf(d.getThreadPoolSize()).getBytes());
                outputStream.write(",".getBytes());
                outputStream.write(d.getLogLevel().getBytes());
                outputStream.write(",".getBytes());
                outputStream.write(String.valueOf(d.getCurrentRegistration()).getBytes());
                outputStream.write(",".getBytes());
                outputStream.write(String.valueOf(d.getLiveSessions()).getBytes());
                outputStream.write(",".getBytes());
                outputStream.write(String.valueOf(d.getMaxRegistration()).getBytes());
                outputStream.write(",".getBytes());
                outputStream.write(String.valueOf(d.getLastPacketID()).getBytes());
                outputStream.write(",".getBytes());
                outputStream.write(String.valueOf(d.getPlayingRole()).getBytes());
                outputStream.write(",".getBytes());
                outputStream.write(String.valueOf(d.getServerStatus()).getBytes());
                outputStream.write("\n".getBytes());
            }
            outputStream.flush();
            outputStream.close();
        } catch(Exception e){
            logger.error("DownloadAuthServers Exception: " + e.toString());
        }
    }
    
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }

    @Override
    public String getServletInfo() {
        return "Short Description";
    }
}
