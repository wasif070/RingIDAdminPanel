/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.servers.authServers;

import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author Kazi Nasir Uddin Oly
 * @Date Jun 11, 2016
 */
public class AuthServerEditAction extends BaseAction {

    static Logger logger = RingLogger.getConfigPortalLogger();

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        String target = checkAuthentication(mapping, form, request);
        try {
            if (target.equals(SUCCESS)) {
                int id = 0;
                AuthServerForm authServerForm = (AuthServerForm) form;
                if (request.getParameter("id") != null) {
                    id = Integer.parseInt(request.getParameter("id"));
                }
                AuthServerTaskScheduler scheduler = AuthServerTaskScheduler.getInstance();
                AuthServerDTO authServerDTO = scheduler.getAuthServerInfoById(id);
                if (authServerDTO != null) {
                    authServerForm.setNewId(authServerDTO.getId());
                    authServerForm.setId(authServerDTO.getId());
                    authServerForm.setStartRingID(authServerDTO.getStartRingID());
                    authServerForm.setEndRingID(authServerDTO.getEndRingID());
                    authServerForm.setServerID(String.valueOf(authServerDTO.getServerID()));
                    authServerForm.setServerIP(authServerDTO.getServerIP());
                    authServerForm.setStartPort(String.valueOf(authServerDTO.getStartPort()));
                    authServerForm.setEndPort(String.valueOf(authServerDTO.getEndPort()));
                    authServerForm.setRelayPort(String.valueOf(authServerDTO.getRelayPort()));
                    authServerForm.setThreadPoolSize(String.valueOf(authServerDTO.getThreadPoolSize()));
                    authServerForm.setLogLevel(authServerDTO.getLogLevel());
                    authServerForm.setCurrentRegistration(String.valueOf(authServerDTO.getCurrentRegistration()));
                    authServerForm.setLiveSessions(String.valueOf(authServerDTO.getLiveSessions()));
                    authServerForm.setMaxRegistration(String.valueOf(authServerDTO.getMaxRegistration()));
                    authServerForm.setLastPacketID(String.valueOf(authServerDTO.getLastPacketID()));
                    authServerForm.setPlayingRole(String.valueOf(authServerDTO.getPlayingRole()));
                    authServerForm.setServerStatus(String.valueOf(authServerDTO.getServerStatus()));
                    target = SUCCESS;
                }
            }
        } catch (Exception e) {
            logger.error("AuthServerEditAction Exception: " + e);
            target = FAILURE;
        }
        return mapping.findForward(target);
    }
}
