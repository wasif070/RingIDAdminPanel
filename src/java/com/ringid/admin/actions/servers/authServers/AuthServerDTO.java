/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.servers.authServers;

import com.ringid.admin.BaseDTO;
import com.ringid.admin.utils.Constants;
import java.util.Comparator;

/**
 *
 * @author Kazi Nasir Uddin Oly
 * @Date Jun 11, 2016
 */
public class AuthServerDTO extends BaseDTO {

    private int id;
    private int newId;
    private String startRingID;
    private String endRingID;
    private int serverID;
    private String serverIP;
    private int startPort;
    private int endPort;
    private int relayPort;
    private int threadPoolSize;
    private String logLevel;
    private int currentRegistration;
    private int liveSessions;
    private int maxRegistration;
    private int lastPacketID;
    private int playingRole;
    private int serverStatus;
    private String maxGeneratedRingId;

    private boolean isSearchByIP = false;

    public String getMaxGeneratedRingId() {
        return maxGeneratedRingId;
    }

    public void setMaxGeneratedRingId(String maxGeneratedRingId) {
        this.maxGeneratedRingId = maxGeneratedRingId;
    }

    public int getNewId() {
        return newId;
    }

    public void setNewId(int newId) {
        this.newId = newId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStartRingID() {
        return startRingID;
    }

    public void setStartRingID(String startRingID) {
        this.startRingID = startRingID;
    }

    public String getEndRingID() {
        return endRingID;
    }

    public void setEndRingID(String endRingID) {
        this.endRingID = endRingID;
    }

    public int getServerID() {
        return serverID;
    }

    public void setServerID(int serverID) {
        this.serverID = serverID;
    }

    public String getServerIP() {
        return serverIP;
    }

    public void setServerIP(String serverIP) {
        this.serverIP = serverIP;
    }

    public int getStartPort() {
        return startPort;
    }

    public void setStartPort(int startPort) {
        this.startPort = startPort;
    }

    public int getEndPort() {
        return endPort;
    }

    public void setEndPort(int endPort) {
        this.endPort = endPort;
    }

    public int getRelayPort() {
        return relayPort;
    }

    public void setRelayPort(int relayPort) {
        this.relayPort = relayPort;
    }

    public int getThreadPoolSize() {
        return threadPoolSize;
    }

    public void setThreadPoolSize(int threadPoolSize) {
        this.threadPoolSize = threadPoolSize;
    }

    public String getLogLevel() {
        return logLevel;
    }

    public void setLogLevel(String logLevel) {
        this.logLevel = logLevel;
    }

    public int getCurrentRegistration() {
        return currentRegistration;
    }

    public void setCurrentRegistration(int currentRegistration) {
        this.currentRegistration = currentRegistration;
    }

    public int getLiveSessions() {
        return liveSessions;
    }

    public void setLiveSessions(int liveSessions) {
        this.liveSessions = liveSessions;
    }

    public int getMaxRegistration() {
        return maxRegistration;
    }

    public void setMaxRegistration(int maxRegistration) {
        this.maxRegistration = maxRegistration;
    }

    public int getLastPacketID() {
        return lastPacketID;
    }

    public void setLastPacketID(int lastPacketID) {
        this.lastPacketID = lastPacketID;
    }

    public int getPlayingRole() {
        return playingRole;
    }

    public void setPlayingRole(int playingRole) {
        this.playingRole = playingRole;
    }

    public int getServerStatus() {
        return serverStatus;
    }

    public void setServerStatus(int serverStatus) {
        this.serverStatus = serverStatus;
    }

    public boolean isIsSearchByIP() {
        return isSearchByIP;
    }

    public void setIsSearchByIP(boolean isSearchByIP) {
        this.isSearchByIP = isSearchByIP;
    }

    public static class CompASC implements Comparator<AuthServerDTO> {

        @Override
        public int compare(AuthServerDTO o1, AuthServerDTO o2) {
            switch (column) {
                case Constants.COLUMN_ONE:
                    return o1.getId() - o2.getId();
                case Constants.COLUMN_TWO:
                    return o1.getStartRingID().compareTo(o2.getStartRingID());
                case Constants.COLUMN_THREE:
                    return o1.getStartRingID().compareTo(o2.getStartRingID());
                case Constants.COLUMN_FOUR:
                    return o1.getEndRingID().compareTo(o2.getEndRingID());
                default:
                    return o1.getId() - o2.getId();
            }
        }
    }

    public static class CompDSC implements Comparator<AuthServerDTO> {

        @Override
        public int compare(AuthServerDTO o1, AuthServerDTO o2) {
            switch (column) {
                case Constants.COLUMN_ONE:
                    return o2.getId() - o1.getId();
                case Constants.COLUMN_TWO:
                    return o2.getStartRingID().compareTo(o1.getStartRingID());
                case Constants.COLUMN_THREE:
                    return o2.getStartRingID().compareTo(o1.getStartRingID());
                case Constants.COLUMN_FOUR:
                    return o2.getEndRingID().compareTo(o1.getEndRingID());
                default:
                    return o2.getId() - o1.getId();
            }
        }
    }
}
