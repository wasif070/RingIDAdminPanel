/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.servers.offlineServers;

import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.Constants;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author Kazi Nasir Uddin Oly
 * @Date Jun 12, 2016
 */
public class OfflineServerListAction extends BaseAction {

    ArrayList<OfflineServerDTO> list = new ArrayList<>();

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = FAILURE;
        try {
            OfflineServerForm offlineServerForm = (OfflineServerForm) form;
            OfflineServerDTO offlineServerDTO = new OfflineServerDTO();
            OfflineServerTaskScheduler scheduler = OfflineServerTaskScheduler.getInstance();

            if (request.getParameter("ForceReload") != null && "1".equals(request.getParameter("ForceReload"))) {
                OfflineServerLoader.getInstance().forceReload();
            }
            if (offlineServerForm.getSearchText() != null && offlineServerForm.getSearchText().length() > 0) {
                offlineServerDTO.setSearchText(offlineServerForm.getSearchText().trim().toLowerCase());
            }
            if (offlineServerForm.getColumn() <= 0) {
                offlineServerForm.setColumn(Constants.COLUMN_ONE);
                offlineServerForm.setSort(Constants.DESC_SORT);
            }
            offlineServerDTO.setColumn(offlineServerForm.getColumn());
            offlineServerDTO.setSortType(offlineServerForm.getSort());
            list = scheduler.getOfflineServerList(offlineServerDTO);
            request.getSession(true).setAttribute(Constants.DATA_ROWS, list);
            managePages(request, offlineServerForm, list.size());
            request.getSession(true).setAttribute("search", offlineServerDTO.getServerIP());
        } catch (NullPointerException e) {
            target = FAILURE;
        }
        return mapping.findForward(target);
    }
}
