/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.servers.offlineServers;

import com.ringid.admin.BaseDAO;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.utils.MyAppError;
import java.sql.SQLException;
import java.util.ArrayList;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Kazi Nasir Uddin Oly
 * @Date Jun 12, 2016
 */
public class OfflineServerDAO extends BaseDAO {

    static Logger logger = RingLogger.getConfigPortalLogger();

    public OfflineServerDAO() {

    }

    public int getMaxId() {
        int id = 0;
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            stmt = db.connection.createStatement();
            String query = "SELECT max(id) AS maxId FROM offlineservers;";
            rs = stmt.executeQuery(query);
            if (rs.next()) {
                id = rs.getInt("maxId");
            }
        } catch (Exception e) {
            logger.error("Exception in AuthSeverDAO[getMaxId] -> " + e);
        } finally {
            close();
        }
        return id;
    }

    public MyAppError addOfflineServerInfo(OfflineServerDTO dto) {
        MyAppError error = new MyAppError();
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            stmt = db.connection.createStatement();
            String query = "INSERT INTO offlineservers(id, serverIP, offlinePort, startPort, endPort, currentRegitration, maxRegitration, serverStatus) VALUES (?, ?, ?, ?, ?, ?, ?, ?);";
            ps = db.connection.prepareStatement(query);
            int index = 1;
            ps.setInt(index++, dto.getId());
            ps.setString(index++, dto.getServerIP());
            ps.setInt(index++, dto.getOfflinePort());
            ps.setInt(index++, dto.getStartPort());
            ps.setInt(index++, dto.getEndPort());
            ps.setInt(index++, dto.getCurrentRegistration());
            ps.setInt(index++, dto.getMaxRegistration());
            ps.setInt(index++, dto.getServerStatus());
            ps.execute();
            ps.close();
        } catch (SQLException e) {
            error.setERROR_TYPE(error.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            logger.debug("Single addOfflineServerInfo SQL Exception --> " + e);
        } catch (Exception e) {
            error.setERROR_TYPE(error.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            logger.debug("Single addOfflineServerInfo Exception --> " + e);
        } finally {
            close();
        }
        return error;
    }

    public MyAppError addOfflineServerInfo(ArrayList<OfflineServerDTO> maps) {
        MyAppError error = new MyAppError();
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            String query = "INSERT INTO offlineservers (id, serverIP, offlinePort, startPort, endPort, currentRegitration, maxRegitration, serverStatus) \n                                        "
                    + "         SELECT * FROM (SELECT ? AS c0, SELECT ? AS c1, ? AS c2, ? AS c3, ? AS c4, ? AS c5, ? AS c6, ? AS c7) AS tmp                                                         \n"
                    + "         WHERE NOT EXISTS (                                                                                        \n"
                    + "         SELECT serverIP FROM offlineservers WHERE serverIP = ?                                                    \n"
                    + "         );";
            ps = db.connection.prepareStatement(query);
            int i = 1;
            for (OfflineServerDTO entry : maps) {
                int index = 1;
                ps.setInt(index++, entry.getOfflinePort());
                ps.setString(index++, entry.getServerIP());
                ps.setInt(index++, entry.getOfflinePort());
                ps.setInt(index++, entry.getStartPort());
                ps.setInt(index++, entry.getEndPort());
                ps.setInt(index++, entry.getCurrentRegistration());
                ps.setInt(index++, entry.getMaxRegistration());
                ps.setInt(index++, entry.getServerStatus());
                ps.setString(index++, entry.getServerIP());
                ps.addBatch();
                i++;
                if ((i + 1) % 1000 == 0) {
                    ps.executeBatch();
                }
            }
            int a[] = ps.executeBatch();
        } catch (SQLException e) {
            error.setERROR_TYPE(error.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            logger.debug("addOfflineServerInfo Map SQL Exception --> " + e);
        } catch (Exception e) {
            error.setERROR_TYPE(error.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            logger.debug("addOfflineServerInfo Map Exception --> " + e);
        } finally {
            close();
        }
        return error;
    }

    public MyAppError updateOfflineServer(OfflineServerDTO dto) {
        MyAppError error = new MyAppError();
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            String query = "UPDATE offlineservers SET id = ?, serverIP = ?, offlinePort = ?, startPort = ?, endPort = ?, currentRegitration = ?, maxRegitration = ?, serverStatus = ? WHERE id = ?;";
            ps = db.connection.prepareStatement(query);
            int index = 1;
            ps.setInt(index++, dto.getNewId());
            ps.setString(index++, dto.getServerIP());
            ps.setInt(index++, dto.getOfflinePort());
            ps.setInt(index++, dto.getStartPort());
            ps.setInt(index++, dto.getEndPort());
            ps.setInt(index++, dto.getCurrentRegistration());
            ps.setInt(index++, dto.getMaxRegistration());
            ps.setInt(index++, dto.getServerStatus());
            ps.setInt(index++, dto.getId());
            ps.execute();
        } catch (SQLException e) {
            error.setERROR_TYPE(error.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            logger.debug("updateOfflineServer SQL Exception --> " + e);
        } catch (Exception e) {
            error.setERROR_TYPE(error.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            logger.debug("updateOfflineServer Exception --> " + e);
        } finally {
            close();
        }
        return error;
    }

    public MyAppError deleteOfflineServer(int id) {
        MyAppError error = new MyAppError();
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            stmt = db.connection.createStatement();
            String sql = "DELETE FROM offlineservers WHERE id = " + id + ";";
            int a = stmt.executeUpdate(sql);
            logger.debug("delete success");
        } catch (SQLException e) {
            error.setERROR_TYPE(error.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            logger.debug("deleteOfflineServer SQL Exception--> " + e);
        } catch (Exception e) {
            error.setERROR_TYPE(error.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            logger.debug("deleteOfflineServer Exception --> " + e);
        } finally {
            close();
        }
        return error;
    }
}
