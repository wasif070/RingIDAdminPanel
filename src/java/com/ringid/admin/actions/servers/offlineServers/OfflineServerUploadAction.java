/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.servers.offlineServers;

import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.utils.MyAppError;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author Kazi Nasir Uddin Oly
 * @Date Jun 12, 2016
 */
public class OfflineServerUploadAction extends BaseAction {

    static Logger logger = RingLogger.getConfigPortalLogger();
    String line = "";
    String cvsSplitBy = ",";

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = checkAuthentication(mapping, form, request);
        if (target.equals(SUCCESS)) {
            String[] dataArray = getDataFromFile(form);
            try {
                ArrayList<OfflineServerDTO> offlineServerList = new ArrayList<>();

                for (String s : dataArray) {
                    String[] offlineServer = s.split(cvsSplitBy);
                    String serverIP = offlineServer[0];
                    String offlinePort = offlineServer[1];
                    String startPort = offlineServer[2];
                    String endPort = offlineServer[3];
                    String currentRegistration = offlineServer[4];
                    String maxRegistration = offlineServer[5];
                    String serverStatus = offlineServer[6];
                    if (serverIP.equalsIgnoreCase("Server IP")) {
                        continue;
                    }

                    OfflineServerDTO offlineServerDTO = new OfflineServerDTO();
                    offlineServerDTO.setServerIP(serverIP);
                    offlineServerDTO.setOfflinePort(Integer.valueOf(offlinePort));
                    offlineServerDTO.setStartPort(Integer.valueOf(startPort));
                    offlineServerDTO.setEndPort(Integer.valueOf(endPort));
                    offlineServerDTO.setCurrentRegistration(Integer.valueOf(currentRegistration));
                    offlineServerDTO.setMaxRegistration(Integer.valueOf(maxRegistration));
                    offlineServerDTO.setServerStatus(Integer.valueOf(serverStatus));

                    offlineServerList.add(offlineServerDTO);
                }

                MyAppError error = OfflineServerTaskScheduler.getInstance().addOfflineServerInfo(offlineServerList);
                if (error.getERROR_TYPE() > 0) {
                    target = FAILURE;
                } else {
                    OfflineServerLoader.getInstance().forceReload();
                }
            } catch (Exception e) {
                logger.error("OfflineServerUploadAction Exception: " + e);
                target = FAILURE;
            }
        }
        return mapping.findForward(target);
    }
}
