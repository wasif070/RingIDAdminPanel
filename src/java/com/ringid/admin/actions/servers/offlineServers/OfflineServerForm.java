/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.servers.offlineServers;

import com.ringid.admin.BaseForm;
import com.ringid.admin.dto.adminAuth.LoginDTO;
import com.ringid.admin.projectMenu.MenuNames;
import com.ringid.admin.utils.Constants;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

/**
 *
 * @author Kazi Nasir Uddin Oly
 * @Date Jun 12, 2016
 */
public class OfflineServerForm extends BaseForm {

    private int id;
    private int newId;
    private String serverIP;
    private String offlinePort;
    private String startPort;
    private String endPort;
    private String currentRegistration;
    private String maxRegistration;
    private String serverStatus;

    private ArrayList<OfflineServerDTO> offlineServerDTO = new ArrayList<OfflineServerDTO>();

    public int getNewId() {
        return newId;
    }

    public void setNewId(int newId) {
        this.newId = newId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getServerIP() {
        return serverIP;
    }

    public void setServerIP(String serverIP) {
        this.serverIP = serverIP;
    }

    public String getOfflinePort() {
        return offlinePort;
    }

    public void setOfflinePort(String offlinePort) {
        this.offlinePort = offlinePort;
    }

    public String getStartPort() {
        return startPort;
    }

    public void setStartPort(String startPort) {
        this.startPort = startPort;
    }

    public String getEndPort() {
        return endPort;
    }

    public void setEndPort(String endPort) {
        this.endPort = endPort;
    }

    public String getCurrentRegistration() {
        return currentRegistration;
    }

    public void setCurrentRegistration(String currentRegistration) {
        this.currentRegistration = currentRegistration;
    }

    public String getMaxRegistration() {
        return maxRegistration;
    }

    public void setMaxRegistration(String maxRegistration) {
        this.maxRegistration = maxRegistration;
    }

    public String getServerStatus() {
        return serverStatus;
    }

    public void setServerStatus(String serverStatus) {
        this.serverStatus = serverStatus;
    }

    public ArrayList<OfflineServerDTO> getOfflineServerDTO() {
        return offlineServerDTO;
    }

    public void setOfflineServerDTO(ArrayList<OfflineServerDTO> offlineServerDTO) {
        this.offlineServerDTO = offlineServerDTO;
    }

    /**
     *
     * @param mapping
     * @param request
     * @return
     */
    @Override
    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        ActionErrors errors = new ActionErrors();
//        LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
//        String FEATURE_NAME = MenuNames.OFFLINE_SERVER;
//        if (loginDTO == null) {
//            errors.add("auth", new ActionMessage("UnAuthorized Access"));
//        } else if (Constants.ADD == getAction() || Constants.UPDATE == getAction()) {
//            if (!(loginDTO.getPermissionLevel() == Constants.SUPER_ADMIN) && !(loginDTO.getFeauterMapList().get(FEATURE_NAME).getHasAddPermission()
//                    || loginDTO.getFeauterMapList().get(FEATURE_NAME).getHasModifyPermission())) {
//                errors.add("auth", new ActionMessage("UnAuthorized Access"));
//            } else {
//                if (getServerIP() == null || getServerIP().length() == 0) {
//                    errors.add("serverIP", new ActionMessage("errors.serverIP.required"));
//                }
//                if (getOfflinePort() == null || getOfflinePort().length() == 0) {
//                    errors.add("offlinePort", new ActionMessage("errors.offlinePort.required"));
//                }
//            }
//        }
        return errors;
    }

}
