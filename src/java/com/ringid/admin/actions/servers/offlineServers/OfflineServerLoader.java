/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.servers.offlineServers;

import com.ringid.admin.BaseDAO;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.utils.Constants;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Kazi Nasir Uddin Oly
 * @Date Jun 12, 2016
 */
public class OfflineServerLoader extends BaseDAO {

    private final static long LOADING_INTERVAL = 60 * 60 * 1000;
    private long loadingTime = 0;
    private HashMap<Integer, OfflineServerDTO> offlineServerHashMapList;
    private static OfflineServerLoader offlineServerLoader = null;
    static Logger logger = RingLogger.getConfigPortalLogger();

    private OfflineServerLoader() {

    }

    public static OfflineServerLoader getInstance() {
        if (offlineServerLoader == null) {
            createLoader();
        }
        return offlineServerLoader;
    }

    private synchronized static void createLoader() {
        if (offlineServerLoader == null) {
            offlineServerLoader = new OfflineServerLoader();
        }
    }

    private void forceLoadData() {
        offlineServerHashMapList = new HashMap<>();
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            stmt = db.connection.createStatement();
            String sql = "SELECT id, serverIP, offlinePort, startPort, endPort, currentRegitration, maxRegitration, serverStatus FROM offlineservers;";
            rs = stmt.executeQuery(sql);
            while (rs.next()) {
                OfflineServerDTO dto = new OfflineServerDTO();
                dto.setId(rs.getInt("id"));
                dto.setServerIP(rs.getString("serverIP"));
                dto.setOfflinePort(rs.getInt("offlinePort"));
                dto.setStartPort(rs.getInt("startPort"));
                dto.setEndPort(rs.getInt("endPort"));
                dto.setCurrentRegistration(rs.getInt("currentRegitration"));
                dto.setMaxRegistration(rs.getInt("maxRegitration"));
                dto.setServerStatus(rs.getInt("serverStatus"));
                offlineServerHashMapList.put(dto.getId(), dto);
            }
            logger.debug(sql + "::" + offlineServerHashMapList.size());

        } catch (Exception e) {
            logger.debug("Offline Server List Exception...", e);
        } finally {
            close();
        }
    }

    private void checkForReload() {
        long currentTime = System.currentTimeMillis();
        if (currentTime - loadingTime > LOADING_INTERVAL) {
            loadingTime = currentTime;
            forceLoadData();
        }
    }

    public synchronized void forceReload() {
        loadingTime = System.currentTimeMillis();
        forceLoadData();
    }

    private boolean contains(OfflineServerDTO dto, String searchText) {
        return (String.valueOf(dto.getId()).contains(searchText))
                || (dto.getServerIP() != null && dto.getServerIP().contains(searchText))
                || (String.valueOf(dto.getOfflinePort()).contains(searchText))
                || (String.valueOf(dto.getStartPort()).contains(searchText))
                || (String.valueOf(dto.getEndPort()).contains(searchText))
                || (String.valueOf(dto.getCurrentRegistration()).contains(searchText))
                || (String.valueOf(dto.getMaxRegistration()).contains(searchText));
    }

    private ArrayList<OfflineServerDTO> getOfflineServerData(OfflineServerDTO sdto) {
        checkForReload();
        ArrayList<OfflineServerDTO> data = new ArrayList<>();
        Set set = offlineServerHashMapList.entrySet();
        Iterator i = set.iterator();
        while (i.hasNext()) {
            Map.Entry me = (Map.Entry) i.next();
            OfflineServerDTO dto = (OfflineServerDTO) me.getValue();
            if (sdto.getSearchText() != null && sdto.getSearchText().length() > 0 && !contains(dto, sdto.getSearchText())) {
                continue;
            }
            data.add(dto);
        }
        if (sdto.getSortType() == Constants.DESC_SORT) {
            Collections.sort(data, new OfflineServerDTO.CompDSC());
        } else {
            Collections.sort(data, new OfflineServerDTO.CompASC());
        }
        return data;
    }

    public synchronized ArrayList<OfflineServerDTO> getOfflineServerList(OfflineServerDTO sdto) {
        ArrayList<OfflineServerDTO> offlineServerList = getOfflineServerData(sdto);
        logger.debug("getOfflineServerList Size: " + offlineServerList.size());
        return offlineServerList;
    }

    public synchronized ArrayList<OfflineServerDTO> getOfflineServerList(int columnValue, int sortType, OfflineServerDTO sdto) {
        ArrayList<OfflineServerDTO> offlineServerList = getOfflineServerData(sdto);
        if (sortType == Constants.DESC_SORT) {
            Collections.sort(offlineServerList, new OfflineServerDTO.CompDSC());
        } else {
            Collections.sort(offlineServerList, new OfflineServerDTO.CompASC());
        }
        logger.debug("getOfflineServerList Size: " + offlineServerList.size());
        return offlineServerList;
    }

    public synchronized ArrayList<OfflineServerDTO> getOfflineServerListForDownload() {
        checkForReload();
        ArrayList<OfflineServerDTO> data = new ArrayList<>(offlineServerHashMapList.values());
        return data;
    }

    public synchronized OfflineServerDTO getOfflineServerDTO(int id) {
        checkForReload();
        if (offlineServerHashMapList.containsKey(id)) {
            return offlineServerHashMapList.get(id);
        }
        return null;
    }
}
