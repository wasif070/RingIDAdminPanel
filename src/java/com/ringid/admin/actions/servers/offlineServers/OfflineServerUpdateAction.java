/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.servers.offlineServers;

import com.google.gson.Gson;
import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.dto.adminAuth.LoginDTO;
import com.ringid.admin.utils.Constants;
import com.ringid.admin.utils.MyAppError;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author Kazi Nasir Uddin Oly
 * @Date Jun 12, 2016
 */
public class OfflineServerUpdateAction extends BaseAction {

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        String target = checkAuthentication(mapping, form, request);
        String activistName;
        try {
            LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
            activistName = loginDTO.getUserName();
        } catch (Exception e) {
            target = UNAUTHORIZED;
            return mapping.findForward(target);
        }
        try {
            if (target.equals(SUCCESS)) {
                OfflineServerForm offlineServerForm = (OfflineServerForm) form;
                OfflineServerTaskScheduler scheduler = OfflineServerTaskScheduler.getInstance();
                OfflineServerDTO offlineServerDTO = scheduler.getOfflineServerDTO(offlineServerForm);
                offlineServerDTO.setId(offlineServerForm.getId());
                offlineServerDTO.setNewId(offlineServerForm.getNewId());
                if (offlineServerDTO.getNewId() <= 0) {
                    offlineServerDTO.setNewId(offlineServerDTO.getId());
                }
                RingLogger.getActivityLogger().debug("[OfflineServerUpdateAction] activistName : " + activistName + " offlineServerDTO --> " + new Gson().toJson(offlineServerDTO));
                MyAppError error = scheduler.updateOfflineServer(offlineServerDTO);
                if (error.getERROR_TYPE() > 0) {
                    target = FAILURE;
                    offlineServerForm.setMessage(error.getErrorMessage());
                    request.getSession(true).setAttribute("error", "2");
                } else {
                    OfflineServerLoader.getInstance().forceReload();
                }
            }
        } catch (Exception e) {
        }
        return mapping.findForward(target);
    }
}
