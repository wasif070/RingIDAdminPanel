/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.servers.offlineServers;

import com.ringid.admin.utils.MyAppError;
import java.util.ArrayList;

/**
 *
 * @author Kazi Nasir Uddin Oly
 * @Date Jun 12, 2016
 */
public class OfflineServerTaskScheduler {

    private OfflineServerTaskScheduler() {

    }

    private static class OfflineServerTaskSchedulerHolder {

        private static final OfflineServerTaskScheduler INSTANCE = new OfflineServerTaskScheduler();
    }

    public static OfflineServerTaskScheduler getInstance() {
        return OfflineServerTaskSchedulerHolder.INSTANCE;
    }

    public ArrayList<OfflineServerDTO> getOfflineServerList(OfflineServerDTO dto) {
        return OfflineServerLoader.getInstance().getOfflineServerList(dto);
    }

    public ArrayList<OfflineServerDTO> getOfflineServerList(int columnValue, int sortType, OfflineServerDTO dto) {
        return OfflineServerLoader.getInstance().getOfflineServerList(columnValue, sortType, dto);
    }

    public int getMaxId() {
        OfflineServerDAO authServerDAO = new OfflineServerDAO();
        return authServerDAO.getMaxId();
    }

    public MyAppError addOfflineServerInfo(OfflineServerDTO p_dto) {
        OfflineServerDAO offlineServerDAO = new OfflineServerDAO();
        return offlineServerDAO.addOfflineServerInfo(p_dto);
    }

    public MyAppError addOfflineServerInfo(ArrayList<OfflineServerDTO> maps) {
        OfflineServerDAO offlineServerDAO = new OfflineServerDAO();
        return offlineServerDAO.addOfflineServerInfo(maps);
    }

    public OfflineServerDTO getOfflineServerInfoById(int id) {
        return OfflineServerLoader.getInstance().getOfflineServerDTO(id);
    }

    public MyAppError deleteOfflineServer(int id) {
        OfflineServerDAO offlineServerDAO = new OfflineServerDAO();
        return offlineServerDAO.deleteOfflineServer(id);
    }

    public MyAppError updateOfflineServer(OfflineServerDTO p_dto) {
        OfflineServerDAO offlineServerDAO = new OfflineServerDAO();
        return offlineServerDAO.updateOfflineServer(p_dto);
    }

    public ArrayList<OfflineServerDTO> getOfflineServerListForDownload() {
        return OfflineServerLoader.getInstance().getOfflineServerListForDownload();
    }

    public OfflineServerDTO getOfflineServerDTO(OfflineServerForm offlineServerForm) {
        OfflineServerDTO offlineServerDTO = new OfflineServerDTO();
        offlineServerDTO.setServerIP(offlineServerForm.getServerIP());
        offlineServerDTO.setOfflinePort(Integer.valueOf(offlineServerForm.getOfflinePort()));
        if (!offlineServerForm.isNull(offlineServerForm.getStartPort())) {
            offlineServerDTO.setStartPort(Integer.valueOf(offlineServerForm.getStartPort()));
        }
        if (!offlineServerForm.isNull(offlineServerForm.getEndPort())) {
            offlineServerDTO.setEndPort(Integer.valueOf(offlineServerForm.getEndPort()));
        }
        if (!offlineServerForm.isNull(offlineServerForm.getCurrentRegistration())) {
            offlineServerDTO.setCurrentRegistration(Integer.valueOf(offlineServerForm.getCurrentRegistration()));
        }
        if (offlineServerForm.isNull(offlineServerForm.getMaxRegistration())) {
            offlineServerDTO.setMaxRegistration(10000);
        } else {
            offlineServerDTO.setMaxRegistration(Integer.valueOf(offlineServerForm.getMaxRegistration()));
        }

        if (offlineServerForm.isNull(offlineServerForm.getServerStatus())) {
            offlineServerDTO.setServerStatus(1);
        } else {
            offlineServerDTO.setServerStatus(Integer.valueOf(offlineServerForm.getServerStatus()));
        }
        return offlineServerDTO;
    }

}
