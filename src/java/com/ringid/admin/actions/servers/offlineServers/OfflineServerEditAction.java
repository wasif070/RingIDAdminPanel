/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.servers.offlineServers;

import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author Kazi Nasir Uddin Oly
 * @Date Jun 12, 2016
 */
public class OfflineServerEditAction extends BaseAction {

    Logger logger = RingLogger.getConfigPortalLogger();

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        String target = checkAuthentication(mapping, form, request);
        try {
            if (target.equals(SUCCESS)) {
                int id = 0;
                OfflineServerForm offlineServerForm = (OfflineServerForm) form;
                if (request.getParameter("id") != null) {
                    id = Integer.parseInt(request.getParameter("id"));
                }
                OfflineServerTaskScheduler scheduler = OfflineServerTaskScheduler.getInstance();
                OfflineServerDTO offlineServerDTO = scheduler.getOfflineServerInfoById(id);
                if (offlineServerDTO != null) {
                    offlineServerForm.setNewId(offlineServerDTO.getId());
                    offlineServerForm.setId(offlineServerDTO.getId());
                    offlineServerForm.setServerIP(offlineServerDTO.getServerIP());
                    offlineServerForm.setOfflinePort(String.valueOf(offlineServerDTO.getOfflinePort()));
                    offlineServerForm.setStartPort(String.valueOf(offlineServerDTO.getStartPort()));
                    offlineServerForm.setEndPort(String.valueOf(offlineServerDTO.getEndPort()));
                    offlineServerForm.setCurrentRegistration(String.valueOf(offlineServerDTO.getCurrentRegistration()));
                    offlineServerForm.setMaxRegistration(String.valueOf(offlineServerDTO.getMaxRegistration()));
                    offlineServerForm.setServerStatus(String.valueOf(offlineServerDTO.getServerStatus()));
                    target = SUCCESS;
                }
            }
        } catch (Exception e) {
            logger.error("OfflineServerEditAction Exception: " + e);
            target = FAILURE;
        }
        return mapping.findForward(target);
    }
}
