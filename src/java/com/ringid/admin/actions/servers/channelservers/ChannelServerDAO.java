/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.servers.channelservers;

import com.ringid.admin.BaseDAO;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.utils.MyAppError;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author mamun
 * @Date Jan 28, 2017
 */
public class ChannelServerDAO extends BaseDAO {

    static Logger logger = RingLogger.getConfigPortalLogger();

    public ChannelServerDAO() {

    }

    public int getMaxId() {
        int id = 0;
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            String query = "SELECT max(id) AS maxId FROM channelservers;";
            rs = db.connection.prepareStatement(query).executeQuery();
            if (rs.next()) {
                id = rs.getInt("maxId");
            }
        } catch (Exception e) {
            logger.error("Exception in ChannelServerDAO[getMaxId] -> " + e);
        } finally {
            close();
        }
        return id;
    }

    public Map<Integer, ChannelServerDTO> getChannelServerHashMap() {
        Map<Integer, ChannelServerDTO> channelServerHashMap = new HashMap<>();
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            String sql = "SELECT id, serverIP, registerPort, heartbitPort, currentRegistration, maxRegistration, serverStatus FROM channelservers;";
            rs = db.connection.prepareStatement(sql).executeQuery();
            while (rs.next()) {
                ChannelServerDTO dto = new ChannelServerDTO();
                dto.setId(rs.getInt("id"));
                dto.setServerIP(rs.getString("serverIP"));
                dto.setRegisterPort(rs.getInt("registerPort"));
                dto.setHeartbitPort(rs.getInt("heartbitPort"));
                dto.setCurrentRegistration(rs.getInt("currentRegistration"));
                dto.setMaxRegistration(rs.getInt("maxRegistration"));
                dto.setServerStatus(rs.getInt("serverStatus"));
                channelServerHashMap.put(dto.getId(), dto);
            }
            logger.debug(sql + " ChannelServerDAO[getChannelServerHashMap] --> ::" + channelServerHashMap.size());
        } catch (Exception e) {
            logger.debug("ChannelServerDAO[getChannelServerHashMap] Exception...", e);
        } finally {
            close();
        }
        return channelServerHashMap;
    }

    public ArrayList<ChannelServerDTO> getChannelServerList(ChannelServerDTO sdto) {
        ArrayList<ChannelServerDTO> channelServerList = new ArrayList<>();
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            String sql = "SELECT id, serverIP, registerPort, heartbitPort, currentRegistration, maxRegistration, serverStatus FROM channelservers;";
            rs = db.connection.prepareStatement(sql).executeQuery();
            while (rs.next()) {
                ChannelServerDTO dto = new ChannelServerDTO();
                dto.setId(rs.getInt("id"));
                dto.setServerIP(rs.getString("serverIP"));
                dto.setRegisterPort(rs.getInt("registerPort"));
                dto.setHeartbitPort(rs.getInt("heartbitPort"));
                dto.setCurrentRegistration(rs.getInt("currentRegistration"));
                dto.setMaxRegistration(rs.getInt("maxRegistration"));
                dto.setServerStatus(rs.getInt("serverStatus"));
                if (sdto.getSearchText() != null && sdto.getSearchText().length() > 0) {
                    if (!(dto.getServerIP().toLowerCase().contains(sdto.getSearchText().trim().toLowerCase()))) {
                        continue;
                    }
                }
                channelServerList.add(dto);
            }
            logger.debug(sql + " ChannelServerDAO[getChannelServerList] --> ::" + channelServerList.size());
        } catch (Exception e) {
            logger.debug("ChannelServerDAO[getChannelServerList] Exception...", e);
        } finally {
            close();
        }
        return channelServerList;
    }

    public ArrayList<ChannelServerDTO> getActiveChannelServerList() {
        ArrayList<ChannelServerDTO> channelServerList = new ArrayList<>();
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            String sql = "SELECT id, serverIP, registerPort, heartbitPort, currentRegistration, maxRegistration, serverStatus FROM channelservers WHERE serverStatus = 1;";
            rs = db.connection.prepareStatement(sql).executeQuery();
            while (rs.next()) {
                ChannelServerDTO dto = new ChannelServerDTO();
                dto.setId(rs.getInt("id"));
                dto.setServerIP(rs.getString("serverIP"));
                dto.setRegisterPort(rs.getInt("registerPort"));
                dto.setHeartbitPort(rs.getInt("heartbitPort"));
                dto.setCurrentRegistration(rs.getInt("currentRegistration"));
                dto.setMaxRegistration(rs.getInt("maxRegistration"));
                dto.setServerStatus(rs.getInt("serverStatus"));
                channelServerList.add(dto);
            }
            logger.debug(sql + " ChannelServerDAO[getActiveChannelServerList] --> ::" + channelServerList.size());

        } catch (Exception e) {
            logger.debug("ChannelServerDAO[getActiveChannelServerList] Exception...", e);
        } finally {
            close();
        }
        return channelServerList;
    }

    public ChannelServerDTO getChannelServerDTO(int id) {
        ChannelServerDTO channelServerDTO = new ChannelServerDTO();
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            String sql = "SELECT id, serverIP, registerPort, heartbitPort, currentRegistration, maxRegistration, serverStatus FROM channelservers where id = ? ;";
            ps = db.connection.prepareStatement(sql);
            ps.setInt(1, id);
            rs = ps.executeQuery();
            while (rs.next()) {
                channelServerDTO.setId(rs.getInt("id"));
                channelServerDTO.setServerIP(rs.getString("serverIP"));
                channelServerDTO.setRegisterPort(rs.getInt("registerPort"));
                channelServerDTO.setHeartbitPort(rs.getInt("heartbitPort"));
                channelServerDTO.setCurrentRegistration(rs.getInt("currentRegistration"));
                channelServerDTO.setMaxRegistration(rs.getInt("maxRegistration"));
                channelServerDTO.setServerStatus(rs.getInt("serverStatus"));
            }
            logger.debug(sql + "::" + channelServerDTO.getId());
        } catch (Exception e) {
            logger.debug("ChannelServerDAO[getChannelServerDTO] List Exception...", e);
        } finally {
            close();
        }
        return channelServerDTO;
    }

    MyAppError addChannelServerInfo(ChannelServerDTO dto) {
        MyAppError error = new MyAppError();
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            String query = "INSERT INTO channelservers (id, serverIP, registerPort, heartbitPort, currentRegistration, maxRegistration, serverStatus) VALUES (?, ?, ?, ?, ?, ?, ?);";
            int i = 1;
            ps = db.connection.prepareStatement(query);
            ps.setInt(i++, dto.getId());
            ps.setString(i++, dto.getServerIP());
            ps.setInt(i++, dto.getRegisterPort());
            ps.setInt(i++, dto.getHeartbitPort());
            ps.setInt(i++, dto.getCurrentRegistration());
            ps.setInt(i++, dto.getMaxRegistration());
            ps.setInt(i++, dto.getServerStatus());
            ps.executeUpdate();
        } catch (SQLException e) {
            error.setERROR_TYPE(MyAppError.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            logger.debug("add Single addChannelServerInfo SQL Exception --> " + e);
        } catch (Exception e) {
            error.setERROR_TYPE(MyAppError.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            logger.debug("add Single addChannelServerInfo Exception --> " + e);
        } finally {
            close();
        }
        return error;
    }

    MyAppError updateChannelServer(ChannelServerDTO dto) {
        MyAppError error = new MyAppError();
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            String query = "UPDATE channelservers SET id = ?, serverIP = ?, registerPort = ?, heartbitPort = ?, currentRegistration = ?, maxRegistration = ?, serverStatus = ? WHERE id = ?;";
            int i = 1;
            ps = db.connection.prepareStatement(query);
            ps.setInt(i++, dto.getNewId());
            ps.setString(i++, dto.getServerIP());
            ps.setInt(i++, dto.getRegisterPort());
            ps.setInt(i++, dto.getHeartbitPort());
            ps.setInt(i++, dto.getCurrentRegistration());
            ps.setInt(i++, dto.getMaxRegistration());
            ps.setInt(i++, dto.getServerStatus());
            ps.setInt(i++, dto.getId());
            ps.executeUpdate();
        } catch (SQLException e) {
            error.setERROR_TYPE(error.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            logger.debug("updateChannelServer SQL Exception --> " + e);
        } catch (Exception e) {
            error.setERROR_TYPE(error.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            logger.debug("updateChannelServer Exception --> " + e);
        } finally {
            close();
        }
        return error;
    }
}
