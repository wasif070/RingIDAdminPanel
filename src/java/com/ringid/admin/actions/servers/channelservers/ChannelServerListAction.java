package com.ringid.admin.actions.servers.channelservers;

import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.utils.Constants;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class ChannelServerListAction extends BaseAction {

    static Logger loggger = RingLogger.getConfigPortalLogger();

    List<ChannelServerDTO> list = new ArrayList<>();

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = FAILURE;
        try {
            ChannelServerForm channelServerForm = (ChannelServerForm) form;
            ChannelServerDTO channelServerDTO = new ChannelServerDTO();
            if (request.getParameter("ForceReload") != null && "1".equals(request.getParameter("ForceReload"))) {
                ChannelServerLoader.getInstance().forceReload();
            }
            ChannelServerTaskScheduler scheduler = ChannelServerTaskScheduler.getInstance();
            if (channelServerForm.getSearchText() != null && channelServerForm.getSearchText().length() > 0) {
                channelServerDTO.setSearchText(channelServerForm.getSearchText().trim().toLowerCase());
            }
            if (channelServerForm.getColumn() <= 0) {
                channelServerForm.setColumn(Constants.COLUMN_ONE);
                channelServerForm.setSort(Constants.DESC_SORT);
            }
            channelServerDTO.setColumn(channelServerForm.getColumn());
            channelServerDTO.setSortType(channelServerForm.getSort());
            list = scheduler.getChannelServersList(channelServerDTO);
            loggger.debug("ChannelServerListAction : [ TotalRecords : " + list.size() + " ]");

            request.getSession(true).setAttribute(Constants.DATA_ROWS, list);
            managePages(request, channelServerForm, list.size());
        } catch (NullPointerException e) {
            loggger.error(e);
            target = FAILURE;
        }
        return mapping.findForward(target);
    }
}
