/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.servers.channelservers;

import auth.com.utils.MyAppError;
import com.google.gson.Gson;
import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.utils.Constants;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import ringid.LoginTaskScheduler;
import ringid.sessionless.SessionlessTaskseheduler;

/**
 *
 * @author mamun
 * @Date Jan 28, 2017
 */
public class ChannelServerReassignAction extends BaseAction {

    static Logger logger = RingLogger.getConfigPortalLogger();

    List<ChannelServerDTO> list = new ArrayList<>();

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        String target = checkAuthentication(mapping, form, request);

        try {
            if (target.equals(SUCCESS)) {
                ChannelServerForm channelServerForm = (ChannelServerForm) form;
                int id = 0;
                if (request.getParameter("id") != null) {
                    id = Integer.parseInt(request.getParameter("id"));
                }
                logger.debug("ChannelServerReassignAction : [Id : " + id + " ]");
                ChannelServerTaskScheduler scheduler = ChannelServerTaskScheduler.getInstance();
                ChannelServerDTO channelServer = scheduler.getChannelServerInfoById(id);
                if (channelServer != null) {
                    LoginTaskScheduler.getInstance().initSessionLessCommunication(Constants.USERID);
                    MyAppError error = SessionlessTaskseheduler.getInstance().serverReasign(Constants.USERID, channelServer.getServerIP(), channelServer.getRegisterPort(), Constants.REASIGN_CHANNEL_SERVER);
                    LoginTaskScheduler.getInstance().destroySessionLessCommunication(Constants.USERID);
                    logger.debug("ChannelServerReassignAction : [error : " + error.getERROR_TYPE() + " ]");
                    if (error.getERROR_TYPE() > 0) {
                        target = FAILURE;
                    } else {
                        ChannelServerLoader.getInstance().forceReload();
                        list = scheduler.getChannelServersList(new ChannelServerDTO());
                        request.getSession(true).setAttribute(Constants.DATA_ROWS, list);
                        managePages(request, channelServerForm, list.size());
                    }
                } else {
                    target = FAILURE;
                }
            }
        } catch (Exception e) {
            logger.error("Exception in ChannelServerReassignAction....", e);
            target = FAILURE;
        }
        return mapping.findForward(target);
    }
}
