package com.ringid.admin.actions.servers.channelservers;

import com.google.gson.Gson;
import com.ringid.admin.BaseDTO;
import com.ringid.admin.utils.Constants;
import java.util.Comparator;

public class ChannelServerDTO extends BaseDTO {

    private int id;
    private int newId;
    private String serverIP;
    private int registerPort;
    private int heartbitPort;
    private int currentRegistration;
    private int maxRegistration;
    private int serverStatus;

    public int getNewId() {
        return newId;
    }

    public void setNewId(int newId) {
        this.newId = newId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getServerIP() {
        return serverIP;
    }

    public void setServerIP(String serverIP) {
        this.serverIP = serverIP;
    }

    public int getRegisterPort() {
        return registerPort;
    }

    public void setRegisterPort(int registerPort) {
        this.registerPort = registerPort;
    }

    public int getHeartbitPort() {
        return heartbitPort;
    }

    public void setHeartbitPort(int heartbitPort) {
        this.heartbitPort = heartbitPort;
    }

    public int getCurrentRegistration() {
        return currentRegistration;
    }

    public void setCurrentRegistration(int currentRegistration) {
        this.currentRegistration = currentRegistration;
    }

    public int getMaxRegistration() {
        return maxRegistration;
    }

    public void setMaxRegistration(int maxRegistration) {
        this.maxRegistration = maxRegistration;
    }

    public int getServerStatus() {
        return serverStatus;
    }

    public void setServerStatus(int serverStatus) {
        this.serverStatus = serverStatus;
    }

    public static class CompASC implements Comparator<ChannelServerDTO> {

        @Override
        public int compare(ChannelServerDTO o1, ChannelServerDTO o2) {
            switch (column) {
                case Constants.COLUMN_ONE:
                    return o1.getId() - o2.getId();
                case Constants.COLUMN_TWO:
                    return o1.getServerIP().compareTo(o2.getServerIP());
                default:
                    return o1.getId() - o2.getId();
            }
        }
    }

    public static class CompDSC implements Comparator<ChannelServerDTO> {

        @Override
        public int compare(ChannelServerDTO o1, ChannelServerDTO o2) {
            switch (column) {
                case Constants.COLUMN_ONE:
                    return o2.getId() - o1.getId();
                case Constants.COLUMN_TWO:
                    return o2.getServerIP().compareTo(o1.getServerIP());
                default:
                    return o2.getId() - o1.getId();
            }
        }
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }

}
