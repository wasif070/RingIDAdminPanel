/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.servers.channelservers;

import com.ringid.admin.utils.MyAppError;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author mamun
 * @Date Jan 28, 2017
 */
public class ChannelServerTaskScheduler {

    private ChannelServerTaskScheduler() {

    }

    private static class ChannelServerTaskSchedulerHolder {

        private static final ChannelServerTaskScheduler INSTANCE = new ChannelServerTaskScheduler();
    }

    public static ChannelServerTaskScheduler getInstance() {
        return ChannelServerTaskSchedulerHolder.INSTANCE;
    }

    public List<ChannelServerDTO> getChannelServersList(ChannelServerDTO sdto) {
        List<ChannelServerDTO> channelServerList = ChannelServerLoader.getInstance().getChannelServerList(sdto);
        return channelServerList;
    }

    public ArrayList<ChannelServerDTO> getActiveChannelServerList() {

        ChannelServerDAO channelServerDAO = new ChannelServerDAO();

        ArrayList<ChannelServerDTO> channelServerDTOs = channelServerDAO.getActiveChannelServerList();

        return channelServerDTOs;
    }

    public ChannelServerDTO getChannelServerInfoById(int id) {
        ChannelServerDTO channelServerDTO = ChannelServerLoader.getInstance().getChannelServerDTO(id);
        return channelServerDTO;
    }

    public int getMaxId() {
        ChannelServerDAO authServerDAO = new ChannelServerDAO();
        return authServerDAO.getMaxId();
    }

    MyAppError addChannelServerInfo(ChannelServerDTO channelServerDTO) {
        ChannelServerDAO channelServerDAO = new ChannelServerDAO();
        return channelServerDAO.addChannelServerInfo(channelServerDTO);
    }

    MyAppError updateChannelServer(ChannelServerDTO channelServerDTO) {
        ChannelServerDAO channelServerDAO = new ChannelServerDAO();
        return channelServerDAO.updateChannelServer(channelServerDTO);
    }
}
