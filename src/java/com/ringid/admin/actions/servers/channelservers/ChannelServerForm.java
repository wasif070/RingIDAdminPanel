package com.ringid.admin.actions.servers.channelservers;

import com.google.gson.Gson;
import com.ringid.admin.BaseForm;
import com.ringid.admin.dto.adminAuth.LoginDTO;
import com.ringid.admin.projectMenu.MenuNames;
import com.ringid.admin.utils.Constants;
import javax.servlet.http.HttpServletRequest;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

//  `id` int(11) NOT NULL AUTO_INCREMENT,
//  `device_type` int(11) DEFAULT NULL,
//  `version` varchar(16) DEFAULT NULL,
public class ChannelServerForm extends BaseForm {

    private int id;
    private int newId;
    private String serverIP;
    private int registerPort;
    private int heartbitPort;
    private int currentRegistration;
    private int maxRegistration;
    private int serverStatus;

    public int getNewId() {
        return newId;
    }

    public void setNewId(int newId) {
        this.newId = newId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getServerIP() {
        return serverIP;
    }

    public void setServerIP(String serverIP) {
        this.serverIP = serverIP;
    }

    public int getRegisterPort() {
        return registerPort;
    }

    public void setRegisterPort(int registerPort) {
        this.registerPort = registerPort;
    }

    public int getHeartbitPort() {
        return heartbitPort;
    }

    public void setHeartbitPort(int heartbitPort) {
        this.heartbitPort = heartbitPort;
    }

    public int getCurrentRegistration() {
        return currentRegistration;
    }

    public void setCurrentRegistration(int currentRegistration) {
        this.currentRegistration = currentRegistration;
    }

    public int getMaxRegistration() {
        return maxRegistration;
    }

    public void setMaxRegistration(int maxRegistration) {
        this.maxRegistration = maxRegistration;
    }

    public int getServerStatus() {
        return serverStatus;
    }

    public void setServerStatus(int serverStatus) {
        this.serverStatus = serverStatus;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }

    @Override
    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        ActionErrors errors = new ActionErrors();
        LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        String FEATURE_NAME = MenuNames.CHANNEL_SERVER;
        if (loginDTO == null) {
            errors.add("auth", new ActionMessage("UnAuthorized Access"));
        } else if (Constants.ADD == getAction() || Constants.UPDATE == getAction()) {
            if (!(loginDTO.getPermissionLevel() == Constants.SUPER_ADMIN) && !(loginDTO.getFeauterMapList().get(FEATURE_NAME).getHasAddPermission()
                    || loginDTO.getFeauterMapList().get(FEATURE_NAME).getHasModifyPermission())) {
                errors.add("auth", new ActionMessage("UnAuthorized Access"));
            } else {
//                    if (getVersion() == null || getVersion().length() < 1) {
//                        errors.add("version", new ActionMessage("errors.version.required"));
//                    }
            }
        }
        return errors;
    }

}
