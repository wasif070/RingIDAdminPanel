/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.servers.channelservers;

import com.google.gson.Gson;
import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.dto.adminAuth.LoginDTO;
import com.ringid.admin.utils.Constants;
import com.ringid.admin.utils.MyAppError;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author ipvision
 */
public class ChannelServerAction extends BaseAction {

    protected final String EDIT_CHANNEL_SERVER = "edit-channel-server";
    static Logger loggger = RingLogger.getConfigPortalLogger();

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = checkAuthentication(mapping, form, request);
        String activistName;
        try {
            LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
            activistName = loginDTO.getUserName();
        } catch (Exception e) {
            target = UNAUTHORIZED;
            return mapping.findForward(target);
        }
        try {
            if (target.equals(SUCCESS)) {
                ChannelServerForm channelServerForm = (ChannelServerForm) form;
                ChannelServerDTO channelServerDTO = new ChannelServerDTO();
                ChannelServerTaskScheduler scheduler = ChannelServerTaskScheduler.getInstance();
                MyAppError myAppError;
                switch (channelServerForm.getAction()) {
                    case Constants.ADD:

                        channelServerDTO.setServerIP(channelServerForm.getServerIP());
                        channelServerDTO.setRegisterPort(channelServerForm.getRegisterPort());
                        channelServerDTO.setHeartbitPort(channelServerForm.getHeartbitPort());
                        channelServerDTO.setCurrentRegistration(channelServerForm.getCurrentRegistration());
                        channelServerDTO.setMaxRegistration(channelServerForm.getMaxRegistration());
                        channelServerDTO.setServerStatus(channelServerForm.getServerStatus());
                        int maxId = scheduler.getMaxId();
                        channelServerDTO.setId(maxId + 1);
                        RingLogger.getActivityLogger().debug("[" + channelServerForm.getAction() + " ChannelServerAction] activistName : " + activistName + " channelServerDTO --> " + new Gson().toJson(channelServerDTO));
                        myAppError = scheduler.addChannelServerInfo(channelServerDTO);
                        if (myAppError.getERROR_TYPE() > MyAppError.NOERROR) {
                            loggger.error("Error in [ChannelServerAction] addChannelServerInfo: " + myAppError.getERROR_TYPE() + " ErrorMessage : " + myAppError.getErrorMessage());
                            channelServerForm.setMessage(myAppError.getErrorMessage());
                            request.getSession(true).setAttribute("error", "2");
                            target = FAILURE;
                        } else {
                            ChannelServerLoader.getInstance().forceReload();
                        }
                        break;
                    case Constants.EDIT:
                        channelServerDTO = scheduler.getChannelServerInfoById(channelServerForm.getId());
                        channelServerForm.setNewId(channelServerDTO.getId());
                        channelServerForm.setAction(Constants.UPDATE);
                        channelServerForm.setServerIP(channelServerDTO.getServerIP());
                        channelServerForm.setRegisterPort(channelServerDTO.getRegisterPort());
                        channelServerForm.setHeartbitPort(channelServerDTO.getHeartbitPort());
                        channelServerForm.setCurrentRegistration(channelServerDTO.getCurrentRegistration());
                        channelServerForm.setMaxRegistration(channelServerDTO.getMaxRegistration());
                        channelServerForm.setServerStatus(channelServerDTO.getServerStatus());

                        target = EDIT_CHANNEL_SERVER;
                        break;
                    case Constants.UPDATE:
                        channelServerDTO.setNewId(channelServerForm.getNewId());
                        channelServerDTO.setId(channelServerForm.getId());
                        channelServerDTO.setServerIP(channelServerForm.getServerIP());
                        channelServerDTO.setRegisterPort(channelServerForm.getRegisterPort());
                        channelServerDTO.setHeartbitPort(channelServerForm.getHeartbitPort());
                        channelServerDTO.setCurrentRegistration(channelServerForm.getCurrentRegistration());
                        channelServerDTO.setMaxRegistration(channelServerForm.getMaxRegistration());
                        channelServerDTO.setServerStatus(channelServerForm.getServerStatus());

                        RingLogger.getActivityLogger().debug("[" + channelServerForm.getAction() + " ChannelServerAction] activistName : " + activistName + " channelServerDTO --> " + new Gson().toJson(channelServerDTO));
                        myAppError = scheduler.updateChannelServer(channelServerDTO);
                        if (myAppError.getERROR_TYPE() > MyAppError.NOERROR) {
                            loggger.error("Error in [ChannelServerAction] updateChannelServer: " + myAppError.getERROR_TYPE() + " ErrorMessage : " + myAppError.getErrorMessage());
                            channelServerForm.setMessage(myAppError.getErrorMessage());
                            request.getSession(true).setAttribute("error", "2");
                            target = FAILURE;
                        } else {
                            ChannelServerLoader.getInstance().forceReload();
                        }
                        break;
                    default:
                        target = SUCCESS;
                        break;
                }
            }
        } catch (NullPointerException e) {
            loggger.error(e);
            target = FAILURE;
        }
        if (SUCCESS.equals(target)) {
            return new ActionForward(mapping.findForward(target).getPath(), true);
        }
        return mapping.findForward(target);
    }
}
