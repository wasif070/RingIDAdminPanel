/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.servers.channelservers;

import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.utils.Constants;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author mamun
 */
public class ChannelServerLoader {

    private final static long LOADING_INTERVAL = 60 * 60 * 1000;
    private long loadingTime = 0;
    private Map<Integer, ChannelServerDTO> channelServerHashMap;
    private Logger logger = RingLogger.getConfigPortalLogger();

    private ChannelServerLoader() {
        channelServerHashMap = new HashMap<>();
    }

    private static class ChannelServerLoaderHolder {

        private static final ChannelServerLoader INSTANCE = new ChannelServerLoader();
    }

    public static ChannelServerLoader getInstance() {
        return ChannelServerLoaderHolder.INSTANCE;
    }

    private void forceLoadData() {
        channelServerHashMap = new HashMap<>();
        channelServerHashMap = new ChannelServerDAO().getChannelServerHashMap();
    }

    private void checkForReload() {
        long currentTime = System.currentTimeMillis();
        if (currentTime - loadingTime > LOADING_INTERVAL) {
            loadingTime = currentTime;
            forceLoadData();
        }
    }

    public synchronized void forceReload() {
        loadingTime = System.currentTimeMillis();
        forceLoadData();
    }

    private boolean contains(ChannelServerDTO dto, String searchText) {
        return (String.valueOf(dto.getId()).contains(searchText))
                || (dto.getServerIP() != null && dto.getServerIP().contains(searchText))
                || (String.valueOf(dto.getRegisterPort()).contains(searchText))
                || (String.valueOf(dto.getCurrentRegistration()).contains(searchText))
                || (String.valueOf(dto.getMaxRegistration()).contains(searchText));
    }

    private List<ChannelServerDTO> getChannelServerData(ChannelServerDTO sdto) {
        checkForReload();
        List<ChannelServerDTO> data = new ArrayList<>();
        Set set = channelServerHashMap.entrySet();
        Iterator i = set.iterator();
        while (i.hasNext()) {
            Map.Entry me = (Map.Entry) i.next();
            ChannelServerDTO dto = (ChannelServerDTO) me.getValue();
            if (sdto.getSearchText() != null && sdto.getSearchText().length() > 0 && !contains(dto, sdto.getSearchText())) {
                continue;
            }
            data.add(dto);
        }
        if (sdto.getSortType() == Constants.DESC_SORT) {
            Collections.sort(data, new ChannelServerDTO.CompDSC());
        } else {
            Collections.sort(data, new ChannelServerDTO.CompASC());
        }
        return data;
    }

    public List<ChannelServerDTO> getChannelServerList(ChannelServerDTO sdto) {
        List<ChannelServerDTO> channelServerDTOs = getChannelServerData(sdto);
        logger.debug("ChannelServerLoader[getChannelServerList] Size: " + channelServerDTOs.size());
        return channelServerDTOs;
    }

    public ChannelServerDTO getChannelServerDTO(int id) {
        checkForReload();
        if (channelServerHashMap.containsKey(id)) {
            return channelServerHashMap.get(id);
        }
        return null;
    }
}
