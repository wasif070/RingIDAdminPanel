/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.servers.voiceServers;

import com.ringid.admin.BaseDTO;
import com.ringid.admin.utils.Constants;
import java.util.Comparator;

/**
 *
 * @author Kazi Nasir Uddin Oly
 * @Date Jun 12, 2016
 */
public class VoiceServerDTO extends BaseDTO {

    private int id;
    private int newId;
    private String serverIP;
    private int registerPort;
    private String countryShortCode;
    private int currentRegistration;
    private int maxRegistration;
    private int speciality;
    private int p2pSessions;
    private int serverStatus;
    private boolean isSearchByIP = false;

    public int getNewId() {
        return newId;
    }

    public void setNewId(int newId) {
        this.newId = newId;
    }

    public boolean isIsSearchByIP() {
        return isSearchByIP;
    }

    public void setIsSearchByIP(boolean isSearchByIP) {
        this.isSearchByIP = isSearchByIP;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSpeciality() {
        return speciality;
    }

    public void setSpeciality(int speciality) {
        this.speciality = speciality;
    }

    public int getServerStatus() {
        return serverStatus;
    }

    public void setServerStatus(int serverStatus) {
        this.serverStatus = serverStatus;
    }

    public String getServerIP() {
        return serverIP;
    }

    public void setServerIP(String serverIP) {
        this.serverIP = serverIP;
    }

    public int getRegisterPort() {
        return registerPort;
    }

    public void setRegisterPort(int registerPort) {
        this.registerPort = registerPort;
    }

    public String getCountryShortCode() {
        return countryShortCode;
    }

    public void setCountryShortCode(String countryShortCode) {
        this.countryShortCode = countryShortCode;
    }

    public int getCurrentRegistration() {
        return currentRegistration;
    }

    public void setCurrentRegistration(int currentRegistration) {
        this.currentRegistration = currentRegistration;
    }

    public int getMaxRegistration() {
        return maxRegistration;
    }

    public void setMaxRegistration(int maxRegistration) {
        this.maxRegistration = maxRegistration;
    }

    public int getP2pSessions() {
        return p2pSessions;
    }

    public void setP2pSessions(int p2pSessions) {
        this.p2pSessions = p2pSessions;
    }

    public static class CompASC implements Comparator<VoiceServerDTO> {

        @Override
        public int compare(VoiceServerDTO o1, VoiceServerDTO o2) {
            switch (column) {
                case Constants.COLUMN_ONE:
                    return o1.getId() - o2.getId();
                case Constants.COLUMN_TWO:
                    return o1.getServerIP().compareTo(o2.getServerIP());
                default:
                    return o1.getId() - o2.getId();
            }
        }
    }

    public static class CompDSC implements Comparator<VoiceServerDTO> {

        @Override
        public int compare(VoiceServerDTO o1, VoiceServerDTO o2) {
            switch (column) {
                case Constants.COLUMN_ONE:
                    return o2.getId() - o1.getId();
                case Constants.COLUMN_TWO:
                    return o2.getServerIP().compareTo(o1.getServerIP());
                default:
                    return o2.getId() - o1.getId();
            }
        }
    }

}
