/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.servers.voiceServers;

import com.ringid.admin.BaseForm;
import com.ringid.admin.dto.adminAuth.LoginDTO;
import com.ringid.admin.projectMenu.MenuNames;
import com.ringid.admin.utils.Constants;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

/**
 *
 * @author Kazi Nasir Uddin Oly
 * @Date Jun 12, 2016
 */
public class VoiceServerForm extends BaseForm {

    private int id;
    private int newId;
    private String serverIP;
    private String registerPort;
    private String countryShortCode;
    private String currentRegistration;
    private String maxRegistration;
    private String speciality;
    private String p2pSessions;
    private String serverStatus;

    private ArrayList<VoiceServerDTO> voiceServerDTO = new ArrayList<VoiceServerDTO>();

    public int getNewId() {
        return newId;
    }

    public void setNewId(int newId) {
        this.newId = newId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getServerIP() {
        return serverIP;
    }

    public void setServerIP(String serverIP) {
        this.serverIP = serverIP;
    }

    public String getRegisterPort() {
        return registerPort;
    }

    public void setRegisterPort(String registerPort) {
        this.registerPort = registerPort;
    }

    public String getCountryShortCode() {
        return countryShortCode;
    }

    public void setCountryShortCode(String countryShortCode) {
        this.countryShortCode = countryShortCode;
    }

    public String getCurrentRegistration() {
        return currentRegistration;
    }

    public void setCurrentRegistration(String currentRegistration) {
        this.currentRegistration = currentRegistration;
    }

    public String getMaxRegistration() {
        return maxRegistration;
    }

    public void setMaxRegistration(String maxRegistration) {
        this.maxRegistration = maxRegistration;
    }

    public String getP2pSessions() {
        return p2pSessions;
    }

    public void setP2pSessions(String p2pSessions) {
        this.p2pSessions = p2pSessions;
    }

    public ArrayList<VoiceServerDTO> getVoiceServerDTO() {
        return voiceServerDTO;
    }

    public void setVoiceServerDTO(ArrayList<VoiceServerDTO> voiceServerDTO) {
        this.voiceServerDTO = voiceServerDTO;
    }

    public String getSpeciality() {
        return speciality;
    }

    public void setSpeciality(String speciality) {
        this.speciality = speciality;
    }

    public String getServerStatus() {
        return serverStatus;
    }

    public void setServerStatus(String serverStatus) {
        this.serverStatus = serverStatus;
    }

    @Override
    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        ActionErrors errors = new ActionErrors();
        LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        String FEATURE_NAME = MenuNames.VOICE_SERVER;
        if (loginDTO == null) {
            errors.add("auth", new ActionMessage("UnAuthorized Access"));
        } else if (Constants.ADD == getAction() || Constants.UPDATE == getAction()) {
            if (!(loginDTO.getPermissionLevel() == Constants.SUPER_ADMIN) && !(loginDTO.getFeauterMapList().get(FEATURE_NAME).getHasAddPermission()
                    || loginDTO.getFeauterMapList().get(FEATURE_NAME).getHasModifyPermission())) {
                errors.add("auth", new ActionMessage("UnAuthorized Access"));
            } else {
                if (getServerIP() == null || getServerIP().length() == 0) {
                    errors.add("serverIP", new ActionMessage("errors.serverIP.required"));
                }
                if (getRegisterPort() == null || getRegisterPort().length() == 0) {
                    errors.add("registerPort", new ActionMessage("errors.registerPort.required"));
                }
            }
        }
        return errors;
    }

}
