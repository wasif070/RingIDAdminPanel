/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.servers.voiceServers;

import com.ringid.admin.BaseDAO;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.utils.Constants;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Kazi Nasir Uddin Oly
 * @Date Jun 12, 2016
 */
public class VoiceServerLoader extends BaseDAO {

    private final static long LOADING_INTERVAL = 60 * 60 * 1000;
    private long loadingTime = 0;
    private HashMap<Integer, VoiceServerDTO> voiceServerHashMapList;
    private static VoiceServerLoader voiceServerLoader = null;
    static Logger logger = RingLogger.getConfigPortalLogger();

    public VoiceServerLoader() {

    }

    public static VoiceServerLoader getInstance() {
        if (voiceServerLoader == null) {
            createLoader();
        }
        return voiceServerLoader;
    }

    private synchronized static void createLoader() {
        if (voiceServerLoader == null) {
            voiceServerLoader = new VoiceServerLoader();
        }
    }

    private void forceLoadData() {
        voiceServerHashMapList = new HashMap<>();
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            stmt = db.connection.createStatement();
            String sql = "SELECT id, serverIP, registerPort, countryShortCode, currentRegitration, maxRegitration, speciality, p2pSessions, serverStatus FROM voiceservers;";
            rs = stmt.executeQuery(sql);
            while (rs.next()) {
                VoiceServerDTO dto = new VoiceServerDTO();
                dto.setId(rs.getInt("id"));
                dto.setServerIP(rs.getString("serverIP"));
                dto.setRegisterPort(rs.getInt("registerPort"));
                dto.setCountryShortCode(rs.getString("countryShortCode"));
                dto.setCurrentRegistration(rs.getInt("currentRegitration"));
                dto.setMaxRegistration(rs.getInt("maxRegitration"));
                dto.setSpeciality(rs.getInt("speciality"));
                dto.setP2pSessions(rs.getInt("p2pSessions"));
                dto.setServerStatus(rs.getInt("serverStatus"));
                voiceServerHashMapList.put(dto.getId(), dto);
            }
            logger.debug(sql + "::" + voiceServerHashMapList.size());

        } catch (Exception e) {
            logger.debug("Voice Server List Exception...", e);
        } finally {
            close();
        }
    }

    private void checkForReload() {
        long currentTime = System.currentTimeMillis();
        if (currentTime - loadingTime > LOADING_INTERVAL) {
            loadingTime = currentTime;
            forceLoadData();
        }
    }

    public synchronized void forceReload() {
        loadingTime = System.currentTimeMillis();
        forceLoadData();
    }

    private boolean contains(VoiceServerDTO dto, String searchText) {
        return (String.valueOf(dto.getId()).contains(searchText))
                || (dto.getServerIP() != null && dto.getServerIP().contains(searchText))
                || (String.valueOf(dto.getRegisterPort()).contains(searchText))
                || (dto.getCountryShortCode() != null && dto.getCountryShortCode().toLowerCase().contains(searchText))
                || (String.valueOf(dto.getCurrentRegistration()).contains(searchText))
                || (String.valueOf(dto.getMaxRegistration()).contains(searchText))
                || (String.valueOf(dto.getSpeciality()).contains(searchText))
                || (String.valueOf(dto.getP2pSessions()).contains(searchText));
    }

    private ArrayList<VoiceServerDTO> getVoiceServerData(VoiceServerDTO sdto) {
        checkForReload();
        ArrayList<VoiceServerDTO> data = new ArrayList<>();
        Set set = voiceServerHashMapList.entrySet();
        Iterator i = set.iterator();
        while (i.hasNext()) {
            Map.Entry me = (Map.Entry) i.next();
            VoiceServerDTO dto = (VoiceServerDTO) me.getValue();
            if (sdto.getSearchText() != null && sdto.getSearchText().length() > 0 && !contains(dto, sdto.getSearchText())) {
                continue;
            }
            data.add(dto);
        }

        if (sdto.getSortType() == Constants.DESC_SORT) {
            Collections.sort(data, new VoiceServerDTO.CompDSC());
        } else {
            Collections.sort(data, new VoiceServerDTO.CompASC());
        }
        return data;
    }

    public synchronized List<VoiceServerDTO> getVoiceServerList(VoiceServerDTO sdto) {
        List<VoiceServerDTO> voiceServerList = getVoiceServerData(sdto);
        logger.debug("getVoiceServerList Size: " + voiceServerList.size());
        return voiceServerList;
    }

    public synchronized List<VoiceServerDTO> getVoiceServerList(int columnValue, int sortType, VoiceServerDTO sdto) {
        List<VoiceServerDTO> voiceServerList = getVoiceServerData(sdto);
        if (sdto.getSortType() == Constants.DESC_SORT) {
            Collections.sort(voiceServerList, new VoiceServerDTO.CompDSC());
        } else {
            Collections.sort(voiceServerList, new VoiceServerDTO.CompASC());
        }
        logger.debug("getVoiceServerList Size: " + voiceServerList.size());
        return voiceServerList;
    }

    public synchronized ArrayList<VoiceServerDTO> getVoiceServerListForDownload() {
        checkForReload();
        ArrayList<VoiceServerDTO> data = new ArrayList<>(voiceServerHashMapList.values());
        return data;
    }

    public synchronized VoiceServerDTO getVoiceServerDTO(int id) {
        checkForReload();
        if (voiceServerHashMapList.containsKey(id)) {
            return voiceServerHashMapList.get(id);
        }
        return null;
    }
}
