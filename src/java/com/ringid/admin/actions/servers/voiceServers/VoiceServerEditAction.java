/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.servers.voiceServers;

import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author Kazi Nasir Uddin Oly
 * @Date Jun 12, 2016
 */
public class VoiceServerEditAction extends BaseAction {

    Logger logger = RingLogger.getConfigPortalLogger();

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        String target = checkAuthentication(mapping, form, request);
        try {
            if (target.equals(SUCCESS)) {
                int id = 0;
                VoiceServerForm voiceServerForm = (VoiceServerForm) form;
                if (request.getParameter("id") != null) {
                    id = Integer.parseInt(request.getParameter("id"));
                }
                VoiceServerTaskScheduler scheduler = VoiceServerTaskScheduler.getInstance();
                VoiceServerDTO voiceServerDTO = scheduler.getVoiceServerInfoById(id);
                if (voiceServerDTO != null) {
                    voiceServerForm.setNewId(voiceServerDTO.getId());
                    voiceServerForm.setId(voiceServerDTO.getId());
                    voiceServerForm.setServerIP(voiceServerDTO.getServerIP());
                    voiceServerForm.setRegisterPort(String.valueOf(voiceServerDTO.getRegisterPort()));
                    voiceServerForm.setCountryShortCode(String.valueOf(voiceServerDTO.getCountryShortCode()));
                    voiceServerForm.setCurrentRegistration(String.valueOf(voiceServerDTO.getCurrentRegistration()));
                    voiceServerForm.setMaxRegistration(String.valueOf(voiceServerDTO.getMaxRegistration()));
                    voiceServerForm.setSpeciality(String.valueOf(voiceServerDTO.getSpeciality()));
                    voiceServerForm.setP2pSessions(String.valueOf(voiceServerDTO.getP2pSessions()));
                    voiceServerForm.setServerStatus(String.valueOf(voiceServerDTO.getServerStatus()));
                    target = SUCCESS;
                }
            }
        } catch (Exception e) {
            logger.error("VoiceServerEditAction Exception: " + e);
            target = FAILURE;
        }
        return mapping.findForward(target);
    }
}
