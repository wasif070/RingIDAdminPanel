/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.servers.voiceServers;

import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.Constants;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author Kazi Nasir Uddin Oly
 * @Date Jun 12, 2016
 */
public class VoiceServerListAction extends BaseAction {

    List<VoiceServerDTO> list = new ArrayList<>();

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = FAILURE;
        try {
            VoiceServerForm voiceServerForm = (VoiceServerForm) form;
            VoiceServerDTO voiceServerDTO = new VoiceServerDTO();
            VoiceServerTaskScheduler scheduler = VoiceServerTaskScheduler.getInstance();

            if (request.getParameter("ForceReload") != null && "1".equals(request.getParameter("ForceReload"))) {
                VoiceServerLoader.getInstance().forceReload();
            }
            if (voiceServerForm.getSearchText() != null && voiceServerForm.getSearchText().length() > 0) {
                voiceServerDTO.setSearchText(voiceServerForm.getSearchText().trim().toLowerCase());
            }
            if (voiceServerForm.getColumn() <= 0) {
                voiceServerForm.setColumn(Constants.COLUMN_ONE);
                voiceServerForm.setSort(Constants.DESC_SORT);
            }
            voiceServerDTO.setColumn(voiceServerForm.getColumn());
            voiceServerDTO.setSortType(voiceServerForm.getSort());
            list = scheduler.getVoiceServerList(voiceServerDTO);
            request.getSession(true).setAttribute(Constants.DATA_ROWS, list);
            managePages(request, voiceServerForm, list.size());
            request.getSession(true).setAttribute("search", voiceServerDTO.getServerIP());
        } catch (NullPointerException e) {
            target = FAILURE;
        }
        return mapping.findForward(target);
    }
}
