/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ringid.admin.actions.servers.voiceServers;

import com.ringid.admin.utils.log.RingLogger;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Kazi Nasir Uddin Oly 
 * @Date Jun 12, 2016
 */
@WebServlet(name = "DownloadVoiceServers", urlPatterns = {"/Download/DownloadVoiceServers"})
public class DownloadVoiceServers extends HttpServlet{
    static Logger logger = RingLogger.getConfigPortalLogger();
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/csv");
        response.setHeader("Content-Disposition", "attachment; filename=\"VoiceServers.csv\"");
        try{
            OutputStream outputStream = response.getOutputStream();
            ArrayList<VoiceServerDTO> list = VoiceServerTaskScheduler.getInstance().getVoiceServerListForDownload();
            outputStream.write("Server IP".getBytes());
            outputStream.write(",".getBytes());
            outputStream.write("Register Port".getBytes());
            outputStream.write(",".getBytes());
            outputStream.write("Short Country Code".getBytes());
            outputStream.write(",".getBytes());
            outputStream.write("Current Registration".getBytes());
            outputStream.write(",".getBytes());
            outputStream.write("Maximum Registration".getBytes());
            outputStream.write(",".getBytes());
            outputStream.write("Speciality".getBytes());
            outputStream.write(",".getBytes()); 
            outputStream.write("P2P Sessions".getBytes());
            outputStream.write(",".getBytes());
            outputStream.write("Server Status".getBytes());
            outputStream.write("\n".getBytes());
            for(VoiceServerDTO d: list){
                outputStream.write(d.getServerIP().getBytes());
                outputStream.write(",".getBytes());
                outputStream.write(String.valueOf(d.getRegisterPort()).getBytes());
                outputStream.write(",".getBytes());
                outputStream.write(d.getCountryShortCode().getBytes());
                outputStream.write(",".getBytes());
                outputStream.write(String.valueOf(d.getCurrentRegistration()).getBytes());
                outputStream.write(",".getBytes());
                outputStream.write(String.valueOf(d.getMaxRegistration()).getBytes());
                outputStream.write(",".getBytes());
                outputStream.write(String.valueOf(d.getSpeciality()).getBytes());
                outputStream.write(",".getBytes());                
                outputStream.write(String.valueOf(d.getP2pSessions()).getBytes());
                outputStream.write(",".getBytes());
                outputStream.write(String.valueOf(d.getServerStatus()).getBytes());
                outputStream.write("\n".getBytes());
            }
            outputStream.flush();
            outputStream.close();
        } catch(Exception e){
            logger.error("DownloadVoiceServers Exception: " + e.toString());
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }

    @Override
    public String getServletInfo() {
        return "Short Description";
    }
}
