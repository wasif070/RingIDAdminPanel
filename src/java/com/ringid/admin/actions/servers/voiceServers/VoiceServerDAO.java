/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.servers.voiceServers;

import com.ringid.admin.BaseDAO;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.utils.MyAppError;
import java.sql.SQLException;
import java.util.ArrayList;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Kazi Nasir Uddin Oly
 * @Date Jun 12, 2016
 */
public class VoiceServerDAO extends BaseDAO {

    static Logger logger = RingLogger.getConfigPortalLogger();

    public VoiceServerDAO() {

    }

    public int getMaxId() {
        int id = 0;
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            stmt = db.connection.createStatement();
            String query = "SELECT max(id) AS maxId FROM voiceservers;";
            rs = stmt.executeQuery(query);
            if (rs.next()) {
                id = rs.getInt("maxId");
            }
        } catch (Exception e) {
            logger.error("Exception in VoiceServerDAO[getMaxId] -> " + e);
        } finally {
            close();
        }
        return id;
    }

    public MyAppError addVoiceServerInfo(VoiceServerDTO dto) {
        MyAppError error = new MyAppError();
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            stmt = db.connection.createStatement();
            String query = "INSERT INTO voiceservers(id, serverIP, registerPort, countryShortCode, currentRegitration, maxRegitration, speciality, p2pSessions, serverStatus)"
                    + " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?);";
            ps = db.connection.prepareStatement(query);
            int index = 1;
            ps.setInt(index++, dto.getId());
            ps.setString(index++, dto.getServerIP());
            ps.setInt(index++, dto.getRegisterPort());
            ps.setString(index++, dto.getCountryShortCode());
            ps.setInt(index++, dto.getCurrentRegistration());
            ps.setInt(index++, dto.getMaxRegistration());
            ps.setInt(index++, dto.getSpeciality());
            ps.setInt(index++, dto.getP2pSessions());
            ps.setInt(index++, dto.getServerStatus());
            ps.execute();
            ps.close();
        } catch (SQLException e) {
            error.setERROR_TYPE(error.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            logger.debug("Single addVoiceServerInfo SQL Exception --> " + e);
        } catch (Exception e) {
            error.setERROR_TYPE(error.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            logger.debug("Single addVoiceServerInfo Exception --> " + e);
        } finally {
            close();
        }
        return error;
    }

    public MyAppError addVoiceServerInfo(ArrayList<VoiceServerDTO> maps) {
        MyAppError error = new MyAppError();
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            String query = "INSERT INTO voiceservers (id, serverIP, registerPort, countryShortCode, currentRegitration, maxRegitration, speciality, p2pSessions, serverStatus)          \n"
                    + "         SELECT * FROM (SELECT ? AS c0, SELECT ? AS c1, ? AS c2, ? AS c3, ? AS c4, ? AS c5, ? AS c6, ? AS c7, ? AS c8) AS tmp                                                         \n"
                    + "         WHERE NOT EXISTS (                                                                                        \n"
                    + "         SELECT serverIP, registerPort, countryShortCode FROM voiceservers WHERE serverIP = ? AND registerPort = ? AND countryShortCode = ?                \n"
                    + "         );";
            ps = db.connection.prepareStatement(query);
            int i = 1;
            for (VoiceServerDTO entry : maps) {
                int index = 1;
                ps.setInt(index++, entry.getId());
                ps.setString(index++, entry.getServerIP());
                ps.setInt(index++, entry.getRegisterPort());
                ps.setString(index++, entry.getCountryShortCode());
                ps.setInt(index++, entry.getCurrentRegistration());
                ps.setInt(index++, entry.getMaxRegistration());
                ps.setInt(index++, entry.getSpeciality());
                ps.setInt(index++, entry.getP2pSessions());
                ps.setInt(index++, entry.getServerStatus());
                ps.setString(index++, entry.getServerIP());
                ps.setInt(index++, entry.getRegisterPort());
                ps.setString(index++, entry.getCountryShortCode());
                ps.addBatch();
                i++;
                if ((i + 1) % 1000 == 0) {
                    ps.executeBatch();
                }
            }
            int a[] = ps.executeBatch();
        } catch (SQLException e) {
            error.setERROR_TYPE(error.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            logger.debug("addVoiceServerInfo Map SQL Exception --> " + e);
        } catch (Exception e) {
            error.setERROR_TYPE(error.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            logger.debug("addVoiceServerInfo Map Exception --> " + e);
        } finally {
            close();
        }
        return error;
    }

    public MyAppError updateVoiceServer(VoiceServerDTO dto) {
        MyAppError error = new MyAppError();
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            String query = "UPDATE voiceservers SET id = ?, serverIP = ?, registerPort = ?, countryShortCode = ?, currentRegitration = ?, maxRegitration = ?, speciality = ?, p2pSessions = ?, serverStatus = ? WHERE id = ?;";
            ps = db.connection.prepareStatement(query);
            int index = 1;
            ps.setInt(index++, dto.getNewId());
            ps.setString(index++, dto.getServerIP());
            ps.setInt(index++, dto.getRegisterPort());
            ps.setString(index++, dto.getCountryShortCode());
            ps.setInt(index++, dto.getCurrentRegistration());
            ps.setInt(index++, dto.getMaxRegistration());
            ps.setInt(index++, dto.getSpeciality());
            ps.setInt(index++, dto.getP2pSessions());
            ps.setInt(index++, dto.getServerStatus());
            ps.setInt(index++, dto.getId());
            ps.execute();
        } catch (SQLException e) {
            error.setERROR_TYPE(error.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            logger.debug("updateVoiceServer SQL Exception --> " + e);
        } catch (Exception e) {
            error.setERROR_TYPE(error.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            logger.debug("updateVoiceServer Exception --> " + e);
        } finally {
            close();
        }
        return error;
    }

    public MyAppError deleteVoiceServer(int id) {
        MyAppError error = new MyAppError();
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            stmt = db.connection.createStatement();
            String sql = "DELETE FROM voiceservers WHERE id = " + id + ";";
            int a = stmt.executeUpdate(sql);
        } catch (SQLException e) {
            error.setERROR_TYPE(error.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            logger.debug("deleteVoiceServer SQL Exception --> " + e);
        } catch (Exception e) {
            error.setERROR_TYPE(error.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            logger.debug("deleteVoiceServer Exception --> " + e);
        } finally {
            close();
        }
        return error;
    }
}
