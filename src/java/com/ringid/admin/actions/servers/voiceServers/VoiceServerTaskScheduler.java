/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.servers.voiceServers;

import com.ringid.admin.utils.MyAppError;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Kazi Nasir Uddin Oly
 * @Date Jun 12, 2016
 */
public class VoiceServerTaskScheduler {

    private VoiceServerTaskScheduler() {

    }

    private static class VoiceServerTaskSchedulerHolder {

        private static final VoiceServerTaskScheduler INSTANCE = new VoiceServerTaskScheduler();
    }

    public static VoiceServerTaskScheduler getInstance() {
        return VoiceServerTaskSchedulerHolder.INSTANCE;
    }

    public List<VoiceServerDTO> getVoiceServerList(VoiceServerDTO dto) {
        return VoiceServerLoader.getInstance().getVoiceServerList(dto);
    }

    public List<VoiceServerDTO> getVoiceServerList(int columnValue, int sortType, VoiceServerDTO dto) {
        return VoiceServerLoader.getInstance().getVoiceServerList(columnValue, sortType, dto);
    }

    public int getMaxId() {
        VoiceServerDAO authServerDAO = new VoiceServerDAO();
        return authServerDAO.getMaxId();
    }

    public MyAppError addVoiceServerInfo(VoiceServerDTO p_dto) {
        VoiceServerDAO voiceServerDAO = new VoiceServerDAO();
        return voiceServerDAO.addVoiceServerInfo(p_dto);
    }

    public MyAppError addVoiceServerInfo(ArrayList<VoiceServerDTO> maps) {
        VoiceServerDAO voiceServerDAO = new VoiceServerDAO();
        return voiceServerDAO.addVoiceServerInfo(maps);
    }

    public VoiceServerDTO getVoiceServerInfoById(int id) {
        return VoiceServerLoader.getInstance().getVoiceServerDTO(id);
    }

    public MyAppError deleteVoiceServer(int id) {
        VoiceServerDAO voiceServerDAO = new VoiceServerDAO();
        return voiceServerDAO.deleteVoiceServer(id);
    }

    public MyAppError updateVoiceServer(VoiceServerDTO p_dto) {
        VoiceServerDAO voiceServerDAO = new VoiceServerDAO();
        return voiceServerDAO.updateVoiceServer(p_dto);
    }

    public ArrayList<VoiceServerDTO> getVoiceServerListForDownload() {
        return VoiceServerLoader.getInstance().getVoiceServerListForDownload();
    }

    public VoiceServerDTO getVoiceServerDTO(VoiceServerForm voiceServerForm) {
        VoiceServerDTO voiceServerDTO = new VoiceServerDTO();
        voiceServerDTO.setServerIP(voiceServerForm.getServerIP());
        voiceServerDTO.setRegisterPort(Integer.valueOf(voiceServerForm.getRegisterPort()));
        if (voiceServerForm.isNull(voiceServerForm.getCountryShortCode())) {
            voiceServerDTO.setCountryShortCode("CA");
        } else {
            voiceServerDTO.setCountryShortCode(voiceServerForm.getCountryShortCode());
        }

        if (!voiceServerForm.isNull(voiceServerForm.getCurrentRegistration())) {
            voiceServerDTO.setCurrentRegistration(Integer.valueOf(voiceServerForm.getCurrentRegistration()));
        }

        if (voiceServerForm.isNull(voiceServerForm.getMaxRegistration())) {
            voiceServerDTO.setMaxRegistration(10000);
        } else {
            voiceServerDTO.setMaxRegistration(Integer.valueOf(voiceServerForm.getMaxRegistration()));
        }

        if (!voiceServerForm.isNull(voiceServerForm.getSpeciality())) {
            voiceServerDTO.setSpeciality(Integer.valueOf(voiceServerForm.getSpeciality()));
        }

        if (!voiceServerForm.isNull(voiceServerForm.getP2pSessions())) {
            voiceServerDTO.setSpeciality(Integer.valueOf(voiceServerForm.getP2pSessions()));
        }

        if (voiceServerForm.isNull(voiceServerForm.getServerStatus())) {
            voiceServerDTO.setServerStatus(1);
        } else {
            voiceServerDTO.setServerStatus(Integer.valueOf(voiceServerForm.getServerStatus()));
        }
        return voiceServerDTO;
    }

}
