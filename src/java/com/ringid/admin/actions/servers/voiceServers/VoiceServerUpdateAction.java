/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.servers.voiceServers;

import com.google.gson.Gson;
import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.dto.adminAuth.LoginDTO;
import com.ringid.admin.utils.Constants;
import com.ringid.admin.utils.MyAppError;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author Kazi Nasir Uddin Oly
 * @Date Jun 12, 2016
 */
public class VoiceServerUpdateAction extends BaseAction {

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        String target = checkAuthentication(mapping, form, request);
        String activistName;
        try {
            LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
            activistName = loginDTO.getUserName();
        } catch (Exception e) {
            target = UNAUTHORIZED;
            return mapping.findForward(target);
        }
        try {
            if (target.equals(SUCCESS)) {
                VoiceServerForm voiceServerForm = (VoiceServerForm) form;
                VoiceServerTaskScheduler scheduler = VoiceServerTaskScheduler.getInstance();
                VoiceServerDTO voiceServerDTO = scheduler.getVoiceServerDTO(voiceServerForm);
                voiceServerDTO.setId(voiceServerForm.getId());
                voiceServerDTO.setNewId(voiceServerForm.getNewId());
                if (voiceServerDTO.getNewId() <= 0) {
                    voiceServerDTO.setNewId(voiceServerDTO.getId());
                }
                RingLogger.getActivityLogger().debug("[VoiceServerUpdateAction] activistName : " + activistName + " voiceServerDTO --> " + new Gson().toJson(voiceServerDTO));
                MyAppError error = scheduler.updateVoiceServer(voiceServerDTO);
                if (error.getERROR_TYPE() > 0) {
                    target = FAILURE;
                    voiceServerForm.setMessage(error.getErrorMessage());
                    request.getSession(true).setAttribute("error", "2");
                } else {
                    VoiceServerLoader.getInstance().forceReload();
                    notifyAuthServers(Constants.RELOAD_VOICE_SERVERS, null);
                }
            }
        } catch (Exception e) {
        }
        return mapping.findForward(target);
    }
}
