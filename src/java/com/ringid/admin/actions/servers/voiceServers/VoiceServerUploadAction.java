/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.servers.voiceServers;

import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.utils.Constants;
import com.ringid.admin.utils.MyAppError;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author Kazi Nasir Uddin Oly
 * @Date Jun 12, 2016
 */
public class VoiceServerUploadAction extends BaseAction {

    static Logger logger = RingLogger.getConfigPortalLogger();
    String line = "";
    String cvsSplitBy = ",";

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = checkAuthentication(mapping, form, request);
        if (target.equals(SUCCESS)) {
            String[] dataArray = getDataFromFile(form);
            try {
                ArrayList<VoiceServerDTO> voiceServerList = new ArrayList<>();

                for (String s : dataArray) {
                    String[] voiceServer = s.split(cvsSplitBy);
                    String serverIP = voiceServer[0];
                    String registerPort = voiceServer[1];
                    String countryShortCode = voiceServer[2];
                    String currentRegistration = voiceServer[3];
                    String maxRegistration = voiceServer[4];
                    String speciality = voiceServer[5];
                    String p2pSessions = voiceServer[6];
                    String serverStatus = voiceServer[7];
                    if (serverIP.equalsIgnoreCase("Server IP")) {
                        continue;
                    }

                    VoiceServerDTO voiceServerDTO = new VoiceServerDTO();
                    voiceServerDTO.setServerIP(serverIP);
                    voiceServerDTO.setRegisterPort(Integer.valueOf(registerPort));
                    voiceServerDTO.setCountryShortCode(countryShortCode);
                    voiceServerDTO.setCurrentRegistration(Integer.valueOf(currentRegistration));
                    voiceServerDTO.setMaxRegistration(Integer.valueOf(maxRegistration));
                    voiceServerDTO.setSpeciality(Integer.valueOf(speciality));
                    voiceServerDTO.setP2pSessions(Integer.valueOf(p2pSessions));
                    voiceServerDTO.setServerStatus(Integer.valueOf(serverStatus));

                    voiceServerList.add(voiceServerDTO);
                }

                MyAppError error = VoiceServerTaskScheduler.getInstance().addVoiceServerInfo(voiceServerList);
                if (error.getERROR_TYPE() > 0) {
                    target = FAILURE;
                } else {
                    VoiceServerLoader.getInstance().forceReload();
                    notifyAuthServers(Constants.RELOAD_VOICE_SERVERS, null);
                }
            } catch (Exception e) {
                logger.debug("VoiceServerUploadAction Exception: " + e);
                target = FAILURE;
            }
        }
        return mapping.findForward(target);
    }
}
