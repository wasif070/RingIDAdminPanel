/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.servers.smsServers;

import com.ringid.admin.BaseDAO;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.utils.MyAppError;
import java.sql.SQLException;
import java.util.ArrayList;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Oly
 */
public class SmsServerDAO extends BaseDAO {

    static Logger logger = RingLogger.getConfigPortalLogger();

    public SmsServerDAO() {

    }

    public int getMaxId() {
        int id = 0;
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            stmt = db.connection.createStatement();
            String query = "SELECT max(id) AS maxId FROM smsservers;";
            rs = stmt.executeQuery(query);
            if (rs.next()) {
                id = rs.getInt("maxId");
            }
        } catch (Exception e) {
            logger.error("Exception in AuthSeverDAO[getMaxId] -> " + e);
        } finally {
            close();
        }
        return id;
    }

    public MyAppError addSmsServerInfo(SmsServerDTO dto) {
        MyAppError error = new MyAppError();
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            stmt = db.connection.createStatement();
            String query = "INSERT INTO smsservers(id, server_url, sms_verification_url, server_status) VALUES (?, ?, ?, ?);";
            ps = db.connection.prepareStatement(query);
            int index = 1;
            ps.setInt(index++, dto.getId());
            ps.setString(index++, dto.getUrl());
            ps.setString(index++, dto.getSmsVerificationUrl());
            ps.setInt(index++, dto.getStatus());
            ps.execute();
            ps.close();
        } catch (SQLException e) {
            error.setERROR_TYPE(error.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            logger.debug("Single addSmsServerInfo SQL Exception --> " + e);
        } catch (Exception e) {
            error.setERROR_TYPE(error.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            logger.debug("Single addSmsServerInfo Exception --> " + e);
        } finally {
            close();
        }
        return error;
    }

    public MyAppError addSmsServerInfo(ArrayList<SmsServerDTO> maps) {
        MyAppError error = new MyAppError();

        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            String query = "INSERT INTO smsservers (id, server_url, sms_verification_url, server_status)                                         "
                    + "         SELECT * FROM (SELECT ? AS c0, SELECT ? AS c1, ? AS c2, ? AS c3) AS tmp                                                                       "
                    + "         WHERE NOT EXISTS (                                                                                          "
                    + "         SELECT server_url, sms_verification_url FROM smsservers WHERE server_url = ? OR sms_verification_url = ? "
                    + "         );";
            ps = db.connection.prepareStatement(query);
            int i = 1;
            for (SmsServerDTO entry : maps) {
                int index = 1;
                ps.setInt(index++, entry.getId());
                ps.setString(index++, entry.getUrl());
                ps.setString(index++, entry.getSmsVerificationUrl());
                ps.setInt(index++, entry.getStatus());
                ps.setString(index++, entry.getUrl());
                ps.setString(index++, entry.getSmsVerificationUrl());
                ps.addBatch();
                i++;
                if ((i + 1) % 1000 == 0) {
                    ps.executeBatch();
                }
            }
            int a[] = ps.executeBatch();
        } catch (SQLException e) {
            error.setERROR_TYPE(error.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            logger.debug("addSmsServerInfo Map SQL Exception --> " + e);
        } catch (Exception e) {
            error.setERROR_TYPE(error.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            logger.debug("addSmsServerInfo Map Exception --> " + e);
        } finally {
            close();
        }
        return error;

    }

    public MyAppError updateSmsServer(SmsServerDTO dto) {
        MyAppError error = new MyAppError();
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            String query = "UPDATE smsservers SET id = ?, server_url = ?, sms_verification_url = ?, server_status = ? WHERE id = ?;";
            ps = db.connection.prepareStatement(query);
            int index = 1;
            ps.setInt(index++, dto.getNewId());
            ps.setString(index++, dto.getUrl());
            ps.setString(index++, dto.getSmsVerificationUrl());
            ps.setInt(index++, dto.getStatus());
            ps.setInt(index++, dto.getId());
            ps.execute();
        } catch (SQLException e) {
            error.setERROR_TYPE(error.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            logger.debug("updateSmsServer SQL Exception  --> " + e);
        } catch (Exception e) {
            error.setERROR_TYPE(error.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            logger.debug("updateSmsServer Exception --> " + e);
        } finally {
            close();
        }
        return error;
    }

    public MyAppError deleteSmsServer(int id) {
        MyAppError error = new MyAppError();
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            stmt = db.connection.createStatement();
            String sql = "DELETE FROM smsservers WHERE id = " + id + ";";
            int a = stmt.executeUpdate(sql);
        } catch (SQLException e) {
            error.setERROR_TYPE(error.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            logger.debug("deleteSmsServer SQL Exception --> " + e);
        } catch (Exception e) {
            error.setERROR_TYPE(error.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            logger.debug("deleteSmsServer Exception --> " + e);
        } finally {
            close();
        }
        return error;
    }

}
