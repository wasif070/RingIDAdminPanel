/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.servers.smsServers;

import com.ringid.admin.BaseForm;
import com.ringid.admin.dto.adminAuth.LoginDTO;
import com.ringid.admin.projectMenu.MenuNames;
import com.ringid.admin.utils.Constants;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

/**
 *
 * @author Kazi Nasir Uddin Oly
 * @Date Jun 2, 2016
 */
public class SmsServerForm extends BaseForm {

    private int id;
    private int newId;
    private String url;
    private String smsVerificationUrl;
    private String status;
    private ArrayList<SmsServerDTO> smsServerList = new ArrayList<SmsServerDTO>();

    public SmsServerForm() {
        super();
    }

    public int getNewId() {
        return newId;
    }

    public void setNewId(int newId) {
        this.newId = newId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getSmsVerificationUrl() {
        return smsVerificationUrl;
    }

    public void setSmsVerificationUrl(String smsVerificationUrl) {
        this.smsVerificationUrl = smsVerificationUrl;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String serverStatus) {
        this.status = serverStatus;
    }

    public ArrayList<SmsServerDTO> getSmsServerList() {
        return smsServerList;
    }

    public void setSmsServerList(ArrayList<SmsServerDTO> smsServerList) {
        this.smsServerList = smsServerList;
    }

    @Override
    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        ActionErrors errors = new ActionErrors();
        LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        String FEATURE_NAME = MenuNames.SMS_SERVER;
        if (loginDTO == null) {
            errors.add("auth", new ActionMessage("UnAuthorized Access"));
        } else if (Constants.ADD == getAction() || Constants.UPDATE == getAction()) {
            if (!(loginDTO.getPermissionLevel() == Constants.SUPER_ADMIN) && !(loginDTO.getFeauterMapList().get(FEATURE_NAME).getHasAddPermission()
                    || loginDTO.getFeauterMapList().get(FEATURE_NAME).getHasModifyPermission())) {
                errors.add("auth", new ActionMessage("UnAuthorized Access"));
            } else {
                if (getUrl() == null || getUrl().length() == 0) {
                    errors.add("url", new ActionMessage("errors.url.required"));
                }
                if (getSmsVerificationUrl() == null || getSmsVerificationUrl().length() == 0) {
                    errors.add("smsVerificationUrl", new ActionMessage("errors.smsVerificationUrl.required"));
                }
            }
        }
        return errors;
    }

}
