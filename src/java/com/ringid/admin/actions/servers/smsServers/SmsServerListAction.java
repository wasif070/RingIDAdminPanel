/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.servers.smsServers;

import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.Constants;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author Kazi Nasir Uddin Oly
 * @Date Jun 2, 2016
 */
public class SmsServerListAction extends BaseAction {

    ArrayList<SmsServerDTO> list = new ArrayList<>();

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = checkAuthentication(mapping, form, request);
        try {
            SmsServerForm smsServerForm = (SmsServerForm) form;
            SmsServerDTO smsServerDTO = new SmsServerDTO();
            SmsServerTaskSchedular schedular = SmsServerTaskSchedular.getInstance();
            if (request.getParameter("ForceReload") != null && "1".equals(request.getParameter("ForceReload"))) {
                SmsServerLoader.getInstance().forceReload();
            }
            if (smsServerForm.getSearchText() != null && smsServerForm.getSearchText().length() > 0) {
                smsServerDTO.setSearchText(smsServerForm.getSearchText().trim().toLowerCase());
            }
            if (smsServerForm.getColumn() <= 0) {
                smsServerForm.setColumn(Constants.COLUMN_ONE);
                smsServerForm.setSort(Constants.DESC_SORT);
            }
            smsServerDTO.setColumn(smsServerForm.getColumn());
            smsServerDTO.setSortType(smsServerForm.getSort());
            list = schedular.getSmsServerList(smsServerDTO);
            request.getSession(true).setAttribute(Constants.DATA_ROWS, list);
            managePages(request, smsServerForm, list.size());
            request.getSession(true).setAttribute("search", smsServerForm.getUrl());
        } catch (NullPointerException e) {
            target = FAILURE;
        }

        return mapping.findForward(target);

    }

}
