/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.servers.smsServers;

import com.ringid.admin.BaseDTO;
import com.ringid.admin.utils.Constants;
import java.util.ArrayList;
import java.util.Comparator;

/**
 *
 * @author Oly
 */
public class SmsServerDTO extends BaseDTO {

    private int id;
    private int newId;
    private String url;
    private String smsVerificationUrl;
    private int status;
    private ArrayList<SmsServerDTO> smsServerList;

    public int getNewId() {
        return newId;
    }

    public void setNewId(int newId) {
        this.newId = newId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getSmsVerificationUrl() {
        return smsVerificationUrl;
    }

    public void setSmsVerificationUrl(String smsVerificationUrl) {
        this.smsVerificationUrl = smsVerificationUrl;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public ArrayList<SmsServerDTO> getSmsServerList() {
        return smsServerList;
    }

    public void setSmsServerList(ArrayList<SmsServerDTO> smsServerList) {
        this.smsServerList = smsServerList;
    }

    public static class CompASC implements Comparator<SmsServerDTO> {

        @Override
        public int compare(SmsServerDTO o1, SmsServerDTO o2) {
            switch (column) {
                case Constants.COLUMN_ONE:
                    return o1.getId() - o2.getId();
                default:
                    return o1.getId() - o2.getId();
            }
        }
    }

    public static class CompDSC implements Comparator<SmsServerDTO> {

        @Override
        public int compare(SmsServerDTO o1, SmsServerDTO o2) {
            switch (column) {
                case Constants.COLUMN_ONE:
                    return o2.getId() - o1.getId();
                default:
                    return o2.getId() - o1.getId();
            }
        }
    }
}
