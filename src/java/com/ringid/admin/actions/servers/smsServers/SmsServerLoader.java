/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.servers.smsServers;

import com.ringid.admin.BaseDAO;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.utils.Constants;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Oly
 */
public class SmsServerLoader extends BaseDAO {

    private final static long LOADING_INTERVAL = 60 * 60 * 1000;
    private long loadingTime = 0;
    private HashMap<Integer, SmsServerDTO> smsServerHashMapList;
    private static SmsServerLoader smsServerLoader = null;
    static Logger logger = RingLogger.getConfigPortalLogger();

    public SmsServerLoader() {

    }

    public static SmsServerLoader getInstance() {
        if (smsServerLoader == null) {
            createLoader();
        }
        return smsServerLoader;
    }

    private synchronized static void createLoader() {
        if (smsServerLoader == null) {
            smsServerLoader = new SmsServerLoader();
        }
    }

    private void forceLoadData() {
        smsServerHashMapList = new HashMap<>();
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            stmt = db.connection.createStatement();
            String sql = "SELECT id, server_url, sms_verification_url, server_status FROM smsservers;";
            rs = stmt.executeQuery(sql);
            while (rs.next()) {
                SmsServerDTO dto = new SmsServerDTO();
                dto.setId(rs.getInt("id"));
                dto.setUrl(rs.getString("server_url"));
                dto.setSmsVerificationUrl(rs.getString("sms_verification_url"));
                dto.setStatus(rs.getInt("server_status"));
                smsServerHashMapList.put(dto.getId(), dto);
            }
            logger.debug(sql + "::" + smsServerHashMapList.size());
        } catch (Exception e) {
            logger.debug("SmsServer List Exception...", e);
        }

    }

    private void checkForReload() {
        long currentTime = System.currentTimeMillis();
        if (currentTime - loadingTime > LOADING_INTERVAL) {
            loadingTime = currentTime;
            forceLoadData();
        }
    }

    private boolean contains(SmsServerDTO dto, String searchText) {
        return (String.valueOf(dto.getId()).contains(searchText))
                || (dto.getUrl() != null && dto.getUrl().toLowerCase().contains(searchText))
                || (dto.getSmsVerificationUrl() != null && dto.getSmsVerificationUrl().toLowerCase().contains(searchText));
    }

    public synchronized ArrayList<SmsServerDTO> getSmsServerList(SmsServerDTO sdto) {
        checkForReload();
        ArrayList<SmsServerDTO> data = new ArrayList<>();
        Set set = smsServerHashMapList.entrySet();
        Iterator i = set.iterator();
        while (i.hasNext()) {
            Map.Entry me = (Map.Entry) i.next();
            SmsServerDTO dto = (SmsServerDTO) me.getValue();
            if (sdto.getSearchText() != null && sdto.getSearchText().length() > 0 && !contains(dto, sdto.getSearchText())) {
                continue;

            }
            data.add(dto);
        }
        if (sdto.getSortType() == Constants.DESC_SORT) {
            Collections.sort(data, new SmsServerDTO.CompDSC());
        } else {
            Collections.sort(data, new SmsServerDTO.CompASC());
        }
        logger.debug("getSmsServerList Size: " + data.size());
        return data;
    }

    public synchronized void forceReload() {
        loadingTime = System.currentTimeMillis();
        forceLoadData();
    }

    public synchronized SmsServerDTO getSmsServerDTO(int id) {
        checkForReload();
        if (smsServerHashMapList.containsKey(id)) {
            return smsServerHashMapList.get(id);
        }
        return null;
    }

    public synchronized ArrayList<SmsServerDTO> getSmsServerForDownload() {
        checkForReload();
        ArrayList<SmsServerDTO> data = new ArrayList<>(smsServerHashMapList.values());
        return data;
    }

}
