/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.servers.smsServers;

import com.ringid.admin.utils.log.RingLogger;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import javax.servlet.ServletException;
import org.apache.logging.log4j.Logger;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Kazi Nasir Uddin Oly 
 * @Date Jun 4, 2016
 */
@WebServlet(name = "DownloadSMSServers", urlPatterns = {"/Download/DownloadSMSServers"})
public class DownloadSmsServers extends HttpServlet {
    static Logger logger = RingLogger.getConfigPortalLogger();
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/csv");
        response.setHeader("Content-Disposition", "attachment; filename=\"smsServers.csv\"");
        try{
            OutputStream outputStream = response.getOutputStream();
            ArrayList<SmsServerDTO> list = SmsServerTaskSchedular.getInstance().getSmsServerListForDownload();
            outputStream.write("Server URL".getBytes());
            outputStream.write(",".getBytes());
            outputStream.write("SMS Verification URL".getBytes());
            outputStream.write(",".getBytes());
            outputStream.write("Server Status".getBytes());
            outputStream.write("\n".getBytes());
            for(SmsServerDTO d: list){
                outputStream.write(d.getUrl().getBytes());
                outputStream.write(",".getBytes());
                outputStream.write(d.getSmsVerificationUrl().getBytes());
                outputStream.write(",".getBytes());
                outputStream.write(String.valueOf(d.getStatus()).getBytes());
                outputStream.write("\n".getBytes());
            }
            outputStream.flush();
            outputStream.close();
        } catch(Exception e){
            logger.error("DownloadSmsServers Exception: " + e.toString());
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }

    @Override
    public String getServletInfo() {
        return "Short Description";
    }
    
    
    
    
    
}
