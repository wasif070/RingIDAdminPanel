/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.servers.smsServers;

import com.ringid.admin.utils.MyAppError;
import java.util.ArrayList;

/**
 *
 * @author Oly
 */
public class SmsServerTaskSchedular {

    private SmsServerTaskSchedular() {

    }

    private static class SmsServerTaskSchedularHolder {

        private static final SmsServerTaskSchedular INSTANCE = new SmsServerTaskSchedular();
    }

    public static SmsServerTaskSchedular getInstance() {
        return SmsServerTaskSchedularHolder.INSTANCE;
    }

    public ArrayList<SmsServerDTO> getSmsServerList(SmsServerDTO dto) {
        return SmsServerLoader.getInstance().getSmsServerList(dto);
    }

    public int getMaxId() {
        SmsServerDAO authServerDAO = new SmsServerDAO();
        return authServerDAO.getMaxId();
    }

    public MyAppError addSmsServerInfo(SmsServerDTO p_dto) {
        SmsServerDAO smsServerDAO = new SmsServerDAO();
        return smsServerDAO.addSmsServerInfo(p_dto);
    }

    public MyAppError addSmsServerInfo(ArrayList<SmsServerDTO> maps) {
        SmsServerDAO smsServerDAO = new SmsServerDAO();
        return smsServerDAO.addSmsServerInfo(maps);
    }

    public SmsServerDTO getSmsServerInfoById(int id) {
        return SmsServerLoader.getInstance().getSmsServerDTO(id);
    }

    public MyAppError deleteSmsServer(int id) {
        SmsServerDAO smsServerDAO = new SmsServerDAO();
        return smsServerDAO.deleteSmsServer(id);
    }

    public MyAppError updateSmsServer(SmsServerDTO p_dto) {
        SmsServerDAO smsServerDAO = new SmsServerDAO();
        return smsServerDAO.updateSmsServer(p_dto);
    }

    public ArrayList<SmsServerDTO> getSmsServerListForDownload() {
        return SmsServerLoader.getInstance().getSmsServerForDownload();
    }

    public SmsServerDTO getSmsServerDTO(SmsServerForm smsServerForm) {
        SmsServerDTO smsServerDTO = new SmsServerDTO();
        smsServerDTO.setUrl(smsServerForm.getUrl());
        smsServerDTO.setSmsVerificationUrl(smsServerForm.getSmsVerificationUrl());
        if (smsServerForm.isNull(smsServerForm.getStatus())) {
            smsServerDTO.setStatus(1);
        } else {
            smsServerDTO.setStatus(Integer.valueOf(smsServerForm.getStatus()));
        }
        return smsServerDTO;
    }
}
