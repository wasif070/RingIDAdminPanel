/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.servers.smsServers;

import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author Kazi Nasir Uddin Oly
 * @Date Jun 4, 2016
 */
public class SmsServerEditAction extends BaseAction {

    Logger logger = RingLogger.getConfigPortalLogger();

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = checkAuthentication(mapping, form, request);
        try {
            if (target.equals(SUCCESS)) {
                int id = 0;
                SmsServerForm smsServerForm = (SmsServerForm) form;
                if (request.getParameter("id") != null) {
                    id = Integer.parseInt(request.getParameter("id"));
                }
                SmsServerTaskSchedular scheduler = SmsServerTaskSchedular.getInstance();
                SmsServerDTO smsServerDTO = scheduler.getSmsServerInfoById(id);
                if (smsServerDTO != null) {
                    smsServerForm.setNewId(smsServerDTO.getId());
                    smsServerForm.setId(smsServerDTO.getId());
                    smsServerForm.setUrl(smsServerDTO.getUrl());
                    smsServerForm.setSmsVerificationUrl(smsServerDTO.getSmsVerificationUrl());
                    smsServerForm.setStatus(String.valueOf(smsServerDTO.getStatus()));
                    target = SUCCESS;
                }
            }
        } catch (Exception e) {
            logger.error("SmsServerEditAction Exception: " + e);
            target = FAILURE;
        }
        return mapping.findForward(target);
    }

}
