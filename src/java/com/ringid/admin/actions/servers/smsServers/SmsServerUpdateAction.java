/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.servers.smsServers;

import com.google.gson.Gson;
import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.dto.adminAuth.LoginDTO;
import com.ringid.admin.utils.Constants;
import com.ringid.admin.utils.MyAppError;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author Kazi Nasir Uddin Oly
 * @Date Jun 4, 2016
 */
public class SmsServerUpdateAction extends BaseAction {

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = checkAuthentication(mapping, form, request);
        String activistName;
        try {
            LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
            activistName = loginDTO.getUserName();
        } catch (Exception e) {
            target = UNAUTHORIZED;
            return mapping.findForward(target);
        }
        SmsServerForm smsServerForm = (SmsServerForm) form;
        MyAppError error = new MyAppError();
        try {
            if (target.equals(SUCCESS)) {
                SmsServerDTO smsServerDTO = new SmsServerDTO();
                smsServerDTO.setId(smsServerForm.getId());
                smsServerDTO.setUrl(smsServerForm.getUrl());
                smsServerDTO.setSmsVerificationUrl(smsServerForm.getSmsVerificationUrl());
                smsServerDTO.setStatus(Integer.valueOf(smsServerForm.getStatus()));
                smsServerDTO.setNewId(smsServerForm.getNewId());
                if (smsServerDTO.getNewId() <= 0) {
                    smsServerDTO.setNewId(smsServerDTO.getId());
                }
                SmsServerTaskSchedular scheduler = SmsServerTaskSchedular.getInstance();
                RingLogger.getActivityLogger().debug("[SmsServerUpdateAction] activistName : " + activistName + " smsServerDTO --> " + new Gson().toJson(smsServerDTO));
                error = scheduler.updateSmsServer(smsServerDTO);
                if (error.getERROR_TYPE() > 0) {
                    target = FAILURE;
                    smsServerForm.setMessage(error.getErrorMessage());
                    request.getSession(true).setAttribute("error", "2");
                } else {
                    SmsServerLoader.getInstance().forceReload();
                }
            }
        } catch (Exception e) {
            smsServerForm.setMessage(error.getErrorMessage());
            request.getSession(true).setAttribute("error", "2");
            target = FAILURE;
        }
        return mapping.findForward(target);
    }

}
