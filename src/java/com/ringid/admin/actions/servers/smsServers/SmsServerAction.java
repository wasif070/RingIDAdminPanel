/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.servers.smsServers;

import com.google.gson.Gson;
import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.dto.adminAuth.LoginDTO;
import com.ringid.admin.utils.Constants;
import com.ringid.admin.utils.MyAppError;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author Kazi Nasir Uddin Oly
 * @Date Jun 2, 2016
 */
public class SmsServerAction extends BaseAction {

    static Logger logger = RingLogger.getConfigPortalLogger();

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = checkAuthentication(mapping, form, request);
        String activistName;
        try {
            LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
            activistName = loginDTO.getUserName();
        } catch (Exception e) {
            target = UNAUTHORIZED;
            return mapping.findForward(target);
        }
        try {
            if (target.equals(SUCCESS)) {
                SmsServerForm smsServerForm = (SmsServerForm) form;
                SmsServerTaskSchedular scheduler = SmsServerTaskSchedular.getInstance();
                SmsServerDTO smsServerDTO = scheduler.getSmsServerDTO(smsServerForm);
                if (smsServerForm.getNewId() <= 0) {
                    int maxId = scheduler.getMaxId();
                    smsServerDTO.setId(maxId + 1);
                }
                RingLogger.getActivityLogger().debug("[add SmsServerAction] activistName : " + activistName + " smsServerDTO --> " + new Gson().toJson(smsServerDTO));
                MyAppError error = scheduler.addSmsServerInfo(smsServerDTO);
                if (error.getERROR_TYPE() > 0) {
                    if (error.getERROR_TYPE() == MyAppError.OTHERERROR) {
                        logger.error("SmsServerAction Other error " + error.getERROR_TYPE());
                        smsServerForm.setMessage(error.getErrorMessage());
                        request.getSession(true).setAttribute("error", "2");
                    }
                    target = FAILURE;
                } else {
                    SmsServerLoader.getInstance().forceReload();
                }
            }
        } catch (Exception e) {
            logger.error("SmsServerAction Exception: " + e);
            target = FAILURE;
        }
        return mapping.findForward(target);
    }
}
