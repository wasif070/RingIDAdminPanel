/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.specialRoom;

import com.ringid.admin.BaseForm;
import com.ringid.admin.dto.adminAuth.LoginDTO;
import com.ringid.admin.utils.Constants;
import javax.servlet.http.HttpServletRequest;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.upload.FormFile;

/**
 *
 * @author mamun
 */
public class SpecialRoomForm extends BaseForm {

    private long roomId;
    private String name;
    private String description;
    private String banner;
    private String votingStartTime;
    private String votingEndTime;
    private long pageId;
    private int pageProfileType;
    private int visible;
    private FormFile theFile;

    public String getVotingStartTime() {
        return votingStartTime;
    }

    public void setVotingStartTime(String votingStartTime) {
        this.votingStartTime = votingStartTime;
    }

    public String getVotingEndTime() {
        return votingEndTime;
    }

    public void setVotingEndTime(String votingEndTime) {
        this.votingEndTime = votingEndTime;
    }

    public FormFile getTheFile() {
        return theFile;
    }

    public void setTheFile(FormFile theFile) {
        this.theFile = theFile;
    }

    public long getRoomId() {
        return roomId;
    }

    public void setRoomId(long roomId) {
        this.roomId = roomId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getBanner() {
        return banner;
    }

    public void setBanner(String banner) {
        this.banner = banner;
    }

    public long getPageId() {
        return pageId;
    }

    public void setPageId(long pageId) {
        this.pageId = pageId;
    }

    public int getPageProfileType() {
        return pageProfileType;
    }

    public void setPageProfileType(int pageProfileType) {
        this.pageProfileType = pageProfileType;
    }

    public int getVisible() {
        return visible;
    }

    public void setVisible(int visible) {
        this.visible = visible;
    }

    @Override
    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        ActionErrors errors = new ActionErrors();
        LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        if (loginDTO == null) {
            errors.add("auth", new ActionMessage("UnAuthorized Access"));
        } else if (Constants.ADD == getAction() || Constants.UPDATE == getAction()) {
            if (getName() == null || getName().length() < 1) {
                errors.add("name", new ActionMessage("errors.file.required"));
            }
            if (getVotingStartTime() == null || getVotingStartTime().length() < 1) {
                errors.add("votingStartTime", new ActionMessage("errors.file.required"));
            }
            if (getVotingEndTime() == null || getVotingEndTime().length() < 1) {
                errors.add("votingEndTime", new ActionMessage("errors.file.required"));
            }
            if (Constants.ADD == getAction()) {
                if (getTheFile() == null || getTheFile().getFileSize() < 1) {
                    errors.add("theFile", new ActionMessage("errors.file.required"));
                }
            }
        }
        return errors;
    }
}
