/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.specialRoom;

import com.ringid.admin.dto.SpecialRoomDTO;
import com.ringid.admin.service.SpecialRoomService;
import auth.com.utils.MyAppError;
import com.google.gson.Gson;
import com.ringid.admin.BaseAction;
import com.ringid.admin.actions.liveRoom.UploadBannerImage;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.dto.adminAuth.LoginDTO;
import com.ringid.admin.utils.Constants;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.upload.FormFile;
import ringid.LoginTaskScheduler;

/**
 *
 * @author mamun
 */
public class SpecialRoomAction extends BaseAction {

    Logger logger = RingLogger.getConfigPortalLogger();

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = checkAuthentication(mapping, form, request);
        String activistName;
        try {
            LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
            activistName = loginDTO.getUserName();
        } catch (Exception e) {
            target = UNAUTHORIZED;
            return mapping.findForward(target);
        }
        try {
            if (target.equals(SUCCESS)) {
                SpecialRoomForm specialRoomForm = (SpecialRoomForm) form;
                SpecialRoomService scheduler = SpecialRoomService.getInstance();
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
                Date date;
                SpecialRoomDTO specialRoomDTO = null;
                com.ringid.admin.utils.MyAppError error = new com.ringid.admin.utils.MyAppError();
                FormFile myFile;
                String fileName = null;
                byte[] fileData = null;
                String bannerUrl;
                MyAppError myAppError;

                switch (specialRoomForm.getAction()) {
                    case Constants.ADD:
                        myFile = specialRoomForm.getTheFile();
                        fileName = myFile.getFileName();
                        fileData = myFile.getFileData();
                        if (fileName.length() < 1 || fileData.length < 1) {
                            throw new Exception("please upload appropriate data!");
                        }
                        specialRoomDTO = new SpecialRoomDTO();
                        myAppError = LoginTaskScheduler.getInstance().login(Constants.USERID, Constants.PASSWORD);
                        RingLogger.getConfigPortalLogger().debug("##  login --> " + new Gson().toJson(myAppError));

                        bannerUrl = new UploadBannerImage().upLoadToCloud(Constants.USERID, fileName, fileData);
                        RingLogger.getConfigPortalLogger().debug("##  bannerUrl --> " + bannerUrl);
                        myAppError = LoginTaskScheduler.getInstance().logout(Constants.USERID);
                        RingLogger.getConfigPortalLogger().debug("##  myAppError --> " + new Gson().toJson(myAppError));

                        specialRoomDTO.setName(specialRoomForm.getName());
                        specialRoomDTO.setDescription(specialRoomForm.getDescription());
                        specialRoomDTO.setBanner(bannerUrl);
                        date = sdf.parse(specialRoomForm.getVotingStartTime());
                        specialRoomDTO.setVotingStartTime(date.getTime());
                        date = sdf.parse(specialRoomForm.getVotingEndTime());
                        specialRoomDTO.setVotingEndTime(date.getTime());
                        specialRoomDTO.setPageId(specialRoomForm.getPageId());
                        specialRoomDTO.setPageProfileType(specialRoomForm.getPageProfileType());
                        specialRoomDTO.setVisible(specialRoomForm.getVisible());

                        RingLogger.getActivityLogger().debug("[add SpecialRoomAction] activistName : " + activistName + " specialRoomDTO --> " + new Gson().toJson(specialRoomDTO));
                        error = scheduler.addSpecialRoom(specialRoomDTO);
                        if (error.getERROR_TYPE() == com.ringid.admin.utils.MyAppError.NOERROR) {
//                            notifyAuthServers(Constants.RELOAD_HOME_SPECIAL_ROOM, null);
                            target = SUCCESS;
                            request.setAttribute("message", "<span style='color: green'>Successfully added</span>");
                        } else {
                            target = FAILURE;
                            request.setAttribute("message", "<span style='color: red'>Failed to added(rc: " + error.getErrorMessage() + ")</span>");
                        }
                        break;
                    case Constants.EDIT:
                        specialRoomDTO = scheduler.getSpecialRoomByRoomId(specialRoomForm.getRoomId());
                        if (specialRoomDTO != null) {
                            target = SUCCESS;
                            specialRoomForm.setRoomId(specialRoomDTO.getRoomId());
                            specialRoomForm.setName(specialRoomDTO.getName());
                            specialRoomForm.setDescription(specialRoomDTO.getDescription());
                            specialRoomForm.setBanner(specialRoomDTO.getBanner());
                            specialRoomForm.setPageId(specialRoomDTO.getPageId());
                            specialRoomForm.setPageProfileType(specialRoomDTO.getPageProfileType());
                            specialRoomForm.setVisible(specialRoomDTO.getVisible());
                            specialRoomForm.setVotingStartTime(sdf.format(new Date(specialRoomDTO.getVotingStartTime())));
                            specialRoomForm.setVotingEndTime(sdf.format(new Date(specialRoomDTO.getVotingEndTime())));
                        } else {
                            target = FAILURE;
                        }
                        break;
                    case Constants.UPDATE:

                        specialRoomDTO = new SpecialRoomDTO();
                        if (specialRoomForm.getTheFile() != null) {
                            myFile = specialRoomForm.getTheFile();
                            fileName = myFile.getFileName();
                            fileData = myFile.getFileData();
                        }
                        if (fileData != null && fileData.length > 0) {
                            myAppError = LoginTaskScheduler.getInstance().login(Constants.USERID, Constants.PASSWORD);
                            RingLogger.getConfigPortalLogger().debug("##  login --> " + new Gson().toJson(myAppError));

                            bannerUrl = new UploadBannerImage().upLoadToCloud(Constants.USERID, fileName, fileData);
                            RingLogger.getConfigPortalLogger().debug("##  bannerUrl --> " + bannerUrl);
                            myAppError = LoginTaskScheduler.getInstance().logout(Constants.USERID);
                            RingLogger.getConfigPortalLogger().debug("##  myAppError --> " + new Gson().toJson(myAppError));
                            specialRoomForm.setBanner(bannerUrl);
                        }
                        specialRoomDTO.setRoomId(specialRoomForm.getRoomId());
                        specialRoomDTO.setBanner(specialRoomForm.getBanner());
                        specialRoomDTO.setName(specialRoomForm.getName());
                        specialRoomDTO.setDescription(specialRoomForm.getDescription());
                        date = sdf.parse(specialRoomForm.getVotingStartTime());
                        specialRoomDTO.setVotingStartTime(date.getTime());
                        date = sdf.parse(specialRoomForm.getVotingEndTime());
                        specialRoomDTO.setVotingEndTime(date.getTime());
                        specialRoomDTO.setPageId(specialRoomForm.getPageId());
                        specialRoomDTO.setPageProfileType(specialRoomForm.getPageProfileType());
                        specialRoomDTO.setVisible(specialRoomForm.getVisible());

                        RingLogger.getActivityLogger().debug("[ update SpecialRoomAction] activistName : " + activistName + " specialRoomDTO --> " + new Gson().toJson(specialRoomDTO));
                        error = scheduler.updatepecialRoom(specialRoomDTO);
                        if (error.getERROR_TYPE() == com.ringid.admin.utils.MyAppError.NOERROR) {
//                            notifyAuthServers(Constants.RELOAD_HOME_SPECIAL_ROOM, null);
                            target = SUCCESS;
                            request.setAttribute("message", "<span style='color: green'>Successfully Updated</span>");
                        } else {
                            target = FAILURE;
                            request.setAttribute("message", "<span style='color: red'>Failed to update</span>");
                        }
                        break;
                    case Constants.DELETE:
                        error = scheduler.deleteSpecialRoom(specialRoomForm.getRoomId());
                        if (error.getERROR_TYPE() == com.ringid.admin.utils.MyAppError.NOERROR) {
//                            notifyAuthServers(Constants.RELOAD_HOME_SPECIAL_ROOM, null);
                            target = SUCCESS;
                            request.setAttribute("message", "<span style='color: green'>Successfully Deleted</span>");
                        } else {
                            target = FAILURE;
                            request.setAttribute("message", "<span style='color: red'>Failed to delete</span>");
                        }
                        break;
                    default:
                        break;
                }
            }
        } catch (Exception e) {
            target = FAILURE;
            logger.error(e);
            request.setAttribute("message", "<span style='color: red'>Expception Occured</span>");
        }
        return mapping.findForward(target);
    }
}
