/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.specialRoom;

import com.ringid.admin.dto.SpecialRoomDTO;
import com.ringid.admin.service.SpecialRoomService;
import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.dto.adminAuth.LoginDTO;
import com.ringid.admin.utils.Constants;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author mamun
 */
public class SpecialRoomListAction extends BaseAction {

    private static final Logger logger = RingLogger.getConfigPortalLogger();

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = SUCCESS;
        try {
            LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        } catch (Exception e) {
            target = UNAUTHORIZED;
            return mapping.findForward(target);
        }
        try {
            SpecialRoomForm specialRoomForm = (SpecialRoomForm) form;

            List<SpecialRoomDTO> specialRoomDTOs = SpecialRoomService.getInstance().getSpecialRoomList(specialRoomForm.getSearchText());
            request.getSession(true).setAttribute(Constants.DATA_ROWS, specialRoomDTOs);
            managePages(request, specialRoomForm, specialRoomDTOs.size());
        } catch (Exception e) {
            logger.error("exception --> " + e);
            target = FAILURE;
        }

        return mapping.findForward(target);
    }
}
