/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ringid.admin.actions.appConstants;

import com.ringid.admin.dto.AppConstantsDTO;
import com.ringid.admin.service.AppConstantsService;
import com.ringid.admin.utils.log.RingLogger;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Kazi Nasir Uddin Oly 
 * @Date Jun 15, 2016
 */

@WebServlet(name = "DownloadAppConstants", urlPatterns = {"/Download/DownloadAppConstants"})
public class DownloadAppConstants extends HttpServlet{
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/csv");
        response.setHeader("Content-Disposition", "attachment; filename=\"AppConstants.csv\"");
        try(OutputStream outputStream = response.getOutputStream()) {
            ArrayList<AppConstantsDTO> list = AppConstantsService.getInstance().getAppConstantsListForDownload();
            outputStream.write("Platform".getBytes());
            outputStream.write(",".getBytes());
            outputStream.write("App Type".getBytes());
            outputStream.write(",".getBytes());
            outputStream.write("Version".getBytes());
            outputStream.write(",".getBytes());
            outputStream.write("Auth Controller".getBytes());
            outputStream.write(",".getBytes());
            outputStream.write("Image Server".getBytes());
            outputStream.write(",".getBytes());
            outputStream.write("Image Resource".getBytes());
            outputStream.write(",".getBytes());
            outputStream.write("Display Digits".getBytes());
            outputStream.write(",".getBytes());
            outputStream.write("Official User Id".getBytes());
            outputStream.write(",".getBytes());
            outputStream.write("Market Server".getBytes());
            outputStream.write(",".getBytes());
            outputStream.write("Market Resource".getBytes());
            outputStream.write(",".getBytes());
            outputStream.write("VOD Server".getBytes());
            outputStream.write(",".getBytes());
            outputStream.write("VOD Resource".getBytes());
            outputStream.write(",".getBytes());
            outputStream.write("Web".getBytes());
            outputStream.write("\n".getBytes());
            for(AppConstantsDTO d: list){
                outputStream.write(String.valueOf(d.getPlatform()).getBytes());
                outputStream.write(",".getBytes());
                outputStream.write(String.valueOf(d.getAppType()).getBytes());
                outputStream.write(",".getBytes());
                outputStream.write(String.valueOf(d.getVersion()).getBytes());
                outputStream.write(",".getBytes());
                outputStream.write(d.getAuthController().getBytes());
                outputStream.write(",".getBytes());
                outputStream.write(d.getImageServer().getBytes());
                outputStream.write(",".getBytes());
                outputStream.write(d.getImageResource().getBytes());
                outputStream.write(",".getBytes());
                outputStream.write(String.valueOf(d.getDisplayDigits()).getBytes());
                outputStream.write(",".getBytes());
                outputStream.write(String.valueOf(d.getOfficialUserId()).getBytes());
                outputStream.write(",".getBytes());
                outputStream.write(d.getMarketServer().getBytes());
                outputStream.write(",".getBytes());
                outputStream.write(d.getMarketResource().getBytes());
                outputStream.write(",".getBytes());
                outputStream.write(d.getVodServer().getBytes());
                outputStream.write(",".getBytes());
                outputStream.write(d.getVodResource().getBytes());
                outputStream.write(",".getBytes());
                outputStream.write(d.getWeb().getBytes());
                outputStream.write("\n".getBytes());
            }
            outputStream.flush();
        } catch(Exception e){
            RingLogger.getConfigPortalLogger().error("Download AppConstants: " + e.toString());
        }
    }
    
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }

    @Override
    public String getServletInfo() {
        return "Short Description";
    }
}
