/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ringid.admin.actions.appConstants;

import com.ringid.admin.dto.AppConstantsDTO;
import com.ringid.admin.service.AppConstantsService;
import com.ringid.admin.repository.AppConstantsLoader;
import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.MyAppError;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author Kazi Nasir Uddin Oly 
 * @Date Jun 15, 2016
 */
public class AppConstantsUploadAction extends BaseAction{
    String line = "";
    String cvsSplitBy = ",";
    
    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
    
    String target = checkAuthentication(mapping, form, request);
        if(target.equals(SUCCESS)){
            String[] dataArray = getDataFromFile(form);
            try{
                ArrayList<AppConstantsDTO> appConstantsList = new ArrayList<>();

                for(String s : dataArray) {
                    String[] appConstants = s.split(cvsSplitBy);
                    String platform = appConstants[0];
                    String appType = appConstants[1];
                    String version = appConstants[2];
                    String authController = appConstants[3];
                    String imageServer = appConstants[4];
                    String imageResource = appConstants[5];
                    String displayDigits = appConstants[6];
                    String officialUserId = appConstants[7];
                    String marketServer = appConstants[8];
                    String marketResource = appConstants[9];
                    String vodServer = appConstants[10];
                    String vodResource = appConstants[11];
                    String web = appConstants[12];
                    String paymentGateway = appConstants[13];
                    String constantsVersion = appConstants[14];
         
                    if(platform.equalsIgnoreCase("Platform")){
                        continue;
                    }

                    AppConstantsDTO appConstantsDTO = new AppConstantsDTO();
                    appConstantsDTO.setPlatform(Integer.valueOf(platform));
                    appConstantsDTO.setAppType(Integer.valueOf(appType));
                    appConstantsDTO.setVersion(Integer.valueOf(version));
                    appConstantsDTO.setAuthController(authController);
                    appConstantsDTO.setImageServer(imageServer);
                    appConstantsDTO.setImageResource(imageResource);
                    appConstantsDTO.setDisplayDigits(Integer.valueOf(displayDigits));
                    appConstantsDTO.setOfficialUserId(Long.valueOf(officialUserId));
                    appConstantsDTO.setMarketServer(marketServer);
                    appConstantsDTO.setMarketResource(marketResource);
                    appConstantsDTO.setVodServer(vodServer);
                    appConstantsDTO.setVodResource(vodResource);
                    appConstantsDTO.setWeb(web);
                    appConstantsDTO.setPaymentGateway(paymentGateway);
                    appConstantsDTO.setConstantsVersion(Integer.valueOf(constantsVersion));

                    appConstantsList.add(appConstantsDTO); 
                }

                MyAppError error = AppConstantsService.getInstance().addAppConstantsInfo(appConstantsList);
                if(error.getERROR_TYPE() > 0){
                    target = FAILURE;
                } else{
                    AppConstantsLoader.getInstance().forceReload();
                }
            } catch(Exception e){
                target = FAILURE;
            }
        }
        return mapping.findForward(target);
    }
}
