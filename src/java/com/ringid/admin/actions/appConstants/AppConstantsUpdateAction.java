/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.appConstants;

import com.ringid.admin.dto.AppConstantsDTO;
import com.ringid.admin.service.AppConstantsService;
import com.ringid.admin.repository.AppConstantsLoader;
import com.google.gson.Gson;
import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.dto.adminAuth.LoginDTO;
import com.ringid.admin.utils.Constants;

import com.ringid.admin.utils.MyAppError;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author Kazi Nasir Uddin Oly
 * @Date Jun 14, 2016
 */
public class AppConstantsUpdateAction extends BaseAction {

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        String target = checkAuthentication(mapping, form, request);
        String activistName;
        try {
            LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
            activistName = loginDTO.getUserName();
        } catch (Exception e) {
            target = UNAUTHORIZED;
            return mapping.findForward(target);
        }
        try {
            if (target.equals(SUCCESS)) {
                AppConstantsForm appConstantsForm = (AppConstantsForm) form;
                AppConstantsService scheduler = AppConstantsService.getInstance();
                AppConstantsDTO appConstantsDTO = scheduler.getAppConstantsDTO(appConstantsForm);
                appConstantsDTO.setId(appConstantsForm.getId());
                RingLogger.getActivityLogger().debug("[AppConstantsUpdateAction] activistName : " + activistName + " appConstantsDTO --> " + new Gson().toJson(appConstantsDTO));
                MyAppError error = scheduler.updateAppConstants(appConstantsDTO);
                if (error.getERROR_TYPE() > 0) {
                    target = FAILURE;
                    appConstantsForm.setMessage(error.getErrorMessage());
                    request.getSession(true).setAttribute("error", "2");
                } else {
                    AppConstantsLoader.getInstance().forceReload();
                }
            }
        } catch (Exception e) {
        }
        return mapping.findForward(target);
    }
}
