/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.appConstants;

import com.ringid.admin.dto.AppConstantsDTO;
import com.ringid.admin.service.AppConstantsService;
import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author Kazi Nasir Uddin Oly
 * @Date Jun 14, 2016
 */
public class AppConstantsEditAction extends BaseAction {

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        String target = checkAuthentication(mapping, form, request);
        try {
            if (target.equals(SUCCESS)) {
                int id = 0;
                AppConstantsForm appConstantsForm = (AppConstantsForm) form;
                if (request.getParameter("id") != null) {
                    id = Integer.parseInt(request.getParameter("id"));
                }
                AppConstantsService scheduler = AppConstantsService.getInstance();
                AppConstantsDTO appConstantsDTO = scheduler.getAppConstantsInfoById(id);
                if (appConstantsDTO != null) {
                    appConstantsForm.setId(appConstantsDTO.getId());
                    appConstantsForm.setPlatform(String.valueOf(appConstantsDTO.getPlatform()));
                    appConstantsForm.setAppType(String.valueOf(appConstantsDTO.getAppType()));
                    appConstantsForm.setVersion(String.valueOf(appConstantsDTO.getVersion()));
                    appConstantsForm.setAuthController(appConstantsDTO.getAuthController());
                    appConstantsForm.setImageServer(appConstantsDTO.getImageServer());
                    appConstantsForm.setImageResource(appConstantsDTO.getImageResource());
                    appConstantsForm.setDisplayDigits(String.valueOf(appConstantsDTO.getDisplayDigits()));
                    appConstantsForm.setOfficialUserId(String.valueOf(appConstantsDTO.getOfficialUserId()));
                    appConstantsForm.setMarketServer(appConstantsDTO.getMarketServer());
                    appConstantsForm.setMarketResource(appConstantsDTO.getMarketResource());
                    appConstantsForm.setVodServer(appConstantsDTO.getVodServer());
                    appConstantsForm.setVodResource(appConstantsDTO.getVodResource());
                    appConstantsForm.setWeb(appConstantsDTO.getWeb());
                    appConstantsForm.setPaymentGateway(appConstantsDTO.getPaymentGateway());
                    appConstantsForm.setConstantsVersion(appConstantsDTO.getConstantsVersion());
                    target = SUCCESS;
                }
            }
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error(e);
            target = FAILURE;
        }
        return mapping.findForward(target);
    }
}
