/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.appConstants;

import com.google.gson.Gson;
import com.ringid.admin.BaseAction;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Rabby
 */
public class AppConstantDefaultValueAction extends BaseAction {

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        String target = checkAuthentication(mapping, form, request);

        try {
            AppConstantsForm appConstantsForm = (AppConstantsForm) form;
            appConstantsForm.setEntryOf(2);
            ServletContext context = request.getSession().getServletContext();
            File appConstants = new File(context.getRealPath("/WEB-INF/classes/appconstants-default.txt"));

            if (appConstants.exists()) {
                FileReader fileReader = new FileReader(appConstants);
                BufferedReader bufferedReader = new BufferedReader(fileReader);
                String line;
                while ((line = bufferedReader.readLine()) != null) {
                    String[] data = line.split("=");
                    switch (data[0]) {
                        case "platform":
                            appConstantsForm.setPlatform(data[1]);
                            break;
                        case "app_type":
                            appConstantsForm.setAppType(data[1]);
                            break;
                        case "auth_controller":
                            appConstantsForm.setAuthController(data[1]);
                            break;
                        case "image_server":
                            appConstantsForm.setImageServer(data[1]);
                            break;
                        case "image_resource":
                            appConstantsForm.setImageResource(data[1]);
                            break;
                        case "display_digits":
                            appConstantsForm.setDisplayDigits(data[1]);
                            break;
                        case "official_user_id":
                            appConstantsForm.setOfficialUserId(data[1]);
                            break;
                        case "market_server":
                            appConstantsForm.setMarketServer(data[1]);
                            break;
                        case "market_resource":
                            appConstantsForm.setMarketResource(data[1]);
                            break;
                        case "vod_server":
                            appConstantsForm.setVodServer(data[1]);
                            break;
                        case "vod_resource":
                            appConstantsForm.setVodResource(data[1]);
                            break;
                        case "web":
                            appConstantsForm.setWeb(data[1]);
                            break;
                        case "paymentGateway":
                            appConstantsForm.setPaymentGateway(data[1]);
                            break;
                        case "constantsVersion":
                            appConstantsForm.setConstantsVersion(Integer.valueOf(data[1]));
                            break;
                        default:
                            break;
                    }
                }
            } else {
                target = FAILURE;
            }

        } catch (IOException | NumberFormatException e) {
            target = FAILURE;
        }
        JSONObject jSONObject = new JSONObject();
        JSONObject pro = new JSONObject();
        JSONObject dev = new JSONObject();
        try {
            dev.put("platform", 2);
            dev.put("appType", 1);
            dev.put("version", "");
            dev.put("entryOf", 1);
            dev.put("authController", "https://devauth.ringid.com/rac/");
            dev.put("imageServer", "https://devimages.ringid.com/");
            dev.put("imageResource", "https://devimagesres.ringid.com/");
            dev.put("displayDigits", 8);
            dev.put("officialUserId", 1);
            dev.put("marketServer", "https://devsticker.ringid.com/");
            dev.put("marketResource", "https://devstickerres.ringid.com/");
            dev.put("vodServer", "https://devmediacloudapi.ringid.com/");
            dev.put("vodResource", "https://devmediacloud.ringid.com/");
            dev.put("web", "https://dev.ringid.com/");
            dev.put("paymentGateway", "payment");
            dev.put("constantsVersion", 0);

            jSONObject.put("dev", dev);

            pro.put("platform", 2);
            pro.put("appType", 1);
            pro.put("version", "");
            pro.put("entryOf", 2);
            pro.put("authController", "https://auth.ringid.com/rac/");
            pro.put("imageServer", "https://images.ringid.com/");
            pro.put("imageResource", "https://imagesres.ringid.com/");
            pro.put("displayDigits", 8);
            pro.put("officialUserId", 1);
            pro.put("marketServer", "https://sticker.ringid.com/");
            pro.put("marketResource", "https://stickerres.ringid.com/");
            pro.put("vodServer", "https://mediacloudapi.ringid.com/");
            pro.put("vodResource", "https://mediacloud.ringid.com/");
            pro.put("web", "https://www.ringid.com/");
            pro.put("paymentGateway", "payment");
            pro.put("constantsVersion", 0);

            jSONObject.put("pro", pro);

            request.setAttribute("appconstantsdata", jSONObject);
        } catch (JSONException ex) {
            ex.printStackTrace();
        }
        return mapping.findForward(target);
    }
}
