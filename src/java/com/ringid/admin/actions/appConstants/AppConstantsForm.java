/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.appConstants;

import com.ringid.admin.BaseForm;
import com.ringid.admin.dto.adminAuth.LoginDTO;
import com.ringid.admin.utils.Constants;
import javax.servlet.http.HttpServletRequest;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

/**
 *
 * @author Kazi Nasir Uddin Oly
 * @Date Jun 14, 2016
 */
public class AppConstantsForm extends BaseForm {

    private int id;
    private String platform;
    private String appType;
    private String version;
    private String authController;
    private String imageServer;
    private String imageResource;
    private String displayDigits;
    private String officialUserId;
    private String marketServer;
    private String marketResource;
    private String vodServer;
    private String vodResource;
    private String web;
    private String paymentGateway;
    private int constantsVersion;
    private int entryOf;

    public int getEntryOf() {
        return entryOf;
    }

    public void setEntryOf(int entryOf) {
        this.entryOf = entryOf;
    }

    public String getPaymentGateway() {
        return paymentGateway;
    }

    public void setPaymentGateway(String paymentGateway) {
        this.paymentGateway = paymentGateway;
    }

    public int getConstantsVersion() {
        return constantsVersion;
    }

    public void setConstantsVersion(int constantsVersion) {
        this.constantsVersion = constantsVersion;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    public String getAppType() {
        return appType;
    }

    public void setAppType(String appType) {
        this.appType = appType;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getAuthController() {
        return authController;
    }

    public void setAuthController(String authController) {
        this.authController = authController;
    }

    public String getImageServer() {
        return imageServer;
    }

    public void setImageServer(String imageServer) {
        this.imageServer = imageServer;
    }

    public String getImageResource() {
        return imageResource;
    }

    public void setImageResource(String imageResource) {
        this.imageResource = imageResource;
    }

    public String getDisplayDigits() {
        return displayDigits;
    }

    public void setDisplayDigits(String displayDigits) {
        this.displayDigits = displayDigits;
    }

    public String getOfficialUserId() {
        return officialUserId;
    }

    public void setOfficialUserId(String officialUserId) {
        this.officialUserId = officialUserId;
    }

    public String getMarketServer() {
        return marketServer;
    }

    public void setMarketServer(String marketServer) {
        this.marketServer = marketServer;
    }

    public String getMarketResource() {
        return marketResource;
    }

    public void setMarketResource(String marketResource) {
        this.marketResource = marketResource;
    }

    public String getVodServer() {
        return vodServer;
    }

    public void setVodServer(String vodServer) {
        this.vodServer = vodServer;
    }

    public String getVodResource() {
        return vodResource;
    }

    public void setVodResource(String vodResource) {
        this.vodResource = vodResource;
    }

    public String getWeb() {
        return web;
    }

    public void setWeb(String web) {
        this.web = web;
    }

    @Override
    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        ActionErrors errors = new ActionErrors();
        LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        if (loginDTO == null) {
            errors.add("auth", new ActionMessage("UnAuthorized Access"));
        }
        return errors;
    }

}
