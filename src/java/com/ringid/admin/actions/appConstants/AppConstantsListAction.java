/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.appConstants;

import com.ringid.admin.dto.AppConstantsDTO;
import com.ringid.admin.service.AppConstantsService;
import com.ringid.admin.repository.AppConstantsLoader;
import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.Constants;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author Kazi Nasir Uddin Oly
 * @Date Jun 14, 2016
 */
public class AppConstantsListAction extends BaseAction {

    ArrayList<AppConstantsDTO> list = new ArrayList<>();

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = FAILURE;
        try {
            AppConstantsForm appConstantsForm = (AppConstantsForm) form;
            AppConstantsDTO appConstantsDTO = new AppConstantsDTO();
            AppConstantsService scheduler = AppConstantsService.getInstance();
            
            if (request.getParameter("ForceReload") != null && "1".equals(request.getParameter("ForceReload"))) {
                AppConstantsLoader.getInstance().forceReload();
            }

            if (request.getParameter("max") != null && "1".equals(request.getParameter("max"))) {
                list = scheduler.getMaxAppConstantsListInfo();
            } else {
                if (appConstantsForm.getSearchText() != null && appConstantsForm.getSearchText().length() > 0) {
                    appConstantsDTO.setSearchText(appConstantsForm.getSearchText().trim().toLowerCase());
                }
                if (appConstantsForm.getColumn() <= 0) {
                    appConstantsForm.setColumn(Constants.COLUMN_ONE);
                    appConstantsForm.setSort(Constants.DESC_SORT);
                }
                appConstantsDTO.setColumn(appConstantsForm.getColumn());
                appConstantsDTO.setSortType(appConstantsForm.getSort());
                list = scheduler.getAppConstantsList(appConstantsDTO);
            }
            request.getSession(true).setAttribute(Constants.DATA_ROWS, list);
            managePages(request, appConstantsForm, list.size());
        } catch (NullPointerException e) {
            target = FAILURE;
        }
        return mapping.findForward(target);
    }
}
