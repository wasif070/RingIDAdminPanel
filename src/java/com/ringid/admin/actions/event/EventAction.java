/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.event;

import com.ringid.admin.service.EventService;
import auth.com.utils.MyAppError;
import com.google.gson.Gson;
import com.ringid.admin.BaseAction;
import com.ringid.admin.actions.liveRoom.UploadBannerImage;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.dto.adminAuth.LoginDTO;
import com.ringid.admin.utils.Constants;
import java.text.SimpleDateFormat;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.upload.FormFile;
import org.ringid.event.EventDTO;
import org.ringid.utilities.ReasonCode;
import ringid.LoginTaskScheduler;
import ringid.sessionless.SessionlessTaskseheduler;

/**
 *
 * @author mamun
 */
public class EventAction extends BaseAction {

    Logger logger = RingLogger.getConfigPortalLogger();

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = checkAuthentication(mapping, form, request);
        String activistName;
        int reasonCode = -1;
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        try {
            LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
            activistName = loginDTO.getUserName();
        } catch (Exception e) {
            target = UNAUTHORIZED;
            return mapping.findForward(target);
        }
        try {
            EventForm eventForm = (EventForm) form;
            EventService scheduler = EventService.getInstance();
            EventDTO dto = new EventDTO();
            long eventId = 0L;
            String fileName = "", profileImageUrl = "", coverimageUrl = "";
            byte[] fileData;
            FormFile myFile;
            MyAppError myAppError;

            if (target.equals(SUCCESS)) {
                switch (eventForm.getAction()) {
                    case Constants.ADD:
                        LoginTaskScheduler.getInstance().initSessionLessCommunication(Constants.USERID);
                        RingLogger.getConfigPortalLogger().debug("## SessionLessCommunication initiated.");
                        eventId = SessionlessTaskseheduler.getInstance().getIdFromUserRingIdGenerator(Constants.USERID);
                        RingLogger.getConfigPortalLogger().debug("##  eventId --> " + eventId);
                        LoginTaskScheduler.getInstance().destroySessionLessCommunication(Constants.USERID);
                        RingLogger.getConfigPortalLogger().debug("## SessionLessCommunication destroyed.");
                        eventForm.setId(eventId);

                        myAppError = LoginTaskScheduler.getInstance().login(Constants.USERID, Constants.PASSWORD);
                        RingLogger.getConfigPortalLogger().debug("##  login --> " + new Gson().toJson(myAppError));
                        if (eventForm.getTheFileProfileImage() != null && eventForm.getTheFileProfileImage().getFileName().length() > 0 && eventForm.getTheFileProfileImage().getFileData().length > 0) {
                            myFile = eventForm.getTheFileProfileImage();
                            fileName = myFile.getFileName();
                            fileData = myFile.getFileData();
                            profileImageUrl = new UploadBannerImage().upLoadToCloud(Constants.USERID, fileName, fileData);
                            RingLogger.getConfigPortalLogger().debug("##  profileImageUrl --> " + profileImageUrl);
                            eventForm.setProfileImageUrl(profileImageUrl);
                        }
                        if (eventForm.getTheFileCoverImage() != null && eventForm.getTheFileCoverImage().getFileName().length() > 0 && eventForm.getTheFileCoverImage().getFileData().length > 0) {
                            myFile = eventForm.getTheFileCoverImage();
                            fileName = myFile.getFileName();
                            fileData = myFile.getFileData();
                            coverimageUrl = new UploadBannerImage().upLoadToCloud(Constants.USERID, fileName, fileData);
                            RingLogger.getConfigPortalLogger().debug("##  coverimageUrl --> " + coverimageUrl);
                            eventForm.setCoverImageUrl(coverimageUrl);
                        }
                        myAppError = LoginTaskScheduler.getInstance().logout(Constants.USERID);
                        RingLogger.getConfigPortalLogger().debug("##  myAppError --> " + new Gson().toJson(myAppError));

                        dto = scheduler.getEventDTO(eventForm);
                        RingLogger.getActivityLogger().debug("[add EventAction] activistName : " + activistName + " eventDTO --> " + new Gson().toJson(dto));
                        reasonCode = scheduler.createEvent(dto);
                        RingLogger.getActivityLogger().debug("[add EventAction] activistName : " + activistName + " reasonCode --> " + reasonCode);
                        if (reasonCode == ReasonCode.NONE) {
                            request.setAttribute("message", "<span style='color:green'>Operation successfull</span>");
                        } else {
                            request.setAttribute("message", "<span style='color:red'>Operation failed[ rc : " + reasonCode + " ]</span>");
                            target = FAILURE;
                        }
                        break;
                    case Constants.EDIT:
                        eventForm.setAction(Constants.UPDATE);
                        break;
                    case Constants.UPDATE:
                        myAppError = LoginTaskScheduler.getInstance().login(Constants.USERID, Constants.PASSWORD);
                        RingLogger.getConfigPortalLogger().debug("##  login --> " + new Gson().toJson(myAppError));
                        if (eventForm.getTheFileProfileImage() != null && eventForm.getTheFileProfileImage().getFileName().length() > 0 && eventForm.getTheFileProfileImage().getFileData().length > 0) {
                            myFile = eventForm.getTheFileProfileImage();
                            fileName = myFile.getFileName();
                            fileData = myFile.getFileData();
                            profileImageUrl = new UploadBannerImage().upLoadToCloud(Constants.USERID, fileName, fileData);
                            RingLogger.getConfigPortalLogger().debug("##  profileImageUrl --> " + profileImageUrl);
                            eventForm.setProfileImageUrl(profileImageUrl);
                        }
                        if (eventForm.getTheFileCoverImage() != null && eventForm.getTheFileCoverImage().getFileName().length() > 0 && eventForm.getTheFileCoverImage().getFileData().length > 0) {
                            myFile = eventForm.getTheFileCoverImage();
                            fileName = myFile.getFileName();
                            fileData = myFile.getFileData();
                            coverimageUrl = new UploadBannerImage().upLoadToCloud(Constants.USERID, fileName, fileData);
                            RingLogger.getConfigPortalLogger().debug("##  coverimageUrl --> " + coverimageUrl);
                            eventForm.setCoverImageUrl(coverimageUrl);
                        }
                        myAppError = LoginTaskScheduler.getInstance().logout(Constants.USERID);
                        RingLogger.getConfigPortalLogger().debug("##  myAppError --> " + new Gson().toJson(myAppError));

                        dto = scheduler.getEventDTO(eventForm);
                        RingLogger.getActivityLogger().debug("[add EventAction] activistName : " + activistName + " eventDTO --> " + new Gson().toJson(dto));
                        reasonCode = scheduler.createEvent(dto);
                        RingLogger.getActivityLogger().debug("[add EventAction] activistName : " + activistName + " reasonCode --> " + reasonCode);
                        if (reasonCode == ReasonCode.NONE) {
                            request.setAttribute("message", "<span style='color:green'>Operation successfull</span>");
                        } else {
                            request.setAttribute("message", "<span style='color:red'>Operation failed[ rc : " + reasonCode + " ]</span>");
                            target = FAILURE;
                        }

                        dto = scheduler.getEventDTO(eventForm);
                        RingLogger.getActivityLogger().debug("[update EventAction] activistName : " + activistName + " eventDTO --> " + new Gson().toJson(dto));
                        reasonCode = scheduler.updateEvent(dto);
                        RingLogger.getActivityLogger().debug("[update EventAction] activistName : " + activistName + " reasonCode --> " + reasonCode);
                        if (reasonCode == ReasonCode.NONE) {
                            request.setAttribute("message", "<span style='color:green'>Operation successfull</span>");
                        } else {
                            request.setAttribute("message", "<span style='color:red'>Operation failed[ rc : " + reasonCode + " ]</span>");
                            target = FAILURE;
                        }
                        break;
                    case Constants.DELETE:
                        RingLogger.getActivityLogger().debug("[delete EventAction] activistName : " + activistName + " id --> " + eventForm.getId());
                        reasonCode = scheduler.deleteEvent(eventForm.getId());
                        RingLogger.getActivityLogger().debug("[delete EventAction] activistName : " + activistName + " reasonCode --> " + reasonCode);
                        if (reasonCode == ReasonCode.NONE) {
                            request.setAttribute("message", "<span style='color:green'>Operation successfull</span>");
                        } else {
                            request.setAttribute("message", "<span style='color:red'>Operation failed[ rc : " + reasonCode + " ]</span>");
                            target = FAILURE;
                        }
                        break;
                }
            } else {
                request.setAttribute("message", "<span style='color:red'>Invalid Operation</span>");
            }
        } catch (Exception e) {
            target = FAILURE;
        }
        return mapping.findForward(target);
    }
}
