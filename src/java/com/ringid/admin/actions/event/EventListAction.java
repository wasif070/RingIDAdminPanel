/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.event;

import com.ringid.admin.service.EventService;
import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.Constants;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.ringid.event.EventDTO;
import org.ringid.utilities.AppConstants;

/**
 *
 * @author mamun
 */
public class EventListAction extends BaseAction {

    List<EventDTO> list = new ArrayList<>();

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = FAILURE;
        try {
            EventForm eventForm = (EventForm) form;
            EventService scheduler = EventService.getInstance();
            int start = 0;
            if (eventForm.getPageNo() > 0) {
                start = (eventForm.getPageNo() - 1) * eventForm.getRecordPerPage();
            }
            switch (eventForm.getEventType()) {
                case AppConstants.EVENT_LIST_TYPE.ONGOING:
                    list = scheduler.getOngoingEvents(eventForm.getStartTimeLong(), start, eventForm.getRecordPerPage());
                    break;
                case AppConstants.EVENT_LIST_TYPE.UPCOMING:
                    list = scheduler.getUpcomingEvents(eventForm.getStartTimeLong(), start, eventForm.getRecordPerPage());
                    break;
                case AppConstants.EVENT_LIST_TYPE.PAST:
                    list = scheduler.getPastEvents(eventForm.getStartTimeLong(), start, eventForm.getRecordPerPage());
                    break;
            }
            request.getSession(true).setAttribute(Constants.DATA_ROWS, list);
            managePages(request, eventForm, list.size());
        } catch (Exception e) {
            target = FAILURE;
        }
        return mapping.findForward(target);
    }
}
