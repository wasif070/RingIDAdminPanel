/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.event;

import com.ringid.admin.BaseForm;
import com.ringid.admin.dto.adminAuth.LoginDTO;
import com.ringid.admin.utils.Constants;
import javax.servlet.http.HttpServletRequest;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.upload.FormFile;

/**
 *
 * @author mamun
 */
public class EventForm extends BaseForm {

    private long id;
    private int eventType;
    private long ownerId;
    private int ownerType;
    private String name;
    private String profileImageUrl;
    private String coverImageUrl;
    private long startTimeLong;
    private String startDate;
    private String startTime;
    private long endTimeLong;
    private String endDate;
    private String endTime;
    private Object data;
    private FormFile theFileProfileImage;
    private FormFile theFileCoverImage;

    public FormFile getTheFileProfileImage() {
        return theFileProfileImage;
    }

    public void setTheFileProfileImage(FormFile theFileProfileImage) {
        this.theFileProfileImage = theFileProfileImage;
    }

    public FormFile getTheFileCoverImage() {
        return theFileCoverImage;
    }

    public void setTheFileCoverImage(FormFile theFileCoverImage) {
        this.theFileCoverImage = theFileCoverImage;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getEventType() {
        return eventType;
    }

    public void setEventType(int eventType) {
        this.eventType = eventType;
    }

    public long getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(long ownerId) {
        this.ownerId = ownerId;
    }

    public int getOwnerType() {
        return ownerType;
    }

    public void setOwnerType(int ownerType) {
        this.ownerType = ownerType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProfileImageUrl() {
        return profileImageUrl;
    }

    public void setProfileImageUrl(String profileImageUrl) {
        this.profileImageUrl = profileImageUrl;
    }

    public String getCoverImageUrl() {
        return coverImageUrl;
    }

    public void setCoverImageUrl(String coverImageUrl) {
        this.coverImageUrl = coverImageUrl;
    }

    public long getStartTimeLong() {
        return startTimeLong;
    }

    public void setStartTimeLong(long startTimeLong) {
        this.startTimeLong = startTimeLong;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public long getEndTimeLong() {
        return endTimeLong;
    }

    public void setEndTimeLong(long endTimeLong) {
        this.endTimeLong = endTimeLong;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    @Override
    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        ActionErrors errors = new ActionErrors();
        LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        if (loginDTO == null) {
            errors.add("auth", new ActionMessage("UnAuthorized Access"));
        }
        return errors;
    }
}
