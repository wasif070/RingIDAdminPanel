/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.ringIdDistributor;

import com.ringid.admin.dto.RingIdDistributorDTO;
import com.ringid.admin.service.RingIdDistributorService;
import com.ringid.admin.repository.RingIdDistributorLoader;
import com.google.gson.Gson;
import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.dto.adminAuth.LoginDTO;
import com.ringid.admin.utils.Constants;
import com.ringid.admin.utils.MyAppError;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author Rabby
 */
public class RingIdDistributorUpdateAction extends BaseAction {

    Logger logger = RingLogger.getConfigPortalLogger();

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = SUCCESS;
        String activistName;
        try {
            LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
            activistName = loginDTO.getUserName();
        } catch (Exception e) {
            target = UNAUTHORIZED;
            return mapping.findForward(target);
        }
        try {
            RingIdDistributorForm ringIdDistributorForm = (RingIdDistributorForm) form;
            RingIdDistributorDTO oldDTO = new RingIdDistributorDTO();
            RingIdDistributorDTO newDTO = new RingIdDistributorDTO();

            oldDTO.setServerId(ringIdDistributorForm.getOldServerId());
            oldDTO.setStartRingId(ringIdDistributorForm.getOldStartRingId());
            oldDTO.setEndRingId(ringIdDistributorForm.getOldEndRingId());

            newDTO.setServerId(ringIdDistributorForm.getServerId());
            newDTO.setStartRingId(ringIdDistributorForm.getStartRingId());
            newDTO.setEndRingId(ringIdDistributorForm.getEndRingId());
            newDTO.setStatus(ringIdDistributorForm.getStatus());
            RingLogger.getActivityLogger().debug("Action : RingIdDistributorUpdateAction[update] activistName : " + activistName + " RingIdDistributorDTO --> " + new Gson().toJson(newDTO));
            MyAppError error = RingIdDistributorService.getInstance().updateRingIdDistributor(oldDTO, newDTO);
            if (error.getERROR_TYPE() == MyAppError.NOERROR) {
                RingIdDistributorLoader.getInstance().forceReload();
                request.getSession(true).setAttribute("message", "<span style='color: green'>Successfully Updated</span>");
            } else {
                target = FAILURE;
                request.getSession(true).setAttribute("message", "<span style='color: red'>Failed to update(" + error.getErrorMessage() + ")</span>");
            }
        } catch (Exception e) {
            logger.error(e);
            request.getSession(true).setAttribute("message", "<span style='color: red'>Failed to update(" + e.getMessage() + ")</span>");
            target = FAILURE;
        }

        return mapping.findForward(target);
    }
}
