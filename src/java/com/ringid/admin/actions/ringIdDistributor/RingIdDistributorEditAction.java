/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.ringIdDistributor;

import com.ringid.admin.dto.RingIdDistributorDTO;
import com.ringid.admin.service.RingIdDistributorService;
import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.utils.MyAppError;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author Rabby
 */
public class RingIdDistributorEditAction extends BaseAction {

    Logger logger = RingLogger.getConfigPortalLogger();

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = SUCCESS;
        try {
            RingIdDistributorForm ringIdDistributorForm = (RingIdDistributorForm) form;
            RingIdDistributorDTO dto = new RingIdDistributorDTO();
            if (request.getParameter("serverId") != null && request.getParameter("serverId").length() > 0) {
                dto.setServerId(Integer.valueOf(request.getParameter("serverId")));
            }
            if (request.getParameter("startRingId") != null && request.getParameter("startRingId").length() > 0) {
                dto.setStartRingId(Long.valueOf(request.getParameter("startRingId")));
            }
            if (request.getParameter("endRingId") != null && request.getParameter("endRingId").length() > 0) {
                dto.setEndRingId(Long.valueOf(request.getParameter("endRingId")));
            }

            RingIdDistributorDTO ringIdDistributorDTO = RingIdDistributorService.getInstance().getRingIdDistributorDTO(dto);
            if (ringIdDistributorDTO != null) {
                ringIdDistributorForm.setServerId(ringIdDistributorDTO.getServerId());
                ringIdDistributorForm.setStartRingId(ringIdDistributorDTO.getStartRingId());
                ringIdDistributorForm.setEndRingId(ringIdDistributorDTO.getEndRingId());
                ringIdDistributorForm.setStatus(ringIdDistributorDTO.getStatus());

                ringIdDistributorForm.setOldServerId(ringIdDistributorDTO.getServerId());
                ringIdDistributorForm.setOldStartRingId(ringIdDistributorDTO.getStartRingId());
                ringIdDistributorForm.setOldEndRingId(ringIdDistributorDTO.getEndRingId());
                target = SUCCESS;
            } else {
                target = FAILURE;
            }
        } catch (Exception e) {
            logger.error(e);
            target = FAILURE;
        }
        return mapping.findForward(target);
    }
}
