/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.ringIdDistributor;

import com.ringid.admin.BaseForm;
import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author Rabby
 */
public class RingIdDistributorForm extends BaseForm {

    private int serverId;
    private long startRingId;
    private long endRingId;
    private int status;
    private int oldServerId;
    private long oldStartRingId;
    private long oldEndRingId;

    public int getOldServerId() {
        return oldServerId;
    }

    public void setOldServerId(int oldServerId) {
        this.oldServerId = oldServerId;
    }

    public long getOldStartRingId() {
        return oldStartRingId;
    }

    public void setOldStartRingId(long oldStartRingId) {
        this.oldStartRingId = oldStartRingId;
    }

    public long getOldEndRingId() {
        return oldEndRingId;
    }

    public void setOldEndRingId(long oldEndRingId) {
        this.oldEndRingId = oldEndRingId;
    }

    public int getServerId() {
        return serverId;
    }

    public void setServerId(int serverId) {
        this.serverId = serverId;
    }

    public long getStartRingId() {
        return startRingId;
    }

    public void setStartRingId(long startRingId) {
        this.startRingId = startRingId;
    }

    public long getEndRingId() {
        return endRingId;
    }

    public void setEndRingId(long endRingId) {
        this.endRingId = endRingId;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        ActionErrors errors = new ActionErrors();
        return errors;
    }
}
