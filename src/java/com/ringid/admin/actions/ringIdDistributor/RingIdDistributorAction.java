/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.ringIdDistributor;

import com.ringid.admin.dto.RingIdDistributorDTO;
import com.ringid.admin.service.RingIdDistributorService;
import com.ringid.admin.repository.RingIdDistributorLoader;
import com.google.gson.Gson;
import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.dto.adminAuth.LoginDTO;
import com.ringid.admin.utils.Constants;
import com.ringid.admin.utils.MyAppError;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author Rabby
 */
public class RingIdDistributorAction extends BaseAction {

    Logger logger = RingLogger.getConfigPortalLogger();

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = SUCCESS;
        String activistName;
        try {
            LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
            activistName = loginDTO.getUserName();
        } catch (Exception e) {
            target = UNAUTHORIZED;
            return mapping.findForward(target);
        }
        try {
            RingIdDistributorForm ringIdDistributorForm = (RingIdDistributorForm) form;
            RingIdDistributorDTO dto = new RingIdDistributorDTO();
            dto.setServerId(ringIdDistributorForm.getServerId());
            dto.setStartRingId(ringIdDistributorForm.getStartRingId());
            dto.setEndRingId(ringIdDistributorForm.getEndRingId());
            dto.setStatus(ringIdDistributorForm.getStatus());
            RingLogger.getActivityLogger().debug("Action : RingIdDistributorAction[add] activistName : " + activistName + " RingIdDistributorDTO --> " + new Gson().toJson(dto));
            MyAppError error = RingIdDistributorService.getInstance().addRingIdDistributor(dto);
            if (error.getERROR_TYPE() == MyAppError.NOERROR) {
                RingIdDistributorLoader.getInstance().forceReload();
                request.getSession(true).setAttribute("message", "<span style='color: green'>Successfully added</span>");
            } else {
                target = FAILURE;
                request.getSession(true).setAttribute("message", "<span style='color: red'>Failed to added(" + error.getErrorMessage() + ")</span>");
            }
        } catch (Exception e) {
            request.getSession(true).setAttribute("message", "<span style='color: red'>Failed to added(" + e.getMessage() + ")</span>");
            target = FAILURE;
        }
        return mapping.findForward(target);
    }
}
