/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.ringIdDistributor;

import com.ringid.admin.dto.RingIdDistributorDTO;
import com.ringid.admin.service.RingIdDistributorService;
import com.ringid.admin.repository.RingIdDistributorLoader;
import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.Constants;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author Rabby
 */
public class RingIdDistributorListAction extends BaseAction {

    private List<RingIdDistributorDTO> list = new ArrayList<>();

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = SUCCESS;
        try {
            RingIdDistributorForm ringIdDistributorForm = (RingIdDistributorForm) form;
            RingIdDistributorDTO dto = new RingIdDistributorDTO();
            if (request.getParameter("ForceReload") != null) {
                RingIdDistributorLoader.getInstance().forceReload();
            }
            if (ringIdDistributorForm.getSearchText() != null && ringIdDistributorForm.getSearchText().length() > 0) {
                dto.setSearchText(ringIdDistributorForm.getSearchText().trim().toLowerCase());
            }
            if (ringIdDistributorForm.getColumn() <= 0) {
                ringIdDistributorForm.setColumn(Constants.COLUMN_ONE);
                ringIdDistributorForm.setSort(Constants.DESC_SORT);
            }
            dto.setColumn(ringIdDistributorForm.getColumn());
            dto.setSortType(ringIdDistributorForm.getSort());
            list = RingIdDistributorService.getInstance().getRingIdDistributorList(dto);
            request.getSession(true).setAttribute(Constants.DATA_ROWS, list);
            managePages(request, ringIdDistributorForm, list.size());
        } catch (Exception e) {
            target = FAILURE;
        }
        return mapping.findForward(target);
    }
}
