/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.settings;

import com.ringid.admin.dto.SettingDTO;
import com.ringid.admin.service.SettingService;
import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author ipvision
 */
public class SettingListEditAction extends BaseAction {

    Logger logger = RingLogger.getConfigPortalLogger();

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = checkAuthentication(mapping, form, request);
        try {
            SettingListForm setttingListForm = (SettingListForm) form;
            if (target.equals(SUCCESS)) {
                if (setttingListForm.getSelectedIDs() != null && setttingListForm.getSelectedIDs().length > 0) {
                    SettingService scheduler = SettingService.getInstance();
                    List<SettingDTO> list = scheduler.getSettingInfoByIds(setttingListForm.getSelectedIDs());
                    setttingListForm.setListItems(list);
                }
            }
        } catch (Exception e) {
            logger.error("SettingListEditAction Exception: " + e);
            target = FAILURE;
        }
        return mapping.findForward(target);
    }
}
