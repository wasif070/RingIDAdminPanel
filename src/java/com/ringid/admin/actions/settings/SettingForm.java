/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.settings;

import com.ringid.admin.dto.SettingDTO;
import com.ringid.admin.BaseForm;
import com.ringid.admin.dto.adminAuth.UserRoleDTO;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import javax.servlet.http.HttpServletRequest;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author ipvision
 */
public class SettingForm extends BaseForm {

    private long id;
    private String name;
    private String settingsValue;
    private Integer dataType;
    private ArrayList<UserRoleDTO> userRoles;
    private Set<Integer> permittedRoles;
    private int[] permittedRoleIds;
    private List<SettingDTO> settingDTOs;

    public List<SettingDTO> getSettingDTOs() {
        return settingDTOs;
    }

    public void setSettingDTOs(List<SettingDTO> settingDTOs) {
        this.settingDTOs = settingDTOs;
    }

    public int[] getPermittedRoleIds() {
        return permittedRoleIds;
    }

    public void setPermittedRoleIds(int[] permittedRoleIds) {
        this.permittedRoleIds = permittedRoleIds;
    }

    public ArrayList<UserRoleDTO> getUserRoles() {
        return userRoles;
    }

    public void setUserRoles(ArrayList<UserRoleDTO> userRoles) {
        this.userRoles = userRoles;
    }

    public Set<Integer> getPermittedRoles() {
        return permittedRoles;
    }

    public void setPermittedRoles(Set<Integer> permittedRoles) {
        this.permittedRoles = permittedRoles;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSettingsValue() {
        return settingsValue;
    }

    public void setSettingsValue(String settingsValue) {
        this.settingsValue = settingsValue;
    }

    public Integer getDataType() {
        return dataType;
    }

    public void setDataType(Integer dataType) {
        this.dataType = dataType;
    }

    /**
     *
     */
    public SettingForm() {
        super();
        // TODO Auto-generated constructor stub
    }

    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        ActionErrors errors = new ActionErrors();

        return errors;
    }
}
