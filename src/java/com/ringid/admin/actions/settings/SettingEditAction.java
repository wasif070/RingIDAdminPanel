/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.settings;

import com.ringid.admin.dto.SettingDTO;
import com.ringid.admin.service.SettingService;
import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.dto.adminAuth.LoginDTO;
import com.ringid.admin.actions.userFeatureManagement.UserFeatureTaskScheduler;
import com.ringid.admin.dto.adminAuth.UserRoleDTO;
import com.ringid.admin.utils.Constants;
import com.ringid.country.CountryDTO;
import com.ringid.country.CountryLoader;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author ipvision
 */
public class SettingEditAction extends BaseAction {

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = checkAuthentication(mapping, form, request);
        LoginDTO loginDTO = null;
        try {
            loginDTO = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        } catch (Exception e) {
            target = UNAUTHORIZED;
            return mapping.findForward(target);
        }
        try {
            if (target.equals(SUCCESS)) {
                long id = 0;
                SettingForm settingForm = (SettingForm) form;
                if (request.getParameter("id") != null) {
                    id = Long.parseLong(request.getParameter("id"));
                }
                SettingService scheduler = SettingService.getInstance();
                SettingDTO settingDTO = scheduler.getSettingInfoById(id, loginDTO);
                if (settingDTO != null) {
                    settingForm.setId(settingDTO.getId());
                    settingForm.setName(settingDTO.getName());
                    settingForm.setSettingsValue(settingDTO.getSettingsValue());
                    settingForm.setDataType(settingDTO.getDataType());
                    if (loginDTO.getPermissionLevel() == Constants.SUPER_ADMIN) {
                        settingForm.setUserRoles(UserFeatureTaskScheduler.getInstance().getUserRoleList(new UserRoleDTO()));
                        settingForm.setPermittedRoles(SettingService.getInstance().getSettingPermissonById(id));
                    }

                    if (settingForm.getName().trim().toLowerCase().equals("home-page-banner-type-seq")) {
                        List<SettingDTO> settingValueList = new ArrayList<>();
                        String[] settingValues = settingForm.getSettingsValue().split(",");
                        Set<Long> testSet = new HashSet<>();
                        if (settingValues != null && settingValues.length > 0) {
                            for (String str : settingValues) {
                                if (str != null && str.trim().length() > 0) {
                                    int val = Integer.parseInt(str.trim());
                                    if (val > 0 && val < DataType.HOME_PAGE_BANNER_TYPE_SEQ.length) {
                                        SettingDTO dto = new SettingDTO();
                                        dto.setSettingsValue(DataType.HOME_PAGE_BANNER_TYPE_SEQ[val]);
                                        dto.setId(val);
                                        dto.setStatus(true);
                                        settingValueList.add(dto);
                                        testSet.add(dto.getId());
                                    }
                                }
                            }
                        }
                        for (int i = 1; i < DataType.HOME_PAGE_BANNER_TYPE_SEQ.length; i++) {
                            if (!testSet.contains(new Long(i))) {
                                SettingDTO dto = new SettingDTO();
                                dto.setSettingsValue(DataType.HOME_PAGE_BANNER_TYPE_SEQ[i]);
                                dto.setId(i);
                                dto.setStatus(false);
                                settingValueList.add(dto);
                                testSet.add(new Long(i));
                            }
                        }
                        settingForm.setSettingDTOs(settingValueList);
                    } else if (settingForm.getName().trim().toLowerCase().equals("call-verification-enabled-country-dialing-codes") || settingForm.getName().trim().toLowerCase().equals("marketplace-enabled-country-dialing-codes")) {
                        List<SettingDTO> settingValueList = new ArrayList<>();
                        String[] settingValues = settingForm.getSettingsValue().split(",");
                        Set<String> testSet = new HashSet<>();
                        CountryLoader countryLoader = CountryLoader.getInstance();
                        List<CountryDTO> countryDTOs = countryLoader.getCountryList();
                        if (settingValues != null && settingValues.length > 0) {
                            for (String str : settingValues) {
                                if (str != null && str.trim().length() > 0) {
                                    SettingDTO dto = new SettingDTO();
                                    dto.setName(countryLoader.getCountryName(str.trim()));
                                    dto.setSettingsValue(str.trim());
                                    dto.setStatus(true);
                                    settingValueList.add(dto);
                                    testSet.add(str.trim());
                                }
                            }
                        }
                        for (CountryDTO countryDTO : countryDTOs) {
                            if (!testSet.contains(countryDTO.getDialingCode())) {
                                SettingDTO dto = new SettingDTO();
                                dto.setName(countryDTO.getName());
                                dto.setSettingsValue(countryDTO.getDialingCode());
                                dto.setStatus(false);
                                settingValueList.add(dto);
                                testSet.add(countryDTO.getDialingCode());
                            }
                        }
                        settingForm.setSettingDTOs(settingValueList);
                    }
                    target = SUCCESS;
                } else {
                    target = FAILURE;
                }
            }
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("SettingEditAction Exception: " + e);
            target = FAILURE;
        }
        return mapping.findForward(target);
    }
}
