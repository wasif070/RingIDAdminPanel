/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.settings;

import com.ringid.admin.service.SettingService;
import com.ringid.admin.repository.SettingLoader;
import com.google.gson.Gson;
import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.dto.adminAuth.LoginDTO;
import com.ringid.admin.utils.Constants;
import com.ringid.admin.utils.MyAppError;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author ipvision
 */
public class SettingListUpdateAction extends BaseAction {

    static Logger logger = RingLogger.getConfigPortalLogger();

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = checkAuthentication(mapping, form, request);
        request.getSession(true).setAttribute("settingErrorMessege", "");
        String activistName;
        try {
            LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
            activistName = loginDTO.getUserName();
        } catch (Exception e) {
            target = UNAUTHORIZED;
            return mapping.findForward(target);
        }
        try {
            if (target.equals(SUCCESS)) {
                SettingListForm settingListForm = (SettingListForm) form;
                SettingService scheduler = SettingService.getInstance();
                RingLogger.getActivityLogger().debug("[SettingListUpdateAction] activistName : " + activistName + " settingListDTO --> " + new Gson().toJson(settingListForm.getListItems()));
                MyAppError error = scheduler.updateSettingList(settingListForm.getListItems());
                error.setERROR_TYPE(MyAppError.NOERROR);
                if (error.getERROR_TYPE() > 0) {
                    if (error.getERROR_TYPE() == MyAppError.VALIDATIONERROR) {
                        logger.error("SettingListUpdateAction Other error " + error.getERROR_TYPE());
                        settingListForm.setMessage(error.getErrorMessage());
                        request.getSession(true).setAttribute("settingErrorMessege", "<span style='color:red'>Update failed!</span>");
                    }
                    target = FAILURE;
                } else {
                    SettingLoader.getInstance().forceReload();
                }
            }
        } catch (Exception e) {
            logger.error("SettingListUpdateAction Exception: " + e);
            target = FAILURE;
            request.getSession(true).setAttribute("settingErrorMessege", "<span style='color:red'>Update failed!</span>");
        }
        return mapping.findForward(target);
    }
}
