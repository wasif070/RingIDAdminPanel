/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.settings;

import com.ringid.admin.service.SettingService;
import com.ringid.admin.repository.SettingLoader;
import com.google.gson.Gson;
import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.dto.adminAuth.LoginDTO;
import com.ringid.admin.utils.Constants;
import com.ringid.admin.utils.MyAppError;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author ipvision
 */
public class SettingDeleteAction extends BaseAction {

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = checkAuthentication(mapping, form, request);
        String activistName;
        SettingForm settingForm = (SettingForm) form;
        try {
            LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
            activistName = loginDTO.getUserName();
        } catch (Exception e) {
            target = UNAUTHORIZED;
            return mapping.findForward(target);
        }
        try {
            if (target.equals(SUCCESS)) {
                SettingService scheduler = SettingService.getInstance();
                long id = 0;
                if (request.getParameter("id") != null) {
                    id = Long.parseLong(request.getParameter("id"));
                    RingLogger.getActivityLogger().debug("[SettingDeleteAction] activistName : " + activistName + " Id --> " + id);
                    MyAppError error = scheduler.deleteSettingInfo(id);
                    if (error.getERROR_TYPE() > 0) {
                        target = FAILURE;
                    } else {
                        SettingLoader.getInstance().forceReload();
                    }
                } else if (settingForm.getSelectedIDs() != null && settingForm.getSelectedIDs().length > 0) {
                    RingLogger.getActivityLogger().debug("[SettingDeleteAction] activistName : " + activistName + " selectedIDs --> " + new Gson().toJson(settingForm.getSelectedIDs()));
                    MyAppError error = new MyAppError();
                    for (String str : settingForm.getSelectedIDs()) {
                        error = scheduler.deleteSettingInfo(Long.parseLong(str));
                    }
                    if (error.getERROR_TYPE() > 0) {
                        target = FAILURE;
                    } else {
                        SettingLoader.getInstance().forceReload();
                    }
                }
            }
        } catch (Exception e) {
            target = FAILURE;
        }
        return mapping.findForward(target);
    }
}
