/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.settings;

import java.util.ArrayList;

/**
 *
 * @author ipvision
 */
public class DataType {

    public static final int DATA_TYPE_STRING = 1;
    public static final int DATA_TYPE_INT = 2;
    public static final int DATA_TYPE_LONG = 3;
    public static final int DATA_TYPE_FLOAT = 4;
    public static final int DATA_TYPE_DOUBLE = 5;
    public static final int DATA_TYPE_BOOLEAN = 6;
    // Home-Page-Banner-Type-Seq
    public static final String[] HOME_PAGE_BANNER_TYPE_SEQ = {"NONE", "FRAME", "CELEBRITY SCHEDULE", "BEAUTY CONTEST", "DONATION PAGE", "MATCH LIST", "LIVE TO CHANNEL", "RINGBIT", "WISH BANNER", "WALLET BANNER"};

    public static String getDataTypeToName(int dataType) {
        switch (dataType) {
            case DataType.DATA_TYPE_STRING:
                return "STRING";
            case DataType.DATA_TYPE_INT:
                return "INT";
            case DataType.DATA_TYPE_LONG:
                return "LONG";
            case DataType.DATA_TYPE_FLOAT:
                return "FLOAT";
            case DataType.DATA_TYPE_DOUBLE:
                return "DOUBLE";
            case DataType.DATA_TYPE_BOOLEAN:
                return "BOOLEAN";
            default:
                return "UNKNOWN";
        }
    }

    public static ArrayList<Integer> getDataTypes() {
        ArrayList<Integer> types = new ArrayList<>();
        types.add(DATA_TYPE_STRING);
        types.add(DATA_TYPE_INT);
        types.add(DATA_TYPE_LONG);
        types.add(DATA_TYPE_FLOAT);
        types.add(DATA_TYPE_DOUBLE);
        types.add(DATA_TYPE_BOOLEAN);
        return types;
    }

}
