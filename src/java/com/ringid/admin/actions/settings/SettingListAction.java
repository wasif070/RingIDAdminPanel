/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.settings;

import com.ringid.admin.dto.SettingDTO;
import com.ringid.admin.service.SettingService;
import com.ringid.admin.repository.SettingLoader;
import com.ringid.admin.BaseAction;
import com.ringid.admin.dto.adminAuth.LoginDTO;
import com.ringid.admin.utils.Constants;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author ipvision
 */
public class SettingListAction extends BaseAction {

    List<SettingDTO> list = new ArrayList<>();

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = SUCCESS;
        LoginDTO loginDTO = null;
        try {
            loginDTO = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        } catch (Exception e) {
            target = UNAUTHORIZED;
            return mapping.findForward(target);
        }
        try {
            SettingForm settingForm = (SettingForm) form;
            SettingDTO settingDTO = new SettingDTO();
            if (request.getParameter("ForceReload") != null && "1".equals(request.getParameter("ForceReload"))) {
                SettingLoader.getInstance().forceReload();
            }
            SettingService scheduler = SettingService.getInstance();
            if (settingForm.getSearchText() != null && settingForm.getSearchText().length() > 0) {
                settingDTO.setSearchText(settingForm.getSearchText().trim().toLowerCase());
            }
            list = scheduler.getSettingList(settingDTO, loginDTO);
            request.getSession(true).setAttribute(Constants.DATA_ROWS, list);
            managePages(request, settingForm, list.size());
            request.getSession(true).setAttribute("search", settingDTO.getName());
        } catch (NullPointerException e) {

        }
        return mapping.findForward(target);
    }
}
