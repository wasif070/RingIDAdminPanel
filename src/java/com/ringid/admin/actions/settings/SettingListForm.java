/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.settings;

import com.ringid.admin.dto.SettingDTO;
import com.ringid.admin.BaseForm;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.collections.Factory;
import org.apache.commons.collections.ListUtils;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author ipvision
 */
public class SettingListForm extends BaseForm implements Factory {

     protected  List<SettingDTO> listItems = ListUtils.lazyList(new ArrayList(),  this);

      public List<SettingDTO> getListItems() {
          return listItems;
      }

      public void setListItems(final List<SettingDTO> newList) {
          listItems = ListUtils.lazyList(newList,  this);
      }

      /**
       * Factory method for LazyList
       * @return 
       */
     @Override
      public Object create() {
          return new SettingDTO();
      }

    /**
     *
     */
    public SettingListForm() {
        super();
        // TODO Auto-generated constructor stub
    }

    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        ActionErrors errors = new ActionErrors();

        return errors;
    }
}
