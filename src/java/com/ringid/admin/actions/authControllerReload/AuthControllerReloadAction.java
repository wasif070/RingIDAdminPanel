/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.authControllerReload;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.utils.Constants;
import com.ringid.admin.utils.MyAppError;
import com.ringid.admin.utils.Utils;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author Rabby
 */
public class AuthControllerReloadAction extends BaseAction {

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {

        int SUCCESS_COUNT = 0, FAILURE_COUNT = 0;
        MyAppError error = new MyAppError();
        String target = checkAuthentication(mapping, form, request);
        AuthControllerReloadForm authControllerReloadForm = (AuthControllerReloadForm) form;
        if (target.equals(SUCCESS)) {
            String name = authControllerReloadForm.getName();
            switch (name) {
                case Constants.API_RELOAD_CONSTANT:
                    error = Utils.forceConstantAPIToReload();
                    break;
                case Constants.API_RELOAD_AUTH_SERVER:
                    error = Utils.forceAuthServerAPIToReload();
                    break;
                case Constants.API_RELOAD_VOICE_SETTINGS:
                    error = Utils.forceVoiceSettingsAPIToReload();
                    break;
                case Constants.API_RELOAD_APP_UPDATE:
                    notifyAuthServers(Constants.RELOAD_APP_UPDATE_URL, null);
                    error = Utils.forceAppUpdateUrlsAPIToReload();
                    break;
                default:
                    error.setERROR_TYPE(MyAppError.OTHERERROR);
                    error.setErrorMessage("Failed To Reload");
                    break;
            }
            if (error.getERROR_TYPE() > 0) {
                request.getSession(true).setAttribute("errorMessage", error.getErrorMessage());
                target = FAILURE;
            } else {
                JsonArray jsonArray = (new JsonParser()).parse(error.getErrorMessage()).getAsJsonArray();
                for (int i = 0; i < jsonArray.size(); i++) {
                    JsonObject jsonObject = jsonArray.get(i).getAsJsonObject();
                    if (jsonObject.get("success").getAsBoolean()) {
                        SUCCESS_COUNT++;
                    } else {
                        FAILURE_COUNT++;
                    }
                }
                request.getSession(true).setAttribute("errorMessage", "Success Response : " + SUCCESS_COUNT + "<br/>Failure Response : " + FAILURE_COUNT);
            }
        } else {
            RingLogger.getConfigPortalLogger().debug("Auth Controller Reload error: authonication error");
        }
        return mapping.findForward(target);
    }
}
