/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.permittedIps;

import com.ringid.admin.dto.PermittedIpsDTO;
import com.ringid.admin.service.PermittedIpsService;
import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author Rabby
 */
public class PermittedIpsEditAction extends BaseAction {

    private static Logger logger = RingLogger.getConfigPortalLogger();

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        String target = checkAuthentication(mapping, form, request);
        try {
            if (target.equals(SUCCESS)) {
                int id = 0;
                PermittedIpsForm permittedIpsForm = (PermittedIpsForm) form;
                if (request.getParameter("id") != null) {
                    id = Integer.parseInt(request.getParameter("id"));
                }
                PermittedIpsService scheduler = PermittedIpsService.getInstance();
                PermittedIpsDTO permittedIpsDTO = scheduler.getPermittedIpsInfoById(id);
                if (permittedIpsDTO != null) {
                    permittedIpsForm.setId(permittedIpsDTO.getId());
                    permittedIpsForm.setIp(permittedIpsDTO.getIp());
                    permittedIpsForm.setStatus(permittedIpsDTO.getStatus());
                    permittedIpsForm.setUpdateTime(permittedIpsDTO.getUpdateTime());
                }
            }
        } catch (Exception e) {
            logger.error(e);
            target = FAILURE;
        }
        return mapping.findForward(target);
    }
}
