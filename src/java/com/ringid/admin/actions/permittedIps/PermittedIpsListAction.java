/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.permittedIps;

import com.ringid.admin.dto.PermittedIpsDTO;
import com.ringid.admin.service.PermittedIpsService;
import com.ringid.admin.repository.PermittedIpsLoader;
import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.Constants;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author Rabby
 */
public class PermittedIpsListAction extends BaseAction {

    ArrayList<PermittedIpsDTO> list = new ArrayList<>();

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = FAILURE;
        try {
            PermittedIpsForm permittedIpsForm = (PermittedIpsForm) form;
            PermittedIpsDTO permittedIpsDTO = new PermittedIpsDTO();
            PermittedIpsService scheduler = PermittedIpsService.getInstance();

            if (request.getParameter("ForceReload") != null && "1".equals(request.getParameter("ForceReload"))) {
                PermittedIpsLoader.getInstance().forceReload();
            }

            if (permittedIpsForm.getSearchText() != null && permittedIpsForm.getSearchText().length() > 0) {
                permittedIpsDTO.setQueryString(permittedIpsForm.getSearchText().trim().toLowerCase());
            }

            list = scheduler.getPermittedIpsList(permittedIpsForm.getColumn(), permittedIpsForm.getSort(), permittedIpsDTO);

            request.getSession(true).setAttribute(Constants.DATA_ROWS, list);
            managePages(request, permittedIpsForm, list.size());
            request.getSession(true).setAttribute("search", permittedIpsDTO.getIp());
        } catch (NullPointerException e) {
            target = FAILURE;
        }
        return mapping.findForward(target);
    }
}
