/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.permittedIps;

import com.ringid.admin.dto.PermittedIpsDTO;
import com.ringid.admin.service.PermittedIpsService;
import com.ringid.admin.repository.PermittedIpsLoader;
import com.google.gson.Gson;
import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.dto.adminAuth.LoginDTO;
import com.ringid.admin.utils.Constants;
import com.ringid.admin.utils.MyAppError;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author Rabby
 */
public class PermittedIpsUpdateAction extends BaseAction {

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        String target = checkAuthentication(mapping, form, request);
        String activistName;
        try {
            LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
            activistName = loginDTO.getUserName();
        } catch (Exception e) {
            target = UNAUTHORIZED;
            return mapping.findForward(target);
        }
        try {
            if (target.equals(SUCCESS)) {
                PermittedIpsForm permittedIpsForm = (PermittedIpsForm) form;
                PermittedIpsService scheduler = PermittedIpsService.getInstance();
                PermittedIpsDTO permittedIpsDTO = new PermittedIpsDTO();
                permittedIpsDTO.setIp(permittedIpsForm.getIp());
                permittedIpsDTO.setStatus(permittedIpsForm.getStatus());
                permittedIpsDTO.setUpdateTime(System.currentTimeMillis());
                permittedIpsDTO.setId(permittedIpsForm.getId());
                RingLogger.getActivityLogger().debug("[PermittedIpsUpdateAction] activistName : " + activistName + " permittedIpsDTO --> " + new Gson().toJson(permittedIpsDTO));
                MyAppError error = scheduler.updatePermittedIps(permittedIpsDTO);
                if (error.getERROR_TYPE() > 0) {
                    target = FAILURE;
                    permittedIpsForm.setMessage(error.getErrorMessage());
                    request.getSession(true).setAttribute("error", "2");
                } else {
                    PermittedIpsLoader.getInstance().forceReload();
                }
            }
        } catch (Exception e) {
            target = FAILURE;
        }
        return mapping.findForward(target);
    }
}
