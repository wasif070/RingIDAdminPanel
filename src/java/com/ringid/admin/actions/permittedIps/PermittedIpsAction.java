/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.permittedIps;

import com.ringid.admin.dto.PermittedIpsDTO;
import com.ringid.admin.service.PermittedIpsService;
import com.ringid.admin.repository.PermittedIpsLoader;
import com.google.gson.Gson;
import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.dto.adminAuth.LoginDTO;
import com.ringid.admin.utils.Constants;
import com.ringid.admin.utils.MyAppError;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author Rabby
 */
public class PermittedIpsAction extends BaseAction {

    Logger logger = RingLogger.getConfigPortalLogger();

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = checkAuthentication(mapping, form, request);
        String activistName;
        try {
            LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
            activistName = loginDTO.getUserName();
        } catch (Exception e) {
            target = UNAUTHORIZED;
            return mapping.findForward(target);
        }
        try {
            if (target.equals(SUCCESS)) {
                PermittedIpsForm permittedIpsForm = (PermittedIpsForm) form;
                PermittedIpsService scheduler = PermittedIpsService.getInstance();
                PermittedIpsDTO dto = new PermittedIpsDTO();
                dto.setIp(permittedIpsForm.getIp());
                dto.setStatus(permittedIpsForm.getStatus());
                dto.setUpdateTime(System.currentTimeMillis());
                RingLogger.getActivityLogger().debug("Action : PermittedIpsAction[add] activistName : " + activistName + " permittedIpsDTO --> " + new Gson().toJson(dto));
                MyAppError error = scheduler.addPermittedIpsInfo(dto);
                if (error.getERROR_TYPE() > 0) {
                    if (error.getERROR_TYPE() == MyAppError.OTHERERROR) {
                        logger.error("PermittedIpsAction Other error " + error.getERROR_TYPE());
                        permittedIpsForm.setMessage(error.getErrorMessage());
                        request.getSession(true).setAttribute("error", "2");
                    }
                    target = FAILURE;
                } else {
                    PermittedIpsLoader.getInstance().forceReload();
                }
            }
        } catch (Exception e) {
            target = FAILURE;
        }
        return mapping.findForward(target);
    }
}
