/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.spamReasons;

import com.ringid.admin.dto.SpamReasonsDTO;
import com.ringid.admin.service.SpamReasonsService;
import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author mamun
 * @Date Feb 7, 2017
 */
public class SpamReasonsEditAction extends BaseAction {

    Logger logger = RingLogger.getConfigPortalLogger();

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        String target = checkAuthentication(mapping, form, request);
        try {
            if (target.equals(SUCCESS)) {
                int id = 0;
                SpamReasonsForm spamReasonsForm = (SpamReasonsForm) form;
                if (request.getParameter("id") != null) {
                    id = Integer.parseInt(request.getParameter("id"));
                }
                SpamReasonsService scheduler = SpamReasonsService.getInstance();
                SpamReasonsDTO spamReasonsDTO = scheduler.getSpamReasonsInfoById(id);
                if (spamReasonsDTO != null) {
                    spamReasonsForm.setId(spamReasonsDTO.getId());
                    spamReasonsForm.setSpamType(spamReasonsDTO.getSpamType());
                    spamReasonsForm.setReason(spamReasonsDTO.getReason());
                    target = SUCCESS;
                }
            }
        } catch (Exception e) {
            logger.error(e);
            target = FAILURE;
        }
        return mapping.findForward(target);
    }
}
