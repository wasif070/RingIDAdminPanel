/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.spamReasons;

import com.ringid.admin.dto.SpamReasonsDTO;
import com.ringid.admin.service.SpamReasonsService;
import com.ringid.admin.repository.SpamReasonsLoader;
import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.utils.MyAppError;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author mamun
 * @Date Feb 7, 2017
 */
public class SpamReasonsUploadAction extends BaseAction {

    static Logger logger = RingLogger.getConfigPortalLogger();
    String line = "";
    String cvsSplitBy = ",";

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {

        String target = checkAuthentication(mapping, form, request);
        if (target.equals(SUCCESS)) {
            String[] dataArray = getDataFromFile(form);
            try {
                ArrayList<SpamReasonsDTO> spamReasonsDTOs = new ArrayList<>();

                for (String s : dataArray) {
                    String[] spamReasons = s.split(cvsSplitBy);
                    String spam_type = spamReasons[0];
                    String reason = spamReasons[1];

                    SpamReasonsDTO spamReasonsDTO = new SpamReasonsDTO();
                    spamReasonsDTO.setSpamType(Integer.valueOf(spam_type));
                    spamReasonsDTO.setReason(reason);

                    spamReasonsDTOs.add(spamReasonsDTO);
                }

                MyAppError error = SpamReasonsService.getInstance().addSpamReasonsInfo(spamReasonsDTOs);
                if (error.getERROR_TYPE() > 0) {
                    target = FAILURE;
                } else {
                    SpamReasonsLoader.getInstance().forceReload();
                }
            } catch (Exception e) {
                e.printStackTrace();
                target = FAILURE;
            }
        }
        return mapping.findForward(target);
    }
}
