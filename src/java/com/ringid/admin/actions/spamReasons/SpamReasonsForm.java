/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.spamReasons;

import com.ringid.admin.BaseForm;
import com.ringid.admin.dto.adminAuth.LoginDTO;
import com.ringid.admin.projectMenu.MenuNames;
import com.ringid.admin.utils.Constants;
import javax.servlet.http.HttpServletRequest;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

/**
 *
 * @author mamun
 * @Date Feb 7, 2017
 */
//   'id' tinyint(4) NOT NULL AUTO_INCREMENT,
//   'spam_type' tinyint(4) DEFAULT NULL,
//  'reason' varchar(1024) DEFAULT NULL,
public class SpamReasonsForm extends BaseForm {

    private int id;
    private int spamType;
    private String reason;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSpamType() {
        return spamType;
    }

    public void setSpamType(int spamType) {
        this.spamType = spamType;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    @Override
    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        ActionErrors errors = new ActionErrors();
        LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        if (loginDTO == null) {
            errors.add("auth", new ActionMessage("UnAuthorized Access"));
        } else if (Constants.ADD == getAction() || Constants.UPDATE == getAction()) {
            if (getReason() == null || getReason().length() < 1) {
                errors.add("reason", new ActionMessage("errors.reason.required"));
            }
        }
        return errors;
    }

}
