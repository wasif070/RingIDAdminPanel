/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.spamReasons;

import com.ringid.admin.dto.SpamReasonsDTO;
import com.ringid.admin.service.SpamReasonsService;
import com.ringid.admin.repository.SpamReasonsLoader;
import com.google.gson.Gson;
import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.dto.adminAuth.LoginDTO;
import com.ringid.admin.utils.Constants;
import com.ringid.admin.utils.MyAppError;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author mamun
 * @Date Feb 7, 2017
 */
public class SpamReasonsAction extends BaseAction {

    Logger logger = RingLogger.getConfigPortalLogger();

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = checkAuthentication(mapping, form, request);
        String activistName;
        try {
            LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
            activistName = loginDTO.getUserName();
        } catch (Exception e) {
            target = UNAUTHORIZED;
            return mapping.findForward(target);
        }
        try {
            if (target.equals(SUCCESS)) {
                SpamReasonsForm spamReasonsForm = (SpamReasonsForm) form;
                SpamReasonsService scheduler = SpamReasonsService.getInstance();
                SpamReasonsDTO spamReasonsDTO = scheduler.getSpamReasonsDTO(spamReasonsForm);
                logger.debug(spamReasonsForm.getSpamType());
                logger.debug(spamReasonsForm.getReason());
                RingLogger.getActivityLogger().debug("[add SpamReasonsAction] activistName : " + activistName + " spamReasonsDTO --> " + new Gson().toJson(spamReasonsDTO));
                MyAppError error = scheduler.addSpamReasonsInfo(spamReasonsDTO);
                if (error.getERROR_TYPE() > 0) {
                    if (error.getERROR_TYPE() == MyAppError.OTHERERROR) {
                        logger.error("Other error in addSpamReasons: " + error.getERROR_TYPE());
                        spamReasonsForm.setMessage(error.getErrorMessage());
                        request.getSession(true).setAttribute("error", "2");
                    } else if (error.getERROR_TYPE() == MyAppError.DBERROR) {
                        logger.error("DB error in addSpamReasons: " + error.getERROR_TYPE());
                        spamReasonsForm.setMessage(error.getErrorMessage());
                        request.getSession(true).setAttribute("error", "2");
                    }
                    target = FAILURE;
                } else {
                    SpamReasonsLoader.getInstance().forceReload();
                }
            }
        } catch (Exception e) {
            target = FAILURE;
        }
        return mapping.findForward(target);
    }
}
