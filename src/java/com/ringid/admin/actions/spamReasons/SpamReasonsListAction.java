/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.spamReasons;

import com.ringid.admin.dto.SpamReasonsDTO;
import com.ringid.admin.service.SpamReasonsService;
import com.ringid.admin.repository.SpamReasonsLoader;
import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.utils.Constants;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author mamun
 * @Date Feb 7, 2017
 */
public class SpamReasonsListAction extends BaseAction {

    static Logger loggger = RingLogger.getConfigPortalLogger();

    ArrayList<SpamReasonsDTO> list = new ArrayList<>();

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = FAILURE;
        try {
            SpamReasonsForm spamReasonsForm = (SpamReasonsForm) form;
            SpamReasonsDTO spamReasonsDTO = new SpamReasonsDTO();
            SpamReasonsService scheduler = SpamReasonsService.getInstance();

            if (request.getParameter("ForceReload") != null && "1".equals(request.getParameter("ForceReload"))) {
                SpamReasonsLoader.getInstance().forceReload();
            }
            spamReasonsDTO = scheduler.getSpamReasonsDTO(spamReasonsForm);
            if (spamReasonsForm.getSearchText() != null) {
                spamReasonsDTO.setQueryString(spamReasonsForm.getSearchText().trim().toLowerCase());
            }
            list = scheduler.getSpamReasonsList(spamReasonsForm.getColumn(), spamReasonsForm.getSort(), spamReasonsDTO);
            request.getSession(true).setAttribute(Constants.DATA_ROWS, list);
            managePages(request, spamReasonsForm, list.size());
            request.getSession(true).setAttribute("search", spamReasonsForm.getReason());
        } catch (NullPointerException e) {
            target = FAILURE;
        }
        return mapping.findForward(target);
    }
}
