/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.sports.homeMatch;

import com.ringid.admin.BaseAction;
import com.ringid.admin.service.SportsService;
import com.ringid.admin.actions.sports.matchChannelMapped.ChannelMatchForm;
import com.ringid.admin.actions.sports.matchChannelMapped.MatchDTO;
import com.ringid.admin.utils.Constants;
import com.ringid.admin.utils.MyAppError;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author Rabby
 */
public class HomeMatchManageAction extends BaseAction {

    List<MatchDTO> list = new ArrayList<>();

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = SUCCESS;
        try {
            ChannelMatchForm channelMatchForm = (ChannelMatchForm) form;
            SportsService scheduler = SportsService.getInstance();
            if (request.getParameter("ForceReload") != null) {
                int forceLoad = Integer.valueOf(request.getParameter("ForceReload"));
                if (forceLoad == 1) {
                    scheduler.deleteEndedHomeMatch();
                }
            }

            if (channelMatchForm.getSelectedIDs() != null && channelMatchForm.getSelectedIDs().length > 0) {
                if (channelMatchForm.getSubmitType() != null) {
                    switch (channelMatchForm.getSubmitType()) {
                        case "Add": {
                            int successCount = 0;
                            int failureCount = 0;
                            for (String matchId : channelMatchForm.getSelectedIDs()) {
                                MyAppError error = scheduler.addHomeMatch(matchId);
                                if (error.getERROR_TYPE() == MyAppError.NOERROR) {
                                    successCount++;
                                } else {
                                    failureCount++;
                                }
                            }
                            request.setAttribute("message", "<span style='color: green'>Operation succssfull. (successCount: " + successCount + ", failureCount: " + failureCount + ")</span>");
                            break;
                        }
                        case "Delete": {
                            int successCount = 0;
                            int failureCount = 0;
                            for (String matchId : channelMatchForm.getSelectedIDs()) {
                                MyAppError error = scheduler.deleteHomeMatch(matchId);
                                if (error.getERROR_TYPE() == MyAppError.NOERROR) {
                                    successCount++;
                                } else {
                                    failureCount++;
                                }
                            }
                            request.setAttribute("message", "<span style='color: green'>Operation succssfull. (successCount: " + successCount + ", failureCount: " + failureCount + ")</span>");
                            break;
                        }
                        default:
                            break;
                    }
                }
            }
            String str = "";
            if (channelMatchForm.getSearchText() != null && channelMatchForm.getSearchText().length() > 0) {
                str = channelMatchForm.getSearchText().trim().toLowerCase();
            }

            list = scheduler.getHomeMatchList(str);
            request.getSession(true).setAttribute(Constants.DATA_ROWS, list);
            managePages(request, channelMatchForm, list.size());

        } catch (Exception e) {
            target = FAILURE;
        }
        return mapping.findForward(target);
    }
}
