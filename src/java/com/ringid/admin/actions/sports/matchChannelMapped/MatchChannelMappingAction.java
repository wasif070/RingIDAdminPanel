/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.sports.matchChannelMapped;

import com.ringid.admin.BaseAction;
import com.ringid.admin.dto.adminAuth.LoginDTO;
import com.ringid.admin.service.SportsService;
import com.ringid.admin.utils.Constants;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author mamun
 */
public class MatchChannelMappingAction extends BaseAction {

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = checkAuthentication(mapping, form, request);
        ChannelMatchForm channelMatchForm = (ChannelMatchForm) form;
        List<MatchDTO> matchDTOs = new ArrayList<>();
        try {
            LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        } catch (Exception e) {
            target = UNAUTHORIZED;
            return mapping.findForward(target);
        }
        try {
            if (target.equals(SUCCESS) && channelMatchForm.getMatchId() != null && channelMatchForm.getMatchId().length() > 0) {
                matchDTOs = SportsService.getInstance().getMatchChannelMappedAndUnmappedListByMatchId(channelMatchForm.getMatchId());
            }
        } catch (Exception e) {
            target = FAILURE;
        }
        request.getSession(true).setAttribute(Constants.DATA_ROWS, matchDTOs);
        return mapping.findForward(target);
    }
}
