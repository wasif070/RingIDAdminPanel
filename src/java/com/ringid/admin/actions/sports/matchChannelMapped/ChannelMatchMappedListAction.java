/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.sports.matchChannelMapped;

import com.ringid.admin.dto.MatchChannelMappedDTO;
import com.ringid.admin.BaseAction;
import com.ringid.admin.service.SportsService;
import com.ringid.admin.utils.Constants;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author mamun
 */
public class ChannelMatchMappedListAction extends BaseAction {

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = SUCCESS;
        try {
            ChannelMatchForm channelMatchForm = (ChannelMatchForm) form;
            List<MatchChannelMappedDTO> matchChannelMappedDTOs = new ArrayList<>();
            if (request.getParameter("ForceReload") != null && "1".equals(request.getParameter("ForceReload"))) {
                ChannelMatchLoader.getInstance().forceReload();
            }
            matchChannelMappedDTOs = SportsService.getInstance().getMatchChannelMapped(channelMatchForm.getMatchId(), channelMatchForm.getMatchStatus());
            request.getSession(true).setAttribute(Constants.DATA_ROWS, matchChannelMappedDTOs);
            managePages(request, channelMatchForm, matchChannelMappedDTOs.size());
        } catch (Exception e) {
            target = FAILURE;
        }
        return mapping.findForward(target);
    }
}
