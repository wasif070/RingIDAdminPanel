/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.sports.matchChannelMapped;

import com.google.gson.Gson;
import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.dto.adminAuth.LoginDTO;
import com.ringid.admin.service.SportsService;
import com.ringid.admin.utils.Constants;
import com.ringid.admin.utils.MyAppError;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author mamun
 */
public class ChannelMatchMappingAction extends BaseAction {

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = checkAuthentication(mapping, form, request);
        String activistName;
        ChannelMatchForm channelMatchForm = (ChannelMatchForm) form;
        try {
            LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
            activistName = loginDTO.getUserName();
        } catch (Exception e) {
            target = UNAUTHORIZED;
            return mapping.findForward(target);
        }
        try {
            if (target.equals(SUCCESS)) {
                MyAppError error = new MyAppError();
                switch (channelMatchForm.getAction()) {
                    case Constants.ADD:
                        RingLogger.getActivityLogger().debug("Action : ChannelMatchMappingAction[add] activistName : " + activistName + " matchId --> " + channelMatchForm.getMatchId() + " channelIds --> " + new Gson().toJson(channelMatchForm.getSelectedIDs()));
                        error = SportsService.getInstance().addChannelMatchMapped(channelMatchForm.getMatchId(), channelMatchForm.getSelectedIDs());
                        if (error.getERROR_TYPE() > 0) {
                            if (error.getERROR_TYPE() == MyAppError.OTHERERROR) {
                                request.setAttribute("message", "<span style='color:red'> Operation Failed </span>");
                            }
                            target = FAILURE;
                        } else {
                            ChannelMatchLoader.getInstance().forceReloadMatchChannelMapped();
                        }
                        break;
                    case Constants.DELETE:
                        RingLogger.getActivityLogger().debug("Action : ChannelMatchMappingAction[delete] activistName : " + activistName + " matchIds --> " + new Gson().toJson(channelMatchForm.getSelectedIDs()) + " channelId --> " + channelMatchForm.getChannelId());
                        if (channelMatchForm.getSelectedIDs() != null && channelMatchForm.getSelectedIDs().length > 0) {
                            error = SportsService.getInstance().deleteChannelMatchMapped(channelMatchForm.getSelectedIDs(), channelMatchForm.getChannelId());
                            if (error.getERROR_TYPE() > 0) {
                                if (error.getERROR_TYPE() == MyAppError.OTHERERROR) {
                                    request.setAttribute("message", "<span style='color:red'> Operation Failed </span>");
                                }
                                target = FAILURE;
                            } else {
                                for (String matchId : channelMatchForm.getSelectedIDs()) {
                                    ChannelMatchLoader.getInstance().forceReloadMatchChannelMapped();
                                }
                            }
                        } else {
                            target = FAILURE;
                            request.setAttribute("message", "<span style='color:red'> Operation Failed </span>");
                        }
                        break;
                    default:
                        target = FAILURE;
                        List<MatchDTO> matchDTOs = SportsService.getInstance().getMatchChannelMappedAndUnmappedListByMatchId(channelMatchForm.getMatchId());
                        request.getSession(true).setAttribute(Constants.DATA_ROWS, matchDTOs);
                        break;
                }
            }
        } catch (Exception e) {
            target = FAILURE;
        }
        if (SUCCESS.equals(target)) {
            return new ActionForward(mapping.findForward(target).getPath(), true);
        }
        return mapping.findForward(target);
    }
}
