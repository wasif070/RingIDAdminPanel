/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.sports.matchChannelMapped;

import com.ringid.admin.dto.MatchChannelMappedDTO;
import com.ringid.admin.dao.SportsDAO;
import auth.com.utils.MyAppError;
import com.google.gson.Gson;
import com.ringid.admin.cassandra.CassandraDAO;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.utils.Constants;
import dto.sports.SportsRequestDTO;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import org.ringid.channel.ChannelDTO;
import org.ringid.feedbacks.SportsFeedBack;
import org.ringid.sports.cricket.MatchDetailsDTO;
import org.ringid.sports.cricket.MatchInfoDTO;
import org.ringid.utilities.AppConstants;
import org.ringid.utilities.ListReturnDTO;
import ringid.LoginTaskScheduler;
import ringid.sports.SportsTaskScheduler;

/**
 *
 * @author mamun
 */
public class ChannelMatchLoader {

    private final static long LOADING_INTERVAL = 60 * 60 * 1000;
    private long loadingTime = 0;
    private Map<String, List<String>> matchChannelMapped;
    private Map<String, MatchInfoDTO> matchMap;
    private Map<String, ChannelDTO> channelMap;
    static Logger logger = RingLogger.getConfigPortalLogger();

    private ChannelMatchLoader() {
        matchChannelMapped = new HashMap<>();
        matchMap = new HashMap<>();
        channelMap = new HashMap<>();
    }

    private static class ChannelMatchLoaderHolder {

        private static final ChannelMatchLoader INSTANCE = new ChannelMatchLoader();
    }

    public static ChannelMatchLoader getInstance() {
        return ChannelMatchLoaderHolder.INSTANCE;
    }

    private Map<String, List<String>> loadData() {
        Map<String, List<String>> matchChannelMap = new HashMap<>();
        try {
            matchChannelMap = SportsDAO.getInstance().getMatchChannelMappedInfo();
        } catch (Exception e) {
            logger.debug("ChannelMatchLoader loadData Exception...", e);
        }
        logger.debug("[loadData] matchChannelMapped --> " + matchChannelMap.size());
        return matchChannelMap;
    }

    private Map<String, MatchInfoDTO> getMatchMap() {
        Map<String, MatchInfoDTO> matchInfoMap = new HashMap<>();

        SportsRequestDTO sportsRequestDTO = new SportsRequestDTO();
        sportsRequestDTO.setMatchStatus(0); // 0 for all matches
        try {
            MyAppError myAppError = LoginTaskScheduler.getInstance().login(Constants.USERID, Constants.PASSWORD);
            RingLogger.getConfigPortalLogger().debug("##  login --> " + new Gson().toJson(myAppError));

            logger.debug("ChannelMatchLoader [getMatchMap] sportsRequestDTO --> " + new Gson().toJson(sportsRequestDTO));
            ArrayList<JSONObject> response = SportsTaskScheduler.getInstance().sportsGetMatchList(Constants.USERID, sportsRequestDTO);

            myAppError = LoginTaskScheduler.getInstance().logout(Constants.USERID);
            RingLogger.getConfigPortalLogger().debug("##  myAppError --> " + new Gson().toJson(myAppError));

            for (JSONObject jSONObject : response) {
                SportsFeedBack sportsFeedBack = new Gson().fromJson(jSONObject.toString(), SportsFeedBack.class);
                if (sportsFeedBack.getMatchList() != null && sportsFeedBack.getMatchList().size() > 0) {
                    for (MatchDetailsDTO dto : sportsFeedBack.getMatchList()) {
                        matchInfoMap.put(dto.getMatchInfo().getMatchID(), dto.getMatchInfo());
                    }
                }
            }
        } catch (Exception e) {
            logger.debug("getFeaturedChannels List Exception...", e);
        }
        logger.debug("ChannelMatchLoader [getMatchMap] matchInfoMap.size --> " + matchInfoMap.size());
        return matchInfoMap;
    }

    private Map<String, ChannelDTO> getChannelMap() {
        ListReturnDTO<ChannelDTO> listReturnDTO = new ListReturnDTO<>();
        Map<String, ChannelDTO> channelDTOMap = new HashMap<>();
        try {
            listReturnDTO = CassandraDAO.getInstance().getChannelsByType(AppConstants.ChannelTypes.TV_CHANNEL, 4, 0, 1000);
        } catch (Exception e) {
            logger.debug("getFeaturedChannels List Exception...", e);
        }
        if (listReturnDTO != null && listReturnDTO.getList() != null && listReturnDTO.getList().size() > 0) {
            for (ChannelDTO dto : listReturnDTO.getList()) {
                channelDTOMap.put(dto.getChannelId().toString(), dto);
            }
        }
        logger.debug("ChannelMatchLoader [getChannelMap] channelDTOMap.size --> " + channelDTOMap.size());
        return channelDTOMap;
    }

    private void forceLoadData() {
        matchChannelMapped = loadData();
        matchMap = getMatchMap();
        channelMap = getChannelMap();
    }

    private void checkForReload() {
        long currentTime = System.currentTimeMillis();
        if (currentTime - loadingTime > LOADING_INTERVAL) {
            loadingTime = currentTime;
            forceLoadData();
        }
    }

    public synchronized void forceReloadMatchChannelMapped() {
        matchChannelMapped = loadData();
    }

    public synchronized void forceReload() {
        loadingTime = System.currentTimeMillis();
        forceLoadData();
    }

    public List<MatchChannelMappedDTO> getMatchChannelMapped(String matchId, int status) {
        checkForReload();
        logger.debug("ChannelMatchLoader [getMatchChannelMapped] matchId --> " + matchId + " status --> " + status);
        List<MatchChannelMappedDTO> matchChannelMappedDTOs = new ArrayList<>();
        for (Map.Entry<String, List<String>> entry : matchChannelMapped.entrySet()) {
            MatchInfoDTO matchInfoDTO = getMatchInfoDTOByMatchId(entry.getKey());
            if (matchInfoDTO == null || (status != 0 && status != matchInfoDTO.getStatus())) {
                continue;
            }
            if (matchId != null && matchId.length() > 0 && !matchId.equals(matchInfoDTO.getMatchID())) {
                continue;
            }
            MatchChannelMappedDTO matchChannelMappedDTO = new MatchChannelMappedDTO();
            List<ChannelDTO> channelDTOs = new ArrayList<>();
            for (String channelId : entry.getValue()) {
                ChannelDTO channelDTO = getChannelByChannelId(channelId);
                if (channelDTO != null) {
                    channelDTOs.add(channelDTO);
                }
            }
            if (channelDTOs.size() <= 0) {
                continue;
            }
            matchChannelMappedDTO.setMatchInfoDTO(matchInfoDTO);
            matchChannelMappedDTO.setChannelDTOList(channelDTOs);
            matchChannelMappedDTOs.add(matchChannelMappedDTO);
        }
        logger.debug("ChannelMatchLoader [getMatchChannelMapped] matchChannelMappedDTOs.size --> " + matchChannelMappedDTOs.size());
        return matchChannelMappedDTOs;
    }

    public List<ChannelDTO> getChannelList() {
        checkForReload();
        List<ChannelDTO> channelDTOs = new ArrayList<>();
        for (Map.Entry<String, ChannelDTO> entry : channelMap.entrySet()) {
            channelDTOs.add(entry.getValue());
        }
        return channelDTOs;
    }

    public ChannelDTO getChannelByChannelId(String channelId) {
        checkForReload();
        if (channelMap.containsKey(channelId)) {
            return channelMap.get(channelId);
        }
        return null;
    }

    public List<MatchInfoDTO> getMatchList() {
        checkForReload();
        List<MatchInfoDTO> matchInfoDTOs = new ArrayList<>();
        for (Map.Entry<String, MatchInfoDTO> entry : matchMap.entrySet()) {
            matchInfoDTOs.add(entry.getValue());
        }
        return matchInfoDTOs;
    }

    public MatchInfoDTO getMatchInfoDTOByMatchId(String matchId) {
        checkForReload();
        if (matchMap.containsKey(matchId)) {
            return matchMap.get(matchId);
        }
        return null;
    }

    public List<MatchDTO> getMatchChannelMappedAndUnmappedListByMatchId(String matchId) {
        List<MatchDTO> matchDTOs = new ArrayList<>();
        List<String> matchChannelMappedList = new ArrayList<>();
        if (matchChannelMapped.containsKey(matchId)) {
            matchChannelMappedList = matchChannelMapped.get(matchId);
        }
        List<ChannelDTO> channelDTOs = getChannelList();
        for (ChannelDTO channelDTO : channelDTOs) {
            MatchDTO matchDTO = new MatchDTO();
            matchDTO.setChannelId(channelDTO.getChannelId().toString());
            matchDTO.setChannelTitle(channelDTO.getTitle());
            if (matchChannelMappedList.contains(channelDTO.getChannelId().toString())) {
                matchDTO.setActive(true);
            }

            matchDTOs.add(matchDTO);
        }
        return matchDTOs;
    }
}
