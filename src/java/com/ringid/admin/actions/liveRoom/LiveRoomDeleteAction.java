/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.liveRoom;

import com.ringid.admin.repository.LiveRoomLoader;
import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.dto.adminAuth.LoginDTO;
import com.ringid.admin.service.LiveStreamingService;
import com.ringid.admin.utils.Constants;
import com.ringid.admin.utils.ReasonCode;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author user
 */
public class LiveRoomDeleteAction extends BaseAction {

    Logger logger = RingLogger.getConfigPortalLogger();

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = checkAuthentication(mapping, form, request);
        String activistName;
        try {
            LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
            activistName = loginDTO.getUserName();
        } catch (Exception e) {
            target = UNAUTHORIZED;
            return mapping.findForward(target);
        }
        try {
            if (target.equals(SUCCESS)) {
                long id = 0;
                if (request.getParameter("id") != null) {
                    id = Long.parseLong(request.getParameter("id"));
                }
                RingLogger.getActivityLogger().debug("[LiveRoomDeleteAction] activistName : " + activistName + " Id --> " + id);
                int reasonCode = LiveStreamingService.getInstance().deleteLiveRoom(id);
                if (reasonCode == ReasonCode.NONE) {
                    LiveRoomLoader.getInstance().forceReload();
                    request.getSession(true).setAttribute("deleteLiveRoomMsg", "Live Room ID " + id + " is deleted seccessfully");
                } else {
                    target = FAILURE;
                    request.getSession(true).setAttribute("deleteLiveRoomMsg", "Failed to delete live room (rc: " + reasonCode + ")");
                }
            }
        } catch (Exception e) {
            logger.error(e);
            request.getSession(true).setAttribute("deleteLiveRoomMsg", "Failed to delete live room");
            target = FAILURE;
        }

        return mapping.findForward(target);
    }
}
