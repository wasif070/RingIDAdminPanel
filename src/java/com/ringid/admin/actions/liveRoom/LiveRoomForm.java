/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.liveRoom;

import com.ringid.admin.dto.live.LiveRoomDTO;
import com.ringid.admin.BaseForm;
import com.ringid.admin.utils.Constants;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.collections.Factory;
import org.apache.commons.collections.ListUtils;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.upload.FormFile;

/**
 *
 * @author Rabby
 */
public class LiveRoomForm extends BaseForm implements Factory {

    private long roomId;
    private String roomName;
    private String description;
    private String banner;
    private long liveUserCount;
    private Integer weight;
    private FormFile theFile;
    private String celebrityRoomPage;
    private int roomType;

    public int getRoomType() {
        return roomType;
    }

    public void setRoomType(int roomType) {
        this.roomType = roomType;
    }

    protected List<LiveRoomDTO> editedRoomsList = ListUtils.lazyList(new ArrayList<>(), this);

    public String getCelebrityRoomPage() {
        return celebrityRoomPage;
    }

    public void setCelebrityRoomPage(String celebrityRoomPage) {
        this.celebrityRoomPage = celebrityRoomPage;
    }

    public List<LiveRoomDTO> getEditedRoomsList() {
        return editedRoomsList;
    }

    public void setEditedRoomsList(final List<LiveRoomDTO> editedRoomsList) {
        this.editedRoomsList = ListUtils.lazyList(editedRoomsList, this);
    }

    public Integer getWeight() {
        return weight;
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }

    public FormFile getTheFile() {
        return theFile;
    }

    public void setTheFile(FormFile theFile) {
        this.theFile = theFile;
    }

    public String getBanner() {
        return banner;
    }

    public void setBanner(String banner) {
        this.banner = banner;
    }

    public long getRoomId() {
        return roomId;
    }

    public void setRoomId(long roomId) {
        this.roomId = roomId;
    }

    public String getRoomName() {
        return roomName;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getLiveUserCount() {
        return liveUserCount;
    }

    public void setLiveUserCount(long liveUserCount) {
        this.liveUserCount = liveUserCount;
    }

    @Override
    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        ActionErrors errors = new ActionErrors();
        if (Constants.ADD == getAction() || Constants.UPDATE == getAction()) {
            if (getRoomName() == null || getRoomName().length() < 1) {
                errors.add("roomName", new ActionMessage("errors.roomname.required"));
            }
//            if (getDescription() == null || getDescription().length() < 1) {
//                errors.add("description", new ActionMessage("errors.description.required"));
//            }
            if (Constants.ADD == getAction()) {
                if (getTheFile() == null || getTheFile().getFileSize() < 1) {
                    errors.add("theFile", new ActionMessage("errors.file.required"));
                }
                if (getCelebrityRoomPage() == null || getCelebrityRoomPage().length() < 1) {
                    errors.add("celebrityRoomPage", new ActionMessage("errors.celebritypage.required"));
                }
            }
        }

        ///checking for room name is empty or not in live room edit list page
        List<LiveRoomDTO> list = getEditedRoomsList();
        for (LiveRoomDTO ldto : list) {
            if (ldto.getRoomName().isEmpty()) {
                errors.add("roomNameError", new ActionMessage("Room Name Field can't be empty.", false));
                break;
            }
        }

        return errors;
    }

    @Override
    public Object create() {
        return new LiveRoomDTO();
    }
}
