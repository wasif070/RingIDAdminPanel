/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.liveRoom;

import com.ringid.admin.dto.live.LiveRoomDTO;
import com.ringid.admin.repository.LiveRoomLoader;
import com.google.gson.Gson;
import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.dto.adminAuth.LoginDTO;
import com.ringid.admin.service.LiveStreamingService;
import com.ringid.admin.utils.Constants;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.Logger;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author user
 */
public class LiveRoomListEditSaveAction extends BaseAction {

    Logger logger = RingLogger.getConfigPortalLogger();

    ActionErrors err = new ActionErrors();
    List<LiveRoomDTO> list = new ArrayList<LiveRoomDTO>();
    int editLiveRoomStatus;

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = checkAuthentication(mapping, form, request);
        try {
            LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
            if (target.equals(SUCCESS)) {
                LiveRoomForm liveRoomForm = (LiveRoomForm) form;
                list = liveRoomForm.getEditedRoomsList();
                RingLogger.getActivityLogger().debug("[LiveRoomListEditSaveAction] activistName : " + loginDTO.getUserName() + " listLiveRoomDTO --> " + new Gson().toJson(list));
                editLiveRoomStatus = LiveStreamingService.getInstance().updateLiveRoomList(list);
                if (editLiveRoomStatus == 1) {
                    LiveRoomLoader.getInstance().forceReload();
                    request.getSession(true).setAttribute("editLiveRoomListStatus", "Successfully Updated Live Rooms List Info");
                    target = SUCCESS;
                } else {
                    request.getSession(true).setAttribute("editLiveRoomListStatus", "Failed to Update any of the Live Rooms Info");
                    target = FAILURE;
                }

            }
        } catch (Exception e) {
            request.getSession(true).setAttribute("editLiveRoomListStatus", "Failed to Update any of the Live Rooms Info");
            logger.error(e);
            target = FAILURE;
        }
        return mapping.findForward(target);
    }
}
