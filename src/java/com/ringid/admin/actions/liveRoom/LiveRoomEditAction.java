/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.liveRoom;

import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.service.LiveStreamingService;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.ringid.livestream.StreamRoomDTO;

/**
 *
 * @author Rabby
 */
public class LiveRoomEditAction extends BaseAction {

    Logger logger = RingLogger.getConfigPortalLogger();

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = checkAuthentication(mapping, form, request);
        try {
            long id = 0;
            if (request.getParameter("id") != null) {
                id = Long.parseLong(request.getParameter("id"));
            }
            LiveRoomForm liveRoomForm = (LiveRoomForm) form;
            StreamRoomDTO streamRoomDTO = LiveStreamingService.getInstance().getLiveRoomById(id);
            if (streamRoomDTO != null) {
                liveRoomForm.setRoomId(streamRoomDTO.getRoomId());
                liveRoomForm.setRoomName(streamRoomDTO.getRoomName());
                liveRoomForm.setBanner(streamRoomDTO.getBanner());
                liveRoomForm.setDescription(streamRoomDTO.getDescription());
                liveRoomForm.setWeight(streamRoomDTO.getWeight());
                target = SUCCESS;
            }
        } catch (Exception e) {
            logger.error(e);
            target = FAILURE;
        }

        return mapping.findForward(target);
    }
}
