/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.liveRoom;

import com.ringid.admin.repository.LiveRoomLoader;
import auth.com.utils.MyAppError;
import com.google.gson.Gson;
import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.dto.adminAuth.LoginDTO;
import com.ringid.admin.service.LiveStreamingService;
import com.ringid.admin.utils.Constants;
import com.ringid.admin.utils.ReasonCode;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.Logger;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.upload.FormFile;
import org.ringid.livestream.StreamRoomDTO;
import ringid.LoginTaskScheduler;
import ringid.sessionless.SessionlessTaskseheduler;

/**
 *
 * @author Rabby
 */
public class LiveRoomAction extends BaseAction {

    private static final Logger logger = RingLogger.getConfigPortalLogger();
    ActionErrors err = new ActionErrors();

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = checkAuthentication(mapping, form, request);
        err.clear();
        String activistName;
        try {
            LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
            activistName = loginDTO.getUserName();
        } catch (Exception e) {
            target = UNAUTHORIZED;
            return mapping.findForward(target);
        }
        try {
            LiveRoomForm liveRoomForm = (LiveRoomForm) form;

            boolean isCelebrityRoomPage = false;
            isCelebrityRoomPage = Boolean.valueOf(liveRoomForm.getCelebrityRoomPage());

            FormFile myFile = liveRoomForm.getTheFile();
            String fileName = myFile.getFileName();
            byte[] fileData = myFile.getFileData();
            if (fileName.length() < 1 || fileData.length < 1) {
                throw new Exception("please upload appropriate data!");
            }

            LoginTaskScheduler.getInstance().initSessionLessCommunication(Constants.USERID);
            RingLogger.getConfigPortalLogger().debug("## SessionLessCommunication initiated.");
            long utId = SessionlessTaskseheduler.getInstance().getIdFromUserRingIdGenerator(Constants.USERID);
            RingLogger.getConfigPortalLogger().debug("##  utId --> " + utId);
            LoginTaskScheduler.getInstance().destroySessionLessCommunication(Constants.USERID);
            RingLogger.getConfigPortalLogger().debug("## SessionLessCommunication destroyed.");
            MyAppError myAppError = LoginTaskScheduler.getInstance().login(Constants.USERID, Constants.PASSWORD);
            RingLogger.getConfigPortalLogger().debug("##  login --> " + new Gson().toJson(myAppError));

            String bannerUrl = new UploadBannerImage().upLoadToCloud(Constants.USERID, fileName, fileData);
            RingLogger.getConfigPortalLogger().debug("##  bannerUrl --> " + bannerUrl);
            myAppError = LoginTaskScheduler.getInstance().logout(Constants.USERID);
            RingLogger.getConfigPortalLogger().debug("##  myAppError --> " + new Gson().toJson(myAppError));

            if (bannerUrl != null && bannerUrl.length() > 0) {
                liveRoomForm.setRoomId(utId);
                liveRoomForm.setBanner(bannerUrl);
                StreamRoomDTO streamRoomDTO = LiveStreamingService.getInstance().getStreamRoomDTO(liveRoomForm);
                int reasonCode = LiveStreamingService.getInstance().addLiveRoom(streamRoomDTO, isCelebrityRoomPage);
                RingLogger.getActivityLogger().debug("Action : LiveRoomAction[add] activistName : " + activistName + " streamRoomDTO --> " + new Gson().toJson(streamRoomDTO) + " isCelebrityRoomPage --> " + isCelebrityRoomPage);
                if (reasonCode == ReasonCode.NONE) {
                    LiveRoomLoader.getInstance().forceReload();
                    err.add("errormsg", new ActionMessage("errors.newsupdate.successful"));
                } else {
                    err.add("errormsg", new ActionMessage("Add LiveRoom failed , reasonCode: " + reasonCode, false));
                    target = FAILURE;
                }
                saveMessages(request.getSession(), err);
            } else {
                target = FAILURE;
                err.add("errormsg", new ActionMessage("Banner Image upload failed", false));
                saveMessages(request, err);
            }

        } catch (Exception e) {
            logger.error(e);
            target = FAILURE;
        }
        return mapping.findForward(target);
    }
}
