/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.liveRoom;

import com.ringid.admin.repository.LiveRoomLoader;
import auth.com.utils.MyAppError;
import com.google.gson.Gson;
import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.dto.adminAuth.LoginDTO;
import com.ringid.admin.service.LiveStreamingService;
import com.ringid.admin.utils.Constants;
import com.ringid.admin.utils.ReasonCode;
import com.ringid.admin.utils.Utils;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.Logger;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.upload.FormFile;
import org.ringid.livestream.StreamRoomDTO;
import ringid.LoginTaskScheduler;

/**
 *
 * @author Rabby
 */
public class LiveRoomUpdateAction extends BaseAction {

    private static final Logger logger = RingLogger.getConfigPortalLogger();
    ActionErrors err = new ActionErrors();

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = checkAuthentication(mapping, form, request);
        String activistName;
        try {
            LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
            activistName = loginDTO.getUserName();
        } catch (Exception e) {
            target = UNAUTHORIZED;
            return mapping.findForward(target);
        }
        err.clear();
        try {
            LiveRoomForm liveRoomForm = (LiveRoomForm) form;
            if (liveRoomForm.getTheFile().getFileName() == null || liveRoomForm.getTheFile().getFileName().length() == 0) {
                StreamRoomDTO streamRoomDTO = LiveStreamingService.getInstance().getStreamRoomDTO(liveRoomForm);
                RingLogger.getActivityLogger().debug("[LiveRoomUpdateAction] activistName : " + activistName + " streamRoomDTO --> " + new Gson().toJson(streamRoomDTO));
                int reasonCode = LiveStreamingService.getInstance().updateLiveRoomInfo(streamRoomDTO);
                if (reasonCode == ReasonCode.NONE) {
                    int result = -1;
                    switch (liveRoomForm.getRoomType()) {
                        case Constants.LIVE_ROOM_TOP:
                            result = LiveStreamingService.getInstance().updateTopLiveRoomPage(liveRoomForm.getRoomId());
                            if (result == ReasonCode.NONE) {
                                request.setAttribute("updateLiveRoomType", "Live Top Room Update Succssful");
                            } else {
                                request.setAttribute("message", "<span style='color: red'>" + Utils.getMessageForReasonCode(result) + "</span>");
                                target = FAILURE;
                            }
                            break;
                        case Constants.LIVE_ROOM_DONATION:
                            result = LiveStreamingService.getInstance().updateDonationRoomPageId(liveRoomForm.getRoomId());
                            if (result == ReasonCode.NONE) {
                                request.setAttribute("updateLiveRoomType", "Live Donation Room Update Succssful");
                            } else {
                                request.setAttribute("message", "<span style='color: red'>" + Utils.getMessageForReasonCode(result) + "</span>");
                                target = FAILURE;
                            }
                            break;
                        default:
                            break;
                    }
                    LiveRoomLoader.getInstance().forceReload();
                    err.add("errormsg", new ActionMessage("errors.newsupdate.successful"));
                } else {
                    err.add("errormsg", new ActionMessage("Update LiveRoom failed , reasonCode: " + reasonCode, false));
                    target = FAILURE;
                }
                saveErrors(request.getSession(), err);
                return mapping.findForward(target);
            }
            FormFile myFile = liveRoomForm.getTheFile();
            String fileName = myFile.getFileName();
            byte[] fileData = myFile.getFileData();
            if (fileName.length() < 1 || fileData.length < 1) {
                throw new Exception("please upload appropriate data!");
            }
            LoginTaskScheduler.getInstance().forceRemove(Constants.USERID);
            MyAppError login = LoginTaskScheduler.getInstance().login(Constants.USERID, Constants.PASSWORD);
            RingLogger.getConfigPortalLogger().debug("## login : " + new Gson().toJson(login));
            String bannerUrl = new UploadBannerImage().upLoadToCloud(Constants.USERID, fileName, fileData);
            RingLogger.getConfigPortalLogger().debug("## bannerUrl : " + bannerUrl);
            LoginTaskScheduler.getInstance().logout(Constants.USERID);
            if (bannerUrl != null && bannerUrl.length() > 0) {
                liveRoomForm.setBanner(bannerUrl);
                StreamRoomDTO streamRoomDTO = LiveStreamingService.getInstance().getStreamRoomDTO(liveRoomForm);
                RingLogger.getActivityLogger().debug("[LiveRoomUpdateAction] activistName : " + activistName + " streamRoomDTO --> " + new Gson().toJson(streamRoomDTO));
                int reasonCode = LiveStreamingService.getInstance().updateLiveRoomInfo(streamRoomDTO);
                if (reasonCode == ReasonCode.NONE) {
                    LiveRoomLoader.getInstance().forceReload();
                    err.add("errormsg", new ActionMessage("errors.newsupdate.successful"));
                } else {
                    err.add("errormsg", new ActionMessage("Update LiveRoom failed , reasonCode: " + reasonCode, false));
                    target = FAILURE;
                }
                saveErrors(request.getSession(), err);
            } else {
                target = FAILURE;
                err.add("errormsg", new ActionMessage("Banner Image Upload failed", false));
                saveErrors(request.getSession(), err);
            }
        } catch (Exception e) {
            logger.error(e);
            target = FAILURE;
        }
        return mapping.findForward(target);
    }
}
