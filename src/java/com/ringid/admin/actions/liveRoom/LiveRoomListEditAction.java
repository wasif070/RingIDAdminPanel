/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.liveRoom;

import com.ringid.admin.dto.live.LiveRoomDTO;
import com.google.gson.Gson;
import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.service.LiveStreamingService;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author user
 */
public class LiveRoomListEditAction extends BaseAction {

    Logger logger = RingLogger.getConfigPortalLogger();

    List<LiveRoomDTO> list;

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = checkAuthentication(mapping, form, request);
        try {
            if (target.equals(SUCCESS)) {
                LiveRoomForm liveRoomForm = (LiveRoomForm) form;

                logger.debug("[LiveRoomListEditAction] roomIds --> " + new Gson().toJson(liveRoomForm.getSelectedIDs()));
                if (liveRoomForm.getSelectedIDs() != null) {
                    list = LiveStreamingService.getInstance().getCheckRoomList(liveRoomForm.getSelectedIDs());
                    liveRoomForm.setEditedRoomsList(list);
                    managePages(request, liveRoomForm, list.size());
                } else {
                    target = FAILURE;
                }
            }
        } catch (Exception e) {
            logger.error(e);
            target = FAILURE;
        }
        return mapping.findForward(target);
    }
}
