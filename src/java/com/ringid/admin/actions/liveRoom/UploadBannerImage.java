/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.liveRoom;

import auth.com.dto.InfoDTo;
import com.ringid.admin.cassandra.CassandraDAO;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.utils.Constants;
import com.ringid.admin.utils.HTTPRequestProcessor;
import java.util.ArrayList;
import java.util.HashMap;
import org.apache.logging.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import ringid.LoginTaskScheduler;

/**
 *
 * @author ipvision
 */
public class UploadBannerImage {

    public String upLoadToCloud(String USER_ID, String filename, byte[] fileData) throws JSONException {
        Boolean returnVal = true;
        String imageUrl = null;
        String serverUrl = Constants.PROFILE_IMAGE_UPLOAD_URL;
        HashMap<String, Object> headerValues = getheaderValues();
        HashMap<String, Object> bodyValues = getBodyValues(USER_ID);
        ArrayList<String> names = new ArrayList<>();
        names.add(filename);
        ArrayList<byte[]> dataValues = new ArrayList<>();
        dataValues.add(fileData);
        RingLogger.getConfigPortalLogger().debug("--- Sending Upload Req --- [serverUrl] : " + serverUrl);
        String response = HTTPRequestProcessor.uploader_method("POST", headerValues, bodyValues, null, names, dataValues, serverUrl, "");
        JSONObject jSONObject = new JSONObject(response);
        response = jSONObject.getString("mainstr");
        RingLogger.getConfigPortalLogger().debug("--- Response received ---");
        if (response != null && response.length() > 0) {
            JSONObject obj = new JSONObject(response);
            if (obj.getBoolean("sucs")) {
                RingLogger.getConfigPortalLogger().debug("Successfull to Upload : pathToFileBannerImage -> [" + serverUrl + "] : response --> " + response);
                imageUrl = obj.getString("iurl");
            } else {
                returnVal = false;
                RingLogger.getConfigPortalLogger().error("Failed to Upload : pathToFileBannerImage -> [" + serverUrl + "] : response --> " + response);
                imageUrl = null;
            }
        }
        return imageUrl;
    }

    private HashMap<String, Object> getheaderValues() {
        HashMap<String, Object> values = new HashMap<>();
        values.put("access-control-allow-origin", "*");
        return values;
    }

    private HashMap<String, Object> getBodyValues(String userId) {
        InfoDTo infoDTo = LoginTaskScheduler.getInstance().getInfo(userId);
        HashMap<String, Object> bodyValues = new HashMap<>();
        bodyValues.put("cimX", 0);
        bodyValues.put("cimY", 0);
        bodyValues.put("ih", 168);
        bodyValues.put("iw", 300);
        bodyValues.put("sId", infoDTo.getSessionId());
        bodyValues.put("uId", infoDTo.getUserId());
        bodyValues.put("authServer", infoDTo.getAuthIP());
        bodyValues.put("comPort", infoDTo.getAuthPORT());
        bodyValues.put("x-app-version", 149);
        return bodyValues;
    }

}
