/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.liveRoom;

import com.ringid.admin.dto.live.LiveRoomDTO;
import com.ringid.admin.repository.LiveRoomLoader;
import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.service.LiveStreamingService;
import com.ringid.admin.utils.Constants;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.ringid.livestream.StreamRoomDTO;

/**
 *
 * @author Rabby
 */
public class LiveRoomListAction extends BaseAction {

    private static final Logger logger = RingLogger.getConfigPortalLogger();
    List<StreamRoomDTO> list;

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = SUCCESS;
        try {
            LiveRoomForm liveRoomForm = (LiveRoomForm) form;
            LiveRoomDTO dto = new LiveRoomDTO();
            if (request.getParameter("ForceReload") != null && "1".equals(request.getParameter("ForceReload"))) {
                LiveRoomLoader.getInstance().forceReload();
            }
            if (liveRoomForm.getSelectedIDs() != null) {
                List<LiveRoomDTO> editlist = LiveStreamingService.getInstance().getCheckRoomList(liveRoomForm.getSelectedIDs());
                liveRoomForm.setEditedRoomsList(editlist);
                managePages(request, liveRoomForm, editlist.size());
                return mapping.findForward("editRoomList");
            }
            if (liveRoomForm.getColumn() <= 0) {
                liveRoomForm.setColumn(Constants.COLUMN_ONE);
                liveRoomForm.setSort(Constants.DESC_SORT);
            }
            if (liveRoomForm.getSearchText() != null && liveRoomForm.getSearchText().length() > 0) {
                dto.setSearchText(liveRoomForm.getSearchText().trim().toLowerCase());
            }
            dto.setColumn(liveRoomForm.getColumn());
            dto.setSortType(liveRoomForm.getSort());
            list = LiveStreamingService.getInstance().getLiveRoomList(dto);
            request.getSession(true).setAttribute(Constants.DATA_ROWS, list);
            managePages(request, liveRoomForm, list.size());
        } catch (Exception e) {
            logger.error(e);
            target = FAILURE;
        }
        return mapping.findForward(target);
    }
}
