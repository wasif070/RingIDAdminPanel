/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.liveToChannel;

import com.google.gson.Gson;
import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.dto.adminAuth.LoginDTO;
import com.ringid.admin.actions.ringId.celebritySchedule.LiveToChannelDTO;
import com.ringid.admin.service.LiveStreamingService;
import com.ringid.admin.utils.Constants;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author mamun
 */
public class LiveToChannelAction extends BaseAction {

    static Logger loggger = RingLogger.getConfigPortalLogger();

    List<LiveToChannelDTO> list = new ArrayList<>();

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = FAILURE;
        int reasonCode = -1;
        String activistName;
        try {
            LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
            activistName = loginDTO.getUserName();
        } catch (Exception e) {
            target = UNAUTHORIZED;
            return mapping.findForward(target);
        }
        try {
            LiveToChannelForm liveToChannelForm = (LiveToChannelForm) form;
            LiveToChannelDTO dto = new LiveToChannelDTO();
            switch (liveToChannelForm.getAction()) {
                case Constants.ADD: {
                    dto = LiveStreamingService.getInstance().getLivetochannelDTO(liveToChannelForm);
                    RingLogger.getActivityLogger().debug("Action : LiveToChannelAction[add/update] activistName : " + activistName + " liveToChannelDTO --> " + new Gson().toJson(dto));
                    reasonCode = LiveStreamingService.getInstance().addOrUpdateLiveToChannelInfo(dto);
                    if (reasonCode == 0) {
                        target = SUCCESS;
                        notifyAuthServers(Constants.RELOAD_LIVE_TO_CHANNEL, "");
                    }
                    break;
                }
                case Constants.EDIT: {
                    dto = LiveStreamingService.getInstance().getLiveToChannelInfo(liveToChannelForm.getPageId());
                    if (dto != null) {
                        liveToChannelForm.setPageId(dto.getPageId());
                        liveToChannelForm.setChannelId(dto.getChannelId());
                        liveToChannelForm.setLiveTime(dto.getLiveTimeStr());
                        liveToChannelForm.setLiveNow(dto.getLiveNow());
                        liveToChannelForm.setTitle(dto.getTitle());
                        liveToChannelForm.setDescription(dto.getDescription());
                        target = SUCCESS;
                    }
                    break;
                }
                case Constants.RESET: {
                    RingLogger.getActivityLogger().debug("Action : LiveToChannelAction[reset live time] activistName : " + activistName + " pageId --> " + liveToChannelForm.getPageId());
                    reasonCode = LiveStreamingService.getInstance().resetLiveTime(liveToChannelForm.getPageId());
                    if (reasonCode == 0) {
                        target = SUCCESS;
                        notifyAuthServers(Constants.RELOAD_LIVE_TO_CHANNEL, "");
                    }
                    break;
                }
                case Constants.DELETE: {
                    RingLogger.getActivityLogger().debug("Action : LiveToChannelAction[delete] activistName : " + activistName + " pageId --> " + liveToChannelForm.getPageId());
                    reasonCode = LiveStreamingService.getInstance().deleteLiveToChannelInfo(liveToChannelForm.getPageId());
                    if (reasonCode == 0) {
                        target = SUCCESS;
                        notifyAuthServers(Constants.RELOAD_LIVE_TO_CHANNEL, "");
                    }
                    break;
                }
                default: {
                    target = SUCCESS;
                    break;
                }
            }
            if (SUCCESS.equals(target) && liveToChannelForm.getAction() != Constants.EDIT) {
                list = LiveStreamingService.getInstance().getLiveToChannelListInfo();
                request.getSession(true).setAttribute(Constants.DATA_ROWS, list);
                managePages(request, liveToChannelForm, list.size());
            }
        } catch (NullPointerException e) {
            target = FAILURE;
        }
        return mapping.findForward(target);
    }
}
