/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.pastLiveFeed;

import com.ringid.admin.BaseAction;
import com.ringid.admin.actions.ringId.searchUser.SearchUserDTO;
import com.ringid.admin.actions.ringId.searchUser.SearchUserForm;
import com.ringid.admin.cassandra.CassandraDAO;
import com.ringid.admin.service.NewsFeedService;
import com.ringid.admin.utils.Constants;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.ringid.utilities.AppConstants;

/**
 *
 * @author mamun
 */
public class PastLiveFeedListAction extends BaseAction {

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = FAILURE;
        try {
            SearchUserForm searchUserForm = (SearchUserForm) form;
            if (searchUserForm.getViewType() <= 0) {
                searchUserForm.setViewType(AppConstants.FeedPullAction.PAST_LIVE_FEED);
            }
            long wallOwnerId = 0;
            if (searchUserForm.getSearchText() != null && searchUserForm.getSearchText().length() > 0) {
                wallOwnerId = CassandraDAO.getInstance().getUserTableId(Long.parseLong(searchUserForm.getSearchText().trim().replace(" ", "")));
                if (wallOwnerId <= 0) {
                    wallOwnerId = Long.parseLong(searchUserForm.getSearchText().trim().replace(" ", ""));
                }
            }
            int action = searchUserForm.getViewType();
            if (wallOwnerId <= 0) {
                wallOwnerId = Constants.LOGGED_IN_USER_ID;
            } else if (action == AppConstants.FeedPullAction.TOP_ROOM_LIVE_FEED) {
                action = AppConstants.FeedPullAction.TOP_ROOM_LIVE_FEED_BY_USER;
            } else if (action == AppConstants.FeedPullAction.PAST_LIVE_FEED) {
                action = AppConstants.FeedPullAction.CELEBRITY_PAST_LIVE_FEED;
            }
            SearchUserDTO searchUserDTO = new SearchUserDTO();
            searchUserDTO.setCelebrityPastLiveFeed(NewsFeedService.getInstance().getNewsFeedList(Constants.LOGGED_IN_USER_ID, wallOwnerId, null, action, 10));
            searchUserForm.setSearchUserDTO(searchUserDTO);
        } catch (NullPointerException e) {
            target = FAILURE;
        }
        return mapping.findForward(target);
    }
}
