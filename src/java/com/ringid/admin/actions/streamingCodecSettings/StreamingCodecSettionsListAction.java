/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.streamingCodecSettings;

import com.ringid.admin.dto.StreamingCodecSettingsDTO;
import com.ringid.admin.BaseAction;
import com.ringid.admin.service.LiveStreamingService;
import com.ringid.admin.utils.Constants;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author Rabby
 */
public class StreamingCodecSettionsListAction extends BaseAction {

    ArrayList<StreamingCodecSettingsDTO> list = new ArrayList<>();

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = FAILURE;
        try {
            StreamingCodecSettingsForm settingsForm = (StreamingCodecSettingsForm) form;
            list = LiveStreamingService.getInstance().getStreamingCodecSettingsList();
            request.getSession(true).setAttribute(Constants.DATA_ROWS, list);
            managePages(request, settingsForm, list.size());
        } catch (Exception e) {
            target = FAILURE;
        }
        return mapping.findForward(target);
    }
}
