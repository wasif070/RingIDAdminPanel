/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.streamingCodecSettings;

import com.ringid.admin.dto.StreamingCodecSettingsDTO;
import com.ringid.admin.BaseAction;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.dto.adminAuth.LoginDTO;
import com.ringid.admin.service.LiveStreamingService;
import com.ringid.admin.utils.Constants;
import com.ringid.admin.utils.MyAppError;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author mamun
 */
public class StreamingCodecSettingsDeleteAction extends BaseAction {

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = SUCCESS;
        String activistName;
        try {
            LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
            activistName = loginDTO.getUserName();
        } catch (Exception e) {
            target = UNAUTHORIZED;
            return mapping.findForward(target);
        }
        try {
            StreamingCodecSettingsForm settingsForm = (StreamingCodecSettingsForm) form;
            StreamingCodecSettingsDTO dto = new StreamingCodecSettingsDTO();
            dto.setStreamType(settingsForm.getStreamType());
            dto.setAudioCodec(settingsForm.getAudioCodec());
            RingLogger.getActivityLogger().debug("[StreamingCodecSettingsDeleteAction] activistName : " + activistName + " streamType --> " + settingsForm.getStreamType() + " audioCodec --> " + settingsForm.getAudioCodec());
            MyAppError error = LiveStreamingService.getInstance().deleteStreamingCodecSettings(dto);
            if (error.getERROR_TYPE() > 0) {
                request.setAttribute("message", "<span style='color:red'>Operation Failed.</span>");
            } else {
                notifyAuthServers(Constants.RELOAD_STREAMING_CODEC_SETTINGS, null);
                request.setAttribute("message", "<span style='color:green'>Operation Successfull.</span>");
            }
        } catch (Exception e) {
            target = FAILURE;
        }
        return mapping.findForward(target);
    }
}
