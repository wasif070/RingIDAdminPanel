/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.liveInfo.liveSummary;

import com.ringid.admin.BaseAction;
import com.ringid.admin.dto.live.LiveInfoDTO;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.service.LiveStreamingService;
import com.ringid.admin.utils.MyAppError;
import java.text.SimpleDateFormat;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author mamun
 */
public class LiveSummaryAction extends BaseAction {

    private static final Logger logger = RingLogger.getConfigPortalLogger();
    List<LiveInfoDTO> list;

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = SUCCESS;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        try {
            LiveSummaryForm liveRoomForm = (LiveSummaryForm) form;

            long startDate = sdf.parse(liveRoomForm.getStartDate()).getTime();
            long endDate = sdf.parse(liveRoomForm.getEndDate()).getTime();

            MyAppError myAppError = LiveStreamingService.getInstance().addLiveSummary(startDate, endDate);
            if (myAppError.getERROR_TYPE() == MyAppError.NOERROR) {
                request.setAttribute("message", "<span style='color:green;'>Live Summary Insert Successful</span>");
            } else {
                request.setAttribute("message", "<span style='color:red;'>Live Summary Insert Failed</span>");
            }
        } catch (Exception e) {
            logger.error(e);
            target = FAILURE;
        }
        return mapping.findForward(target);
    }

    private String getRingId(String ringId) {
        long mod = (long) Math.pow(10, 8);
        long ringIdLongValue = Long.valueOf(ringId);
        return "21" + (ringIdLongValue % mod);
    }
}
