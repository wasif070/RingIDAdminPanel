/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.liveInfo.liveInfoDetails;

import com.ringid.admin.dto.live.LiveInfoDetailsDTO;
import com.ringid.admin.BaseAction;
import com.ringid.admin.cassandra.CassandraDAO;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.service.LiveStreamingService;
import com.ringid.admin.utils.Constants;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.ringid.pages.ServiceDTO;
import org.ringid.users.UserBasicInfoDTO;

/**
 *
 * @author mamun
 */
public class OldLiveInfoDetailsAction extends BaseAction {

    private static final Logger logger = RingLogger.getConfigPortalLogger();
    List<LiveInfoDetailsDTO> list;

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = SUCCESS;
        try {
            LiveInfoDetailsForm liveInfoDetailsForm = (LiveInfoDetailsForm) form;

            LiveInfoDetailsDTO liveInfoDetailsDTO = new LiveInfoDetailsDTO();

            if (list != null && list.size() > 0 && liveInfoDetailsForm.getAction() != Constants.SEARCH && liveInfoDetailsForm.getColumn() > 0) {
                managePages(request, liveInfoDetailsForm, list.size());
                int fromIndex = (liveInfoDetailsForm.getPageNo() - 1) * liveInfoDetailsForm.getRecordPerPage();
                int toIndex = (liveInfoDetailsForm.getPageNo()) * liveInfoDetailsForm.getRecordPerPage();
                switch (liveInfoDetailsForm.getColumn()) {
                    case Constants.COLUMN_ONE:
                        if (liveInfoDetailsForm.getSort() == Constants.ASC_SORT) {
                            Collections.sort(list, new LiveInfoDetailsDTO.CompStartDateASC());
                        } else {
                            Collections.sort(list, new LiveInfoDetailsDTO.CompStartDateDSC());
                        }
                        break;
                    case Constants.COLUMN_TWO:
                        if (liveInfoDetailsForm.getSort() == Constants.ASC_SORT) {
                            Collections.sort(list, new LiveInfoDetailsDTO.CompEndDateASC());
                        } else {
                            Collections.sort(list, new LiveInfoDetailsDTO.CompEndDateDSC());
                        }
                        break;
                    case Constants.COLUMN_THREE:
                        if (liveInfoDetailsForm.getSort() == Constants.ASC_SORT) {
                            Collections.sort(list, new LiveInfoDetailsDTO.CompDurationASC());
                        } else {
                            Collections.sort(list, new LiveInfoDetailsDTO.CompDurationDSC());
                        }
                        break;
                    case Constants.COLUMN_FOUR:
                        if (liveInfoDetailsForm.getSort() == Constants.ASC_SORT) {
                            Collections.sort(list, new LiveInfoDetailsDTO.CompStreamDateASC());
                        } else {
                            Collections.sort(list, new LiveInfoDetailsDTO.CompStreamDateDSC());
                        }
                        break;
                    default:
                        if (liveInfoDetailsForm.getSort() == Constants.ASC_SORT) {
                            Collections.sort(list, new LiveInfoDetailsDTO.CompDurationASC());
                        } else {
                            Collections.sort(list, new LiveInfoDetailsDTO.CompDurationDSC());
                        }
                        liveInfoDetailsForm.setColumn(Constants.COLUMN_THREE);
                        break;
                }
                request.getSession(true).setAttribute(Constants.DATA_ROWS, list.subList(fromIndex, toIndex > list.size() ? list.size() : toIndex));
                return mapping.findForward(target);
            } else if (list != null && list.size() > 0 && (liveInfoDetailsForm.getAction() == Constants.PAGE_NAVIGATION || liveInfoDetailsForm.getAction() == Constants.RECORD_PER_PAGE_CHANGE)) {
                managePages(request, liveInfoDetailsForm, list.size());
                int fromIndex = (liveInfoDetailsForm.getPageNo() - 1) * liveInfoDetailsForm.getRecordPerPage();
                int toIndex = (liveInfoDetailsForm.getPageNo()) * liveInfoDetailsForm.getRecordPerPage();
                request.getSession(true).setAttribute(Constants.DATA_ROWS, list.subList(fromIndex, toIndex > list.size() ? list.size() : toIndex));
                return mapping.findForward(target);
            }

            if (liveInfoDetailsForm.getStreamDate() != null && liveInfoDetailsForm.getStreamDate().length() > 0) {
                liveInfoDetailsForm.setStartDate(liveInfoDetailsForm.getStreamDate());
                liveInfoDetailsForm.setEndDate(liveInfoDetailsForm.getStreamDate());
            }
            String ringIdStr = "";
            if (liveInfoDetailsForm.getRingId() != null && liveInfoDetailsForm.getRingId().length() > 0) {
                ringIdStr = getRingId(liveInfoDetailsForm.getRingId());

                UserBasicInfoDTO userBasicInfo = CassandraDAO.getInstance().getUserBasicInfoByRingID(Long.valueOf(ringIdStr), true);
                if (userBasicInfo.getUserType() == 1) {
                    HashMap<Integer, List<ServiceDTO>> pageMap = CassandraDAO.getInstance().getPageMap(userBasicInfo.getUserId());
                    List<ServiceDTO> serviceList = new ArrayList<>();
                    for (Map.Entry<Integer, List<ServiceDTO>> entry : pageMap.entrySet()) {
                        serviceList.addAll(entry.getValue());
                    }
                    for (ServiceDTO serviceDTO : serviceList) {
                        UserBasicInfoDTO pageBasicInfo = CassandraDAO.getInstance().getUserBasicInfo(serviceDTO.getPageId());
                        if (pageBasicInfo.getRingId() > 0) {
                            ringIdStr += (!ringIdStr.isEmpty() ? "," : "") + pageBasicInfo.getRingId();
                        }
                    }
                } else if (userBasicInfo.getUserType() > 1) {
                    long pageOwnerId = CassandraDAO.getInstance().getPageOwnerId(userBasicInfo.getUserId());
                    HashMap<Integer, List<ServiceDTO>> pageMap = CassandraDAO.getInstance().getPageMap(pageOwnerId);
                    List<ServiceDTO> serviceList = new ArrayList<>();
                    for (Map.Entry<Integer, List<ServiceDTO>> entry : pageMap.entrySet()) {
                        serviceList.addAll(entry.getValue());
                    }
                    for (ServiceDTO serviceDTO : serviceList) {
                        UserBasicInfoDTO pageBasicInfo = CassandraDAO.getInstance().getUserBasicInfo(serviceDTO.getPageId());
                        if (pageBasicInfo.getRingId() > 0) {
                            ringIdStr += (!ringIdStr.isEmpty() ? "," : "") + pageBasicInfo.getRingId();
                        }
                    }

                    UserBasicInfoDTO ownerBasicInfo = CassandraDAO.getInstance().getUserBasicInfo(pageOwnerId);
                    ringIdStr += (!ringIdStr.isEmpty() ? "," : "") + ownerBasicInfo.getRingId();
                }
            }
            liveInfoDetailsDTO.setStartDate(liveInfoDetailsForm.getStartDate());
            liveInfoDetailsDTO.setEndDate(liveInfoDetailsForm.getEndDate());
            liveInfoDetailsDTO.setRingId(ringIdStr);

            list = LiveStreamingService.getInstance().getOldLiveInfoDetailsList(liveInfoDetailsDTO);

            managePages(request, liveInfoDetailsForm, list.size());
            int fromIndex = (liveInfoDetailsForm.getPageNo() - 1) * liveInfoDetailsForm.getRecordPerPage();
            int toIndex = (liveInfoDetailsForm.getPageNo()) * liveInfoDetailsForm.getRecordPerPage();
            Collections.sort(list, new LiveInfoDetailsDTO.CompDurationASC());
            liveInfoDetailsForm.setColumn(Constants.COLUMN_THREE);
            liveInfoDetailsForm.setSort(Constants.ASC_SORT);
            request.getSession(true).setAttribute(Constants.DATA_ROWS, list.subList(fromIndex, toIndex > list.size() ? list.size() : toIndex));
        } catch (Exception e) {
            logger.error(e);
            target = FAILURE;
        }
        return mapping.findForward(target);
    }

    private String getRingId(String ringId) {
        long mod = (long) Math.pow(10, 8);
        long ringIdLongValue = Long.valueOf(ringId);
        return "21" + (ringIdLongValue % mod);
    }
}
