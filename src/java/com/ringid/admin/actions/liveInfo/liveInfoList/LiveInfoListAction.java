/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.liveInfo.liveInfoList;

import com.ringid.admin.dto.live.LiveInfoDTO;
import com.ringid.admin.BaseAction;
import com.ringid.admin.cassandra.CassandraDAO;
import com.ringid.admin.dto.live.LiveSummaryDTO;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.service.LiveStreamingService;
import com.ringid.admin.utils.Constants;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.ringid.pages.ServiceDTO;
import org.ringid.users.UserBasicInfoDTO;

/**
 *
 * @author ipvision
 */
public class LiveInfoListAction extends BaseAction {

    private static final Logger logger = RingLogger.getConfigPortalLogger();
    List<LiveSummaryDTO> list;

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = SUCCESS;
        try {
            LiveInfoForm liveRoomForm = (LiveInfoForm) form;
            LiveInfoDTO liveInfoDTO = new LiveInfoDTO();
            if (list != null && list.size() > 0 && liveRoomForm.getAction() != Constants.SEARCH && liveRoomForm.getColumn() > 0) {
                managePages(request, liveRoomForm, list.size());
                int fromIndex = (liveRoomForm.getPageNo() - 1) * liveRoomForm.getRecordPerPage();
                int toIndex = (liveRoomForm.getPageNo()) * liveRoomForm.getRecordPerPage();
                switch (liveRoomForm.getColumn()) {
                    case Constants.COLUMN_ONE:
                        if (liveRoomForm.getSort() == Constants.ASC_SORT) {
                            Collections.sort(list, new LiveSummaryDTO.CompStreamDateASC());
                        } else {
                            Collections.sort(list, new LiveSummaryDTO.CompStreamDateDSC());
                        }
                        break;
                    case Constants.COLUMN_TWO:
                        if (liveRoomForm.getSort() == Constants.ASC_SORT) {
                            Collections.sort(list, new LiveSummaryDTO.CompRingIdASC());
                        } else {
                            Collections.sort(list, new LiveSummaryDTO.CompRingIdDSC());
                        }

                        break;
                    case Constants.COLUMN_THREE:
                        if (liveRoomForm.getSort() == Constants.ASC_SORT) {
                            Collections.sort(list, new LiveSummaryDTO.CompNameASC());
                        } else {
                            Collections.sort(list, new LiveSummaryDTO.CompNameDSC());
                        }
                        break;
                    case Constants.COLUMN_FOUR:
                        if (liveRoomForm.getSort() == Constants.ASC_SORT) {
                            Collections.sort(list, new LiveSummaryDTO.CompTotalLiveDurationASC());
                        } else {
                            Collections.sort(list, new LiveSummaryDTO.CompTotalLiveDurationDSC());
                        }
                        break;
                    case Constants.COLUMN_FIVE:
                        if (liveRoomForm.getSort() == Constants.ASC_SORT) {
                            Collections.sort(list, new LiveSummaryDTO.CompNoOfAppearanceASC());
                        } else {
                            Collections.sort(list, new LiveSummaryDTO.CompNoOfAppearanceDSC());
                        }
                        break;
                    default:
                        if (liveRoomForm.getSort() == Constants.ASC_SORT) {
                            Collections.sort(list, new LiveSummaryDTO.CompStreamDateASC());
                        } else {
                            Collections.sort(list, new LiveSummaryDTO.CompStreamDateDSC());
                        }
                        liveRoomForm.setColumn(Constants.COLUMN_ONE);
                        break;
                }
                request.getSession(true).setAttribute(Constants.DATA_ROWS, list.subList(fromIndex, toIndex > list.size() ? list.size() : toIndex));
                return mapping.findForward(target);
            } else if (list != null && list.size() > 0 && (liveRoomForm.getAction() == Constants.PAGE_NAVIGATION || liveRoomForm.getAction() == Constants.RECORD_PER_PAGE_CHANGE)) {
                managePages(request, liveRoomForm, list.size());
                int fromIndex = (liveRoomForm.getPageNo() - 1) * liveRoomForm.getRecordPerPage();
                int toIndex = (liveRoomForm.getPageNo()) * liveRoomForm.getRecordPerPage();
                request.getSession(true).setAttribute(Constants.DATA_ROWS, list.subList(fromIndex, toIndex > list.size() ? list.size() : toIndex));
                return mapping.findForward(target);
            }

            String ringIdStr = "";
            if (liveRoomForm.getRingId() != null && liveRoomForm.getRingId().length() > 0) {
                ringIdStr = getRingId(liveRoomForm.getRingId());

                UserBasicInfoDTO userBasicInfo = CassandraDAO.getInstance().getUserBasicInfoByRingID(Long.valueOf(ringIdStr), true);
                if (userBasicInfo.getUserType() == 1) {
                    HashMap<Integer, List<ServiceDTO>> pageMap = CassandraDAO.getInstance().getPageMap(userBasicInfo.getUserId());
                    List<ServiceDTO> serviceList = new ArrayList<>();
                    for (Map.Entry<Integer, List<ServiceDTO>> entry : pageMap.entrySet()) {
                        serviceList.addAll(entry.getValue());
                    }
                    for (ServiceDTO serviceDTO : serviceList) {
                        UserBasicInfoDTO pageBasicInfo = CassandraDAO.getInstance().getUserBasicInfo(serviceDTO.getPageId());
                        if (pageBasicInfo.getRingId() > 0) {
                            ringIdStr += (!ringIdStr.isEmpty() ? "," : "") + pageBasicInfo.getRingId();
                        }
                    }
                } else if (userBasicInfo.getUserType() > 1) {
                    long pageOwnerId = CassandraDAO.getInstance().getPageOwnerId(userBasicInfo.getUserId());
                    HashMap<Integer, List<ServiceDTO>> pageMap = CassandraDAO.getInstance().getPageMap(pageOwnerId);
                    List<ServiceDTO> serviceList = new ArrayList<>();
                    for (Map.Entry<Integer, List<ServiceDTO>> entry : pageMap.entrySet()) {
                        serviceList.addAll(entry.getValue());
                    }
                    for (ServiceDTO serviceDTO : serviceList) {
                        UserBasicInfoDTO pageBasicInfo = CassandraDAO.getInstance().getUserBasicInfo(serviceDTO.getPageId());
                        if (pageBasicInfo.getRingId() > 0) {
                            ringIdStr += (!ringIdStr.isEmpty() ? "," : "") + pageBasicInfo.getRingId();
                        }
                    }

                    UserBasicInfoDTO ownerBasicInfo = CassandraDAO.getInstance().getUserBasicInfo(pageOwnerId);
                    ringIdStr += (!ringIdStr.isEmpty() ? "," : "") + ownerBasicInfo.getRingId();
                }
            }
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

            liveInfoDTO.setRingId(ringIdStr);
            liveInfoDTO.setStartDate(liveRoomForm.getStartDate());
            liveInfoDTO.setEndDate(liveRoomForm.getEndDate());

            liveInfoDTO.setStartTime(sdf.parse(liveRoomForm.getStartDate()).getTime());
            liveInfoDTO.setEndTime(sdf.parse(liveRoomForm.getEndDate()).getTime());

            list = LiveStreamingService.getInstance().getLiveInfoListByDate(liveInfoDTO);
            managePages(request, liveRoomForm, list.size());
            int fromIndex = (liveRoomForm.getPageNo() - 1) * liveRoomForm.getRecordPerPage();
            int toIndex = (liveRoomForm.getPageNo()) * liveRoomForm.getRecordPerPage();
            Collections.sort(list, new LiveSummaryDTO.CompStreamDateASC());
            liveRoomForm.setColumn(Constants.COLUMN_ONE);
            liveRoomForm.setSort(Constants.ASC_SORT);
            request.getSession(true).setAttribute(Constants.DATA_ROWS, list.subList(fromIndex, toIndex > list.size() ? list.size() : toIndex));
        } catch (Exception e) {
            logger.error(e);
            target = FAILURE;
        }
        return mapping.findForward(target);
    }

    private String getRingId(String ringId) {
        long mod = (long) Math.pow(10, 8);
        long ringIdLongValue = Long.valueOf(ringId);
        return "21" + (ringIdLongValue % mod);
    }
}
