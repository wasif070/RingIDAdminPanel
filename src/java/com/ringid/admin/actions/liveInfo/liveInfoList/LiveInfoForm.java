/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.actions.liveInfo.liveInfoList;

import com.ringid.admin.BaseForm;
import com.ringid.admin.dto.adminAuth.LoginDTO;
import com.ringid.admin.utils.Constants;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

/**
 *
 * @author ipvision
 */
public class LiveInfoForm extends BaseForm {

    private String streamId;
    private long userId;
    private String ringId;
    private long ownId;
    private long startTime;
    private long endTime;
    private long duration;
    private int roomId;
    private int device;
    private String streamDate;
    private int noOfAppearance;
    private String startDate;
    private String endDate;
    private SimpleDateFormat formatter;
    private String country;
    private int countryId;
    private boolean oldLiveInfoSearch;
    private String totalDurationStr;

    public String getTotalDurationStr() {
        return totalDurationStr;
    }

    public void setTotalDurationStr(String totalDurationStr) {
        this.totalDurationStr = totalDurationStr;
    }

    public LiveInfoForm() {
        formatter = new SimpleDateFormat("yyyy-MM-dd");
        oldLiveInfoSearch = false;
    }

    public boolean isOldLiveInfoSearch() {
        return oldLiveInfoSearch;
    }

    public void setOldLiveInfoSearch(boolean oldLiveInfoSearch) {
        this.oldLiveInfoSearch = oldLiveInfoSearch;
    }

    public int getCountryId() {
        return countryId;
    }

    public void setCountryId(int countryId) {
        this.countryId = countryId;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getStreamId() {
        return streamId;
    }

    public void setStreamId(String streamId) {
        this.streamId = streamId;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getRingId() {
        return ringId;
    }

    public void setRingId(String ringId) {
        this.ringId = ringId;
    }

    public long getOwnId() {
        return ownId;
    }

    public void setOwnId(long ownId) {
        this.ownId = ownId;
    }

    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public long getEndTime() {
        return endTime;
    }

    public void setEndTime(long endTime) {
        this.endTime = endTime;
    }

    public long getDuration() {
        return duration;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }

    public int getRoomId() {
        return roomId;
    }

    public void setRoomId(int roomId) {
        this.roomId = roomId;
    }

    public int getDevice() {
        return device;
    }

    public void setDevice(int device) {
        this.device = device;
    }

    public String getStreamDate() {
        return streamDate;
    }

    public void setStreamDate(String streamDate) {
        this.streamDate = streamDate;
    }

    public int getNoOfAppearance() {
        return noOfAppearance;
    }

    public void setNoOfAppearance(int noOfAppearance) {
        this.noOfAppearance = noOfAppearance;
    }

    @Override
    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        ActionErrors errors = new ActionErrors();
        LoginDTO loginDTO = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        if (loginDTO == null) {
            errors.add("auth", new ActionMessage("UnAuthorized Access"));
        } else {                                                                    ///ensuring the difference between start and end date is 30 days
            String errormsg = "";
            try {
                if (isOldLiveInfoSearch() && (getRingId() == null || getRingId().length() <= 0)) {
                    errors.add("ringId", new ActionMessage("RingId Required", false));
                }
                if ((getEndDate() == null && getStartDate() == null) || (getEndDate().isEmpty() && getStartDate().isEmpty())) {
                    setStartDate(formatter.format(new Date()));
                    setEndDate(formatter.format(new Date()));
                } else if (!getEndDate().isEmpty() && getStartDate().isEmpty()) {
                    Calendar cal = Calendar.getInstance();
                    cal.setTime(formatter.parse(getEndDate()));
                    cal.add(cal.DATE, -30);
                    setStartDate(formatter.format(cal.getTime()));
                    errormsg += "Adjusted StartDate: " + getStartDate() + ". ";
                } else if (getEndDate().isEmpty() && !getStartDate().isEmpty()) {
                    Calendar cal = Calendar.getInstance();

                    cal.setTime(formatter.parse(getStartDate()));
                    cal.add(cal.DATE, 30);
                    setEndDate(formatter.format(cal.getTime()));
                    errormsg += "Adjusted EndDate: " + getEndDate() + ". ";
                }

                Date start_d = formatter.parse(getStartDate());
                Date end_d = formatter.parse(getEndDate());
                int diffInDays = (int) ((end_d.getTime() - start_d.getTime()) / (1000 * 60 * 60 * 24));
                if (diffInDays < 0) {
                    errors.add("dateerror", new ActionMessage("End Date must be greater or equal Start Date", false));
                    errormsg += "End Date must be greater or equal Start Date. ";
                } else if (diffInDays > 30 && !isOldLiveInfoSearch()) {
                    errors.add("dateerror", new ActionMessage("Interval between End Date and Start Date can't be more than 30 days", false));
                    errormsg += "Interval between End Date and Start Date can't be more than 30 days. ";
                }

            } catch (ParseException ex) {
                errors.add("dateerror", new ActionMessage("Date Parse Error", false));
                errormsg += "Date Parse Error ";
            }

            request.getSession(true).setAttribute("dateerror", errormsg);
        }
        return errors;
    }
}
