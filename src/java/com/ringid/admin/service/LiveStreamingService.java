/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.service;

import com.ringid.admin.cassandra.CassandraDAO;
import com.ringid.admin.dto.ChannelVerificationDTO;
import com.ringid.admin.dao.LiveStreamingDAO;
import com.ringid.admin.dto.live.LiveInfoDetailsDTO;
import com.ringid.admin.dto.live.LiveInfoDTO;
import com.ringid.admin.dto.live.LiveSummaryDTO;
import com.ringid.admin.dto.live.LiveRoomDTO;
import com.ringid.admin.actions.liveRoom.LiveRoomForm;
import com.ringid.admin.repository.LiveRoomLoader;
import com.ringid.admin.repository.LiveSummaryRepository;
import com.ringid.admin.actions.liveToChannel.LiveToChannelForm;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.actions.ringId.celebritySchedule.CelebrityScheduleLoader;
import com.ringid.admin.actions.ringId.celebritySchedule.LiveToChannelDTO;
import com.ringid.admin.dto.StreamingCodecSettingsDTO;
import com.ringid.admin.utils.Constants;
import com.ringid.admin.utils.MyAppError;
import com.ringid.admin.utils.ReasonCode;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;
import org.ringid.livestream.StreamRoomDTO;
import org.ringid.livestream.StreamingDTO;
import org.ringid.livestream.StreamingTariffDTO;
import org.ringid.pages.PageDTO;
import org.ringid.users.LiveUserDetailsDTO;
import org.ringid.users.UserDTO;
import org.ringid.utilities.AppConstants;

/**
 *
 * @author Rabby
 */
public class LiveStreamingService {

    private static LiveStreamingService instance = null;
    private final SimpleDateFormat dateFormat;

    private LiveStreamingService() {
        dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    }

    public static LiveStreamingService getInstance() {
        if (instance == null) {
            createInstance();
        }
        return instance;
    }

    private synchronized static void createInstance() {
        if (instance == null) {
            instance = new LiveStreamingService();
        }
    }

    public static MyAppError updateManualRecordedLive(UUID streamId, StreamingDTO stream) {
        return LiveStreamingDAO.getInstance().updateManualRecordedLive(streamId, stream);
    }

    public ArrayList<StreamingCodecSettingsDTO> getStreamingCodecSettingsList() {
        return LiveStreamingDAO.getInstance().getStreamingCodecSettingsList();
    }

    public StreamingCodecSettingsDTO getStreamingCodecSettingsLByType(int type) {
        return LiveStreamingDAO.getInstance().getStreamingCodecSettingsByType(type);
    }

    public MyAppError updateStreamingCodecSettings(StreamingCodecSettingsDTO sdto) {
        return LiveStreamingDAO.getInstance().updateStreamingCodecSettings(sdto);
    }

    public MyAppError deleteStreamingCodecSettings(StreamingCodecSettingsDTO dto) {
        return LiveStreamingDAO.getInstance().deleteStreamingCodecSettings(dto);
    }

    public MyAppError addStreamingCodecSettings(StreamingCodecSettingsDTO dto) {
        return LiveStreamingDAO.getInstance().addStreamingCodecSettings(dto);
    }

    public int updateDonationRoomPageId(long pageId) {
        int reasonCode = -1;
        try {
            reasonCode = CassandraDAO.getInstance().updateDonationRoomPageId(pageId);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [updateDonationRoomPageId] --> " + e);
        }
        return reasonCode;
    }

    public int updateTopLiveRoomPage(long pageId) {
        int reasonCode = -1;
        try {
            reasonCode = CassandraDAO.getInstance().updateTopLiveRoomPage(pageId);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [updateTopLiveRoomPage] --> " + e);
        }
        return reasonCode;
    }

    public List<StreamRoomDTO> getLiveRoomList(LiveRoomDTO dto) {
        return LiveRoomLoader.getInstance().getLiveRoomList(dto);
    }

    public StreamRoomDTO getLiveRoomById(long id) {
        return LiveRoomLoader.getInstance().getLiveRoomById(id);
    }

    public int addLiveRoom(StreamRoomDTO sdto, boolean isCelebrityRoomPage) {
        int reasonCode = -1;
        try {
            reasonCode = CassandraDAO.getInstance().addLiveRoom(sdto, isCelebrityRoomPage);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("[addLiveRoom] Exception --> " + e);
        }
        return reasonCode;
    }

    public int updateLiveRoomInfo(StreamRoomDTO sdto) {
        int reasonCode = -1;
        try {
            reasonCode = CassandraDAO.getInstance().updateLiveRoomInfo(sdto);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("[updateLiveRoomInfo] Exception --> " + e);
        }
        return reasonCode;
    }

    public int updateLiveRoomList(List<LiveRoomDTO> list) {

        int flag = 0; ///flag=1 successful, flag=0 failed

        String successMsg = "Successfully updated Room Ids: ", errorMsg = " Can't update Room Ids: ";

        for (LiveRoomDTO ldto : list) {
            int reasonCode = -1;

            try {
                reasonCode = CassandraDAO.getInstance().updateLiveRoomInfo(convertStreamRoomDTO(ldto));
                if (reasonCode == ReasonCode.NONE) {
                    successMsg += "[" + ldto.getRoomId() + "] ";
                    if (flag == 0) {
                        flag = 1;
                    }
                } else {
                    errorMsg += "[" + ldto.getRoomId() + "] ";
                }
            } catch (Exception e) {
                errorMsg += "[" + ldto.getRoomId() + "] ";
            }

        }
        RingLogger.getConfigPortalLogger().debug(successMsg + errorMsg);

        return flag;
    }

    public int deleteLiveRoom(long roomId) {
        int reasonCode = -1;

        try {
            reasonCode = CassandraDAO.getInstance().deleteLiveRoom(roomId);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("[deleteLiveRoomInfo] Exception --> " + e);
        }

        return reasonCode;

    }

    public StreamRoomDTO getStreamRoomDTO(LiveRoomForm liveRoomForm) {
        StreamRoomDTO streamRoomDTO = new StreamRoomDTO();
        streamRoomDTO.setRoomId(liveRoomForm.getRoomId());
        streamRoomDTO.setRoomName(liveRoomForm.getRoomName());
        streamRoomDTO.setDescription(liveRoomForm.getDescription());
        streamRoomDTO.setBanner(liveRoomForm.getBanner());
        streamRoomDTO.setWeight(liveRoomForm.getWeight());

        return streamRoomDTO;
    }

    public List<LiveRoomDTO> getCheckRoomList(String[] roomIds) {
        List<LiveRoomDTO> list = new ArrayList<>();
        List<StreamRoomDTO> streamRoomDTOs = LiveRoomLoader.getInstance().getCheckRoomList(roomIds);
        for (StreamRoomDTO dto : streamRoomDTOs) {
            list.add(convertLiveRoomDTO(dto));
        }
        return list;
    }

    private StreamRoomDTO convertStreamRoomDTO(LiveRoomDTO ldto) {
        StreamRoomDTO sdto = new StreamRoomDTO();
        sdto.setBanner(ldto.getBanner());
        sdto.setDescription(ldto.getDescription());
        sdto.setLiveUserCount(ldto.getLiveUserCount());
        sdto.setRoomId(ldto.getRoomId());
        sdto.setRoomName(ldto.getRoomName());
        sdto.setWeight(ldto.getWeight());

        return sdto;
    }

    private LiveRoomDTO convertLiveRoomDTO(StreamRoomDTO sdto) {
        LiveRoomDTO ldto = new LiveRoomDTO();
        ldto.setRoomId(sdto.getRoomId());
        ldto.setBanner(sdto.getBanner());
        ldto.setDescription(sdto.getDescription());
        ldto.setLiveUserCount(sdto.getLiveUserCount());
        ldto.setRoomName(sdto.getRoomName());
        ldto.setWeight(sdto.getWeight());

        return ldto;
    }

    public List<LiveSummaryDTO> getOldLiveInfoList(LiveInfoDTO liveInfoDTO) {
        long totalLiveDuration = 0L;
        int totalNoOfAppearance = 0;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        List<LiveSummaryDTO> liveSummaryDTOs = new ArrayList<>();
        List<LiveInfoDTO> liveInfoDTOList = new ArrayList<>();

        liveInfoDTOList = LiveStreamingDAO.getInstance().getLiveInfoList(liveInfoDTO, "streaminghistory");

        for (LiveInfoDTO liveDTO : liveInfoDTOList) {
            LiveSummaryDTO dto = new LiveSummaryDTO();
            dto.setRingId(liveDTO.getRingId());
            dto.setTotalLiveDuration(liveDTO.getTotalLiveDuration());
            totalLiveDuration += liveDTO.getTotalLiveDuration();
            dto.setTotalLiveDurationStr(liveDTO.getTotalLiveDurationStr());
            dto.setNoOfAppearance(liveDTO.getNoOfAppearance());
            totalNoOfAppearance += liveDTO.getNoOfAppearance();
            dto.setDevice(liveDTO.getDevice());
            dto.setName(liveDTO.getFirstName());
            dto.setCountry(liveDTO.getCountry());
            dto.setCountryId(liveDTO.getCountryId());
            dto.setStreamDateStr(liveDTO.getStreamDate());

            liveSummaryDTOs.add(dto);
        }
        liveInfoDTOList = new ArrayList<>();
        liveInfoDTOList = LiveStreamingDAO.getInstance().getLiveInfoList(liveInfoDTO, "streaminghistory_bk");

        for (LiveInfoDTO liveDTO : liveInfoDTOList) {
            LiveSummaryDTO dto = new LiveSummaryDTO();
            dto.setRingId(liveDTO.getRingId());
            dto.setTotalLiveDuration(liveDTO.getTotalLiveDuration());
            totalLiveDuration += liveDTO.getTotalLiveDuration();
            dto.setTotalLiveDurationStr(liveDTO.getTotalLiveDurationStr());
            dto.setNoOfAppearance(liveDTO.getNoOfAppearance());
            totalNoOfAppearance += liveDTO.getNoOfAppearance();
            dto.setDevice(liveDTO.getDevice());
            dto.setName(liveDTO.getFirstName());
            dto.setCountry(liveDTO.getCountry());
            dto.setCountryId(liveDTO.getCountryId());
            dto.setStreamDateStr(liveDTO.getStreamDate());

            liveSummaryDTOs.add(dto);
        }
        liveInfoDTO.setTotalLiveDuration(totalLiveDuration);
        liveInfoDTO.setNoOfAppearance(totalNoOfAppearance);
        return liveSummaryDTOs;
    }

    public MyAppError addLiveSummary(long startDate, long endDate) {
        return LiveStreamingDAO.getInstance().addLiveSummary(startDate, endDate);
    }

    public List<LiveSummaryDTO> getLiveInfoListByRingId(LiveInfoDTO liveInfoDTO) {
        List<LiveSummaryDTO> list = new ArrayList<>();
        List<LiveSummaryDTO> liveSummaryDTOs = LiveSummaryRepository.getInstance().getLiveSummaryList(liveInfoDTO);
        Map<String, LiveSummaryDTO> listMap = new HashMap<>();
        liveSummaryDTOs.stream().forEach((dto) -> {
            if (listMap.containsKey(dto.getRingId())) {
                LiveSummaryDTO sdto = getLiveSummaryDTO(listMap.get(dto.getRingId()));
                sdto.setNoOfAppearance(sdto.getNoOfAppearance() + dto.getNoOfAppearance());
                sdto.setTotalLiveDuration(sdto.getTotalLiveDuration() + dto.getTotalLiveDuration());
                listMap.put(dto.getRingId(), sdto);
            } else {
                listMap.put(dto.getRingId(), dto);
            }
        });
        listMap.entrySet().stream().forEach((entry) -> {
            list.add(entry.getValue());
        });
        return list;
    }

    public List<LiveSummaryDTO> getLiveInfoListByDate(LiveInfoDTO liveInfoDTO) {
        return LiveSummaryRepository.getInstance().getLiveSummaryList(liveInfoDTO);
    }

    public List<LiveInfoDetailsDTO> getOldLiveInfoDetailsList(LiveInfoDetailsDTO liveInfoDetailsDTO) {
        List<LiveInfoDetailsDTO> liveInfoDetailsDTOs = LiveStreamingDAO.getInstance().getLiveInfoDetailsList(liveInfoDetailsDTO, "");
        liveInfoDetailsDTOs.addAll(LiveStreamingDAO.getInstance().getLiveInfoDetailsList(liveInfoDetailsDTO, "streaminghistory_bk"));
        return liveInfoDetailsDTOs;
    }

    public List<LiveInfoDetailsDTO> getLiveInfoDetailsList(LiveInfoDetailsDTO liveInfoDetailsDTO) {
        return LiveStreamingDAO.getInstance().getLiveInfoDetailsList(liveInfoDetailsDTO, null);
    }

    public List<LiveInfoDTO> getLiveInfoList(LiveInfoDTO liveInfoDTO, String tableName) {
        return LiveStreamingDAO.getInstance().getLiveInfoList(liveInfoDTO, tableName);
    }

    public List<LiveInfoDTO> getLiveInfoListByRingId(LiveInfoDTO liveInfoDTO, String tableName) {
        return LiveStreamingDAO.getInstance().getLiveInfoListByRingId(liveInfoDTO, tableName);
    }

    public List<StreamingDTO> getAllCelebrityLiveSchedule() {
        return CelebrityScheduleLoader.getInstance().getCelebrityLiveScheduleList();
    }

    public List<StreamingDTO> getCelebrityLiveSchedule(long pageId, String pageName) {
        StreamingDTO sdto = new StreamingDTO();
        sdto.setUserTableId(pageId);
        sdto.setName(pageName);
        return CelebrityScheduleLoader.getInstance().getCelebrityLiveScheduleList(sdto);
    }

    public int deleteCelebrityLiveSchedule(long pageId, int userType, long liveTime) {
        int reasonCode = -1;
        try {
            reasonCode = CassandraDAO.getInstance().deleteCelebrityLiveSchedule(pageId, userType, liveTime, true);
        } catch (Exception e) {
        }
        return reasonCode;
    }

    public int editCelebrityLiveSchedule(long userId, int userType, long curLiveTime, long newLiveTime, UUID newLiveTimeUUID, long donationPageId) {
        int reasonCode = -1;
        try {
            reasonCode = CassandraDAO.getInstance().editCelebrityLiveSchedule(userId, userType, curLiveTime, newLiveTime, newLiveTimeUUID, donationPageId);
        } catch (Exception e) {
        }
        return reasonCode;
    }

    public int addCelebrityLiveSchedule(long userId, int userType, long donationPageId, long liveTime, UUID liveTimeUUID, int scheduleSerial) {
        int reasonCode = -1;
        try {
            reasonCode = CassandraDAO.getInstance().addCelebrityLiveSchedule(userId, userType, donationPageId, liveTime, liveTimeUUID, scheduleSerial);
        } catch (Exception ex) {
            //reasonCod
        }
        return reasonCode;
    }

    public boolean updateLiveVerifiedSettings(boolean isVerified) {
        boolean result = false;
        try {
            result = CassandraDAO.getInstance().updateLiveVerifiedSettings(isVerified);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Throwable [updateLiveVerifiedSettings] --> ", e);
        }
        return result;
    }

    public List<UserDTO> getFeaturedStreamOwnersList() {
        List<UserDTO> userDTOs = new ArrayList<>();
        int dataSize = 0, limit = 1000;
        long pivot = 0;
        try {
            List<LiveUserDetailsDTO> liveUserDetailsDTOs = new ArrayList<>();
            do {
                List<LiveUserDetailsDTO> temp = CassandraDAO.getInstance().getLiveFeaturedUsers(pivot, limit);
                dataSize = temp.size();
                if (dataSize > 0) {
                    liveUserDetailsDTOs.addAll(temp);
                    pivot = temp.get(dataSize - 1).getUserID();
                }
            } while (dataSize == limit);
            for (LiveUserDetailsDTO dto : liveUserDetailsDTOs) {
                UserDTO userDTO = new UserDTO();
                userDTO.setRingID(CassandraDAO.getInstance().getRingID(dto.getUserID()));
                userDTO.setProfileImage(dto.getProfileImageUrl());
                userDTO.setFullName(dto.getUserName());
                userDTO.setUserType(dto.getUserType());
                userDTO.setWeight(dto.getWeight());
                userDTOs.add(userDTO);
            }
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception [getFeaturedStreamOwnersList] --> ", e);
        }
        return userDTOs;
    }

    public List<UserDTO> getFeaturedStreamOwnersList(int pageType, String searchText, int column, int sort) {
        RingLogger.getConfigPortalLogger().error("[getFeaturedStreamOwnersList] searchText --> " + searchText);
        List<UserDTO> liveFeaturedUserDTOs = new ArrayList<>();
        int dataSize = 0, limit = 1000;
        long pivot = 0;
        try {
            List<LiveUserDetailsDTO> liveUserDetailsDTOs = new ArrayList<>();
            do {
                List<LiveUserDetailsDTO> temp = CassandraDAO.getInstance().getLiveFeaturedUsers(pivot, limit);
                dataSize = temp.size();
                if (dataSize > 0) {
                    liveUserDetailsDTOs.addAll(temp);
                    pivot = temp.get(dataSize - 1).getUserID();
                }
            } while (dataSize == limit);
            for (LiveUserDetailsDTO dto : liveUserDetailsDTOs) {
                UserDTO userDTO;
                long ringIdCass = 0L;
                if (pageType != 0 && pageType != dto.getUserType()) {
                    continue;
                }
                userDTO = CassandraDAO.getInstance().getUserDetails(dto.getUserID(), false);
                ringIdCass = userDTO.getRingID();
                if (searchText != null && searchText.length() > 0) {
                    if (String.valueOf(ringIdCass).toLowerCase().contains(searchText.trim().toLowerCase())
                            || dto.getUserName().toLowerCase().contains(searchText.trim().toLowerCase())
                            || (userDTO.getCountry() != null && userDTO.getCountry().toLowerCase().contains(searchText.trim().toLowerCase()))) {
                        userDTO.setProfileImage(dto.getProfileImageUrl());
                        userDTO.setFullName(dto.getUserName());
                        userDTO.setUserType(dto.getUserType());
                        userDTO.setWeight(dto.getWeight());
                        liveFeaturedUserDTOs.add(userDTO);
                    }
                } else {
                    userDTO.setProfileImage(dto.getProfileImageUrl());
                    userDTO.setFullName(dto.getUserName());
                    userDTO.setUserType(dto.getUserType());
                    userDTO.setWeight(dto.getWeight());
                    liveFeaturedUserDTOs.add(userDTO);
                }
            }
            switch (column) {
                case Constants.COLUMN_ONE:
                    Collections.sort(liveFeaturedUserDTOs, (sort == Constants.ASC_SORT ? new Comparator<UserDTO>() {
                        @Override
                        public int compare(UserDTO o1, UserDTO o2) {
                            return Long.compare(o1.getRingID(), o2.getRingID());
                        }
                    }
                            : new Comparator<UserDTO>() {
                        @Override
                        public int compare(UserDTO o1, UserDTO o2) {
                            return Long.compare(o2.getRingID(), o1.getRingID());
                        }
                    }));
                    break;
                case Constants.COLUMN_TWO:
                    Collections.sort(liveFeaturedUserDTOs, (sort == Constants.ASC_SORT ? new Comparator<UserDTO>() {
                        @Override
                        public int compare(UserDTO o1, UserDTO o2) {
                            return o1.getFullName().compareToIgnoreCase(o2.getFullName());
                        }
                    }
                            : new Comparator<UserDTO>() {
                        @Override
                        public int compare(UserDTO o1, UserDTO o2) {
                            return o2.getFullName().compareToIgnoreCase(o1.getFullName());
                        }
                    }));
                    break;
                case Constants.COLUMN_THREE:
                    Collections.sort(liveFeaturedUserDTOs, (sort == Constants.ASC_SORT ? new Comparator<UserDTO>() {
                        @Override
                        public int compare(UserDTO o1, UserDTO o2) {
                            return o1.getWeight() - o2.getWeight();
                        }
                    }
                            : new Comparator<UserDTO>() {
                        @Override
                        public int compare(UserDTO o1, UserDTO o2) {
                            return o2.getWeight() - o1.getWeight();
                        }
                    }));
                    break;
                default:
                    Collections.sort(liveFeaturedUserDTOs, (sort == Constants.ASC_SORT ? new Comparator<UserDTO>() {
                        @Override
                        public int compare(UserDTO o1, UserDTO o2) {
                            return o1.getWeight() - o2.getWeight();
                        }
                    }
                            : new Comparator<UserDTO>() {
                        @Override
                        public int compare(UserDTO o1, UserDTO o2) {
                            return o2.getWeight() - o1.getWeight();
                        }
                    }));
                    break;
            }
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception [getFeaturedStreamOwnersList] --> ", e);
        }
        return liveFeaturedUserDTOs;
    }

    public int updateLiveVerificationState(String[] ids, boolean state) {
        int responseCode = -1;
        try {
            for (String id : ids) {
                responseCode = CassandraDAO.getInstance().updateLiveVerificationState(Long.parseLong(id), state);
            }
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception [updateLiveVerificationState] --> ", e);
        }
        return responseCode;
    }

    public List<StreamingDTO> getVerifiedLiveUser(String searchKey) {
        RingLogger.getConfigPortalLogger().error("[getVerifiedLiveUser] searchKey --> " + searchKey);
        List<StreamingDTO> userDTOs = new ArrayList<>();
        try {
            List<StreamingDTO> verifiedLiveUser = CassandraDAO.getInstance().getVerifiedStreams();
            verifiedLiveUser
                    .stream()
                    .filter(dto -> {
                        if (searchKey != null && searchKey.length() > 0) {
                            if (!(dto.getuId().contains(searchKey.trim().toLowerCase())
                                    || dto.getName().toLowerCase().contains(searchKey.trim().toLowerCase()))) {
                                return false;
                            }
                        }
                        return true;
                    })
                    .forEach(dto -> {
                        userDTOs.add(dto);
                    });
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [getVerifiedLiveUser] e -> " + e);
        }
        return userDTOs;
    }

    public List<StreamingDTO> getUnverifiedLiveUser(String searchKey) {
        RingLogger.getConfigPortalLogger().error("[getUnverifiedLiveUser] searchKey --> " + searchKey);
        List<StreamingDTO> userDTOs = new ArrayList<>();
        try {
            List<StreamingDTO> unverifiedLiveUser = CassandraDAO.getInstance().getUnverifiedStreams();
            unverifiedLiveUser.stream().filter(dto -> {
                if (searchKey != null && searchKey.length() > 0) {
                    if (!(dto.getuId().contains(searchKey.trim().toLowerCase())
                            || dto.getName().toLowerCase().contains(searchKey.trim().toLowerCase()))) {
                        return false;
                    }
                }
                return true;
            }).forEach(dto -> {
                userDTOs.add(dto);
            });
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [getUnverifiedLiveUser] e -> " + e);
        }
        return userDTOs;
    }

    public List<StreamingTariffDTO> getAllTariff() {
        List<StreamingTariffDTO> tariffList = new ArrayList<>();
        try {
            tariffList = CassandraDAO.getInstance().getAllTariff();
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [getAllTariff] --> " + e);
        }
        return tariffList;
    }

    public int addTariff(int tariff) {
        int responseCode = -1;
        try {
            StreamingTariffDTO dto = new StreamingTariffDTO();
            dto.setTariff(tariff);
            responseCode = CassandraDAO.getInstance().addTariff(dto);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception [updateRestrictionTime] --> ", e);
        }
        return responseCode;
    }

    public int deleteLiveCallTariff(int tariff) {
        int reasonCode = -1;
        try {
            StreamingTariffDTO dto = new StreamingTariffDTO();
            dto.setTariff(tariff);
            reasonCode = CassandraDAO.getInstance().deleteLiveCallTariff(dto);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [deleteLiveCallTariff] --> " + e);
        }
        return reasonCode;
    }

    public int updateLiveScheduleSerial(long userId, int userType, long liveTime, int scheduleSerial) {
        int reasonCode = -1;
        try {
            reasonCode = CassandraDAO.getInstance().updateLiveScheduleSerial(userId, userType, liveTime, scheduleSerial);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [updateLiveScheduleSerial] --> " + e);
        }
        return reasonCode;
    }

    public int addLiveSubscriptionFee(int fee) {
        int reasonCode = -1;
        StreamingTariffDTO dto = new StreamingTariffDTO();
        dto.setTariff(fee);
        try {
            reasonCode = CassandraDAO.getInstance().addLiveSubscriptionFee(dto);
        } catch (Exception e) {
        }
        return reasonCode;
    }

    public int deleteLiveSubscriptionFee(int fee) {
        int reasonCode = -1;
        StreamingTariffDTO dto = new StreamingTariffDTO();
        dto.setTariff(fee);
        try {
            reasonCode = CassandraDAO.getInstance().deleteLiveSubscriptionFee(dto);
        } catch (Exception e) {
        }
        return reasonCode;
    }

    public List<StreamingTariffDTO> getAllLiveSubscriptionFees() {
        List<StreamingTariffDTO> streamingTariffDTOs = new ArrayList<>();
        try {
            streamingTariffDTOs = CassandraDAO.getInstance().getAllSubscriptionFees();
        } catch (Exception e) {
        }
        return streamingTariffDTOs;
    }

    public int removePastLiveFeed(UUID liveTimeUUID, UUID feedId, int action, int removeFeatured) {
        int result = -1;
        try {
            if (action > 0) {
                switch (action) {
                    case AppConstants.FeedPullAction.FEATURED_PAST_LIVE_FEED:
                        result = CassandraDAO.getInstance().removeFeaturedPastLiveFeed(liveTimeUUID, feedId);
                        break;
                    case AppConstants.FeedPullAction.VERIFIED_TOP_ROOM_PAST_LIVE_FEED:
                        result = CassandraDAO.getInstance().removeVerifiedTopPastLiveFeed(liveTimeUUID, feedId);
                        break;
                    case AppConstants.FeedPullAction.VERIFIED_CELEBRITY_ROOM_PAST_LIVE_FEED:
                        result = CassandraDAO.getInstance().removeVerifiedCelebrityPastLiveFeed(liveTimeUUID, feedId);
                        break;
                }
            }
        } catch (Exception e) {
        }
        return result;
    }

    public int addPastLiveFeed(UUID liveTimeUUID, UUID feedId, int action, int makeFeatured) {
        int result = -1;
        try {
            if (action > 0) {
                switch (action) {
                    case AppConstants.FeedPullAction.PAST_LIVE_FEED:
                        result = (makeFeatured == 1) ? CassandraDAO.getInstance().addFeaturedPastLiveFeed(liveTimeUUID, feedId) : CassandraDAO.getInstance().addVerifiedCelebrityPastLiveFeed(liveTimeUUID, feedId);
                        break;
                    case AppConstants.FeedPullAction.CELEBRITY_PAST_LIVE_FEED:
                        result = CassandraDAO.getInstance().addFeaturedPastLiveFeed(liveTimeUUID, feedId);
                        break;
                    case AppConstants.FeedPullAction.TOP_ROOM_LIVE_FEED:
                        result = CassandraDAO.getInstance().addVerifiedTopPastLiveFeed(liveTimeUUID, feedId);
                        break;
                }
            }
        } catch (Exception e) {
        }
        return result;
    }

    public int resetLiveTime(long pageId) {
        return LiveStreamingDAO.getInstance().resetLiveTime(pageId);
    }

    public List<LiveToChannelDTO> getLiveToChannelListInfo() {
        List<LiveToChannelDTO> list = new ArrayList<>();
        list = LiveStreamingDAO.getInstance().getLiveToChannelListInfo();
        List<Long> pageIds = list.stream().map(LiveToChannelDTO::getPageId).collect(Collectors.toList());
        List<UUID> channelIds = list.stream().map(dto -> UUID.fromString(dto.getChannelId())).collect(Collectors.toList());
        try {
            List<PageDTO> pageDTOs = CassandraDAO.getInstance().getPageDetailsForAdmin(pageIds, false);
            List<ChannelVerificationDTO> channelVerificationDTOs = ChannelService.getInstance().getChannelVerificationDTOList(channelIds);
            for (LiveToChannelDTO liveToChannelDTO : list) {
                if (liveToChannelDTO.getLiveTime() > 0) {
                    liveToChannelDTO.setLiveTimeStr(dateFormat.format(new Date(liveToChannelDTO.getLiveTime())));
                }
                for (PageDTO pageDTO : pageDTOs) {
                    if (liveToChannelDTO.getPageId() == pageDTO.getPageId()) {
                        liveToChannelDTO.setPageName(pageDTO.getPageName());
                        break;
                    }
                }
                for (ChannelVerificationDTO channelVerificationDTO : channelVerificationDTOs) {
                    if (liveToChannelDTO.getChannelId().equals(channelVerificationDTO.getChannelId().toString())) {
                        liveToChannelDTO.setChannelName(channelVerificationDTO.getTitle());
                        break;
                    }
                }
            }
        } catch (Exception ex) {
            RingLogger.getConfigPortalLogger().error("Exception in [getLiveToChannelListInfo] --> " + ex);
        }
        return list;
    }

    public int addOrUpdateLiveToChannelInfo(LiveToChannelDTO dto) {
        return LiveStreamingDAO.getInstance().addOrUpdateLiveToChannelInfo(dto);
    }

    public int deleteLiveToChannelInfo(Long pageId) {
        return LiveStreamingDAO.getInstance().deleteLiveToChannelInfo(pageId);
    }

    public LiveToChannelDTO getLiveToChannelInfo(long pageId) {
        LiveToChannelDTO dto = LiveStreamingDAO.getInstance().getLiveToChannelInfo(pageId);
        if (dto.getLiveTime() > 0) {
            dto.setLiveTimeStr(dateFormat.format(new Date(dto.getLiveTime())));
        }
        return dto;
    }

    public LiveToChannelDTO getLivetochannelDTO(LiveToChannelForm liveToChannelForm) {
        LiveToChannelDTO dto = new LiveToChannelDTO();
        dto.setPageId(liveToChannelForm.getPageId());
        dto.setLiveNow(liveToChannelForm.getLiveNow());
        if (liveToChannelForm.getChannelId() != null && liveToChannelForm.getChannelId().length() > 0) {
            dto.setChannelId(liveToChannelForm.getChannelId());
        } else {
            dto.setChannelId("");
        }
        if (liveToChannelForm.getTitle() != null && liveToChannelForm.getTitle().length() > 0) {
            dto.setTitle(liveToChannelForm.getTitle());
        } else {
            dto.setTitle("");
        }
        if (liveToChannelForm.getDescription() != null && liveToChannelForm.getDescription().length() > 0) {
            dto.setDescription(liveToChannelForm.getDescription());
        } else {
            dto.setDescription("");
        }
        if (liveToChannelForm.getLiveTime() != null && liveToChannelForm.getLiveTime().length() > 0) {
            try {
                dto.setLiveTime(dateFormat.parse(liveToChannelForm.getLiveTime()).getTime());
            } catch (ParseException ex) {
            }
        } else {
            dto.setLiveTime(0L);
        }
        return dto;
    }

    private LiveSummaryDTO getLiveSummaryDTO(LiveSummaryDTO sdto) {
        LiveSummaryDTO dto = new LiveSummaryDTO();
        dto.setId(sdto.getId());
        dto.setRingId(sdto.getRingId());
        dto.setName(sdto.getName());
        dto.setTotalLiveDuration(sdto.getTotalLiveDuration());
        dto.setTotalLiveDurationStr(sdto.getTotalLiveDurationStr());
        dto.setNoOfAppearance(sdto.getNoOfAppearance());
        dto.setDevice(sdto.getDevice());
        dto.setCountry(sdto.getCountry());
        dto.setCountryId(sdto.getCountryId());
        dto.setStreamDate(sdto.getStreamDate());
        dto.setStreamDateStr(sdto.getStreamDateStr());
        return dto;
    }

    public String getCloudValidity(long datetime, long currentTime) throws ParseException {
        long different = datetime - currentTime;
        long secondsInMillis = 1000;
        long minutesInMillis = secondsInMillis * 60;
        long hoursInMillis = minutesInMillis * 60;
        long daysInMillis = hoursInMillis * 24;

        long days = different / daysInMillis;
        different = different % daysInMillis;

        long hours = different / hoursInMillis;
        different = different % hoursInMillis;

        long minutes = different / minutesInMillis;
        different = different % minutesInMillis;

        long seconds = different / secondsInMillis;
        return new StringBuilder().append("0-").append(days).append("-").append(hours).append("-").append(minutes).append("-").append(seconds).toString();
    }
}
