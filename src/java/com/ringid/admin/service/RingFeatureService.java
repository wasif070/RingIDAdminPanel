/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.service;

import com.ringid.admin.dao.RingFeatureDAO;
import com.ringid.admin.dto.RingFeatureDTO;
import com.ringid.admin.utils.MyAppError;
import java.util.List;

/**
 *
 * @author Rabby
 */
public class RingFeatureService {

    private RingFeatureService() {

    }

    private static class RingFeatureTaskSchedulerHolder {

        private static final RingFeatureService INSTANCE = new RingFeatureService();
    }

    public static RingFeatureService getInstance() {
        return RingFeatureTaskSchedulerHolder.INSTANCE;
    }

    //Feature
    public List<RingFeatureDTO> getFeatureList(RingFeatureDTO sdto) {
        return RingFeatureDAO.getInstance().getFeatureList(sdto);
    }

    public RingFeatureDTO getFeatureById(int Id) {
        return RingFeatureDAO.getInstance().getFeatureById(Id);
    }

    public MyAppError addFeature(RingFeatureDTO sdto) {
        return RingFeatureDAO.getInstance().addFeature(sdto);
    }

    public MyAppError updateFeature(RingFeatureDTO sdto) {
        return RingFeatureDAO.getInstance().updateFeature(sdto);
    }

    public MyAppError deleteFeature(int id) {
        return RingFeatureDAO.getInstance().deleteFeature(id);
    }

    //FeatureMapping
    public List<RingFeatureDTO> getFeatureMappingList(RingFeatureDTO sdto) {
        return RingFeatureDAO.getInstance().getFeatureMappingList(sdto);
    }

    public List<RingFeatureDTO> getFeatureMappingByFeatureId(int featureId, int appType) {
        return RingFeatureDAO.getInstance().getFeatureMappingByFeatureId(featureId, appType);
    }

    public MyAppError addOrUpdateFeatureMapping(RingFeatureDTO sdto) {
        return RingFeatureDAO.getInstance().addOrUpdateFeatureMapping(sdto);
    }

    public MyAppError deleteFeatureMapping(RingFeatureDTO sdto) {
        return RingFeatureDAO.getInstance().deleteFeatureMapping(sdto);
    }

    public MyAppError deleteFeatureMappingById(int featureId, int appType) {
        return RingFeatureDAO.getInstance().deleteFeatureMappingById(featureId, appType);
    }
}
