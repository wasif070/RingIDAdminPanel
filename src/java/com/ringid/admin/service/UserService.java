/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.service;

import com.google.gson.Gson;
import com.ipvision.ringid.storage.utils.Pair;
import com.ringid.admin.cassandra.CassandraDAO;
import com.ringid.admin.dao.UserDAO;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.actions.ringId.resetPassword.ResetPasswordDTO;
import com.ringid.admin.actions.ringId.searchUser.CallAndChatStatusDTO;
import com.ringid.admin.actions.ringId.searchUser.FriendListReturnDTO;
import com.ringid.admin.actions.ringId.searchUser.SearchUserDTO;
import com.ringid.admin.actions.ringId.searchUser.SearchUserLoader;
import com.ringid.admin.actions.ringId.searchUser.UserDetailsDTO;
import com.ringid.admin.actions.ringId.unverifyNumber.UnverifyNumberDTO;
import com.ringid.admin.actions.ringId.userTableIdFormRingId.UserTableIdFromRingIdDTO;
import com.ringid.admin.actions.ringId.verifyNumber.VerifyNumberDTO;
import com.ringid.admin.utils.Constants;
import com.ringid.admin.utils.MyAppError;
import com.ringid.admin.utils.ReasonCode;
import dto.exception.InitializationException;
import dto.exception.ValidationException;
import dto.sessionless.SessionLessInfoDTO;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import org.json.JSONException;
import org.ringid.contacts.SearchedContactDTO;
import org.ringid.newsfeeds.FeedDTO;
import org.ringid.pages.PageDTO;
import org.ringid.pages.ServiceDTO;
import org.ringid.users.DeactiveReasonDTO;
import org.ringid.users.UserBasicInfoDTO;
import org.ringid.users.UserDTO;
import org.ringid.users.UserDeactivationDTO;
import org.ringid.users.UserLoginDetailsDTO;
import org.ringid.users.UserProfileDetailsDTO;
import org.ringid.users.UserSettingsDTO;
import org.ringid.utilities.AppConstants;
import ringid.LoginTaskScheduler;
import ringid.command.CommandTaskScheduler;
import ringid.sessionless.SessionlessTaskseheduler;

/**
 *
 * @author Rabby
 */
public class UserService {

    private static UserService instance = null;
    private final long MOD = 100000000;
    private final long PREFIX = 2100000000;

    private UserService() {
    }

    public static UserService getInstance() {
        if (instance == null) {
            createInstance();
        }
        return instance;
    }

    private synchronized static void createInstance() {
        if (instance == null) {
            instance = new UserService();
        }
    }

    public String adminAction(String userId, int actionType, int device, String message) {
        String response = null;
        String authIP = null;
        int authPort = 0;
        try {
            LoginTaskScheduler.getInstance().initSessionLessCommunication(userId);
            SessionLessInfoDTO dto = SessionlessTaskseheduler.getInstance().getRingServerInfo(userId, userId);
            if (dto != null && dto.getIp() != null) {
                authIP = dto.getIp().getHostAddress();
                authPort = dto.getPort();
            }
            switch (actionType) {
                case AppConstants.ADMIN_ACTION.DESTROY_USER_SESSION://<editor-fold defaultstate="collapsed" desc="10">
                {
                    response = CommandTaskScheduler.getInstance().destroySession(userId, userId, authIP, authPort);
                    break;
                }
                //</editor-fold>
                case AppConstants.ADMIN_ACTION.DESTROY_DEVICE_SESSIONS://<editor-fold defaultstate="collapsed" desc="13">
                {
                    response = CommandTaskScheduler.getInstance().destroyDeviceSession(userId, device, authIP, authPort);
                    break;
                }
                //</editor-fold>
                case AppConstants.ADMIN_ACTION.DESTROY_ALL_SESSIONS://<editor-fold defaultstate="collapsed" desc="17">
                {
                    response = CommandTaskScheduler.getInstance().destroyAllSession(userId, authIP, authPort);
                    break;
                }
                //</editor-fold>
                case AppConstants.ADMIN_ACTION.SESSION_DETAILS://<editor-fold defaultstate="collapsed" desc="18">
                {
                    response = CommandTaskScheduler.getInstance().sessionDetails(userId, authIP, authPort);
                    break;
                }
                //</editor-fold>
                case AppConstants.ADMIN_ACTION.CLEAR_CACHE://<editor-fold defaultstate="collapsed" desc="23">
                {
                    response = CommandTaskScheduler.getInstance().clearCache(userId, authIP, authPort);
                    break;
                }
                //</editor-fold>
                case AppConstants.ADMIN_ACTION.SIGN_OUT://<editor-fold defaultstate="collapsed" desc="24">
                {
                    response = CommandTaskScheduler.getInstance().signOut(userId, message, authIP, authPort);
                    break;
                }
                //</editor-fold>
                case AppConstants.ADMIN_ACTION.SEND_GENERAL_MESSAGE://<editor-fold defaultstate="collapsed" desc="25">
                {
                    response = CommandTaskScheduler.getInstance().sendGeneralMessage(userId, userId, message, authIP, authPort);
                    break;
                }
                //</editor-fold>
            }
            LoginTaskScheduler.getInstance().destroySessionLessCommunication(userId);
        } catch (InitializationException | ValidationException | IOException | InterruptedException | JSONException e) {
            RingLogger.getConfigPortalLogger().error("Exception [adminAction] --> ", e);
        }
        return response;
    }

    public int makeUserFeatured(long ringId, int weight, boolean calculateUtId) {
        int responseCode = -1;
        try {
            long userId;
            if (calculateUtId) {
                userId = CassandraDAO.getInstance().getUserTableId(ringId);
            } else {
                userId = ringId;
            }
            responseCode = CassandraDAO.getInstance().makeUserFeatured(userId, weight);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception [makeUserFeatured] --> ", e);
        }
        return responseCode;
    }

    public int updateFeaturedStatus(long ringId, int weight, boolean userVerificationStatus) {
        int reasonCode = -1;
        try {
            long userId = CassandraDAO.getInstance().getUserTableId(ringId);
            reasonCode = CassandraDAO.getInstance().updateFeaturedStatus(userId, weight, userVerificationStatus);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception [updateFeaturedStatus] --> " + e);
        }
        return reasonCode;
    }

    public int updateFeaturedUsersWeight(long ringId, int weight) {
        int reasonCode = -1;
        try {
            long userId = CassandraDAO.getInstance().getUserTableId(ringId);
            reasonCode = CassandraDAO.getInstance().updateFeaturedUsersWeight(userId, weight);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception [updateFeaturedUsersWeight] --> " + e);
        }
        return reasonCode;
    }

    public long getPageOwnerId(long pageId) {
        long pageOwnerId = 0;
        try {
            pageOwnerId = CassandraDAO.getInstance().getPageOwnerId(pageId);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [getPageOwnerId] -> " + e);
        }
        return pageOwnerId;
    }

    public int deactivateUserAccount(String id) {
        int reasonCode = -1;
        try {
            long userId = Long.parseLong(id);
            long tempId = CassandraDAO.getInstance().getUserTableId(userId);
            if (tempId > 0) {
                userId = tempId;
            }
            List<DeactiveReasonDTO> list = CassandraDAO.getInstance().getDeactivateReasons();
            UserDeactivationDTO userDeactivationDTO = new UserDeactivationDTO();
            userDeactivationDTO.setUserId(userId);
            if (list.size() > 0) {
                userDeactivationDTO.setReason(list.get(0).getId());
            }
            reasonCode = CassandraDAO.getInstance().deactivateUserAccount(userDeactivationDTO);
            if (reasonCode == ReasonCode.NONE) {
                long ringId = CassandraDAO.getInstance().getRingID(userId);
                LoginTaskScheduler.getInstance().initSessionLessCommunication(Constants.USERID);
                SessionLessInfoDTO infoDto = SessionlessTaskseheduler.getInstance().getRingServerInfo(Constants.USERID, String.valueOf(ringId));
                if (infoDto != null) {
                    RingLogger.getConfigPortalLogger().info("[forceSignOut] authIP: " + infoDto.getIp().getHostAddress() + " authPort: " + infoDto.getPort());
                    String result2 = SessionlessTaskseheduler.getInstance().forceSignOut(Constants.USERID, String.valueOf(ringId), "Account Deactivated", infoDto.getIp().getHostAddress(), infoDto.getPort());
                    RingLogger.getConfigPortalLogger().info("[forceSignOut] ringId --> " + ringId + " response --> " + result2);
                }
                LoginTaskScheduler.getInstance().destroySessionLessCommunication(Constants.USERID);
            }
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [deactivateUserAccount] --> " + e);
        }
        return reasonCode;
    }

    public List<UserBasicInfoDTO> getUserListByCountryCode(String countryCode) {
        return SearchUserLoader.getInstance().getUserListByCountryCode(countryCode);
    }

    public UserBasicInfoDTO getUserBasicInfo(long pageId) {
        UserBasicInfoDTO userBasicInfoDTO = null;
        try {
            userBasicInfoDTO = CassandraDAO.getInstance().getUserBasicInfo(pageId);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [getUserBasicInfo] --> " + e);
        }
        return userBasicInfoDTO;
    }

    public HashMap<Long, UserBasicInfoDTO> getUserBasicInfoHashMap(List<FeedDTO> feedDTOs) {
        HashMap<Long, UserBasicInfoDTO> userBasicInfoDTOHashMap = new HashMap<>();
        try {
            for (FeedDTO feedDTO : feedDTOs) {
                if (feedDTO.getParentFeedId() != null) {
                    RingLogger.getConfigPortalLogger().info("[getUserBasicInfoHashMap] --> [ WallOwnerId --> " + feedDTO.getParentFeedInfo().getWallOwnerId() + " ]");
                    UserBasicInfoDTO userBasicInfoDTO = CassandraDAO.getInstance().getUserBasicInfo(feedDTO.getParentFeedInfo().getWallOwnerId());
                    userBasicInfoDTOHashMap.put(userBasicInfoDTO.getUserId(), userBasicInfoDTO);
                }
            }
            RingLogger.getConfigPortalLogger().info("[getUserBasicInfoHashMap] --> " + userBasicInfoDTOHashMap.size());
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Error in getUserBasicInfoHashMap: " + e.getMessage());
        }
        return userBasicInfoDTOHashMap;
    }

    public int changeRingId(long oldRingId, long newRingId) {
        int reasonCode = -1;
        try {
            reasonCode = CassandraDAO.getInstance().changeRingId(oldRingId, newRingId);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [changeRingId] --> " + e);
        }
        return reasonCode;
    }

    public boolean isValiduser(long ringID) {
        boolean validity = false;
        try {
            long userTableId = CassandraDAO.getInstance().getUserTableId(ringID);
            if (userTableId > 0) {
                validity = true;
            }
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception [isValiduser] --> ", e);
        }
        return validity;
    }

    public int unverifyMyNumber(UnverifyNumberDTO dto) {
        int result = -1;
        try {
            long userTableId = CassandraDAO.getInstance().getUserTableId(Long.parseLong(dto.getUserId()));
            result = CassandraDAO.getInstance().unverifyMyNumber(dto.getDialingCode(), dto.getMobileNumber(), userTableId);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception [unverifyMyNumber] --> ", e);
        }
        return result;
    }

    public int verifyMyNumber(VerifyNumberDTO dto) {
        int result = -1;
        try {
            long userTableId = CassandraDAO.getInstance().getUserTableId(Long.parseLong(dto.getUserId()));
            result = CassandraDAO.getInstance().verifyMyNumber(dto.getDialingCode(), dto.getMobileNumber(), userTableId);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception [unverifyMyNumber] --> ", e);
        }
        return result;
    }

    public void unverifyNumberIfVerified(VerifyNumberDTO dto) {
        try {
            long userTableId = CassandraDAO.getInstance().getUserTableId(Long.parseLong(dto.getUserId()));
            UserSettingsDTO userSettingsDTO = CassandraDAO.getInstance().getMySettings(userTableId);
            if (userSettingsDTO != null && userSettingsDTO.getIsMyNumberVerified() == AppConstants.YES) {
                CassandraDAO.getInstance().unverifyMyNumber(userSettingsDTO.getMobilePhoneDialingCode(), userSettingsDTO.getMobilePhone(), userTableId);
            }
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception [unverifyNumberIfVerified] --> ", e);
        }
    }

    public auth.com.utils.MyAppError resetPassword(ResetPasswordDTO dto) {
        auth.com.utils.MyAppError error = new auth.com.utils.MyAppError();
        try {
            if (dto.getUserId() == null || dto.getUserId().length() == 0) {
                error.setERROR_TYPE(MyAppError.OTHERERROR);
                error.setErrorMessage("Invalid UserId");
                return error;
            } else {
                long utId = CassandraDAO.getInstance().getUserTableId(Long.parseLong(dto.getUserId()));
                if (utId == 0) {
                    error.setERROR_TYPE(MyAppError.OTHERERROR);
                    error.setErrorMessage("Invalid UserId");
                    return error;
                }
            }
            if (dto.getPassword() == null || dto.getPassword().length() == 0) {
                error.setERROR_TYPE(MyAppError.OTHERERROR);
                error.setErrorMessage("Invalid Password");
                return error;
            }
            RingLogger.getConfigPortalLogger().debug("[resetPassword] userId -> " + dto.getUserId() + " password -> " + dto.getPassword());
            error = SessionlessTaskseheduler.getInstance().resetPassword(dto.getUserId(), dto.getPassword());
            if (error.getERROR_TYPE() == MyAppError.NOERROR) {
                com.ringid.admin.utils.MyAppError myAppError = updatePassword(dto);
                if (myAppError.getERROR_TYPE() > 0) {
                    error.setERROR_TYPE(MyAppError.OTHERERROR);
                    error.setErrorMessage("Password reset successfully but Mysql update failed.");
                    RingLogger.getConfigPortalLogger().error("Password reset successfully but Mysql update failed.");
                }
            }
        } catch (Exception e) {
            error.setERROR_TYPE(MyAppError.OTHERERROR);
            error.setErrorMessage("Exception occurred");
            RingLogger.getConfigPortalLogger().error("Exception in [resetPassword] -> " + e);
        }
        return error;
    }

    public List<UserTableIdFromRingIdDTO> getUsetTableIdFromRingId(String[] ringIds) {
        List<UserTableIdFromRingIdDTO> list = new ArrayList<>();
        try {
            for (String ringId : ringIds) {
                long rId;
                try {
                    rId = Long.parseLong(ringId.trim());
                } catch (Exception e) {
                    RingLogger.getConfigPortalLogger().error("Invalid Ringid --> " + ringId);
                    continue;
                }
                long utId = CassandraDAO.getInstance().getUserTableId(rId);
                UserTableIdFromRingIdDTO dto = new UserTableIdFromRingIdDTO();
                dto.setRingId(rId);
                dto.setUserTableId(utId);
                list.add(dto);
            }
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception [getUsetTableIdFromRingId] --> ", e);
        }
        return list;
    }

    public SearchUserDTO searchUserContacts(String searchText, int start, int limit) {
        SearchUserDTO searchResult = new SearchUserDTO();
        List<SearchedContactDTO> searchResultPeople = new ArrayList<>();
        List<SearchedContactDTO> searchResultPage = new ArrayList<>();
        HashMap<String, SearchedContactDTO> searchResultMap = new HashMap<>();
        try {
            if (searchText != null && searchText.length() > 0) {
                RingLogger.getConfigPortalLogger().info("[SearchUsers] searchText -> " + searchText);
                /*All search Result*/
                List<SearchedContactDTO> searchResultTemp = new ArrayList<>();
                // SearchUser By RingID or UserTableId
                SearchUserDTO searchUserDTO = searchUserInfo(searchText);
                if (searchUserDTO != null && searchUserDTO.getUserDetailsDTO() != null) {
                    SearchedContactDTO searchSingleContact = convertUserDetailesDTOToSearchedContactDTO(searchUserDTO.getUserDetailsDTO());
                    searchResultTemp.add(searchSingleContact);
                }
                //getSearchResult
                List<SearchedContactDTO> searchResults = getSearchResult(AppConstants.SEARCH_TYPE.COMBINED_DEFULT_VIEW, searchText, start, limit);
                if (searchResults != null) {
                    searchResultTemp.addAll(searchResults);
                }
                /*END*/
                searchResultTemp
                        .stream()
                        .filter(dto -> {
                            return !searchResultMap.containsKey(dto.getUserIdentity());
                        })
                        .forEach(dto -> {
                            searchResultMap.put(dto.getUserIdentity(), dto);
                            if (dto.getProfileType() == AppConstants.UserTypeConstants.DEFAULT_USER) {
                                searchResultPeople.add(dto);
                            } else {
                                searchResultPage.add(dto);
                            }
                        });
                searchResult.setSearchResultPeople(searchResultPeople);
                searchResult.setSearchResultPage(searchResultPage);
            }
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in searchUsers -> " + e);
        }
        RingLogger.getConfigPortalLogger().info("[searchUsers] searchUserResult -> " + new Gson().toJson(searchResult));
        return searchResult;
    }

    public List<SearchedContactDTO> getSearchResult(int searchType, String searchParam, int start, int limit) {
        List<SearchedContactDTO> searchResult = new ArrayList<>();
        if (limit <= 1 || limit > 10) {
            limit = 10;
        }
        try {
            if (searchParam != null && searchParam.length() > 0) {
                switch (searchType) {
                    case AppConstants.SEARCH_TYPE.ONLY_PAGE: {
                        searchResult = CassandraDAO.getInstance().getSearchResult(searchParam, AppConstants.ALL_PAGES_UserTypeConstants, start, limit);
                        break;
                    }
                    case AppConstants.SEARCH_TYPE.DEFAULT_USER: {
                        searchResult = CassandraDAO.getInstance().getSearchResult(searchParam, AppConstants.ONLY_DEFAULT_USER, start, limit);
                        break;
                    }
                    case AppConstants.SEARCH_TYPE.COMBINED_DEFULT_VIEW:
                    default: {
                        List<SearchedContactDTO> searchResultOnlyPage = CassandraDAO.getInstance().getSearchResult(searchParam, AppConstants.ALL_PAGES_UserTypeConstants, start, limit);
                        if (searchResultOnlyPage != null) {
                            searchResult = searchResultOnlyPage;
                        }
                        List<SearchedContactDTO> searchResultOnlyUser = CassandraDAO.getInstance().getSearchResult(searchParam, AppConstants.ONLY_DEFAULT_USER, start, limit);
                        if (searchResultOnlyUser != null) {
                            searchResult.addAll(searchResultOnlyUser);
                        }
                        break;
                    }
                }
            }
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in getSearchResult -> " + e);
        }
        return searchResult;
    }

    public SearchUserDTO searchUserInfo(String searchText) {
        SearchUserDTO searchUserDTO = new SearchUserDTO();
        UserDetailsDTO userDetailsDTO = null;
        try {
            if (searchText != null && searchText.length() > 0) {
                long userTableId = 0L;
                try {
                    userTableId = Long.parseLong(searchText.trim());
                } catch (Exception e) {
                    userTableId = 0L;
                }
                long tempId = CassandraDAO.getInstance().getUserTableId(getRingId(userTableId));
                if (tempId > 0) {
                    userTableId = tempId;
                }

                UserProfileDetailsDTO userProfileDetailsDTO = CassandraDAO.getInstance().getMyProfile(userTableId);
                UserDTO userDTO = CassandraDAO.getInstance().getUserDetails(userTableId, true);
                userDetailsDTO = getUserDetails(userProfileDetailsDTO, userDTO);
                if (userDetailsDTO != null) {
                    searchUserDTO.setUserDetailsDTO(userDetailsDTO);
                    searchUserDTO.setUserDeviceList(CassandraDAO.getInstance().getDeviceInfo(userTableId));
                    if (userDetailsDTO.getUserType() != AppConstants.UserTypeConstants.DEFAULT_USER) {
                        long pageOwnerId = CassandraDAO.getInstance().getPageOwnerId(userDTO.getUserTableID());
                        searchUserDTO.setPageOwnerInfo(CassandraDAO.getInstance().getUserBasicInfo(pageOwnerId));
                    } else {
                        HashMap<Integer, List<ServiceDTO>> userPageMap = CassandraDAO.getInstance().getPageMap(userTableId);
                        if (userPageMap != null && userPageMap.size() > 0) {
                            searchUserDTO.setUserPageMap(userPageMap);
                        }
                        Calendar calendar = Calendar.getInstance();
                        calendar.setTimeInMillis(CassandraDAO.getInstance().getRegistrationDate(userTableId));
                        String registrationDateStr = new StringBuilder().append(calendar.get(Calendar.DAY_OF_MONTH)).append("/").append(calendar.get(Calendar.MONTH)).append("/").append(calendar.get(Calendar.YEAR)).toString();
                        userDetailsDTO.setRegistrationDateStr(registrationDateStr);
                        Pair<Long, Integer> userLastSeen = CassandraDAO.getInstance().userLastSeen(userTableId);
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
                        userDetailsDTO.setLastLoginTime(sdf.format(new Date(userLastSeen.getFirst())));
                        if (userLastSeen.getSecond() < AppConstants.DEVICES.length) {
                            userDetailsDTO.setLastLoginPlatform(AppConstants.DEVICES[userLastSeen.getSecond()]);
                        } else {
                            userDetailsDTO.setLastLoginPlatform("" + userLastSeen.getSecond());
                        }
                    }
                    //followerCount and friendCount
                    long followerCount = 0;
                    long followingCount = 0;
                    long friendCount = 0;
                    long user_id = userDetailsDTO.getUtid();
                    switch (userDetailsDTO.getUserType()) {
                        case AppConstants.UserTypeConstants.USER_PAGE:
                        case AppConstants.UserTypeConstants.CELEBRITY:
                        case AppConstants.UserTypeConstants.DONATION_PAGE: {
                            PageDTO pageDTO = CassandraDAO.getInstance().getUserPageInfo(user_id);
                            if (pageDTO != null) {
                                followerCount = pageDTO.getFollowerCount();
                                userDetailsDTO.setIsPageVerified(pageDTO.getIsPageVerified());
                            }
                            break;
                        }
                        case AppConstants.UserTypeConstants.SPORTS_PAGE:
                        case AppConstants.UserTypeConstants.NEWSPORTAL:
                        case AppConstants.UserTypeConstants.MEDIA_PAGE:
                        case AppConstants.UserTypeConstants.BUSINESS_PAGE:
                        case AppConstants.UserTypeConstants.PRODUCT_PAGE: {
                            PageDTO pageDTO = CassandraDAO.getInstance().getPageInfo(user_id);
                            if (pageDTO != null) {
                                followerCount = pageDTO.getFollowerCount();
                                userDetailsDTO.setIsPageVerified(pageDTO.getIsPageVerified());
                            }
                            break;
                        }
                        case AppConstants.UserTypeConstants.DEFAULT_USER: {
                            followerCount = CassandraDAO.getInstance().getTotalFollowerCount(user_id);
                            followingCount = CassandraDAO.getInstance().getTotalFollowingCount(user_id);
                            friendCount = CassandraDAO.getInstance().getFriendCount(user_id);
                            break;
                        }
                        default: {
                            break;
                        }
                    }
                    userDetailsDTO.setFollowerCount(followerCount);
                    userDetailsDTO.setFollowingCount(followingCount);
                    userDetailsDTO.setFriendCount(friendCount);
                    List<CallAndChatStatusDTO> callAndChatStatusDTOs = new ArrayList<>();
                    for (int i = 0; i < AppConstants.APP_TYPES_ARRAY.length; i++) {
//                        Pair<Integer, Boolean> anonymousAndGlobalCallStatus = CassandraDAO.getInstance().getAnonymousCallStatusAndGlobalCallStatus(userTableId, AppConstants.APP_TYPES_ARRAY[i]);
                        CallAndChatStatusDTO callAndChatStatusDTO = new CallAndChatStatusDTO();
                        callAndChatStatusDTO.setAnonymousCallStatus(CassandraDAO.getInstance().getAnonymousCallStatus(userTableId, AppConstants.APP_TYPES_ARRAY[i]));
                        callAndChatStatusDTO.setGlobalCallStatus(CassandraDAO.getInstance().getGlobalCallStatus(userTableId, AppConstants.APP_TYPES_ARRAY[i]) == true ? 1 : 0);
                        callAndChatStatusDTO.setAppType(AppConstants.APP_TYPES_ARRAY[i]);
                        UserSettingsDTO userSettingsDTO = CassandraDAO.getInstance().getMySettings(user_id);
                        callAndChatStatusDTO.setAnonymousChatStatus(userSettingsDTO.getAnonymousChat());
                        callAndChatStatusDTOs.add(callAndChatStatusDTO);
                    }
                    searchUserDTO.setCallAndChatStatusDTOs(callAndChatStatusDTOs);
                }
            }
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception [searchUserInfo] --> ", e);
        }
        return searchUserDTO;
    }

    private UserDetailsDTO getUserDetails(UserProfileDetailsDTO userProfileDetailsDTO, UserDTO userDTO) {

        UserDetailsDTO userDetailsDTO = null;
        if (userDTO != null) {
            userDetailsDTO = new UserDetailsDTO();
            userDetailsDTO.setRingId(userDTO.getRingID());
            userDetailsDTO.setUtid(userDTO.getUserTableID());
            userDetailsDTO.setFullName(userDTO.getFullName());
            userDetailsDTO.setProfileImageUrl(userDTO.getProfileImage());
            userDetailsDTO.setCoverImageUrl(getCropCoverImageUrl(userDTO.getCoverImage()));
            userDetailsDTO.setUserType(userDTO.getUserType());
            switch (userDTO.getUserType()) {
                case AppConstants.UserTypeConstants.DEFAULT_USER:
                    userDetailsDTO.setUserTypeStr("User");
                    break;
                case AppConstants.UserTypeConstants.SPECIAL_CONTACT:
                    userDetailsDTO.setUserTypeStr("Special Contact");
                    break;
                case AppConstants.UserTypeConstants.USER_PAGE:
                    userDetailsDTO.setUserTypeStr("Page");
                    break;
                case AppConstants.UserTypeConstants.ROOM_PAGE:
                    userDetailsDTO.setUserTypeStr("Room");
                    break;
                case AppConstants.UserTypeConstants.DONATION_PAGE:
                    userDetailsDTO.setUserTypeStr("Donation");
                    break;
                case AppConstants.UserTypeConstants.SPORTS_PAGE:
                    userDetailsDTO.setUserTypeStr("Sports");
                    break;
                case AppConstants.UserTypeConstants.CELEBRITY:
                    userDetailsDTO.setUserTypeStr("Celebrity");
                    break;
                case AppConstants.UserTypeConstants.NEWSPORTAL:
                    userDetailsDTO.setUserTypeStr("Newsportal");
                    break;
                case AppConstants.UserTypeConstants.MEDIA_PAGE:
                    userDetailsDTO.setUserTypeStr("Media");
                    break;
                case AppConstants.UserTypeConstants.BUSINESS_PAGE:
                    userDetailsDTO.setUserTypeStr("Business");
                    break;
                case AppConstants.UserTypeConstants.GROUP:
                    userDetailsDTO.setUserTypeStr("Group");
                    break;
                case AppConstants.UserTypeConstants.PRODUCT_PAGE:
                    userDetailsDTO.setUserTypeStr("Product");
                    break;
                default:
                    userDetailsDTO.setUserTypeStr(String.valueOf(userDTO.getUserType()));
                    break;
            }
            userDetailsDTO.setActiveStatus(userDTO.getActiveStatus());
            switch (userDTO.getActiveStatus()) {
                case AppConstants.UserActiveStatus.ACTIVE:
                    userDetailsDTO.setActiveStatusStr("Active");
                    break;
                case AppConstants.UserActiveStatus.DEACTIVE:
                    userDetailsDTO.setActiveStatusStr("Deactive");
                    break;
                case AppConstants.UserActiveStatus.CLOSED:
                    userDetailsDTO.setActiveStatusStr("Closed");
                    break;
                default:
                    userDetailsDTO.setActiveStatusStr("" + userDTO.getActiveStatus());
                    break;
            }

            userDetailsDTO.setGender(userDTO.getGender());
            userDetailsDTO.setMobilePhone(userDTO.getMobilePhone());
            userDetailsDTO.setDialingCode(userDTO.getMobilePhoneDialingCode());
            userDetailsDTO.setEmail(userDTO.getEmail());
            userDetailsDTO.setCountry(userDTO.getCountry());
            userDetailsDTO.setCurrentCity(userDTO.getCurrentCity());
            userDetailsDTO.setHomeCity(userDTO.getHomeCity());
            userDetailsDTO.setAboutMe(userDTO.getAboutMe());
            userDetailsDTO.setSecondaryEmail(userProfileDetailsDTO.getSecondaryEmail());
            userDetailsDTO.setBirthday(userProfileDetailsDTO.getBirthDay() + "/" + userProfileDetailsDTO.getBirthMonth() + "/" + userProfileDetailsDTO.getBirthYear());
            userDetailsDTO.setSecondaryMobileNumber(userProfileDetailsDTO.getSecondaryMobileNumber());
            userDetailsDTO.setSecondaryDialingCode(userProfileDetailsDTO.getSecondaryDialingCode());
            userDetailsDTO.setSpamUser(userDTO.isSpammedUser());
        }
        return userDetailsDTO;
    }

    private SearchedContactDTO convertUserDetailesDTOToSearchedContactDTO(UserDetailsDTO sdto) {
        SearchedContactDTO dto = new SearchedContactDTO();
        dto.setFullName(sdto.getFullName());
        dto.setUserIdentity("" + sdto.getRingId());
        dto.setUserTableID(sdto.getUtid());
        dto.setCountry(sdto.getCountry());
        dto.setProfileImage(sdto.getProfileImageUrl());
        dto.setProfileType(sdto.getUserType());
        dto.setMobilePhone(sdto.getMobilePhone());
        dto.setDialingCode(sdto.getDialingCode());
        return dto;
    }

    public MyAppError updatePassword(ResetPasswordDTO resetPasswordDTO) {
        String encryptPassword = auth.com.utils.Utils.toSHA1(resetPasswordDTO.getPassword());
        return UserDAO.getInstance().updatePassword(resetPasswordDTO.getUserId(), encryptPassword);
    }

    public MyAppError updatePhoneNumberInfo(String mobileNumber, String dialingCode, String userId, int verified) {
        return UserDAO.getInstance().updateMyPhoneNumberInfo(mobileNumber, dialingCode, userId, verified);
    }

    public UserBasicInfoDTO getUserBasicInfoByRingID(long ringID) {
        UserBasicInfoDTO userBasicInfoDTO = null;
        try {
            userBasicInfoDTO = CassandraDAO.getInstance().getUserBasicInfoByRingID(getRingId(ringID), true);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [getUserBasicInfoByRingID] --> " + e);
        }
        return userBasicInfoDTO;
    }

    public UserSettingsDTO getMySettings(long userId) {
        UserSettingsDTO userSettingsDTO = null;
        try {
            userSettingsDTO = CassandraDAO.getInstance().getMySettings(userId);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [getMySettings] --> " + e);
        }
        return userSettingsDTO;
    }

    public int resetSignupRetryLimit(String dialingCode, String mobileNo) {
        int reasonCode = -1;
        try {
            reasonCode = UserDAO.getInstance().resetSignupRetryLimit(dialingCode, mobileNo);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [resetSignupRetryLimit] --> " + e);
        }
        return reasonCode;
    }

    public long getUserIdFromRingId(String ringId) {
        long userId = 0L;
        try {
            userId = CassandraDAO.getInstance().getUserTableId(Long.parseLong(getRingId(ringId)));
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception [updateNameUpdateLimit] --> ", e);
        }
        return userId;
    }

    public MyAppError updateRingId(String oldRingId, String newRingId) {
        return UserDAO.getInstance().updateRingId(oldRingId, newRingId);
    }

    public int updateTotalFriendsLimit(int limit) {
        int reasonCode = -1;
        try {
            reasonCode = CassandraDAO.getInstance().updateTotalFriendsLimit(limit);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception [updateTotalFriendsLimit] --> ", e);
        }
        return reasonCode;
    }

    public int updatePendingFriendsLimit(int limit) {
        int reasonCode = -1;
        try {
            reasonCode = CassandraDAO.getInstance().updatePendingFriendsLimit(limit);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception [updatePendingFriendsLimit] --> ", e);
        }
        return reasonCode;
    }

    public int getPendingFriendLimit() {
        int limit = 0;
        try {
            limit = CassandraDAO.getInstance().getPendingFriendLimit();
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [getPendingFriendLimit ] --> " + e.getMessage());
        }
        return limit;
    }

    public int getTotalFriendLimit() {
        int limit = 0;
        try {
            limit = CassandraDAO.getInstance().getTotalFriendLimit();
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [getTotalFriendLimit ] --> " + e.getMessage());
        }
        return limit;
    }

    public FriendListReturnDTO getFriendList(long userId, long pivotId, int limit) {
        FriendListReturnDTO friendListReturnDTO = new FriendListReturnDTO();
        try {
            ArrayList<UserDTO> friendList = CassandraDAO.getInstance().getFriendList(userId, pivotId, limit);
            friendListReturnDTO.setFriendList(friendList);
            long maxId = 0;
            for (UserDTO dto : friendList) {
                maxId = Math.max(maxId, dto.getUserTableID());
            }
            friendListReturnDTO.setNextPivotId(maxId);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [getFriendList] e -> " + e);
        }
        return friendListReturnDTO;
    }

    public UserLoginDetailsDTO getLoginUserByRingId(long ringId, int appType, int devicePlatform) {
        UserLoginDetailsDTO dto = new UserLoginDetailsDTO();
        try {
            dto = CassandraDAO.getInstance().getLoginUserByRingId(ringId, appType, devicePlatform);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [getLoginUserByRingId] --> " + e.getMessage());
        }
        return dto;
    }

    private String getRingId(String ringId) {
        if (ringId != null && ringId.length() == 8) {
            ringId = "21" + ringId;
        }
        return ringId;
    }

    private long getRingId(long ringID) {
        return PREFIX + (ringID % MOD);
    }

    private String getCropCoverImageUrl(String url) {
        if (url == null || url.length() == 0) {
            return url;
        }
        String result = url.substring(0, url.lastIndexOf("/") + 1) + "crp" + url.substring(url.lastIndexOf("/") + 1, url.length());
        return result;
    }
}
