/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.service;

import com.ringid.admin.cassandra.CassandraDAO;
import com.ringid.admin.dao.FloatingMenuDAO;
import com.ringid.admin.dto.FloatingMenuDTO;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.dto.FloatingMenuSlideItemDTO;
import com.ringid.admin.utils.Constants;
import com.ringid.admin.utils.MyAppError;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import javafx.util.Pair;
import org.apache.logging.log4j.Logger;
import org.ringid.channel.ChannelCategoryDTO;
import org.ringid.channel.ChannelDTO;
import org.ringid.pages.PageDTO;
import org.ringid.users.UserDTO;
import org.ringid.utilities.AppConstants;
import org.ringid.utilities.PivotedListReturnDTO;

/**
 *
 * @author Rabby
 */
public class FloationMenuService {

    static private Logger logger = RingLogger.getConfigPortalLogger();

    private FloationMenuService() {

    }

    private static class CommonTaskSchedulerHolder {

        private static final FloationMenuService INSTANCE = new FloationMenuService();
    }

    public static FloationMenuService getInstance() {
        return CommonTaskSchedulerHolder.INSTANCE;
    }

    private boolean containsAny(FloatingMenuDTO dto, String queryString) {
        return (dto.getName() != null && dto.getName().toLowerCase().contains(queryString))
                || (String.valueOf(dto.getTab()).contains(queryString))
                || (String.valueOf(dto.getType()).contains(queryString))
                || (String.valueOf(dto.getWeight()).contains(queryString))
                || (String.valueOf(dto.getSlideTime()).contains(queryString))
                || (String.valueOf(dto.getRedirectId()).contains(queryString));
    }

    public List<FloatingMenuDTO> getFloatingMenuList(FloatingMenuDTO sdto) {
        List<FloatingMenuDTO> floatingMenuDTOs = new ArrayList<>();
        List<FloatingMenuDTO> list = FloatingMenuDAO.getInstance().getFloatingMenuList();
        for (FloatingMenuDTO dto : list) {
            if (sdto.getSearchText() != null && sdto.getSearchText().length() > 0 && !containsAny(dto, sdto.getSearchText())) {
                continue;
            }
            floatingMenuDTOs.add(dto);
        }
        if (sdto.getSortType() == Constants.DESC_SORT) {
            Collections.sort(floatingMenuDTOs, new FloatingMenuDTO.CompDSC());
        } else {
            Collections.sort(floatingMenuDTOs, new FloatingMenuDTO.CompASC());
        }
        return floatingMenuDTOs;
    }

    public MyAppError addFloatingMenuInfo(FloatingMenuDTO floatingMenuDTO) {
        return FloatingMenuDAO.getInstance().addFloatingMenuInfo(floatingMenuDTO);
    }

    public FloatingMenuDTO getFloatingMenuByType(int tab, int type, String name) {
        return FloatingMenuDAO.getInstance().getFloatingMenuByType(tab, type, name);
    }

    public MyAppError updateFloatingMenuInfo(FloatingMenuDTO dto, String name) {
        return FloatingMenuDAO.getInstance().updateFloatingMenuInfo(dto, name);
    }

    public MyAppError deleteFloatingMenu(int type) {
        return FloatingMenuDAO.getInstance().deleteFloatingMenu(type);
    }

    public MyAppError addFloatingMenuSlideItem(int featureType, String[] featureIds) {
        MyAppError error = new MyAppError();
        for (String id : featureIds) {
            error = FloatingMenuDAO.getInstance().addFloatingMenuSlideItem(featureType, id);
        }
        return error;
    }

    public MyAppError deleteFloatingMenuSlideItem(int featureType, String[] featureIds) {
        MyAppError error = new MyAppError();
        for (String id : featureIds) {
            error = FloatingMenuDAO.getInstance().deleteFloatingMenuSlideItem(featureType, id);
        }
        return error;
    }

    public List<FloatingMenuSlideItemDTO> getData(int featureType, int status, String queryString) {
        List<FloatingMenuSlideItemDTO> listItems = new ArrayList<>();
        Map<Pair<Integer, String>, FloatingMenuSlideItemDTO> floatingMenuSlideItemDTOs = FloatingMenuDAO.getInstance().getFloationMenuSlideItemList(featureType);
        List<ChannelDTO> channelList;
        try {
            switch (featureType) {
                case AppConstants.DYNAMIC_FLOATING_MENU.TOP_CHANNEL:
                    channelList = CassandraDAO.getInstance().getVerifiedChannels();
                    for (ChannelDTO dto : channelList) {
                        FloatingMenuSlideItemDTO floatingMenuSlideItemDTO = new FloatingMenuSlideItemDTO();
                        floatingMenuSlideItemDTO.setFeatureType(featureType);
                        floatingMenuSlideItemDTO.setFeatureId(String.valueOf(dto.getChannelId()));
                        if (floatingMenuSlideItemDTOs.containsKey(new Pair<>(floatingMenuSlideItemDTO.getFeatureType(), floatingMenuSlideItemDTO.getFeatureId()))) {
                            floatingMenuSlideItemDTO.setStatus(1);
                        } else {
                            floatingMenuSlideItemDTO.setStatus(0);
                        }
                        floatingMenuSlideItemDTO.setName(dto.getTitle());
                        floatingMenuSlideItemDTO.setProfileImageUrl(dto.getProfileImgUrl());

                        if (status != -1 && floatingMenuSlideItemDTO.getStatus() != status) {
                            continue;
                        }

                        listItems.add(floatingMenuSlideItemDTO);
                    }
//                    listItems.addAll(floatingMenuSlideItemDTOs);
                    break;
                case AppConstants.DYNAMIC_FLOATING_MENU.NEWS_PORTAL:
                    List<PageDTO> pageDTOs = new ArrayList<>();
                    List<PageDTO> featuredPageDTOs;
                    int limit = 1000;
                    UUID pivotId = null;
                    PivotedListReturnDTO<Long, UUID> pivotedListReturnDTO = CassandraDAO.getInstance().getFeaturedPageIds(AppConstants.UserTypeConstants.NEWSPORTAL, pivotId, limit, AppConstants.SCROLL_DOWN);
                    if (pivotedListReturnDTO != null && pivotedListReturnDTO.getList().size() > 0) {
                        pageDTOs = CassandraDAO.getInstance().getPageDetailsForAdmin(pivotedListReturnDTO.getList(), false);
                    }

                    for (PageDTO dto : pageDTOs) {
                        FloatingMenuSlideItemDTO floatingMenuSlideItemDTO = new FloatingMenuSlideItemDTO();
                        floatingMenuSlideItemDTO.setFeatureType(featureType);
                        floatingMenuSlideItemDTO.setFeatureId(String.valueOf(dto.getPageId()));
                        if (floatingMenuSlideItemDTOs.containsKey(new Pair<>(floatingMenuSlideItemDTO.getFeatureType(), floatingMenuSlideItemDTO.getFeatureId()))) {
                            floatingMenuSlideItemDTO.setStatus(1);
                        } else {
                            floatingMenuSlideItemDTO.setStatus(0);
                        }
                        floatingMenuSlideItemDTO.setName(dto.getPageName());
                        floatingMenuSlideItemDTO.setProfileImageUrl(dto.getProfileImageUrl());

                        if (status != -1 && floatingMenuSlideItemDTO.getStatus() != status) {
                            continue;
                        }

                        listItems.add(floatingMenuSlideItemDTO);
                    }
//                    listItems.addAll(floatingMenuSlideItemDTOs);
                    break;
                case AppConstants.DYNAMIC_FLOATING_MENU.CELEBRITY:
                    List<UserDTO> celebrityUserDTOs = new ArrayList<>();
                    long dataSize = 0,
                     pivot = 0;
                    do {
                        List<UserDTO> temp = CassandraDAO.getInstance().getCelebrityList(pivot, 1000);
                        dataSize = temp.size();
                        pivot = 0;
                        if (dataSize > 0) {
                            pivot = temp.get(temp.size() - 1).getCelebrityDTO().getPageId();
                            celebrityUserDTOs.addAll(temp);
                        }
                    } while (dataSize >= 1000);
                    for (UserDTO dto : celebrityUserDTOs) {
                        FloatingMenuSlideItemDTO floatingMenuSlideItemDTO = new FloatingMenuSlideItemDTO();
                        floatingMenuSlideItemDTO.setFeatureType(featureType);
                        floatingMenuSlideItemDTO.setFeatureId(String.valueOf(dto.getCelebrityDTO().getPageId()));
                        if (floatingMenuSlideItemDTOs.containsKey(new Pair<>(floatingMenuSlideItemDTO.getFeatureType(), floatingMenuSlideItemDTO.getFeatureId()))) {
                            floatingMenuSlideItemDTO.setStatus(1);
                        } else {
                            floatingMenuSlideItemDTO.setStatus(0);
                        }
                        floatingMenuSlideItemDTO.setName(dto.getCelebrityDTO().getPageName());
                        floatingMenuSlideItemDTO.setProfileImageUrl(dto.getCelebrityDTO().getProfileImageUrl());

                        if (status != -1 && floatingMenuSlideItemDTO.getStatus() != status) {
                            continue;
                        }

                        listItems.add(floatingMenuSlideItemDTO);
                    }
//                    listItems.addAll(floatingMenuSlideItemDTOs);
                    break;
                case AppConstants.DYNAMIC_FLOATING_MENU.SPORTS:
                    channelList = CassandraDAO.getInstance().getVerifiedChannels();
                    for (ChannelDTO dto : channelList) {
                        List<ChannelCategoryDTO> channelCategoryDTOs = dto.getChannelCategory();
                        for (ChannelCategoryDTO channelCategoryDTO : channelCategoryDTOs) {
                            if (channelCategoryDTO.getCategory() != null && channelCategoryDTO.getCategory().equalsIgnoreCase("sports")) {
                                FloatingMenuSlideItemDTO floatingMenuSlideItemDTO = new FloatingMenuSlideItemDTO();
                                floatingMenuSlideItemDTO.setFeatureType(featureType);
                                floatingMenuSlideItemDTO.setFeatureId(String.valueOf(dto.getChannelId()));
                                if (floatingMenuSlideItemDTOs.containsKey(new Pair<>(floatingMenuSlideItemDTO.getFeatureType(), floatingMenuSlideItemDTO.getFeatureId()))) {
                                    floatingMenuSlideItemDTO.setStatus(1);
                                } else {
                                    floatingMenuSlideItemDTO.setStatus(0);
                                }
                                floatingMenuSlideItemDTO.setName(dto.getTitle());
                                floatingMenuSlideItemDTO.setProfileImageUrl(dto.getProfileImgUrl());

                                if (status != -1 && floatingMenuSlideItemDTO.getStatus() != status) {
                                    continue;
                                }

                                listItems.add(floatingMenuSlideItemDTO);
                            }
                        }
                    }
                    break;
                case AppConstants.DYNAMIC_FLOATING_MENU.MUSIC:
                    channelList = CassandraDAO.getInstance().getVerifiedChannels();
                    for (ChannelDTO dto : channelList) {
                        List<ChannelCategoryDTO> channelCategoryDTOs = dto.getChannelCategory();
                        for (ChannelCategoryDTO channelCategoryDTO : channelCategoryDTOs) {
                            if (channelCategoryDTO.getCategory() != null && channelCategoryDTO.getCategory().equalsIgnoreCase("MUSIC")) {
                                FloatingMenuSlideItemDTO floatingMenuSlideItemDTO = new FloatingMenuSlideItemDTO();
                                floatingMenuSlideItemDTO.setFeatureType(featureType);
                                floatingMenuSlideItemDTO.setFeatureId(String.valueOf(dto.getChannelId()));
                                if (floatingMenuSlideItemDTOs.containsKey(new Pair<>(floatingMenuSlideItemDTO.getFeatureType(), floatingMenuSlideItemDTO.getFeatureId()))) {
                                    floatingMenuSlideItemDTO.setStatus(1);
                                } else {
                                    floatingMenuSlideItemDTO.setStatus(0);
                                }
                                floatingMenuSlideItemDTO.setName(dto.getTitle());
                                floatingMenuSlideItemDTO.setProfileImageUrl(dto.getProfileImgUrl());

                                if (status != -1 && floatingMenuSlideItemDTO.getStatus() != status) {
                                    continue;
                                }

                                listItems.add(floatingMenuSlideItemDTO);
                            }
                        }
                    }
                    break;
                case AppConstants.DYNAMIC_FLOATING_MENU.MEDIA:
                    channelList = CassandraDAO.getInstance().getVerifiedChannels();
                    for (ChannelDTO dto : channelList) {
                        List<ChannelCategoryDTO> channelCategoryDTOs = dto.getChannelCategory();
                        for (ChannelCategoryDTO channelCategoryDTO : channelCategoryDTOs) {
                            if (channelCategoryDTO.getCategory() != null && channelCategoryDTO.getCategory().equalsIgnoreCase("MEDIA")) {
                                FloatingMenuSlideItemDTO floatingMenuSlideItemDTO = new FloatingMenuSlideItemDTO();
                                floatingMenuSlideItemDTO.setFeatureType(featureType);
                                floatingMenuSlideItemDTO.setFeatureId(String.valueOf(dto.getChannelId()));
                                if (floatingMenuSlideItemDTOs.containsKey(new Pair<>(floatingMenuSlideItemDTO.getFeatureType(), floatingMenuSlideItemDTO.getFeatureId()))) {
                                    floatingMenuSlideItemDTO.setStatus(1);
                                } else {
                                    floatingMenuSlideItemDTO.setStatus(0);
                                }
                                floatingMenuSlideItemDTO.setName(dto.getTitle());
                                floatingMenuSlideItemDTO.setProfileImageUrl(dto.getProfileImgUrl());

                                if (status != -1 && floatingMenuSlideItemDTO.getStatus() != status) {
                                    continue;
                                }

                                listItems.add(floatingMenuSlideItemDTO);
                            }
                        }
                    }
                    break;
            }
        } catch (Exception e) {
        }
        if (queryString != null && queryString.length() > 0) {
            String str = queryString.trim().toLowerCase();
            List<FloatingMenuSlideItemDTO> list = new ArrayList<>();
            for (FloatingMenuSlideItemDTO dto : listItems) {
                if (!dto.getName().toLowerCase().contains(str)) {
                    continue;
                }
                list.add(dto);
            }
            return list;
        }
        return listItems;
    }
}
