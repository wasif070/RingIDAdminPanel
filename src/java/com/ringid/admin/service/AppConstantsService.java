/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.service;

import com.ringid.admin.dto.AppConstantsDTO;
import com.ringid.admin.actions.appConstants.AppConstantsForm;
import com.ringid.admin.repository.AppConstantsLoader;
import com.ringid.admin.dao.AppConstantsDAO;
import com.ringid.admin.utils.MyAppError;
import java.util.ArrayList;

/**
 *
 * @author Kazi Nasir Uddin Oly
 * @Date Jun 14, 2016
 */
public class AppConstantsService {

    private AppConstantsService() {

    }

    private static class AppConstantsTaskSchedulerHolder {

        private static final AppConstantsService INSTANCE = new AppConstantsService();
    }

    public static AppConstantsService getInstance() {
        return AppConstantsTaskSchedulerHolder.INSTANCE;
    }

    public ArrayList<AppConstantsDTO> getAppConstantsList(AppConstantsDTO dto) {
        return AppConstantsLoader.getInstance().getAppConstantsList(dto);
    }

    public ArrayList<AppConstantsDTO> getMaxAppConstantsListInfo() {
        return AppConstantsLoader.getInstance().getMaxAppConstantsListInfo();
    }

    public MyAppError addAppConstantsInfo(AppConstantsDTO p_dto) {
        return AppConstantsDAO.getInstance().addAppConstantsInfo(p_dto);
    }

    public MyAppError addAppConstantsInfo(ArrayList<AppConstantsDTO> maps) {
        return AppConstantsDAO.getInstance().addAppConstantsInfo(maps);
    }

    public AppConstantsDTO getAppConstantsInfoById(int id) {
        return AppConstantsLoader.getInstance().getAppConstantsDTO(id);
    }

    public MyAppError deleteAppConstants(int id) {
        return AppConstantsDAO.getInstance().deleteAppConstants(id);
    }

    public MyAppError updateAppConstants(AppConstantsDTO p_dto) {
        return AppConstantsDAO.getInstance().updateAppConstants(p_dto);
    }

    public ArrayList<AppConstantsDTO> getAppConstantsListForDownload() {
        return AppConstantsLoader.getInstance().getAppConstantsListForDownload();
    }

    public AppConstantsDTO getAppConstantsDTO(AppConstantsForm appConstantsForm) {
        AppConstantsDTO appConstantsDTO = new AppConstantsDTO();

        if (!appConstantsForm.isNull(appConstantsForm.getPlatform())) {
            appConstantsDTO.setPlatform(Integer.valueOf(appConstantsForm.getPlatform()));
        }

        if (!appConstantsForm.isNull(appConstantsForm.getAppType())) {
            appConstantsDTO.setAppType(Integer.valueOf(appConstantsForm.getAppType()));
        }

        if (!appConstantsForm.isNull(appConstantsForm.getVersion())) {
            appConstantsDTO.setVersion(Integer.valueOf(appConstantsForm.getVersion()));
        }
        appConstantsDTO.setAuthController(appConstantsForm.getAuthController());
        appConstantsDTO.setDisplayDigits(8);
        appConstantsDTO.setImageResource(appConstantsForm.getImageResource());
        appConstantsDTO.setImageServer(appConstantsForm.getImageServer());
        appConstantsDTO.setMarketResource(appConstantsForm.getMarketResource());
        appConstantsDTO.setMarketServer(appConstantsForm.getMarketServer());
        appConstantsDTO.setOfficialUserId(1);
        appConstantsDTO.setPaymentGateway(appConstantsForm.getPaymentGateway());
        appConstantsDTO.setVodResource(appConstantsForm.getVodResource());
        appConstantsDTO.setVodServer(appConstantsForm.getVodServer());
        appConstantsDTO.setWeb(appConstantsForm.getWeb());
        appConstantsDTO.setConstantsVersion(0);
        
        return appConstantsDTO;
    }
}
