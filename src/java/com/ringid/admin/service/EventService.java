/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.service;

import com.google.gson.Gson;
import com.ringid.admin.cassandra.CassandraDAO;
import com.ringid.admin.actions.event.EventForm;
import com.ringid.admin.utils.log.RingLogger;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import org.apache.logging.log4j.Logger;
import org.ringid.event.EventDTO;

/**
 *
 * @author mamun
 */
public class EventService {

    static Logger logger = RingLogger.getConfigPortalLogger();
    private SimpleDateFormat dateFormat;

    private EventService() {
        dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
    }

    private static class EventTaskSchedulerHolder {

        private static final EventService INSTANCE = new EventService();
    }

    public static EventService getInstance() {
        return EventTaskSchedulerHolder.INSTANCE;
    }

    public int createEvent(EventDTO eventDTO) {
        int reasonCode = -1;
        try {
            reasonCode = CassandraDAO.getInstance().createEvent(eventDTO);
        } catch (Exception e) {
        }
        return reasonCode;
    }

    public int updateEvent(EventDTO eventDTO) {
        RingLogger.getConfigPortalLogger().debug("[updateEvent] eventDTO -->" + new Gson().toJson(eventDTO));
        int reasonCode = -1;
        try {
            reasonCode = CassandraDAO.getInstance().updateEvent(eventDTO);
        } catch (Exception e) {
        }
        return reasonCode;
    }

    public int deleteEvent(long eventId) {
        int reasonCode = -1;
        try {
            reasonCode = CassandraDAO.getInstance().deleteEvent(eventId);
        } catch (Exception e) {
        }
        return reasonCode;
    }

    public List<EventDTO> getPastEvents(long curTime, int start, int limit) {
        List<EventDTO> eventDTOs = new ArrayList<>();
        try {
            eventDTOs = CassandraDAO.getInstance().getPastEvents(curTime, start, limit);
        } catch (Exception e) {
        }
        return eventDTOs;
    }

    public List<EventDTO> getOngoingEvents(long curTime, int start, int limit) {
        List<EventDTO> eventDTOs = new ArrayList<>();
        try {
            eventDTOs = CassandraDAO.getInstance().getOngoingEvents(curTime, start, limit);
        } catch (Exception e) {
        }
        return eventDTOs;
    }

    public List<EventDTO> getUpcomingEvents(long curTime, int start, int limit) {
        List<EventDTO> eventDTOs = new ArrayList<>();
        try {
            eventDTOs = CassandraDAO.getInstance().getUpcomingEvents(curTime, start, limit);
        } catch (Exception e) {
        }
        return eventDTOs;
    }

    public EventDTO getEventDTO(EventForm eventForm) {

        EventDTO dto = new EventDTO();

        dto.setId(eventForm.getId());
        dto.setEventType(eventForm.getEventType());
        dto.setOwnerId(eventForm.getOwnerId());
        dto.setOwnerType(eventForm.getOwnerType());
        if (eventForm.getName() != null) {
            dto.setName(eventForm.getName());
        }
        if (eventForm.getProfileImageUrl() != null && eventForm.getProfileImageUrl().length() > 0) {
            dto.setProfileImage(eventForm.getProfileImageUrl());
        }
        if (eventForm.getCoverImageUrl() != null && eventForm.getCoverImageUrl().length() > 0) {
            dto.setCoverImage(eventForm.getCoverImageUrl());
        }
        if (eventForm.getStartDate() != null && eventForm.getStartDate().length() > 0) {
            dto.setStartDate(eventForm.getStartDate());
            try {
                dto.setStartTimeLong(dateFormat.parse(eventForm.getStartDate()).getTime());
            } catch (ParseException ex) {
            }
        }
        if (eventForm.getStartTime() != null && eventForm.getStartTime().length() > 0) {
            dto.setStartTime(eventForm.getStartTime());
        }
        if (eventForm.getEndDate() != null && eventForm.getEndDate().length() > 0) {
            dto.setEndDate(eventForm.getEndDate());
            try {
                dto.setEndTimeLong(dateFormat.parse(eventForm.getEndDate()).getTime());
            } catch (ParseException ex) {
            }
        }
        if (eventForm.getEndTime() != null && eventForm.getEndTime().length() > 0) {
            dto.setEndTime(eventForm.getEndTime());
        }
        if (eventForm.getData() != null) {
            dto.setData(eventForm.getData());
        }
        return dto;
    }
}
