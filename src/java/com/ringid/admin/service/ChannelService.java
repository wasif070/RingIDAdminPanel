/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.service;

import com.google.gson.Gson;
import com.ringid.admin.cassandra.CassandraDAO;
import com.ringid.admin.dto.ChannelVerificationDTO;
import com.ringid.admin.actions.channel.channelCategory.ChannelCategoryForm;
import com.ringid.admin.actions.channel.channelSubscription.ChannelSubscriptionForm;
import com.ringid.admin.repository.ChannelSubscriptionLoader;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.repository.ChannelCategoryLoader;
import com.ringid.admin.repository.ChannelRepository;
import com.ringid.admin.utils.Constants;
import com.ringid.admin.utils.ReasonCode;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.ringid.channel.ChannelCategoryDTO;
import org.ringid.channel.ChannelMediaDTO;
import org.ringid.channel.ChannelSubscriptionDTO;

/**
 *
 * @author Rabby
 */
public class ChannelService {

    private static ChannelService instance = null;

    private ChannelService() {
    }

    public static ChannelService getInstance() {
        if (instance == null) {
            createInstance();
        }
        return instance;
    }

    private synchronized static void createInstance() {
        if (instance == null) {
            instance = new ChannelService();
        }
    }
    
    public ChannelCategoryDTO getChannelCategory(ChannelCategoryForm channelCategoryForm) {
        ChannelCategoryDTO dto = new ChannelCategoryDTO();
        dto.setCategoryId(channelCategoryForm.getCategoryId());
        dto.setCategory(channelCategoryForm.getCategory());
        dto.setBanner(channelCategoryForm.getBanner());
        dto.setDescription(channelCategoryForm.getDescription());
        dto.setVerticalWeight(channelCategoryForm.getVerticalWeight());
        dto.setHorizontalWeight(channelCategoryForm.getHorizontalWeight());

        return dto;
    }

    public int addChannelCategory(ChannelCategoryDTO categoryDTO) {
        int reasonCode = -1;
        try {
            reasonCode = CassandraDAO.getInstance().addChannelCategory(categoryDTO);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [addChannelCategory] --> " + e);
        }
        return reasonCode;
    }

    public int updateChannelCategory(ChannelCategoryDTO categoryDTO) {
        int reasonCode = -1;
        try {
            reasonCode = CassandraDAO.getInstance().updateChannelCategory(categoryDTO);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [updateChannelCategory] --> " + e);
        }
        return reasonCode;
    }

    public int deleteChannelCategory(int categoryId) {
        int reasonCode = -1;
        try {
            reasonCode = CassandraDAO.getInstance().deleteChannelCategory(categoryId);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [deleteChannelCategory] --> " + e);
        }
        return reasonCode;
    }

    public List<ChannelCategoryDTO> getChannelCategoryList(ChannelVerificationDTO dto) {
        return ChannelCategoryLoader.getInstance().getChannelCategoryList(dto);
    }

    public ChannelCategoryDTO getChannelCategoryDTOById(int channelCategoryId) {
        return ChannelCategoryLoader.getInstance().getChannelCategoryDTOById(channelCategoryId);
    }

    public int updateIsChannelSharable(UUID channelId, boolean isChannelSharable) {
        int reasonCode = -1;
        try {
            reasonCode = CassandraDAO.getInstance().updateIsChannelSharable(channelId, isChannelSharable);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("[ChannelService]Exception in [updateIsChannelSharable] --> " + e.getMessage());
        }
        return reasonCode;
    }

    public int verifyChannelOfficially(ArrayList<UUID> channelIds) {
        int reasonCode = 0;
        try {
            int[] response = new int[channelIds.size()];
            int j = 0;
            for (UUID channelId : channelIds) {
                response[j++] = CassandraDAO.getInstance().verifyChannelOfficially(channelId);
            }
            for (int i = 0; i < response.length; i++) {
                if (response[i] != ReasonCode.NONE) {
                    reasonCode = response[i];
                    break;
                }
            }
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().info("##Exception verifyChannelOfficially.. " + e);
            reasonCode = ReasonCode.EXCEPTION_OCCURED;
        }
        return reasonCode;
    }

    public int unverifyChannelOfficially(ArrayList<UUID> channelIds) {
        int reasonCode = 0;
        try {
            int[] response = new int[channelIds.size()];
            int j = 0;
            for (UUID channelId : channelIds) {
                response[j++] = CassandraDAO.getInstance().unverifyChannelOfficially(channelId);
            }
            for (int i = 0; i < response.length; i++) {
                if (response[i] != ReasonCode.NONE) {
                    reasonCode = response[i];
                    break;
                }
            }
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().info("##Exception unverifyChannelOfficially.. " + e);
            reasonCode = ReasonCode.EXCEPTION_OCCURED;
        }
        return reasonCode;
    }

    public int changeChannelOwnership(UUID channelId, long oldUserId, long newUserId) {
        int reasonCode = -1;
        try {
            reasonCode = CassandraDAO.getInstance().changeChannelOwnership(channelId, oldUserId, newUserId);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [changeChannelOwnership] --> " + e);
        }
        return reasonCode;
    }

    public int updateChannelServerIpAndPort(UUID channelId, String oldServerIp, int oldPort, String newServerIp, int newPort) {
        int reasonCode = -1;
        try {
            reasonCode = CassandraDAO.getInstance().updateChannelServerIpAndPort(channelId, oldServerIp, oldPort, newServerIp, newPort);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [updateChannelServerIpAndPort] --> " + e);
        }
        return reasonCode;
    }

    public int updateStreamingServerIpAndPort(UUID channelId, String currentStreamServerIp, int currentPort, String newStreamingServerIp, int newPort) {
        int reasonCode = -1;
        try {
            reasonCode = CassandraDAO.getInstance().updateStreamingServerIpAndPort(channelId, currentStreamServerIp, currentPort, newStreamingServerIp, newPort);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [updateStreamingServerIpAndPort] --> " + e);
        }
        return reasonCode;
    }

    public int updateChannelBooster(UUID channelId, long channelBoster) {
        int reasonCode = -1;
        try {
            reasonCode = CassandraDAO.getInstance().updateChannelBooster(channelId, channelBoster);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [updateChannelBooster] --> " + e);
        }
        return reasonCode;
    }

    public ArrayList<ChannelVerificationDTO> getChannelVerificationList(ChannelVerificationDTO sdto) {
        return ChannelRepository.getInstance().getChannelVerificationList(sdto);
    }

    public ChannelVerificationDTO getChannelVerificationDTO(UUID id, int channelType) {
        return ChannelRepository.getInstance().getChannelVerificationDTO(id, channelType);
    }

    public int verifyChannel(ArrayList<UUID> channelIds) {
        int reasonCode = 0;
        try {
            int[] response = new int[channelIds.size()];
            int j = 0;
            for (UUID channelId : channelIds) {
                response[j++] = CassandraDAO.getInstance().verifyChannel(channelId);
            }
            for (int i = 0; i < response.length; i++) {
                if (response[i] != ReasonCode.NONE) {
                    reasonCode = response[i];
                    break;
                }
            }
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().info("##Exception verifyChannel.. " + e);
            reasonCode = ReasonCode.EXCEPTION_OCCURED;
        }
        return reasonCode;
    }

    public int unverifyChannel(ArrayList<UUID> channelIds) {
        int reasonCode = 0;
        try {
            int[] response = new int[channelIds.size()];
            int j = 0;
            for (UUID channelId : channelIds) {
                response[j++] = CassandraDAO.getInstance().unverifyChannel(channelId);
            }
            for (int i = 0; i < response.length; i++) {
                if (response[i] != ReasonCode.NONE) {
                    reasonCode = response[i];
                    break;
                }
            }
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().info("##Exception unverifyChannel.. " + e);
            reasonCode = ReasonCode.EXCEPTION_OCCURED;
        }
        return reasonCode;
    }

    public int deleteChannel(ArrayList<UUID> channelIds) {
        int reasonCode = 0;
        try {
            int[] response = new int[channelIds.size()];
            int j = 0;
            for (UUID channelId : channelIds) {
                response[j++] = CassandraDAO.getInstance().deleteChannel(channelId);
            }
            for (int i = 0; i < response.length; i++) {
                if (response[i] != ReasonCode.NONE) {
                    reasonCode = response[i];
                    break;
                }
            }
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().info("##Exception deleteChannel.. " + e);
            reasonCode = ReasonCode.EXCEPTION_OCCURED;
        }
        return reasonCode;
    }

    public ChannelMediaDTO getChannelMediaDetails(UUID mediaId) {
        ChannelMediaDTO channelMediaDTO = null;
        try {
            channelMediaDTO = CassandraDAO.getInstance().getChannelMediaDetails(mediaId);
            RingLogger.getConfigPortalLogger().debug("channelMediaDTO --> " + new Gson().toJson(channelMediaDTO));
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().debug("Exception --> " + e);
        }
        return channelMediaDTO;
    }

    public List<ChannelMediaDTO> getChannelMediaList(UUID channelId) {
        List<ChannelMediaDTO> channelMediaList = new ArrayList<>();
        try {
            channelMediaList = CassandraDAO.getInstance().getUniquePublishedMedia(channelId);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().debug("Exception --> " + e);
        }
        RingLogger.getConfigPortalLogger().debug("[getChannelMediaList] channelMediaList.size() --> " + channelMediaList.size());
        return channelMediaList;
    }

    public int makeChannelFeatured(ArrayList<UUID> channelIds) {
        int reasonCode = 0;
        try {
            int[] response = new int[channelIds.size()];
            int j = 0;
            for (UUID channelId : channelIds) {
                response[j++] = CassandraDAO.getInstance().makeChannelFeatured(channelId);
            }
            for (int i = 0; i < response.length; i++) {
                if (response[i] != ReasonCode.NONE) {
                    reasonCode = response[i];
                    break;
                }
            }
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().info("##Exception verifyChannel.. " + e);
            reasonCode = ReasonCode.EXCEPTION_OCCURED;
        }
        return reasonCode;
    }

    public ChannelSubscriptionDTO getChannelSubscriptionDTO(ChannelSubscriptionForm channelSubscriptionForm) {
        ChannelSubscriptionDTO channelSubscriptionDTO = new ChannelSubscriptionDTO();
        switch (channelSubscriptionForm.getSubTypeName()) {
            case "DAILY": {
                channelSubscriptionDTO.setSubscriptionDuration(Constants.DAILY);
                break;
            }
            case "WEEKLY": {
                channelSubscriptionDTO.setSubscriptionDuration(Constants.WEEKLY);
                break;
            }
            case "MONTHLY": {
                channelSubscriptionDTO.setSubscriptionDuration(Constants.MONTHLY);
                break;
            }
            case "QUARTERLY": {
                channelSubscriptionDTO.setSubscriptionDuration(Constants.QUARTERLY);
                break;
            }
            case "HALF YEARLY": {
                channelSubscriptionDTO.setSubscriptionDuration(Constants.HALF_YEARLY);
                break;
            }
            case "YEARLY": {
                channelSubscriptionDTO.setSubscriptionDuration(Constants.YEARLY);
                break;
            }
        }
        channelSubscriptionDTO.setSubscriptionTypeName(channelSubscriptionForm.getSubTypeName());
        channelSubscriptionDTO.setSubscriptionTypeId(channelSubscriptionForm.getSubTypeId());
        channelSubscriptionDTO.setSubscriptionFee(channelSubscriptionForm.getSubFee());
        channelSubscriptionDTO.setIsDefault(channelSubscriptionForm.getIsDefault());
        return channelSubscriptionDTO;
    }

    public int addChannelSubscriptionType(ChannelSubscriptionDTO channelSubscriptionDTO) {
        int reasonCode = -1;
        try {
            reasonCode = CassandraDAO.getInstance().addChannelSubscriptionType(channelSubscriptionDTO);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [addChannelSubscriptionType] --> " + e);
        }
        return reasonCode;
    }

    public int deleteChannelSubscriptionType(int subTypeId) {
        int reasonCode = -1;
        try {
            reasonCode = CassandraDAO.getInstance().deleteChannelSubscriptionType(subTypeId);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [deleteChannelSubscriptionType] --> " + e);
        }
        return reasonCode;
    }

    public List<ChannelSubscriptionDTO> getChannelSubscriptionTypeList() {
        return ChannelSubscriptionLoader.getInstance().getChannelSubscriptionList();
    }

    public List<ChannelVerificationDTO> getChannelVerificationDTOList(List<UUID> channelIds) {
        return ChannelRepository.getInstance().getChannelVerificationDTOList(channelIds);
    }
}
