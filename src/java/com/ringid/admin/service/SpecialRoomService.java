/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.service;

import com.ringid.admin.dao.LiveStreamingDAO;
import com.ringid.admin.dto.SpecialRoomDTO;
import com.ringid.admin.utils.MyAppError;
import java.util.List;

/**
 *
 * @author mamun
 */
public class SpecialRoomService {

    private SpecialRoomService() {

    }

    private static class SpecialRoomTaskSchedulerHolder {

        private static final SpecialRoomService INSTANCE = new SpecialRoomService();
    }

    public static SpecialRoomService getInstance() {
        return SpecialRoomTaskSchedulerHolder.INSTANCE;
    }

    public List<SpecialRoomDTO> getSpecialRoomList(String searchText) {
        return LiveStreamingDAO.getInstance().getSpecialRoomList(searchText);
    }

    public SpecialRoomDTO getSpecialRoomByRoomId(long roomId) {
        return LiveStreamingDAO.getInstance().getSpecialRoomByRoomId(roomId);
    }

    public MyAppError addSpecialRoom(SpecialRoomDTO dto) {
        return LiveStreamingDAO.getInstance().addSpecialRoom(dto);
    }

    public MyAppError updatepecialRoom(SpecialRoomDTO dto) {
        return LiveStreamingDAO.getInstance().updateSpecialRoom(dto);
    }

    public MyAppError deleteSpecialRoom(long roomId) {
        return LiveStreamingDAO.getInstance().deleteSpecialRoom(roomId);
    }
}
