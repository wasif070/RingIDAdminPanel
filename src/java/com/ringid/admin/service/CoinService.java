/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.service;

import com.ipvision.ringid.storage.coin.dto.CoinLogDTO;
import com.ringid.admin.dto.PermittedCountryDTO;
import com.ringid.admin.repository.PermittedCountryLoader;
import com.ringid.admin.cassandra.CassandraDAO;
import com.ringid.admin.dto.PaymentMethodDTO;
import com.ringid.admin.actions.coinExchange.PaymentMethodForm;
import com.ringid.admin.repository.PaymentMethodLoader;
import com.ringid.admin.dao.CoinDAO;
import com.ringid.admin.utils.Constants;
import com.ringid.admin.utils.MyAppError;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.json.JSONException;
import org.json.JSONObject;
import org.ringid.utilities.AppConstants;
import ringid.sessionless.SessionlessTaskseheduler;

/**
 *
 * @author Rabby
 */
public class CoinService {

    private static CoinService instance = null;

    private CoinService() {
    }

    public static CoinService getInstance() {
        if (instance == null) {
            createInstance();
        }
        return instance;
    }

    private synchronized static void createInstance() {
        if (instance == null) {
            instance = new CoinService();
        }
    }

    public PaymentMethodDTO getPaymentMethodDTOById(long id) {
        return PaymentMethodLoader.getInstance().getPaymentMethodDTOById(id);
    }

    public List<PaymentMethodDTO> getPaymentMethodList(PaymentMethodDTO dto) {
        return PaymentMethodLoader.getInstance().getPaymentMethodList(dto);
    }

    public PaymentMethodDTO getPaymentMethodDTO(PaymentMethodForm paymentMethodForm) {
        PaymentMethodDTO dto = new PaymentMethodDTO();
        dto.setId(paymentMethodForm.getId());
        dto.setIsOpen(paymentMethodForm.isIsOpen());
        if (paymentMethodForm.getName() != null && paymentMethodForm.getName().length() > 0) {
            dto.setName(paymentMethodForm.getName());
        }
        return dto;
    }

    public MyAppError addPaymentMethod(PaymentMethodDTO dto) {
        return CoinDAO.getInstance().addPaymentMethod(dto);
    }

    public MyAppError updatePaymentMethod(PaymentMethodDTO dto) {
        return CoinDAO.getInstance().updatePaymentMethod(dto);
    }

    public MyAppError deletePaymentMethod(long id) {
        return CoinDAO.getInstance().deletePaymentMethodById(id);
    }

    public ArrayList<PermittedCountryDTO> getPermittedCountryList(String mobilePhoneDialingCode, int status, String queryString) {
        return PermittedCountryLoader.getInstance().getPermittedCountryList(mobilePhoneDialingCode, status, queryString);
    }

    public MyAppError updateRewardPermittedCountryInfo(PermittedCountryDTO dto) {
        return CoinDAO.getInstance().updateRewardPermittedCountryInfo(dto);
    }

    public PermittedCountryDTO getPermittedCountryInfoByCountryShortCode(String countryShortCode) {
        return PermittedCountryLoader.getInstance().getPermittedCountryInfoByCountryShortCode(countryShortCode);
    }

    public MyAppError deleteRewardPermittedCountryInfo(String countryShortCode) {
        return CoinDAO.getInstance().deleteRewardPermittedCountryInfo(countryShortCode);
    }

    public List<CoinLogDTO> getCoinTransaction(long userTableId, UUID pivotId, boolean isGift, int limit) {
        List<CoinLogDTO> coinLogDTOs = null;
        try {
            coinLogDTOs = CassandraDAO.getInstance().getCoinTransaction(userTableId, pivotId, isGift, AppConstants.SCROLL_DOWN, limit);
        } catch (Exception ex) {
        }
        return coinLogDTOs;
    }

    public String getWalletInfo(long userTableId) {
        String walletInfo = SessionlessTaskseheduler.getInstance().getWalletInfo(Constants.USERID, userTableId);
        JSONObject response = new JSONObject();
        try {
            JSONObject jSONObject = new JSONObject(walletInfo);
            if (jSONObject.has("info")) {
                JSONObject info = jSONObject.getJSONObject("info");
                response.put("totalCoin", info.getLong("totalCoin"));
                response.put("giftCoin", info.getLong("giftCoin"));
                response.put("myCoin", info.getLong("myCoin"));
            }
            if (jSONObject.has("blockedEarnedCoin")) {
                response.put("blockedEarnedCoin", jSONObject.getLong("blockedEarnedCoin"));
            }
            if (jSONObject.has("blockedGiftCoin")) {
                response.put("blockedGiftCoin", jSONObject.getLong("blockedGiftCoin"));
            }
        } catch (JSONException e) {

        }
        return response.toString();
    }
}
