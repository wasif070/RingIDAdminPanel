/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.service;

import com.ringid.admin.dto.MediaDTO;
import com.ringid.admin.repository.MediaLoader;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.ringid.newsfeeds.CassMultiMediaDTO;

/**
 *
 * @author Kazi Nasir Uddin Oly
 * @Date Dec 10, 2016
 */
public class MediaService {

    private static MediaService mediaTaskScheduler;

    private MediaService() {

    }

    public static MediaService getInstance() {
        if (mediaTaskScheduler == null) {
            createScheduler();
        }
        return mediaTaskScheduler;
    }

    private synchronized static void createScheduler() {
        if (mediaTaskScheduler == null) {
            mediaTaskScheduler = new MediaService();
        }
    }

    public List<CassMultiMediaDTO> getRecentMedias(MediaDTO dto) {
        return MediaLoader.getInstance().getRecentMedias(dto);
    }

    public List<CassMultiMediaDTO> getTrendingMedias(MediaDTO dto) {
        return MediaLoader.getInstance().getTrendingMedia(dto);
    }

    public boolean makeTrending(ArrayList<UUID> mediaIds) throws Exception {
        return MediaLoader.getInstance().makeTrending(mediaIds);
    }

    public boolean removeTrending(List<CassMultiMediaDTO> mediaDTos) throws Exception {
        return MediaLoader.getInstance().removeTrending(mediaDTos);
    }

    public CassMultiMediaDTO getMediaDetails(int index) {
        return MediaLoader.getInstance().getMediaDetails(index);
    }

    public CassMultiMediaDTO getTrendingMediaDetails(int index) {
        return MediaLoader.getInstance().getTrendingMediaDetails(index);
    }

    public List<CassMultiMediaDTO> getTrendingMultiMediaDTOsFromId(ArrayList<UUID> mediaIds) {
        return MediaLoader.getInstance().getTrendingMutliMediaDTOsFromId(mediaIds);
    }

    public List<CassMultiMediaDTO> getRecentMediasByRingId(Long ringId) {
        return MediaLoader.getInstance().getRecentMediasByRingId(ringId);
    }

    public List<CassMultiMediaDTO> getTrendingMediasByRingId(Long ringId) {
        return MediaLoader.getInstance().getTrendingMediasByRingId(ringId);
    }

}
