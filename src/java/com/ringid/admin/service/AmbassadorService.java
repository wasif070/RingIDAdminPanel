/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.service;

import com.ringid.admin.repository.AmbassadorLoader;
import com.ringid.admin.cassandra.CassandraDAO;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.repository.ProductPageLoader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.ringid.pages.PageDTO;
import org.ringid.pages.ServiceDTO;
import org.ringid.users.UserBasicInfoDTO;

/**
 *
 * @author mamun
 */
public class AmbassadorService {

    private AmbassadorService() {

    }

    private static class AmbassadorTaskSchedulerHolder {

        private static final AmbassadorService INSTANCE = new AmbassadorService();
    }

    public static AmbassadorService getInstance() {
        return AmbassadorTaskSchedulerHolder.INSTANCE;
    }

    public List<Long> addAmbassador(long productPageId, List<Long> ambassadorList) {
        List<Long> unsuccessfulList = new ArrayList<>();
        try {
            unsuccessfulList = CassandraDAO.getInstance().addAmbassador(productPageId, ambassadorList);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().debug("Exception in [addAmbassador] -> " + e);
        }
        return unsuccessfulList;
    }

    public List<Long> removeAmbassador(long productPageId, List<Long> ambassadorList) {
        List<Long> unsuccessfulList = new ArrayList<>();
        try {
            unsuccessfulList = CassandraDAO.getInstance().removeAmbassador(productPageId, ambassadorList);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().debug("Exception in [removeAmbassador] -> " + e);
        }
        return unsuccessfulList;
    }

    public List<PageDTO> getAmbassadorsOfProductPage(long prodcutPageId) {
        List<PageDTO> ambassadorListPageDTOs = new ArrayList<>();
        try {
            ambassadorListPageDTOs = AmbassadorLoader.getInstance().getAmbassadorsOfProductPage(prodcutPageId);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().debug("Exception in [getAmbassadorsOfProductPage] -> " + e);
        }
        return ambassadorListPageDTOs;
    }

    public List<Long> getAllProductPageIds() {
        List<Long> productPageList = new ArrayList<>();
        try {
            productPageList = ProductPageLoader.getInstance().getAllProductPageIds();
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().debug("Exception in [getAllProductPageIds] -> " + e);
        }
        return productPageList;
    }

    public List<PageDTO> getAllProductPageDTOs() {
        List<PageDTO> productPageDTOList = new ArrayList<>();
        try {
            productPageDTOList = ProductPageLoader.getInstance().getAllProductPageDTOs();
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().debug("Exception in [getAllProductPageDTOs] -> " + e);
        }
        return productPageDTOList;
    }

    public UserBasicInfoDTO getUserBasicInfo(String searchText) {
        UserBasicInfoDTO userBasicInfoDTO = null;
        try {
            long userId = Long.parseLong(searchText);
            long tempId = CassandraDAO.getInstance().getUserTableId(userId);
            if (tempId > 0) {
                userId = tempId;
            }
            userBasicInfoDTO = CassandraDAO.getInstance().getUserBasicInfo(userId);
        } catch (Exception ex) {

        }
        return userBasicInfoDTO;
    }

    public List<ServiceDTO> getUserPageDTOs(long userId) {
        List<ServiceDTO> userPageDTOs = new ArrayList<>();
        try {
            HashMap<Integer, List<ServiceDTO>> pageMap = CassandraDAO.getInstance().getPageMap(userId);
            for (Map.Entry<Integer, List<ServiceDTO>> entry : pageMap.entrySet()) {
                userPageDTOs.addAll(entry.getValue());
            }
        } catch (Exception ex) {

        }
        return userPageDTOs;
    }
}
