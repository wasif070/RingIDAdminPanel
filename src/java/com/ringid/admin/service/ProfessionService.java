/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.service;

import com.ringid.admin.repository.ProfessionLoader;
import com.ringid.admin.cassandra.CassandraDAO;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.dto.ProfessionDTO;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author mamun
 */
public class ProfessionService {

    private static final Logger logger = RingLogger.getConfigPortalLogger();

    private ProfessionService() {
    }

    private static class ProfessionTaskSchedulerHolder {

        private static final ProfessionService INSTANCE = new ProfessionService();
    }

    public static synchronized ProfessionService getInstance() {
        return ProfessionTaskSchedulerHolder.INSTANCE;
    }

    public List<String> addProfessionData(List<String> professionData) {
        List<String> unsuccessfullProfession = new ArrayList<>();
        try {
            unsuccessfullProfession = CassandraDAO.getInstance().addProfessionData(professionData);
        } catch (Exception e) {
            logger.error("Exception in [updateTopLiveRoomPage] --> " + e);
        }
        return unsuccessfullProfession;
    }

    public List<ProfessionDTO> getProfessionList(ProfessionDTO dto) {
        List<ProfessionDTO> professionDTOs = new ArrayList<>();
        try {
            professionDTOs = ProfessionLoader.getInstance().getProfessionList(dto);
        } catch (Exception e) {
            logger.error("Exception in [getProfessionData] --> " + e);
        }
        return professionDTOs;
    }

    public int deleteProfession(UUID professionId) {
        int reasonCode = -1;
        try {
            reasonCode = CassandraDAO.getInstance().deleteProfession(professionId);
        } catch (Exception e) {
            logger.error("Exception in [updateTopLiveRoomPage] --> " + e);
        }
        return reasonCode;
    }
}
