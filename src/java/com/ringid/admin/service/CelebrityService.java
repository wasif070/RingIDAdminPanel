/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.service;

import com.ringid.admin.cassandra.CassandraDAO;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.utils.Constants;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import org.ringid.celebrities.CelebrityCategoryDTO;
import org.ringid.users.UserDTO;

/**
 *
 * @author Rabby
 */
public class CelebrityService {

    private static CelebrityService instance = null;

    private CelebrityService() {
    }

    public static CelebrityService getInstance() {
        if (instance == null) {
            createInstance();
        }
        return instance;
    }

    private synchronized static void createInstance() {
        if (instance == null) {
            instance = new CelebrityService();
        }
    }

    public List<CelebrityCategoryDTO> getCelebrityCategoriesList() {
        List<CelebrityCategoryDTO> list = new ArrayList<>();
        try {
            list = CassandraDAO.getInstance().getCelebrityCategoryList();
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Throwable [getCelebrityCategories] --> ", e);
        }
        return list;
    }

    public List<String> getCategoryListOfAParticularCelebrity(long ringId) {
        List<String> list = new ArrayList<>();
        try {
            list = CassandraDAO.getInstance().getCategoryListOfAParticularCelebrity(ringId);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Throwable [getCategoryListOfAParticularCelebrity] --> ", e);
        }
        return list;
    }

    public boolean addCelebrityCategory(long ringId, HashMap<String, Integer> categoryMap) {
        boolean result = false;
        try {
            result = CassandraDAO.getInstance().addCelebrityCategory(ringId, categoryMap);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Throwable [addCelebrityCategory] --> ", e);
        }
        return result;
    }

    public int updateCelebrityCategory(long ringId, HashMap<String, Integer> categoryMap) {
        int reasonCode = -1;
        try {
            boolean result = CassandraDAO.getInstance().updateCelebrityCategory(ringId, categoryMap);
            if (result == true) {
                reasonCode = 0;
            }
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Throwable [updateCelebrityCategory] --> ", e);
        }
        return reasonCode;
    }

    public Integer getCelebrityMinGiftValue(long celebrityPageId) {
        Integer celebrityMinGiftValue = null;
        try {
            celebrityMinGiftValue = CassandraDAO.getInstance().getCelebrityMinGiftValue(celebrityPageId);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [setCelebrityMinGiftValue] --> " + e);
        }
        return celebrityMinGiftValue;
    }

    public int setCelebrityMinGiftValue(long celebrityPageId, int giftValue) {
        int reasonCode = -1;
        try {
            reasonCode = CassandraDAO.getInstance().setCelebrityMinGiftValue(celebrityPageId, giftValue);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [setCelebrityMinGiftValue] --> " + e);
        }
        return reasonCode;
    }

    public UserDTO getCelebrityByPageId(long id) {
        UserDTO userDTO;
        try {
            userDTO = CassandraDAO.getInstance().getCelebrityByPageId(id);
        } catch (Exception e) {
            userDTO = new UserDTO();
            RingLogger.getConfigPortalLogger().error("Exception [getCelebrityByPageId] --> ", e);
        }
        return userDTO;
    }

    public List<UserDTO> getCelebrityList(String searchText, int column, int sort) {
        List<UserDTO> celebrityUserDTOs = new ArrayList<>();
        try {
            long pivot = 0;
            int limit = 1000, dataSize = 0;
            List<UserDTO> list = new ArrayList<>();
            do {
                List<UserDTO> temp = CassandraDAO.getInstance().getCelebrityList(pivot, limit);
                dataSize = temp.size();
                if (dataSize > 0) {
                    list.addAll(temp);
                    pivot = temp.get(dataSize - 1).getUserTableID();
                }
            } while (dataSize == limit);
            list
                    .stream()
                    .filter(dto -> {
                        if (searchText != null && searchText.length() > 0) {
                            if (!(dto.getCelebrityDTO().getPageName().toLowerCase().contains(searchText.trim().toLowerCase())
                                    || String.valueOf(dto.getCelebrityDTO().getPageRingId()).toLowerCase().contains(searchText.trim().toLowerCase())
                                    || String.valueOf(dto.getCelebrityDTO().getPageOwner()).toLowerCase().contains(searchText.trim().toLowerCase())
                                    || (dto.getCelebrityDTO().getCountry() != null && dto.getCelebrityDTO().getCountry().toLowerCase().contains(searchText.trim().toLowerCase())))) {
                                return false;
                            }
                        }
                        return true;
                    })
                    .forEach(dto -> {
                        celebrityUserDTOs.add(dto);
                    });
            switch (column) {
                case Constants.COLUMN_ONE:
                    Collections.sort(celebrityUserDTOs, (sort == Constants.ASC_SORT ? new Comparator<UserDTO>() {
                        @Override
                        public int compare(UserDTO o1, UserDTO o2) {
                            return Long.compare(o1.getCelebrityDTO().getPageId(), o2.getCelebrityDTO().getPageId());
                        }
                    }
                            : new Comparator<UserDTO>() {
                        @Override
                        public int compare(UserDTO o1, UserDTO o2) {
                            return Long.compare(o2.getCelebrityDTO().getPageId(), o1.getCelebrityDTO().getPageId());
                        }
                    }));
                    break;
                case Constants.COLUMN_TWO:
                    Collections.sort(celebrityUserDTOs, (sort == Constants.ASC_SORT ? new Comparator<UserDTO>() {
                        @Override
                        public int compare(UserDTO o1, UserDTO o2) {
                            return o1.getCelebrityDTO().getPageName().compareToIgnoreCase(o2.getCelebrityDTO().getPageName());
                        }
                    }
                            : new Comparator<UserDTO>() {
                        @Override
                        public int compare(UserDTO o1, UserDTO o2) {
                            return o2.getCelebrityDTO().getPageName().compareToIgnoreCase(o1.getCelebrityDTO().getPageName());
                        }
                    }));
                    break;
                default:
                    Collections.sort(celebrityUserDTOs, (sort == Constants.ASC_SORT ? new Comparator<UserDTO>() {
                        @Override
                        public int compare(UserDTO o1, UserDTO o2) {
                            return Long.compare(o1.getCelebrityDTO().getPageId(), o2.getCelebrityDTO().getPageId());
                        }
                    }
                            : new Comparator<UserDTO>() {
                        @Override
                        public int compare(UserDTO o1, UserDTO o2) {
                            return Long.compare(o2.getCelebrityDTO().getPageId(), o1.getCelebrityDTO().getPageId());
                        }
                    }));
                    break;
            }
            RingLogger.getConfigPortalLogger().debug("[getCelebrityList] userDTOs size --> " + celebrityUserDTOs.size());
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception [getCelebrityList] --> ", e);
        }
        return celebrityUserDTOs;
    }

    public int deleteCelebrityPage(long pageId, boolean disableAutoFollow) {
        int responseCode = -1;
        try {
            responseCode = CassandraDAO.getInstance().revokeCelebrityUserPageStatus(pageId, disableAutoFollow);
            if (responseCode == 0) {
                PageService.getInstance().destroyPageOwnerSession(pageId);
            }
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception [deleteCelebrityPage] --> ", e);
        }
        return responseCode;
    }

    public int makeUserPagesCelebrity(String[] pageIds) {
        int responseCode = -1;
        try {
            if (pageIds != null) {
                for (String pageId : pageIds) {
                    responseCode = makePageCelebrity(Long.parseLong(pageId));
                }
            }
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception [makeUserCelebrity] --> ", e);
        }
        return responseCode;
    }

    public int makePageCelebrity(long pageId) {
        int responseCode = -1;
        try {
            responseCode = CassandraDAO.getInstance().makeUserPageCelebrity(pageId, null);
            if (responseCode == 0) {
                PageService.getInstance().destroyPageOwnerSession(pageId);
            }
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception [makeCelebrity] --> ", e);
        }
        return responseCode;
    }
}
