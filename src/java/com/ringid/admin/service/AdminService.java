/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.service;

import com.ringid.admin.actions.appUpdateUrl.AppUpdateUrlForm;
import com.ringid.admin.cassandra.CassandraDAO;
import com.ringid.admin.dao.AdminDAO;
import com.ringid.admin.dto.AppUpdateUrlDTO;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.utils.MyAppError;
import java.util.ArrayList;
import java.util.List;
import org.ringid.users.DeactiveReasonDTO;
import org.ringid.utilities.ObjectReturnDTO;

/**
 *
 * @author Rabby
 */
public class AdminService {

    private static AdminService instance = null;

    private AdminService() {
    }

    public static AdminService getInstance() {
        if (instance == null) {
            createInstance();
        }
        return instance;
    }

    private synchronized static void createInstance() {
        if (instance == null) {
            instance = new AdminService();
        }
    }

    public List<AppUpdateUrlDTO> getAppUpdateUrlListInfo() {
        return AdminDAO.getInstance().getAppUpdateUrlListInfo();
    }

    public MyAppError addAppUpdateUrlInfo(AppUpdateUrlDTO dto) {
        return AdminDAO.getInstance().addAppUpdateUrlInfo(dto);
    }

    public MyAppError updateAppUpdateUrlInfo(int id, String updateUrl) {
        return AdminDAO.getInstance().updateAppUpdateUrlInfo(id, updateUrl);
    }

    public MyAppError deleteAppUpdateUrlInfo(int id) {
        return AdminDAO.getInstance().deleteAppUpdateUrlInfo(id);
    }

    public AppUpdateUrlDTO getAppUpdateUrlDTO(AppUpdateUrlForm appUpdateUrlForm) {
        AppUpdateUrlDTO dto = new AppUpdateUrlDTO();
        dto.setId(appUpdateUrlForm.getId());
        dto.setPlatform(appUpdateUrlForm.getPlatform());
        dto.setAppType(appUpdateUrlForm.getAppType());
        dto.setUpdateUrl(appUpdateUrlForm.getUpdateUrl());
        dto.setAppName(appUpdateUrlForm.getAppName());
        return dto;
    }

    public List<DeactiveReasonDTO> getDeactivateReasonsList(String queryString) {
        List<DeactiveReasonDTO> deactiveReasonDTOs = new ArrayList<>();
        try {
            List<DeactiveReasonDTO> list = CassandraDAO.getInstance().getDeactivateReasons();
            for (DeactiveReasonDTO dto : list) {
                if (queryString != null && queryString.length() > 0 && !dto.getDescription().toLowerCase().contains(queryString)) {
                    continue;
                }
                deactiveReasonDTOs.add(dto);
            }
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception [getDeactivateReasonsList] --> " + e);
        }
        return deactiveReasonDTOs;
    }

    public int addDeactivateReasonsInfo(int id, String description) {
        int reasonCode = -1;
        try {
            reasonCode = CassandraDAO.getInstance().addDeactivateReasons(id, description);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [addDeactivateReasonsInfo] --> " + e);
        }
        return reasonCode;
    }

    public int addDeactivateReasonsInfo(String description) {
        return AdminDAO.getInstance().addDeactivateReasonsInfo(description);
    }

    public int updateOTPRetryLimit(int limit) {
        int reasonCode = -1;
        try {
            reasonCode = CassandraDAO.getInstance().updateOTPRetryLimit(limit);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [updateOTPRetryLimit] --> " + e.getMessage());
        }
        return reasonCode;
    }

    public int updateOTPRetryResetTime(long time) {
        int reasonCode = -1;
        try {
            reasonCode = CassandraDAO.getInstance().updateOTPRetryResetTime(time);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [updateOTPRetryResetTime] --> " + e.getMessage());
        }
        return reasonCode;
    }

    public int getOTPRetryLimit() {
        int limit = 0;
        try {
            limit = CassandraDAO.getInstance().getOTPRetryLimit();
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [getOTPRetryLimit] --> " + e.getMessage());
        }
        return limit;
    }

    public long getOTPRetryLimitResetTime() {
        long time = 0L;
        try {
            time = CassandraDAO.getInstance().getOTPRetryLimitResetTime();
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [getOTPRetryLimitResetTime ] --> " + e.getMessage());
        }
        return time;
    }

    public int resetSecondaryContactVerificationHistory(long userId, int contactType) {
        int result = -1;
        try {
            result = CassandraDAO.getInstance().resetSecondaryContactVerificationHistory(userId, contactType);
        } catch (Exception e) {
        }
        return result;
    }

    public int resetDeviceIdVerificationHistory(String deviceId) {
        int result = -1;
        try {
            result = CassandraDAO.getInstance().resetDeviceIdVerificationHistory(deviceId);
        } catch (Exception e) {
        }
        return result;
    }

    public int updateNameUpdateLimit(int value) {
        int responseCode = -1;
        try {
            responseCode = CassandraDAO.getInstance().updateNameUpdateLimit(value);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception [updateNameUpdateLimit] --> ", e);
        }
        return responseCode;
    }

    public boolean anonymousCallChatOnOff(int settingsName, int enable, long userId) {
        boolean reasonCode = false;
        try {
            reasonCode = CassandraDAO.getInstance().updateToggleSettings(settingsName, enable, userId);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [anonymousCallChat] --> " + e);
        }
        return reasonCode;
    }

    public long getDefaultRestrictedTime() {
        long restrictionTime = 0;
        try {
            restrictionTime = CassandraDAO.getInstance().getDefaultRestrictedTime();
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().debug("Exception in [getDefaultRestrictedTime] --> " + e);
        }
        return restrictionTime;
    }

    public int updateRestrictionTime(long time) {
        int responseCode = -1;
        try {
            responseCode = CassandraDAO.getInstance().updateRestrictionTime(time);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception [updateRestrictionTime] --> ", e);
        }
        return responseCode;
    }

    /*
     * resetRetryLimit
     */
    public int resetMNVCRetryLimit(long userId) {
        int result = -1;
        try {
            result = CassandraDAO.getInstance().resetMNVCRetryLimit(userId);
        } catch (Exception e) {
        }
        return result;
    }

    public int resetRVCRetryLimit(long userId) {
        int result = -1;
        try {
            result = CassandraDAO.getInstance().resetRVCRetryLimit(userId);
        } catch (Exception e) {
        }
        return result;
    }

    public int resetOTPHistory(long userId) {
        int result = -1;
        try {
            result = CassandraDAO.getInstance().resetOTPHistory(userId);
        } catch (Exception e) {
        }
        return result;
    }

    public int resetOTPHistoryByDevice(long userId) {
        int result = -1;
        try {
            result = CassandraDAO.getInstance().resetOTPHistoryByDevice(userId);
        } catch (Exception e) {
        }
        return result;
    }

    public int resetOTPHistoryByDeviceId(String deviceId) {
        int result = -1;
        try {
            result = CassandraDAO.getInstance().resetOTPHistoryByDeviceId(deviceId);
        } catch (Exception e) {
        }
        return result;
    }

    public int resetOTPHistoryByNumber(String mobileNumber) {
        int result = -1;
        try {
            result = CassandraDAO.getInstance().resetOTPHistoryByNumber(mobileNumber);
        } catch (Exception e) {
        }
        return result;
    }

    public ObjectReturnDTO getLatestOTP(String ringId) {
        ObjectReturnDTO returnDTO = null;
        try {
            long userId = Long.parseLong(ringId);
            long tempId = CassandraDAO.getInstance().getUserTableId(userId);
            if (tempId > 0) {
                userId = tempId;
            }
            returnDTO = CassandraDAO.getInstance().getLatestOTP(userId);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [getLatestOTP] --> " + e);
        }
        return returnDTO;
    }

    public int getCurrentNameUpdateLimit() {
        int value = 0;
        try {
            value = CassandraDAO.getInstance().getCurrentNameUpdateLimit();
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [getCurrentNameUpdateLimit] --> " + e);
        }
        return value;
    }

    public MyAppError deleteAllVirtualRingIdBeforeDate(long sendDate) {
        return AdminDAO.getInstance().deleteAllVirtualRingIdBeforeDate(sendDate);
    }

    public int getAllVirtualRingIdCountBeforeDate(long sendDate) {
        return AdminDAO.getInstance().getAllVirtualRingIdCountBeforeDate(sendDate);
    }
}
