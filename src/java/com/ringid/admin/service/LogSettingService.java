/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.service;

import com.ringid.admin.dao.LogSettingDAO;
import com.ringid.admin.dto.LogSettingDTO;
import com.ringid.admin.actions.logSettings.LogSettingForm;
import com.ringid.admin.repository.LogSettingLoader;
import com.ringid.admin.utils.MyAppError;
import java.util.List;

/**
 *
 * @author Rabby
 */
public class LogSettingService {

    private LogSettingService() {

    }

    private static class LogSettingTaskSchedulerHolder {

        private static final LogSettingService INSTANCE = new LogSettingService();
    }

    public static LogSettingService getInstance() {

        return LogSettingTaskSchedulerHolder.INSTANCE;
    }

    public List<LogSettingDTO> getLogSettingList(LogSettingDTO dto) {
        return LogSettingLoader.getInstance().getLogSettingList(dto);
    }

    public LogSettingDTO getLogSettingDTO(LogSettingForm logSettingForm) {
        LogSettingDTO logSettingDTO = new LogSettingDTO();

        if (!logSettingForm.isNull(logSettingForm.getName())) {
            logSettingDTO.setName(logSettingForm.getName());
        }

        logSettingDTO.setValue(logSettingForm.getValue());

        return logSettingDTO;
    }

    public MyAppError addLogSettingInfo(LogSettingDTO p_dto) {
        return LogSettingDAO.getInstance().addLogSettingInfo(p_dto);
    }

    public MyAppError deleteLogSettingInfo(int id) {
        return LogSettingDAO.getInstance().deleteLogSettingInfo(id);
    }

    public LogSettingDTO getLogSettingInfoById(int id) {
        return LogSettingLoader.getInstance().getLogSettingDTO(id);
    }

    public MyAppError updateLogSetting(LogSettingDTO p_dto) {
        return LogSettingDAO.getInstance().updateLogSettingInfo(p_dto);
    }

    public MyAppError updateLogSettingList(List<LogSettingDTO> list) {
        return LogSettingDAO.getInstance().updateLogSettingInfoList(list);
    }

    public List<LogSettingDTO> getLogSettingInfoListById(int[] idx) {
        return LogSettingLoader.getInstance().getLogSettingDTOList(idx);
    }
}
