/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.service;

import com.ringid.admin.dto.BreakingNewsDTO;
import com.ringid.admin.repository.BreakingNewsRepository;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import org.ringid.newsportals.NPBasicFeedInfoDTO;

/**
 *
 * @author Kazi Nasir Uddin Oly
 * @Date Dec 8, 2016
 */
public class BreakingNewsService {

    private final int DEFAULT_DURATION = 100000;//seconds

    private BreakingNewsService() {

    }

    private static class BreakingNewsTaskSchedulerHolder {

        private static final BreakingNewsService INSTANCE = new BreakingNewsService();
    }

    public static synchronized BreakingNewsService getInstance() {
        return BreakingNewsTaskSchedulerHolder.INSTANCE;
    }

    public ArrayList<NPBasicFeedInfoDTO> getBreakingNewsListFromPages(BreakingNewsDTO dto) {
        return new ArrayList<>(BreakingNewsRepository.getInstance().getBreakingNewsListFromPages(dto));
    }

    public ArrayList<NPBasicFeedInfoDTO> getCommonBreakingNewsList(BreakingNewsDTO dto) {
        return new ArrayList<>(BreakingNewsRepository.getInstance().getCommonBreakingNewsList(dto));
    }

    public int makeCommonBreakingFeed(ArrayList<UUID> feedIds) {
        Map<UUID, Integer> feedIdDuration = new HashMap<>();
        feedIds
                .stream()
                .forEach((uuid) -> {
                    feedIdDuration.put(uuid, DEFAULT_DURATION);
                });
        return BreakingNewsRepository.getInstance().makeCommonBreaking(feedIdDuration);
    }

    public int removeFromBreakingFeed(ArrayList<UUID> feedIds) {
        return BreakingNewsRepository.getInstance().removeFromCommonBreaking(feedIds);
    }

}
