/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.service;

import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.actions.sports.matchChannelMapped.ChannelMatchLoader;
import com.ringid.admin.dao.SportsDAO;
import com.ringid.admin.dto.MatchChannelMappedDTO;
import com.ringid.admin.actions.sports.matchChannelMapped.MatchDTO;
import com.ringid.admin.utils.MyAppError;
import java.util.ArrayList;
import java.util.List;
import org.ringid.channel.ChannelDTO;
import org.ringid.sports.cricket.MatchInfoDTO;
import org.ringid.utilities.AppConstants;

/**
 *
 * @author mamun
 */
public class SportsService {

    private static SportsService instance = null;

    private SportsService() {
    }

    public static SportsService getInstance() {
        if (instance == null) {
            createInstance();
        }
        return instance;
    }

    private synchronized static void createInstance() {
        if (instance == null) {
            instance = new SportsService();
        }
    }

    public void deleteEndedHomeMatch() {
        List<MatchInfoDTO> matchInfoList = ChannelMatchLoader.getInstance().getMatchList();
        List<String> homeMatchList = SportsDAO.getInstance().getHomeMatchList();

        for (String matchId : homeMatchList) {
            boolean contains = false;
            for (MatchInfoDTO dto : matchInfoList) {
                if (dto.getMatchID().equals(matchId)) {
                    contains = true;
                    break;
                }
            }
            if (!contains) {
                SportsDAO.getInstance().deleteHomeMatch(matchId);
            }
        }
    }

    public List<MatchDTO> getHomeMatchList(String queryString) {
        List<MatchDTO> list = new ArrayList<>();
        List<MatchInfoDTO> matchInfoList = ChannelMatchLoader.getInstance().getMatchList();
        List<String> homeMatchList = SportsDAO.getInstance().getHomeMatchList();

        for (MatchInfoDTO dto : matchInfoList) {
            MatchDTO matchDTO = new MatchDTO();
            matchDTO.setMatchId(dto.getMatchID());
            matchDTO.setTitle(dto.getTitle());
            matchDTO.setStatus(dto.getStatus());
            switch (dto.getStatus()) {
                case AppConstants.MATCH_STATUS.UPCOMING:
                    matchDTO.setStatusStr("UPCOMING");
                    break;
                case AppConstants.MATCH_STATUS.RUNNING:
                    matchDTO.setStatusStr("RUNNING");
                    break;
                case AppConstants.MATCH_STATUS.ENDED:
                    matchDTO.setStatusStr("ENDED");
                    break;
            }
            if (homeMatchList.contains(dto.getMatchID())) {
                matchDTO.setActive(true);
            } else {
                matchDTO.setActive(false);
            }

            if (queryString != null && queryString.length() > 0 && dto.getTitle() != null && !dto.getTitle().toLowerCase().contains(queryString)) {
                continue;
            }

            list.add(matchDTO);
        }
        return list;
    }

    public MyAppError addHomeMatch(String matchId) {
        return SportsDAO.getInstance().addHomeMatch(matchId);
    }

    public MyAppError deleteHomeMatch(String matchId) {
        return SportsDAO.getInstance().deleteHomeMatch(matchId);
    }

    public MatchDTO getMatchInfoDTOByMatchId(String matchId) {
        MatchInfoDTO matchInfoDTO = new MatchInfoDTO();
        MatchDTO matchDTO = new MatchDTO();
        try {
            matchInfoDTO = ChannelMatchLoader.getInstance().getMatchInfoDTOByMatchId(matchId);
            matchDTO.setMatchId(matchInfoDTO.getMatchID());
            matchDTO.setTitle(matchInfoDTO.getTitle());
            matchDTO.setStatus(matchInfoDTO.getStatus());
            switch (matchInfoDTO.getStatus()) {
                case AppConstants.MATCH_STATUS.UPCOMING:
                    matchDTO.setStatusStr("UPCOMING");
                    break;
                case AppConstants.MATCH_STATUS.RUNNING:
                    matchDTO.setStatusStr("RUNNING");
                    break;
                case AppConstants.MATCH_STATUS.ENDED:
                    matchDTO.setStatusStr("ENDED");
                    break;
            }
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().debug("Exception --> " + e);
        }
        return matchDTO;
    }

    public List<MatchChannelMappedDTO> getMatchChannelMapped(String matchId, int status) {
        List<MatchChannelMappedDTO> matchChannelMappedDTOs = new ArrayList<>();
        try {
            matchChannelMappedDTOs = ChannelMatchLoader.getInstance().getMatchChannelMapped(matchId, status);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().debug("Exception --> " + e);
        }
        return matchChannelMappedDTOs;
    }

    public List<MatchInfoDTO> getMatchList() {
        List<MatchInfoDTO> matchInfoDTOs = new ArrayList<>();
        try {
            matchInfoDTOs = ChannelMatchLoader.getInstance().getMatchList();
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().debug("Exception --> " + e);
        }
        RingLogger.getConfigPortalLogger().debug("[getMatchList] matchInfoDTOs.size() --> " + matchInfoDTOs.size());
        return matchInfoDTOs;
    }

    public List<ChannelDTO> getChannelList() {
        List<ChannelDTO> channelDTOs = new ArrayList<>();
        try {
            channelDTOs = ChannelMatchLoader.getInstance().getChannelList();
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().debug("Exception --> " + e);
        }
        RingLogger.getConfigPortalLogger().debug("[getChannelList] channelDTOs.size() --> " + channelDTOs.size());
        return channelDTOs;
    }

    public MyAppError addChannelMatchMapped(String matchId, String[] selectedIDs) {
        return SportsDAO.getInstance().addChannelMatchMapped(matchId, selectedIDs);
    }

    public MyAppError deleteChannelMatchMapped(String[] matchIds, String channelId) {
        return SportsDAO.getInstance().deleteChannelMatchMapped(matchIds, channelId);
    }

    public List<MatchDTO> getMatchChannelMappedAndUnmappedListByMatchId(String matchId) {
        return ChannelMatchLoader.getInstance().getMatchChannelMappedAndUnmappedListByMatchId(matchId);
    }
}
