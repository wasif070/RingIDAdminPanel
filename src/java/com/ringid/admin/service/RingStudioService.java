package com.ringid.admin.service;

import com.ringid.admin.dao.RingStudioDAO;
import com.ringid.admin.dto.RingStudioDTO;
import com.ringid.admin.actions.ringstudio.RingStudioForm;
import com.ringid.admin.utils.Constants;
import com.ringid.admin.utils.MyAppError;
import com.ringid.country.CountryLoader;
import java.util.ArrayList;
import java.util.Collections;

public class RingStudioService {

    private RingStudioService() {

    }

    private static class RingStudioServiceHolder {

        private static final RingStudioService INSTANCE = new RingStudioService();
    }

    public static RingStudioService getInstance() {
        return RingStudioServiceHolder.INSTANCE;
    }

    public ArrayList<RingStudioDTO> getRingStudioList() {
        ArrayList<RingStudioDTO> ringStudioList = RingStudioDAO.getInstance().getRingStudioList();
        Collections.sort(ringStudioList, new RingStudioDTO.CompIdDSC());

        return ringStudioList;
    }

    private boolean containsAny(RingStudioDTO dto, String queryString) {
        String countryName = CountryLoader.getInstance().getCountryName(dto.getCountryCode());
        return (dto.getDevice() > 0 && dto.getDevice() < Constants.DEVICE.length && Constants.DEVICE[dto.getDevice()].toLowerCase().contains(queryString))
                || (dto.getAppType() > 0 && dto.getAppType() < Constants.APP_TYPE.length && Constants.APP_TYPE[dto.getAppType()].toLowerCase().contains(queryString))
                || (dto.getImageName() != null && dto.getImageName().toLowerCase().contains(queryString))
                || (dto.getTitle() != null && dto.getTitle().toLowerCase().contains(queryString))
                || (dto.getUrl() != null && dto.getUrl().toLowerCase().contains(queryString))
                || (countryName != null && countryName.length() > 0 && countryName.toLowerCase().contains(queryString));
    }

    public ArrayList<RingStudioDTO> getRingStudioList(RingStudioDTO sdto) {
        ArrayList<RingStudioDTO> ringStudioDAOList = RingStudioDAO.getInstance().getRingStudioList();
        ArrayList<RingStudioDTO> ringStudioDTOs = new ArrayList<>();
        for (RingStudioDTO dto : ringStudioDAOList) {
            if (sdto.getSearchText() != null && sdto.getSearchText().length() > 0 && !containsAny(dto, sdto.getSearchText())) {
                continue;
            }
            ringStudioDTOs.add(dto);
        }
        return ringStudioDTOs;
    }

    public MyAppError addRingStudioInfo(RingStudioDTO p_dto) {
        return RingStudioDAO.getInstance().addRingStudioInfo(p_dto);
    }

    public RingStudioDTO getRingStudioInfoById(int id) {
        RingStudioDTO ringStudioDTO = RingStudioDAO.getInstance().getRingStudioDTO(id);
        return ringStudioDTO;
    }

    public MyAppError deleteRingStudio(int id) {
        return RingStudioDAO.getInstance().deleteRingStudio(id);
    }

    public MyAppError updateRingStudio(RingStudioDTO p_dto) {
        return RingStudioDAO.getInstance().updateRingStudio(p_dto);
    }

    public ArrayList<RingStudioDTO> getImageAndCountryCodeList() {
        ArrayList<RingStudioDTO> ringStudioList = RingStudioDAO.getInstance().getImageAndCountryCodeList();
        return ringStudioList;
    }

    public RingStudioDTO getRingStudioDTO(RingStudioForm ringStudioForm) {
        RingStudioDTO ringStudioDTO = new RingStudioDTO();

//        if (!ringStudioForm.isNull(ringStudioForm.getDevice())) {
//            ringStudioDTO.setDevice(ringStudioForm.getDevice());
//        }
//
//        if (!ringStudioForm.isNull(ringStudioForm.getVersion())) {
//            ringStudioDTO.setVersion(ringStudioForm.getVersion());
//        }
        return ringStudioDTO;
    }
}
