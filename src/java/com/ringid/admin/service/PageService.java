/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.service;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.ringid.admin.cassandra.CassandraDAO;
import com.ringid.admin.dao.PageDAO;
import com.ringid.admin.dto.DonationPageDTO;
import com.ringid.admin.repository.DonationPageLoader;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.actions.newsPortal.FeaturedPageLoader;
import com.ringid.admin.dto.newsportal.NewsPortalDTO;
import com.ringid.admin.actions.newsPortal.NewsPortalFeedBack;
import com.ringid.admin.actions.newsPortal.NewsPortalForm;
import com.ringid.admin.actions.ringId.autoFollowPages.AutoFollowPageLoader;
import com.ringid.admin.repository.NewsPortalLoader;
import com.ringid.admin.dto.newsportal.NewsPortalCategoryDTO;
import com.ringid.admin.repository.NewsPortalCategoryLoader;
import com.ringid.admin.dto.newsportal.NewsPortalTypeDTO;
import com.ringid.admin.dto.UserPageDTO;
import com.ringid.admin.utils.Constants;
import com.ringid.admin.utils.MyAppError;
import com.ringid.admin.utils.ReasonCode;
import dto.sessionless.SessionLessInfoDTO;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import org.ringid.contacts.FollowRuleDTO;
import org.ringid.newsfeeds.CassImageDTO;
import org.ringid.newsfeeds.ImageDTO;
import org.ringid.pages.CustomCategoryDTO;
import org.ringid.pages.PageCategoryDTO;
import org.ringid.pages.PageDTO;
import org.ringid.pages.ServiceDTO;
import org.ringid.users.UserBasicInfoDTO;
import org.ringid.users.UserDeactivationDTO;
import org.ringid.utilities.AppConstants;
import ringid.LoginTaskScheduler;
import ringid.sessionless.SessionlessTaskseheduler;

/**
 *
 * @author Rabby
 */
public class PageService {

    private static PageService instance = null;

    private PageService() {
    }

    public static PageService getInstance() {
        if (instance == null) {
            createInstance();
        }
        return instance;
    }

    private synchronized static void createInstance() {
        if (instance == null) {
            instance = new PageService();
        }
    }

    public int assignPageRingId(long userId, long ringId) {
        int reasonCode = -1;
        try {
            reasonCode = CassandraDAO.getInstance().assignPageRingId(userId, ringId);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [assignPageRingId] --> ", e);
        }
        return reasonCode;
    }

    public MyAppError updateAutoFollowPageStatusInfo(long pageId, boolean isEnable) {
        try {
            return PageDAO.getInstance().updateAutoFollowPageStatusInfo(pageId, isEnable);
        } catch (Exception e) {

        }
        return null;
    }

    public List<UserPageDTO> getAutoFollowPages(UserPageDTO userPageDTO) {
        List<UserPageDTO> list = new ArrayList<>();
        try {
            list = AutoFollowPageLoader.getInstance().getAutoFollowPages(userPageDTO);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [makePageDefault] --> " + e);
        }
        return list;
    }

    public int makePageDefault(long ownerId, long pageId) {
        int reasonCode = -1;
        try {
            reasonCode = CassandraDAO.getInstance().makePageDefault(ownerId, pageId);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [makePageDefault] --> " + e);
        }
        return reasonCode;
    }

    public MyAppError addPageCategory(NewsPortalCategoryDTO p_dto) {
        MyAppError error = new MyAppError();
        try {
            PageCategoryDTO pageCategoryDTO = new PageCategoryDTO();
            pageCategoryDTO.setCategoryName(p_dto.getName());
            pageCategoryDTO.setPageType(p_dto.getType());
            int reasonCode = CassandraDAO.getInstance().addPageCategory(pageCategoryDTO);
            if (reasonCode != 0) {
                error.setERROR_TYPE(MyAppError.DBERROR);
                error.setErrorMessage("ADD operation failed. reasonCode --> " + reasonCode);
            } else {
                error.setErrorMessage("Added successfully.");
            }
        } catch (Exception e) {
            error.setERROR_TYPE(MyAppError.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            RingLogger.getConfigPortalLogger().error("Exception in [addNewsPortalCategoryInfo]" + e);
        }
        return error;
    }

    public MyAppError addPageCategories(ArrayList<NewsPortalCategoryDTO> maps) {
        MyAppError error = new MyAppError();
        try {
            for (NewsPortalCategoryDTO p_dto : maps) {
                PageCategoryDTO pageCategoryDTO = new PageCategoryDTO();
                pageCategoryDTO.setCategoryName(p_dto.getName());
                pageCategoryDTO.setPageType(p_dto.getType());
                CassandraDAO.getInstance().addPageCategory(pageCategoryDTO);
            }
        } catch (Exception e) {
            error.setERROR_TYPE(MyAppError.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            RingLogger.getConfigPortalLogger().error("Exception in [addNewsPortalCategoryInfo]" + e);
        }
        return error;
    }

    public MyAppError deletePageCategory(UUID id, int pageType) {
        MyAppError error = new MyAppError();
        try {
            int reasonCode = CassandraDAO.getInstance().deletePageCategory(id, pageType);
            if (reasonCode > 0) {
                error.setERROR_TYPE(reasonCode);
                error.setErrorMessage("Unable to delete.");
            }
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [deleteNewsPortalCategory]" + e);
        }
        return error;
    }

    public MyAppError updatePageCategory(NewsPortalCategoryDTO p_dto) {
        MyAppError error = new MyAppError();
        try {
            int reasonCode = CassandraDAO.getInstance().editPageCategory(p_dto.getPageId(), p_dto.getName(), p_dto.getType());
            if (reasonCode > 0) {
                error.setERROR_TYPE(reasonCode);
                error.setErrorMessage("Unable to update.");
            }
        } catch (Exception e) {
            error.setERROR_TYPE(MyAppError.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            RingLogger.getConfigPortalLogger().error("Exception in [updateNewsPortalCategory]" + e);
        }
        return error;
    }

    public MyAppError addDefaultPageCategory(NewsPortalTypeDTO p_dto) {
        MyAppError error = new MyAppError();
        try {
            PageCategoryDTO pageCategoryDTO = new PageCategoryDTO();
            pageCategoryDTO.setCategoryName(p_dto.getName());
            pageCategoryDTO.setPageType(p_dto.getType());
            int reasonCode = CassandraDAO.getInstance().addDefaultCategory(pageCategoryDTO);
            if (reasonCode != 0) {
                error.setERROR_TYPE(MyAppError.DBERROR);
                error.setErrorMessage("ADD operation failed. reasonCode --> " + reasonCode);
            } else {
                error.setErrorMessage("Added successfully.");
            }
        } catch (Exception e) {
            error.setERROR_TYPE(MyAppError.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            RingLogger.getConfigPortalLogger().error(e);
        }
        return error;
    }

    public MyAppError addDefaultPageCategories(ArrayList<NewsPortalTypeDTO> maps) {
        MyAppError error = new MyAppError();
        try {
            for (NewsPortalTypeDTO p_dto : maps) {
                PageCategoryDTO pageCategoryDTO = new PageCategoryDTO();
                pageCategoryDTO.setCategoryName(p_dto.getName());
                pageCategoryDTO.setPageType(p_dto.getType());
                CassandraDAO.getInstance().addDefaultCategory(pageCategoryDTO);
            }
        } catch (Exception e) {
            error.setERROR_TYPE(MyAppError.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            RingLogger.getConfigPortalLogger().error(e);
        }
        return error;
    }

    public MyAppError deleteDefaultPageCategory(UUID defaultCategoryId, int pageType) {
        MyAppError error = new MyAppError();
        try {
            int reasonCode = CassandraDAO.getInstance().deleteDefaultCategory(defaultCategoryId, pageType);
            if (reasonCode > 0) {
                error.setERROR_TYPE(reasonCode);
                error.setErrorMessage("Unable to delete.");
            }
        } catch (Exception e) {
        }
        return error;
    }

    public MyAppError updateDefaultPageCategory(NewsPortalTypeDTO p_dto) {
        MyAppError error = new MyAppError();
        try {
            int reasonCode = CassandraDAO.getInstance().editDefaultCategory(p_dto.getId(), p_dto.getName(), p_dto.getType());
            if (reasonCode > 0) {
                error.setERROR_TYPE(reasonCode);
                error.setErrorMessage("Unable to update.");
            }
        } catch (Exception e) {
            error.setERROR_TYPE(MyAppError.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            RingLogger.getConfigPortalLogger().error(e);
        }
        return error;
    }

    public NewsPortalFeedBack getPageList(NewsPortalDTO sdto, int start, int limit) {
        NewsPortalFeedBack newsPortalFeedBack = new NewsPortalFeedBack();
        List<NewsPortalDTO> newsPortalDTOs = new ArrayList<>();
        int fromIndex, toIndex;
        fromIndex = start;
        toIndex = start + limit;
        RingLogger.getConfigPortalLogger().info("fromIndex: " + fromIndex + " toIndex: " + toIndex);
        try {
            newsPortalDTOs = NewsPortalLoader.getInstance().getNewsPortalDTOList(sdto);
            if (sdto.getSortType() == Constants.DESC_SORT) {
                Collections.sort(newsPortalDTOs, new NewsPortalDTO.CompDSC());
            } else {
                Collections.sort(newsPortalDTOs, new NewsPortalDTO.CompASC());
            }
            toIndex = (int) Math.min(toIndex, newsPortalDTOs.size());
            newsPortalFeedBack.setNewsPortalList(newsPortalDTOs.subList(fromIndex, toIndex));
            newsPortalFeedBack.setTotalResult(newsPortalDTOs.size());
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().debug("Exception --> " + e);
        } catch (Throwable t) {
            RingLogger.getConfigPortalLogger().debug("Throwable --> ", t);
        }
        return newsPortalFeedBack;
    }

    public int updatePageWeight(long pageId, int pageType, int weight) {
        int reasonCode = -1;
        try {
            reasonCode = CassandraDAO.getInstance().updatePageWeight(pageId, pageType, weight);
        } catch (Exception e) {
        }
        return reasonCode;
    }

    public MyAppError changePagesDiscoverableState(String[] pageIds, boolean discoverable) {
        int reasonCode;
        MyAppError myAppError = new MyAppError();
        myAppError.setErrorMessage("Updated successfully.");
        for (String pageId : pageIds) {
            try {
                if (discoverable) {
                    reasonCode = CassandraDAO.getInstance().makeDiscoverable(Long.parseLong(pageId));
                } else {
                    reasonCode = CassandraDAO.getInstance().removeFromDiscoverable(Long.parseLong(pageId));
                }
                RingLogger.getConfigPortalLogger().debug("## reasonCode :: reasonCode --> " + reasonCode);
                if (reasonCode > 0) {
                    myAppError.setERROR_TYPE(reasonCode);
                    switch (reasonCode) {
                        case ReasonCode.CANNOT_BE_DISCOVERABLE:
                            myAppError.setErrorMessage("This page doesn't have any category.");
                            break;
                        default:
                            myAppError.setErrorMessage("Update failed");
                            break;
                    }
                    break;
                }
            } catch (Exception e) {
                myAppError.setERROR_TYPE(MyAppError.DBERROR);
                myAppError.setErrorMessage("Update failed");
                break;
            }
        }
        return myAppError;
    }

    public MyAppError makePagesTrending(String[] pageIds) {
        return PageDAO.getInstance().updatePageTrending(pageIds, true);
    }

    public MyAppError makePagesNotTrending(String[] pageIds) {
        return PageDAO.getInstance().updatePageTrending(pageIds, false);
    }

    public MyAppError makePageFeatured(String[] pageIds, int pageType) {
        MyAppError myAppError = new MyAppError();
        Set<String> countriesSet = new HashSet<>();
        countriesSet.add("world");
        int reasonCode = -1;
        try {
            for (String pageid : pageIds) {
                FollowRuleDTO dto = new FollowRuleDTO();
                dto.setCountriesISO(countriesSet);
                reasonCode = CassandraDAO.getInstance().makePageFeatured(Long.parseLong(pageid), pageType, dto);
            }
            if (reasonCode == ReasonCode.NONE) {
                myAppError.setErrorMessage("Page Featured Successful");
            } else {
                myAppError.setERROR_TYPE(reasonCode);
            }
        } catch (Exception e) {
            myAppError.setERROR_TYPE(MyAppError.OTHERERROR);
        }
        return myAppError;
    }

    public MyAppError makePageUnfeatured(String[] pageIds, int pageType, boolean disableAutoFollow) {
        MyAppError myAppError = new MyAppError();
        List<Long> pageids = new ArrayList<>();
        for (String pageid : pageIds) {
            pageids.add(Long.parseLong(pageid));
        }
        int reasonCode = -1;
        try {
            reasonCode = CassandraDAO.getInstance().makePageUnfeatured(pageids, pageType, disableAutoFollow);
            if (reasonCode == ReasonCode.NONE) {
                myAppError.setErrorMessage("Page Unfeatured Successful");
            } else {
                myAppError.setERROR_TYPE(reasonCode);
            }
        } catch (Exception e) {
            myAppError.setERROR_TYPE(MyAppError.OTHERERROR);
        }
        return myAppError;
    }

    public NewsPortalFeedBack getFeaturedPages(NewsPortalDTO newsPortalDTO, int start, int limit) {
        NewsPortalFeedBack newsPortalFeedBack = new NewsPortalFeedBack();
        List<NewsPortalDTO> newsPortalList = new ArrayList<>();
        int fromIndex, toIndex;
        fromIndex = start;
        toIndex = start + limit;
        RingLogger.getConfigPortalLogger().info("fromIndex: " + fromIndex + " toIndex: " + toIndex);
        try {
            newsPortalList = FeaturedPageLoader.getInstance().getFeaturedPageList(newsPortalDTO);
            if (newsPortalDTO.getSortType() == Constants.DESC_SORT) {
                Collections.sort(newsPortalList, new NewsPortalDTO.CompDSC());
            } else {
                Collections.sort(newsPortalList, new NewsPortalDTO.CompASC());
            }
        } catch (Exception ex) {
            RingLogger.getConfigPortalLogger().error("Exception in [getFeaturedPages] --> ", ex);
        }
        toIndex = (int) Math.min(toIndex, newsPortalList.size());
        newsPortalFeedBack.setNewsPortalList(newsPortalList.subList(fromIndex, toIndex));
        newsPortalFeedBack.setTotalResult(newsPortalList.size());
        return newsPortalFeedBack;
    }

    public int enableOrDisablePageAutoFollow(String pageRingId, int autoFollowStatus) {
        int reasonCode = -1;
        long pageId = 0L;
        try {
            if (pageRingId != null && pageRingId.length() > 0) {
                pageId = Long.parseLong(pageRingId);
                long tempId = CassandraDAO.getInstance().getUserTableId(pageId);
                if (tempId > 0) {
                    pageId = tempId;
                }
                reasonCode = CassandraDAO.getInstance().enableOrDisableAutoFollow(pageId, (autoFollowStatus == 1));
                if (reasonCode == ReasonCode.NONE) {
                    MyAppError error = PageService.getInstance().updateAutoFollowPageStatusInfo(pageId, (autoFollowStatus == 1));
                    if (error != null && error.getERROR_TYPE() == MyAppError.NOERROR) {
                        AutoFollowPageLoader.getInstance().forceReload();
                    }
                }
            }
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [enableOrDisableAutoFollow] --> " + e);
        }
        return reasonCode;
    }

    public PageDTO getPageInfo(long pageId) {
        PageDTO pageDTO = new PageDTO();
        try {
            pageDTO = CassandraDAO.getInstance().getPageInfo(pageId);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [getPageInfo] --> " + e);
        }
        return pageDTO;
    }

    public int updatePage(PageDTO pageDTO) {
        int reasonCode = -1;
        try {
            switch (pageDTO.getPageType()) {
                case AppConstants.UserTypeConstants.USER_PAGE:
                    reasonCode = CassandraDAO.getInstance().updateUserPage(pageDTO, pageDTO.getPageId());
                    break;
                case AppConstants.UserTypeConstants.NEWSPORTAL:
                case AppConstants.UserTypeConstants.BUSINESS_PAGE:
                case AppConstants.UserTypeConstants.MEDIA_PAGE:
                case AppConstants.UserTypeConstants.SPORTS_PAGE:
                case AppConstants.UserTypeConstants.PRODUCT_PAGE:
                    reasonCode = CassandraDAO.getInstance().updatePage(pageDTO, pageDTO.getPageId());
                    if (reasonCode == 0) {
                        if (pageDTO.getProfileImageId() != null) {
                            updatePageProfileImage(pageDTO.getOwnerId(), pageDTO.getPageId(), pageDTO.getProfileImageId(), pageDTO.getProfileImageUrl());
                        }
                        if (pageDTO.getCoverImageId() != null) {
                            updatePageCoverImage(pageDTO.getOwnerId(), pageDTO.getPageId(), pageDTO.getCoverImageId(), pageDTO.getCoverImageUrl(), pageDTO.getCoverImageX(), pageDTO.getCoverImageY());
                        }
                    }
                    break;
                default:
                    break;
            }
            if (reasonCode == AppConstants.NONE) {
                PageDAO.getInstance().updatePage(pageDTO);
            }
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [updatePage] -- > " + e);
        }
        return reasonCode;
    }

    public int deactiveUserPages(String[] deactivateReasons) {
        int reasonCode = -1;
        try {
            for (String deactivateReason : deactivateReasons) {
                UserDeactivationDTO dto = new UserDeactivationDTO();
                JsonParser parser = new JsonParser();
                JsonObject jsonObject = (JsonObject) parser.parse(deactivateReason);
                dto.setUserId(jsonObject.get("pageId").getAsLong());
                dto.setReason(jsonObject.get("reason").getAsInt());
                reasonCode = CassandraDAO.getInstance().deactivatePage(dto);
                if (reasonCode == 0) {
                    PageDAO.getInstance().updatePageStatus(dto.getUserId(), AppConstants.UserActiveStatus.DEACTIVE);
                }
            }
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [deactiveUserPages] -- > " + e);
        }
        return reasonCode;
    }

    public int createPage(PageDTO pageDTO) {
        int reasonCode = -1;

        try {
            UserBasicInfoDTO userBasicInfoDTO = CassandraDAO.getInstance().getUserBasicInfoByRingID(pageDTO.getOwnerId(), true);
            if (userBasicInfoDTO == null) {
                RingLogger.getConfigPortalLogger().debug("[createPage] Invalid owner Id " + pageDTO.getOwnerId());
                return reasonCode;
            }
            pageDTO.setOwnerId(userBasicInfoDTO.getUserId());
            pageDTO.setPageOwner(userBasicInfoDTO.getUserId());
            switch (pageDTO.getPageType()) {
                case AppConstants.UserTypeConstants.USER_PAGE:
                    reasonCode = CassandraDAO.getInstance().createUserPage(pageDTO);
                    break;
                case AppConstants.UserTypeConstants.NEWSPORTAL:
                case AppConstants.UserTypeConstants.BUSINESS_PAGE:
                case AppConstants.UserTypeConstants.MEDIA_PAGE:
                case AppConstants.UserTypeConstants.SPORTS_PAGE:
                case AppConstants.UserTypeConstants.PRODUCT_PAGE:
                    NewsPortalCategoryDTO dto = new NewsPortalCategoryDTO();
                    dto.setType(pageDTO.getPageType());
                    dto.setIsSearchByType(true);
                    ArrayList<NewsPortalCategoryDTO> categoryList = NewsPortalCategoryLoader.getInstance().getNewsPortalCategoryList(dto);
                    if (categoryList != null && categoryList.size() > 0) {
                        pageDTO.setPageCategoryId(categoryList.get(0).getPageId());
                        pageDTO.setPageCategoryName(categoryList.get(0).getName());
                    }
                    reasonCode = CassandraDAO.getInstance().createPage(pageDTO);
                    if (reasonCode == 0) {
                        if (pageDTO.getProfileImageId() != null) {
                            updatePageProfileImage(pageDTO.getOwnerId(), pageDTO.getPageId(), pageDTO.getProfileImageId(), pageDTO.getProfileImageUrl());
                        }
                        if (pageDTO.getCoverImageId() != null) {
                            updatePageCoverImage(pageDTO.getOwnerId(), pageDTO.getPageId(), pageDTO.getCoverImageId(), pageDTO.getCoverImageUrl(), pageDTO.getCoverImageX(), pageDTO.getCoverImageY());
                        }
                        if (pageDTO.getPageType() == AppConstants.UserTypeConstants.PRODUCT_PAGE) {
                            if (categoryList != null && categoryList.size() > 0) {
//                                CassandraDAO.getInstance().editDefaultCategory(categoryList.get(0).getPageId(), categoryList.get(0).getName());
                                CustomCategoryDTO customCategoryDTO = new CustomCategoryDTO();
                                PageCategoryDTO pageCategoryDTO = new PageCategoryDTO();
                                pageCategoryDTO = NewsPortalCategoryLoader.getInstance().getProductPageDefaultCategoryDTO();
//                                customCategoryDTO.setCustomCategoryId(pageCategoryDTO.getPageCategoryId());
                                customCategoryDTO.setDefaultCategoryId(pageCategoryDTO.getDefaultPageCategoryId());
//                                customCategoryDTO.setOldCategoryName(categoryList.get(0).getName());
                                customCategoryDTO.setType(AppConstants.UserTypeConstants.PRODUCT_PAGE);
                                customCategoryDTO.setCategoryName(categoryList.get(0).getName());
                                customCategoryDTO.setPageId(pageDTO.getPageId());
                                customCategoryDTO.setPageType(pageDTO.getPageType());
                                CassandraDAO.getInstance().addUserDefinedPageCategory(pageDTO.getPageId(), customCategoryDTO, 0);
                            }
                        }
                    }
                    break;
                default:
                    break;
            }
            if (reasonCode == AppConstants.NONE) {
                PageDAO.getInstance().addPage(pageDTO);
            }
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [createPage] -- > " + e);
        }
        return reasonCode;
    }

    public int updatePageProfileImage(long ownerId, long pageId, UUID profileImageId, String imageUrl) {
        int reasonCode = -1;
        try {
            reasonCode = CassandraDAO.getInstance().updatePageProfileImage(ownerId, pageId, profileImageId, imageUrl);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().debug("Exception in [updatePageProfileImage] -> " + e);
        }
        return reasonCode;
    }

    public int updatePageCoverImage(long ownerId, long pageId, UUID profileImageId, String imageUrl, int x, int y) {
        int reasonCode = -1;
        try {
            reasonCode = CassandraDAO.getInstance().updatePageCoverImage(ownerId, pageId, profileImageId, imageUrl, x, y);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().debug("Exception in [updatePageCoverImage] -> " + e);
        }
        return reasonCode;
    }

    public List<UserPageDTO> getUserPageList(UserPageDTO sdto) {
        List<UserPageDTO> pageList = new ArrayList<>();
        try {
            long userTableId = sdto.getPageId();
            long tempId = CassandraDAO.getInstance().getUserTableId(sdto.getPageId());
            if (tempId > 0) {
                userTableId = tempId;
            }
            if (userTableId > 0) {
                HashMap<Integer, List<ServiceDTO>> userPageMap = CassandraDAO.getInstance().getPageMap(userTableId);
                List<ServiceDTO> serviceDTOs = new ArrayList<>();
                switch (sdto.getPageType()) {
                    case AppConstants.UserTypeConstants.USER_PAGE:
                        if (userPageMap.containsKey(AppConstants.UserTypeConstants.USER_PAGE)) {
                            serviceDTOs = userPageMap.get(AppConstants.UserTypeConstants.USER_PAGE);
                            for (ServiceDTO dto : serviceDTOs) {
                                UserPageDTO userPageDTO = new UserPageDTO();
                                userPageDTO.setPageId(dto.getPageId());
                                userPageDTO.setPageName(dto.getServiceName());
                                userPageDTO.setPageType(AppConstants.UserTypeConstants.USER_PAGE);
                                userPageDTO.setPageRingId(dto.getRingId());
                                userPageDTO.setProfileImageUrl(dto.getProfileImage());
                                switch (dto.getPageActiveStatus()) {
                                    case AppConstants.UserActiveStatus.ACTIVE:
                                        userPageDTO.setPageActiveStatus("ACTIVE");
                                        break;
                                    case AppConstants.UserActiveStatus.DEACTIVE:
                                        userPageDTO.setPageActiveStatus("DEACTIVE");
                                        break;
                                    case AppConstants.UserActiveStatus.CLOSED:
                                        userPageDTO.setPageActiveStatus("CLOSED");
                                        break;
                                }

                                pageList.add(userPageDTO);
                            }
                        }
                        break;
                    case AppConstants.UserTypeConstants.DONATION_PAGE:
                        if (userPageMap.containsKey(AppConstants.UserTypeConstants.DONATION_PAGE)) {
                            serviceDTOs = userPageMap.get(AppConstants.UserTypeConstants.DONATION_PAGE);
                            for (ServiceDTO dto : serviceDTOs) {
                                UserPageDTO userPageDTO = new UserPageDTO();
                                userPageDTO.setPageId(dto.getPageId());
                                userPageDTO.setPageName(dto.getServiceName());
                                userPageDTO.setPageType(AppConstants.UserTypeConstants.DONATION_PAGE);
                                userPageDTO.setPageRingId(dto.getRingId());
                                userPageDTO.setProfileImageUrl(dto.getProfileImage());
                                switch (dto.getPageActiveStatus()) {
                                    case AppConstants.UserActiveStatus.ACTIVE:
                                        userPageDTO.setPageActiveStatus("ACTIVE");
                                        break;
                                    case AppConstants.UserActiveStatus.DEACTIVE:
                                        userPageDTO.setPageActiveStatus("DEACTIVE");
                                        break;
                                    case AppConstants.UserActiveStatus.CLOSED:
                                        userPageDTO.setPageActiveStatus("CLOSED");
                                        break;
                                }

                                pageList.add(userPageDTO);
                            }
                        }
                        break;
                    case AppConstants.UserTypeConstants.CELEBRITY:
                        if (userPageMap.containsKey(AppConstants.UserTypeConstants.CELEBRITY)) {
                            serviceDTOs = userPageMap.get(AppConstants.UserTypeConstants.CELEBRITY);
                            for (ServiceDTO dto : serviceDTOs) {
                                UserPageDTO userPageDTO = new UserPageDTO();
                                userPageDTO.setPageId(dto.getPageId());
                                userPageDTO.setPageName(dto.getServiceName());
                                userPageDTO.setPageType(AppConstants.UserTypeConstants.CELEBRITY);
                                userPageDTO.setPageRingId(dto.getRingId());
                                userPageDTO.setProfileImageUrl(dto.getProfileImage());
                                switch (dto.getPageActiveStatus()) {
                                    case AppConstants.UserActiveStatus.ACTIVE:
                                        userPageDTO.setPageActiveStatus("ACTIVE");
                                        break;
                                    case AppConstants.UserActiveStatus.DEACTIVE:
                                        userPageDTO.setPageActiveStatus("DEACTIVE");
                                        break;
                                    case AppConstants.UserActiveStatus.CLOSED:
                                        userPageDTO.setPageActiveStatus("CLOSED");
                                        break;
                                }
                                pageList.add(userPageDTO);
                            }
                        }
                        break;
                    default:
                        if (userPageMap.containsKey(AppConstants.UserTypeConstants.USER_PAGE)) {
                            serviceDTOs = userPageMap.get(AppConstants.UserTypeConstants.USER_PAGE);
                            for (ServiceDTO dto : serviceDTOs) {
                                UserPageDTO userPageDTO = new UserPageDTO();
                                userPageDTO.setPageId(dto.getPageId());
                                userPageDTO.setPageName(dto.getServiceName());
                                userPageDTO.setPageType(AppConstants.UserTypeConstants.USER_PAGE);
                                userPageDTO.setPageRingId(dto.getRingId());
                                userPageDTO.setProfileImageUrl(dto.getProfileImage());
                                switch (dto.getPageActiveStatus()) {
                                    case AppConstants.UserActiveStatus.ACTIVE:
                                        userPageDTO.setPageActiveStatus("ACTIVE");
                                        break;
                                    case AppConstants.UserActiveStatus.DEACTIVE:
                                        userPageDTO.setPageActiveStatus("DEACTIVE");
                                        break;
                                    case AppConstants.UserActiveStatus.CLOSED:
                                        userPageDTO.setPageActiveStatus("CLOSED");
                                        break;
                                }

                                pageList.add(userPageDTO);
                            }
                        }
                        if (userPageMap.containsKey(AppConstants.UserTypeConstants.DONATION_PAGE)) {
                            serviceDTOs = userPageMap.get(AppConstants.UserTypeConstants.DONATION_PAGE);
                            for (ServiceDTO dto : serviceDTOs) {
                                UserPageDTO userPageDTO = new UserPageDTO();
                                userPageDTO.setPageId(dto.getPageId());
                                userPageDTO.setPageName(dto.getServiceName());
                                userPageDTO.setPageType(AppConstants.UserTypeConstants.DONATION_PAGE);
                                userPageDTO.setPageRingId(dto.getRingId());
                                userPageDTO.setProfileImageUrl(dto.getProfileImage());
                                switch (dto.getPageActiveStatus()) {
                                    case AppConstants.UserActiveStatus.ACTIVE:
                                        userPageDTO.setPageActiveStatus("ACTIVE");
                                        break;
                                    case AppConstants.UserActiveStatus.DEACTIVE:
                                        userPageDTO.setPageActiveStatus("DEACTIVE");
                                        break;
                                    case AppConstants.UserActiveStatus.CLOSED:
                                        userPageDTO.setPageActiveStatus("CLOSED");
                                        break;
                                }

                                pageList.add(userPageDTO);
                            }
                        }
                        if (userPageMap.containsKey(AppConstants.UserTypeConstants.CELEBRITY)) {
                            serviceDTOs = userPageMap.get(AppConstants.UserTypeConstants.CELEBRITY);
                            for (ServiceDTO dto : serviceDTOs) {
                                UserPageDTO userPageDTO = new UserPageDTO();
                                userPageDTO.setPageId(dto.getPageId());
                                userPageDTO.setPageName(dto.getServiceName());
                                userPageDTO.setPageType((AppConstants.UserTypeConstants.CELEBRITY));
                                userPageDTO.setPageRingId(dto.getRingId());
                                userPageDTO.setProfileImageUrl(dto.getProfileImage());
                                switch (dto.getPageActiveStatus()) {
                                    case AppConstants.UserActiveStatus.ACTIVE:
                                        userPageDTO.setPageActiveStatus("ACTIVE");
                                        break;
                                    case AppConstants.UserActiveStatus.DEACTIVE:
                                        userPageDTO.setPageActiveStatus("DEACTIVE");
                                        break;
                                    case AppConstants.UserActiveStatus.CLOSED:
                                        userPageDTO.setPageActiveStatus("CLOSED");
                                        break;
                                }

                                pageList.add(userPageDTO);
                            }
                        }
                        break;
                }
            }
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception [getUserPageList] --> ", e);
        }
        if (sdto.getSortType() == Constants.DESC_SORT) {
            Collections.sort(pageList, new UserPageDTO.CompDSC());
        } else {
            Collections.sort(pageList, new UserPageDTO.CompASC());
        }
        return pageList;
    }

    public FollowRuleDTO getPageFollowRulesByPageId(long pageId) {
        FollowRuleDTO followRuleDTO = new FollowRuleDTO();
        if (pageId > 0) {
            try {
                long tempId = CassandraDAO.getInstance().getUserTableId(pageId);
                if (tempId > 0) {
                    pageId = tempId;
                }
                followRuleDTO = CassandraDAO.getInstance().getPageFollowRuleByPageId(pageId);
            } catch (Exception e) {
                RingLogger.getConfigPortalLogger().error("Exception id getPageFollowRulesByPageId --> " + e);
            }
        }
        return followRuleDTO;
    }

    public int updatePageFollowRules(long pageId, int pageType, FollowRuleDTO dto) {
        int reasonCode = -1;
        try {
            FollowRuleDTO followRuleDTO = CassandraDAO.getInstance().getPageFollowRuleByPageId(pageId);
            if (followRuleDTO != null) {
                reasonCode = CassandraDAO.getInstance().updatePageFollowRules(pageId, pageType, dto);
            } else {
                reasonCode = CassandraDAO.getInstance().addAutoFollowForUserPage(pageId, dto);
            }
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("## Exception in updatePageFollowRules: " + e);
        }
        return reasonCode;
    }

    public int addAutoFollowForUserPage(long pageId, FollowRuleDTO dto) {
        int reasonCode = -1;
        try {
            reasonCode = CassandraDAO.getInstance().addAutoFollowForUserPage(pageId, dto);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("## Exception in addAutoFollowForUserPage : " + e);
        }
        return reasonCode;
    }

    public MyAppError addDonationPageBannerImageAndVisibility(long pageId, String bannerUrl, int visible) {
        MyAppError myAppError = new MyAppError();
        try {
            myAppError = PageDAO.getInstance().addDonationPageBannerImageAndVisibility(pageId, bannerUrl, visible);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [addOrUpdateDonationPageBannerImageAndVisibility] --> " + e);
        }
        return myAppError;
    }

    public DonationPageDTO getDonationPage() {
        DonationPageDTO donationPageDTO = new DonationPageDTO();
        try {
            donationPageDTO = PageDAO.getInstance().getDonationPage();
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [getDonationPage] --> " + e);
        }
        return donationPageDTO;
    }

    public List<PageDTO> getDonationPages(DonationPageDTO dto) {
        List<PageDTO> pageDTOs = null;
        try {
            pageDTOs = DonationPageLoader.getInstance().getDonationPageList(dto);
        } catch (Exception e) {

        }
        return pageDTOs;
    }

    public PageDTO getDonationPageById(long pageId) {
        PageDTO pageDTO = null;
        try {
            pageDTO = DonationPageLoader.getInstance().getDonationPageById(pageId);
        } catch (Exception e) {
        }
        return pageDTO;
    }

    public int createDonationPage(PageDTO pageDTO) {
        int reasonCode = -1;
        try {
            reasonCode = CassandraDAO.getInstance().createDonationPage(pageDTO);
            if (reasonCode == AppConstants.NONE) {
                PageDAO.getInstance().addPage(pageDTO);
            }
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [createDonationPage] --> " + e);
        }
        return reasonCode;
    }

    public int updateDonationPage(PageDTO pageDTO, long pageId) {
        int reasonCode = -1;
        try {
            reasonCode = CassandraDAO.getInstance().updateDonationPage(pageDTO, pageId);
            if (reasonCode == AppConstants.NONE) {
                PageDAO.getInstance().updatePage(pageDTO);
            }
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [updateDonationPage] --> " + e);
        }
        return reasonCode;
    }

    public int deactivateDonationPage(long pageId) {
        int reasonCode = -1;
        try {
            reasonCode = CassandraDAO.getInstance().deactivateDonationPage(pageId);
            if (reasonCode == AppConstants.NONE) {
                PageDAO.getInstance().updatePageStatus(pageId, AppConstants.UserActiveStatus.DEACTIVE);
            }
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [deactivateDonationPage] --> " + e);
        }
        return reasonCode;
    }

    public List<ServiceDTO> getUserPageList(long ringId) {
        List<ServiceDTO> pageList = new ArrayList<>();
        try {
            long userTableId = CassandraDAO.getInstance().getUserTableId(ringId);
            if (userTableId > 0) {
                HashMap<Integer, List<ServiceDTO>> userPageMap = CassandraDAO.getInstance().getPageMap(userTableId);
                pageList.addAll(userPageMap.get(AppConstants.UserTypeConstants.USER_PAGE));
            }
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception [makeUserCelebrity] --> ", e);
        }
        return pageList;
    }

    public void destroyPageOwnerSession(long pageId) {
        RingLogger.getConfigPortalLogger().info("[destroyPageOwnerSession] pageId -> " + pageId);
        try {
            long pageOwnerId = CassandraDAO.getInstance().getPageOwnerId(pageId);
            UserBasicInfoDTO pageOwnerInfo = CassandraDAO.getInstance().getUserBasicInfo(pageOwnerId);
            String pageOwnerRingId = String.valueOf(pageOwnerInfo.getRingId());
            LoginTaskScheduler.getInstance().initSessionLessCommunication(Constants.USERID);
            SessionLessInfoDTO infoDto = SessionlessTaskseheduler.getInstance().getRingServerInfo(Constants.USERID, pageOwnerRingId);
            if (infoDto != null) {
                RingLogger.getConfigPortalLogger().info("[destroyPageOwnerSession] authIP: " + infoDto.getIp().getHostAddress() + " authPort: " + infoDto.getPort());
                String result2 = SessionlessTaskseheduler.getInstance().destroySession(Constants.USERID, pageOwnerRingId, infoDto.getIp().getHostAddress(), infoDto.getPort());
                RingLogger.getConfigPortalLogger().info("[destroyPageOwnerSession] ringId --> " + pageOwnerRingId + " response --> " + result2);
            }
            LoginTaskScheduler.getInstance().destroySessionLessCommunication(Constants.USERID);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [destroyPageOwnerSession] ->" + e);
        }
    }

    public int verifyPage(long pageId, int pageType) {
        int reasonCode = -1;
        try {
            reasonCode = CassandraDAO.getInstance().verifyPage(pageId, pageType);
        } catch (Exception e) {
        }
        return reasonCode;
    }

    public int unverifyPage(long pageId, int pageType) {
        int reasonCode = -1;
        try {
            reasonCode = CassandraDAO.getInstance().unverifyPage(pageId, pageType);
        } catch (Exception e) {
        }
        return reasonCode;
    }

    public int addProfileOrCoverImage(ImageDTO imageDTO) {
        int reasonCode = -1;
        try {
            CassImageDTO cassImageDTO = new CassImageDTO();
            cassImageDTO.setImageUrl(imageDTO.getImageURL());
            cassImageDTO.setImageHeight(imageDTO.getImageHeight());
            cassImageDTO.setImageWidth(imageDTO.getImageWidth());
            cassImageDTO.setImageType(imageDTO.getImageType());
            cassImageDTO.setPrivacy(imageDTO.getImagePrivacy());
            cassImageDTO.setUserId(imageDTO.getUserTableID());

            reasonCode = CassandraDAO.getInstance().addProfileOrCoverImage(cassImageDTO);
            if (reasonCode == AppConstants.NONE) {
                imageDTO.setImageID(cassImageDTO.getImageId());
            }
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [addProfileOrCoverImage] " + e);
        }
        return reasonCode;
    }

    public NewsPortalDTO getNewsPortalDTO(NewsPortalForm newsPortalForm) {
        NewsPortalDTO dto = new NewsPortalDTO();

        dto.setDiscoverable(newsPortalForm.getDiscoverable());
        if (newsPortalForm.getDiscoverable() < 0) {
            dto.setDiscoverable(0);
            newsPortalForm.setDiscoverable(0);
        }
        dto.setDiscoverable(newsPortalForm.getDiscoverable());
        dto.setTrending(newsPortalForm.getTrending());
        dto.setPageName(newsPortalForm.getPageName());
        dto.setPageType(newsPortalForm.getPageType());
        if (newsPortalForm.getPageType() <= 0) {
            newsPortalForm.setPageType(AppConstants.UserTypeConstants.NEWSPORTAL);
            dto.setPageType(AppConstants.UserTypeConstants.NEWSPORTAL);
        }
        dto.setPageId(newsPortalForm.getPageId());
        dto.setPageRingId(newsPortalForm.getPageRingId());
        dto.setCountryName(newsPortalForm.getCountry());
        dto.setProfileImageUrl(newsPortalForm.getProfileImageUrl());
        dto.setCoverImageUrl(newsPortalForm.getCoverImageUrl());
        dto.setSlogan(newsPortalForm.getSlogan());
        dto.setPageOwnerId(newsPortalForm.getOwnerId());
        dto.setWeight(newsPortalForm.getWeight());

        return dto;
    }
}
