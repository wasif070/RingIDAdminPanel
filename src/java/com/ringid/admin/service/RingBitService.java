/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.service;

import com.ringid.admin.dao.RingBitDAO;
import com.ringid.admin.dto.RingBitDTO;
import com.ringid.admin.actions.ringBit.RingBitForm;
import com.ringid.admin.repository.RingBitLoader;
import com.ringid.admin.actions.settings.DataType;
import com.ringid.admin.utils.MyAppError;
import java.util.List;

/**
 *
 * @author Rabby
 */
public class RingBitService {

    private RingBitService() {

    }

    private static class RingBitTaskSchedulerHolder {

        private static final RingBitService INSTANCE = new RingBitService();
    }

    public static RingBitService getInstance() {
        return RingBitTaskSchedulerHolder.INSTANCE;
    }

    public List<RingBitDTO> getRingBitList(String searchText) {
        return RingBitLoader.getInstance().getRingBitList(searchText);
    }

    public RingBitDTO getRingBitDTO(RingBitForm ringBitForm) {
        RingBitDTO dto = new RingBitDTO();
        if (ringBitForm.getRingBitImageId() > 0) {
            dto.setRingBitImageId(ringBitForm.getRingBitImageId());
        }
        if (ringBitForm.getRingBitTextId() > 0) {
            dto.setRingBitTextId(ringBitForm.getRingBitTextId());
        }
        if (ringBitForm.getSettingsValueImage() != null) {
            dto.setSettingValueImage(ringBitForm.getSettingsValueImage());
        }
        if (ringBitForm.getSettingsValueText() != null) {
            dto.setSettingValueText(ringBitForm.getSettingsValueText());
        }
        dto.setDataType(DataType.DATA_TYPE_STRING);
        return dto;
    }

    public MyAppError addRingBitSettingInfo(RingBitDTO dto) {
        return RingBitDAO.getInstance().addRingBitSettingInfo(dto);
    }

    public MyAppError updateRingBitSettingInfo(RingBitDTO dto) {
        return RingBitDAO.getInstance().updateRingBitSettingInfo(dto);
    }
}
