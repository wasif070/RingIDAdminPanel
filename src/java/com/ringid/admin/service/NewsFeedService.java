/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.service;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.ringid.admin.cassandra.CassandraDAO;
import com.ringid.admin.utils.log.RingLogger;
import com.ringid.admin.actions.ringId.RingIdLoader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import org.ringid.newsfeeds.FeedDTO;
import org.ringid.newsfeeds.FeedMoodDTO;
import org.ringid.newsfeeds.FeedReturnDTO;
import org.ringid.newsfeeds.HighlightedFeedDTO;
import org.ringid.newsfeeds.StatusTagDTO;
import org.ringid.users.UserBasicInfoDTO;
import org.ringid.utilities.AppConstants;

/**
 *
 * @author Rabby
 */
public class NewsFeedService {

    private static NewsFeedService instance = null;

    private NewsFeedService() {
    }

    public static NewsFeedService getInstance() {
        if (instance == null) {
            createInstance();
        }
        return instance;
    }

    private synchronized static void createInstance() {
        if (instance == null) {
            instance = new NewsFeedService();
        }
    }

    public int editStatus(FeedDTO feedDTO, long sessionUserId) {
        int reasonCode = -1;
        try {
            reasonCode = CassandraDAO.getInstance().editStatus(feedDTO, sessionUserId);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("## Exception [editStatus] --> ", e);
        }
        return reasonCode;
    }

//    public FeedReturnDTO getFeedList
    public FeedMoodDTO getFeedMoodDTO(int moodId) {
        FeedMoodDTO feedMoodDTO = null;
        try {
            if (moodId > 0) {
                feedMoodDTO = CassandraDAO.getInstance().getFeedMoodDTO(moodId);
            }
        } catch (Exception e) {
        }
        return feedMoodDTO;
    }

    public int updateHighlightedFeedWeight(UUID feedId, int weight) {
        int reasonCode = -1;
        try {
            reasonCode = CassandraDAO.getInstance().updateHighlightedFeedWeight(feedId, weight);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [updateHighlightedFeedWeight] --> " + e);
        }
        return reasonCode;
    }

    public List<HighlightedFeedDTO> getHighlightedFeeds(int feedType) {
        List<HighlightedFeedDTO> highlightedFeedDTOs = new ArrayList<>();
        try {
            highlightedFeedDTOs = CassandraDAO.getInstance().getHighlightedFeeds(feedType);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [getHighlightedFeeds] --> " + e);
        }
        return highlightedFeedDTOs;
    }

    public int addFeedInHighlights(int feedType, String[] feedIdAndWeight) {
        int reasonCode = -1;
        JsonParser jsonParser = new JsonParser();
        try {
            for (String str : feedIdAndWeight) {
                JsonObject jsonObject = (JsonObject) jsonParser.parse(str);
                if (jsonObject.has("feedId") && jsonObject.has("weight")) {
                    reasonCode = CassandraDAO.getInstance().addFeedInHighlights(feedType, UUID.fromString(jsonObject.get("feedId").getAsString()), jsonObject.get("weight").getAsInt());
                } else {
                    RingLogger.getConfigPortalLogger().debug("[addFeedInHighlights] jsonObject -->  " + jsonObject.toString());
                }
            }
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [addFeedInHighlights] --> " + e);
        }
        return reasonCode;
    }

    public int removeFeedFromHighlights(UUID feedId) {
        int reasonCode = -1;
        try {
            reasonCode = CassandraDAO.getInstance().removeFeedFromHighlights(feedId);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [removeFeedFromHighlights] --> " + e);
        }
        return reasonCode;
    }

    public List<FeedDTO> getFeedList(long userId, int start, int limit, int action) {
        List<FeedDTO> feedDTOs = null;
        try {
            feedDTOs = RingIdLoader.getInstance().getfeedList(userId);
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in [getFeedList] --> " + e);
        }
        return feedDTOs;
    }

    public FeedReturnDTO getNewsFeedList(long sessionUserId, long wallOwnerId, UUID pivodFeedId, int action, int limit) {
        FeedReturnDTO feedReturnDTO = new FeedReturnDTO();
        RingLogger.getConfigPortalLogger().info("[getNewsFeedList] sessionUserId -> " + sessionUserId + " wallOwnerId -> " + wallOwnerId + " pivodFeedId -> " + (pivodFeedId == null ? "null" : pivodFeedId) + " limit -> " + limit);
        if (limit < 1) {
            limit = 10;
        }
        try {
            feedReturnDTO = CassandraDAO.getInstance().getFeedList(sessionUserId, wallOwnerId, null, action, pivodFeedId, null, AppConstants.SCROLL_DOWN, limit);
            for (FeedDTO dto : feedReturnDTO.getFeedDTOList()) {
                populateFeedDTO(dto);
                if (dto.getParentFeedInfo() != null) {
                    populateFeedDTO(dto.getParentFeedInfo());
                }
            }
            RingLogger.getConfigPortalLogger().debug("[getNewsFeedList] feedReturnDTO --> " + new Gson().toJson(feedReturnDTO));
        } catch (Exception e) {
            RingLogger.getConfigPortalLogger().error("Exception in getNewsFeedList -> " + e);
        }
        return feedReturnDTO;
    }

    private static void populateFeedDTO(FeedDTO dto) {
        try {
            //guestWriter
            if (dto.getGuestWriterId() > 0) {
                UserBasicInfoDTO basicInfoDTO = CassandraDAO.getInstance().getUserBasicInfo(dto.getGuestWriterId());
                dto.setGuestWriterInfo(basicInfoDTO);
            }
            //feedMood
            if (dto.getFeelings() != null) {
                dto.setMoodList(feedMoodFromCass(dto.getFeelings()));
            }
            //textTag
            if (dto.getTextTag() != null) {
                dto.setStatusTags(textTagFromCass(dto.getTextTag()));
            }
            //wallOwnerInfo
            dto.setWallOwnerInfo(CassandraDAO.getInstance().getUserBasicInfo(dto.getWallOwnerId()));
        } catch (Exception e) {
        }
    }

    private static List<FeedMoodDTO> feedMoodFromCass(List<Integer> moodIds) {
        List<FeedMoodDTO> feedMoodDTOs = null;
        if (moodIds != null) {
            try {
                feedMoodDTOs = new ArrayList<>();
                for (int id : moodIds) {
                    FeedMoodDTO feedMoodDTO = CassandraDAO.getInstance().getFeedMoodDTO(id);
                    feedMoodDTOs.add(feedMoodDTO);
                }
            } catch (Exception e) {
            }
        }
        return feedMoodDTOs;
    }

    private static List<StatusTagDTO> textTagFromCass(Map<Integer, Long> textTagMap) {
        List<StatusTagDTO> statusTagList = null;//new ArrayList<>();
        if (textTagMap != null) {
            try {
                statusTagList = new ArrayList<>();
                for (Map.Entry<Integer, Long> entry : textTagMap.entrySet()) {
                    StatusTagDTO statusTagDTO = new StatusTagDTO();
                    statusTagDTO.setPosition(entry.getKey());
                    statusTagDTO.setUserId(entry.getValue());
                    UserBasicInfoDTO userBasicInfoDTO = CassandraDAO.getInstance().getUserBasicInfo(statusTagDTO.getUserId());
                    if (userBasicInfoDTO != null) {
                        statusTagDTO.setName(userBasicInfoDTO.getFirstName());
                        statusTagDTO.setProfileImage(userBasicInfoDTO.getProfileImageURL());
                        statusTagDTO.setProfileImageId(userBasicInfoDTO.getProfileImageId());
                        statusTagDTO.setProfileType(userBasicInfoDTO.getUserType());
                        statusTagDTO.setRingID(userBasicInfoDTO.getUserIdentity());
                    }
                    statusTagList.add(statusTagDTO);
                }
            } catch (Exception e) {
            }
        }
        return statusTagList;
    }
}
