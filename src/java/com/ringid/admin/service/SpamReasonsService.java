/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.service;

import com.ringid.admin.repository.SpamReasonsLoader;
import com.ringid.admin.dao.SpamReasonsDAO;
import com.ringid.admin.dto.SpamReasonsDTO;
import com.ringid.admin.actions.spamReasons.SpamReasonsForm;
import com.ringid.admin.utils.MyAppError;
import java.util.ArrayList;

/**
 *
 * @author mamun
 * @Date Feb 7, 2017
 */
public class SpamReasonsService {

    private SpamReasonsService() {

    }

    private static class SpamReasonsTaskSchedulerHolder {

        private static final SpamReasonsService INSTANCE = new SpamReasonsService();
    }

    public static SpamReasonsService getInstance() {
        return SpamReasonsTaskSchedulerHolder.INSTANCE;
    }

    public ArrayList<SpamReasonsDTO> getSpamReasonsList(SpamReasonsDTO dto) {
        return SpamReasonsLoader.getInstance().getSpamReasonsList(dto);
    }

    public ArrayList<SpamReasonsDTO> getSpamReasonsList(int columnValue, int sortType, SpamReasonsDTO dto) {
        return SpamReasonsLoader.getInstance().getSpamReasonsList(columnValue, sortType, dto);
    }

    public MyAppError addSpamReasonsInfo(SpamReasonsDTO p_dto) {
        return SpamReasonsDAO.getInstance().addSpamReasonsInfo(p_dto);
    }

    public MyAppError addSpamReasonsInfo(ArrayList<SpamReasonsDTO> maps) {
        return SpamReasonsDAO.getInstance().addSpamReasonsInfo(maps);
    }

    public SpamReasonsDTO getSpamReasonsInfoById(int id) {
        return SpamReasonsLoader.getInstance().getSpamReasonsDTO(id);
    }

    public MyAppError deleteSpamReasons(int id) {
        return SpamReasonsDAO.getInstance().deleteSpamReasons(id);
    }

    public MyAppError updateSpamReasons(SpamReasonsDTO p_dto) {
        return SpamReasonsDAO.getInstance().updateSpamReasons(p_dto);
    }

    public ArrayList<SpamReasonsDTO> getSpamReasonsListForDownload() {
        return SpamReasonsLoader.getInstance().getSpamReasonsListForDownload();
    }

    public SpamReasonsDTO getSpamReasonsDTO(SpamReasonsForm spamReasonsForm) {
        SpamReasonsDTO spamReasonsDTO = new SpamReasonsDTO();

        if (!spamReasonsDTO.isNull(String.valueOf(spamReasonsForm.getSpamType()))) {
            spamReasonsDTO.setSpamType(spamReasonsForm.getSpamType());
        }

        if (!spamReasonsDTO.isNull(spamReasonsForm.getReason())) {
            spamReasonsDTO.setReason(spamReasonsForm.getReason());
        }

        if (!spamReasonsDTO.isNull(spamReasonsForm.getSearchText())) {
            spamReasonsDTO.setReason(spamReasonsForm.getSearchText());
        }

        return spamReasonsDTO;
    }

    ArrayList<SpamReasonsDTO> getSpamReasonsSearchList(SpamReasonsDTO spamReasonsDTO) {
        return SpamReasonsLoader.getInstance().getSpamReasonsSearchList(spamReasonsDTO);
    }
}
