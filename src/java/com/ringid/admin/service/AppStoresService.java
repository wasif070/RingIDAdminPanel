/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.service;

import com.ringid.admin.dto.AppStoresDTO;
import com.ringid.admin.actions.appStores.AppStoresForm;
import com.ringid.admin.dao.AppStoresDAO;
import com.ringid.admin.utils.MyAppError;
import java.util.ArrayList;
import java.util.Collections;

/**
 *
 * @author mamun
 * @Date Jan 28, 2017
 */
public class AppStoresService {

    private AppStoresService() {
    }

    private static class AppStoresTaskSchedulerHolder {

        private static final AppStoresService INSTANCE = new AppStoresService();
    }

    public static AppStoresService getInstance() {
        return AppStoresTaskSchedulerHolder.INSTANCE;
    }

    public ArrayList<AppStoresDTO> getAppStoresList() {
        ArrayList<AppStoresDTO> appStoresList = AppStoresDAO.getInstance().getAppStoresList();
        Collections.sort(appStoresList, new AppStoresDTO.CompIdDSC());
        return appStoresList;
    }

    public ArrayList<AppStoresDTO> getAppStoresList(int columnValue, int sortType, AppStoresDTO dto) {
        ArrayList<AppStoresDTO> appStoresList = AppStoresDAO.getInstance().getAppStoresList(dto.getQueryString());
        Collections.sort(appStoresList, new AppStoresDTO.CompIdDSC());
        return appStoresList;
    }

    public MyAppError addAppStoresInfo(AppStoresDTO p_dto) {
        return AppStoresDAO.getInstance().addAppStoresInfo(p_dto);
    }

    public AppStoresDTO getAppStoresInfoById(int id) {
        return AppStoresDAO.getInstance().getAppStoresDTO(id);
    }

    public MyAppError deleteAppStores(int id) {
        return AppStoresDAO.getInstance().deleteAppStores(id);
    }

    public MyAppError updateAppStores(AppStoresDTO p_dto) {
        return AppStoresDAO.getInstance().updateAppStores(p_dto);
    }

    public AppStoresDTO getAppStoresDTO(AppStoresForm appStoresForm) {
        AppStoresDTO appStoresDTO = new AppStoresDTO();
        if (!appStoresForm.isNull(appStoresForm.getDeviceType())) {
            appStoresDTO.setDevice(Integer.parseInt(appStoresForm.getDeviceType()));
        }
        if (!appStoresForm.isNull(appStoresForm.getVersion())) {
            appStoresDTO.setVersion(appStoresForm.getVersion());
        }
        return appStoresDTO;
    }
}
