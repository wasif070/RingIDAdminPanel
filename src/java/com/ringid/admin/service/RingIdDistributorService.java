/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.service;

import com.ringid.admin.repository.RingIdDistributorLoader;
import com.ringid.admin.dao.RingIdDistributorDAO;
import com.ringid.admin.dto.RingIdDistributorDTO;
import com.ringid.admin.utils.MyAppError;
import java.util.List;

/**
 *
 * @author Rabby
 */
public class RingIdDistributorService {

    private RingIdDistributorService() {

    }

    private static class RingIdDistributorTaskSchedulerHolder {

        private static final RingIdDistributorService INSTANCE = new RingIdDistributorService();
    }

    public static RingIdDistributorService getInstance() {
        return RingIdDistributorTaskSchedulerHolder.INSTANCE;
    }

    public List<RingIdDistributorDTO> getRingIdDistributorList(RingIdDistributorDTO dto) {
        return RingIdDistributorLoader.getInstance().getRingIdDistributorList(dto);
    }

    public RingIdDistributorDTO getRingIdDistributorDTO(RingIdDistributorDTO dto) {
        return RingIdDistributorLoader.getInstance().getRingIdDistributorDTO(dto);
    }

    public MyAppError addRingIdDistributor(RingIdDistributorDTO dto) {
        return RingIdDistributorDAO.getInstance().addRingIdDistributor(dto);
    }

    public MyAppError updateRingIdDistributor(RingIdDistributorDTO oldDTO, RingIdDistributorDTO newDTO) {
        return RingIdDistributorDAO.getInstance().updateRingIdDistributor(oldDTO, newDTO);
    }

    public MyAppError deleteRingIdDistributor(RingIdDistributorDTO dto) {
        return RingIdDistributorDAO.getInstance().deleteRingIdDistributor(dto);
    }

}
