/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.service;

import com.ringid.admin.repository.SettingLoader;
import com.ringid.admin.dao.SettingDAO;
import com.ringid.admin.dto.adminAuth.LoginDTO;
import com.ringid.admin.dto.SettingDTO;
import com.ringid.admin.actions.settings.SettingForm;
import com.ringid.admin.actions.settings.SettingListForm;
import com.ringid.admin.utils.MyAppError;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 *
 * @author ipvision
 */
public class SettingService {

    private SettingService() {

    }

    private static class SettingTaskSchedulerHolder {

        private static final SettingService INSTANCE = new SettingService();
    }

    public static SettingService getInstance() {
        return SettingTaskSchedulerHolder.INSTANCE;
    }

    public List<SettingDTO> getSettingList(SettingDTO dto, LoginDTO loginDTO) {
        return SettingLoader.getInstance().getSettingList(dto, loginDTO);
    }

    public SettingDTO getSettingDTO(SettingForm settingForm) {
        String strSettingValue = "";
        SettingDTO settingDTO = new SettingDTO();
        settingDTO.setId(settingForm.getId());
        settingDTO.setSettingsValue(strSettingValue);
        if (!settingForm.isNull(settingForm.getName())) {
            settingDTO.setName(settingForm.getName());
        }
        if (!settingForm.isNull(settingForm.getSettingsValue())) {
            settingDTO.setSettingsValue(settingForm.getSettingsValue());
        }
        if ((settingForm.getName().trim().toLowerCase().equals("home-page-banner-type-seq") || settingForm.getName().trim().toLowerCase().equals("call-verification-enabled-country-dialing-codes") || settingForm.getName().trim().toLowerCase().equals("marketplace-enabled-country-dialing-codes")) && settingForm.getSelectedIDs() != null && settingForm.getSelectedIDs().length > 0) {
            for (String str : settingForm.getSelectedIDs()) {
                if (strSettingValue.length() > 0) {
                    strSettingValue += "," + str;
                } else {
                    strSettingValue = str;
                }
            }
            settingDTO.setSettingsValue(strSettingValue);
        }
        settingDTO.setDataType(settingForm.getDataType());
        return settingDTO;
    }

    public List<SettingDTO> getSettingDTOList(SettingListForm settingForm) {
        List<SettingDTO> settingDTOList = new ArrayList<>();

        settingForm.getListItems().stream()
                .map((dto) -> {
                    SettingDTO settingDTO = new SettingDTO();
                    if (!settingForm.isNull(dto.getName())) {
                        settingDTO.setName(dto.getName());
                    }
                    if (!settingForm.isNull(dto.getSettingsValue())) {
                        settingDTO.setSettingsValue(dto.getSettingsValue());
                    }
                    settingDTO.setDataType(dto.getDataType());
                    return dto;
                })
                .forEach((dto) -> {
                    settingDTOList.add(dto);
                });
        return settingDTOList;
    }

    public MyAppError addSettingInfo(SettingDTO p_dto) {
        return SettingDAO.getInstance().addSettingInfo(p_dto);
    }

    public MyAppError deleteSettingInfo(long id) {
        return SettingDAO.getInstance().deleteSettingInfo(id);
    }

    public SettingDTO getSettingInfoById(long id, LoginDTO loginDTO) {
        return SettingLoader.getInstance().getSettingDTO(id, loginDTO);
    }

    public List<SettingDTO> getSettingInfoByIds(String[] selectedIDs) {
        return SettingLoader.getInstance().getSettingDTOList(selectedIDs);
    }

    public MyAppError updateSetting(SettingDTO p_dto) {
        return SettingDAO.getInstance().updateSettingInfo(p_dto);
    }

    public MyAppError updateSettingList(List<SettingDTO> list) {
        return SettingDAO.getInstance().updateSettingInfoList(list);
    }

    public Set<Integer> getSettingPermissonById(long id) {
        return SettingDAO.getInstance().getSettingPermissionById(id);
    }

    public MyAppError addSettingPermission(long id, int[] roleId) {
        return SettingDAO.getInstance().addSettingPermission(id, roleId);
    }
}
