/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ringid.admin.service;

import com.ringid.admin.repository.PermittedIpsLoader;
import com.ringid.admin.dao.PermittedIpsDAO;
import com.ringid.admin.dto.PermittedIpsDTO;
import com.ringid.admin.utils.MyAppError;
import java.util.ArrayList;

/**
 *
 * @author Rabby
 */
public class PermittedIpsService {

    private PermittedIpsService() {

    }

    private static class PermittedIpsTaskSchedulerHolder {

        private static final PermittedIpsService INSTANCE = new PermittedIpsService();
    }

    public static PermittedIpsService getInstance() {
        return PermittedIpsTaskSchedulerHolder.INSTANCE;
    }

    public ArrayList<PermittedIpsDTO> getPermittedIpsList(PermittedIpsDTO dto) {
        return PermittedIpsLoader.getInstance().getPermittedIpsList(dto);
    }

    public ArrayList<PermittedIpsDTO> getPermittedIpsList(int columnValue, int sortType, PermittedIpsDTO dto) {
        return PermittedIpsLoader.getInstance().getPermittedIpsList(columnValue, sortType, dto);
    }

    public MyAppError addPermittedIpsInfo(PermittedIpsDTO p_dto) {
        return PermittedIpsDAO.getInstance().addPermittedIps(p_dto);
    }

    public PermittedIpsDTO getPermittedIpsInfoById(int id) {
        return PermittedIpsLoader.getInstance().getPermittedIpsDTO(id);
    }

    public MyAppError deletePermittedIps(int id) {
        return PermittedIpsDAO.getInstance().deletePermittedIps(id);
    }

    public MyAppError updatePermittedIps(PermittedIpsDTO p_dto) {
        return PermittedIpsDAO.getInstance().updatePermittedIps(p_dto);
    }

    public ArrayList<PermittedIpsDTO> getPermittedIpsListForDownload() {
        return PermittedIpsLoader.getInstance().getPermittedIpsListForDownload();
    }

}
